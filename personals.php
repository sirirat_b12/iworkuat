<?php 
session_start();
header('Content-Type: text/html; charset=utf-8');
// if(!isset($_SESSION["User"]['UserCode']) && ($_SESSION["User"]['type'] == "SuperAdmin")){
if(!isset($_SESSION["User"]['UserCode'])){
	echo "<script>alert('กรุณาเข้าสู่ระบบ!!!');</script>";
	echo '<META http-equiv="refresh" content="0;URL=login.php">';
	exit();
}

include "include/inc_header.php"; 
// include "include/inc_function.php"; 
include "include/inc_menu.php"; 
	
	$case = ($_SESSION["User"]['type'] != "SuperAdmin") ? "Sale" : "";
	$personnel = getAllPersonnel($case);
	$suplist = getsuplist();
	
	$sup_dropdown = "";
	foreach($suplist as $key_s => $sup){
		$sup_dropdown .= "<option value='".$sup["personnel_code"]."'>".$sup["sup_name"]."</option>";
	}					 
?> 

<div class="main">
	<div class="main-content p20">
		<div class=" bgff"> 
			<div class="p20">
			<h3 class="text-center">รายชื่อสมาชิก</h3>
			 <?php if($_SESSION["User"]['type'] == "SuperAdmin"){ ?>
			<div class="">
				<a href="#" class="c0aaaef adduser" ><i class="fa fa-pencil-square-o fwb fs14" aria-hidden="true"></i> เพิ่มสมาชิก</a>
			</div>
			 <?php } ?>
				<table id="table" class="table table-striped p5" data-search="true" data-toggle="table" data-pagination="true" data-page-size="100" data-page-list="[100, 150, 200, 250, 300. 500]" data-height="750" >
					<thead>
						<tr>
							<th class="t_c"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></th>
							<th class="t_c">รหัสพนักงาน</th>
							<?php if($_SESSION["User"]['type'] == "SuperAdmin"){ ?>
							<th class="t_c">รหัสผ่าน</th>
							<?php } ?>
							<th class="t_c">ชื่อ</th>
							<th class="t_c">นามสกุล</th>
							<th class="t_c">เบอร์ต่อ</th>
							<th data-field="phone" class="t_c">เบอร์</th>
							<th class="t_c">อีเมล์</th>
							<th class="t_c">ไลน์</th>
							<th class="t_c">สิทธิ์</th>
							<th class="t_c">Supervisor</th>		  
							<th class="t_c">สถานะ</th>
						</tr>
					</thead>
					<tbody class="fs12">
						<?php 
							foreach ($personnel as $key => $value) { 
						?>
							<tr>
								<td class="text-center">
									<a class="c0aaaef cursorPoin" onclick="edituserlist(<?php echo $value["personnel_id"]; ?>)">แก้ไข</a>
								</td>	
								<td class="t_c"><?php echo $value["personnel_code"]; ?></td>
								<?php if($_SESSION["User"]['type'] == "SuperAdmin"){ ?>
								<td class="t_c"><?php echo $value["personnel_password"]; ?></td>
								<?php } ?>
								<td class="t_c"><?php echo $value["personnel_firstname"]; ?></td>
								<td class="t_c"><?php echo $value["personnel_lastname"]; ?></td>
								<td class="t_c"><?php echo $value["personnel_extension"]; ?></td>
								<td class="t_c"><?php echo $value["personnel_phone"]; ?></td>
								<td class="t_c"><?php echo $value["personnel_email"]; ?></td>
								<td class="t_c"><?php echo $value["personnel_line"]; ?></td>
								<td class="t_c"><?php echo $value["personnel_type"]; ?></td>
								<td class="t_c"><?php echo $value["sup_name"]; ?></td>							  
								<td class="t_c"><?php echo ($value["personnel_status"] == 1) ? "ใช้งาน" : "ปิดใช้งาน" ; ?></td>
								
							</tr>
						<?php  } ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<div class="modal fade  bs-example-modal-lg" id="userAdd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header text-c">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">เพิ่มข้อมูลพนักงาน</h4>
			</div>
			<div class="modal-body">
				<form action="include/inc_action.php" method="POST"  id="addProfile" name="addProfile" >
					<input type="hidden" name="action" id="action" value="userAdd">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="fwn fs12">รหัสพนักงาน</label>
								<input class="form-control" type="text" name="personnel_code" required>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="fwn fs12">รหัสเข้าระบบ</label>
								<input class="form-control" type="password" name="personnel_password" required>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="fwn fs12">ชื่อ</label>
								<input class="form-control" type="text" name="personnel_firstname" irequired>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="fwn fs12">นามสกุล</label>
								<input class="form-control" type="text" name="personnel_lastname"  required>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="fwn fs12">เบอร์ต่อ</label>
								<input class="form-control" type="text" name="personnel_extension"  required>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="fwn fs12">เบอร์</label>
								<input class="form-control" type="text" name="personnel_phone"  required>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="fwn fs12">อีเมล์</label>
								<input class="form-control" type="text" name="personnel_email"  required>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="fwn fs12">ไลน์</label>
								<input class="form-control" type="text" name="personnel_line" required>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="fwn fs12">ประเภท</label>
								<select class="form-control" name="personnel_type" required>
							  		<option value="0">กรุณาเลือก</option>
							  		<option value="Sale">Sale</option>
							  		<option value="CallCenter">CallCenter</option>
							  		<option value="SuperAdmin">SuperAdmin</option>
							    </select>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="fwn fs12">Supervisor</label>
								<select class="form-control" name="personnel_supervisor" required>
							  		<option value="0">กรุณาเลือก</option>
							  		<?php echo $sup_dropdown; ?>
							    </select>
							</div>
						</div>	
						<div class="col-md-6">
							<div class="form-group">
								<label class="fwn fs12">สถานะ</label>
								<select class="form-control" name="personnel_status" required>
							  		<option value="1">ใช้งาน</option>
							  		<option value="0">ปิดใช้งาน</option>
							    </select>
							</div>
						</div>
					</div>
					<div class="t_c"><button type="submit" class="btn btn-success "><i class="fa fa-check-circle"></i>บันทึก</button></div>
				</form>
			</div>
			<div class="modal-footer"><button type="button" class="btn btn-default" data-dismiss="modal">Close</button></div>

		</div>
	</div>
</div>

<div class="modal fade  bs-example-modal-lg" id="userEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header text-c">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">ข้อมูลพนักงาน ตำแหน่ง <?php echo $userProfile["personnel_type"]; ?></h4>
			</div>
			<div class="modal-body">
				<form action="include/inc_action.php" method="POST"  id="editProfile" name="editProfile" >
					<input type="hidden" name="action" id="action" value="updateProfile">
					<input type="hidden" name="userid" id="userid" >
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="fwn fs12">รหัสพนักงาน</label>
								<input class="form-control" type="text" name="personnel_code" id="personnel_code" >
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="fwn fs12">รหัสเข้าระบบ</label>
								<input class="form-control" type="password" name="personnel_password" id="personnel_password">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="fwn fs12">ชื่อ</label>
								<input class="form-control" type="text" name="personnel_firstname" id="personnel_firstname" >
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="fwn fs12">นามสกุล</label>
								<input class="form-control" type="text" name="personnel_lastname" id="personnel_lastname" >
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="fwn fs12">เบอร์ต่อ</label>
								<input class="form-control" type="text" name="personnel_extension"  id="personnel_extension"  >
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="fwn fs12">เบอร์</label>
								<input class="form-control" type="text" name="personnel_phone" id="personnel_phone" >
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="fwn fs12">อีเมล์</label>
								<input class="form-control" type="text" name="personnel_email" id="personnel_email" >
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="fwn fs12">ไลน์</label>
								<input class="form-control" type="text" name="personnel_line" id="personnel_line" >
							</div>
						</div>
			<?php	if($_SESSION["User"]['type'] == "SuperAdmin"){?>			
						<div class="col-md-6">
							<div class="form-group">
								<label class="fwn fs12">ประเภท</label>
								<select class="form-control" name="personnel_type" id="personnel_type"  >
							  		<option value="">กรุณาเลือก</option>
							  		<option value="Sale">Sale</option>
							  		<option value="CallCenter">CallCenter</option>
							  		<option value="SuperAdmin">SuperAdmin</option>
							    </select>
							</div>
						</div>
			<?php	}	?>
						<div class="col-md-6">
							<div class="form-group">
								<label class="fwn fs12">Supervisor</label>
								<select class="form-control" name="personnel_supervisor" id="personnel_supervisor">
							  		<option value="0">กรุณาเลือก</option>
							  		<?php echo $sup_dropdown; ?>
							    </select>
							</div>
						</div>	
						<div class="col-md-6">
							<div class="form-group">
								<label class="fwn fs12">สถานะ</label>
								<select class="form-control" name="personnel_status" id="personnel_status"  >
							  		<option value="1">ใช้งาน</option>
							  		<option value="0">ปิดใช้งาน</option>
							    </select>
							</div>
						</div>
					</div>
					<div class="t_c"><button type="submit" class="btn btn-info btnsendPOemail"><i class="fa fa-check-circle"></i> แก้ไขข้อมูล</button></div>
				</form>
			</div>
			<div class="modal-footer"><button type="button" class="btn btn-default" data-dismiss="modal">Close</button></div>

		</div>
	</div>
</div>
<?php include "include/inc_footer.php"; ?> 
<script type="text/javascript">

	$(".adduser").click(function() {
	    $('#userAdd').modal('show'); 
	});

	
	function edituserlist(id){
		console.log(id);
		$.ajax({ 
			url: 'include/inc_action.php',
			type:'POST',
			// dataType: 'json',
			data: {action: 'getUsers', id:id},
			success:function(rs){
				var obj = jQuery.parseJSON(rs);
				$("#userid").val(obj.personnel_id);
				$("#personnel_code").val(obj.personnel_code);
				$("#personnel_password").val(obj.personnel_password);
				$("#personnel_firstname").val(obj.personnel_firstname);
				$("#personnel_lastname").val(obj.personnel_lastname);
				$("#personnel_phone").val(obj.personnel_phone);
				$("#personnel_extension").val(obj.personnel_extension);
				$("#personnel_email").val(obj.personnel_email);
				$("#personnel_line").val(obj.personnel_line);
				$("#personnel_type option[value='"+obj.personnel_type+"']").prop('selected', true);
				$("#personnel_status option[value='"+obj.personnel_status+"']").prop('selected', true);
				$("#personnel_supervisor option[value='"+obj.personnel_supervisor+"']").prop('selected', true);																				   
			}
		}); 
		$('#userEdit').modal('show'); 
	}
	function queryParams() {
	    return {
	        type: 'owner',
	        sort: 'updated',
	        direction: 'desc',
	        per_page: 1,
	        page: 1
	    };
	}
</script>