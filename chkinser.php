<?php 
session_start();

header('Content-Type: text/html; charset=utf-8');
if($_SESSION["User"]['UserCode']){
	echo "<script>alert('กรุณาเข้าสู่ระบบ!!!');</script>";
	echo '<META http-equiv="refresh" content="0;URL=login.php">';
	exit();
}

include "include/inc_header.php"; 
// include "include/inc_function.php"; 
include "include/inc_menu.php"; 
include "include/inc_function_offer.php"; 

if($_GET["cpid"]){

	$getRowCPAllBycode = getRowCPAllBycode($_GET["cpid"]);
	$MakeMS = ucwords(strtolower($getRowCPAllBycode["Make_Name_TH"]));
	$modelMS = ucwords(strtolower($getRowCPAllBycode["Model_Desc"]));
	$yearMS = $getRowCPAllBycode["Sub_Model"];
	$ccMS = $getRowCPAllBycode["Seat_CC_Weight"]."cc";
	$getmodel = getGeneration($MakeMS); 
}
	$class = getClass();
	$make = getMake();
	$insurer = getInsurer();

if($_SESSION['inslist']){
	$getlistIns = getlistIns();
}
// echo "<pre>".print_r($_SESSION['inslist'],1)."</pre>";
// echo "<pre>".print_r($getlistIns,1)."</pre>";

?> 
<div class="main">
			<!-- MAIN CONTENT -->
	<div class="main-content">
		<div class="container-fluid">
			<div class="panel panel-profile">
				<div class="clearfix">
					<!-- LEFT COLUMN -->
					<div class="profile-left">
						<div class="boxSearch">
							<p class="text-center fs16 fwb">กรุณาเลือกการค้นหา <?php echo $_GET["cpid"] ?></p>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
									  	<label for="usr">ยี่ห้อรถ<span class="c_red"> *</span>:</label>
									  	<select class="form-control formInput2" id="brand" name="brand" onchange="getGeneration(this.value);">
									  		<!-- <option value="Ford">Ford</option> -->
									  		<option value="0">กรุณาเลือก</option>
									  		<option value="Honda" <?php if($MakeMS == "Honda"){ echo "selected"; } ?>>Honda</option>
									  		<option value="Toyota" <?php if($MakeMS == "Toyota"){ echo "selected"; } ?>>Toyota</option>
									  		<option value="Ford" <?php if($MakeMS == "Ford"){ echo "selected"; } ?>>Ford</option>
									  		<option value="Isuzu" <?php if($MakeMS == "Isuzu"){ echo "selected"; } ?>>Isuzu</option>
									  		<option value="Mazda" <?php if($MakeMS == "Mazda"){ echo "selected"; } ?>>Mazda</option>
									  		<option value="Nissan" <?php if($MakeMS == "Nissan"){ echo "selected"; } ?>>Nissan</option>
									  		<option value="Suzuki" <?php if($MakeMS == "Suzuki"){ echo "selected"; } ?>>Suzuki</option>
									  		<option value="Mitsubishi" <?php if($MakeMS == "Mitsubishi"){ echo "selected"; } ?>>Mitsubishi</option>
									  		<option value="Chevrolet" <?php if($MakeMS == "Chevrolet"){ echo "selected"; } ?>>Chevrolet</option>
									  		<option value="0" disabled="disabled" separator="true">---------------------------------</option>
									  		<?php foreach ($make as $key => $value) { ?>
									  			<option value="<?php echo $value['redbook_tks_make']; ?>" <?php if($MakeMS == $value['redbook_tks_make']){ echo "selected"; } ?>><?php echo $value['redbook_tks_make']; ?></option>
									  		<?php } ?>
									  	</select>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
								  		<label for="sel1">ประเภทประกันภัย<span class="c_red"> *</span>:</label>
								  		<select class="form-control formInput2" id="class" name="nameclass" >
								  		<!-- <option value="1">1</option> -->
								  			<option value="all">ทุกประเภท</option>
									  		<?php foreach ($class as $key => $value) { ?>
									  			<option value="<?php echo $value['class_name']; ?>"><?php echo $value['class_name']; ?></option>
									  		<?php } ?>
								  		</select>
									</div>	
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
									  	<label for="usr">รุ่นรถ<span class="c_red"> *</span>:</label>
									  	<select class="form-control formInput2" id="model" name="model" onchange="getCC(this.value);">
									  	<!-- <option value="City">Ranger 2 Doors</option> -->
									  		<option value="0">กรุณาเลือกยี่ห้อรถ</option>
									  		<?php  foreach ($getmodel as $key => $value) { ?>
									  			<option value="<?php echo $value['redbook_tks_model']; ?>" <?php if(trim($modelMS) == $value['redbook_tks_model']){ echo "selected"; } ?> >
									  				<?php echo $value['redbook_tks_model']; ?>
									  			</option>
									  		<?php } ?>
									  	</select>
									</div>
								</div>
								
								<div class="col-md-6">
									<div class="form-group">
									  	<label for="usr">บริษัทประกัน:</label>
									  	<select class="form-control formInput2" id="insurers" name="insurer">
									  		<!-- <option value="วิริยะประกันภัย">วิริยะประกันภัย</option> -->
									  		<option value="0">ทั้งหมด</option>
									  		<?php foreach ($insurer as $key => $value) { ?>
									  			<option value="<?php echo $value['insurer_name']; ?>"><?php echo $value['insurer_name']; ?></option>
									  		<?php } ?>
									  	<!-- <input type="text" class="form-control" id="year" name="nameyear" value="2016"> -->
									    </select>
									</div>
								</div>
								
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
									  	<label for="usr">ปีออกรถ<span class="c_red"> *</span>:</label>
									    <select class="form-control formInput2" id="year" name="nameyear" onchange="getCC(this.value);">
									    <!-- <option value="2016">2016</option> -->
								  		<?php 
								  			$yearNow = date("Y");
								  			for ($i=0; $i <= 30 ; $i++) { 
								  				$rs = $yearNow - $i ;
								  		?>
									   	 	<option value="<?php echo $rs ?>" <?php if($rs == $yearMS){ echo "selected"; } ?>><?php echo $rs ?></option>
								  		<?php } ?>
								    </select> 
									  	<!-- <input type="text" class="form-control" id="year" name="nameyear" value="2016"> -->
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
									  	<label for="usr">ประเภทอู่:</label>
									  	<select class="form-control formInput2" id="repair" name="repair">
									  		<option value="0">ทั้งหมด</option>
									  		<option value="hall">ซ่อมห้าง</option>
									  		<option value="park">ซ่อมอู่</option>
									    </select>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
									  	<label for="usr">CC <span class="c_red"> *</span>:</label>
									   	<select class="form-control formInput2" id="cc" name="cc" >
									   		<!-- <option value="1500cc">1500cc</option> -->
									  		<option value="0">กรุณาเลือก รุ่นรถ ยี่ห้อ และปี</option>
									  		<?php 
									  			$getcccar = getCC($MakeMS, $modelMS, $yearMS);
									  			foreach ($getcccar as $key => $value) {
									  		?>
									  			<option value="<?php echo $value['redbook_tks_cc']; ?>" <?php if($value['redbook_tks_cc'] == $ccMS){ echo "selected"; } ?>>
									  				<?php echo $value['redbook_tks_cc']; ?>
									  			</option>
									  		<?php } ?>
									  	</select>
									</div>
								</div>
								<div class="col-md-6">
									<div class="mt10"><input type="checkbox" name="deduct" id="deduct" > ค่าเสียหายส่วนแรก</div>
								</div>
							</div>
							<div class="row">
								<div  class="col-md-6">
									<a href="#" class="btnCellcenter  cff8000 fwb"><i class="lnr lnr-location"></i> สร้างลูกค้าใหม่</a>
									<span class="fs12 mt5">* กรณีลูกค้าใหม่</span>
								</div>
								<div  class="col-md-6">
									<div class="text-center"><button type="Submit" id="btnSearch" class="btn btn-danger">ค้นหาข้อมูลประกัน</button></div>
								</div>
							</div>
								<div class="fs12 text-left">
										<b>หมายเหตุ</b>
										<br>- กรุณาตรวจสอบข้อมูลทุกครั้งก่อนทำการบันทึกข้อมูล เพื่อความถูกต้อง
										<br>- รหัสแพคเกตบางบริษัทตั้งขึ้นเองเนื่องจากบริษัทประกันไม่ให้รหัส กรุณาเช็คชื่อแพคเกตให้ครบถ้วน
										<br>- ทุนประกันกลางเป็นราคากลางที่กำหนดไว้ ไม่สามารถใช้ได้ทุกบริษัทกรุณาตวจสอบอย่างละเอียด
									</div>
						</div>
						<div class="boxList">
							<p class="fs14 t_c fwb">แพคเก็จที่เลือก</p>
							<table class="table table-bordered fs12">
								<thead>
									<tr>
										<th>รายละเอียด</th>
										<th>เบี้ย</th>
									</tr>
								</thead>
								<tbody id="tableList" class="table-list fs13">
									<?php 
									if($_SESSION['inslist']){ 
										foreach ($getlistIns as $key => $value) { 
									?>
										<tr id='tableINS<?php echo $value['insurance_id']; ?>'>
											<td class="p10">
												<?php echo $value["insurance_insurer"];?>
												<br><b><?php echo $value["insurance_Name"]?></b>
												<br><b>ทุน</b> <span class="cff8000"><?php echo number_format($value["inscost_minamount"]).'-'.number_format($value["inscost_maxamount"]); ?> </span>บาท 
											</td>
											<td class="p10">
												<b>เบี้ยสุทธิ</b> <?php echo number_format($value["inscost_premamount"]); ?><b >บ.</b>
												<br><b>รวมภาษี</b> <?php echo number_format($value["inscost_taxamount"]); ?><b> บ.</b>
												<br><b>ส่วนลด</b> <?php echo number_format($value["discount"]); ?><b> บ.</b>
												<br><b>ขาย</b> <?php echo number_format($value["payper"]); ?><b> บ.</b>
											</td>
										</tr>
									<?php  }
									} ?>
								</tbody>
							</table>
							<div id="divnMail" class="t_c">
								<form action="checkout.php" method="GET" >
									<!-- <input type="hidden" name="action" id="action" value="sendListsProduct"> -->
									<input type="hidden" name="make" id="ck_make">
									<input type="hidden" name="model" id="ck_model">
									<input type="hidden" name="cc" id="ck_cc">
									<input type="hidden" name="year" id="ck_year">
									<input type="hidden" name="class" id="ck_class">
									<input type="hidden" name="repair" id="ck_repair">
									<input type="hidden" name="cpid" value="<?php echo $_GET["cpid"];?>">
									<div class="t_c">
										<button type="submit" class="btn btn-success btnsendPOemail"><i class="fa fa-check-circle"></i> ส่งข้อเสนอ</button>
										<button type="button" class="btn btn-danger " id="btnDelPo"><i class="fa fa-trash-o"></i> ลบข้อเสนอทั้งหมด</button>
									</div>
								</form>
								<!-- <button type="button" class="btn btn btn-success btnSendPo <?php if(!$_SESSION['inslist']){ echo "dn"; } ?>" ><i class="lnr lnr-location"></i> ทำใบเสนอราคา</button>
								<button type="button" class="btn btn-danger " id="btnDelPo"><i class="fa fa-trash-o"></i> ลบข้อเสนอทั้งหมด</button> -->
							</div>	
						</div>
					</div>
					<!-- END LEFT COLUMN -->
					<!-- RIGHT COLUMN -->
					<div class="profile-right">
						<h4 class="heading">ข้อมูลประกัน</h4>
						<!-- <p class="fs12 mt5">- ทุนประกันกลาง มาจากทุนประกันที่มีอยู่ในระบบ รบกวนเช็คความถูกต้องในแต่หละบริษัท</p> -->
						<div class="panel-body">
						<table class="table table-hover table-bordered">
							<thead class="bgbaf4bc">
								<tr>
									<th>#</th>
									<th>รายละเอียดประกัน</th>
									<th>ส่วนลด</th>
									<th>ความคุ้มครอง</th>
								</tr>
							</thead>
							<tbody id="table_data" class="table-list fs13"><tr><td colspan="4" class="text-center">กรุณาเลือกข้อมูล</td></tr></tbody>
						</table>
					</div>
					</div>
					<!-- END RIGHT COLUMN -->
				</div>
			</div>
		</div>
	</div>
	<!-- END MAIN CONTENT -->
</div>


<div class="modal fade" id="Roadside" tabindex="-1" role="dialog" aria-labelledby="RoadsideTitle" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<p class="modal-title fs16 fwb cff8000 " id="memberModalLabel">บริการช่วยเหลือฉุกเฉิน<span id="insName"></span> บริษัท <span id="insClass"></span></p>
			</div>
			<div class="">
				<div class="p20">
					<div class="row">
						<div class="col-md-12">
							<!-- <div >
								<p class="fwb fs18 c2457ff ">สายด่วน/ เบอร์ติดต่อ</p>
								<p class="mt15 lh20" id="RoadsideContact"></p>
							</div> -->
							<div >
								<p class="fwb fs18 c2457ff ">รายละเอียด</p>
								<p class="mt15 lh20" id="RoadsideDescription"></p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="CondiPayModal" tabindex="-1" role="dialog" aria-labelledby="CondiPayTitle" aria-hidden="true">
	<div class="modal-dialog modal-lg ">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<p class="modal-title fs16 fwb cff8000 " >เงื่อนไข ตัดบัตรเครดิตโดยตรงกับ บริษัทประกัน</p>
			</div>
			<div class="">
				<div class="p20">
					<div class="row">
						<div >
							<div class="col-md-6">
								<p class="fwb fs18 c2457ff ">ระยะเวลา</p>
								<p class="mt15 lh20" id="CondiPay_Date"></p>
							</div>
							<div class="col-md-6">
								<p class="fwb fs18 c2457ff ">บัตรที่เข้าร่วม</p>
								<p class="mt15 lh20" id="CondiPay_Join_Cedit"></p>
							</div>
							<div class="col-md-6">
								<p class="fwb fs18 c2457ff ">จำนวนผ่อน</p>
								<p class="mt15 lh20" id="CondiPay_Installment_Count"></p>
							</div>
							<div class="col-md-6">
								<p class="fwb fs18 c2457ff ">เงื่อนไข</p>
								<p class="mt15 lh20 cf40053" id="CondiPay_Conditions"></p>
							</div>
							<div class="col-md-12 mt10">
								<p class="fwb fs18 c2457ff ">เงื่อนไข Cash Back</p>
								<table class="table table-responsive table-hover table-striped table-bordered fs10">
									<tr>
										<th>เบี้ย</th>
										<th>จำนวนเงิน</th>
										<th>วันที่โอนคืน</th>
									</tr>
									<tr>
										<td>6,000 - 8,999</td>
										<td>300</td>
										<td rowspan="5">
											<div>* ชำระ 1 - 15   ทำเงินคืน   วันที่ 25</div>
											<div>* ชำระ 16 - 31 ทำเงินคืน  วันที่ 5</div>
										</td>
									</tr>
									<tr>
										<td>9,000 - 11,999</td>
										<td>500</td>
									</tr>
									<tr>
										<td>12,000 - 14,999</td>
										<td>700</td>
									</tr>
									<tr>
										<td>15,000 - 19,999</td>
										<td>900</td>
									</tr>
									<tr>
										<td>20000 ขึ้นไป</td>
										<td>1,000</td>
									</tr>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="conditionANC" tabindex="-1" role="dialog" aria-labelledby="CondiPayTitle" aria-hidden="true">
	<div class="modal-dialog modal-lg ">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<p class="modal-title fs16 fwb cff8000 " >เงื่อนไข การผ่อนชำระกับ ANC </p>
			</div>
			<div class="">
				<div class="p20">
					<div class="row">
						<div >
							<p class="fwb fs18 c2457ff ">บัญชาลิสซิ่ง เอกสารประกอบการช้าร่วมโครงการ ตัวอย่างการผ่อน</p>
							<p><b>กรณีบุคคลรรรมดา</b></p> 
							<p>1. สำเนาบัตรประชาชน</p>
							<p>2. หลักฐานการชำระเงินงวดแรก</p>
							<p>3. คำขอผ่อนชำระเบี้ยประกันภัย</p>
							<p>4. หนังสือมอบอำนาจยกเลิกกรมธรรม์</p>

							<p><b>กรณีนิติบุคคล</b></p> 
							<p>1. สำเนาหนังสือริบรองบริษัท ไม่เกิน 3 เดือน</p>
							<p>2. สำเนาบัตรประชาชน กรรมการลงนาม</p>
							<p>3. หลักฐานกาชำระเงินงวดแรก</p>
							<p>4. หนังสือมอบอำนาจยกเลิกกรมธรรม์</p>
							<p>5. เช็คสั่งจ่ายล่วงหน้างวดแรก</p>

							<p><b>ขั้นตอน</b></p> 
							<p>1. ส่งเอกสาร</p>
							<p>2. ใบ pay หรือ เช็ค เข้า ANC 30% งวดที่ 1 *พรบ.ไม่สามารถผ่อนชำระได้*</p>
							<p>3. ใบยืนยันการเข้าระบบงาน ANC</p>
							<p><b>เงื่อนไขการผ่อนชำระ</b></p>
							<p>1. เบี้ยประกันภัยที่ ไม่เกิน 10,000 บาท สามารถผ่อนชำระได้ 4 งวด</p>
							<p>2. เบี้ยประกันภัยที่ เกิน 10,000 บาท สามารถผ่อนชำระได้ 4 และ 6 งวด</p>
							<p>3. งวดที่ 1 ชำระผ่าน ANC Broker โดยมียอดชำระที่ 30% ของค่าเบี้ยประกันภัย</p>

							<p><b>ฝ่ายบัญชี</b></p> 
							<p>1. ตรวจสอบเอกสารต่าง ๆ และยอดชำระเงิน จะต้องส่งให้ลูกค้ากรอก</p>
							<p>2. อนุมัติงานเพื่อส่งให้บัญชา</p>

							<p>ตัวอย่างการผ่อน ซื้อประกันรถราคาเต็ม 17,500
							การคิดงวดแรก นำ 17,500+ 30% จ่ายงวดแรก 5,250 บาท 17,500-5,250 เหลือ 12,250 บาท</p>

							<p class="c00ac0a"><b>บริษัท เอ เอ็น ซี โบรกเกอร์เรจ จำกัด</b></p>
							<p class="c00ac0a"><b>ธ.กรุงเทพ สาขาราชวัตร 146-4-09655-9</b></p>
							<p class="c00ac0a"><b>ธ.กรุงไทย สาขาราชวัตร 473-0-09315-1</b></p>
							<p class="c00ac0a"><b>ธ.กสิกรไทย สาขาราชวัตร 722-2-32941-2</b></p>
							<p class="c00ac0a"><b>ธ.ไทยพาณิชย์ สาขาราชวัตร 130-2-22890-2</b></p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<?php include "include/inc_footer.php"; ?> 
<!-- Magnify Image Viewer CSS -->
	<link href="magnifybox/css/jquery.magnify.css" rel="stylesheet">

	<!-- Magnify Image Viewer JS -->
	<script src="magnifybox/js/jquery.magnify.js"></script>
<script type="text/javascript">
	function noteShow(id){
		// console.log(id);
		$("#desc_"+id).toggle();
	}
	function btnShowPay(id){
		// console.log(id);
		$("#btnShowPay_"+id).toggle();
	}

	function showRoadside(insName, insClass){
		// console.log("insName");
		$("#insClass").html("");
		$.ajax({ 
			url: 'include/inc_action.php',
			type:'POST',
			// dataType: 'json',
			// data: {action:'getRoadsideIns',insName:insName, insClass:insClass},
			data: {action:'getRoadsideIns',insName:insName},
			success:function(rs){
				console.log(rs);
				$("#RoadsideContact, #RoadsideDescription").empty();
				// $("#RoadsideContact").html(rs);
				$("#RoadsideDescription").html(rs);
				$("#insClass").html(insName);
			}
		});
		$('#Roadside').modal();
	}

	function showCondiPay(con_ID){
		console.log("showCondiPay");
		$("#insClass").html("");
		$.ajax({ 
			url: 'include/inc_action.php',
			type:'POST',
			dataType: 'json',
			data: {action:'getCondiPay',con_ID:con_ID},
			success:function(rs){
				// console.log(rs);
				$("#CondiPay_Date").html(rs.startDate+" ถึง "+rs.EndDate);
				$("#CondiPay_Join_Cedit").html(rs.Join_Cedit);
				$("#CondiPay_Installment_Count").html(rs.Installment_Count);
				$("#CondiPay_Conditions").html(rs.Conditions);
				
				$("#insClass").html(insClass);
			}
		});
		$('#CondiPayModal').modal();
	}

	function showCondiANC(){
		console.log("showCondiANC");
		$('#conditionANC').modal();
	}
	
	function getGeneration(brand) {
	    var generation = 'generation';
	    var brand = brand;
		$.ajax({ 
			url: 'include/inc_action.php',
			type:'POST',
			dataType: 'json',
			data: {action:'generation',brand:brand},
			success:function(rs){
				$("#model").empty();
				$("#model").append('<option value="0">กรุณาเลือก</option>');
				$.each( rs, function( key, val ) {
		            $("#model").append('<option value="'+val.redbook_tks_model+'">'+val.redbook_tks_model+'</option>');
		        });
			}
		});
	}
	function getCC(rs) {
	    var brand = $("#brand").val();
		var generation = $("#model").val();
		var year = $("#year").val();
		$.ajax({ 
			url: 'include/inc_action.php',
			type:'POST',
			dataType: 'json',
			data: {action:"sqlCC", brand:brand, model:generation, year:year},
			success:function(rs){
				// console.log(rs);
				$("#cc").empty();
				$("#cc").append('<option value="0">กรุณาเลือก</option>');
				$.each( rs, function( key, val ) {
		            $("#cc").append('<option value="'+val.redbook_tks_cc+'">'+val.redbook_tks_cc+'</option>');
		        });
			}
		});
	}
	function numCommas(x) {
		if(x){
	    	return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	    }else{
	    	return 0.00;
	    }
	}
	function chknum(ip,ek) {
		if(!isNaN(ip.value)){
			return true;
		}
		ip.value="";
		alert('กรุณากรอกเป็นหมายเลขเท่านั้น');
		return true;
	}

	$(".btnCellcenter").click(function() {
		console.log($("#brand").val());
		$("#call_make").val($("#brand").val());
		$("#call_model").val($("#model").val());
		$("#call_cc").val($("#cc").val());
		$("#call_year").val($("#year").val());
		$("#call_class").val($("#class").val());
		$("#call_repair").val($("#repair").val());
		$('#sendPOCustomer').modal('show'); 
	});

	function setCarFrom(){
		$("#ck_make").val($("#brand").val());
		$("#ck_model").val($("#model").val());
		$("#ck_cc").val($("#cc").val());
		$("#ck_year").val($("#year").val());
		$("#ck_class").val($("#class").val());
		$("#ck_repair").val($("#repair").val());
	}

	function chkIns(ins, cost){
		if($('#chkIns'+cost).is(':checked') == true){
			if(ins && cost){
				var num = $('[name="chkIns[]"]:checked').length;
				var detail = '';
				$.ajax({ 
					url: 'include/inc_action.php',
					type:'POST',
					dataType: 'json',
					data: {action:"getInsurList", insid:ins, costid:cost},
					success:function(rs){
						console.log(rs);
						var num = rs.dataInSur.length;
						for (var i = 0; i < num; i++){
							var insur = rs.dataInSur[i].insurance_id;
							var inscost_id = rs.dataInSur[i].inscost_id;
							detail += "<tr id=tableINS"+inscost_id+">";
							detail += "<td class='p10'><b>"+rs.dataInSur[i].insurance_insurer+"</b><br>"+rs.dataInSur[i].insurance_Name+"<br><b>ทุน</b><span class='cff8000'> "+numCommas(rs.dataInSur[i].inscost_minamount)+"-"+numCommas(rs.dataInSur[i].inscost_maxamount)+"</span> บาท </td>";
							detail += "<td class='p10'>";
							detail += "<b>เบี้ยสุทธิ</b> "+numCommas(rs.dataInSur[i].inscost_premamount)+" <b>บ.</b>";
							detail += "<br><b>รวมภาษี</b> "+numCommas(rs.dataInSur[i].inscost_taxamount)+" <b>บ.</b>";
							detail += "<br><b>ส่วนลด</b> "+numCommas(rs.dataInSur[i].discount)+" <b>บ.</b>";
							detail += "<br><b>ขาย</b> "+numCommas(rs.dataInSur[i].payper)+" <b>บ.</b>";
							detail += "</td>";
							detail += "</tr>";
							$('#chkIns'+inscost_id).attr('checked', true);
						}
						$("#tableList").empty();
						$('#tableList').append(detail);
						$("#divnMail").show();
						$(".btnSendPo").show();
						if(rs.error != 1){
							alert(rs.error);
							$('#chkIns'+cost).attr('checked', false);
						}
					}
				});
			}
    }else if( $('#chkIns'+cost).is(':checked') == false){
    	// console.log("delSessionPO"+cost);
      $.ajax({ 
				url: 'include/inc_action.php',
				type:'POST',
				data: {action:"delSessionPO", insid:ins, costid:cost},
				success:function(rs){
					console.log(rs);
					if(rs==1){
						$("#tableINS"+cost).remove();
					}
				}
			});
   	}
	}

	$("#btnDelPo").click(function(){
		if(confirm("ยืนยันการลบขอเสนอทั้งหมด")){
			window.location.href = "chkinser.php";
			$.ajax({ 
				url: 'include/inc_action.php',
				type:'POST',
				dataType: 'json',
				data: {action:"delpo"},
				success:function(rs){
					<?php if($_GET["cpid"]) { ?>
						window.location.href = "chkinser.php?cpid=<?php echo $_GET["cpid"] ?>"; 
					<?php }else{ ?>
						window.location.href = "chkinser.php"; 
					<?php } ?>
				}
			});
		}
	});
	$("#btnSearch").click(function(){
		$("#table_data").empty();
		var type = "insurance";
		var class1 = $("#class").val();
		var brand = $("#brand").val();
		var generation = $("#model").val();
		var year = $("#year").val();
		var insurers = $("#insurers").val();
		var repair = $("#repair").val();
		var cc = $("#cc").val();
		var deduct = 0;
		if ($('#deduct').is(":checked")){
		  deduct = 1;
		}
		if(type == "insurance"){
			$.ajax('include/inc_action.php', {
				type: 'POST',
				data: {
					'action' : 'getInserance',
					'type': type,
					'class': class1,
					'brand': brand,
					'generation': generation,
					'year': year,
					'cc': cc,
					'repair': repair,
					'deduct': deduct,
					'insurers': insurers
				},
				crossDomain: true,
				success: function(rs) {
					if(rs){
				        $("#showins").show();
						$('#table_data').append(rs);
						$("#msg_error").hide();
						setCarFrom()
					}else{
						$("#showins").hide();
						$('#table_data').append("<td colspan='4' class='t_c pt50'><span class='fs20 fwb'>ไม่พบข้อมูลที่ค้นหา</span></td>");
					}
				}
			}); 
		}
	});

</script>