<?php 

session_start();
if(!isset($_SESSION["User"]['UserCode'])){
	echo "<script>alert('กรุณาเข้าสู่ระบบ!!!');</script>";
	echo '<META http-equiv="refresh" content="0;URL=login.php">';
	exit();
} 
// if($_SESSION["User"]['type'] == "CallCenter"){
// 	echo '<META http-equiv="refresh" content="0;URL=chkinser.php">';
// 	exit();
// }
header('Content-Type: text/html; charset=utf-8');
include "include/inc_header.php"; 
include "include/inc_menu.php"; 
$UserCode = $_SESSION["User"]['UserCode'];

if($_SESSION["User"]['type'] == "QualityControl"){
	$status = $_GET["filter"] ? $_GET["filter"] : 0 ;
	$send_orders = getAllSendPO($UserCode, "QualityControl", $status, $_GET["pocode"]);

}else if($_SESSION["User"]['type'] == "Sale" || $_SESSION["User"]['type'] == "CallCenter"){
	if($_GET["pocode"]){
		$send_orders = getAllSendPO($UserCode, "", $status, $_GET["pocode"]);
	}else{
		$status = $_GET["filter"] ? $_GET["filter"] : 0 ;
		$send_orders = getAllSendPO($UserCode, "", $status, $_GET["pocode"]);
	}

}else if($_GET["pocode"]){
	$send_orders = getAllSendPO("", "", "", $_GET["pocode"]);

}else{
	$status = $_GET["filter"] ? $_GET["filter"] : 0 ;
	$send_orders = getAllSendPO("", "", $status, "");
}
// echo "<pre>".print_r($send_orders,1)."</pre>";
$companyIns = getCompanyBysrv();
$getPremium = getPremium();
$getInsuranceType = getInsuranceType();

$getCompanyBysrv = getCompanyBysrv();
?> 
<div class="main">
	<div class="main-content p20">
		<div class=" bgff"> 
			<div class="p20">
				<div>
					<h3 >รายการแจ้งงาน</h3>
						<span><a href="#" title="แจ้งงาน" class="btn btn-danger " id="btn_open_send_order"><i class="fa fa-paper-plane-o" aria-hidden="true"> แจ้งงาน</i></a></span>
						<div class="mt30 dn" id="fromSendWork">
							<div class="row">
								<div class="col-md-6">
									<div class="col-md-4">
										<div class="form-group">
											<input class="form-control formInput2" type="text" name="po_code_search" id="po_code_search"  required>
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<input type="radio" name="search_type" value="1" checked> รหัส PO | 
											<input type="radio" name="search_type" value="2" > รหัสลูกค้า
										</div>
									</div>
									<div class="col-md-4" class="text-c">
										<div class="form-group" class="text-c">
											<button type="button" id="btn_send_order" class="btn  btn-info"><i class="fa fa-search-plus" aria-hidden="true"></i> เช็คข้อมูล</button>
										</div>
									</div>
								</div>
							</div>
							<div class="row mt20" style="border-bottom: 3px solid #555;">
									<form action="include/inc_action_chk.php" method="POST" id="form_send_order" enctype="multipart/form-data">
										<input type="hidden" value="sendOrderAdmin" name="action" id="action"> 
										<input type="hidden" value="<?php echo $_SESSION["User"]['UserCode'] ?>" name="personnel_code" id="personnel_code"> 
										<input type="hidden" value="<?php echo $_SESSION["User"]['firstname']." ".$_SESSION["User"]['lastname'] ?>" name="personnel_name" id="personnel_name">
										<input type="hidden" name="send_type" id="send_type"> 
											<table id="tableList" class="table table-bordered p5" >
												<thead>
													<tr>
														<th class="t_c"><i class="fa fa-list-ul" aria-hidden="true"></i></th>
														<th class="t_c">PO</th>
														<th class="t_c">วันคุ้มครอง</th>
														<th class="t_c">ชื่อลูกค้า</th>
														<th class="t_c">แพจเกต</th>
														<th class="t_c">ทุนประกัน</th>
														<th class="t_c">เบี้ย</th> 
														<th class="t_c">ของแถม</th> 
													</tr>
												</thead>
												<tbody>
													<tr id="tr_List"><td colspan="10" class="t_c fwb">กรุณาค้นหา</td></tr>
												</tbody>
											</table>
										<div class="col-md-12" style="background-color: #fafafa;padding-bottom: 15px;">
											<div class="col-md-6 imgCarBox">
												<div class="row mt20">
													<div class="col-md-3">
														<span class="fl fs12 cff2da5 cff2da5">รูปรถด้านซ้าย</span><span>
															<input type="file" name="filUpload[carleft]" id="fileUp_1" class="fl form-control inputFile" onchange="ValidateSingleInput(this);" accept="image/gif, image/jpeg"></span>
													</div>
													<div class="col-md-3">
														<span class="fl fs12 cff2da5">รูปรถด้านขวา</span><span>
															<input type="file" name="filUpload[carright]" id="fileUp_2" class="fl form-control inputFile" onchange="ValidateSingleInput(this);" accept="image/gif, image/jpeg"></span>
													</div>
												
													<div class="col-md-3">
														<span class="fl fs12 cff2da5">รูปรถด้านหน้า</span><span>
															<input type="file" name="filUpload[carfront]" id="fileUp_3" class="fl form-control inputFile" onchange="ValidateSingleInput(this);" accept="image/gif, image/jpeg"></span>
													</div>
													<div class="col-md-3">
														<span class="fl fs12 cff2da5">รูปรถด้านท้าย</span><span>
															<input type="file" name="filUpload[carback]" id="fileUp_4" class="fl form-control inputFile" onchange="ValidateSingleInput(this);" accept="image/gif, image/jpeg"></span>
													</div>
												</div>
												<div class="row mt15">
													<div class="col-md-3">
														<span class="fl fs12 cff2da5">สำเนารถ</span><span>
															<input type="file" name="filUpload[carbook]" id="fileUp_5" class="fl form-control inputFile" onchange="ValidateSingleInput(this);" accept="image/gif, image/jpeg , .pdf" ></span>
													</div>
													<div class="col-md-3">
														<span class="fl fs12 cff2da5">ใบขับบี่</span><span>
															<input type="file" name="filUpload[cardriver]" id="fileUp_6" class="fl form-control inputFile" onchange="ValidateSingleInput(this);" accept="image/gif, image/jpeg , .pdf"></span>
													</div>
													<div class="col-md-3 filecompanyBox">
														<span class="fl fs12 cff2da5">หนังสือรับรองบริษัท</span><span>
															<input type="file" name="filUpload[company]" id="fileUp_7" class="fl form-control inputFile" onchange="ValidateSingleInput(this);" accept="image/gif, image/jpeg , .pdf"></span>
													</div>
													<div class="col-md-3 filecompanyBox">
														<span class="fl fs12 cff2da5">บัตร ปชช. กรรมการ</span><span>
															<input type="file" name="filUpload[companycard]" id="fileUp_8" class="fl form-control inputFile" onchange="ValidateSingleInput(this);" accept="image/gif, image/jpeg , .pdf"></span>
													</div>
												</div>
												<div class="row mt15">
													<div class="col-md-3">
														<span class="fl fs12 cff2da5">เอกสารอื่นๆ 1</span><span>
															<input type="file" name="filUpload[carall1]" id="fileUp_9" class="fl form-control inputFile" onchange="ValidateSingleInput(this);" accept="image/gif, image/jpeg , .pdf"></span>
													</div>
													<div class="col-md-3">
														<span class="fl fs12 cff2da5">เอกสารอื่นๆ 2</span><span>
															<input type="file" name="filUpload[carall2]" id="fileUp_10" class="fl form-control inputFile" onchange="ValidateSingleInput(this);" accept="image/gif, image/jpeg , .pdf"></span>
													</div>
													<div class="col-md-3">
														<span class="fl fs12 cff2da5">เอกสารอื่นๆ 3</span><span>
															<input type="file" name="filUpload[carall3]" id="fileUp_11" class="fl form-control inputFile" onchange="ValidateSingleInput(this);" accept="image/gif, image/jpeg , .pdf"></span>
													</div>
													<div class="col-md-3">
														<span class="fl fs12 cff2da5">เอกสารอื่นๆ 4</span><span>
															<input type="file" name="filUpload[carall4]" id="fileUp_12" class="fl form-control inputFile" onchange="ValidateSingleInput(this);" accept="image/gif, image/jpeg , .pdf"></span>
													</div>
												</div>
												<div class="row mt15">
													<div class="row">
														<div class="col-md-3">
															<span class="fl fs12 cff2da5">หลักฐานประกอบตรวจ 1</span><span>
																<input type="file" name="filUpload[reCheck1]" id="fileUp_13" class="fl form-control inputFile" onchange="ValidateSingleInput(this);" accept="image/gif, image/jpeg , .pdf" ></span>
														</div>
														<div class="col-md-3">
															<span class="fl fs12 cff2da5">หลักฐานประกอบตรวจ 2</span><span>
																<input type="file" name="filUpload[reCheck2]" id="fileUp_14" class="fl form-control inputFile" onchange="ValidateSingleInput(this);" accept="image/gif, image/jpeg , .pdf"></span>
														</div>
														<div class="col-md-3">
															<span class="fl fs12 cff2da5">หลักฐานประกอบตรวจ 3</span><span>
																<input type="file" name="filUpload[reCheck3]" id="fileUp_15" class="fl form-control inputFile" onchange="ValidateSingleInput(this);" accept="image/gif, image/jpeg , .pdf" ></span>
														</div>
														<div class="col-md-3">
															<span class="fl fs12 cff2da5">หลักฐานประกอบตรวจ 4</span><span>
																<input type="file" name="filUpload[reCheck4]" id="fileUp_16" class="fl form-control inputFile" onchange="ValidateSingleInput(this);" accept="image/gif, image/jpeg , .pdf"></span>
														</div>
													</div>
													<p class="fs12 c00ac0a">*** ไฟล์นามสกุล PNG | JPG | PDF  เท่านั้น *** </p>
												</div>
											</div>
											<div class="col-md-6 ">
												<div class="mt20" >
														<label for="taxamount">หมายเหตุ</label>
														<textarea name="notes" id="notes" style="width: 100%; font-size: 12px; height: 100px;" ></textarea>
												</div>
												<div class="t_c clearfix mt20">
													<button type="button" class="btn btn-success btnOrders"><i class="fa fa-check-circle"></i> ส่งงาน</button>
												</div>
											</div>
										</div>
										<div class="row pt20">						
									</div>
									</form>

							</div>
						</div>

					<div>
						<div class="mt15">
							<div class="col-md-2 mb15">
								<label for="filterStatus">สถานะ</label>
								<select class="filterStatus form-control fs12" data-tableId="table1" onchange="filterTable()">
									<?php if($_SESSION["User"]['type'] == "QualityControl"){ ?>
									  <option value="0" <?php if($status == 0){ echo "selected";} ?>>รอตรวจ</option>
									  <option value="1" <?php if($status == 1){ echo "selected";} ?>>ไม่ผ่าน</option>
									<?php }else{ ?>
										<option value="0" <?php if($status == 0){ echo "selected";} ?>>รอตรวจ</option>
									  <option value="1" <?php if($status == 1){ echo "selected";} ?>>ไม่ผ่าน</option>
									  <option value="2" <?php if($status == 2){ echo "selected";} ?>>รอแจ้งงาน</option>
									  <option value="3" <?php if($status == 3){ echo "selected";} ?>>แจ้งงาน</option>    
									  <option value="4" <?php if($status == 4){ echo "selected";} ?>>เสร็จ</option>
									<?php } ?>
								</select>
							</div>
							<div class="col-md-2 mb15">
								<label for="filter">PO</label>
								<input type="text" class=" form-control fs12" id="pocodeSe" value="<?php echo $_GET["pocode"];  ?>">
							</div>
								<div class="col-md-2 mb15">
									<a href="sendorders.php" class="btn btn-danger mt20">รีเช็ต</a>
									<span class="btn btn-success mt20" onclick="filterPOcode()">ค้นหา</span>
								</div>
						</div>
						<div class="mt15">
							<table id="table" class="table table-striped p5" data-toggle="table" <?php if($_SESSION["User"]['type'] != "QualityControl"){ ?> data-detail-view="true" <?php } ?>>
								<thead>
									<tr>
										<th class="t_c"><i class="fa fa-list-ul" aria-hidden="true"></i></th>
										<th data-field="statusType" class="t_c">สถานะ</th>
										<th data-field="noti_work_code" class="t_c dn"></th>
										<th data-field="pocode" data-sortable="true" class="t_c">PO</th>
										<th data-field="start_cover_date" data-sortable="true" class="t_c">วันคุ้มครอง</th>
										<th class="t_c">ชื่อ-นามสกุล</th>
										<th class="t_c">แพจเกต</th>
										<th class="t_c">ทุนประกัน</th>
										<th class="t_c">ชำระ</th>
										<th class="t_c">ผู้ขาย</th> 
										<th class="t_c">ผู้ตรวจ</th>
									</tr>

								</thead>
								<tbody class="fs12">
									<?php 
										//if($apiMain){
											foreach ($send_orders as $key => $value) {  
												if($value["start_cover_date"] == date("Y-m-d")){
													$bgColor = "class='bgbaf4bc'";
												}else if($value["status"] == "1"){
													$bgColor = "class='bgffeee8'";
												}else if($value["urgent"] == "1"){
													$bgColor = "class='bgNEW'";
												}else{
													$bgColor = "";
												}

												if($value["enable"] == "0"){
													$enable = "<span style='color: #f44;'>ยกเลิก</span>";
												}else{
													$enable = "<span style='color: #009710;'>เปิดใช้</span>";
												}

												if($_SESSION["User"]['type'] != "Sale"){
													if($value["status"] == 0 && $value["datetime_chk"]){
														$bgColor = "class='bgfffbd8'";
													}
												}

												if($value["endorse"] == "1"){
													$endorse = "<span style='color: #f44;'>*สลักหลัง</span>";
												}else{
													$endorse ="";
												}
									?>
										<tr <?php echo $bgColor; ?> data-status="<?php echo $value["status"]; ?>" data-inser="<?php echo $value["insuere_company"]; ?>">
											<td class="wpno t_c"> 
												<?php if($_SESSION["User"]['type'] == "Sale" || $_SESSION["User"]['type'] == "CallCenter"){ ?>
													<?php if($value["status"] == 1 ){ ?>
														<a href="sendordersviews.php?pocode=<?php echo $value["po_code"];  ?>&numcode=<?php echo $value["noti_work_code"];  ?>" style="color: #003cff;">
															<i class="fa fa-cog"></i></a> | 
														<span class="fs14 btnselecttitle" style="color: #ff0000;" onclick="changEnable('<?php echo $value["noti_work_id"]?>','<?php echo $value["noti_work_code"]; ?>',0);"><i class="fa fa-trash"></i></span> 
													<?php }else{ ?>
														<a href="sendordersviews.php?pocode=<?php echo $value["po_code"];  ?>&numcode=<?php echo $value["noti_work_code"];  ?>" style="color: #003cff;"><i class="fa fa-search-plus" aria-hidden="true"></i></a>
														<?php if($value["status"] == 0 ){ ?>
															| <span class="fs14 btnselecttitle" style="color: #ff0000;" onclick="changEnable('<?php echo $value["noti_work_id"]?>','<?php echo $value["noti_work_code"]; ?>',0);"><i class="fa fa-trash"></i></span>
														<?php } ?>

													<?php } ?>

												<?php }else if($_SESSION["User"]['type'] == "QualityControl"){ ?>
													<?php if($value["status"] == 0 && $UserCode == $value["chk_code"]){ ?>
														<a href="sendorderslists.php?pocode=<?php echo $value["po_code"];  ?>&numcode=<?php echo $value["noti_work_code"];  ?>"><i class="fa fa-list-ul" aria-hidden="true"></i></a>
													<?php }else{ ?>
														<a href="sendordersviews.php?pocode=<?php echo $value["po_code"];  ?>&numcode=<?php echo $value["noti_work_code"];  ?>" style="color: #003cff;"><i class="fa fa-search-plus" aria-hidden="true"></i></a>
													<?php } ?>
													
													<?php if($UserCode == $value["personnel_code"]){ ?>
															| <a href="sendordersviews.php?pocode=<?php echo $value["po_code"];  ?>&numcode=<?php echo $value["noti_work_code"];  ?>" style="color: #003cff;"><i class="fa fa-search-plus" aria-hidden="true"></i></a>
															| <span class="fs14 btnselecttitle" style="color: #ff0000;" onclick="changEnable('<?php echo $value["noti_work_id"]?>','<?php echo $value["noti_work_code"]; ?>',0);"><i class="fa fa-trash"></i></span> 
													<?php } ?>

												<?php }else{ ?>
														<a href="sendordersviews.php?pocode=<?php echo $value["po_code"];  ?>&numcode=<?php echo $value["noti_work_code"];  ?>" style="color: #003cff;"><i class="fa fa-search-plus" aria-hidden="true"></i></a> | 
														<a href="sendorderslists.php?pocode=<?php echo $value["po_code"];  ?>&numcode=<?php echo $value["noti_work_code"];  ?>" style="color: #ac056c;"><i class="fa fa-list-ul" aria-hidden="true"></i></a>
												<?php } ?>
											</td>
											<td class="wpno t_c rowStatus" data-type="status_<?php echo $value["status"]; ?>">
												<div><?php echo setStatus($value["status"]); ?></div>
												<div style="color: #ff0000;" class="fs14 fwb" ><?php echo $endorse; ?></div>
											</td>
											<td class="t_l dn"><?php echo $value["noti_work_code"]; ?></td>
											<td class="t_l rowpo"> 
												<b ><a href="listpayment.php?po=<?php echo $value["po_code"]; ?>" target="_bank" style="color: #ff2da5;"><?php echo $value["po_code"]; ?></a></b>
												<div class="clearfix fs10"><?php echo $value["noti_work_code"]; ?></div>
												<div class="fs12"><?php echo ($value["enable"]==1) ? "<span class='c00ac0a'>ใช้งาน</span>" : "<span class='cf80000'>ยกเลิก</span>"; ?></div>
											</td>
											<td><span style="color: #ff2da5;"><?php echo date("d-m-Y",strtotime($value["start_cover_date"])); ?></span></td>
											<td class="t_l <?php echo $bgColor; ?>">
												<b><?php echo $value["cus_name"]; ?></b><?php if($value["status"] == 4) { echo "<div class='c2457ff'>".$value["policy_number"]."</div>"; } ?>
											</td>
											<td class="t_l rowInser" data-type="<?php echo $value["insuere_company"]; ?>">
												<div class="clearfix fs10 c2457ff"><?php echo $value["insuere_company"]; ?></div>
												<div class="clearfix c9c00c8"><?php echo $value["package_name"]; ?></div>
												<div class="clearfix fs10 "><?php echo $value["insurance_type"]; ?></div>
											</td>
											<td class="t_r"><?php echo number_format($value["insuere_cost"],2); ?></td>
											<td class="t_r">
												<div class="clearfix "><b>สุทธิ: </b><?php echo number_format($value["netpremium"],2); ?></div>
												<div class="clearfix "><b>รวมภาษี: </b><?php echo number_format($value["premium"],2); ?></div>
												<div class="clearfix "><b>จ่าย: </b><?php echo number_format($value["taxamount"],2); ?></div>
											</td>
											<td class="t_l wpno">
													<div class="clearfix c2457ff "><?php echo $value["personnel_name"]; ?></div>
													<div><span style="color: #ff2da5;"><?php echo date("d-m-Y H:i:m",strtotime($value["created_date"])); ?></span></div>
													<div class="clearfix fs10 "><?php echo $value["send_type"] ? "<b>การส่ง :</b> ".$value["send_type"] : ""; ?></div>
											</td>
											<!-- <td ><span style="color: #ff2da5;"><?php echo date("d-m-Y H:i:m",strtotime($value["created_date"])); ?></span></td> -->
											<td class="t_l">
												<div class="clearfix "><?php echo $value["chk_name"]." ".$value["chk_lname"]; ?></div>
												<div class="clearfix "><?php echo $value["datetime_chk"]; ?></div>
											</td>
										</tr>
									<?php }
									//} ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<?php include "include/inc_footer.php"; ?> 
<!-- <script src="//code.jquery.com/jquery-3.2.1.min.js"></script> -->
<script src="fancybox/dist/jquery.fancybox.min.js"></script>
<script type="text/javascript">
	var $table = $('#table');
	$table.on('expand-row.bs.table', function (e, index, row, $detail) {
    var noti_id = row.noti_work_code; 
  	var res = $("#desc" + index).html(); 
  	$.ajax({ 
			url: 'include/inc_action_chk.php',
			type:'POST',
			data: {action:"getImageOrNote", noti_id:noti_id},
			success:function(rs){
				// console.log(rs);
				$detail.html(rs);
			}
		});
	});

$('[data-fancybox="imgTable"]').fancybox({
  buttons : [
    'download',
     'zoom',
    'close'
  ],
  // protect : false,
  // animationEffect : "zoom-in-out",
  // transitionEffect : "fade",
  // zoomOpacity : "auto",
  // animationDuration : 500,
  // zoomType: 'innerzoom',
});

function chknum(ip,ek) {
	if (ek.code == "Comma") { 
			ip.value = ip.value.replace(/,/g, "");
			return true;
	}
	if(isNaN(ip.value)){
		alert('กรุณากรอกเป็นหมายเลขเท่านั้น');
		ip.value = "";
		return true;
	}
		return true;
	}

function filterTable(val){
	var filterStatus = $(".filterStatus").val();
	if(filterStatus){
		 window.location.href = "sendorders.php?filter="+filterStatus;
	}
}

function queryParams() {
    return {
        type: 'owner',
        sort: 'updated',
        direction: 'desc',
        per_page: 1,
        page: 1
    };
}


function filterPOcode(){
	pocodeSe = $("#pocodeSe").val();
	console.log(pocodeSe);
	if(pocodeSe){
		window.location.href = "sendorders.php?pocode="+pocodeSe;
	}else{
		alert("กรุณากรอก PO เพื่อค้นหา");
	}
}

function filterInser(){
	var filterInser = $(".filterInser").val();  
  var row = $('tbody tr'); 
  if(filterInser){ 
    row.hide();
    row.each(function(i, el) {
    	datainser = $(el).attr('data-inser');
    	if(datainser == filterInser) {
          $(el).show();
       	}
    });

  }else{
  	row.show();
  } 
}

$(".filterPO").keyup(function() {
   var searchText = $(this).val().toLowerCase();
    $.each($("#table tbody tr .rowpo"), function() {
        if($(this).text().toLowerCase().indexOf(searchText) === -1)
           $(this).parent().hide();
        else
           $(this).parent().show();                
    });
});

$(document).ready(function() {

		$("#btn_open_send_order").click(function() {
				$( "#fromSendWork" ).toggle("slow");
		});	

		$("input[name=po_type]").click(function() {
			name = $('input[name=po_type]:checked').val();
			if(name == "บริษัท"){
				$(".filecompanyBox").show();
			}else{
				$(".filecompanyBox").hide();
			}
		});


		$("#insuere_company").change(function() {
			var insuere = $("#insuere_company").val();
			frncmp = setimgCompany(insuere);
			if(frncmp == 1){
				$(".imgCarBox").show();
			}else{
				$(".imgCarBox").hide();
			}
		});

    $("#btn_send_order ").click(function() {
    	$("#tableList tbody").empty();
    	var poCode = $("#po_code_search").val();
    	var search_type = $("input[name=search_type]:checked").val();

        $.ajax({ 
					url: 'include/inc_action_ms.php',
					type:'POST',
					dataType: 'json',
					data: {action: 'searchPO', pocode:poCode, search_type:search_type},
					success:function(rs){ 
						// console.log("rs"+rs);
							var num = rs.length;
							var details = "";
							$("#tr_List").hide();
							for (var i = 0; i < num; i++){
								var d = new Date(rs[i].Coverage_Start_Date.date);
								var now = new Date();
								var day = ("0" + d.getDate()).slice(-2);
								var month = ("0" + (d.getMonth() + 1)).slice(-2);
								var today = d.getFullYear()+"-"+month+"-"+day ;
								

								if(rs[i].cus_type == "C"){
									cus_type = "ส่วนบุคคล";
								}else{
									cus_type = "บริษัท";
								}

								if(rs[i].Net_Premium){
									netpremium = rs[i].Net_Premium;
								}else{
									netpremium = rs[i].Compulsory;
								}

								txt = '<tr>';
								txt += '<td>';
								txt += '<div><input type="checkbox" class="chkpo" name="select_po['+rs[i].PO_ID+']" value="'+rs[i].PO_ID+'" > <span class="cff8000 fwb">แจ้งงาน</span></div>';
								txt += '<div><input type="checkbox" name="casePO['+rs[i].PO_ID+'][discount_cctv]" value="1" > ส่วนลดกล้อง</div>';
								txt += '<div><input type="checkbox" name="casePO['+rs[i].PO_ID+'][endorse]" value="1" > สลักหลัง</div>';
								txt += '</td>';

								txt += '<td class="t_c cff2da5 fwb">'+rs[i].PO_ID+'</td>';
								txt += '<td class="t_c c00ac0a fwb">'+today+'</td>';
								txt += '<td>'+rs[i].Title_Name+" "+rs[i].FName+" "+rs[i].LName+'</td>';

								txt += '<td>';
								txt += '<div><b>ประเภทประกัน: </b>'+cus_type+'</div>';
								txt += '<div><b>ประกันชั้น: </b>'+rs[i].Insurance_Name+'</div>';
								if(rs[i].Insurance_Package_Name){
									txt += '<div><b>แพจเกต: </b>'+rs[i].Insurance_Package_Name+'</div>';
								}
								txt += '<div><b>บริษัทประกัน: </b>'+rs[i].Insurer_Name+'</div>';
								txt += '<div><b>การส่ง: </b>'+rs[i].Delivery_Type_Desc+'</div>';
								txt += '</td>';

								txt += '<td class="t_c">'+rs[i].Capital+'</td>';
								txt += '<td>';
								txt += '<div><b>เบี้ยสุทธิ: </b>'+netpremium+'</div>';
								txt += '<div><b>เบี้ยรวมภาษี: </b>'+rs[i].Total_Premium+'</div>';
								txt += '<div><b>ส่วนลด: </b>'+rs[i].Discount+'</div>';
								txt += '<div><b>รวม: </b>'+rs[i].Premium_After_Disc+'</div>';
								txt += '</td>';

								txt += '<td>';
								if(rs[i].Premium_Desc){
									txt += '<div><b>ของกำนัล: </b>'+rs[i].Premium_Desc+'</div>';
									txt += '<div><p>ระบุ PO ที่รับของกำนัล</p><input type="text" name="casePO['+rs[i].PO_ID+'][giftvoucher_po]" value="'+rs[i].PO_ID+'"/></div>';
								}
								txt += '</td>';

								txt += '</tr>';

    						$('#tableList tbody').append(txt);
							}
								
						}
				});
    });

    $(".btnOrders").click(function() {
    	 var countCK = $('.chkpo:checked').length;
		    if(countCK > 0){
		      $(".btnOrders").hide();
    			$("#form_send_order").submit();
		    }else{
		      alert("กรุณาเลือก PO ที่ต้องการแจ้งงาน");
		    }
		});
});


function fnSendType(){
	send = $("#send_type").val();
	if(send == "พนักงานจัดส่ง"){
		$("#addressSend").show();
	}else{
		$("#addressSend").hide();
	}
}
function changEnable(id, codes, status){
	// console.log(id);console.log(codes);console.log(status);
	if(confirm("ต้องการยกเลิกการแจ้งงานนี้?")){
		$.ajax({ 
			url: 'include/inc_action_chk.php',
			type:'POST',
			data: {action: 'delPONOti', id:id, code:codes, status:status},
			success:function(rs){
				window.location.reload(true);
			}
		});
	}
}

function ValidateSingleInput(oInput) {
var _validFile = [".jpg", ".jpeg", ".pdf",".png",];    
    if (oInput.type == "file") {
        var sFileName = oInput.value;
         if (sFileName.length > 0) {
            var blnValid = false;
            for (var j = 0; j < _validFile.length; j++) {
                var sCurExtension = _validFile[j];
                if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                    blnValid = true;
                    break;
                }
            }
             
            if (!blnValid) {
                alert("กรุณาเลือกไฟล์ ที่มีนามสกุล PNG | JPG | PDF เท่านั้น" );
                oInput.value = "";
                return false;
            }
        }
    }
    return true;
}

function isImage(filename) {
    var ext = getExtension(filename);
    switch (ext.toLowerCase()) {
	    case 'jpg':
	    case 'png':
	    case 'pdf':
	      return true;
    }
    return false;
}

function btnComments(e){
	var val = e.id;
	console.log(e);
	code = val.split("ments_");
	mentID = code[1];
	mentTxt = $("#comment_"+mentID).val();
	console.log("#listMent_"+mentTxt);
	if(mentTxt != 0){
	 $.ajax({ 
			url: 'include/inc_action_chk.php',
			type:'POST',
			data: {action: 'addComments', noti_work_code:mentID, comment:mentTxt},
			success:function(rs){
				// console.log("#listMent_"+mentID);
				$("#listMent_"+mentID).empty();
				$("#comment_"+mentID).val(0);
				$("#listMent_"+mentID).append(rs);
			}
		});
	}else{
		alert("กรุณาเลือก Comment");
	}
}

</script>