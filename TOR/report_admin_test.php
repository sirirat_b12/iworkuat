<?php 
session_start();
ini_set('max_execution_time', 186);
ini_set('memory_limit', '2048M');
$UserCode = $_SESSION["User"]['UserCode'];
$userType = $_SESSION["User"]['type'];

if(!isset($UserCode)){
	echo "<script>alert('กรุณาเข้าสู่ระบบ!!!');</script>";
	echo '<META http-equiv="refresh" content="0;URL=login.php">';
	exit();
}

header('Content-Type: text/html; charset=utf-8');
include "include/inc_header.php"; 
include "include/inc_menu.php"; 
include "reports/inc_function_report.php";
// include('qrcode/qrcode.class.php');

$getMonthSelect = getMonthSelect();
$year = date("Y");
if($_GET["txtse"]){
	// $getMountListdate = getMountListdate($_GET["txtse"]);
	$getListAdmin = getListAdmin($_GET["txtse"]);
	$getDataToAdmin = getDataToAdmin($_GET["txtse"]);
}

if($_GET["date"]){
	$getDetailPOByAdmin = getDetailPOByAdmin($_GET["user"], $_GET["date"]);
}
// echo "<pre>".print_r($getDetailPOByAdmin,1)."</pre>";
?>
<div class="main">
	<div class="">
		<div class="p20">
				<div class="row">
					<div class="col-md-12">
						<div class="panel">
								<div class="panel-heading">
									<h4 class="panel-title"><b>รายงาน Admin <?php echo "ปี ".$year;?></b></h4>
								</div>
								<div class="panel-body fs14">
									<div class="row">
										<div class="col-md-4 ">
											<span class="fs18 cff2da5 mr15">เดือน</span>
											<select name="mounth" id="mounthOpt" class="form-control formInput2 dib" style="width: 80%" >
												<option value="">:: กรุณาเลือก ::</option>
												<?php foreach ($getMonthSelect as $key => $value) { ?>
													<option value="<?php echo $value["months"];?>" <?php if($_GET["txtse"] ==  $value["months"]){ echo "selected"; } ?>>
														<?php echo date("F", strtotime($year."-".$value["months"]."-01")) ?>
													</option>
												<?php } ?>
											</select>
										</div>
									</div>
									<div class="row mt20">
										<div class="col-md-12">
											<div class="row">
												<div class="col-md-2"></div><div class="col-md-10"><?php echo $value["PO_ID"]; ?></div>
											</div>
											<?php if($getDataToAdmin){?>
												<table class="table table-hover table-bordered" id="tableMoveQC">
													<thead>
														<tr class="bgbaf4bc">
															<th class="t_c">วันที่</th>
															<?php foreach ($getListAdmin as $key => $value) { ?>
																<th class="t_c">[<?php echo (getPersonProfile($value["admin_code"],"3"))?>]<br><?php echo $value["admin_code"];?></th>
															<?php } ?>
															<th class="t_c">รวม</th>
														</tr>
													</thead>
													<tbody id="table_data" class="table-list fs13">
														<?php 
															foreach ($getDataToAdmin as $key => $valDate) {
															$sumrow = 0; 
															$date = date("d F Y",strtotime($key));
															?>
															<tr>
																<td class="text-center"><?php echo $date;?></td>
																<?php 
																	foreach ($getListAdmin as $keyAdmin => $valAdmin) { 
																		$countNum = $getDataToAdmin[$key][$valAdmin["admin_code"]];
																		$sumrow = $sumrow+$countNum;
																		if($countNum){
																?>
																	<td class="t_c">
																		<span class="cursorPoin cff2da5" onclick="getDetailPO('<?php echo $valAdmin["admin_code"];?>','<?php echo $key?>')"><?php echo $countNum; ?></span>
																	</td>
																<?php }else{ 
																				echo '<td class="t_c">-</td>';
																			}
																	} ?>
																<td class="text-center bgNEW fwb"><?php echo $sumrow;?></td>
															</tr>
														<?php }  ?>
														<?php $sumrowTotal =0;
																foreach ($getDataToAdmin as $key => $valDate) {
																	foreach ($getListAdmin as $keyAdmin => $valAdmin) { 
																		$countNum = $getDataToAdmin[$key][$valAdmin["admin_code"]];
																		$sumrowAll[$valAdmin["admin_code"]] = $sumrowAll[$valAdmin["admin_code"]] + $countNum;
																		$sumrowTotal = $sumrowTotal + $countNum;
																	} 
																} 
														?>
														<tr class="bgNEW fwb">
															<td class="t_c">รวม</td>
																<?php 
																	foreach ($getListAdmin as $keyAdmin => $valAdmin) { 
																?>
																	<td class="t_c"><?php echo number_format($sumrowAll[$valAdmin["admin_code"]]) ;?></td>
																<?php } ?>
															<td class="t_c"><?php echo number_format($sumrowTotal);?></td>
														</tr>
													</tbody>
												</table>
											<?php } ?>
										</div>
									</div>
									<?php if($_GET["date"]){?>
										<div class="row mt20">
											<table class="table table-hover" >
														<thead>
															<tr>
																<th class="t_c">#</th>
																<th class="t_c">PO</th>
																<th class="t_c" style="width: 10%">สถานะ</th>
																<th class="t_c" style="width: 20%">ลูกค้า</th>
																<th class="t_c" style="width: 10%">พนักงาน</th>
																<th class="t_c" style="width: 10%">ผู้ตรวจ</th>
																<th class="t_c" style="width: 10%">Admin</th>
																<th class="t_c" style="width: 25%">สรุปขั้นตอนตรวจ</th>
															</tr>
														</thead>
														<tbody id="tableListDetail" class="table-list fs13">
															<?php $i=1; foreach ($getDetailPOByAdmin as $key => $value) { 
																$status = ($value["enable"]==1) ? "<span class='fwb c00ac0a'>ใช้งาน</span>" : "<span class='fwb cf80000'>ยกเลิก</span>";;
															?>
																<tr>
																	<td><?php echo $i++; ?></td>
																	<td class="t_l">
																		<div class="fwb cff2da5">
																			<a target="_Blank" class="cff2da5" href="sendordersviews.php?pocode=<?php echo $value["po_code"] ?>&numcode=<?php echo $value["noti_work_code"] ?>"><?php echo $value["po_code"] ?> | <?php echo $value["noti_work_code"] ?></a>
																		</div>
																		<div><?php echo $value["insurance_type"] ?> | <?php echo $value["insuere_company"] ?></div>
																		<div class="c2457ff"><?php echo $value["package_name"] ?></div>
																	</td>
																	<td class="t_c"><?php echo setStatusPO($value["status"])." | ".$status; ?></td>
																	<td class="t_l"><?php echo $value["cus_name"] ?></td>
																	<td class="t_t">
																		<div><?php echo $value["personnel_name"] ?></div>
																		<div><?php echo date("d-m-Y H:i:m",strtotime($value["created_date"])) ?></div>
																	</td>
																	<td class="t_t">
																		<div><?php echo $value["chk_code"] ?></div>
																		<div class="clearfix fs10 c9c00c8">เริ่ม: <?php echo $value["datetime_chk_open"]; ?></div>
																		<div class="clearfix fs10 cff2da5">เสร็จ: <?php echo $value["datetime_chk"]; ?></div>
																		<div class="clearfix fs10 c2457ff fwb">
																			สรุป: <?php echo $value["datetime_chk_open"] ? diff2time($value["datetime_chk"],$value["datetime_chk_open"]) : "-"; ?>
																		</div>
																	</td>
																	<td class="t_t">
																		<div><?php echo $value["admin_code"] ?></div>
																		<div class="clearfix fs10 c9c00c8">เริ่ม: <?php echo $value["datetime_send_open"]; ?></div>
																		<div class="clearfix fs10 cff2da5">เสร็จ: <?php echo $value["datetime_send"]; ?></div>
																		<div class="clearfix fs10 c2457ff fwb">
																			สรุป: <?php echo $value["datetime_send_open"] ? diff2time($value["datetime_send"],$value["datetime_send_open"]) : "-"; ?>
																		</div>
																	</td>
																	<td class="t_t">
																		<?php foreach ($value["steplists"] as $key => $valStep) { ?>
																			<div class=" c2457ff"> >> <?php echo $valStep["comment"] ?></div>
																			<div class="fs10"><b><?php echo $valStep["datetime"] ?></b> | <?php echo $valStep["comment_byname"] ?></div>
																		<?php } ?>
																	</td>
																</tr>
															<?php } ?>
														</tbody>
													</table>
										</div>
									<?php } ?>
								</div>
						</div>
					</div>
				</div>
		</div>
	</div>
</div>
<?php include "include/inc_footer.php"; ?> 
<script src="fancybox/dist/jquery.fancybox.min.js"></script>
<script type="text/javascript">

	$("#mounthOpt").on('change', function() {
		if(this.value){
			window.location.href = "report_admin.php?txtse="+this.value;
		}

  });

<?php if($_GET["txtse"]){?>
function getDetailPO(user, date){
	txtse = <?php echo $_GET["txtse"]; ?>;
	if(txtse){
		window.location.href = "report_admin.php?txtse="+txtse+"&user="+user+"&date="+date;
	}
}
<?php } ?>
  

function filterPOcode(){
	txtse = $("#txtse").val();
	if(txtse){
		window.location.href = "counters.php?txtse="+txtse;
	}else{
		alert("กรุณากรอก ข้อมูล เพื่อค้นหา");
	}
}

</script>