<?php 
session_start();
ini_set('max_execution_time', 186);
ini_set('memory_limit', '2048M');
$UserCode = $_SESSION["User"]['UserCode'];
$userType = $_SESSION["User"]['type'];

if(!isset($UserCode)){
	echo "<script>alert('กรุณาเข้าสู่ระบบ!!!');</script>";
	echo '<META http-equiv="refresh" content="0;URL=login.php">';
	exit();
}

header('Content-Type: text/html; charset=utf-8');
include "include/inc_header.php"; 
include "include/inc_menu.php"; 
include "reports/inc_function_report.php";
// include('qrcode/qrcode.class.php');

$dateNow = date("Y-m-d");

$getDepartment = getDepartment();
if($_GET["depart"]){
	$getReportFollow7Day = getReportFollow7Day($_GET["depart"]);
	// echo "<pre>".print_r($getReportFollow7Day,1)."</pre>";
}
?>
<div class="main">
	<div class="">
		<div class="p20">
				<div class="row">
					<div class="col-md-12">
						<div class="panel">
								<div class="panel-heading"><h4 class="panel-title"><b>รายงานการติดตาม</b></h4></div>
								<div class="panel-body fs14">
									<div class="row">
										<div class="col-md-4 ">
											<span class="fs18 cff2da5 mr15">ทีม</span>
											<select name="Department" id="Department" class="form-control formInput2 dib" style="width: 80%" >
												<option value="">:: กรุณาเลือก ::</option>
												<?php foreach ($getDepartment as $key => $value) {?>
													<option value="<?php echo $value["Department_ID"];?>" <?php if($_GET["depart"] ==  $value["Department_ID"]){ echo "selected"; } ?>>
														<?php echo $value["Department_Name"]." | ".$value["Remark"];?>
													</option>
												<?php } ?>
											</select>
										</div>
									</div>
									<div class="row mt20">
										<div class="col-md-12">
											<div class="row">
												<div class="col-md-2"></div><div class="col-md-10"><?php echo $value["PO_ID"]; ?></div>
											</div>
											<?php if($getReportFollow7Day){ 
												?>
												<table class="table table-hover table-bordered" id="indextable" >
													<thead>
														<tr class="bgbaf4bc">
															<th class="t_c">รหัสพนักงาน</th>
															<th class="t_c">ชื่อ-นามสกุล</th>
															<th class="t_c">รวมค้าง</th>
															<th class="t_c">
																<?php echo date("j M", strtotime($dateNow . "-7 days")) ?>
															</th>
															<th class="t_c">
																<?php echo date("j M", strtotime($dateNow . "-6 days")) ?>
															</th>
															<th class="t_c">
																<?php echo date("j M", strtotime($dateNow . "-5 days")) ?>
															</th>
															<th class="t_c">
																<?php echo date("j M", strtotime($dateNow . "-4 days")) ?>
															</th>
															<th class="t_c">
																<?php echo date("j M", strtotime($dateNow . "-3 days")) ?>
															</th>
															<th class="t_c">
																<?php echo date("j M", strtotime($dateNow . "-2 days")) ?>
															</th>
															<th class="t_c">
																<?php echo date("j M", strtotime($dateNow . "-1 days")) ?>
															</th>	
															<th class="t_c cff2da5">
																<?php echo date("j M", strtotime($dateNow)) ?>
															</th>
															<th class="t_c">
																<?php echo date("j M", strtotime($dateNow . "+1 days")) ?>
															</th>
															<th class="t_c">
																<?php echo date("j M", strtotime($dateNow . "+2 days")) ?>
															</th>
															<th class="t_c">
																<?php echo date("j M", strtotime($dateNow . "+3 days")) ?>
															</th>
															<th class="t_c">
																<?php echo date("j M", strtotime($dateNow . "+4 days")) ?>
															</th>
															<th class="t_c">
																<?php echo date("j M", strtotime($dateNow . "+5 days")) ?>
															</th>
															<th class="t_c">
																<?php echo date("j M", strtotime($dateNow . "+6 days")) ?>
															</th>
															<th class="t_c">
																<?php echo date("j M", strtotime($dateNow . "+7 days")) ?>
															</th>

														</tr>
													</thead>
													<tbody >
														<?php 
															$sumAll = 0;
															foreach ($getReportFollow7Day as $key => $value) {
																$totalAfterDay += $value["SumAfterDay"];
																$totalDay1 += $value["Day1"];
																$totalDay2 += $value["Day2"];
																$totalDay3 += $value["Day3"];
																$totalDay4 += $value["Day4"];
																$totalDay5 += $value["Day5"];
																$totalDay6 += $value["Day6"];
																$totalDay7 += $value["Day7"];
																$totalDay8 += $value["Day8"];
																$totalDay9 += $value["Day9"];
																$totalDay10 += $value["Day10"];
																$totalDay11 += $value["Day11"];
																$totalDay12 += $value["Day12"];
																$totalDay13 += $value["Day13"];
																$totalDay14 += $value["Day14"];
																$totalDay15 += $value["Day15"];
														?>
															<tr>
																<td class="t_c"><?php echo $value["User_ID"]; ?></td>
																<td class="t_l"><?php echo $value["User_FName"]." ".$value["User_LName"]; ?></td>
																<td class="t_c cf40053 fwb">
																	<div class='cursorPoin' onclick="btnSearch('<?php echo $value["User_ID"]; ?>')"><?php echo $value["SumAfterDay"]; ?></div>
																</td>
																<td class="t_c cf40053"><?php echo $value["Day1"]; ?></td>
																<td class="t_c cf40053"><?php echo $value["Day2"]; ?></td>
																<td class="t_c cf40053"><?php echo $value["Day3"]; ?></td>
																<td class="t_c cf40053"><?php echo $value["Day4"]; ?></td>
																<td class="t_c cf40053"><?php echo $value["Day5"]; ?></td>
																<td class="t_c cf40053"><?php echo $value["Day6"]; ?></td>
																<td class="t_c cf40053"><?php echo $value["Day7"]; ?></td>
																<td class="t_c c2457ff fwb"><?php echo $value["Day8"]; ?></td>
																<td class="t_c c9c00c8"><?php echo $value["Day9"]; ?></td>
																<td class="t_c c9c00c8"><?php echo $value["Day10"]; ?></td>
																<td class="t_c c9c00c8"><?php echo $value["Day11"]; ?></td>
																<td class="t_c c9c00c8"><?php echo $value["Day12"]; ?></td>
																<td class="t_c c9c00c8"><?php echo $value["Day13"]; ?></td>
																<td class="t_c c9c00c8"><?php echo $value["Day14"]; ?></td>
																<td class="t_c c9c00c8"><?php echo $value["Day15"]; ?></td>
															</tr>
															<?php } ?>
													</tbody>
													<tfoot>
														<tr class="fwb fs16 bgfffbd8">
															<td class="t_c" colspan="2">รวม</td>
															<td class="t_c"><?php echo $totalAfterDay; ?></td>
															<td class="t_c"><?php echo $totalDay1; ?></td>
															<td class="t_c"><?php echo $totalDay2; ?></td>
															<td class="t_c"><?php echo $totalDay3; ?></td>
															<td class="t_c"><?php echo $totalDay4; ?></td>
															<td class="t_c"><?php echo $totalDay5; ?></td>
															<td class="t_c"><?php echo $totalDay6; ?></td>
															<td class="t_c"><?php echo $totalDay7; ?></td>
															<td class="t_c"><?php echo $totalDay8; ?></td>
															<td class="t_c"><?php echo $totalDay9; ?></td>
															<td class="t_c"><?php echo $totalDay10; ?></td>
															<td class="t_c"><?php echo $totalDay11; ?></td>
															<td class="t_c"><?php echo $totalDay12; ?></td>
															<td class="t_c"><?php echo $totalDay13; ?></td>
															<td class="t_c"><?php echo $totalDay14; ?></td>
															<td class="t_c"><?php echo $totalDay15; ?></td>
														</tr>
													</tfoot>
												</table>
											<?php } ?>
										</div>
										<div class="col-md-12">
											<table class="table table-hover table-bordered" id="indextable" >
												<thead>
													<tr class="bgbaf4bc">
														<th class="t_c">CP</th>
														<th class="t_c">วันที่ติดตาม</th>
														<th class="t_c">รายละเอียด</th>
														<th class="t_c">นัดหมายครั้งถัดไป</th>
														<th class="t_c">นัดหมายโดย</th>
												 	</th>
												</thead>
												<tbody id="tbOutstand">
													
												</tbody>
											</table>
										</div>
									</div>
								</div>
						</div>
					</div>
				</div>
		</div>
	</div>
</div>
<?php include "include/inc_footer.php"; ?> 
<?php include "include/inc_function_tablesort.php"; ?>
<script type="text/javascript">


  $("#Department").on('change', function() {
  	txtse = $("#txtse").val();
		if(this.value){
			window.location.href = "report_follow.php?depart="+this.value;
		}

  });

  function btnSearch(userid) {
		$("#tbOutstand").empty();
		$.ajax({ 
			url: 'reports/inc_action_report.php',
			type:'POST',
			// dataType: 'json',
			data: {action:"getFollowUpOutstand", userid:userid},
			success:function(rs){
				// console.log(rs);
				$("#tbOutstand").append(rs);
			}
		});
	}


</script>



