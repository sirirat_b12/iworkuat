<?php 
ini_set("memory_limit","1200M");
session_start();
$usercode = $_SESSION["User"]['UserCode'];

header('Content-Type: text/html; charset=utf-8');
include "inc_config.php";
// include "include/inc_function_chk.php"; 
include "counters/inc_function_counters.php";
include("mpdf/mpdf.php");

function delMyPicBarcode(){
  $files = glob('picBarcode/*'); // get all file names
  if($files){
    foreach($files as $file){ // iterate files
      if(is_file($file))
        // echo $file."<br>";
        unlink($file); // delete file
    }
  }
}

//delMyPicBarcode();

function genBarcodePDF($po,$MyBarCode){
	include('barcodeClsass/BCGFont.php');
	include('barcodeClsass/BCGColor.php');
	include('barcodeClsass/BCGDrawing.php'); 
	include('barcodeClsass/BCGcode128.barcode.php');
	$font = new BCGFont('barcodeClsass/font/Arial.ttf', 10);
	$colorFront = new BCGColor(0, 0, 0);
	$colorBack = new BCGColor(255, 255, 255);
	$color_white = new BCGColor(255, 255, 255); 

	$barcode = $MyBarCode;
	$code = new BCGcode128();
	$code->setScale(1);
	$code->setThickness(30);
	$code->setForegroundColor($colorFront);
	$code->setBackgroundColor($colorBack);
	$code->setFont(0);
	$code->setStart(1);
	$code->setTilde(true);
	$code->parse($barcode);

	$purl = "picBarcode/".$po.".png";
	$drawing = new BCGDrawing($purl, $color_white);
	$drawing->setBarcode($code);
	$drawing->draw();
	$drawing->finish(BCGDrawing::IMG_FORMAT_PNG);
}

$paycode =  base64_decode($_GET["paycode"]);
$getListCounterAll = getListCounterAll("", $paycode, "pdf");

if(!$getListCounterAll){
	echo "<script>alert('ไม่พบข้อมูลกรุณาตรวจสอบ');</script>";
	echo '<META http-equiv="refresh" content="0;URL=https://www.asiadirect.co.th/store/">';
	exit();
}


$ref2 = $getListCounterAll[0]["reference_2"];
$nameBarcode = $paycode."_".date("ymdHsis");
$genBarcode = "|024554000009700".chr(13)."".$paycode."".chr(13)."".$ref2."".chr(13)."0";
$MyBarCode = genBarcodePDF($nameBarcode, $genBarcode);
$barcode = "|024554000009700 ".$paycode." ".$ref2." 0";
// echo "<pre>".print_r($getListCounterAll,1)."</pre>";



/*
$mpdf = new mPDF('UTF-8','A4','','',5,5,5,5,10,10);
$mpdf->autoScriptToLang = true;



$order_id_paycode = "99920181001171050";
$order_id_paycode = "20181001171050";
$paycode =  base64_decode($order_id_paycode);

$ref2 = $order_id_paycode;
$nameBarcode = $paycode."_".date("ymdHsis");



$genBarcode = "|024554000009700".chr(13)."".$paycode."".chr(13)."".$ref2."".chr(13)."0";
$MyBarCode = genBarcodePDF($nameBarcode, $genBarcode);
$barcode = "|024554000009700 ".$paycode." ".$ref2." 0";
*/


 //echo "<pre>".print_r($getListCounterAll,1)."</pre>"; exit(); 









$mpdf = new mPDF('UTF-8','A4','','',5,5,5,5,10,10);
$mpdf->autoScriptToLang = true;

$html = '
<style>
	.container{
		font-family: thsaraban;
	    font-size: 16px;
	    line-height: 18px;
	    color:#000;
	}
	table{
		font-family: thsaraban;
	  font-size: 16px;
	  line-height: 15px;
	  color: #000;
	}
	p{
	    text-align: justify;
	}
	h1{
	    text-align: center;
	}
	.tableCar th{
		padding: 5px;
    border-bottom: 1px solid #000;
    border-top: 1px solid #000;
    border-right:1px solid #000;
    text-align:center;
  }
	.tableCar td {
		line-height: 15px;
		border-bottom: 1px solid #000;
		border-right:1px solid #000;
		padding: 5px;
	}
	.tableCar th:last-child, .tableCar td:last-child{ border-right:none;}
	.{ font-size:10px;}
	.fs12{ font-size:12px;}
	.fs14{ font-size:14px;}
	.fs16{ font-size:16px;}
	.fs18{ font-size:18px;}
	.fs20{ font-size:20px;}
	.fs30{ font-size:30px;}
	.fwb{font-weight: bold;}
	.t_c{text-align: center;}
	.t_l{text-align: left;}
	.t_r{text-align: right;}
	.t_c{text-align: center;}
	.t_l{text-align: left;}
	.t_r{text-align: right;}
	.mt5{margin-top:5px;}
	.mt10{margin-top:10px;}
	.mt15{margin-top:15px;}
	.mt20{margin-top:20px;}
	.mt30{margin-top:30px;}
	.mt40{margin-top:40px;}
	.mt50{margin-top:50px;}
	.mb5{margin-bottom:5px;}
	.mb10{margin-bottom:10px;}
	.mb15{margin-bottom:15px;}
	.ml5{margin-left:5px;}
	.ml10{margin-left:10px;}
	.ml15{margin-left:15px;}
	.ml20{margin-left:20px;}
	.ml52{margin-left:52px;}
	.m0{margin:0px}
	p{margin:0}
	.fl{float: left; }
	.fr{float: right; }
	.p5{padding:5px}
	.p10{padding:10px}
	.pt5{padding-top:5px}
	.pb10{padding-bottom:10px;}
	.bgea{background-color: #eaeaea;}
	.bor_1{border:1px solid #000;}
	.bor_t{border-top:1px solid #000;}
	.bor_l{border-left:1px solid #000;}
	.bor_r{border-right:1px solid #000;}
	.bor_b{border-bottom:1px solid #000;}
	.boxCoverage{
		line-height: 18px;
		float: left; 
		text-align:center; 
		width: 33.20%;
	}
</style>';
$html .= '
		<div class="container fs16" style="margin-top: 50px;">
			<p class="t_r">'.date("d/m/Y H:i:s").'</p>
			<div class="" style="line-height:18px;">
					<div style="padding: 15px;font-size: 16px;text-align: center;">
            <div style="text-align: center; padding-top: 10px;"><img src="img/logo_110.png" alt="" width="70px;"></div>
	            <div class="mt10 t_c">
	                <b style="font-size:20px">บริษัท เอเชียไดเร็ค อินชัวร์รันส์ โบรคเกอร์ จำกัด</b>
	                <br><b>เลขประจำตัวผู้เสียภาษี 0245540000097</b>
	                <br> เลขที่ 626 อาคารบีบีดี (พระราม 4) ชั้น 11 ถนนพระรามที่ 4 แขวงมหาพฤฒาราม เขตบางรัก กรุงเทพฯ 10500<br> เปิดให้บริการ วันจันทร์-วันศุกร์ เวลา 8.30-17.00 น. โทร. 02-089-2000
	            </div>
           </div>
          </div>
				<div class="fs16" style="border-top: 2px solid #f7941e;">
					<div class="fl mt15">
						<div class="p5 mt5">
							<div class="fl" style="float: left; width: 70%;">
								<p class="fs20"><b>ผู้ให้บริการ / SERVICE : Counter Service (ร้าน 7-11)</b></p>
								<p class="fs30 mt10 mb10"><b>รหัสชำระเงิน / Pay Code : '.$paycode.'</b></p>
							</div>
							<div class="fr" style="float: right; width: 25%;">
								<img src="img/cs.jpg" alt="" width="120px;" style="text-align:right">
							</div>
						</div>
					</div>
				</div>
				<div class="bor_1">
					<table class="tableCar fs16" style="width:100%;" border="0" cellspacing="0">
						<thead>
							<tr >
								<th>เลขที่ใบสั่งซื้อ</th>
								<th>ชื่อสินค้า</th>
								<th>งวดที่</th>
								<th>วันที่ครบชำระ</th>
								<th>ผู้ชำระ</th>
								<th>สถานะ</th>
								<th style="border-right:none;">จำนวนเงิน</th>
							</tr>
						</thead>
						<tbody>';
							foreach ($getListCounterAll as $key => $value) {
								$total += $value["amount"];
								$getPORow = getPORow($value["po_id"]);
								if($getPORow == 'ERQ' && $value["status_pay"] == 0){
									$status_pay = "ยกเลิก";
								}else{
									$status_pay = ($value["status_pay"] == 0) ? "ตั้งหนี้" : "จ่ายแล้ว" ;
								}
								//$insurType = ($value["insur_type"] == "insur") ? "ประกันภัย" : "พรบ." ;
								$insurType = "สินค้าพิเศษเอเชียไดเร็ค" ;
								$html .= '
										<tr>
											<td class="t_c">'.$value["po_id"].'</td>
											<td class="t_c">'.$insurType.'</td>
											<td class="t_c">'.$value["installment_num"].'</td>
											<td class="t_c">'.$value["installment_due_date"]->format("d/m/Y").'</td>
											<td >'.$value["customer_name"].'</td>
											<td class="t_c">'.$status_pay.'</td>
											<td style="border-right:none;" class="t_r"><b>'.number_format($value["amount"],2).'</b></td>
										</tr>';
							}
					$html .= '
								<tr>
									<td class="t_c" colspan="6"><b>รวม</b></td>
									<td class="t_r" style="border-right:none;"><b>'.number_format($total,2).'</b></td>
								</tr>
						</tbody>
					</table>
				</div>
				<div class="t_c mt30 fs20 fwb"></div>
				<div>
					<div class="mt20 mb20 t_c">
						<img src="picBarcode/'.$nameBarcode.'.png" alt="" width="300x" height="30px">
						<p class="t_c">'.$barcode.'</p>
					</div>
				</div>
				<div class="t_c mt30 fs20 fwb"></div>
				<div class="t_c mt5 fs16 fwb">!!! ใบชำระนี้สามารถใช้ได้กับ เคาน์เตอร์เซอร์วิส (ร้าน 7-11) เท่านั้น !!!</div>
				<div style="text-align: center;font-size: 14px;border-top: 2px solid #f7941e;padding: 10px; margin-top:20px;">
            02-625-1000 | info@asiadirect.co.th | <a href="https://www.asiadirect.co.th">www.asiadirect.co.th</a>
            <br>Copyright © 2017 Asia Direct Insurance Broker
        </div>
			</div>
		</div>
	';

// echo $html;

$mpdf->WriteHTML($html);
$mpdf->Output("ADBCS-".$paycode.".pdf","I");
exit;

?>