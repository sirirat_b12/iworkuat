<?php 
ini_set("memory_limit","1200M");
session_start();
if(!isset($_SESSION["User"]['UserCode'])){
	echo "<script>alert('กรุณาเข้าสู่ระบบ!!!');</script>";
	echo '<META http-equiv="refresh" content="0;URL=login.php">';
	exit();
}
header('Content-Type: text/html; charset=utf-8');
// include "include/inc_header.php"; 
include "inc_config.php";
include "include/inc_function.php";
include "include/inc_function_offer.php"; 

$cpid = $_GET["cpid"];
$getcpAll = getRowCPAllBycode($cpid);
$getCompareOption = getCompareOption($cpid);
$getCoverageItem = getCoverageItemList($cpid);
// echo "<pre>".print_r($getCompareOption,1)."</pre>";
include("mpdf/mpdf.php");

$mpdf = new mPDF('UTF-8','A4','','',5,5,5,5,10,10);
$mpdf->autoScriptToLang = true;

$html = 
'<style>
.container{
	font-family: thsaraban;
    font-size: 16px;
    line-height: 16px;
}
table{
	font-family: thsaraban;
  font-size: 16px;
  line-height: 15px;
}
p{
    text-align: justify;
}
h1{
    text-align: center;
}
.fs10{ font-size:10px;}
.fs12{ font-size:12px;}
.fs14{ font-size:14px;}
.fs16{ font-size:16px;}
.fs18{ font-size:18px;}
.fs20{ font-size:20px;}
.fwb{font-weight: bold;}
.t_c{text-align: center;}
.t_l{text-align: left;}
.t_r{text-align: right;}
.m0{margin:0px}
p{margin:0}
.bgea{background-color: #eaeaea;}
</style>
		<div class="container">
			<div class="t_c">
				<p class="t_r fs16">'.date("d-m-Y H:i:s").'</p>
				<img src="img/logo2.jpg" alt="" style="width:50px">
				<div class="fs16 fwb" style="margin-top:5px">บริษัท เอเชียไดเร็ค อินชัวร์รันส์ โบรคเกอร์ จำกัด</div>
				<div class="fs14">เลขที่ 626 อาคารบีบีดี ชั้น 11 ซอยจินดาถวิล ถนนพระราม 4 แขวงมหาพฤฒาราม เขตบางรัก กรุงเทพฯ 10500 โทร.02-089-2000</div>
			</div>
			<p class="fs16" style="margin-top:5px"><b>เรื่อง/Subject</b> การเสนอเบี้ยประกันภัยรถยนต์/Proposal of motor insurance premium</p>
			<div >
				<div style="clear:both;margin-top:5px">
          <div style="float: left; width: 45%; margin-bottom: 0pt; ">
          	<p><b >เรียน/To </b><b>คุณ</b> '.$getcpAll["Customer_FName"].' '.$getcpAll["Customer_LName"].' </p>
          	<p><b >เลขอ้างอิง</b> '.$getcpAll["CP_ID"].'</p>
						<p><b>โทรศัพท์/Phone.</b> '.$getcpAll["Mobile_No"].' </p>
					</div>
          <div style="float: right; width: 45%; margin-bottom: 0pt; ">
          	<p><b >ผู้ขาย</b> '.$getcpAll["User_FName"].' '.$getcpAll["User_LName"].'</p>
						<p><b>โทรศัพท์/Phone.</b> '.$getcpAll["User_Phone"].'</p>
						<p><b>E-Mail:</b> '.$getcpAll["User_Email"].' </p>
					</div>
        </div>  
            <div style="clear:both; padding-top:5px ; font-size:16px"> 
                <b>รายละเอียดรถ</b>
                <b style="color:#000;">ยี่ห้อ</b> '.$getcpAll["Make_Name_TH"].' <b>รุ่นรถยนต์</b> '.$getcpAll["Model_Desc"].' 
                <b>ขนาดเครื่องยนต์</b> '.$getcpAll["Seat_CC_Weight"].' <b>ปีจดทะเบียน</b> '.$getcpAll["Sub_Model"].'
            </div>
            <div style="margin-top:2px; font-size:16px">
            ตามที่ท่านได้สอบถามเบี้ยประกันภัยรถยนต์ บริษัทฯ มีความยินดีที่จะเสนอรายละเอียดความคุ้มครองและเบี้ยประกันภัยดังนี้<br>
				 </div>';
		if($getCompareOption){
			$html .= "
        		<table style='width:100%;margin-top: 5px;' border='1' cellspacing='0'>
                	<thead style='background-color: #55b7de;'>
						<tr>
							<th style='min-width:45%;padding: 2px;' width=30%>เงื่อนไขความคุ้มครอง</th> ";
					foreach ($getCompareOption as $key => $data2) {
						$Current_Or_Proposal = ($data2["option"]["Current_Or_Proposal"] == "C") ? "ความคุ้มครองเดิม":"ข้อเสนอใหม่";
						$html .= "<th >".($Current_Or_Proposal)."</th>";
					}
					$html .= "
						</tr>
					</thead>
					<tbody>
						<tr>
							<td style='text-align:right;padding: 3px;'><b>บริษัทประกัน</b></td>";
							foreach ($getCompareOption as $key => $data2) {
								$html .= "<td style='padding: 3px;text-align: center;'>".$data2["option"]["Insurer_Initials"]."</td>";
							}
					$html .= "
						</tr>
						<tr>
							<td style='text-align:right'><b>ประเภทประกัน</b></td>";
							foreach ($getCompareOption as $key => $data2) {
								$html .= "<td style='padding: 3px;text-align: center;'> ".$data2["option"]["Insurance_Name"]."</td>";
							}
					$html .= "
						</tr>
						<tr>
							<td style='text-align:right'><b>แพ็คเกจ</b></td>";
							foreach ($getCompareOption as $key => $data2) {
								$html .= "<td style='padding: 3px;text-align: center; font-size:14px'>".$data2["option"]["Insurance_Package_Name"]."</td>";
							}
					$html .= "
						</tr>
						<tr>
							<td style='text-align:right'><b>ประเภทการซ่อม</b></td>";
							foreach ($getCompareOption as $key => $data2) { 
								$repair = ($data2["option"]["Repair_Type"] == "C") ? "ซ่อมห้าง":"ซ่อมอู่";
								$html .= "<td style='padding: 3px;text-align: center;'>".$repair."</td>";
							}
					$html .= "
						</tr>
						<tr>
							<td style='text-align:center; padding: 3px;'><b>ความรับผิดต่อบุคคลภายนอก</b></td>
							<td colspan='".count($getCompareOption)."'></td>
						</tr>
						<tr>
							<td>1) ความเสียหายต่อชีวิต ร่างกาย หรืออนามัย (บาท/คน)</td>";
							foreach ($getCompareOption as $key => $data2) { 
								$html .= "<td style='text-align:right; padding: 3px;'>".number_format($getCompareOption[$key]["item"]["502"],2)."</td>";
							}
					$html .= "
						</tr>
						<tr>
							<td>ความเสียหายต่อชีวิต ร่างกาย หรืออนามัย (บาท/ครั้ง)</td>";
							foreach ($getCompareOption as $key => $data2) { 
								$html .= "<td style='text-align:right; padding: 3px;'>".number_format($getCompareOption[$key]["item"]["503"],2)."</td>";
							}
					$html .= "
						</tr>
						<tr>
							<td>2) ความเสียหายต่อทรัพย์สิน (บาท/ครั้ง)</td>";
							foreach ($getCompareOption as $key => $data2) { 
								$html .= "<td style='text-align:right; padding: 3px;'>".number_format($getCompareOption[$key]["item"]["504"],2)."</td>";
							}
					$html .= "
						</tr>
						<tr>
							<td>2.1) ความเสียหายส่วนแรก (บาท/ครั้ง)</td>";
							foreach ($getCompareOption as $key => $data2) { 
								$html .= "<td style='text-align:right; padding: 3px;'>".number_format($getCompareOption[$key]["item"]["505"],2)."</td>";
							}
					$html .= "
						</tr>
						<tr>
							<td style='text-align:center; padding: 3px;'><b>รถยนต์เสียหาย สูญหาย ไฟไหม้</b></td>
							<td colspan='".count($getCompareOption)."'></td>
						</tr>";
					$html .= "
						</tr>
						<tr>
							<td>1) ความเสียหายต่อรถยนต์ (บาท/ครั้ง)</td>";
							foreach ($getCompareOption as $key => $data2) { 
								$html .= "<td style='text-align:right; padding: 3px;'>".number_format($getCompareOption[$key]["item"]["536"],2)."</td>";
							}
					$html .= "
						</tr>
						<tr>
							<td>2) ความเสียหายส่วนแรก (กรณีฝ่ายผิด) (บาท/ครั้ง)</td>";
							foreach ($getCompareOption as $key => $data2) { 
								$html .= "<td style='text-align:right; padding: 3px;'>".number_format($getCompareOption[$key]["item"]["505"],2)."</td>";
							}
					$html .= "
						</tr>
						<tr>
							<td>3) รถยนต์สูญหาย/ไฟไหม้ (บาท)</td>";
							foreach ($getCompareOption as $key => $data2) { 
								$html .= "<td style='text-align:right; padding: 3px;'>".number_format($getCompareOption[$key]["item"]["507"],2)."</td>";
							}
					$html .= "
						</tr>
						<tr>
							<td style='text-align:center; padding: 3px;'><b>ความคุ้มครองตามเอกสารแนบท้าย</b></td>
							<td colspan='".count($getCompareOption)."'></td>
						</tr>";
					$html .= "
						</tr>
						<tr>
							<td>
								1) อุบัติเหตุส่วนบุคคล<br>
								ก) ผู้ขับขี่ (บาท/คน)<br>
								ข) ผู้โดยสาร (คน) (บาท/คน)
							</td>";
							foreach ($getCompareOption as $key => $data2) { 
								foreach ($getCompareOption[$key]["item"] as $key2 => $value) {
									$person = getRowCoverageItem($key2);
									if($key2 >= 512 && $key2 <= 519 || $key2 == 539 || $key2 == 559 ){
										$html .= "<td style='text-align:right; padding: 3px;'><br>
										(1) ".number_format($value,2)."<br>
										(".($person).") ".number_format($value,2)."<br>
										</td>";
									}	
								}	
							}
					$html .= "
						</tr>
						<tr>
							<td>2) ค่ารักษาพยาบาล (คน) (บาท/คน)</td>";
							foreach ($getCompareOption as $key => $data2) { 
								foreach ($getCompareOption[$key]["item"] as $key2 => $value) {
									if($key2 >= 520 && $key2 <= 527 || $key2 == 540 || $key2 == 560){
										$person = getRowCoverageItem($key2);
										$html .= "<td style='text-align:right; padding: 3px;'>(".($person+1).") ".number_format($value,2)."</td>";
									}	
								}	
							}
					$html .= "
						</tr>
						<tr>
							<td>3) การประกันตัวผู้ขับขี่ (บาท/ครั้ง)</td>";
							foreach ($getCompareOption as $key => $data2) { 
								$html .= "<td style='text-align:right; padding: 3px;'>".number_format($getCompareOption[$key]["item"]["532"],2)."</td>";
							}
					$html .= "
						</tr>
						<tr class='bgea fs10'>
							<td style='text-align:right;padding: 2px; '><b>เบี้ยสุทธิ</b></td>";
							foreach ($getCompareOption as $key => $data2) { 
								$html .= "<td style='text-align:right; padding: 2px;'><b>".number_format($data2["option"]["Net_Premium"],2)."</b></td>";
							}
					$html .= "
						</tr>
						<tr class='bgea fs10'>
							<td style='text-align:right;padding: 2px;'><b>เบี้ยประกันภัย</b></td>";
							foreach ($getCompareOption as $key => $data2) { 
								$html .= "<td style='text-align:right; padding: 2px;'><b>".number_format($data2["option"]["Total_Premium"],2)."</b></td>";
							}
					$html .= "</tr>
					<tr class='bgea fs10'>
						<td style='text-align:right;padding: 2px;'><b>เบี้ย พรบ.</b></td>";
						foreach ($getCompareOption as $key => $data2) { 
							$html .= "<td style='text-align:right; padding: 2px;'><b>".number_format($data2["option"]["Compulsory"],2)."</b></td>";
						}
					$html .= "
						</tr>
						<tr class='bgea fs10'>
							<td style='text-align:right;padding: 2px;'><b>ส่วนลด</b></td>";
							foreach ($getCompareOption as $key => $data2) {
								$html .= "<td style='text-align:right; padding: 2px;'><b>".number_format($data2["option"]["Discount"],2)."</b></td>";	
							}
					$html .= "
						</tr>
						<tr class='bgea fs10'>
							<td style='text-align:right;padding: 2px;'><b>เบี้ยประกันภัยรวมหลังหักส่วนลด</b></td>";
							foreach ($getCompareOption as $key => $data2) { 
								$html .= "<td style='text-align:right; padding: 2px;'><b>".number_format($data2["option"]["Premium_After_Disc"],2)."</b></td>";
							}
					$html .= "</tr>
						<tr class='bgea fs10'>
							<td style='text-align:right;padding: 2px;'><b>ชำระเงินสดงวดเดียว</b></td>";
							foreach ($getCompareOption as $key => $data2) { 
								// $payNow = $data2["option"]["Premium_After_Disc"] + $data2["option"]["Compulsory"];
								$p1 = ($data2["option"]["Current_Or_Proposal"] == "C") ? "-" : number_format($data2["option"]["Premium_After_Disc"],2);
								$html .= "<td style='text-align:right; padding: 2px;'><b>".$p1."</b></td>";
							}
					// $html .= "</tr>
					// 	<tr class='bgea fs10'>
					// 		<td style='text-align:right;padding: 2px;'><b>ผ่อนเงินสด (บาท)</b></td>>";
					// 			foreach ($getCompareOption as $key => $data2) { 
					// 				$down5per = $data2["option"]["Total_Premium"] - (($data2["option"]["Net_Premium"] * 5 ) / 100);
					// 				$p2 = ($data2["option"]["Current_Or_Proposal"] == "C") ? "-" : number_format(ceil($down5per/100)*100,2);
					// 				$html .= "<td style='text-align:right; padding: 2px;'><b>".$p2."</b></td>";
					// 			}
					// 	$html .= "</tr>
					// 	<tr class='bgea fs10'>
					// 		<td style='text-align:right;padding: 2px;'><b>ผ่อนเงินสด (งวด) งวดที่ 1<br> งวดที่ 2-3</b></td>";
					// 		foreach ($getCompareOption as $key => $data2) { 
					// 			$down = $data2["option"]["Total_Premium"] - (($data2["option"]["Net_Premium"] * 5 ) / 100);
					// 			$downUp = ceil($down/100)*100;
					// 			$down1 = round(($downUp * 40) / 100);
					// 			$down2 = round(($downUp - $down1) / 2);
					// 			if($data2["option"]["Current_Or_Proposal"] != "C"){
					// 				$html .= "<td style='text-align:right; padding: 2px;'><b>".number_format($down1+$data2["option"]["Compulsory"],2)."<br>".number_format($down2,2)."</b></td>";
					// 			}else{
					// 				$html .= "<td style='text-align:right; padding: 2px;'><b>-</b></td>";
					// 			}
					// 		}
						// $html .= "</tr>
						// 	<tr class='bgea fs10'>
						// 		<td style='text-align:right;padding: 2px;'><b>ผ่อนกับบัตรเครดิต 10 งวด งวดละ</b></td>";
						// 		foreach ($getCompareOption as $key => $data2) {
						// 			$pay = $data2["option"]["Total_Premium"] + $data2["option"]["Compulsory"];
						// 			$credit = ceil($pay / 10);
						// 			$p3 = ($data2["option"]["Current_Or_Proposal"] == "C") ? "-" : number_format($credit,2);
						// 			$html .= "<td style='text-align:right; padding: 2px;'><b>".$p3."</b></td>";
						// 		}
						// $html .= "</tr>";
						$html .= "</tbody>
          </table>   ";
        }
		$html .= '
			
			</div>
			<p style="margin: 10px 0px 0px 20px; font-size:14px;"> <b>หมายเหตุ</b>
			<br> - เอกสารประกอบการทำประกัน 1.สำเนากรมธรรม์เดิม 2.สำเนาบัตรประชาชน 3.สำเนาใบขับขี่(กรณีระบุชื่อ) 4.รูปถ่าย(กรณีประเภท1)
			<br> - ใบเสนอราคามีอายุ 15 วัน นับจากวันที่ท่านได้รับใบเสนอราคานี้
			<br> - หากลูกค้ามีการแจ้งเคลมหลังจากใบเสนอราคาฉบับนี้ เบี้ยประกันอาจมีการเปลี่ยนแปลง ซึ่งบริษัทจะแจ้งให้ทราบทันที
			<br> - แถมบริการช่วยเหลือฉุกเฉินบนท้องถนนฟรี 24 ชั่วโมง มูลค่าสูงสุด 2,900 บาท
			<br> - ผ่อนผ่านบัตรเครดิต กรุงไทย กสิกรไทย กรุงศรี โอมโปร เซ็นทรัล โลตัส เฟิร์สช๊อย
			<br> - กรณีผ่อนชำระ ทั้งบัตรเครดิตหรือเงินสด จะได้รับกรมธรรม์ฉบับจริงเมื่อชำระงวดสุดท้ายเรียบร้อยแล้ว
			<br> - ผ่อนผ่านบัตรเครดิต กรุงไทย กสิกรไทย กรุงศรี โฮมโปร เซ็นทรัล โลตัส เฟิร์สช๊อย 0% 3/6/10/12 เดือน
			<br> - บริษัทขอสงวนสิทธิ์ในการเปลี่ยนแปลงข้อมูล ทุนประกัน เบี้ยประกัน ความคุ้มครอง  โดยจะแจ้งให้ทราบในภายหลัง
			</p>
			<div></div>
		</div>
		';
// echo $html;
// echo $html;
$mpdf->SetWatermarkText('Asia Direct');
$mpdf->showWatermarkText = true;
$mpdf->WriteHTML($html);
$mpdf->Output("ADB-".$cpid.".pdf","I");
exit;

?>