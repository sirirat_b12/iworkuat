<?php 
session_start();

if(!isset($_SESSION["User"]['UserCode'])){
	echo "<script>alert('กรุณาเข้าสู่ระบบ!!!');</script>";
	echo '<META http-equiv="refresh" content="0;URL=login.php">';
	exit();
}

include "include/inc_header.php"; 
// include "include/inc_function.php"; 
include "include/inc_menu.php"; 
$sale = $_SESSION["User"]['UserCode'];
$apiMain = getAllCustomerMain($sale);
?> 

<div class="main">
	<div class="main-content p20">
		<div class=" bgff"> 
			<div class="p20">
			<h3 class="text-center">รายชื่อลูกค้า</h3>
				<table id="table" class="table table-striped p5" data-toggle="table" data-pagination="true" data-search="true"  data-page-size="100" data-page-list="[100, 150, 200, 250, 300]" >
					<thead>
						<tr>
							<th class="t_c"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></th>
							<th class="t_c"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></th>
							<th class="t_c">CP</th>
							<th class="t_c">iBroker</th>
							<th class="t_c">สถานะ</th>
							<th data-field="pocode" data-sortable="true">รหัส</th>
							<th >ชื่อ-นามสกุล</th>
							<th data-field="phone" class="t_c">เบอร์</th>
							<th>อีเมล์</th>
							<th>ไลน์</th>
							<th>ผู้ขาย</th>
							<th>ยี่ห้อรถ</th>
							<th>รุ่น</th>
							<th class="t_c">ปี</th>
							<th class="t_c">cc</th>
							<th data-field="created_date" data-sortable="true" class="t_c">วันที่ทำรายการ</th>
							
						</tr>
					</thead>
					<tbody class="fs12">
						<?php 
							if($apiMain){
								foreach ($apiMain as $key => $value) { 
									if($value["responsecode"] == 0){
										$response = "กรุณาลองใหม่";
									}elseif ($value["responsecode"] == 1) {
										$response = "ข้อเสนอใหม่";
									}elseif ($value["responsecode"] == 2) {
										$response = "ข้อมูลซ้ำ";
									}elseif ($value["responsecode"] == 4) {
										$response = "แก้ไขข้อเสนอ";
									}
						?>
							<tr>
								<td class="text-center">
									<?php //if(getCountapilist($value["api_id"]) > 0){ ?>
										<a href="export.php?pocode=<?php echo $value["pocode"]; ?>" title="PDF" target="_bank" class="c0aaaef "><i class="fas fa-file-pdf"></i></a>
									<?php //} ?>
								</td>
								<td class="t_c">
									<?php if($value["status"]==0){ ?>
										<a href="checkedit.php?pocode=<?php echo $value["pocode"]; ?>" title="แก้ไข" target="_bank" class="c02c "><i class="far fa-edit"></i></a>
									<?php } ?>
								</td>
								<td class="t_c cff8000"><?php echo $value["cpid"]; ?></td>
								<td class="t_c c0aaaef"><?php echo $response ; ?></td>
								<td class="t_c <?php echo ($value["status"] == 0 ) ? "c0aaaef" : "cff8000" ; ?>"><?php echo ($value["status"] == 0 ) ? "รอตอบรับ" : "ลูกค้าตกลง" ; ?></td>
								<td><?php echo $value["pocode"]; ?></td>
								<td ><?php echo $value["name"]; ?></td>
								<td class="t_c"><?php echo $value["phone"]; ?></td>
								<td><?php echo $value["email"]; ?></td>
								<td><?php echo $value["line"]; ?></td>
								<td ><?php echo $value["personnel_name"]; ?></td>
								<td><?php echo $value["make"]; ?></td>
								<td><?php echo $value["model"]; ?></td>
								<td class="t_c"><?php echo $value["year"]; ?></td>
								<td class="t_c"><?php echo $value["cc"]; ?></td>
								<td class="t_c"><?php echo $value["created_date"]; ?></td>
								<span style="display: none;" id="desc<?php echo $key; ?>">
				                    <div class="row" id="dataBox<?php echo $key; ?>"></div>
				                </span>
							</tr>
						<?php }
						} ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<?php include "include/inc_footer.php"; ?> 
<script type="text/javascript">
// var $table = $('#table');
// $table.on('expand-row.bs.table', function (e, index, row, $detail) {
//     var pcode = row.pocode;
//   	var res = $("#desc" + index).html();
//   	$.ajax({ 
// 		url: 'include/inc_action.php',
// 		type:'POST',
// 		data: {action:"getAllCustomerlist", pcode:pcode},
// 		success:function(rs){
// 			// console.log(rs);
// 			$detail.html(rs);
// 		}
// 	});
// });


function queryParams() {
    return {
        type: 'owner',
        sort: 'updated',
        direction: 'desc',
        per_page: 1,
        page: 1
    };
}

</script>