<?php
session_start();
date_default_timezone_set("Asia/Bangkok");
header('Content-Type: text/html; charset=utf-8');

include "../inc_config.php";
include "../include/inc_function_chk.php"; 
// include "../include/inc_header.php";
 // echo "<pre>".print_r($_POST,1)."</pre>";

// $search_type = 2;
// $pocode = 'P1296';
$search_type = $_POST["search_type"];
$pocode = $_POST["poCode"];
$searchPO = searchPO($pocode, $search_type);
 // echo "<pre>".print_r($searchPO,1)."</pre>";
?>
<!-- <link rel="stylesheet" href="../assets/css/bootstrap.min.css">
<link rel="stylesheet" href="../table_js/dist/bootstrap-table.min.css">
	<link rel="stylesheet" href="../assets/css/style.css"> -->

<div class="main-content p20 fs12">
	<form action="include/inc_action_chk.php" method="POST" id="form_send_order" enctype="multipart/form-data">
		<?php if($searchPO){ ?>
			<div class="t_r clearfix">
				<button type="button" class="btn btn-success btnOrders"><i class="fa fa-check-circle"></i> ส่งงาน</button>
			</div>
		<?php } ?>
		<input type="hidden" value="sendOrderAdmin" name="action" id="action"> 
		<input type="hidden" value="<?php echo $_SESSION["User"]['UserCode'] ?>" name="personnel_code" id="personnel_code"> 
		<input type="hidden" value="<?php echo $_SESSION["User"]['firstname']." ".$_SESSION["User"]['lastname'] ?>" name="personnel_name" id="personnel_name">

		<?php 
			foreach ($searchPO as $key => $value) { 
				$bgColor = ($value["Insurance_Name"] != "พรบ.") ? "bgf8f8f8" : "bgNEW";
		?>
			<div class="<?php echo $bgColor;?>" style="border-bottom: 2px solid #0089f6;">
				<div class="row p10">
					<div class="col-md-2">
							<div>
								<input type="checkbox" onclick="fnShowBoxImg('<?php echo $value["PO_ID"]?>')" class="chkpo" name="select_po[<?php echo $value["PO_ID"]?>]" value="<?php echo $value["PO_ID"]?>" > <span class="cff8000 fwb">แจ้งงาน</span>
							</div>
							<div><input type="checkbox" name="casePO[<?php echo $value["PO_ID"]?>][discount_cctv]" value="1" > ส่วนลดกล้อง</div>
							<div><input type="checkbox" name="casePO[<?php echo $value["PO_ID"]?>][endorse]" value="1" > สลักหลัง</div>
					</div>
					<div class="col-md-4">
						<div>
							<b class="cff2da5"><?php echo $value["PO_ID"] ?></b> 
							| <b class="c00ac0a"><?php echo $value["Coverage_Start_Date"]->format('d-m-Y'); ?></b>
							| <b class="cff8000"><?php echo $value["Title_Name"]." ".$value["FName"]." ".$value["LName"]; ?></b>
						</div>
						<!-- <div><b class="cff8000"><?php echo $value["Title_Name"]." ".$value["FName"]." ".$value["LName"]; ?></b></div> -->
						<div><b>ประเภทประกัน: </b><?php echo ($value["cus_type"]=='C') ? "ส่วนบุคคล" : "บริษัท"; ?></div>
						<div><b>ประกันชั้น: </b><?php echo $value["Insurance_Name"]  ?></div>
						<?php if($value["Insurance_Package_Name"]){ ?>
							<div><b>แพจเกต: </b><?php echo $value["Insurance_Package_Name"]  ?></div>
						<?php } ?>
						<div><b>บริษัทประกัน: </b><?php echo $value["Insurer_Name"]  ?></div>
						
						<?php if($value["Delivery_Type_Desc"]){ ?>
							<div><b>การส่ง: </b><?php echo $value["Delivery_Type_Desc"]  ?></div>
						<?php } ?>

						<?php if($value["Premium_Desc"]){ ?>
							<div><b>ของกำนัล: </b><?php echo $value["Premium_Desc"] ?></div>
							<div><b>ระบุ PO ที่รับของกำนัล</b>
								<input type="text" name="casePO[<?php echo $value["PO_ID"] ?>][giftvoucher_po]" value="<?php echo $value["PO_ID"] ?>"/></div>
						<?php } ?>
					</div>
					<div class="col-md-2">
						<div><b>ทุนประกัน: </b><?php echo number_format($value["Capital"],2); ?></div>
						<div><b>เบี้ยสุทธิ: </b><?php echo $value["Net_Premium"] ? number_format($value["Net_Premium"],2) : number_format($value["Compulsory"],2); ?></div>
						<div><b>เบี้ยรวมภาษี: </b><?php echo number_format($value["Total_Premium"],2); ?></div>
						<div><b>ส่วนลด: </b><?php echo number_format($value["Discount"],2); ?></div>
						<div><b>รวม: </b><?php echo number_format($value["Premium_After_Disc"],2); ?></div>
					</div>
					<div class="col-md-4">
							<p><b>หมายเหตุ</b></p>
							<textarea name="casePO[<?php echo $value["PO_ID"]?>][notes]" style="width: 100%; font-size: 12px; height: 60px;" ></textarea>
					</div>
				<?php if($value["Insurance_Name"] != "พรบ."){?>
					<div id="BoxUpImg_<?php echo $value["PO_ID"]?>"  class="col-md-12 dn" >
						<div class="col-md-12">
							<div class="row mt20">
								<div class="col-md-2">
									<span class="fl fs12 cff2da5 cff2da5">รูปรถด้านซ้าย</span><span>
										<input type="file" name="filUpload[<?php echo $value["PO_ID"]?>][carleft]" class="fl form-control inputFile" onchange="ValidateSingleInput(this);" accept="image/gif, image/jpeg"></span>
								</div>
								<div class="col-md-2">
									<span class="fl fs12 cff2da5">รูปรถด้านขวา</span><span>
										<input type="file" name="filUpload[<?php echo $value["PO_ID"]?>][carright]" class="fl form-control inputFile" onchange="ValidateSingleInput(this);" accept="image/gif, image/jpeg"></span>
								</div>
							
								<div class="col-md-2">
									<span class="fl fs12 cff2da5">รูปรถด้านหน้า</span><span>
										<input type="file" name="filUpload[<?php echo $value["PO_ID"]?>][carfront]" class="fl form-control inputFile" onchange="ValidateSingleInput(this);" accept="image/gif, image/jpeg"></span>
								</div>
								<div class="col-md-2">
									<span class="fl fs12 cff2da5">รูปรถด้านท้าย</span><span>
										<input type="file" name="filUpload[<?php echo $value["PO_ID"]?>][carback]" class="fl form-control inputFile" onchange="ValidateSingleInput(this);" accept="image/gif, image/jpeg"></span>
								</div>
								<div class="col-md-2">
									<span class="fl fs12 cff2da5">สำเนารถ</span><span>
										<input type="file" name="filUpload[<?php echo $value["PO_ID"]?>][carbook]" class="fl form-control inputFile" onchange="ValidateSingleInput(this);" accept="image/gif, image/jpeg , .pdf" ></span>
								</div>
								<div class="col-md-2">
									<span class="fl fs12 cff2da5">ใบขับบี่</span><span>
										<input type="file" name="filUpload[<?php echo $value["PO_ID"]?>][cardriver]" class="fl form-control inputFile" onchange="ValidateSingleInput(this);" accept="image/gif, image/jpeg , .pdf"></span>
								</div>
							</div>
							<div class="row mt15">
								<div class="col-md-2 ">
									<span class="fl fs12 cff2da5">หนังสือรับรองบริษัท</span><span>
										<input type="file" name="filUpload[<?php echo $value["PO_ID"]?>][company]" class="fl form-control inputFile" onchange="ValidateSingleInput(this);" accept="image/gif, image/jpeg , .pdf"></span>
								</div>
								<div class="col-md-2 ">
									<span class="fl fs12 cff2da5">บัตร ปชช. กรรมการ</span><span>
										<input type="file" name="filUpload[<?php echo $value["PO_ID"]?>][companycard]" class="fl form-control inputFile" onchange="ValidateSingleInput(this);" accept="image/gif, image/jpeg , .pdf"></span>
								</div>
								<div class="col-md-2">
									<span class="fl fs12 cff2da5">เอกสารอื่นๆ 1</span><span>
										<input type="file" name="filUpload[<?php echo $value["PO_ID"]?>][carall1]" class="fl form-control inputFile" onchange="ValidateSingleInput(this);" accept="image/gif, image/jpeg , .pdf"></span>
								</div>
								<div class="col-md-2">
									<span class="fl fs12 cff2da5">เอกสารอื่นๆ 2</span><span>
										<input type="file" name="filUpload[<?php echo $value["PO_ID"]?>][carall2]" class="fl form-control inputFile" onchange="ValidateSingleInput(this);" accept="image/gif, image/jpeg , .pdf"></span>
								</div>
								<div class="col-md-2">
									<span class="fl fs12 cff2da5">เอกสารอื่นๆ 3</span><span>
										<input type="file" name="filUpload[<?php echo $value["PO_ID"]?>][carall3]" class="fl form-control inputFile" onchange="ValidateSingleInput(this);" accept="image/gif, image/jpeg , .pdf"></span>
								</div>
								<div class="col-md-2">
									<span class="fl fs12 cff2da5">เอกสารอื่นๆ 4</span><span>
										<input type="file" name="filUpload[<?php echo $value["PO_ID"]?>][carall4]" class="fl form-control inputFile" onchange="ValidateSingleInput(this);" accept="image/gif, image/jpeg , .pdf"></span>
								</div>
							</div>
							<div class="row mt15">
								<div class="col-md-2">
									<span class="fl fs12 cff2da5">หลักฐานประกอบตรวจ 1</span><span>
										<input type="file" name="filUpload[<?php echo $value["PO_ID"]?>][reCheck1]" class="fl form-control inputFile" onchange="ValidateSingleInput(this);" accept="image/gif, image/jpeg , .pdf" ></span>
								</div>
								<div class="col-md-2">
									<span class="fl fs12 cff2da5">หลักฐานประกอบตรวจ 2</span><span>
										<input type="file" name="filUpload[<?php echo $value["PO_ID"]?>][reCheck2]" class="fl form-control inputFile" onchange="ValidateSingleInput(this);" accept="image/gif, image/jpeg , .pdf"></span>
								</div>
								<div class="col-md-2 ">
									<span class="fl fs12 cff2da5">หลักฐานประกอบตรวจ 3</span><span>
										<input type="file" name="filUpload[<?php echo $value["PO_ID"]?>][reCheck3]" class="fl form-control inputFile" onchange="ValidateSingleInput(this);" accept="image/gif, image/jpeg , .pdf" ></span>
								</div>
								<div class="col-md-2 ">
									<span class="fl fs12 cff2da5">หลักฐานประกอบตรวจ 4</span><span>
										<input type="file" name="filUpload[<?php echo $value["PO_ID"]?>][reCheck4]" class="fl form-control inputFile" onchange="ValidateSingleInput(this);" accept="image/gif, image/jpeg , .pdf"></span>
								</div>
								<div class="col-md-2 ">
									<span class="fl fs12 cff2da5">หลักฐานประกอบตรวจ 5</span><span>
										<input type="file" name="filUpload[<?php echo $value["PO_ID"]?>][reCheck5]" class="fl form-control inputFile" onchange="ValidateSingleInput(this);" accept="image/gif, image/jpeg , .pdf" ></span>
								</div>
								<div class="col-md-2 ">
									<span class="fl fs12 cff2da5">หลักฐานประกอบตรวจ 6</span><span>
										<input type="file" name="filUpload[<?php echo $value["PO_ID"]?>][reCheck6]" class="fl form-control inputFile" onchange="ValidateSingleInput(this);" accept="image/gif, image/jpeg , .pdf"></span>
								</div>
								<p class="fs12 c00ac0a">*** ไฟล์นามสกุล PNG | JPG | PDF  เท่านั้น *** </p>
							</div>
						</div>
					</div>
				<?php } ?>
				</div>
			</div>
		<?php } ?>
		<?php if($searchPO){ ?>
			<div class="t_c clearfix mt20">
				<button type="button" class="btn btn-success btnOrders"><i class="fa fa-check-circle"></i> ส่งงาน</button>
			</div>
		<?php }else{ ?>
			<div class="fs18 fwb cff2da5 t_c">************** ไม่พบข้อมูลกรุณาตรวจสอบ **************</div>
		<?php } ?>
	</form>
</div>
<!-- <script src="../assets/vendor/jquery/jquery.min.js"></script>
<script src="../assets/vendor/bootstrap/js/bootstrap.min.js"></script> -->
<script type="text/javascript">

function fnShowBoxImg(pocode){
	if($('.chkpo').is(":checked")){
	// console.log("checked:"+pocode);
		$("#BoxUpImg_"+pocode).slideDown();
	}else{
		$("#BoxUpImg_"+pocode).slideUp();
	}
}

$(".btnOrders").click(function() {
	 var countCK = $('.chkpo:checked').length;
    if(countCK > 0){
      // $(".btnOrders").hide();
			$("#form_send_order").submit();
    }else{
      alert("กรุณาเลือก PO ที่ต้องการแจ้งงาน");
    }
});

</script>