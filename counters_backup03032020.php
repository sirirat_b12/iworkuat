<?php 

session_start();
$UserCode = $_SESSION["User"]['UserCode'];
$userType = $_SESSION["User"]['type'];

if(!isset($UserCode)){
	echo "<script>alert('กรุณาเข้าสู่ระบบ!!!');</script>";
	echo '<META http-equiv="refresh" content="0;URL=login.php">';
	exit();
}

header('Content-Type: text/html; charset=utf-8');
include "include/inc_header.php"; 
include "include/inc_menu.php"; 
include "counters/inc_function_counters.php";
// include('qrcode/qrcode.class.php');

if(isset($_GET["txtse"])){
	$getListCounterAll = getListCounterAll($UserCode, $_GET["txtse"], "page");
	$paycose = $getListCounterAll[0]["paycode"];
}


// echo "<pre>".print_r($_SESSION,1)."</pre>";
?>
<div class="main">
	<div class="">
		<div class="p20">
				<div class="row">
					<div class="col-md-12">
						<div class="panel">
								<div class="panel-heading">
									<h4 class="panel-title"><b>ระบบชำระเงิน Counter Service</b></h4>
								</div>
								<div class="panel-body fs14">
									<div class="row">
										<div class="col-md-5 t_r"><h4>รหัสลูกค้า :</h4></div>
										<div class="col-md-2">
											<input class="form-control formInput2" type="text" name="txtSearch" id="txtSearch"  value="<?php echo $_GET["txtsearch"] ?>" required>
										</div>
										<div class="col-md-2">
											<button type="button" id="btn_send_order" class="btn  btn-info"><i class="fa fa-search-plus" aria-hidden="true"></i> เช็คข้อมูล</button>
										</div>
										<!-- <div class="col-md-6 t_r"> <input type="text" id="search" placeholder="ค้นหา" class="form-control " style="width: 30%;float: right;"/></div> -->
									</div>
									<div class="row mt20">
										<div class="col-md-12">
											<div class="row">
												<div class="col-md-2"></div><div class="col-md-10"><?php echo $value["PO_ID"]; ?></div>
											</div>
											<form action="counters/inc_action_counters.php" method="POST" id="frmListPay">
												<input type="hidden" name="action" value="inserListpay">
												<table class="table table-hover" id="tableMoveQC">
													<thead>
														<tr>
															<th class="t_c">
																<div><input type="checkbox" id="chkInsAll" name="chkInsAll" ></div>
															</th>
															<th class="t_c">งวดที่</th>
															<th class="t_c">PO</th>
															<th class="t_c">ชื่อ-นามสกุล</th>
															<th class="t_c">ประเภทประกัน</th>
															<th class="t_c">กำหนดชำระ</th>
															<th class="t_r">เบี้ยสุทธิ</th>
															<th class="t_r">อากร</th>
															<th class="t_r">ภาษี</th>
															<th class="t_r">เบี้ยรวม</th>
															<th class="t_r">ส่วนลด</th>
															<th class="t_r">จ่ายสุทธิ</th>
															<th class="t_c">สถานะของงวดชำระ</th>
															<!-- <th class="t_c">QR Code</th> -->
															<!-- <th class="t_c" >หมายเหตุ</th> -->
														</tr>
													</thead>
													<tbody id="table_data" class="table-list fs13"><tr><td colspan="12" class="text-center">กรุณาเลือกข้อมูล</td></tr></tbody>
												</table>
												<div class="p10 cff2da5" style="background-color: #ebebeb;">
													<div class="row">
														<div class="col-md-4">
															<p class="fwb">Email</p>
															<input type="text" name="email" id="txtemail" class="form-control cff2da5" >
														</div>
														<div class="col-md-4">
															<p class="fwb">Phone</p>
															<input type="text" name="phone" id="txtphone" class="form-control cff2da5" >
														</div>
														<div class="col-md-4">
															<p class="fwb">หมายเหตุ</p>
															<input type="text" name="remark" class="form-control cff2da5" value="">
														</div>
													</div>
													<div class="row">
														<div id="Waittxt" class="fs20 t_c  fwb mt25 dn">กรุณารอ..</div>
														<div class="col-md-12 mt20 t_c">
															<input type="button" class="btn btn-success" id="btnsub" value="ตกลง" onclick="btnsubmit()">
															<a href="counters.php" class="btn btn-danger" id="btncancel">ยกเลิก</a>
														</div>
													</div>
												</div>
											</form>
										</div>
									</div>
								</div>
						</div>
					</div>
				</div>
		</div>
	</div>
	<div class="">
		<div class="p20">
			<div class="row">
				<div class="col-md-12">
					<div class="panel">
							<div class="panel-heading">
								<div class="row mt15">
									<div class="col-md-2 cff2da5">
										<label for="filter" >PO Code | Pay Code | เบอร์</label>
										<input type="text" class="cff2da5 form-control fs12" id="txtse" value="<?php echo trim($_GET["txtse"]); ?>">
									</div>
									<div class="col-md-2 ">
										<span class="btn btn-success mt20" onclick="filterPOcode()">ค้นหา</span>
										<a href="counters.php" class="btn btn-danger mt20">รีเช็ต</a>
									</div>
								</div>
							</div>
							<div class="panel-body fs14">
									<table id="tablecp" class="table p5" data-toggle="table" data-pagination="true" data-page-size="100" data-page-list="[100, 200, 250, 300]">
										<thead class="fs13 c000000">
											<tr>
												<th class="t_c">#</th>
												<th class="t_c">Pay Code</th>
												<th class="t_c">ใช้งาน</th>
												<th class="t_c">สถานะ</th> 
												<th class="t_c">RV</th>
												<th class="t_c">วันที่ชำระ</th>
												<th class="t_c">งวดชำระ</th>
												<th class="t_c">Ref.1</th>
												<th class="t_c">Ref.2</th>
												<th class="t_c">รหัสลูกค้า</th>
												<th class="t_c">ชื่อ-นามสกุล</th>
												<th class="t_c">เบอร์</th>
												<th class="t_c">PO Code</th>
												<th class="t_c">ประเภท</th>
												<th class="t_c">วันครบชำระ</th>
												<th class="t_c">จำนวน</th>
												<th class="t_c">วันที่สร้าง</th>
												<th class="t_c">ผู้สร้าง</th>
											</tr>
										</thead>
										<tbody class="fs12 c000000">
											<?php 
												foreach ($getListCounterAll as $key => $value) {
													$bg = '';
													$status_pay = ($value["status_pay"] == 0 ) ? "ตั้งหนี้" : "ชำระ"; 
													$dudDate = ($value["installment_due_date"]) ? $value["installment_due_date"]->format("d/m/Y") : "-";
													if($value["status_pay"] != 0){
														$bg = 'class="bgbaf4bc"';
													}else if($value["enable"] != 'Y'){
														$bg = 'class="bgffeee8"';
													}else if($value["enable"] == 'N'){
														$bg = 'class="bgffeee8"';
													}

													$payment_date = ($value["payment_date"]->format("d/m/Y H:i:s") != "01/01/1900 00:00:00") ? $value["payment_date"]->format("d/m/Y H:i:s") : "-";
											 ?>
											<tr <?php echo $bg; ?> >
												<td class="t_c cf80000 ">
													<?php if($value["status_pay"]==0 && $value["enable"] == 'Y'){ ?>
														<span onclick="fnDelCS('<?php echo $value["counter_id"]; ?>','<?php echo $value["paycode"]; ?>')" class="cursorPoin"><i class="fas fa-trash"></i></span> 
													<?php } ?>
												</td>
												<td class="t_c c2457ff fwb">
													<a href="export_counterservices.php?paycode=<?php echo base64_encode($value["paycode"]); ?>" target='_bank' class="c2457ff"><i class="far fa-file-pdf fs14 " aria-hidden="true"></i> <?php echo $value["paycode"]; ?></a>
												</td>
												<td class="t_c <?php echo ($value["enable"]=='Y') ? "cff2da5" : ""; ?> fwb">
													<?php echo ($value["enable"]=='Y') ? "ใช้งาน" : "ยกเลิก"; ?>
												</td>
												<td class="t_c c9c00c8"><?php echo $status_pay; ?></td>
												<td class="t_c fwb cff8000"><?php echo $value["receive_id"]; ?></td>
												<td class="t_c cff2da5 fwb"><?php echo $payment_date; ?></td>
												<td class="t_c c2457ff"><?php echo $value["installment_num"]; ?></td>
												<td class="t_c "><?php echo $value["reference_1"]; ?></td>
												<td class="t_c "><?php echo $value["reference_2"]; ?></td>
												<td class="t_c "><?php echo $value["customer_id"]; ?></td>
												<td class="t_l cff2da5 fwb"><?php echo $value["customer_name"]; ?></td>
												<td class="t_c "><?php echo $value["tel_no"]; ?></td>
												<td class="t_c "><?php echo $value["po_id"]; ?></td>
												<td class="t_c "><?php echo ($value["insur_type"] == "insur") ? "ประกันภัย" : "พรบ."; ?></td>
												<td class="t_c "><?php echo $dudDate; ?></td>
												<td class="t_c "><?php echo number_format($value["amount"],2); ?></td>
												<td class="t_c cff2da5 fwb"><?php echo $value["created_date"]->format("d/m/Y H:i:s"); ?></td>
												<td class="t_c "><?php echo $value["created_by"]; ?></td>
											</tr>
											<?php } ?>
										</tbody>
								</table>
							</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php include "include/inc_footer.php"; ?> 
<script src="fancybox/dist/jquery.fancybox.min.js"></script>
<script type="text/javascript">

	function btnsubmit(){
		var countCK = $('input[name="inskey[]"]:checked').length;
  	if(countCK > 0){
  		if( confirm("คุณต้องการสร้างการชำระเงินผ่านระบบ Counter Service?")){
  			$("#btnsub, #btncancel").hide();
  			$("#Waittxt").show();
				$("#frmListPay").submit();
  		}
  	}else{
  		alert("กรุณาเลือกงวดที่ชำระ");
  		$("#btnsub, #btncancel").show();
  		$("#Waittxt").hide();
  	}
	}
	$("#chkInsAll").click(function() {
    if($('#chkInsAll').is(':checked')){
      $(".inskey").prop('checked', true);
      
    }else{
      $(".inskey").prop('checked', false);
      
    }
  });

	$("#btn_send_order").click(function() {
			$("#table_data").empty();
    	var txtsearch = $("#txtSearch").val();
    	if(txtsearch){
	    	$.ajax({ 
						url: 'counters/inc_action_counters.php',
						type:'POST',
						data: {action: 'GetListCS', txtsearch:txtsearch},
						success:function(rs){ 
							// console.log(rs);
							$('#table_data').append(rs);
						}
				});
				$.ajax({ 
						url: 'counters/inc_action_counters.php',
						type:'POST',
						dataType: 'json',
						data: {action: 'GetDataPhone', txtsearch:txtsearch},
						success:function(rs){ 
							// console.log(rs.phone);
							$("#txtphone").val(rs.phone);
							$("#txtemail").val(rs.email);
							// $('#table_data').append(rs);
						}
				});
			}else{
				alert("กรุณากรอกรหัสลูกค้าหรือเบอร์โทรศัพท์ลูกค้า");
			}

  });

function fnDelCS(cs_id, paycode){
	if(confirm("คุณต้องการลบการชำระเงิน รหัส "+paycode)){
  	$.ajax({ 
				url: 'counters/inc_action_counters.php',
				type:'POST',
				data: {action: 'deleteListcs', cs_id:cs_id, paycode:paycode},
				success:function(rs){ 
					// console.log(rs);
					if(rs == 1){
						alert("ลบการชำระเงิน "+paycode+" สำเร็๋จ");
						window.location.href = "counters.php?txtse="+paycode;
					}else{
						alert("ไม่สามารถลบการชำระเงิน "+paycode+" กรุณาลองใหม่");
						window.location.href = "counters.php?txtse="+paycode;
					}
				}
		});
	}
}
  

function filterPOcode(){
	txtse = $("#txtse").val();
	if(txtse){
		window.location.href = "counters.php?txtse="+txtse;
	}else{
		alert("กรุณากรอก ข้อมูล เพื่อค้นหา");
	}
}

</script>