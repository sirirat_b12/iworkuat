<?php 
function getMaxIDRef1($table){ // tor 20-09-2021
	global $connMS; 
	$sql = "SELECT MAX(reference_1) AS reference_1 From [dbo].".$table." 
	WHERE year(create_date) = '".date("Y")."' 
	AND MONTH(create_date) = '".date("m")."' ";
	$stmt = sqlsrv_query( $connMS, $sql );
	if(sqlsrv_has_rows($stmt)) {
		$row = sqlsrv_fetch_array($stmt);
		$code = substr($row["reference_1"],-7);
		$idMax = (date("y")+43)."".date("mdHi")."".str_pad(intval($code)+1,7,"0",STR_PAD_LEFT);
		return $idMax;
		exit();
	}  
}

function getInstallment($po_id,$ins_id) { //tor
	global $connMS;
	$sql = "SELECT  * FROM Installment WHERE Installment.PO_ID = '".$po_id."' AND Installment.Installment_ID = '".$ins_id."' ";
	// $sql = "SELECT  * FROM Installment WHERE Installment.PO_ID = '".$po_id."' ";
	$stmt = sqlsrv_query( $connMS, $sql );
	if(sqlsrv_has_rows($stmt)) {
		$row = sqlsrv_fetch_array($stmt);
		
	}  return $row; 
}
 
function getPOPayForCounter($txtSe){
	global $connMS;
	$resultarray = array();
  	$sql = "SELECT   Purchase_Order.PO_ID, counter_service.paycode,counter_service.enable, Insurance.Insurance_Name, Purchase_Order.PO_Date, Purchase_Order.Operation_Type_ID, Purchase_Order.Insurance_ID, Purchase_Order.Customer_ID, Purchase_Order.Agent_ID, Purchase_Order.Employee_ID, Purchase_Order.Status_ID,Status.Status_Desc,
		Installment.PO_ID, Installment.Installment_ID, Installment.Installment_Due_Date, Installment.ISTM_Net_Premium, Installment.ISTM_Duty, 
		Installment.ISTM_Tax, Installment.ISTM_Total_Premium, Installment.ISTM_Discount,Installment.ISTM_Total_Amount, Installment.Installment_Status_ID,
		Installment_Status.Installment_Status, Installment.Remark, Customer.Customer_ID, Customer.Customer_FName, Customer.Customer_LName, Customer.EMail, 
		Customer.Tel_No, Customer.Mobile_No
		FROM Purchase_Order 
		LEFT JOIN  Installment ON Purchase_Order.PO_ID = Installment.PO_ID
		LEFT JOIN  counter_service ON Installment.PO_ID = counter_service.PO_ID AND Installment.Installment_ID = counter_service.installment_num and counter_service.enable='Y'
		INNER JOIN  Customer ON Purchase_Order.Customer_ID = Customer.Customer_ID
		LEFT JOIN  Status ON Purchase_Order.Status_ID = Status.Status_ID
		LEFT JOIN  Installment_Status ON Installment.Installment_Status_ID = Installment_Status.Installment_Status_ID
		LEFT JOIN  Insurance ON Purchase_Order.Insurance_ID = Insurance.Insurance_ID
		WHERE Purchase_Order.Customer_ID LIKE '%".$txtSe."%'
		AND Installment.Installment_Status_ID = '001'
		--AND Purchase_Order.Status_ID IN ('RCA','RVP','CLS')
		ORDER BY Purchase_Order.Insurance_ID,Installment.Installment_ID ASC";
	  $stmt = sqlsrv_query( $connMS, $sql );
    if(sqlsrv_has_rows($stmt)) {
	    while( $row = sqlsrv_fetch_array($stmt) ) { 
	      $resultarray[] = $row;
	    }
	    return $resultarray;
			exit();
		}
}

 


























function actionSendSMS($phone, $txt){ // tor
	// $txt = 'โปรดกด '.$url.' เพื่อชำระเงิน ผ่าน QRCode  โทร. 020892000';
	if($txt){
		$result_sms = sms::send_sms('adbwecare','6251002','0823450022', $txt,'AsiaDirect','','');
		// $result_sms = sms::send_sms('adbwecare','6251002',$phone, $txt,'AsiaDirect','','');
	} 
	if($result_sms){
		return 1;
	}else{
		return 0;
	} 
}
  

function shorturl($long_url){
  $url = "https://adbwecare.com/shorturl/insert.php";
  $data = array(
    'action' => "inserUrl",
    'long_url' => $long_url
  );
  $ch = curl_init($url);
  curl_setopt($ch, CURLOPT_POST, 1);
  curl_setopt($ch, CURLOPT_POSTREDIR, 3);
  curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
  curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
  curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  $result = curl_exec($ch);
  $error_msg = curl_error($ch);
  curl_close($ch);

  return $result;
}

?>