<?php
session_start();
header('Content-Type: text/html; charset=utf-8');
include "include/inc_header.php"; 


// $_GET["p"] = 'UE82MTA2NjAyNg==';
$pocode = base64_decode($_GET["p"]);
$getPO = getPurchaseBycode($pocode);
$Cardetail = getCardetailBycode($pocode);
 // echo "<pre>".print_r($Cardetail,1)."</pre>";
if(!$getPO){
	echo "<script>alert('ไม่พบข้อมูลในระบบ กรุณาติดต่อทางอีเมล์แทน');</script>";
	echo '<meta http-equiv="refresh" content="0;URL=https://www.asiadirect.co.th">';
	exit();
}
?>

<div class="clearfix container ">
	<div class="p20">
		<div class="bgff p20 fs18 lh30 "> 
				<div >
					<div class="fs20 fwb c2457ff ">รถทะเบียน <?php echo $Cardetail["Plate_No"]." ".getProvinceBycode($Cardetail["Plate_Province"]);?></div>
					<div class="cff2da5 fwb">คุณ <?php echo $getPO["Customer_FName"]." ".$getPO["Customer_LName"];?></div>
					<div>เริ่มความคุ้มครอง : <?php echo $getPO["Coverage_Start_Date"]->format('d/m/Y');?> | สิ้นความคุ้มครอง : <?php echo $getPO["Coverage_End_Date"]->format('d/m/Y');?></div>
				</div>
				<hr>
				<?php if(!$getPO["Notice_No"]){ ?>
					<div>
						<form action="">
							<input type="hidden" name="p" value="<?php echo $_GET["p"]; ?>">
							<input type="hidden" name="poid" id="poid" value="<?php echo $pocode; ?>">
							<div>
								<p class="fwb">เลขรับแจ้ง</p>
								<p class="fs14 lh20">วันที่ทำรายการ : <?php echo date("d-m-Y");?></p>
								<input type="text" name="Notice_No" id="Notice_No" class="form-control" required>
							</div>
							<div class="mt20 t_c">
								<input type="reset" class="btn btn-danger" value="ยกเลิก">
								<input type="button" class="btn btn-success" value="ยืนยัน" id="btncallNoticeNO">
							</div>
						</form>
					</div>
				<?php }else{ ?>
					<div class="fs25 fwb t_c cf40053">เลขรับแจ้ง : <?php echo $getPO["Notice_No"]; ?></div>
				<?php } ?>
		</div>
	</div>
</div>

<?php include "include/inc_footer.php"; ?> 
<script type="text/javascript">
	$( "#btncallNoticeNO" ).click(function() {
		Notice_No = $("#Notice_No").val();
		poid = $("#poid").val();
		if(Notice_No){
			$.ajax({ 
					url: 'include/inc_action_chk.php',
					type:'POST',
					data: {action: 'insercallNotice', Notice_No:Notice_No , poid:poid},
					success:function(rs){
						// console.log(rs);
						if(rs){
							alert("เพิ่มเลขรับแจ้งเข้าระบบเรียบร้อย");
						}else{
							alert("ไม่สามารถทำรายการได้ กรุณาลองใหม่อีกครั้ง");
						}
						window.location.reload(true);
					}
				});
		}else{
			alert("กรุณากรอก เลขรับแจ้ง");
		}
	});
</script>
