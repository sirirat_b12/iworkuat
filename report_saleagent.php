<?php 
session_start();
ini_set('max_execution_time', 186);
ini_set('memory_limit', '2048M');
$UserCode = $_SESSION["User"]['UserCode'];
$userType = $_SESSION["User"]['type'];

if(!isset($UserCode)){
	echo "<script>alert('กรุณาเข้าสู่ระบบ!!!');</script>";
	echo '<META http-equiv="refresh" content="0;URL=login.php">';
	exit();
}

header('Content-Type: text/html; charset=utf-8');
include "include/inc_header.php"; 
include "include/inc_menu.php"; 
include "reports/inc_function_report.php";
// include('qrcode/qrcode.class.php');

$year = date("Y");
$_GET["txtse"] = ($_GET["txtse"]) ? $_GET["txtse"] : date("Y-m");
if($_GET["txtse"]){
	$getDataCount = getSaleAgentforMounth($_GET["txtse"]);
}

?>
<div class="main">
	<div class="">
		<div class="p20">
				<div class="row">
					<div class="col-md-12">
						<div class="panel">
								<div class="panel-heading">
									<h4 class="panel-title"><b>รายงานยอดขาย Agent เดือน <?php echo date("F");?></b></h4>
								</div>
								<div class="panel-body fs14">
									<div class="row">
										<div class="col-md-4 ">
											<span class="fs18 cff2da5 mr15">เดือน</span>
											<select name="mounth" id="mounthOpt" class="form-control formInput2 dib" style="width: 80%" >
												<option value="">:: กรุณาเลือก ::</option>
													<?php 
														$dateLast = ( date("Y") - 1)."-".date("m");
														for($i=1; $i<=12; $i++) { 
															$txtM = "+".$i." month";
															$dateForValue = date("Y-m",strtotime($txtM, strtotime($dateLast)));
													?>
														<option value="<?php echo $dateForValue ;?>" <?php if($_GET["txtse"] ==  $dateForValue){ echo "selected"; } ?>>
															<?php echo date("Y m F",strtotime($dateForValue)) ?>
														</option>
													<?php } ?>
											</select>
										</div>
									</div>
									<div class="row mt20">
										<div class="col-md-12">
											<div class="row">
												<div class="col-md-2"></div><div class="col-md-10"><?php echo $value["PO_ID"]; ?></div>
											</div>
											<?php if($getDataCount){?>
												<table class="table table-hover table-bordered" id="indextable" >
													<thead>
														<tr class="bgbaf4bc">
															<th class="t_c">รหัสพนักงาน</th>
															<th class="t_c">ชื่อ-นามสกุล</th>
															<th class="t_c"><a href="javascript:SortTable(2,'N');">ยอดชำระ</a></th>
															<th class="t_c"><a href="javascript:SortTable(3,'N');">ยอดรอชำระ</a></th>
														</tr>
													</thead>
													<tbody id="table_data" class="table-list fs13">
														<?php 
															$sumAll = 0;
															foreach ($getDataCount as $key => $value) {
															$sumAll = $value["sumNew"] + $value["sumRenew"]; 
															
															$totalsumNew +=  $value["sumNew"];
															$totalsumRCA +=  $value["sumRCA"];
														?>
															<tr>
																<td class="t_c"><?php echo $value["Agent_ID"]; ?></td>
																<td class="t_l"><?php echo $value["Agent_Name"]." ".$value["Agent_Surname"]; ?></td>
																<td class="t_r fwb "><?php echo number_format($value["sumNew"],2); ?></td>
																<td class="t_r fwb "><?php echo number_format($value["sumRCA"],2); ?></td>
															</tr>
															<?php } ?>
													<tfoot>
														<tr class="fwb fs16 bgfffbd8">
															<td class="t_c" colspan="2">รวม</td>
															<td class="t_r fwb "><?php echo number_format($totalsumNew,2); ?></td>
															<td class="t_r fwb "><?php echo number_format($totalsumRCA,2); ?></td>
														</tr>
													</tfoot>
												</table>
											<?php } ?>
										</div>
									</div>
								</div>
						</div>
					</div>
				</div>
		</div>
	</div>
</div>
<?php include "include/inc_footer.php"; ?> 
<?php include "include/inc_function_tablesort.php"; ?>
<script type="text/javascript">

	$("#mounthOpt").on('change', function() {
		if(this.value){
			window.location.href = "report_saleagent.php?txtse="+this.value;
		}

  });

<?php if($_GET["txtse"]){?>
function getDetailPO(user, date){
	txtse = <?php echo $_GET["txtse"]; ?>;
	if(txtse){
		window.location.href = "report_saleagent.php?txtse="+txtse+"&user="+user+"&date="+date;
	}
}
<?php } ?>
  

function filterPOcode(){
	txtse = $("#txtse").val();
	if(txtse){
		window.location.href = "counters.php?txtse="+txtse;
	}else{
		alert("กรุณากรอก ข้อมูล เพื่อค้นหา");
	}
}

function random_rgba() {
    var o = Math.round, r = Math.random, s = 255;
    return 'rgba(' + o(r()*s) + ',' + o(r()*s) + ',' + o(r()*s) + ',' + r().toFixed(2) + ')';
}

</script>