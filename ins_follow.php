<?php 

session_start();

$UserCode = $_SESSION["User"]['UserCode'];
$userType = $_SESSION["User"]['type'];

// echo "<pre>".print_r($_SESSION,1)."</pre>"; exit;
if($UserCode){
	echo "<script>alert('กรุณาเข้าสู่ระบบ!!!');</script>";
	echo '<META http-equiv="refresh" content="0;URL=login.php">';
	exit();
}

header('Content-Type: text/html; charset=utf-8');
include "include/inc_header.php"; 
include "include/inc_menu.php"; 
include "insfollow/inc_function_insfollow.php"; 


$date = ($_GET["date"]) ? $_GET["date"] : date("Y-m-d");
$dateStart = ($_GET["datestart"]) ? $_GET["datestart"] : date("Y-m-d");
$dateEnd = ($_GET["dateend"]) ? $_GET["dateend"] : date("Y-m-d");
$getRemind = getInstallment("", "", "Remind");

$getInstallment = getInstallment($date, $_GET["txtse"], "");
$getInstallmentFollowTopic = getInstallmentFollowTopic($userType);
if($_SESSION["User"]['type'] == "SuperAdmin" || $_SESSION["User"]['type'] == "Accounting" ){
	$Postpone = getPostpone();
	$getCase014 = getInstallment("", "", "014");
	$getInstallmentForAsia3M = getInstallmentForAsia3M($dateStart, $dateEnd, $_GET["txtse"], "");
}
if($_SESSION["User"]['type'] == "SuperAdmin" || $_SESSION["User"]['type'] == "Admin"  ){
	// $getCancelByAccounting = getCancelByAccounting();
}
// echo "<pre>".print_r($getInstallmentForAsia3M,1)."</pre>"; 
?>

<div class="main">
	<div class="p20">
		<div class="panel">
			<!-- <div class="panel-heading"> -->
				<div class="main-content p20">
					<ul class="nav nav-tabs">
						<li class="active"><a data-toggle="tab" href="#menu1">งวดชำระ</a></li>
						<li ><a data-toggle="tab" href="#menu2">ติดตามอีกครั้ง</a></li>
					<?php if($_SESSION["User"]['type'] == "SuperAdmin" || $_SESSION["User"]['type'] == "Accounting"  ){ ?>
						<li><a data-toggle="tab" href="#menu3">ขอเลื่อนวันชำระ</a></li>
						<li><a data-toggle="tab" href="#menu4">ขอยกเลิกโดยเซลล์</a></li>
						<li><a data-toggle="tab" href="#menu6">Asia 3M</a></li>
					<?php } ?>
					<?php if($_SESSION["User"]['type'] == "SuperAdmin" || $_SESSION["User"]['type'] == "Admin"  ){ ?>
						<!-- <li><a data-toggle="tab" href="#menu5">ยกเลิกกรมธรรม์</a></li> -->
					<?php } ?>
					</ul>
				</div> 
				<div class="tab-content">
						<div id="menu1" class="tab-pane fade in active">
							<div class="panel-heading">
								<div class="row mt15">
									<div class="col-md-2 cff2da5">
										<label for="filter" >วันที่</label>
										<input type="date" class=" form-control fs12" id="date" value="<?php echo $date; ?>">
									</div>
									<div class="col-md-2 cff2da5">
										<label for="filter" >PO Code</label>
										<input type="text" class="cff2da5 form-control fs12" id="txtse" value="<?php echo trim($_GET["txtse"]); ?>">
									</div>
									<div class="col-md-2 ">
										<a href="ins_follow.php" class="btn btn-danger mt10">รีเช็ต</a>
										<span class="btn btn-success mt10" onclick="filterPOcode()">ค้นหา</span>
									</div>
								</div>
							</div>
							<div class="panel-body fs14">
								<table id="tablecp" class="table p5" data-toggle="table" data-pagination="true" data-page-size="200" data-page-list="[200, 300, 400]">
									<thead class="fs13 c000000">
										<tr>
											<th class="t_c">Follow</th>
											<?php if($_SESSION["User"]['type'] == "SuperAdmin" || $_SESSION["User"]['type'] == "Accounting"  ){ ?>
												<th class="t_c">SMS</th>
											<?php } ?>
											<th class="t_c">PO</th>
											<th class="t_c">สถานะ PO</th>
											<th class="t_c">งวดที่</th> 
											<th class="t_c">วันชำระ</th>
											<th class="t_c">Follow</th>
											<th class="t_c">ชื่อลูกค้า</th>
											<th class="t_c">เบอร์</th>
											<th class="t_c">ทะเบียนรถ</th>
											<!-- <th class="t_c">รหัสพนักงานขาย</th> -->
											<th class="t_c">พนักงานขาย</th>
											<th class="t_c">จำนวนเงิน</th>
											<th class="t_c">สถานะ</th>
										</tr>
									</thead>
									<tbody class="fs12">
										<?php 
										foreach ($getInstallment as $key => $value) { 
											$phone = $value["Tel_No"] ? $value["Tel_No"] : $value["Mobile_No"];
											$Due = $value["Installment_Due_Date"]->format("d/m/Y");
											$FollowID = $value["Installment_Followup_Topic_ID"];

											if($FollowID == '008'  || $FollowID == '004' || $FollowID == '012'){
												$bgcolor = "bgffeee8";
											}elseif($FollowID == '009'){
												$bgcolor = "bgf7a937";
											}elseif($FollowID == '001'){
												$bgcolor = "bgNEW";
											}elseif($FollowID == '007'){
												$bgcolor = "bgbaf4bc";
											}else{
												$bgcolor = "";
											}

										?>
											<tr class="<?php echo $bgcolor ?>" >
												<td class="t_c c00ac0a fwb" >
													<span class="cursorPoin" onclick="onModal('<?php echo $value["PO_ID"]; ?>', '<?php echo $value["Installment_ID"]; ?>', '<?php echo $value["Employee_ID"]; ?>')"><i class="far fa-comments" ></i> Follow </span>
												</td>
												<?php if($_SESSION["User"]['type'] == "SuperAdmin" || $_SESSION["User"]['type'] == "Accounting"  ){ ?>
												<td class="t_c c9c00c8 fwb">
													<?php if($phone && $value["Installment_Status_ID"] == '001'){ ?>
														<span class="cursorPoin" onclick="sendSMS('<?php echo $value["PO_ID"]; ?>', '<?php echo $value["Installment_ID"]; ?>',  '<?php echo $phone; ?>', '<?php echo $value["ISTM_Total_Amount"]; ?>', '<?php echo $value["Plate_No"]; ?>', '<?php echo $Due; ?>')"><i class="far fa-envelope-open"></i> SMS </span>
												</td>
													<?php } ?>
												<?php } ?>
													<td class="t_c cff2da5 fwb"><?php echo $value["PO_ID"]; ?></td>
													<td class="t_c c2457ff fwb"><?php echo $value["Status_ID"]; ?></td>
													<td class="t_c c2457ff fwb"><?php echo $value["Installment_ID"]; ?></td> 
													<td class="t_c cf40053 fwb"><?php echo $Due; ?></td>
													<td class="t_l c2457ff fwb"><?php echo $value["Installment_Followup_Topic"]; ?></td>
													<td class="t_l"><?php echo $value["Customer_FName"]." ".$value["Customer_LName"]; ?></td>
													<td class="t_c"><?php echo $phone ; ?></td>
													<td class="t_c"><?php echo $value["Plate_No"]; ?></td>
													<!-- <td class="t_c"><?php echo $value["Employee_ID"]; ?></td> -->
													<td class="t_l"><?php echo $value["User_FName"]." ".$value["User_LName"]; ?></td>
													<td class="t_r fwb"><?php echo number_format($value["ISTM_Total_Amount"],2); ?></td>
													<td class="t_c"><?php echo $value["Installment_Status"]; ?></td>
												</tr>
											<?php } ?>
										</tbody>
								</table>
							</div>
						</div>
						<div id="menu2" class="tab-pane fade">
							<div class="fs14">
								<table id="tablecp" class="table p5" data-toggle="table" data-pagination="true" data-page-size="200" data-page-list="[200, 300, 400]">
									<thead class="fs13 c000000">
										<tr>
											<th class="t_c">Follow</th>
											<?php if($_SESSION["User"]['type'] == "SuperAdmin" || $_SESSION["User"]['type'] == "Accounting"  ){ ?>
												<th class="t_c">SMS</th>
											<?php } ?>
											<th class="t_c">ยกเลิกติดตาม</th> 
											<th class="t_c">ติดตาม</th> 
											<th class="t_c">PO</th>
											<th class="t_c">งวดที่</th> 
											<th class="t_c">วันชำระ</th>
											<th class="t_c">Follow</th>
											<th class="t_c">ชื่อลูกค้า</th>
											<th class="t_c">เบอร์</th>
											<th class="t_c">ทะเบียนรถ</th>
											<!-- <th class="t_c">รหัสพนักงานขาย</th> -->
											<th class="t_c">พนักงานขาย</th>
											<th class="t_c">จำนวนเงิน</th>
											<th class="t_c">สถานะ</th>
										</tr>
									</thead>
									<tbody class="fs12">
										<?php 
										foreach ($getRemind as $key => $value) { 
											$phone = $value["Tel_No"] ? $value["Tel_No"] : $value["Mobile_No"];
											$Due = $value["Installment_Due_Date"]->format("d/m/Y");
											$FollowID = $value["Installment_Followup_Topic_ID"];

											if($FollowID == '008'  || $FollowID == '004'){
												$bgcolor = "bgffeee8";
											}elseif($FollowID == '009'){
												$bgcolor = "bgf7a937";
											}elseif($FollowID == '001'){
												$bgcolor = "bgNEW";
											}elseif($FollowID == '007'){
												$bgcolor = "bgbaf4bc";
											}else{
												$bgcolor = "";
											}

										?>
											<tr class="<?php echo $bgcolor ?>" >
												<td class="t_c c00ac0a fwb" >
													<span class="cursorPoin" onclick="onModal('<?php echo $value["PO_ID"]; ?>', '<?php echo $value["Installment_ID"]; ?>', '<?php echo $value["Employee_ID"]; ?>')"><i class="far fa-comments" ></i> Follow </span>
												</td>
												<?php if($_SESSION["User"]['type'] == "SuperAdmin" || $_SESSION["User"]['type'] == "Accounting"  ){ ?>
												<td class="t_c c9c00c8 fwb">
													<?php if($phone && $value["Installment_Status_ID"] == '001'){ ?>
														<span class="cursorPoin" onclick="sendSMS('<?php echo $value["PO_ID"]; ?>', '<?php echo $value["Installment_ID"]; ?>', '<?php echo $phone; ?>', '<?php echo $value["ISTM_Total_Amount"]; ?>', '<?php echo $value["Plate_No"]; ?>', '<?php echo $Due; ?>')"><i class="far fa-envelope-open"></i> SMS </span>
												</td>
													<?php } ?>
												<?php } ?>
													<td class="t_c cff2da5 fwb">	<span class="cursorPoin" onclick="cancelFollow('<?php echo $value["PO_ID"]; ?>', '<?php echo $value["Installment_ID"]; ?>')"><i class="far fa-bell-slash"></i> ยกเลิกตาม </span></td>
													<td class="t_c fwb"><?php echo $value["Remind_Date"]->format("d/m/Y"); ?></td>
													<td class="t_c fwb"><?php echo $value["PO_ID"]; ?></td>
													<td class="t_c fwb"><?php echo $value["Installment_ID"]; ?></td> 
													<td class="t_c cf40053 fwb"><?php echo $Due; ?></td>
													<td class="t_l c2457ff fwb"><?php echo $value["Installment_Followup_Topic"]; ?></td>
													<td class="t_l"><?php echo $value["Customer_FName"]." ".$value["Customer_LName"]; ?></td>
													<td class="t_c"><?php echo $phone ; ?></td>
													<td class="t_c"><?php echo $value["Plate_No"]; ?></td>
													<!-- <td class="t_c"><?php echo $value["Employee_ID"]; ?></td> -->
													<td class="t_l"><?php echo $value["User_FName"]." ".$value["User_LName"]; ?></td>
													<td class="t_r fwb"><?php echo number_format($value["ISTM_Total_Amount"],2); ?></td>
													<td class="t_c"><?php echo $value["Installment_Status"]; ?></td>
												</tr>
											<?php } ?>
										</tbody>
								</table>
							</div>
						</div>
						<div id="menu3" class="tab-pane fade">
							<table id="tablecp" class="table p5" data-toggle="table" data-pagination="true" data-page-size="200" data-page-list="[200, 300, 400]">
								<thead class="fs13 c000000">
									<tr>
										<th class="t_c">#</th>
										<th class="t_c">PO</th>
										<th class="t_c">งวดที่</th> 
										<th class="t_c">วันที่ ติดตาม</th>
										<th class="t_c">วันชำระเดิม</th>
										<th class="t_c">วันที่ขอเลื่อน</th>
										<th class="t_c">รายละเอียด</th>
										<th class="t_c">พนักงานขาย</th>
									</tr>
								</thead>
								<tbody class="fs12">
									<?php foreach ($Postpone as $key => $value) { 
												$Postpone_Date = $value["Postpone_Date"]->format("d/m/Y");
												$Installment_Due_Date = $value["Installment_Due_Date"]->format("d/m/Y");
												
										?>
										<tr>
											<td class="t_c c00ac0a fwb">
												<span class="cursorPoin" onclick="onPostpone('<?php echo $value["Installment_Followup_id"]; ?>')"><i class="fas fa-bolt"></i> ตรวจสอบ</span>
											</td>
											<td class="t_c cff2da5 fwb"><?php echo $value["PO_ID"]; ?></td>
											<td class="t_c c2457ff fwb"><?php echo $value["Installment_ID"]; ?></td> 
											<td class="t_c"><?php echo $value["Installment_Followup_DateTime"]->format("d/m/Y H:i:s"); ?></td>
											<td class="t_c cf40053 fwb"><?php echo $Installment_Due_Date; ?></td>
											<td class="t_c cf40053 fwb"><?php echo $Postpone_Date; ?></td>
											<td class="t_l"><?php echo $value["Installment_Followup_Detail"]; ?></td>
											<td class="t_l"><?php echo $value["User_FName"]." ".$value["User_LName"]; ?></td>
										</tr>
									<?php  } ?>
								</tbody>
							</table>
						</div>
						<div id="menu4" class="tab-pane fade">
							<div class="fs14">
								<table id="tablecp" class="table p5" data-toggle="table" data-pagination="true" data-page-size="200" data-page-list="[200, 300, 400]">
									<thead class="fs13 c000000">
										<tr>
											<th class="t_c">Follow</th>
											<?php if($_SESSION["User"]['type'] == "SuperAdmin" || $_SESSION["User"]['type'] == "Accounting"  ){ ?>
												<th class="t_c">SMS</th>
											<?php } ?>
											<th class="t_c">ติดตาม</th> 
											<th class="t_c">PO</th>
											<th class="t_c">งวดที่</th> 
											<th class="t_c">วันชำระ</th>
											<th class="t_c">Follow</th>
											<th class="t_c">ชื่อลูกค้า</th>
											<th class="t_c">เบอร์</th>
											<th class="t_c">ทะเบียนรถ</th>
											<!-- <th class="t_c">รหัสพนักงานขาย</th> -->
											<th class="t_c">พนักงานขาย</th>
											<th class="t_c">จำนวนเงิน</th>
											<th class="t_c">สถานะ</th>
										</tr>
									</thead>
									<tbody class="fs12">
										<?php 
										foreach ($getCase014 as $key => $value) { 
											$phone = $value["Tel_No"] ? $value["Tel_No"] : $value["Mobile_No"];
											$Due = $value["Installment_Due_Date"]->format("d/m/Y");
											$FollowID = $value["Installment_Followup_Topic_ID"];

											if($FollowID == '008'  || $FollowID == '004'){
												$bgcolor = "bgffeee8";
											}elseif($FollowID == '009'){
												$bgcolor = "bgf7a937";
											}elseif($FollowID == '001'){
												$bgcolor = "bgNEW";
											}elseif($FollowID == '007'){
												$bgcolor = "bgbaf4bc";
											}else{
												$bgcolor = "";
											}

										?>
											<tr class="<?php echo $bgcolor ?>" >
												<td class="t_c c00ac0a fwb" >
													<span class="cursorPoin" onclick="onModal('<?php echo $value["PO_ID"]; ?>', '<?php echo $value["Installment_ID"]; ?>', '<?php echo $value["Employee_ID"]; ?>')"><i class="far fa-comments" ></i> Follow </span>
												</td>
												<?php if($_SESSION["User"]['type'] == "SuperAdmin" || $_SESSION["User"]['type'] == "Accounting"  ){ ?>
												<td class="t_c c9c00c8 fwb">
													<?php if($phone && $value["Installment_Status_ID"] == '001'){ ?>
														<span class="cursorPoin" onclick="sendSMS('<?php echo $value["PO_ID"]; ?>', '<?php echo $value["Installment_ID"]; ?>', '<?php echo $phone; ?>', '<?php echo $value["ISTM_Total_Amount"]; ?>', '<?php echo $value["Plate_No"]; ?>', '<?php echo $Due; ?>')"><i class="far fa-envelope-open"></i> SMS </span>
												</td>
													<?php } ?>
												<?php } ?>
													<td class="t_c fwb"><?php echo $value["Remind_Date"]->format("d/m/Y"); ?></td>
													<td class="t_c fwb"><?php echo $value["PO_ID"]; ?></td>
													<td class="t_c fwb"><?php echo $value["Installment_ID"]; ?></td> 
													<td class="t_c cf40053 fwb"><?php echo $Due; ?></td>
													<td class="t_l c2457ff fwb"><?php echo $value["Installment_Followup_Topic"]; ?></td>
													<td class="t_l"><?php echo $value["Customer_FName"]." ".$value["Customer_LName"]; ?></td>
													<td class="t_c"><?php echo $phone ; ?></td>
													<td class="t_c"><?php echo $value["Plate_No"]; ?></td>
													<!-- <td class="t_c"><?php echo $value["Employee_ID"]; ?></td> -->
													<td class="t_l"><?php echo $value["User_FName"]." ".$value["User_LName"]; ?></td>
													<td class="t_r fwb"><?php echo number_format($value["ISTM_Total_Amount"],2); ?></td>
													<td class="t_c"><?php echo $value["Installment_Status"]; ?></td>
												</tr>
											<?php } ?>
										</tbody>
								</table>
							</div>
						</div>
						<div id="menu5" class="tab-pane fade">
							<table id="tablecp" class="table p5" data-toggle="table" data-pagination="true" data-page-size="200" data-page-list="[200, 300, 400]">
								<thead class="fs13 c000000">
									<tr>
										<th class="t_c">#</th>
										<th class="t_c">ยกเลิก</th>
										<th class="t_c">PO</th>			
										<th class="t_c">บริษัทประกัน</th>	
										<th class="t_c">แพตเกจ</th>								
										<th class="t_c">เลขกรมธรรม์</th>	
										<th class="t_c">ลูกค้า</th>	
										<th class="t_c">รายละเอียด</th>
										<th class="t_c">เวลาทำรายการ</th>
										<th class="t_c">พนักงานขาย</th>
									</tr>
								</thead>
								<tbody class="fs12">
									<?php foreach ($getCancelByAccounting as $key => $value) { 
												$Postpone_Date = $value["Postpone_Date"]->format("d/m/Y");
												$numberPolicy = $value["Policy_No"] ? $value["Policy_No"] : $value["Compulsory_No"];
										?>
										<tr <?php echo ($value["Active"] == "M")? "class='bgIPC2'":"" ?> >
											<td class="t_c c00ac0a fwb">
												<a href="ins_email.php?poid=<?php echo $value["PO_ID"]; ?>&key=<?php echo $value["Installment_Followup_id"];?>&ins=<?php echo $value["Installment_ID"];?>" target="_Bank" class="c2457ff fwb"><i class="fas fa-envelope-open"></i> ส่งอีเมล์</a>
											</td>
											<td class="t_c cff2da5 fwb">
												<span class="cursorPoin" onclick="submitCancelINS('<?php echo $value["PO_ID"]; ?>','<?php echo $value["Installment_ID"]; ?>', '<?php echo $value["Employee_ID"]; ?>')"><i class="far fa-bell-slash"></i> ยกเลิกกรมธรรม์</span>
											</td>
											<td class="t_c c2457ff fwb"><?php echo $value["PO_ID"]; ?></td>
											<td class="t_l fwb"><?php echo $value["Insurer_Name"]; ?></td>
											<td class="t_l fwb"><?php echo $value["Insurance_Package_Name"]; ?></td>
											<td class="t_l c2457ff fwb"><?php echo $numberPolicy; ?></td>
											<td class="t_l fwb"><?php echo $value["Customer_FName"]." ".$value["Customer_LName"]; ?></td>
											<td class="t_l"><?php echo $value["Installment_Followup_Detail"]; ?></td>
											<td class="t_l"><?php echo $value["Create_Date"]->format("d/m/Y H:i:s"); ?></td>
											<td class="t_l"><?php echo $value["User_FName"]." ".$value["User_LName"]; ?></td>
										</tr>
									<?php  } ?>
								</tbody>
							</table>
						</div>
						<div id="menu6" class="tab-pane fade in">
							<div class="panel-heading">
								<div class="row mt15">
									<div class="col-md-2 cff2da5">
										<label for="filter" >เริ่มวันที่</label>
										<input type="date" class=" form-control fs12" id="dateCase6" value="<?php echo $dateStart; ?>">
									</div>
									<div class="col-md-2 cff2da5">
										<label for="filter" >ถึงวันที่</label>
										<input type="date" class=" form-control fs12" id="dateCase6End" value="<?php echo $dateEnd; ?>">
									</div>
									<div class="col-md-2 ">
										<a href="ins_follow.php" class="btn btn-danger mt10">รีเช็ต</a>
										<span class="btn btn-success mt10" onclick="filterPOcodeCase6()">ค้นหา</span>
									</div>
								</div>
							</div>
							<div class="panel-body fs14">
								<table id="tablecp" class="table p5" data-toggle="table" data-pagination="true" data-page-size="200" data-page-list="[200, 300, 400]">
									<thead class="fs13 c000000">
										<tr>
											<th class="t_c">Follow</th>
											<?php if($_SESSION["User"]['type'] == "SuperAdmin" || $_SESSION["User"]['type'] == "Accounting"  ){ ?>
												<th class="t_c">SMS</th>
											<?php } ?>
											<th class="t_c">PO</th>
											<th class="t_c">สถานะ PO</th>
											<th class="t_c">งวดที่</th> 
											<th class="t_c">วันชำระ</th>
											<th class="t_c">Follow</th>
											<th class="t_c">ชื่อลูกค้า</th>
											<th class="t_c">เบอร์</th>
											<th class="t_c">ทะเบียนรถ</th>
											<!-- <th class="t_c">รหัสพนักงานขาย</th> -->
											<th class="t_c">พนักงานขาย</th>
											<th class="t_c">จำนวนเงิน</th>
											<th class="t_c">สถานะ</th>
										</tr>
									</thead>
									<tbody class="fs12">
										<?php 
										foreach ($getInstallmentForAsia3M as $key => $value) { 
											$phone = $value["Tel_No"] ? $value["Tel_No"] : $value["Mobile_No"];
											$Due = $value["Installment_Due_Date"]->format("d/m/Y");
											$FollowID = $value["Installment_Followup_Topic_ID"];

											if($FollowID == '008'  || $FollowID == '004' || $FollowID == '012'){
												$bgcolor = "bgffeee8";
											}elseif($FollowID == '009'){
												$bgcolor = "bgf7a937";
											}elseif($FollowID == '001'){
												$bgcolor = "bgNEW";
											}elseif($FollowID == '007'){
												$bgcolor = "bgbaf4bc";
											}else{
												$bgcolor = "";
											}

										?>
											<tr class="<?php echo $bgcolor ?>" >
												<td class="t_c c00ac0a fwb" >
													<span class="cursorPoin" onclick="onModal('<?php echo $value["PO_ID"]; ?>', '<?php echo $value["Installment_ID"]; ?>', '<?php echo $value["Employee_ID"]; ?>')"><i class="far fa-comments" ></i> Follow </span>
												</td>
												<?php if($_SESSION["User"]['type'] == "SuperAdmin" || $_SESSION["User"]['type'] == "Accounting"  ){ ?>
												<td class="t_c c9c00c8 fwb">
													<?php if($phone && $value["Installment_Status_ID"] == '001'){ ?>
														<span class="cursorPoin" onclick="sendSMS('<?php echo $value["PO_ID"]; ?>', '<?php echo $value["Installment_ID"]; ?>',  '<?php echo $phone; ?>', '<?php echo $value["ISTM_Total_Amount"]; ?>', '<?php echo $value["Plate_No"]; ?>', '<?php echo $Due; ?>')"><i class="far fa-envelope-open"></i> SMS </span>
												</td>
													<?php } ?>
												<?php } ?>
													<td class="t_c cff2da5 fwb"><?php echo $value["PO_ID"]; ?></td>
													<td class="t_c c2457ff fwb"><?php echo $value["Status_ID"]; ?></td>
													<td class="t_c c2457ff fwb"><?php echo $value["Installment_ID"]; ?></td> 
													<td class="t_c cf40053 fwb"><?php echo $Due; ?></td>
													<td class="t_l c2457ff fwb"><?php echo $value["Installment_Followup_Topic"]; ?></td>
													<td class="t_l"><?php echo $value["Customer_FName"]." ".$value["Customer_LName"]; ?></td>
													<td class="t_c"><?php echo $phone ; ?></td>
													<td class="t_c"><?php echo $value["Plate_No"]; ?></td>
													<!-- <td class="t_c"><?php echo $value["Employee_ID"]; ?></td> -->
													<td class="t_l"><?php echo $value["User_FName"]." ".$value["User_LName"]; ?></td>
													<td class="t_r fwb"><?php echo number_format($value["ISTM_Total_Amount"],2); ?></td>
													<td class="t_c"><?php echo $value["Installment_Status"]; ?></td>
												</tr>
											<?php } ?>
										</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>


<div class="modal fade" id="ModalFollow" tabindex="-1" role="dialog" aria-labelledby="ModalFollowTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<p class="modal-title fs16 fwb" id="memberModalLabel"><b>การติดตาม</b> <span id="titlePO"></span> <b>งวดชำระ</b> <span id="titleINS"></span></p>
			</div>
			<div class="">
				<div class="p20">
					<div class="row">
						<div class="col-md-6 ModalFollowBody" style="background-color: #ffffed;"></div>
						<div class="col-md-6 boxSubmit">
							<form action="javascript:void(0);" name="fromAddfollow" id="fromAddfollow" >
								<input type="hidden" value="AddModalFollow" name="action">
								<input type="hidden" name="PO_ID" id="PO_ID">
								<input type="hidden" name="Installment_ID" id="Installment_ID">
								<div class="mt10">
									<lable class="cff2da5 ">วันที่ทำรายการ</lable>	
									<input type="datetime" name="Installment_Followup_DateTime" value="<?php echo date("Y-m-d H:i:s");?>" class="form-control" readonly>
								</div>
								<div class="mt10">
									<lable class="cff2da5 ">หัวข้อ *</lable>	
									<select name="Installment_Followup_Topic" id="Followup_Topic" class="form-control" onchange="getTopicval(this.value);" required> 
										<option value="">== กรุณาเลือก ==</option>';
										<?php 
											foreach ($getInstallmentFollowTopic as $key => $topic) {
											 	if($topic["Installment_Followup_Topic_ID"] != "007"){ 
											?>
											<option value="<?php echo $topic["Installment_Followup_Topic_ID"];?>"><?php  echo $topic["Installment_Followup_Topic"]?></option>';
											<?php }
											} ?>
									</select>
								</div>
								<!-- <div class="mt10 postpone dn">
									<lable class="cff2da5 ostpone">เลื่อนชำระเป็นวันที่ *</lable>	
									<input type="date" name="Postpone_Date" id="Postpone_Date"  class="form-control" >
								</div> -->
								<div class="mt10 mb10 fs16">
									<input type="checkbox" name="Remind_Status" id="Remind_Status" value="1" > <span class="c00ac0a ">ติดตามใหม่อีกครั้ง</span>
								</div>
								<div class="mt10 remindDate dn">
									<lable class="cff2da5 ostpone">ติดตามใหม่วันที่ *</lable>	
									<input type="date" name="Remind_Date" id="Remind_Date"  class="form-control" >
								</div>
								<div class="mt10">
									<lable class="cff2da5 ">รายละเอียด *</lable>	
									<textarea name="Installment_Followup_Detail" id="Installment_Followup_Detail"  class="form-control" style="height: 100px;" required></textarea>
								</div>
								<div class="t_c mt10 ">
									<button class="btn btn-success " onClick="submitAddfollow()">บันทึก</button>
									<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="ModalPostpone" tabindex="-1" role="dialog" aria-labelledby="ModalPostponeTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered ">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<p class="modal-title fs16 fwb" id="memberModalLabel"><b>ขอเลื่อนวันชำระ </b> <span id="PostponePO"></span> <b>งวดชำระ</b> <span id="PostponeINS"></span></p>
			</div>
			<div class="">
				<div class="p20">
					<div class="row">
						<div class="col-md-12">
							<form action="insfollow/inc_action_insfollow.php" name="fromUpdatePostpone" id="fromUpdatePostpone" method="POST">
								<input type="hidden" value="UpdatePostpone" name="action">
								<input type="hidden" name="caseFN" id="caseFN">
								<input type="hidden" name="Installment_Followup_id" id="Installment_Followup_id">
								<input type="hidden" name="PO_ID" id="upPOid">
								<input type="hidden" name="Installment_ID" id="upInstallment">
								<div class="p5 bgee">
									<p class="fwb">เหตุผลในการข้อเลื่อน</p>
									<p class="cff8000" id="lastDetail"></p>
								</div>
								<div class="mt10">
									<lable class="c9c00c8 ">วันที่ทำรายการ</lable>	
									<input type="datetime" name="Installment_Followup_DateTime" value="<?php echo date("Y-m-d H:i:s");?>" class="form-control" readonly>
								</div>
								<div class="mt10">
									<lable class="c9c00c8 ostpone">เลื่อนชำระเป็นวันที่ *</lable>	
									<input type="date" name="Postpone_Date" id="upPostpone_Date"  class="form-control" >
								</div>
								<div class="mt10">
									<lable class="c9c00c8 ">รายละเอียด *</lable>	
									<textarea name="Installment_Followup_Detail" id="upDetail"  class="form-control" style="height: 100px;" ></textarea>
								</div>
								<div class="mt10 mb10 fs16">
									<input type="checkbox" name="Remind_Status" id="Remind_StatusV2" value="1" > <span class="c00ac0a ">ติดตามใหม่อีกครั้ง</span>
								</div>
								<div class="mt10 remindDateV2 dn">
									<lable class="cff2da5 ostpone">ติดตามใหม่วันที่ *</lable>	
									<input type="date" name="Remind_Date" id="Remind_DateV2"  class="form-control" >
								</div>
								<div class="t_c mt10">
									<button type="button" class="btn btn-success" onclick="submitPostpone(1)">อนุมัติ</button>
									<button type="button" class="btn btn-danger" onclick="submitPostpone(0)">ยกเลิกคำขอ</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<div class="modal fade" id="ModalPhone" tabindex="-1" role="dialog" aria-labelledby="ModalPhoneTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered ">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<p class="modal-title fs16 fwb cff8000 " id="memberModalLabel">ส่ง SMS แจ้งเตือนลูกค้า <span id="cusPhone"></span></p>
			</div>
			<div class="">
				<div class="p20">
					<div class="row">
						<div class="col-md-12">
							<p class="fwb fs16">กรุณาเลือก SMS ที่จะจัดส่ง</p>
							<input type="hidden" name="smsPO" id="smsPO">
							<input type="hidden" name="smsIns" id="smsIns">
							<input type="hidden" name="smsPhone" id="smsPhone">
							<div class="row">
								<div class="mt5 col-md-4"><input type="radio" name="msgsmscase" value="C1"> ล่าช้าเกินกำหนด</div>
								<div class="mt5 col-md-4"><input type="radio" name="msgsmscase" value="C2"> ใกล้ถึงวันชำระ</div>
								<!-- <div class="mt5 col-md-4"><input type="radio" name="msgsmscase" value="C3"> แจ้งยกเลิกกรมธรรม์</div> -->
							</div>
							<div class="mt20 t_c"><button type="button" class="btn btn-success" onclick="submitSMS()">ส่ง SMS</button></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>



<?php include "include/inc_footer.php"; ?> 
<script src="fancybox/dist/jquery.fancybox.min.js"></script>
<script type="text/javascript">


$('#Remind_Status').change(function () {
  if ($('#Remind_Status').is(":checked")){
  	$(".remindDate").slideDown();
  }else{
  	$(".remindDate").slideUp();
  }
});

$('#Remind_StatusV2').change(function () {
  if ($('#Remind_StatusV2').is(":checked")){
  	$(".remindDateV2").slideDown();
  }else{
  	$(".remindDateV2").slideUp();
  }
});



	function getTopicval(rs){
		if(rs == "001"){
			$(".postpone ").slideDown();
		}else{
			$(".postpone ").slideUp();
		}
	}
	function filterPOcode(){
		txtse = $("#txtse").val();
		date = $("#date").val();
		window.location.href = "ins_follow.php?txtse="+txtse+"&date="+date;
	}

	function filterPOcodeCase6(){
		txtse = $("#txtse").val();
		dateStart = $("#dateCase6").val();
		dateend = $("#dateCase6End").val();
		window.location.href = "ins_follow.php?txtse="+txtse+"&datestart="+dateStart+"&dateend="+dateend;
	}

	function submitPostpone(rs){
		$("#caseFN").val(rs);
		console.log();
		$( "#fromUpdatePostpone" ).submit();
	}

	function onModal(po, ins, emp_id){
		UserCode = "<?php echo $_SESSION["User"]['UserCode']; ?>";
		$.ajax({ 
			url: 'insfollow/inc_action_insfollow.php',
			type:'POST',
			data: {action: 'ModalFollowBody', po:po, ins:ins},
			success:function(rs){
				$("#titlePO, #PO_ID").html(po);
				$("#titleINS, #Installment_ID").html(ins);
				$("#PO_ID").val(po);
				$("#Installment_ID").val(ins);
				$(".ModalFollowBody").html(rs);
				<?php if($_SESSION["User"]['type'] == "Sale"){ ?>
					if(emp_id != UserCode){
						$(".boxSubmit").slideUp(ins);
					}
				<?php  } ?>
				$('#ModalFollow').modal();
			}
		});
	}
	
	function onPostpone(id){
		$.ajax({ 
			url: 'insfollow/inc_action_insfollow.php',
			type:'POST',
			data: {action: 'getPostpone', id:id},
			success:function(rs){
				val = jQuery.parseJSON(rs);
				$("#Installment_Followup_id").val(val.Installment_Followup_id);
				$("#PostponePO").html(val.PO_ID);
				$("#PostponeINS").html(val.Installment_ID);
				$("#upPOid").val(val.PO_ID);
				$("#upInstallment").val(val.Installment_ID);
				$("#lastDetail").html(val.Installment_Followup_Detail);
				postpone = val.Postpone_Date["date"].split(' ');
				$("#upPostpone_Date").val(postpone[0]);
				postponeDate = postpone[0];
				$('#ModalPostpone').modal();
			}
		});
	}

 	function deleteComment(id, poid, ins){
 		console.log(id);
 		if(confirm("คุณต้องการลบการติดตาม? ")){
			$.ajax({ 
				url: 'insfollow/inc_action_insfollow.php',
				type:'POST',
				data: {action: 'deleteComment', id:id, poid:poid, ins:ins},
				success:function(rs){
					console.log(rs);
					$(".ModalFollowBody").html(rs);
				}
			});
		}
 	}

	function submitCancelINS(poid, ins, empid){
		// console.log(poid);
		if(confirm("ต้องการยกเลิกกรมธรรม์ "+poid)){
			$.ajax({ 
				url: 'insfollow/inc_action_insfollow.php',
				type:'POST',
				data: {action: 'submitCancelINS', poid:poid, ins:ins, empid:empid},
				success:function(rs){
					// console.log(rs);
					if(rs == 1){
						alert("แก้ไขสถานะ PO เรียบร้อยกรุณาตรวจสอบ");
					}else{
						alert("กรุณาลองใหม่อีกครั้ง");
					}
					window.location.reload(true);
				}
			});
		}
	}

	function cancelFollow(poid, ins){
		if(confirm("ต้องการยกเลิกติดตาม "+poid+ " งวดที่ " + ins)){
			$.ajax({ 
				url: 'insfollow/inc_action_insfollow.php',
				type:'POST',
				data: {action: 'cancelFollow', poid:poid, ins:ins},
				success:function(rs){
					// console.log(rs);
					if(rs == 1){
						alert("ยกเลิกการติดตามเรียบร้อย");
					}else{
						alert("กรุณาลองใหม่อีกครั้ง");
					}
					window.location.reload(true);
				}
			});
		}
	}

	function sendSMS(po, ins, phone,  price, Plate_No, Due){
		$("#cusPhone").html(phone);
		$("#smsPO").val(po);
		$("#smsIns").val(ins);
		$("#smsPhone").val(phone);
		$('#ModalPhone').modal();
	}

	function submitSMS(){
		msgsmscase = $('input[name=msgsmscase]:checked').val();
		po = $('#smsPO').val();
		ins = $('#smsIns').val();
		phone = $('#smsPhone').val();
		console.log(ins);
		if(msgsmscase){
			$.ajax({ 
				url: 'insfollow/inc_action_insfollow.php',
				type:'POST',
				data: {action: 'sendSMS', casesms:msgsmscase, po:po, ins:ins, phone:phone},
				success:function(rs){
					console.log(rs);
					if(rs = 0){
						alert("กรุณาลองใหม่อีกครั้ง");
					}
					$('#ModalPhone').modal('toggle');
				}
			});
		}else{
			alert("กรุณาเลือกการส่ง SMS");
		}
	}

	function submitAddfollow(){
		fromAddfollow = $( "#fromAddfollow" ).serialize();
		Detail = $("#Installment_Followup_Detail").val();
		Followup_Topic = $("#Followup_Topic").val();
		Postpone_Date = $("#Postpone_Date").val();
		Remind_Date = $("#Remind_Date").val();
		if(Detail && Followup_Topic){
			// if(Followup_Topic == "001" && !Postpone_Date){
			// 	alert("กรุณาเลือกวันที่เลือกชำระ");
			if($('#Remind_Status').is(":checked") && !Remind_Date){
				alert("กรุณาเลือกวันที่ติดตามใหม่");
			}else{
				$.ajax({ 
					url: 'insfollow/inc_action_insfollow.php',
					type:'POST',
					data: fromAddfollow,
					success:function(rs){
						console.log(rs);
						if(rs == 0){
							// alert("กรุณาลองใหม่อีกครั้ง");
						}else{
							$(".ModalFollowBody").html(rs);
							$('#Installment_Followup_Detail, #Postpone_Date ,#Followup_Topic, #Remind_Date').val('');
							$('#Remind_Status').prop('checked', false);
							$(".postpone, .remindDate ").slideUp();
						}
					}
				});
			}
		}

	}
</script>