<?php 

session_start();
if(!isset($_SESSION["User"]['UserCode'])){
	echo "<script>alert('กรุณาเข้าสู่ระบบ!!!');</script>";
	echo '<META http-equiv="refresh" content="0;URL=login.php">';
	exit();
}

if(!$_GET["pocode"] && !$_GET["numcode"] && $_SESSION["User"]['type'] == "QualityControl" && $_SESSION["User"]['type'] == "CallCenter"){
	echo '<META http-equiv="refresh" content="0;URL=sendorders.php">';
	exit();
}

header('Content-Type: text/html; charset=utf-8');
include "include/inc_header.php"; 
include "include/inc_menu.php"; 

$pocode = $_GET["pocode"];
$numcode = $_GET["numcode"];
// $getPO = getPurchaseBycode($pocode);
// $Cardetail = getCardetailBycode($pocode);
// $getCommentlist = getCommentlist();
$getIR = getInsuranceRequest($pocode);
$getNoti = getNotiWorkByID($numcode); 
$personnel = getRowpersonnelByCode($getNoti["personnel_code"]);
$getFile = getNotiWorkFilesByID($numcode);
$getCheck = getNotiWorkCheck($numcode, $pocode);
$datakeyword = unserialize($getCheck["keyword"]);
$datadata = unserialize($getCheck["data"]);
$datachecklists = unserialize($getCheck["checklists"]);
$datacomments = unserialize($getCheck["comments"]);
$getPremium = getPremium();
$companyIns = getCompanyBysrv();
$getInsuranceType = getInsuranceType();
$getNotiComment = getNotiComment($numcode);
$getCommenStatus = getCommenStatus();

// echo "<pre>".print_r($getNoti,1)."</pre>";
$arrFile = array('carleft', 'carright', 'carfront', 'carback', 'carbook', 'cardriver', 'carall1','carall2','carall3','carall4', 'company', 'companycard', 'reCheck1', 'reCheck2', 'reCheck3', 'reCheck4');
?> 
<div class="main">
	<div class="main-content p20">
		<form action="include/inc_action_chk.php" method="POST" id="form_send_order" enctype="multipart/form-data">
			<input type="hidden" name="action" id="action" value="sendUpdatelistchk">
			<input type="hidden" name="noti_id" id="noti_id" value="<?php echo $getNoti["noti_work_id"]; ?>">
			<input type="hidden" name="pocode" id="pocode" value="<?php echo $pocode; ?>">
			<input type="hidden" name="numcode" id="numcode" value="<?php echo $numcode; ?>">
			<input type="hidden" name="insurer" id="insurer" value="<?php echo $getNoti["insuere_company"]; ?>">
			<div class="p20">
				<div class="row">
					<div class="col-md-12">
						<div class="panel">
								<div class="panel-heading">
									<h4 class="panel-title"><b>ข้อมูลแจ้งงาน</b></h4>
									<span class="fs12"><b>รหัส : </b><?php echo $getNoti["noti_work_code"];?></span>
								</div>
								<div class="panel-body fs14">
									<?php if($getNoti["status"] == 1){ ?>
										<div class="col-lg-12">
											<div class="row">
												<div class="col-md-6">
													<div class="row">
														<div class="col-lg-4 t_r"><b>PO :</b></div>
														<div class="col-lg-8">
															<input class="form-control formInput2 dib" type="text" name="po_code" id="po_code"  value="<?php echo $getNoti["po_code"];?>" required style="width: 80%;">
															<span class="btn btn-info" id="btn_send_order">เช็คข้อมูล</span>
														</div>
													</div>
													<div class="row mt10">
														<div class="col-lg-4 t_r"><b>ประเภทประกัน :</b></div>
														<div class="col-lg-8">
															<input type="radio" name="po_type" id="po_type_p" value="ส่วนบุคคล" <?php if($getNoti["po_type"] =="ส่วนบุคคล"){ echo "checked"; }?> > ส่วนบุคคล | 
															<input class="ml10" type="radio" name="po_type" id="po_type_c" value="บริษัท" <?php if($getNoti["po_type"] =="บริษัท"){ echo "checked"; }?>> บริษัท
														</div>
													</div>
													<div class="row mt10">
														<div class="col-lg-4 t_r"><b>ชื่อลูกค้า :</b></div>
														<div class="col-lg-8">
															<input class="form-control formInput2" type="text" name="cus_name" id="cus_name" value="<?php echo $getNoti["cus_name"];?>" required>
														</div>
													</div>
													<div class="row mt10">
														<div class="col-lg-4 t_r"><b>วันเริ่มคุ้มครอง :</b></div>
														<div class="col-lg-8">
															<input type="date" name="start_cover_date" id="start_cover_date" class="form-control formInput2" value="<?php echo $getNoti["start_cover_date"];?>" required></div>
													</div>
													
													<div class="row mt10">
														<div class="col-lg-4 t_r"><b>ส่วนลดกล้อง :</b></div>
														<div class="col-lg-8">
															<input type="checkbox" id="discount_cctv" name="discount_cctv" value="1" <?php if($getNoti["discount_cctv"] == 1){ echo "checked"; }?>> 
														</div>
													</div>
													<div class="row mt10">
														<div class="col-lg-4 t_r"><b>ของแถม :</b></div>
														<div class="col-lg-8">
															<select class="form-control formInput2 fs12" name="giftvoucher" id="giftvoucher" >
																<option value="0">กรุณาเลือก</option>
																<?php foreach ($getPremium as $key => $value) { ?>
																	<option value="<?php echo  $value["Premium_Desc"]; ?>" <?php echo ($getNoti["giftvoucher"] == $value["Premium_Desc"])? "selected" : "";?>>
																		<?php echo  $value["Premium_Desc"]; ?>
																		</option>
																<?php } ?>
															</select>
														</div>
													</div>
													<div class="row mt10">
														<div class="col-lg-4 t_r"><b>หมายเหตุ :</b></div>
														<div class="col-lg-8">
															<textarea name="notes" id="notes" style="width: 100%; font-size: 12px; height: 100px;" ><?php echo $getNoti["notes"];?></textarea>
														</div>
													</div>
													<div class="row mt10">
														<div class="col-lg-4 t_r"><b>แพจเกต :</b></div>
														<div class="col-lg-8">
															<input class="form-control formInput2" type="hidden" name="package_id" id="packageID" value="<?php echo $getNoti["package_id"];?>"  >
															<input class="form-control formInput2" type="text" name="package_name" id="packageName" value="<?php echo $getNoti["package_name"];?>" >
														</div>
													</div>
													<div class="row mt10">
														<div class="col-lg-4 t_r"><b>ประกันชั้น :</b></div>
														<div class="col-lg-8">
															<select class="form-control formInput2 fs12" name="insurance_type" id="InsuranceType" >
																	<option value="0">กรุณาเลือก</option>
																	<?php foreach ($getInsuranceType as $key => $value) { ?>
																		<option value="<?php echo  $value["Insurance_Name"]; ?>" <?php echo ($getNoti["insurance_type"] == $value["Insurance_Name"])? "selected" : "";?>><?php echo  $value["Insurance_Name"]; ?></option>
																	<?php } ?>
																</select>
														</div>
													</div>
													<div class="row mt10">
														<div class="col-lg-4 t_r"><b>บริษัทประกัน :</b></div>
														<div class="col-lg-8">
															<select id="insuere_company" class="form-control formInput2 formInput" name="insuere_company" >
									              <option value="">กรุณาเลือก</option>
									              <?php foreach ($companyIns as $compa) { ?>
									              	 <option value="<?php echo $compa["Insurer_Initials"];?>" <?php echo ($getNoti["insuere_company"] == $compa["Insurer_Initials"])? "selected" : "";?>>
									              	 	<?php echo $compa["Insurer_Name"];?></option>
									              <?php } ?>
										          </select>
														</div>
													</div>
													<div class="row mt10">
														<div class="col-lg-4 t_r"><b>ทุนประกัน :</b></div>
														<div class="col-lg-8 t_r">
															<input class="form-control formInput2" type="text" name="insuere_cost" id="insuere_cost" value="<?php echo $getNoti["insuere_cost"];?>" required>
														</div>
													</div>
													<div class="row mt10">
														<div class="col-lg-4 t_r"><b>เบี้ยสุทธิ :</b></div>
														<div class="col-lg-8 t_r">
															<input class="form-control formInput2 t_r" type="text" name="netpremium" id="netpremium" value="<?php echo $getNoti["netpremium"];?>" required>
														</div>
													</div>
													<div class="row mt10">
														<div class="col-lg-4 t_r"><b>เบี้ยรวมภาษี :</b></div>
														<div class="col-lg-8 t_r">
															<input class="form-control formInput2 t_r" type="text" name="premium" id="premium" value="<?php echo $getNoti["premium"];?>" required>
														</div>
													</div>
													<div class="row mt10">
														<div class="col-lg-4 t_r"><b>ส่วนลด :</b></div>
														<div class="col-lg-8 t_r">
															<input class="form-control formInput2 t_r" type="text" name="discount" id="discount" value="<?php echo $getNoti["discount"];?>" >
														</div>
													</div>
													<div class="row mt10">
														<div class="col-lg-4 t_r"><b>รวม :</b></div>
														<div class="col-lg-8 t_r">
															<input class="form-control formInput2 t_r" type="text" name="taxamount" id="taxamount" value="<?php echo $getNoti["taxamount"];?>" >
														</div>
													</div>
													<div class="row mt10">
														<div class="col-lg-4 t_r"><b>การจัดส่งเอกสาร</b></div>
														<div class="col-lg-8 t_r">
															<select id="send_type" class="form-control formInput2 formInput" name="send_type" onchange="fnSendType();" >
									              <option value="">กรุณาเลือก</option>
									              <option value="ลูกค้ารับเอง" <?php if($getNoti["send_type"]=="ลูกค้ารับเอง") { echo "selected"; } ?> >ลูกค้ารับเอง</option>
									              <option value="พนักงานจัดส่ง" <?php if($getNoti["send_type"]=="พนักงานจัดส่ง") { echo "selected"; } ?>>พนักงานจัดส่ง</option>
									              <option value="ลงทะเบียน" <?php if($getNoti["send_type"]=="ลงทะเบียน") { echo "selected"; } ?>>ลงทะเบียน</option>
									              <option value="EMS" <?php if($getNoti["send_type"]=="EMS") { echo "selected"; } ?>>EMS</option>
										          </select>
														</div>
													</div>
													<div class="row mt10">
														<div class="col-lg-4 t_r"><b>ที่อยู่ส่งเอกสาร</b></div>
														<div class="col-lg-8 t_r">
															<input class="form-control formInput2 t_l" type="text" name="send_addr" id="send_addr" value="<?php echo $getNoti["send_addr"];?>" >
														</div>
													</div>
													<div class="row mt10">
														<div class="col-lg-4 t_r"><b>ผู้แจ้งงาน :</b></div>
														<div class="col-lg-4"><?php echo $getNoti["personnel_code"]." ". $getNoti["personnel_name"];?></div>
													</div>
													<div class="row mt10">
														<div class="col-lg-4 t_r"><b>สถานะ :</b></div>
														<div class="col-lg-4"><?php echo setStatus($getNoti["status"]);?></div>
													</div>
													<div class="row mt10">
														<div class="col-lg-4 t_r"><b>การใช้งาน :</b></div>
														<div class="col-lg-4"><?php echo ($getNoti["enable"] == 1) ? "ใช้งาน" : "ปิดใช้งาน" ; ?></div>
													</div>
												</div>
												<div class="col-md-6">
													<?php $num = 1;
														foreach ($arrFile as $key => $value) {
															$getImageEdit =  getImageEdit($getNoti["noti_work_code"],$value); 
															$path  = ($getImageEdit["pathfile"]) ? $getImageEdit["pathfile"]."/" : "";
														 ?>
														<div class="col-md-4 mt15" style="height: 150px;">
															<div class="fl fs12"><b><?php echo caseTitle("file".$value); ?></b></div>
															<?php if($getImageEdit){ ?>
																<div class="col-md-4 clearb">
																	 	<?php 
																	 		if($getImageEdit["name_types"] !== "pdf"){ 
																	 			echo '<a href="myfile/'.$path.''.$getImageEdit["file_name"].'" data-fancybox="watermark" class="fancybox">
																							<img src="myfile/'.$path.''.$getImageEdit["file_name"].'" alt="" class="img-responsive rounded" style="height: 120px;max-width: 120px;">
																						</a>';
																			}else{
																				echo '<a href="myfile/'.$path.''.$getImageEdit["file_name"].'" data-fancybox="watermark" class="fancybox">
																				<i class="fa fa-file" style="font-size:  50px;color: #555;"></i></a>';
																			} ?>
																	<div class="fs12 cursorPoin cff0000" onclick="delfile('<?php echo $getImageEdit["noti_work_files_id"];?>','<?php echo $path.''.$getImageEdit["file_name"];?>');">
																		<i class="fa fa-trash"></i> ลบไฟล์
																	</div>

																</div>
															<?php } ?>
															<div class="col-md-8 clearb">
																<?php if(!$getImageEdit){ ?>
																	<input type="file" name="filUpload[<?php echo $value;?>]" id="fileUp_<?php echo $num++;?>" class="fl form-control inputFile" 
																	accept="image/gif, image/jpeg, .pdf" onchange="ValidateSingleInput(this);">
																<?php } ?>
															</div>
														</div>
													<?php } ?>
												</div>
											</div>
											<div class="row t_c mt20">
													<input type="submit" class="btn btn-info" value="บันทึกข้อมูล" >
													<span class="btn btn-success" onclick="btnChangStatus('<?php echo $getNoti["noti_work_code"];?>','<?php echo $getNoti["admin_code"];?>')">ส่งตรวจอีกครั้ง</span>
													<input type="reset" class="btn btn-danger" value="ยกเลิก" >
											</div>
									</div>
									<?php }else{ ?>
										<div class="row mb20">
											<div class="col-md-3">
												<div class="row">
													<div class="col-lg-4 t_r"><b>PO :</b></div>
													<div class="col-lg-8"><?php echo $getNoti["po_code"];?></div>
												</div>
												<div class="row mt10">
													<div class="col-lg-4 t_r"><b>ประเภทประกัน :</b></div>
													<div class="col-lg-8"> <?php echo ($getNoti["po_type"] == "บริษัท")? "บริษัท" : "ส่วนบุคคล" ;?></div>
												</div>
												<div class="row mt10">
													<div class="col-lg-4 t_r"><b>ชื่อลูกค้า :</b></div>
													<div class="col-lg-8"><?php echo $getNoti["cus_name"]; ?></div>
												</div>
												<div class="row mt10">
													<div class="col-lg-4 t_r"><b>วันเริ่มคุ้มครอง :</b></div>
													<div class="col-lg-8"><?php echo $getNoti["start_cover_date"]; ?></div>
												</div>
												
												<div class="row mt10">
													<div class="col-lg-4 t_r"><b>ส่วนลดก้อง :</b></div>
													<div class="col-lg-8"><?php echo ($getNoti["discount_cctv"] == 1) ? "มีส่วนลเกล้อง" : "ไม่มี" ;?></div>
												</div>
												<div class="row mt10">
													<div class="col-lg-4 t_r"><b>ของแถม :</b></div>
													<div class="col-lg-8"> <?php echo $getNoti["giftvoucher"]; ?></div>
												</div>
												<div class="row mt10">
														<div class="col-md-4 t_r"><b>การจัดส่งเอกสาร :</b></div>
														<div class="col-md-8"><?php echo ($getNoti["send_type"]) ? $getNoti["send_type"] : "-";?></div>
													</div>
												<?php if($getNoti["send_type"] == "พนักงานจัดส่ง"){?>
													<div class="row mt10">
														<div class="col-md-4 t_r"><b>ที่อยู่ส่งเอกสาร :</b></div>
														<div class="col-md-8"><?php echo ($getNoti["send_addr"]) ? $getNoti["send_addr"] : "-";?></div>
													</div>
												<?php } ?>
												<div class="row mt10">
													<div class="col-lg-4 t_r"><b>หมายเหตุ :</b></div>
													<div class="col-lg-8"><?php echo $getNoti["notes"];?></div>
												</div>
											</div>
											<div class="col-md-3">
												<div class="row mt10">
													<div class="col-lg-4 t_r"><b>แพจเกต :</b></div>
													<div class="col-lg-8"><?php echo $getNoti["package_name"];?></div>
												</div>
												<div class="row mt10">
													<div class="col-lg-4 t_r"><b>ประกันชั้น :</b></div>
													<div class="col-lg-8"><?php echo $getNoti["insurance_type"];?> </div>
												</div>
												<div class="row mt10">
													<div class="col-lg-4 t_r"><b>บริษัทประกัน :</b></div>
													<div class="col-lg-8"><?php echo $getNoti["insuere_company"];?></div>
												</div>
												<div class="row mt10">
													<div class="col-lg-4 t_r"><b>ทุนประกัน :</b></div>
													<div class="col-lg-8 t_r"><?php echo number_format($getNoti["insuere_cost"]);?> <b>บาท</b></div>
												</div>
												<div class="row mt10">
													<div class="col-lg-4 t_r"><b>เบี้ยสุทธิ :</b></div>
													<div class="col-lg-8 t_r"><?php echo number_format($getNoti["netpremium"]);?> <b>บาท</b></div>
												</div>
												<div class="row mt10">
													<div class="col-lg-4 t_r"><b>เบี้ยรวมภาษี :</b></div>
													<div class="col-lg-8 t_r"><?php echo number_format($getNoti["premium"]);?> <b>บาท</b></div>
												</div>
												<div class="row mt10">
													<div class="col-lg-4 t_r"><b>ส่วนลด :</b></div>
													<div class="col-lg-8 t_r"><?php echo number_format($getNoti["discount"]);?> <b>บาท</b></div>
												</div>
												<div class="row mt10">
													<div class="col-lg-4 t_r"><b>รวม :</b></div>
													<div class="col-lg-8 t_r"><?php echo number_format($getNoti["taxamount"]);?> <b>บาท</b></div>
												</div>
											</div>
											<div class="col-md-3">
												<div class="row mt10">
													<div class="col-lg-4 t_r"><b>ผู้แจ้งงาน :</b></div>
													<div class="col-lg-8">
														<div><?php echo $getNoti["personnel_code"]." ". $getNoti["personnel_name"];?></div>
														<div><?php echo $personnel["personnel_email"];?></div>
													</div>
												</div>
												<div class="row mt10">
													<div class="col-lg-4 t_r"><b>สถานะ :</b></div>
													<div class="col-lg-4"><?php echo setStatus($getNoti["status"]);?></div>
												</div>
												<div class="row mt10">
													<div class="col-lg-4 t_r"><b>การใช้งาน :</b></div>
													<div class="col-lg-4"><?php echo ($getNoti["enable"] == 1) ? "ใช้งาน" : "ปิดใช้งาน" ; ?></div>
												</div>
												<?php if($getNoti["insurance_type"] != "พรบ." || $getNoti["status"] != 1){ ?>
												<div class="row mt10">
													<div class="col-lg-4 t_r"><b>ใบคำขอ :</b></div>
													<div class="col-lg-4">
														<?php if($getIR["Request_ID"]){ ?>
															<a href="export_request.php?pocode=<?php echo $pocode;?>" target="_bank" class="cff2da5"><?php echo $getIR["Request_ID"]; ?></a>
														<?php }else{?>
															<span class="cursorPoin cff2da5" onclick="addRequest()"><i class="fa fa-file-pdf-o fwb fs14" aria-hidden="true"></i> สร้าง</span>
														<?php } ?>
													</div>
												</div>
											<?php } ?>

												<?php if($_SESSION["User"]['type'] == "Admin" || $_SESSION["User"]['type'] == "SuperAdmin"){ ?>
												
												<div class="row mt30 t_c">
														<?php if($value["status"] != 1){ ?>
														<span class="fs14 btnselecttitle btn btn-danger" title="ตีกลับ" onclick="changStatus('<?php echo $getNoti["noti_work_id"]?>','<?php echo $numcode; ?>',1);">ตีกลับ</span>  
														<?php }if($value["status"] != 3){ ?>
															<span class="fs14 btnselecttitle btn btn-primary" title="กำลังแจ้งงาน" onclick="changStatus('<?php echo $getNoti["noti_work_id"]?>','<?php echo $numcode; ?>',3);">แจ้งงาน</span>
														<?php }if($value["status"] != 4){ ?>
															<span class="fs14 btnselecttitle btn btn-success" title="แจ้งงานเสร็จสิ้น"onclick="changStatus('<?php echo $getNoti["noti_work_id"]?>','<?php echo $numcode; ?>',4);">เสร็จสิ้น</span>
														<?php } ?>
														<br><br><!-- <span class="fs14 btnselecttitle btn" title="web service" onclick="window.open('http://61.90.142.230/webservice/tvi/index.php?PO_ID=<?php echo $_GET['pocode']; ?>')">TVI</span> -->
														<span class="fs14 btnselecttitle btn" title="web service" onclick="window.open('http://61.90.142.230/webservice/tvi/333auto.php?auto=auto&PO_ID=<?php echo $_GET['pocode']; ?>')">TVI-A</span>  

														
												</div>
												<?php } ?>
											</div>
											<?php if($_SESSION["User"]['type'] == "Admin" || $_SESSION["User"]['type'] == "SuperAdmin"){ ?>
												<div class="col-md-3">
													<div style="background-color: #ffffed; padding: 10px;">
														<p class="cff0000"><b>หมายเหตุ</b></p>
															<div id="listMent">
														<?php foreach ($getNotiComment as $key => $value) { ?>
																<div style="color: #b0b0b0;font-size: 11px;"> <span ><?php echo $value["datetime"] ?></span> | <?php echo $value["comment_byname"] ?> </div>
																<div class="ml10 fs12 c1641ff"> >>> <?php echo $value["comment"] ; ?></div>
														<?php } ?>
															</div>
														<?php if($_SESSION["User"]['type'] == "Admin" || $_SESSION["User"]['type'] == "SuperAdmin"){ ?>
															<div class="mt15">
																<input type="text"  name="comment" id="comment"  class="form-control formInput fs12 mb5">
																<input type="button" class="btn btn-info" value="send"   onclick="btnComments('<?php echo $numcode; ?>')">
															</div>
														<?php } ?>
													</div>
												</div>
											<?php } ?>
										</div>
									<?php } ?>
								xxxxx</div>
					</div>
					
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="panel">
							<div class="panel-heading">
								<h4 class="panel-title"><b>ข้อมูลการตรวจ</b></h4>
							</div>
							<div class="panel-body fs12">
								<div class="row">
									<div class="col-md-9">
										<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
										<?php 
											foreach ($getCheck as $key => $value) { 
												$datakeyword = unserialize($value["keyword"]);
												$datadata = unserialize($value["data"]);
												$datachecklists = unserialize($value["checklists"]);
												$datacomments = unserialize($value["comments"]);
											?>

										  <div class="panel panel-default">
										    <div class="panel-heading" role="tab" id="heading<?php echo $key ?>">
										      <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $key ?>" style="color: #ff2da5;" aria-expanded="true" aria-controls="collapseOne">
										     		<h4 class="panel-title">
										          :: ครั้งที่ <?php echo count($getCheck) - $key ?> | <span class="fs12"><?php echo date("d-m-Y H:i:s",strtotime($value["datetime"])); ?> </span> | <span class="fs12"><?php echo $value["check_by"]; ?></span> :: คลิก
										      	</h4>
										      </a>
										    </div>
										    <div id="collapse<?php echo $key ?>" class="panel-collapse collapse " role="tabpanel" aria-labelledby="heading<?php echo $key ?>">
										      <div class="panel-body">
										       	<div class="fs12 mt15 cff0000"><b>หมายเหตุ QC ตรวจงาน : </b><?php echo $value["notes"]?></div>
										       	<table class="table table-hover table-striped table-bordered">
															<thead>
																<tr>
																	<th class="t_r" style="width: 20%;">รายการ</th>
																	<th>ข้อมูล</th>
																	<th class="t_c" style="width: 15%;">ตรวจข้อมูล</th>
																	<th class="t_c" >หมายเหตุ</th>
																</tr>
															</thead>
															<tbody>
																<?php 
																	$dataDetail = "";
																	foreach ($datakeyword as $key => $value) {
																		$file = strpos($value,"file");
																		if($file !== false){ ?>
																	<?php	}else{ ?>
																			<tr>
																				<td class="t_r"><b><?php echo caseTitle($value);?></b></td>
																				<td class="c1641ff"><?php echo $datadata[$key] ? $datadata[$key] :"-";?></td>
																				<td class="t_c"><?php echo ($datachecklists[$key]) ? "ผ่าน" : "ไม่ผ่าน";?> </td>
																				<td class="t_c"><?php echo $datacomments[$key] ? $datacomments[$key] : "-"; ?></td>
																			</tr>
																<?php		}
																 } ?>
																<?php 
																	foreach ($getFile as $key => $value) { 
																		$path  = ($value["pathfile"]) ? $value["pathfile"]."/" : "";
																		$fileType = explode(".", $value["file_name"]);
																	?>
																	<tr>
																		<td class="t_r"><b><?php echo caseTitle("file".$value["file_type"]);?> </b></td>
																		<td class="c1641ff">
																			<?php if($fileType[1] == "png" || $fileType[1] == "jpg"){ ?>
																				<a href="myfile/<?php echo $path."".$value["file_name"]; ?>" data-fancybox="watermark" class="fancybox">
																					<img src="myfile/<?php echo $path."".$value["file_name"]; ?>" alt="" class="img-responsive rounded" style="width: 150px;max-height: 150px;">
																				</a>
																			<?php }else{?>
																				<a href="myfile/<?php echo $path."".$value["file_name"]; ?>" data-fancybox="watermark" class="fancybox">
																					<?php echo $value["file_name"]; ?>
																				</a>
																			<?php } ?>
																		</td>
																		<td class="t_c"></td>
																		<td class="t_c"></td>
																	</tr>
																<?php } ?>
															</tbody>
														</table>
										      </div>
										    </div>
										  </div>
									<?php } ?>
										</div>
									</div>
									<?php if($_SESSION["User"]['type'] == "Sale" || $_SESSION["User"]['type'] == "SuperAdmin"){ ?>
										<div class="col-md-3">
											<div style="background-color: #ffffed; padding: 10px;">
												<p class="cff0000"><b>หมายเหตุ</b></p>
													<div id="listMent">
												<?php foreach ($getNotiComment as $key => $value) { ?>
														<div style="color: #b0b0b0;font-size: 11px;"> <span ><?php echo $value["datetime"] ?></span> | <?php echo $value["comment_byname"] ?> </div>
														<div class="ml10 fs12 c1641ff"> >>> <?php echo $value["comment"] ; ?></div>
												<?php } ?>
													</div>
											</div>

										</div>
									<?php } ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
<?php include "include/inc_footer.php"; ?> 
<script src="fancybox/dist/jquery.fancybox.min.js"></script>
<style type="text/css">
.panel .table > thead > tr > th:last-child,
.panel .table > tbody > tr > td:last-child{
	padding-left: 0px
}
</style>
<script type="text/javascript">
 $("#btn_send_order").click(function() {
	var poCode = $("#po_code").val();
    $.ajax({ 
			url: 'include/inc_action_ms.php',
			type:'POST',
			dataType: 'json',
			data: {action: 'searchPO', pocode:poCode},
			success:function(rs){ 
					var num = rs.length;
					var details = "";
					for (var i = 0; i < num; i++){
						var d = new Date(rs[i].Coverage_Start_Date.date);
						$("#po_code").val(rs[i].PO_ID);
						$("#cus_name").val(rs[i].Title_Name+" "+rs[i].FName+" "+rs[i].LName);
						$("#insuere_cost").val(rs[i].Capital);
						initials = rs[i].Insurer_Initials;
						cusType = rs[i].cus_type;
						if(cusType == "C"){
							$("#po_type_c").prop("checked", true);
						}else{
							$("#po_type_p").prop("checked", true);
						}
						$("#packageID").val(rs[i].Insurance_Package_ID);
						$("#packageName").val(rs[i].Insurance_Package_Name);
						$("#insuere_company").val(initials);
						
						if(rs[i].Net_Premium){
							$("#netpremium").val(rs[i].Net_Premium);
						}else{
							$("#netpremium").val(rs[i].Compulsory);
						}
						$("#discount").val(rs[i].Discount);
						$("#premium").val(rs[i].Total_Premium);
						$("#taxamount").val(rs[i].Premium_After_Disc);
						$("#giftvoucher").val(rs[i].Premium_Desc);
						$("#InsuranceType").val(rs[i].Insurance_Name);

						if(rs[i].Marine_Reference){
							$(".boxmarine").show();
							$("#marine").val(rs[i].Marine_Reference);
							$("#marinetxt").html(rs[i].Marine_Reference);
							$('#caseMarine').prop('checked', true);
						}else{
							$(".boxmarine").hide();
							$("#marine").val("");
							$('#caseMarine').prop('checked', false);
						}

						var now = new Date();
						var day = ("0" + d.getDate()).slice(-2);
						var month = ("0" + (d.getMonth() + 1)).slice(-2);
						var today = d.getFullYear()+"-"+month+"-"+day ;
						$("#start_cover_date").val(today);
						
					}
						
				}
		});
});
function getboxgift(){
	var gift = $("#giftvoucher").val();
	var po = $("#po_code_search").val();
	if(gift){
		 $(".boxgift").show();
		 $("#giftvoucher_po").val(po);
	}else{
		$(".boxgift").hide();
	}
	// console.log(gift);
}
	$(".rd_chk").click(function() {
		val = $('.rd_chk:checked').val();
		name = $(this).attr("name");
		repName = name.replace("chk_", "comment_");
		if(val == "0"){
				$("#comment_"+repName).show(); 
			}else{
				$("#comment_"+repName).hide();
			}
			
	});
$(document).ready(function() {
	$(".btnSendAdd").click(function() { 
		status = $("#mainStatus").val();
		if(status != 0){
			if( confirm("ตรวจสอบข้อมูลสมบูรณ์ ต้องการบันทึกผลการประเมิน !!!") ){
				// console.log("btnSendAdd");
				$("#frmsendAddlistchk").submit();
			}
		}else{
			alert("กรุณาเลือกสถานะ!!!");
		}
		
	});
});

function ValidateSingleInput(oInput) {
var _validFile = [".jpg", ".jpeg", ".pdf",".png",];    
    if (oInput.type == "file") {
        var sFileName = oInput.value;
         if (sFileName.length > 0) {
            var blnValid = false;
            for (var j = 0; j < _validFile.length; j++) {
                var sCurExtension = _validFile[j];
                if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                    blnValid = true;
                    break;
                }
            }
             
            if (!blnValid) {
                alert("กรุณาเลือกไฟล์ ที่มีนามสกุล PNG | JPG | PDF เท่านั้น" );
                oInput.value = "";
                return false;
            }
        }
    }
    return true;
}

	function funChk(name) {
		// console.log("input[name='check["+name+"][chk]']:checked");
		val = $("input[name='check["+name+"][chk]']:checked").val(); 
		// console.log(val);
		if(val == 0){
				$("#comment_"+name).show();
			}else{
				$("#comment_"+name).hide();
			}

	}
function changStatus(id, codes, status){
	// console.log(codes);
	if(status == 1){
		txt = "ต้องการยกเลิก รายการแจ้งงานนี้";
	}else if(status == 3){
		txt = "ยืนยันการรับงาน";
	}else if(status == 4){
		txt = "ยืนยันการแจ้งงานสมบูรณ์";
	}
	if(confirm(txt)){
		$.ajax({ 
			url: 'include/inc_action_chk.php',
			type:'POST',
			data: {action: 'upDateStatus', id:id, code:codes, status:status},
			success:function(rs){ 
				if(status == 1){
					window.location = "ordersadmins.php";
				}else{
					window.location.reload(true);
				}
			}
		});
	}
	
}
function addRequest(){ 
	pocode = $("#pocode").val();
	if( confirm("สร้างใบคำขอ?")){
		$.ajax({ 
			url: 'include/inc_action_chk.php',
			type:'POST',
			data: {action: 'addRequest', pocode:pocode},
			success:function(rs){
				window.location.reload(true);
			}
		});
	}
}

function btnComments(code){
	var val = code;
	// console.log(val);
	mentTxt = $("#comment").val();
	// console.log("#listMent_"+mentTxt);
	if(mentTxt != 0){
	 $.ajax({ 
			url: 'include/inc_action_chk.php',
			type:'POST',
			data: {action: 'addComments', noti_work_code:code, comment:mentTxt},
			success:function(rs){
				console.log(rs);
				$("#listMent").empty();
				$("#comment").val("");
				$("#listMent").append(rs);
			}
		});
	}else{
		alert("กรุณาเลือก Comment");
	}
}

function btnChangStatus(code,admin_code){
	insuere = $("#insuere_company").val();
	if( confirm("คุณต้องการส่งงานอีกครั้ง")){
			$.ajax({ 
				url: 'include/inc_action_chk.php',
				type:'POST',
				data: {action: 'btnChangStatus', code:code, admin_code:admin_code, insuere:insuere},
				success:function(rs){  
					if(rs){
						alert("เปลี่ยนสถานะเรียบร้อย กรุณารอการตรวจเช็ค");
						window.location.href = 'sendorders.php';
					}else{
						alert("ไม่สามารถเปลี่ยนสถานะได้ กรุณาลองใหม่อีกครั้ง");
						window.location.reload(true);
					}
				}
			});
	}

}

function delfile(code, filetype, name){
	// console.log(code+" - "+filetype+ "" + name );
	if( confirm("ต้องการลบไฟล์ !!!")){
		$.ajax({ 
			url: 'include/inc_action_chk.php',
			type:'POST',
			data: {action: 'delfileEdit', code:code, name:name},
			success:function(rs){ 
				// console.log(rs);
				if(rs == 0){
					alert("ไม่สามารถลบไฟล์ได้ กรุณาลองใหม่");
				}else{
					alert("ลบไฟล์เรียบร้อย");
					window.location.reload(true);
				}
			}
		});
	}
}
$( '[data-fancybox="watermark"]' ).fancybox({
  buttons : [
    'download',
    'zoom',
    'close'
  ],
  protect : false,
  animationEffect : "zoom-in-out",
  transitionEffect : "fade",
  zoomOpacity : "auto",
  animationDuration : 500,
  zoomType: 'innerzoom',

});
</script>