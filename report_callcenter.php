<?php 
session_start();
ini_set('max_execution_time', 186);
ini_set('memory_limit', '2048M');
$UserCode = $_SESSION["User"]['UserCode'];
$userType = $_SESSION["User"]['type'];

if(!isset($UserCode)){
	echo "<script>alert('กรุณาเข้าสู่ระบบ!!!');</script>";
	echo '<META http-equiv="refresh" content="0;URL=login.php">';
	exit();
}

header('Content-Type: text/html; charset=utf-8');
include "include/inc_header.php"; 
include "include/inc_menu.php"; 
include "callcenters/inc_function_call.php";
// include('qrcode/qrcode.class.php');

$year = date("Y");
$_GET["txtse"] = ($_GET["txtse"]) ? $_GET["txtse"] : date("Y-m");
if($_GET["txtse"]){
	$getCallcenterBySubject = getCallcenterBySubject($_GET["txtse"]);
	$getCallcenterByEmp = getCallcenterByEmp($_GET["txtse"]);
	$getEmpofMonth = getEmpofMonth($_GET["txtse"]);
}
?>
<div class="main">
	<div class="">
		<div class="p20">
			<div class="row">
				<div class="col-md-12">
					<div class="panel">
						<div class="panel-heading">
							<h4 class="panel-title"><b>รายงานยอดขาย Callcenter เดือน <?php echo date("F");?></b></h4>
						</div>
						<div class="panel-body fs14">
							<div class="row">
								<div class="col-md-4 ">
									<span class="fs18 cff2da5 mr15">เดือน</span>
									<select name="mounth" id="mounthOpt" class="form-control formInput2 dib" style="width: 80%" >
										<option value="">:: กรุณาเลือก ::</option>
										<?php 
										$dateLast = ( date("Y") - 1)."-".date("m");
										for($i=1; $i<=12; $i++) { 
											$txtM = "+".$i." month";
											$dateForValue = date("Y-m",strtotime($txtM, strtotime($dateLast)));
											?>
											<option value="<?php echo $dateForValue ;?>" <?php if($_GET["txtse"] ==  $dateForValue){ echo "selected"; } ?>>
												<?php echo date("Y m F",strtotime($dateForValue)) ?>
											</option>
										<?php } ?>
									</select>
								</div>
								<div class="col-md-4">
									<sapn class="btn btn-info" onclick="btnExport()">Export</sapn>
								</div>
							</div>
							<div class="row mt20">
								<div class="col-md-12">
									<?php if($getCallcenterBySubject){?>
										<table class="table table-hover table-bordered" id="indextable" >
											<thead>
												<tr class="bgbaf4bc">
													<th class="t_c">#</th>
													<th class="t_c">เรื่องทีติดต่อ</th>
													<th class="t_c">จำนวนสาย</th>
												</tr>
											</thead>
											<tbody id="table_data" class="table-list fs13">
												<?php 
													$sumAll = 0;
													$i = 1;
													foreach ($getCallcenterBySubject as $key => $value) {
														$sumAll +=  $value["sumCall"];
												?>
													<tr>
														<td class="t_c"><?php echo $i++; ?></td>
														<td class="t_l"><?php echo $value["Call_Subject"]; ?></td>
														<td class="t_c fwb "><?php echo $value["sumCall"]; ?></td>
													</tr>
												<?php } ?>
												<tfoot>
													<tr class="fwb fs16 bgfffbd8">
														<th class="t_c" colspan="2">รวม</th>
														<td class="t_c fwb "><?php echo $sumAll; ?></td>
													</tr>
												</tfoot>
											</table>
										<?php } ?>
									</div>
							</div>
							<div class="row mt20">
								<div class="col-md-12">
									<?php if($getCallcenterByEmp){?>
										<table class="table table-hover table-bordered" id="indextable" >
											<thead>
												<tr class="bgbaf4bc">
													<th class="t_c">ผู้รับสาย</th>
													<?php foreach ($getEmpofMonth as $key => $value) { ?>
														<th class="t_c"><?php echo $value["Create_By"]." ".$value["User_FName"] ?></th>
													<?php } ?>
												</tr>
											</thead>
											<tbody id="table_data" class="table-list fs13">
												<?php 
													$sumAll = 0;
													$i = 1;
													foreach ($getCallcenterByEmp as $key => $value) { 
												?>
													<tr>
														<td class="t_c fwb "><?php echo $value["dateList"]->format("d/m/Y"); ?></td>
														<?php foreach ($getEmpofMonth as $key => $valueEmp) { ?>
															<td class="t_c"><?php echo $value[$valueEmp["Create_By"]]; ?></td>
														<?php } ?>
														
													</tr>
												<?php } ?>
												<tfoot>
													<tr class="fwb fs16 bgfffbd8">
														<th class="t_c" >รวม</th>
														<?php foreach ($getEmpofMonth as $key => $valueEmp) { 
															$first_names = array_column($getCallcenterByEmp, $valueEmp["Create_By"]);
															$sumArr = array_sum($first_names);
														?>															
															<td class="t_c fwb "><?php echo $sumArr; ?></td>
														<?php } ?>
													</tr>
												</tfoot>
											</table>
										<?php } ?>
								</div>
							</div>
							<div class="row mt20">
								<div class="col-md-6">
									<canvas id="myChart" style="max-height: 450px; max-width: 890px"></canvas>
								</div>
								<div class="col-md-6">
									<canvas id="lineChart" style="max-height: 450px; max-width: 890px"></canvas>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php include "include/inc_footer.php"; ?> 
	<?php include "include/inc_function_tablesort.php"; ?>
<script src="js/Chart.bundle.js"></script>
<script src="js/utils.js"></script>
<script type="text/javascript">
	
	function btnExport(){
		var mounthOpt = $("#mounthOpt").val();
		if(mounthOpt){
			// window.open("callcenters/export_inform.php?date="+date+"&insurers="+insurers);
			window.location.href = "callcenters/export_callcanter.php?date="+mounthOpt;
		}
	}
	
	$("#mounthOpt").on('change', function() {
		if(this.value){
			window.location.href = "report_callcenter.php?txtse="+this.value;
		}

	});

	function random_rgba() {
		var o = Math.round, r = Math.random, s = 255;
		return 'rgba(' + o(r()*s) + ',' + o(r()*s) + ',' + o(r()*s) + ',' + r().toFixed(2) + ')';
	}

	function getRandomColor() {
	    var letters = '0123456789ABCDEF'.split('');
	    var color = '#';
	    for (var i = 0; i < 6; i++ ) {
	        color += letters[Math.floor(Math.random() * 16)];
	    }
	    return color;
	}


	window.onload = function() {
		var ctx = document.getElementById("myChart").getContext('2d');
		var data, options;
		var mounth = $("#mounthOpt").val();
		$.ajax({ 
			url: 'callcenters/inc_action_call.php',
			type:'POST',
			dataType: 'json',
			data: {action: 'reportCharCallcenterOne', mounth:mounth},
			success:function(rs){  
				var labelArray = [];
				var series = [];
				var coloR = [];
				var dynamicColors = function() {
		            var r = Math.floor(Math.random() * 255);
		            var g = Math.floor(Math.random() * 150);
		            var b = Math.floor(Math.random() * 100);
		            return "rgb(" + r + "," + g + "," + b + ", 0.5)";
		        };


				for (var i in rs){
					labelArray.push(rs[i].subject);
					series.push(rs[i].sumCase);
					coloR.push(dynamicColors());
				}

				var myChart = new Chart(ctx, {
					type: 'horizontalBar',
					data: {
						labels: labelArray,
						datasets: [{
							label: 'แยกตามเรืองติดต่อ',
							data: series,
							backgroundColor : coloR,
							borderColor: coloR,
							hoverBorderColor: coloR,
							borderWidth: 1,
							fill: true,
							pointRadius: 8,
							pointHoverRadius: 15,
						}]
					},
					options: {
						responsive: true,
						elements: {
							point: {
								pointStyle: 'rectRot'
							}
						},
						scales: {
							yAxes: [{
								ticks: {
									beginAtZero: true
								},
								scaleLabel: {
									display: true,
									labelString: 'เรื่องติดต่อ'
								}
							}],
							xAxes: [{
								display: true,
								scaleLabel: {
									display: true,
									labelString: 'จำนวน'
								}
							}],
						}
					}
				});
			}
		});


		var ctxL = document.getElementById("lineChart").getContext('2d');
		var data, options;
		var mounth = $("#mounthOpt").val();
		$.ajax({ 
			url: 'callcenters/inc_action_call.php',
			type:'POST',
			dataType: 'json',
			data: {action: 'reportCharBydate', mounth:mounth},
			success:function(rs){
				console.log(rs);
				var labelArray = [];
				var data1 = [];
				var data2 = [];
				var data3 = [];
				var coloR = [];
				var dynamicColors = function() {
		            var r = Math.floor(Math.random() * 255);
		            var g = Math.floor(Math.random() * 150);
		            var b = Math.floor(Math.random() * 100);
		            return "rgb(" + r + "," + g + "," + b + ", 0.5)";
		        };


				for (var i in rs){
					labelArray.push(rs[i].date);
					data1.push(rs[i].emp1);
					data2.push(rs[i].emp2);
					data3.push(rs[i].emp3);
					coloR.push(dynamicColors());
				}  
				var myLineChart = new Chart(ctxL, {
					type: 'line',
					data: {
						labels: labelArray,
						datasets: [{
							label: "อัมพกา ปัญญาบุญ",
							data: data1,
							backgroundColor: ['rgba(105, 0, 132, .2)',],
							borderColor: ['rgba(200, 99, 132, .7)',],
							borderWidth: 2
						},
						{
							label: "ธนวิชญ์ แก้วแกมทอง",
							data: data2,
							backgroundColor: ['rgba(0, 137, 132, .2)',],
							borderColor: ['rgba(0, 10, 130, .7)',],
							borderWidth: 2
						},
						{
							label: "อารดา อาระเบียร์",
							data: data3,
							backgroundColor: ['rgba(136, 230, 127, 1)',],
							borderColor: ['rgba(136, 230, 127, 1)',],
							borderWidth: 2
						}
						]
					},
					options: {
						responsive: true
					}
				});
			}
		});

	};


	
</script>