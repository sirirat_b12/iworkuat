
<?php
// require '../backoffice/init.php';
date_default_timezone_set('Asia/Bangkok');
header('Content-Type: text/html; charset=utf-8');

$strExcelFileName = "reportdelivery".date('YmdHis').".xls";

header("Content-Type: application/vnd.ms-excel; name=\"$strExcelFileName\"");
header("Content-Disposition: inline; filename=\"$strExcelFileName\"");
header("Pragma:no-cache");

include "../inc_config.php"; 
include "inc_function_delivery.php"; 
// $getPOCancelReport = getPOCancelReport($_GET['txtse']);
$getDeliveryBackAll = getDeliveryBackAll("", $_GET["txtse"],"", "");
// echo "<pre>".print_r($getDeliveryBackAll,1)."</pre>";
// exit();

?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>
    <div id="SiXhEaD_Excel" align=center x:publishsource="Excel">
        <table x:str border=1 cellpadding=1 cellspacing=1 width=100% style="border-collapse:collapse">
            <tr>
                <th valign="middle">PO</th>
                <th valign="t_c">ประเภท</th>
                <th valign="t_c">วันที่รับเอกสาร</th>
                <th valign="t_c">พนักงานขาย</th>
                <th valign="t_c">Tracking Number</th>
                <th valign="t_c">ประเภทจัดส่ง</th>
                <th valign="t_c">ที่อยู่</th>
                <th valign="t_c">เหตุผลในการตีกลับ</th>
                <th valign="t_c">ผลการตรวจสอบ</th>
                <th valign="t_c">สถานะ</th>
                <th valign="t_c">ผู้ตรวจสอบ/ผู้แจ้ง</th>
                <th valign="t_c">เวลาทำรายการ</th>
                <th valign="t_c">หมายเหตุ</th>
                
            </tr>
            <?php 
                $i = 1;
                foreach ($getDeliveryBackAll as $key => $objResult) {
                    if($objResult["Addr2"]){
                        $eAddr = explode("|", $objResult["Addr2"]);
                        if($eAddr[0]){
                            $addr = "เลขที่ ".$eAddr[0];
                        }if($eAddr[1]){
                            $addr .= " อาคาร".$eAddr[1];
                        }if($eAddr[2]){
                            $addr .= " หมู่ที่ ".$eAddr[2];
                        }if($eAddr[3]){
                            $addr .= " ซอย ".$eAddr[3];
                        }if($eAddr[4]){
                            $addr .= " ถนน ".$eAddr[4];
                        }
                    }else{
                        $addr = $objResult["Addr1"];
                    }

                    if($objResult["delivery_back_type"]=="policy"){
                        $type = "กรมธรรม์";
                    }else if($objResult["delivery_back_type"]=="act"){
                        $type = "พรบ.";
                    }else if($objResult["delivery_back_type"]=="noti"){
                        $type = "ใบเตือน";
                    }else if($objResult["delivery_back_type"]=="endorse"){
                        $type = "สลักหลัง";
                    }else if($objResult["delivery_back_type"]=="giftcard"){
                        $type = "บัตรกำนัล";
                    }
                    
                    $addr = $addr." <br>ตำบล/เขต ".$objResult["Subdistrict_Name_TH"]." อำเภอ/เขต ".$objResult["District_Name_TH"]." จังหวัด ".$objResult["Province_Name_TH"]." ".$objResult["Post_Code"]; 
            ?>
                    
                     <tr>
                        <td valign="fwb t_c"><div valign="cf40053 "><?php echo $objResult["po_id"]; ?></div></td>
                        <td valign="fwb t_c nowrap c2457ff"><?php echo $type; ?></td>
                        <td valign="fwb t_c nowrap cff2da5"><?php echo $objResult["delivery_back_date"]->format("d-m-Y"); ?></td>
                        <td valign="nowrap"><?php echo $objResult["User_FName"]." ".$objResult["User_LName"]; ?></td>
                        <td valign="fwb t_c"><div><?php echo $objResult["tracking"]; ?></div></td>
                        <td valign="middle"><div valign="cf40053 "><?php echo $objResult["delivery_Type"]; ?></div></td>
                        <td valign="middle"><?php echo $addr; ?></td>
                        <td valign="middle"><?php echo $objResult["reason"]; ?></td>
                        <td valign="middle"><?php echo $objResult["result"]; ?></td>
                        <td valign="middle"><?php echo $objResult["status"]; ?></td>
                        <td valign="middle"><div><?php echo $objResult["DBCreate_By"]; ?></div> </td>
                        <td valign="middle"><div><?php echo $objResult["DBCreate_Date"]->format("d-m-Y H:i:s"); ?></div></td>
                        <td valign="middle"><?php echo $objResult["note"]; ?></td>
                    </tr>
                    <?php
                }
            ?>
        </table>
    </div>
    <script>
        window.onbeforeunload = function(){return false;};
        setTimeout(function(){window.close();}, 10000);
    </script>
</body>
</html>
