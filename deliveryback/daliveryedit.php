<?php
session_start();
date_default_timezone_set("Asia/Bangkok");
header('Content-Type: text/html; charset=utf-8');

include "../inc_config.php";
include "inc_function_delivery.php";

$delivery_back_id = $_POST["delivery_back_id"];
$getDelivery = getDeliveryBackForEdit($delivery_back_id);
// echo "<pre>".print_r($getDelivery,1)."</pre>";

if($getDelivery["Addr2"]){
	$eAddr = explode("|", $getDelivery["Addr2"]);
	if($eAddr[0]){
		$addr = "เลขที่ ".$eAddr[0];
	}if($eAddr[1]){
		$addr .= " อาคาร".$eAddr[1];
	}if($eAddr[2]){
		$addr .= " หมู่ที่ ".$eAddr[2];
	}if($eAddr[3]){
		$addr .= " ซอย ".$eAddr[3];
	}if($eAddr[4]){
		$addr .= " ถนน ".$eAddr[4];
	}
}else{
	$addr = $getDelivery["Addr1"];
}
$addr = $addr." ตำบล/เขต ".$getDelivery["Subdistrict_Name_TH"]." อำเภอ/เขต ".$getDelivery["District_Name_TH"]." จัวหวัด ".$getDelivery["Province_Name_TH"]." ".$getDelivery["Post_Code"];
?>

<div class="main-content p20 fs12">
	<form action="deliveryback/inc_action_delivery.php" method="POST" id="form_send_order" >
		<input type="hidden" value="editDelivery" name="action">
		<input type="hidden" value="<?php echo $delivery_back_id; ?>" name="delivery_back_id">
		<div class="row">
			<div class="col-md-12">
				<span class="fwb fs16">ประเภทจดหมาย <?php echo ($getDelivery["delivery_back_type"]=="policy") ? "กรมธรรม์" : "ใบเตือน"; ?></span>
			</div>
			<div class="col-md-12 mt10">
				<p class="fwb">PO *</p>
				<input type="text" value="<?php echo $getDelivery["PO_ID"]; ?>" class="form-control formInput2" name="PO_ID" readonly>
			</div>
			<div class="col-md-12 mt10">
				<p class="fwb">ลูกค้า *</p>
				<input type="text" value="<?php echo $getDelivery["Customer_FName"]." ".$getDelivery["Customer_LName"]; ?>" class="form-control formInput2" name="addr" readonly >
			</div>
			<div class="col-md-12 mt10">
				<p class="fwb">ผู้ขาย *</p>
				<input type="text" value="<?php echo $getDelivery["User_FName"]." ".$getDelivery["User_LName"]; ?>" class="form-control formInput2" name="addr" readonly >
			</div>
			<div class="col-md-12 mt10">
				<p class="fwb">ที่อยู่ *</p>
				<input type="text" value="<?php echo $addr; ?>" class="form-control formInput2" name="addr" readonly >
			</div>
			<?php if($_SESSION["User"]['type'] != "Sale"){?>
			<div class="col-md-12 mt10">
				<p class="fwb">วันที่รับเอกสาร *</p>
				<input type="date" value="<?php echo date("Y-m-d"); ?>" class="form-control formInput2" name="delivery_back_date" >
			</div>
			<div class="col-md-12 mt10">
				<p class="fwb">Tracking Number *</p>
				<input type="text" value="<?php echo $getDelivery["Reference_No"]; ?>" class="form-control formInput2" name="tracking" >
			</div>
			<div class="col-md-12 mt10">
				<p class="fwb">ประเภทการส่ง *</p>
				<select name="delivery_Type" id="delivery_Type" class="form-control formInput2" required>
					<option value="">กรุณาเลือก</option>
					<option value="J&T EXPRESS" <?php if($getDelivery["delivery_Type"] == "J&T EXPRESS"){ echo "selected"; } ?>>J&T EXPRESS</option>
					<option value="ไปรษณีย์" <?php if($getDelivery["delivery_Type"] == "ไปรษณีย์"){ echo "selected"; } ?>>ไปรษณีย์</option>
				</select>
			</div>
			<div class="col-md-12 mt10">
				<p class="fwb">เหตุผลในการตีกลับ *</p>
				<select name="reason" id="reason" class="form-control formInput2" required>
					<option value="">กรุณาเลือก</option>
					<option value="จ่าหน้าไม่ชัดเจน" <?php if($getDelivery["reason"] == "จ่าหน้าไม่ชัดเจน"){ echo "selected"; } ?>>จ่าหน้าไม่ชัดเจน</option>
					<option value="ไม่มีเลขที่บ้านตามจ่าหน้า" <?php if($getDelivery["reason"] == "ไม่มีเลขที่บ้านตามจ่าหน้า"){ echo "selected"; } ?>>ไม่มีเลขที่บ้านตามจ่าหน้า</option>
					<option value="ไม่ยอมรับ" <?php if($getDelivery["reason"] == "ไม่ยอมรับ"){ echo "selected"; } ?>>ไม่ยอมรับ</option>
					<option value="ไม่มีผู้รับตามจ่าหน้า" <?php if($getDelivery["reason"] == "ไม่มีผู้รับตามจ่าหน้า"){ echo "selected"; } ?>>ไม่มีผู้รับตามจ่าหน้า</option>
					<option value="ไม่มารับภายในกำหนด" <?php if($getDelivery["reason"] == "ไม่มารับภายในกำหนด"){ echo "selected"; } ?>>ไม่มารับภายในกำหนด</option>
					<option value="เลิกกิจการ" <?php if($getDelivery["reason"] == "เลิกกิจการ"){ echo "selected"; } ?>>เลิกกิจการ</option>
					<option value="ย้ายไม่ทราบที่อยู่ใหม่" <?php if($getDelivery["reason"] == "ย้ายไม่ทราบที่อยู่ใหม่"){ echo "selected"; } ?>>ย้ายไม่ทราบที่อยู่ใหม่</option>
					<option value="ติดต่อลูกค้าไม่ได้" <?php if($getDelivery["reason"] == "ติดต่อลูกค้าไม่ได้"){ echo "selected"; } ?>>ติดต่อลูกค้าไม่ได้</option>
					<option value="ลูกค้าปฏิเสธการรับ" <?php if($getDelivery["reason"] == "ลูกค้าปฏิเสธการรับ"){ echo "selected"; } ?>>ลูกค้าปฏิเสธการรับ</option>
					<option value="เบอร์ระงับการใช้งาน" <?php if($getDelivery["reason"] == "เบอร์ระงับการใช้งาน"){ echo "selected"; } ?>>เบอร์ระงับการใช้งาน</option>
					<option value="ปิดเครื่อง" <?php if($getDelivery["reason"] == "ปิดเครื่อง"){ echo "selected"; } ?>>ปิดเครื่อง</option>
					<option value="ผู้รับย้ายที่อยู่" <?php if($getDelivery["reason"] == "ผู้รับย้ายที่อยู่"){ echo "selected"; } ?>>ผู้รับย้ายที่อยู่</option>
					<option value="อื่นๆ" <?php if($getDelivery["reason"] == "อื่นๆ"){ echo "selected"; } ?>>อื่นๆ</option>
				</select>
			</div>
			<div class="col-md-12 mt10">
				<p class="fwb">สถานะ *</p>
				<select name="status" id="status" class="form-control formInput2" required>		
					<option value="">กรุณาเลือก</option>
					<option value="แอดมินรับเอกสารตีกลับ" <?php if($getDelivery["status"] == "แอดมินรับเอกสารตีกลับ"){ echo "selected"; } ?>>แอดมินรับเอกสารตีกลับ</option>
					<option value="แจ้งSalesรับทราบ" <?php if($getDelivery["status"] == "แจ้งSalesรับทราบ"){ echo "selected"; } ?>>แจ้งSales รับทราบ</option>
					<option value="รอSalesตรวจสอบข้อมูลอีกครั้ง" <?php if($getDelivery["status"] == "รอSalesตรวจสอบข้อมูลอีกครั้ง"){ echo "selected"; } ?>>รอ Sales ตรวจสอบข้อมูลอีกครั้ง</option>
					<option value="รอแอดมินจัดส่งให้ใหม่" <?php if($getDelivery["status"] == "รอแอดมินจัดส่งให้ใหม่"){ echo "selected"; } ?>>รอแอดมินจัดส่งให้ใหม่</option>
					<option value="กำลังจัดส่ง" <?php if($getDelivery["status"] == "กำลังจัดส่ง"){ echo "selected"; } ?>>กำลังจัดส่ง</option>
					<option value="ลูกค้าติดต่อแจ้งที่อยู่แล้ว" <?php if($getDelivery["status"] == "ลูกค้าติดต่อแจ้งที่อยู่แล้ว"){ echo "selected"; } ?>>ลูกค้าติดต่อแจ้งที่อยู่แล้ว</option>
					<option value="ไม่สามาถรถติดตามลูกค้าได้" <?php if($getDelivery["status"] == "ไม่สามาถรถติดตามลูกค้าได้"){ echo "selected"; } ?>>ไม่สามาถรถติดตามลูกค้าได้</option>
				</select>
			</div>
			<?php }elseif ($_SESSION["User"]['type'] == "Sale") { ?>
				<div class="col-md-12 mt10">
					<p class="fwb">สถานะ *</p>
					<select name="status" id="status" class="form-control formInput2" required>		
						<option value="">กรุณาเลือก</option>
						<option value="รอแอดมินจัดส่งให้ใหม่">รอแอดมินจัดส่งให้ใหม่</option>
						<option value="ไม่สามาถรถติดตามลูกค้าได้">ไม่สามาถรถติดตามลูกค้าได้</option>
						<option value="ไม่ต้องจัดส่ง ติดต่อลูกค้าแล้ว">ไม่ต้องจัดส่ง ติดต่อลูกค้าแล้ว</option>
					</select>
				</div>
			<?php } ?>
			<div class="col-md-12 mt10">
				<p class="fwb">ผลการตรวจสอบ *</p>
				<input type="text" class="form-control formInput2" name="result" value="<?php echo $getDelivery["result"]; ?>">
			</div>
			<div class="col-md-12 mt10">
				<p class="fwb">หมายเหตุ *</p>
				<textarea name="note" style="width: 100%; height: 100px;"><?php echo $getDelivery["note"]; ?></textarea>
			</div>

			<div class="t_c mt15"><input type="submit" value="บันทึก" class="btn btn-success"></div>
		</div>
	</form>
</div>