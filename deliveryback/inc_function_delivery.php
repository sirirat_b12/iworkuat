<?php



function getDelivery($poid){
	global $connMS;
	$resultarray = array();
	$sql = "SELECT Purchase_Order.PO_ID, Purchase_Order.Remark As PORemark,  Purchase_Order.Create_Date As POCreate_Date,  Purchase_Order.Create_By As POCreate_By,  Purchase_Order.Update_Date As POUpdate_Date, Purchase_Order.Employee_ID,  Purchase_Order.Update_By As POUpdate_By, Customer.Customer_ID, Customer.Customer_FName, Customer.Customer_LName, My_User.User_FName, My_User.User_LName, Insurance.Insurance_Name, Purchase_Order_Mail_Address.*, Province.Province_Name_TH, District.District_Name_TH, Subdistrict.Subdistrict_Name_TH, Policy_Delivery.Reference_No
	FROM Purchase_Order LEFT JOIN Customer ON Purchase_Order.Customer_ID = Customer.Customer_ID
	LEFT JOIN My_User ON Purchase_Order.Employee_ID = My_User.User_ID
	LEFT JOIN [dbo].[Insurance] ON Purchase_Order.Insurance_ID = Insurance.Insurance_ID
	LEFT JOIN [dbo].Purchase_Order_Insurer ON Purchase_Order.PO_ID = Purchase_Order_Insurer.PO_ID
	LEFT JOIN [dbo].[Purchase_Order_Mail_Address] ON Purchase_Order.PO_ID = Purchase_Order_Mail_Address.PO_ID
	LEFT JOIN [dbo].[Policy_Delivery] ON Purchase_Order.PO_ID = Policy_Delivery.PO_ID
  LEFT JOIN [dbo].[Province] ON Purchase_Order_Mail_Address.Province_ID = Province.Province_ID
  LEFT JOIN [dbo].[District] ON Purchase_Order_Mail_Address.District_ID = District.District_ID
	LEFT JOIN [dbo].[Subdistrict] ON Purchase_Order_Mail_Address.Subdistrict_ID = Subdistrict.Subdistrict_ID
	 
	WHERE Purchase_Order.PO_ID = '".$poid."' ";
	$stmt = sqlsrv_query( $connMS, $sql );
  if(sqlsrv_has_rows($stmt)) {
  	$row = sqlsrv_fetch_array($stmt);
  	return  $row;
    exit();
  }
}

function getDeliveryBackAll($case, $txtse, $poid, $statusTxt){
	global $connMS;
	$resultarray = array();
	$exDate = explode("-", $txtse);
	$year = $exDate[0];
	$mounth = $exDate[1];
  if($mounth){
		$datestart =  $year."-".$mounth."-01";
    $dateNow = $year."-".$mounth."-".date('t',strtotime($year."-".$mounth))." 23:59:59";
  }

	$sql = "SELECT Delivery_Back.*, Purchase_Order.PO_ID, Purchase_Order.Remark As PORemark,  Delivery_Back.Create_Date As DBCreate_Date,  Delivery_Back.Create_By As DBCreate_By, Customer.Customer_ID, Customer.Customer_FName, Customer.Customer_LName, My_User.User_FName, My_User.User_LName, Insurance.Insurance_Name, Purchase_Order_Mail_Address.*, Province.Province_Name_TH, District.District_Name_TH, Subdistrict.Subdistrict_Name_TH, Policy_Delivery.Reference_No
	
	FROM Delivery_Back 
	LEFT JOIN Purchase_Order ON Delivery_Back.po_id = Purchase_Order.PO_ID
	LEFT JOIN Customer ON Purchase_Order.Customer_ID = Customer.Customer_ID
	LEFT JOIN My_User ON Delivery_Back.Employee_ID = My_User.User_ID
	LEFT JOIN [dbo].[Insurance] ON Purchase_Order.Insurance_ID = Insurance.Insurance_ID
	LEFT JOIN [dbo].Purchase_Order_Insurer ON Purchase_Order.PO_ID = Purchase_Order_Insurer.PO_ID
	LEFT JOIN [dbo].[Purchase_Order_Mail_Address] ON Purchase_Order.PO_ID = Purchase_Order_Mail_Address.PO_ID
	LEFT JOIN [dbo].[Policy_Delivery] ON Purchase_Order.PO_ID = Policy_Delivery.PO_ID
    LEFT JOIN [dbo].[Province] ON Purchase_Order_Mail_Address.Province_ID = Province.Province_ID
    LEFT JOIN [dbo].[District] ON Purchase_Order_Mail_Address.District_ID = District.District_ID
	LEFT JOIN [dbo].[Subdistrict] ON Purchase_Order_Mail_Address.Subdistrict_ID = Subdistrict.Subdistrict_ID ";
	if($poid){
		$sql .= " WHERE Delivery_Back.PO_ID = '".$poid."' ";
	}else{
		if($case || !$mounth){
			$sql .= " WHERE Delivery_Back.Employee_ID = '".$case."' ";
		}else if(!$case || $mounth){
			$sql .= " WHERE Delivery_Back.delivery_back_date >= '".$datestart."' AND Delivery_Back.delivery_back_date <= '".$dateNow."' ";
		}

		if($statusTxt){
			$sql .= " AND Delivery_Back.status = '".$statusTxt."' ";
		}
	}
	$sql .= " ORDER BY Delivery_Back.delivery_back_id DESC";
	// echo $sql;
	$stmt = sqlsrv_query( $connMS, $sql );
  if(sqlsrv_has_rows($stmt)) {
        while( $row = sqlsrv_fetch_array($stmt) ) { 
          $resultarray[] = $row;
        }
        return $resultarray;
        exit();
    }
}


function getDeliveryBackForEdit($delivery_back_id){
	global $connMS;
	$resultarray = array();
	$sql = "SELECT Delivery_Back.*, Purchase_Order.PO_ID, Purchase_Order.Remark As PORemark,  Delivery_Back.Create_Date As DBCreate_Date,  Delivery_Back.Create_By As DBCreate_By, Customer.Customer_ID, Customer.Customer_FName, Customer.Customer_LName, My_User.User_FName, My_User.User_LName, Insurance.Insurance_Name, Purchase_Order_Mail_Address.Addr1, Purchase_Order_Mail_Address.Addr2, Province.Province_Name_TH, District.District_Name_TH, Subdistrict.Subdistrict_Name_TH, Policy_Delivery.Reference_No
	FROM Delivery_Back 
	LEFT JOIN Purchase_Order ON Delivery_Back.po_id = Purchase_Order.PO_ID
	LEFT JOIN Customer ON Purchase_Order.Customer_ID = Customer.Customer_ID
	LEFT JOIN My_User ON Delivery_Back.Employee_ID = My_User.User_ID
	LEFT JOIN [dbo].[Insurance] ON Purchase_Order.Insurance_ID = Insurance.Insurance_ID
	LEFT JOIN [dbo].Purchase_Order_Insurer ON Purchase_Order.PO_ID = Purchase_Order_Insurer.PO_ID
	LEFT JOIN [dbo].[Purchase_Order_Mail_Address] ON Purchase_Order.PO_ID = Purchase_Order_Mail_Address.PO_ID
	LEFT JOIN [dbo].[Policy_Delivery] ON Purchase_Order.PO_ID = Policy_Delivery.PO_ID
  LEFT JOIN [dbo].[Province] ON Purchase_Order_Mail_Address.Province_ID = Province.Province_ID
  LEFT JOIN [dbo].[District] ON Purchase_Order_Mail_Address.District_ID = District.District_ID
	LEFT JOIN [dbo].[Subdistrict] ON Purchase_Order_Mail_Address.Subdistrict_ID = Subdistrict.Subdistrict_ID
	WHERE Delivery_Back.delivery_back_id = '".$delivery_back_id."' ";
 	$stmt = sqlsrv_query( $connMS, $sql );
  if(sqlsrv_has_rows($stmt)) {
  	$row = sqlsrv_fetch_array($stmt);
  	return  $row;
    exit();
  }
}



function curlLineNoti($poid, $Employee_ID, $delivery_back_type){
	global $connMS;
	
	$user_id = $created_by;
	$messages = '';
	if($value["delivery_back_type"]=="policy"){
		$messages .= "แจ้งเตือน กรมธรรม์";
	}else{
		$messages .= "แจ้งเตือน จดหมายแจ้งเตือน";
	}
	$messages .= '\n '.$poid;
	$messages .= '\nกรุณาตรวจสอบ แก้ไข และเปลี่ยนสถานะ \nเพื่อให้ Admin ดำเนินการจัดส่งอีกครั้ง';
// echo $messages;
	$url  = 'https://www.asiadirect.co.th/line-at/ibroker/linemeapi.php';
	$data = array(
	    'user_id' => $Employee_ID,
	    'status' => "Delivery Mgm",
	    'messages' => $messages
	);
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$result = curl_exec($ch);
		$error_msg = curl_error($ch);
		curl_close($ch);
	return 1;
	exit();
}


function sendMailForDelivery($poid, $note){ 

	$mail = new PHPMailer\PHPMailer\PHPMailer(true); 
		$bodyMail = "<div style='font-size:16px;'>กรุณาตรวจสอบและจัดส่งเอกสารของ ".$poid."</div> <div><b>หมายเหตุ :</b>".$note."</div>";
  	$mail->CharSet = 'utf-8';
  	$mail->SMTPDebug = 0;                                 // Enable verbose debug output
    $mail->isSMTP();                                      // Set mailer to use SMTP
    $mail->Host = 'smtp.gmail.com'; // Specify main and backup SMTP servers
    $mail->SMTPAuth = true;  
    $mail->Username = "admin@asiadirect.co.th";
    $mail->Password = "@db6251000";
    $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
    $mail->Port = 587;                                    // TCP port to connect to

    $mail->setFrom('admin@asiadirect.co.th', 'Asia Direct');
	  // $mail->AddAddress("narongrit.c@asiadirect.co.th", "Narongrit"); 
	  $mail->AddAddress("anchalee.i@asiadirect.co.th", "Anchalee");
	  $mail->AddAddress("tanawut.j@asiadirect.co.th", "Tanawut");           // Name is optional
    $mail->isHTML(true);                                  // Set email format to HTML
    $mail->Subject = "แจ้งการจัดส่งเอกสารใหม่อีกครั้ง ".$poid;
    $mail->Body    = $bodyMail;

    // $mail->send();
// exit();
	if(!$mail->Send()) {
		return 0;
	} else {
		return 1;
	}
}

?>