<?php
session_start();
date_default_timezone_set("Asia/Bangkok");
header('Content-Type: text/html; charset=utf-8');

include "../inc_config.php";
include "inc_function_delivery.php";

$po_id = $_POST["poCode"];
$getDelivery = getDelivery($po_id);
// echo "<pre>".print_r($_POST,1)."</pre>";
// echo "<pre>".print_r($getDelivery,1)."</pre>";
if($getDelivery["Addr2"]){
	$eAddr = explode("|", $getDelivery["Addr2"]);
	if($eAddr[0]){
		$addr = "เลขที่ ".$eAddr[0];
	}if($eAddr[1]){
		$addr .= " อาคาร".$eAddr[1];
	}if($eAddr[2]){
		$addr .= " หมู่ที่ ".$eAddr[2];
	}if($eAddr[3]){
		$addr .= " ซอย ".$eAddr[3];
	}if($eAddr[4]){
		$addr .= " ถนน ".$eAddr[4];
	}
}else{
	$addr = $getDelivery["Addr1"];
}
$addr = $addr." ตำบล/เขต ".$getDelivery["Subdistrict_Name_TH"]." อำเภอ/เขต ".$getDelivery["District_Name_TH"]." จัวหวัด ".$getDelivery["Province_Name_TH"]." ".$getDelivery["Post_Code"];
?>

<div class="main-content p20 fs12">
	<form action="deliveryback/inc_action_delivery.php" method="POST" id="form_send_order" >
		<input type="hidden" value="addDeliveryBack" name="action">
		<input type="hidden" value="<?php echo $getDelivery["Employee_ID"]; ?>" name="Employee_ID">
		<div class="row">
			<div class="col-md-12">
				<span class="fwb">ประเภทจดหมาย</span>
					<input type="radio" name="delivery_back_type" value="policy" required> กรมธรรม์
					<input type="radio" name="delivery_back_type" value="act" required> พรบ.
					<input type="radio" name="delivery_back_type" value="noti" required> ใบเตือน
					<input type="radio" name="delivery_back_type" value="endorse" required> สลักหลัง
					<input type="radio" name="delivery_back_type" value="giftcard" required> บัตรกำนัล
			</div>
			<div class="col-md-12 mt10">
				<p class="fwb">PO *</p>
				<input type="text" value="<?php echo $getDelivery["PO_ID"]; ?>" class="form-control formInput2" name="PO_ID" readonly>
			</div>
			<div class="col-md-12 mt10">
				<p class="fwb">ลูกค้า *</p>
				<input type="text" value="<?php echo $getDelivery["Customer_FName"]." ".$getDelivery["Customer_LName"]; ?>" class="form-control formInput2" name="addr" readonly >
			</div>
			<div class="col-md-12 mt10">
				<p class="fwb">ผู้ขาย *</p>
				<input type="text" value="<?php echo $getDelivery["User_FName"]." ".$getDelivery["User_LName"]; ?>" class="form-control formInput2" name="addr" readonly >
			</div>
			<div class="col-md-12 mt10">
				<p class="fwb">ที่อยู่ *</p>
				<input type="text" value="<?php echo $addr; ?>" class="form-control formInput2" name="addr" readonly >
			</div>
			<div class="col-md-12 mt10">
				<p class="fwb">วันที่รับเอกสาร *</p>
				<input type="date" value="<?php echo date("Y-m-d"); ?>" class="form-control formInput2" name="delivery_back_date" >
			</div>
			<div class="col-md-12 mt10">
				<p class="fwb">Tracking Number *</p>
				<input type="text" value="<?php echo $getDelivery["Reference_No"]; ?>" class="form-control formInput2" name="tracking" >
			</div>
			<div class="col-md-12 mt10">
				<p class="fwb">ประเภทการส่ง *</p>
				<select name="delivery_Type" id="delivery_Type" class="form-control formInput2" required>
					<option value="">กรุณาเลือก</option>
					<option value="J&T EXPRESS">J&T EXPRESS</option>
					<option value="ไปรษณีย์">ไปรษณีย์</option>
				</select>
			</div>
			<div class="col-md-12 mt10">
				<p class="fwb">เหตุผลในการตีกลับ *</p>
				<select name="reason" id="reason" class="form-control formInput2" required>
					<option value="">กรุณาเลือก</option>
					<option value="จ่าหน้าไม่ชัดเจน">จ่าหน้าไม่ชัดเจน</option>
					<option value="ไม่มีเลขที่บ้านตามจ่าหน้า">ไม่มีเลขที่บ้านตามจ่าหน้า</option>
					<option value="ไม่ยอมรับ">ไม่ยอมรับ</option>
					<option value="ไม่มีผู้รับตามจ่าหน้า">ไม่มีผู้รับตามจ่าหน้า</option>
					<option value="ไม่มารับภายในกำหนด">ไม่มารับภายในกำหนด</option>
					<option value="เลิกกิจการ">เลิกกิจการ</option>
					<option value="ย้ายไม่ทราบที่อยู่ใหม่">ย้ายไม่ทราบที่อยู่ใหม่</option>
					<option value="ติดต่อลูกค้าไม่ได้">ติดต่อลูกค้าไม่ได้</option>
					<option value="ลูกค้าปฏิเสธการรับ">ลูกค้าปฏิเสธการรับ</option>
					<option value="เบอร์ระงับการใช้งาน">เบอร์ระงับการใช้งาน</option>
					<option value="ปิดเครื่อง">ปิดเครื่อง</option>
					<option value="ผู้รับย้ายที่อยู่">ผู้รับย้ายที่อยู่</option>
					<option value="อื่นๆ">อื่นๆ</option>
				</select>
			</div>
			<div class="col-md-12 mt10">
				<p class="fwb">สถานะ *</p>
				<select name="status" id="status" class="form-control formInput2" required>		
					<option value="">กรุณาเลือก</option>
					<option value="แอดมินรับเอกสารตีกลับ">แอดมินรับเอกสารตีกลับ</option>
					<option value="แจ้งSalesรับทราบ">แจ้งSalesรับทราบ</option>
					<option value="รอSalesตรวจสอบข้อมูลอีกครั้ง">รอSalesตรวจสอบข้อมูลอีกครั้ง</option>
					<option value="รอแอดมินจัดส่งให้ใหม่">รอแอดมินจัดส่งให้ใหม่</option>
					<option value="กำลังจัดส่ง">กำลังจัดส่ง</option>
					<option value="ลูกค้าติดต่อแจ้งที่อยู่แล้ว">ลูกค้าติดต่อแจ้งที่อยู่แล้ว</option>
					<option value="ไม่สามาถรถติดตามลูกค้าได้">ไม่สามาถรถติดตามลูกค้าได้</option>
				</select>
			</div>
			<div class="col-md-12 mt10">
				<p class="fwb">หมายเหตุ *</p>
				<textarea name="note" style="width: 100%; height: 100px;"></textarea>
			</div>

			<div class="t_c mt15"><input type="submit" value="บันทึก" class="btn btn-success"></div>
		</div>
	</form>
</div>