<?php
session_start();
date_default_timezone_set("Asia/Bangkok");
header('Content-Type: text/html; charset=utf-8');

include "../inc_config.php";
require '../phpmailer6/src/PHPMailer.php';
require '../phpmailer6/src/SMTP.php';
require '../phpmailer6/src/Exception.php';
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;
include "inc_function_delivery.php";


if($_POST["action"] == "addDeliveryBack"){
// echo "<pre>".print_r($_POST,1)."</pre>";
	$user =$_SESSION["User"]['UserCode']; 
	$sql = "INSERT INTO Delivery_Back ([po_id],[delivery_back_type],  [delivery_back_date], [Employee_ID], [tracking], [reason], [result], [delivery_Type],[status], [note], [Create_Date], [Create_By], [Update_Date], [Update_By])
     VALUES ('".$_POST["PO_ID"]."', '".$_POST["delivery_back_type"]."', '".$_POST["delivery_back_date"]."', '".$_POST["Employee_ID"]."', '".$_POST["tracking"]."', '".$_POST["reason"]."', '".$_POST["result"]."', '".$_POST["delivery_Type"]."', '".$_POST["status"]."', '".$_POST["note"]."','".date('Y-m-d H:i:s')."', '".$user."', '".date('Y-m-d H:i:s')."', '".$user."')";
	
	$stmt = sqlsrv_query( $connMS, $sql);
	if($stmt){
		if($_POST["status"] != "ลูกค้าติดต่อแจ้งที่อยู่แล้ว"){
	// curlLineNoti($_POST["PO_ID"], $_POST["Employee_ID"], $_POST["delivery_back_type"]);
		}
		echo "<script>alert('ทำรายการสำเร็จ');</script>";
		echo '<META http-equiv="refresh" content="0;URL=../delivery_back.php">';
		exit();
	}else{
		echo "<script>alert('ไม่สามารถทำรายการได้ กรุณาลองอีกครั้ง');</script>";
		echo '<META http-equiv="refresh" content="0;URL=../delivery_back.php">';
		exit();
	}

}else if($_POST["action"] == "deleteDelivery"){
	$sql = "DELETE FROM [Delivery_Back] WHERE delivery_back_id = '".$_POST["delivery_back_id"]."' ";
	$stmt = sqlsrv_query( $connMS, $sql);
	if($stmt){
		echo "1";
	}else{
		echo "0";
	}
	exit();

}elseif($_POST["action"] == "editDelivery"){
// echo "<pre>".print_r($_POST,1)."</pre>";
	$user =$_SESSION["User"]['UserCode']; 
	if($_SESSION["User"]['type'] != "Sale"){
		$sql = "UPDATE [Delivery_Back] SET [reason] = '".$_POST["reason"]."', [result] = '".$_POST["result"]."', [delivery_Type] = '".$_POST["delivery_Type"]."', [status] = '".$_POST["status"]."', [note] = '".$_POST["note"]."', [Update_Date] = '".date('Y-m-d H:i:s')."', [Update_By] = '".$user."' WHERE delivery_back_id = '".$_POST["delivery_back_id"]."'";
	}else{
		$sql = "UPDATE [Delivery_Back] SET [result] = '".$_POST["result"]."', [delivery_Type] = '".$_POST["delivery_Type"]."', [status] = '".$_POST["status"]."', [note] = '".$_POST["note"]."', [Update_Date] = '".date('Y-m-d H:i:s')."', [Update_By] = '".$user."' WHERE delivery_back_id = '".$_POST["delivery_back_id"]."'";
	}

	$stmt = sqlsrv_query( $connMS, $sql);

	// if($_POST["status"] == "รอแอดมินจัดส่งให้ใหม่"){
	// 	sendMailForDelivery($_POST["PO_ID"], $POST["note"]);
	// }
	if($stmt){
		echo "<script>alert('ทำรายการสำเร็จ');</script>";
		echo '<META http-equiv="refresh" content="0;URL=../delivery_back.php">';
		exit();
	}else{
		echo "<script>alert('ไม่สามารถทำรายการได้ กรุณาลองอีกครั้ง');</script>";
		echo '<META http-equiv="refresh" content="0;URL=../delivery_back.php">';
		exit();
	}
}


?>