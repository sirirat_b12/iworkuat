<?php 

session_start();
$UserCode = $_SESSION["User"]['UserCode'];
$userType = $_SESSION["User"]['type'];

if(!isset($UserCode)){
	echo "<script>alert('กรุณาเข้าสู่ระบบ!!!');</script>";
	echo '<META http-equiv="refresh" content="0;URL=login.php">';
	exit();
}
if($userType == "Sale" || $userType == "CallCenter"){
	echo '<META http-equiv="refresh" content="0;URL=index.php">';
	exit();
}
header('Content-Type: text/html; charset=utf-8');
include "include/inc_header.php"; 
include "include/inc_menu.php"; 


if($userType == "QualityControl" || $userType == "Admin"){
	$send_orders = getAllListForMove($UserCode);
	$getQCcheck = getPersonnelByType($userType);
}else{
	$send_orders = getAllListForMove();
	$getQCcheck = getPersonnelByType();
}


// echo "<pre>".print_r($getQCcheck,1)."</pre>";
?>
<div class="main">
	<div class="main-content p20">
		<div class="p20">
				<div class="row">
					<div class="col-md-12">
						<div class="panel">
								<div class="panel-heading">
									<h4 class="panel-title"><b>ย้าย Lead </b></h4>
								</div>
								<div class="panel-body fs14">
									<div class="row">
										<div class="col-md-1"><h4>ย้ายไปที่ :</h4></div>
										<div class="col-md-3">
											
											<select name="chk_code" id="chk_code" class="form-control formInput2">
												<option value="">กรุณาเลือก</option>
												<?php foreach ($getQCcheck as $key => $value) { ?>
													<option value="<?php echo $value["personnel_code"] ?>"><?php echo $value["personnel_code"]." ".$value["personnel_firstname"]." ".$value["personnel_lastname"] ?></option>
												<?php } ?>
											</select>
										</div>
										<div class="col-md-2"><button class="btn btn-info" id="btnAddMove">เปลี่ยน</button></div>
										<div class="col-md-6 t_r"> <input type="text" id="search" placeholder="ค้นหา" class="form-control " style="width: 30%;float: right;"/></div>
									</div>
									<div class="row mt20">
										<div class="col-md-12">
											<form action="include/inc_action_chk.php" method="POST" id="frmMoveQC">
												<input type="hidden" name="action" value="addMoveQC">
												<input type="hidden" name="newqcCode" id="newqcCode" >
												<input type="hidden" name="changType" value="<?php echo $userType ?>" >
												<table class="table table-hover" id="tableMoveQC">
													<thead>
														<tr>
															<th class="t_c">
																<!-- <input type="checkbox" id="checkAllMoveQC" value="<?php echo $value["noti_work_code"]; ?>" > -->
															</th>
															<th data-field="statusType" class="t_c">สถานะ</th>
															<!-- <th class="t_c">ใช้งาน</th> -->
															<th data-field="noti_work_code" class="t_c dn"></th>
															<th data-field="pocode" data-sortable="true" class="t_c">PO</th>
															<th class="t_c">ชื่อ-นามสกุล</th>
															<th class="t_c">แพจเกต</th>
															<th data-field="start_cover_date" data-sortable="true" class="t_c">วันคุ้มครอง</th>
															<th class="t_c">ผู้ขาย</th> 
															<th data-field="created_date" data-sortable="true" class="t_c">วันที่ทำรายการ</th>
															<?php 
															if($userType == "QualityControl") { 
																echo '<th class="t_c">ผู้ตรวจ</th>';
															}else if($userType == "Admin") { 
																echo '<th class="t_c">แจ้งงาน</th>';
															}else{ 
																echo '<th class="t_c">ผู้ตรวจ</th>';
																echo '<th class="t_c">แจ้งงาน</th>';
															}
															?>
															<!-- <th class="t_c" >หมายเหตุ</th> -->
														</tr>
													</thead>
													<tbody>
														<?php $i=1;  foreach ($send_orders as $key => $value) { ?>
																<tr >
																	<td class=" t_c"> 
																		<input type="checkbox" name="chktxt[]" class="chkMoveQC"  value="<?php echo $value["noti_work_code"]."_".$value["noti_work_id"]."_".$value["status"]; ?>" >
																	</td>
																	<td class="wpno t_c"><?php echo setStatus($value["status"]); ?></td>
																	<td class="t_l dn"><?php echo $value["noti_work_code"]; ?></td>
																	<td class=" t_l"> 
																		<b><?php echo $value["po_code"]; ?></b>
																		<div class="clearfix fs10"><?php echo $value["noti_work_code"]; ?></div>
																	</td>
																	<td class="t_l <?php echo $bgColor; ?>"><b><?php echo $value["cus_name"]; ?></b></td>
																	<td class="t_l">
																		<div class="clearfix fs10 c2457ff"><?php echo $value["insuere_company"]; ?></div>
																		<div class="clearfix c9c00c8"><?php echo $value["package_name"]; ?></div>
																		<div class="clearfix fs10 "><?php echo $value["insurance_type"]; ?></div>
																	</td>
																	<td><?php echo $value["start_cover_date"]; ?></td>
																	<td class="t_l ">
																			<div class="clearfix c2457ff "><?php echo $value["personnel_name"]; ?></div>
																			<div class="clearfix fs10 "><?php echo $value["send_type"] ? "<b>การส่ง :</b> ".$value["send_type"] : ""; ?></div>
																	</td>
																	<td ><?php echo $value["created_date"]; ?></td>
																	<?php if($userType == "QualityControl") {  ?>
																		<td class="t_l">
																			<div class="clearfix "><b>ชื่อ: </b><?php echo $value["Fname"]." ".$value["Lname"]; ?></div>
																			<div class="clearfix "><b>เวลา: </b><?php echo $value["datetime_chk"]; ?></div>
																		</td>
																	<?php	}else if($userType == "Admin") { ?>
																		<td class="t_l">
																			<div class="clearfix "><?php echo $value["Fname"]." ".$value["Lname"]; ?></div>
																		</td>
																	<?php	}else{  ?>
																		<td class="t_l">
																			<div class="clearfix "><b>ชื่อ: </b><?php echo $value["Fname"]." ".$value["Lname"]; ?></div>
																			<div class="clearfix "><b>เวลา: </b><?php echo $value["datetime_chk"]; ?></div>
																		</td>
																		<td class="t_l">
																			<div class="clearfix "><?php echo $value["admin_code"]; ?></div>
																		</td>
																	<?php	} ?>
																</tr>
														<?php } ?>
													</tbody>
												</table>
											</form>
										</div>
									</div>
								</div>
						</div>
					</div>
				</div>
		</div>
	</div>
</div>
<?php include "include/inc_footer.php"; ?> 
<script type="text/javascript">
	$("#search").keyup(function(){
	    var searchText = $(this).val().toLowerCase();
	    // console.log(this);
	    // Show only matching TR, hide rest of them
	    $.each($("#tableMoveQC tbody tr"), function() {
	        if($(this).text().toLowerCase().indexOf(searchText) === -1)
	           $(this).hide();
	        else
	           $(this).show();                
	    });
	}); 

	$("#checkAllMoveQC").click(function() {
    if($('#checkAllMoveQC').is(':checked')){
      $(".chkMoveQC").prop('checked', true);
    }else{
      $(".chkMoveQC").prop('checked', false);
      
    }
  });

  $("#btnAddMove").click(function() {
    var countCK = $('input[name="chktxt[]"]:checked').length;
    var newqcCode = $("#chk_code").val();
    var newqcCodeTxt = $("#chk_code option:selected").text();
    if(countCK > 0 && newqcCode){
    	if(confirm("คุณต้องการย้ายข้อมูลไปให้ "+newqcCodeTxt)){
	      $("#newqcCode").val(newqcCode); console.log(newqcCode);
	      $("#frmMoveQC").submit();
	    }
    }else{
      alert("กรุณาเลือก บุคคลที่ต้องการย้ายไป");
    }
  });
</script>