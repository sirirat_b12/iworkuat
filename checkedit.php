<?php 
session_start();
header('Content-Type: text/html; charset=utf-8');
if(!isset($_SESSION["User"]['UserCode'])){
	echo "<script>alert('กรุณาเข้าสู่ระบบ!!!');</script>";
	echo '<META http-equiv="refresh" content="0;URL=login.php">';
	exit();
}

include "include/inc_header.php"; 
// include "include/inc_function.php"; 
include "include/inc_menu.php"; 

$pocode = $_GET["pocode"];
$api = getAllCustomerCase($pocode);
$sale = getAllSale($api["personnel_code"]);
$listapi = getAllCustomerlist($api["pocode"], $api["api_id"]);

if($_SESSION["User"]['type'] != "SuperAdmin"){
	if($api["status"] == 1 || $api["personnel_code"] != $_SESSION["User"]['UserCode']){
		echo "<script>alert('ไม่สามารถทำรายการได้');</script>";
		echo '<META http-equiv="refresh" content="0;URL=chklists.php">';
		exit();
	}
}
// echo "<pre>".print_r($listapi,1)."</pre>";

if($_GET){
	$getlistIns = getlistIns();
}
?> 
<div class="main">
			<!-- MAIN CONTENT -->
	<div class="main-content">
		<div class="container-fluid">
			<div class="panel panel-profile">
				<div class="clearfix">
					<form action="include/inc_action.php" method="POST" id="sendUpdateLead">
						<input type="hidden" name="action" id="action" value="sendUpdateLead">
						<!-- <input type="hidden" name="action" id="action" value="sendUpdateLead" > -->
						<input type="hidden" name="cpid" value="<?php echo $api["cpid"]; ?>">
						<input type="hidden" name="api_id" value="<?php echo $api["api_id"]; ?>">
						<input type="hidden" name="pocode" value="<?php echo $api["pocode"]; ?>">
						<input type="hidden" name="make" value="<?php echo $api["make"]; ?>">
						<input type="hidden" name="model" value="<?php echo $api["model"]; ?>">
						<input type="hidden" name="cc" value="<?php echo $api["cc"]; ?>">
						<input type="hidden" name="year" value="<?php echo $api["year"]; ?>">
						<input type="hidden" name="cpid" value="<?php echo $api["cpid"]; ?>">
						<input type="hidden" name="statusMain" id="statusMain">
						<!-- LEFT COLUMN -->
						<div class="profile-left">
							<div class="boxSearch">
								<p class="text-center fs16 fwb">กรุณาเลือกการค้นหา</p>
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<input class="form-control" type="text" name="name" id="name" value="<?php echo $api["name"]; ?>" readonly="readonly" >
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<input class="form-control" type="email" name="email" id="email" value="<?php echo $api["email"]; ?>" placeholder="อีเมล์*" readonly="readonly" >
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<input class="form-control" type="" name="phone" id="phone" value="<?php echo $api["phone"]; ?>" placeholder="เบอร์โทรศัพท์*" maxlength="10" onkeyup="chknum(this,event)" readonly="readonly" >
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<input class="form-control" type="text" name="line" id="line" value="<?php echo $api["line"]; ?>" placeholder="ไลน์" readonly="readonly">
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<select id="newfrom" class="form-control formInput" name="newfrom" readonly="readonly">
					                            <option value="AsiaDirect" <?php if($api["newfrom"]=="AsiaDirect"){ echo $api["line"]; } ?> >ไม่สามารถระบุได้</option>
					                            <option value="CallCenter" <?php if($api["newfrom"]=="CallCenter"){ echo $api["line"]; } ?>>CallCenter</option>
					                            <option value="Google" <?php if($api["newfrom"]=="Google"){ echo $api["line"]; } ?>>Google</option>
					                            <option value="Facebook" <?php if($api["newfrom"]=="Facebook"){ echo $api["line"]; } ?>>Facebook</option>
					                            <option value="Otherwebsites" <?php if($api["newfrom"]=="Otherwebsites"){ echo $api["line"]; } ?>>เว็บไซต์อื่นๆ</option>
					                            <option value="Brochure" <?php if($api["newfrom"]=="Brochure"){ echo $api["line"]; } ?>>โบว์ชัวร์</option>
					                            <option value="Recommend" <?php if($api["newfrom"]=="Recommend"){ echo $api["line"]; } ?>>ผู้อื่นแนะนำ</option>
					                        </select>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<textarea class="form-control" rows="1" name="remark" placeholder="หมายเหตุ" readonly="readonly"><?php echo $api["remark"]; ?></textarea>
										</div>
									</div>
								</div>
							</div>
							<div class="boxList">
								<div id="divnMail" class="t_c">
									<button type="button" class="btn btn-primary btnsubmitedit" ><i class="lnr lnr-location"></i> แก้ไขในเสนอราคา</button>
									<button type="button" class="btn btn-success btnsubmitPO dn" ><i class="lnr lnr-location"></i> ยืนยันทำใบเสนอราคา</button>
								</div>	
							</div>
						</div>
						<!-- END LEFT COLUMN -->
						<!-- RIGHT COLUMN -->
						<div class="profile-right">
							<h4 class="heading">ข้อเสนอประกันภัย <?php echo $api["cpid"]; ?></h4>
							<div class="panel-body">
								<div class="row">
									<?php 
										if($listapi){ 
											foreach($listapi as $key => $value) {
												$money1 = 0; 
												$money2 = 0;
												$insurance = getRowInsurance($value['insurance_id']);
												//$value = getRowInsur($_SESSION['inslist']['planid'][$key], $_SESSION['inslist']['costid'][$key]);
												//$insurer = getRowInsurer($value["insurance_insurer"]);
												$insurPrine = ($value["inscost_maxamount"] == 0) ? $value["inscost_minamount"] : $goodretail;
												$protect1 = getProtect($value['insurance_id'], "ความรับผิดชอบต่อบุคคลภายนอก");
												$protect2 = getProtect($value['insurance_id'], "ความรับผิดต่อตัวรถยนต์");
												$protect3 = getProtect($value['insurance_id'], "ความคุ้มครองตามเอกสารแนบท้าย");
												// echo "<pre>".print_r($insurance,1)."</pre>";
												if($value["salecost"]){
											        $premium = $value["taxamount"] - $value["salecost"];
											        $discountSum = $value["salecost"];
												}else if($value["percen_sale"] != 0){
											        $discount = ($value["premamount"]  * $value["percen_sale"] ) / 100;
											        $premium = ceil(($value["taxamount"] - $discount) / 100) * 100;
											        $discountSum = $value["taxamount"] - $premium;
											    }else{
											        $premium = $value["taxamount"];
											    }

											    if($value["installment"]){
											    	$down = ceil($value["taxamount"]/$value["installment"]);
											    }
											    $money1 = ceil(($value['taxamount'] * 40 ) / 100);
												$money2 = ceil(($value['taxamount'] - $money1) / 2);

									?>
										<div class="fs14  boxselect_<?php echo $key?>">
											<input type="hidden" name="ins[<?php echo $key?>][apilists_id]" value="<?php echo $value["apilists_id"]; ?>">
											<input type="hidden" name="ins[<?php echo $key?>][salecost]" id="salecost<?php echo $key?>" value="<?php echo $insurance["salecost"]; ?>">
											<input type="hidden" name="ins[<?php echo $key?>][percen_sale]" id="percen_sale<?php echo $key?>" value="<?php echo $insurance["percen_sale"]; ?>">
											<input type="hidden" name="ins[<?php echo $key?>][class]" value="<?php echo $value["class"]; ?>">
											<input type="hidden" name="ins[<?php echo $key?>][insurercode]" value="<?php echo $value["insurercode"]; ?>">
											<input type="hidden" name="ins[<?php echo $key?>][insurername]" value="<?php echo $value["insurername"]; ?>">
											<input type="hidden" name="ins[<?php echo $key?>][cartype]" value="<?php echo $value["insurance_code"]; ?>">
											<input type="hidden" name="ins[<?php echo $key?>][insurance_id]" value="<?php echo $value["insurance_id"]; ?>">
											<input type="hidden" name="ins[<?php echo $key?>][insurance_cost_id]" value="<?php echo $value["insurance_cost_id"]; ?>">
											<input type="hidden" name="ins[<?php echo $key?>][package_code]" value="<?php echo $value["package_code"]; ?>">
											<!-- <input type="hidden" name="ins[<?php echo $key?>][package_name]" value="<?php echo $value["package_name"]; ?>"> -->
											<input type="hidden" name="ins[<?php echo $key?>][repairs]" value="<?php echo $value["repairs"]; ?>">
											<input type="hidden" name="ins[<?php echo $key?>][installment]" id="installment<?php echo $key?>" value="<?php echo $value["installment"]; ?>">
											<input type="hidden" name="ins[<?php echo $key?>][downs]" id="downs<?php echo $key?>" value="<?php echo $down; ?>">
											<input type="hidden" name="ins[<?php echo $key?>][status]" id="status_<?php echo $key?>" class="statusAc">
											<div class="col-md-4">
												<div class="bgffecd5 t_l titleprochk btnselecttitle" id="select_<?php echo $key?>">
													<input type="checkbox" name="insSelect" id="insChk<?php echo $key?>" value='1' class="insSelect">
													<b class="ml5">ข้อเสนอที่ <?php echo $key+1?></b>
												</div>
												<div class="row">
													<div class="col-md-12 "><b>แพจเกต</b></div>
													<div class="col-md-12">
														<div class="form-group">
															<input type="text" name="ins[<?php echo $key?>][package_name]" id="package_name<?php echo $key?>" class="form-control " value="<?php echo $value["package_name"]; ?>" >
														</div>
													</div>
												</div>
												<div><b>ประกันชั่น </b> <?php echo $value["class"]; ?></div>
												<div><b>บริษัท</b> <?php echo $value["insurername"];?></div>
												<div class="row">
													<div class="col-md-7 t_r p5">ทุนประกัน</div>
													<div class="col-md-5">
														<div class="form-group">
															<input type="text" name="ins[<?php echo $key?>][inscost]" class="form-control inputChkout" calPremium value="<?php echo number_format($value["inscost"],2); ?>" id="inscost_<?php echo $key?>" >
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-7 t_r p5">เบี้ยประกันสุทธิ</div>
													<div class="col-md-5">
														<div class="form-group">
															<input type="hidden" name="netpremium<?php echo $key?>" id="netpremium<?php echo $key?>" value="<?php echo $value["premamount"]; ?>">
															<input type="text" name="ins[<?php echo $key?>][netpremium]" class="form-control inputChkout" value="<?php echo number_format($value["netpremium"],2); ?>" id="netpremium_<?php echo $key?>" readonly="readonly">
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-7 t_r p5">เบี้ยประกันรวม</div>
													<div class="col-md-5">
														<div class="form-group">
															<input type="hidden" name="premium<?php echo $key?>" id="premiumSum<?php echo $key?>" value="<?php echo $value["premium"]; ?>">
															<input type="text" name="ins[<?php echo $key?>][premium]" id="premium_<?php echo $key?>" class="form-control inputChkout calPremium" value="<?php echo number_format($value["premium"],2); ?>" >
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-7 t_r p5">ส่วนลด</div>
													<div class="col-md-5">
														<div class="form-group">
															<input type="hidden" name="disSum<?php echo $key?>" id="disSum<?php echo $key?>" value="<?php echo $value["discount"]; ?>">
															<input type="text" name="ins[<?php echo $key?>][discount]" id="dis_<?php echo $key?>" class="form-control inputChkout discal" value="<?php echo number_format($value["discount"],2); ?>" >
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-7 t_r p5">พรบ.</div>
													<div class="col-md-5">
														<div class="form-group">
															<select id="act<?php echo $key?>" class="form-control formInput actprice" name="ins[<?php echo $key?>][actprice]"  >
									                            <option value="0" <?php if($value["actprice"] == 0){ echo "selected";}?>>ไม่มี</option>
									                            <option value="645.21" <?php if($value["actprice"] == '645.21'){ echo "selected";}?>>เก๋ง 645.21</option>
									                            <option value="967.28" <?php if($value["actprice"] == "967.28"){ echo "selected";}?>>กระบะ 967.28</option>
									                            <option value="1182.35" <?php if($value["actprice"] == "1182.35"){ echo "selected";}?>>รถตู้ 1,182.35</option>
									                        </select>
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-7 t_r p5">จ่ายสุทธิ</div>
													<div class="col-md-5">
														<div class="form-group">
															<input type="text" name="ins[<?php echo $key?>][taxamount]" id="taxamount<?php echo $key?>" class="form-control inputChkout" value="<?php echo number_format($value["taxamount"],2); ?>" readonly="readonly">
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-7 t_r p5">แสดงส่วนลด</div>
													<div class="col-md-5">
														<div class="form-group">
															<input type="checkbox" name="ins[<?php echo $key?>][showdiscount]" id="" class="showDis" onclick="" value="1" data-id="" checked>
														</div>
													</div>
												</div>
												<!-- <div class="row">
													<div class="col-md-7 t_r p5">ผ่อนเงินสด 3 เดือน</div>
													<div class="col-md-5">
														<div class="form-group">
															<input type="checkbox" name="ins[<?php echo $key?>][showdown]" id="chkdown" class="showDis" value="1" data-id="" checked>
														</div>
													</div>
												</div> -->
												
												<!-- <div class="row">
													<div class="col-md-7 t_r ">ผ่อน 0% นาน <?php echo $value["installment"]; ?> ด.</div>
													<div class="col-md-5">
														<b>งวดละ</b> <span id="down_<?php echo $key?>"><?php echo number_format($value["downs"],2); ?></span> <b>บาท</b>
													</div>
												</div> -->
												<div class="row">
													<div class="col-md-7 t_r ">ผ่อนเงินสด</div>
													<div class="col-md-5">
														<div class="form-group">
															<select id="chkdownpay<?php echo $key?>" class="form-control formInput chkdownpay" name="ins[<?php echo $key?>][showdown]"  >
									                            <option value="0" <?php if($value["showdown"] == '0'){ echo "selected";}?>>ไม่มี</option>
									                            <option value="3" <?php if($value["showdown"] == '3'){ echo "selected";}?>>3 เดือน</option>
									                            <option value="6" <?php if($value["showdown"] == '6'){ echo "selected";}?>>6 เดือน</option>
									                        </select>
														</div>
													</div>
													<div class="row p15">
														<div id="showdowncash<?php echo $key?>">
															<?php 
																$getlistapiDown = getlistapiDown($value["apilists_id"]);
																foreach ($getlistapiDown as $keyDown => $valDown) { 
															?>
																<div class="col-md-4">
																	<div class="form-group"><span class="fs12">งวดที่ <?php echo $valDown["periods"]?></span><input type="text" name="ins[<?php echo $key?>][downcash][<?php echo $keyDown+1?>]" class="form-control inputChkout" id="money<?php echo $valDown["periods"]?>_0" value="<?php echo $valDown["price"]?>">
																	</div>
																</div>
															<?php } ?>
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-7 t_r ">ผ่อนบัตร 0%</div>
													<div class="col-md-5">
														<div class="form-group">
															<select id="install<?php echo $key?>" class="form-control formInput install" name="ins[<?php echo $key?>][install]"  >
									                            <option value="0">ไม่มี</option>
									                            <option value="3" <?php if($value["installment"] == '3'){ echo "selected";}?>>3 เดือน</option>
									                            <option value="6" <?php if($value["installment"] == '6'){ echo "selected";}?>>6 เดือน</option>
									                            <option value="10" <?php if($value["installment"] == '10'){ echo "selected";}?>>10 เดือน</option>
									                            <option value="12" <?php if($value["installment"] == '12'){ echo "selected";}?>>12 เดือน</option>
									                        </select>
														</div>
														<b>งวดละ</b> <span id="down_<?php echo $key?>"><?php echo number_format($value["downs"]);?></span> <b>บาท</b>
													</div>
												</div>
												<div class="bge3 titleprochk mt15"><b>ความรับผิดต่อบุคคลภายนอก</b></div>
												<div class="row">
													<div class="col-md-7 t_r p5">ความเสียหายต่อชีวิต ร่างกาย หรืออนามัย/ค</div>
													<div class="col-md-5">
														<div class="form-group">
															<input type="text" name="ins[<?php echo $key?>][C502]" class="form-control inputChkout protect_<?php echo $key?>" value="<?php echo number_format($value["c502"],2); ?>" >
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-7 t_r p5">ความเสียหายต่อชีวิต ร่างกาย หรืออนามัย/ครั้ง</div>
													<div class="col-md-5">
														<div class="form-group">
															<input type="text" name="ins[<?php echo $key?>][C503]" class="form-control inputChkout protect_<?php echo $key?>" value="<?php echo number_format($value["c503"],2); ?>" >
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-7 t_r p5">ความเสียหายต่อทรัพย์สิน</div>
													<div class="col-md-5">
														<div class="form-group">
															<input type="text" name="ins[<?php echo $key?>][C504]" class="form-control inputChkout protect_<?php echo $key?>" value="<?php echo number_format($value["c504"],2); ?>" >
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-7 t_r p5">ความเสียหายส่วนแรก</div>
													<div class="col-md-5">
														<div class="form-group">
															<input type="text" name="ins[<?php echo $key?>][deduct]" class="form-control inputChkout protect_<?php echo $key?>" value="<?php echo number_format($value["deduct"],2); ?>" >
														</div>
													</div>
												</div>

												<div class="bge3 titleprochk"><b>รถยนต์เสียหาย สูญหาย ไฟไหม้</b></div>
												<div class="row">
													<div class="col-md-7 t_r p5">ความเสียหายต่อตัวรถยนต์</div>
													<div class="col-md-5">
														<div class="form-group">
															<input type="text" name="ins[<?php echo $key?>][C536]" class="form-control inputChkout protect_<?php echo $key?>" value="<?php echo number_format($value["c536"],2); ?>" >
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-7 t_r p5">รถยนต์สูญหาย/ไฟไหม้</div>
													<div class="col-md-5">
														<div class="form-group">
															<input type="text" name="ins[<?php echo $key?>][C507]" class="form-control inputChkout protect_<?php echo $key?>" value="<?php echo number_format($value["c507"],2); ?>" >
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-7 t_r p5">คุ้มครองน้ำท่วม</div>
													<div class="col-md-5">
														<div class="form-group">
															<input type="text" name="ins[<?php echo $key?>][C508]" class="form-control inputChkout protect_<?php echo $key?>" value="<?php echo number_format($value["c508"],2); ?>" >
														</div>
													</div>
												</div>

												<div class="bge3 titleprochk"><b>ความคุ้มครองตามเอกสารแนบท้าย</b></div>
												<div class="row">
													<div class="col-md-7 t_r p5">อุบัติเหตุส่วนบุคคล</div>
													<div class="col-md-5">
														<div class="form-group">
															<input type="text" name="ins[<?php echo $key?>][C517]" class="form-control inputChkout protect_<?php echo $key?>" value="<?php echo number_format($value["c517"],2); ?>" >
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-7 t_r p5">ค่ารักษาพยาบาล</div>
													<div class="col-md-5">
														<div class="form-group">
															<input type="text" name="ins[<?php echo $key?>][C525]" class="form-control inputChkout protect_<?php echo $key?>" value="<?php echo number_format($value["c525"],2); ?>" >
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-7 t_r p5">การประกันตัวผู้ขับขี่ในคดีอาญา</div>
													<div class="col-md-5">
														<div class="form-group">
															<input type="text" name="ins[<?php echo $key?>][C532]" class="form-control inputChkout protect_<?php echo $key?>" value="<?php echo number_format($value["c532"],2); ?>" >
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-7 t_r p5">คุ้มครองจำนวน(คน)</div>
													<div class="col-md-5">
														<div class="form-group">
															<input type="text" name="ins[<?php echo $key?>][no_ofpax]" class="form-control inputChkout protect_<?php echo $key?>" value="<?php echo $value["no_ofpax"]; ?>" >
														</div>
													</div>
												</div>
											</div>
										</div>
									<?php  }
									} ?>
								</div>
							<div class="mt20">
								<b>หมายเหตุ</b> ส่วนลดกรอกได้เป็นจำนวน บาท เท่านั้น และต้องไม่เกินจำนวน 8% ของเบี้ยรวมภาษี	
							</div>
						</div>
						</div>
						<!-- END RIGHT COLUMN -->
					</form>
				</div>
			</div>
		</div>
	</div>
	<!-- END MAIN CONTENT -->
</div>
<?php include "include/inc_footer.php"; ?> 
<script type="text/javascript">
	// function calDown(key){
	// 	dis = $("#dis_"+key).val().replace(",","");
	// 	premium = parseFloat($("#premium_"+key).val().replace(",",""));
	// 	act = parseFloat($("#act"+key+" option:selected").val());
	// 	taxamount = premium - dis;
	// 	tax = (premium - dis) + act ;
	// 	netpremium = premium / 1.0743;
	// 	money1 = Math.ceil((taxamount * 40 ) / 100);
	// 	money2 = Math.ceil((taxamount - money1) / 2);
	// 	$("#money1_"+key).val(money1+act);
	// 	$("#money2_"+key).val(money2);
	// 	$("#money3_"+key).val(money2);
	// }
	function calDown(key){
		downpay = parseFloat($("#chkdownpay"+key+" option:selected").val());
		netpremium = parseFloat($("#netpremium_"+key).val());
    	premium = parseFloat($("#premium_"+key).val().replace(",",""));
    	act = parseFloat($("#act"+key+" option:selected").val());
    	install = parseFloat($("#install"+key+" option:selected").val());
    	dis = $("#dis_"+key).val().replace(",","");
    	taxamount = parseFloat(premium - dis);
		if(downpay){
			if(downpay == 3){
				percenDown = 5;
			}else if(downpay == 6){
				percenDown = 3;
			}
	    	discount = (netpremium  * percenDown ) / 100;
			discount2 = Math.ceil((premium - discount) / 100) * 100;
			discountSum = premium - discount2;
			sumpremium =  premium - discountSum;
	    	countDown = parseInt(downpay-1);
	    	sumDis1 = Math.ceil((sumpremium * 40 ) / 100) + act;
	    	$("#money1_"+key).val(sumDis1.toFixed(2));
	    }
	    if(install>0){
	    	installDown = Math.ceil(taxamount / install);
	   		$("#down_"+key).html(numCommas(installDown.toFixed(2)));
	   		$("#downs"+key).val(installDown.toFixed(2));
	   	}
			console.log('netpremium:'+netpremium);
		// $("#money1_"+key).val(money1+act);
		// $("#money2_"+key).val(money2);
		// $("#money3_"+key).val(money2);
	}
	$(document).ready(function(){
		 $(document).on('blur', '.discal', function() {
	        var galutinis = $(this).attr('id');
	        key = galutinis.replace("dis_","");
			disMax = parseFloat($("#disSum"+key).val());
			dis = $("#dis_"+key).val().replace(",","");
			var act = parseFloat($("#act"+key+" option:selected").val());
			$("#dis_"+key).val(numCommas(dis));
			tax = (parseFloat($("#premiumSum"+key).val()) - dis) + parseFloat(act);
			$("#taxamount"+key).val(numCommas(tax.toFixed(2)));
			calDown(key);
	    });

		$(document).on('blur', '.calPremium', function() {
	    	var premiumid = $(this).attr('id');
	        key = premiumid.replace("premium_","");
			dis = $("#dis_"+key).val().replace(",","");
			premium = parseFloat($("#premium_"+key).val().replace(",",""));
			act = parseFloat($("#act"+key+" option:selected").val());
			salecost = parseFloat($("#salecost"+key).val());
			percen_sale = parseFloat($("#percen_sale"+key).val());
			if(salecost > 0){
				discount = salecost;
			}else{
				discount = (premium  * percen_sale ) / 100;
			}
			tax = (premium - discount) + act ;
			netpremium = premium / 1.0743;
			$("#taxamount"+key).val(numCommas(tax.toFixed(2)));
			$("#netpremium_"+key).val(numCommas(netpremium.toFixed(2)));
			$("#dis_"+key).val(numCommas(discount.toFixed(2)));
			// console.log('discount:'+discount);
			calDown(key);
	    });
	});
	$( ".actprice" ).change(function() {
    	down = 0.00;
    	var pid = $(this).attr('id');
    	key = pid.replace("act","");
			var act = parseFloat($("#"+pid+" option:selected").val());
			premium = parseFloat($("#premium_"+key).val().replace(",",""));
			dis = parseFloat($("#dis_"+key).val().replace(",",""));
			installment1 = parseFloat($("#installment"+key).val().replace(",",""));
			if(installment1){
				down = Math.ceil((premium+act)/installment1).toFixed(2);
			}
			taxamount = premium - dis ;
			money1 = Math.ceil((taxamount * 40 ) / 100);
			money2 = Math.ceil((taxamount - money1) / 2);

			if(act > 0 ){
				var rs = taxamount + act;
				$("#taxamount"+key).val(numCommas(rs.toFixed(2)));
			}else{
				$("#taxamount"+key).val(numCommas(taxamount.toFixed(2)));
			}
		calDown(key);
		// 	// console.log('down:'+down);
		// $("#money1_"+key).val(money1+act);
		// $("#money2_"+key).val(money2);
		// $("#money3_"+key).val(money2);
		// // $("#moneys1_"+key).html(numCommas((money1+act).toFixed(2)));
		// // $("#moneys2_"+key).html(numCommas(money2.toFixed(2)));
		// // $("#moneys3_"+key).html(numCommas(money2.toFixed(2)));
		// $("#downs"+key).val(numCommas(down));
		// $("#down_"+key).html(numCommas(down));
	});

	$(".btnselecttitle").on('click', function() {
		var chk = $("input:checkbox[name='insSelect']").is(":checked");
	  	if(chk == true){
		  	var $box = $(this);
		  	key = $(this).attr('id').replace("select_","");
			// console.log('key:'+key);
		  	$("input:text").prop('readonly', true);
		  	$(".boxselect").removeClass("boxselect_active");
		  	$(".statusAc").val("");
		  	$("input:checkbox[name='insSelect']").prop("checked", false);
		  	$(".btnsubmitedit").addClass("dn");
		  	$(".btnsubmitPO").removeClass("dn");
		  	$("#action").val("sendUpdateLead");

		  	$(".boxselect_"+key).addClass("boxselect_active");
		  	$("#status_"+key).val("active");
		  	$("#statusMain").val("active");
			$("#insChk"+key).prop("checked", true);
			$("#package_name"+key).prop('readonly', false);
			$("#money1_"+key).prop('readonly', false);
			$("#money2_"+key).prop('readonly', false);
			$("#money3_"+key).prop('readonly', false);
			$("#money4_"+key).prop('readonly', false);
			$("#money5_"+key).prop('readonly', false);
			$("#money6_"+key).prop('readonly', false);
			$("#inscost_"+key).prop('readonly', false);
		    $("#dis_"+key).prop('readonly', false);
		    $("#inscost"+key).prop('readonly', false);
		    $("#premium_"+key).prop('readonly', false);
		    $(".protect_"+key).prop('readonly', false);
		}else{
			$("#statusMain").val("");
			$(".btnsubmitedit").removeClass("dn");
		  	$(".btnsubmitPO").addClass("dn");
			$("#action").val("sendUpdateLead");
			$("input:checkbox[name='insSelect']").prop("checked", false);
			$("input:text").prop('readonly', true);
			console.log('true:'+key);
		}

	});

	$(".btnsubmitPO").on('click', function() {
	  	var chk = $("input:checkbox[name='insSelect']").is(":checked");
	  	if(chk == true){
	  		if(confirm("ยืนยันการสร้างใบสั่งซื้อสินค้า")){
	  			$( "#sendUpdateLead" ).submit();
	  		}
	  	}else{
	  		alert("กรุณาเลือกข้อเสนอ 1 ข้อเสนอ เพื่อสร้างใบสั่งซื้อ");
	  	}

		  //   var group = "input:
	});
	$(".btnsubmitedit").on('click', function() {
  		if(confirm("ยืนยันแก้ไขเสนอราคา")){
  			$( "#sendUpdateLead" ).submit();
  			$(".btnsubmitedit").slideUp();
  		}

		  //   var group = "input:
	});

	 $( ".install" ).change(function() {
	    	var rs = 0;
	    	var ins = $(this).attr('id');
	    	key = ins.replace("install",""); 
	    	var install = parseFloat($("#"+ins+" option:selected").val());
		    var taxamount = parseFloat($("#taxamount"+key).val().replace(",",""));
	    	// console.log(taxamount);  
	    	if(install>0){
		    	var rs = Math.ceil(parseFloat(taxamount/install));
		    	$("#down_"+key).html(numCommas(rs));
		    	$("#downs"+key).val(rs);
	    	}else{
	    		$("#down_"+key).html(0);
	    		$("#downs"+key).val(0);	    		
	    	}
	    });
	    $( ".chkdownpay" ).change(function() {
	    	var	rs = ''; 
	    	var ins = $(this).attr('id');
	    	key = ins.replace("chkdownpay","");
	    	netpremium = parseFloat($("#netpremium_"+key).val().replace(",",""));
	    	var downpay = parseFloat($("#"+ins+" option:selected").val());
	    	premium = parseFloat($("#premium_"+key).val().replace(",",""));
	    	act = parseFloat($("#act"+key+" option:selected").val());
	    	dis = $("#dis_"+key).val().replace(",","");
	    	taxamount = parseFloat(premium - dis);
	    	
			if(downpay == 3){
				percenDown = 5;
			}else if(downpay == 6){
				percenDown = 3;
			}else{
				percenDown = 0;
			} 
	    	discount = (netpremium  * percenDown ) / 100;
			discount2 = Math.ceil((premium - discount) / 100) * 100;
			discountSum = premium - discount2;
			sumpremium =  premium - discountSum;
	    	countDown = parseInt(downpay-1);
	    	for (var i = 1; i <= downpay; i++) {
	    		if(i==1){
	    			sumDis1 = Math.ceil((sumpremium * 40 ) / 100) + act;
		    		rs += '<div class="col-md-4">';
		    		rs += '<div class="form-group">';
		    		rs += '<span class="fs12">งวดที่ '+i+'</span>';
		    		rs += '<input type="text" name="ins['+key+'][downcash]['+i+']" class="form-control inputChkout" id="money'+i+'_'+key+'" value="'+sumDis1+'">';
		    		rs += '</div>';
		    		rs += '</div>';
	    		}else{
	    			money2 = Math.ceil((sumpremium - ((sumpremium * 40 ) / 100)) / countDown);
	    			rs += '<div class="col-md-4">';
		    		rs += '<div class="form-group">';
		    		rs += '<span class="fs12">งวดที่ '+i+'</span>';
		    		rs += '<input type="text" name="ins['+key+'][downcash]['+i+']" class="form-control inputChkout" id="money'+i+'_'+key+'" value="'+money2+'">';
		    		rs += '</div>';
		    		rs += '</div>';
	    		}
	    	}
	    	$("#showdowncash"+key).html(rs);
	    	// 
	    });


</script>