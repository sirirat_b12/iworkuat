<?php 
ini_set("memory_limit","1200M");
session_start();
if(!isset($_SESSION["User"]['UserCode'])){
	echo "<script>alert('กรุณาเข้าสู่ระบบ!!!');</script>";
	echo '<META http-equiv="refresh" content="0;URL=login.php">';
	exit();
}
header('Content-Type: text/html; charset=utf-8');
// include "include/inc_header.php"; 
include "inc_config.php";
include "include/inc_function.php";
$pocode = $_GET["pocode"];
$api = getAllCustomerCase($pocode);
$sale = getAllSale($api["personnel_code"]);
$listapi = getAllCustomerlist($api["pocode"], $api["api_id"]);
$countdown = getCountdown($api["api_id"]);
$countAct = getCountACT($api["api_id"]);
$countInstall = getCountinstall($api["api_id"]);
// echo "<pre>".print_r($listapi,1)."</pre>";
include("mpdf/mpdf.php");

$mpdf = new mPDF('UTF-8','A4','','',5,5,5,5,10,10);
$mpdf->autoScriptToLang = true;
// $mpdf->SetDisplayMode('fullpage');
// $stylesheet = file_get_contents('assets/css/style.css');
// $mpdf->WriteHTML($stylesheet,1);

$html = 
'<style>
.container{
	font-family: thsaraban;
    font-size: 18px;
    line-height: 16px;
}
table{
	font-family: thsaraban;
    font-size: 16px;
    line-height: 15px;
}
p{
    text-align: justify;
}
h1{
    text-align: center;
}
.fs10{ font-size:10px;}
.fs12{ font-size:12px;}
.fs14{ font-size:14px;}
.fs16{ font-size:16px;}
.fwb{font-weight: bold;}
.t_c{text-align: center;}
.t_l{text-align: left;}
.t_r{text-align: right;}
.m0{margin:0px}
p{margin:0}
.bgea{background-color: #eaeaea;}
</style>
		<div class="container">
			<div class="t_c">
				<p class="t_r fs10">'.date("Y-m-d H:i:s").'</p>
				<img src="img/logo2.jpg" alt="" style="width:50px">
				<div class="fs18 fwb" style="margin-top:10px">บริษัท เอเชียไดเร็ค อินชัวร์รันส์ โบรคเกอร์ จำกัด</div>
				<div class="fs16">เลขที่ 626 อาคารบีบีดี ชั้น 11 ซอยจินดาถวิล ถนนพระราม 4 แขวงมหาพฤฒาราม เขตบางรัก กรุงเทพฯ 10500 โทร.02-089-2000</div>
			</div>
			<p  style="margin-top:10px"><b>เรื่อง/Subject</b> การเสนอเบี้ยประกันภัยรถยนต์/Proposal of motor insurance premium</p>
			<div >
				<div style="clear:both;margin-top:10px">
                    <div style="float: left; width: 45%; margin-bottom: 0pt; ">
                    	<p><b >เรียน/To </b><b>คุณ</b> '.$api["name"].' </p>
                    	<p><b >เลขอ้างอิง</b> '.$api["pocode"].'</p>
						<p><b>โทรศัพท์/Phone.</b> '.$api["phone"].' </p>
						<p><b>E-Mail:</b> '.$api["email"].'</p>
					</div>
                    <div style="float: right; width: 45%; margin-bottom: 0pt; ">
                    	<p><b >ผู้ขาย</b> '.$api["personnel_name"].'</p>
						<p><b>โทรศัพท์/Phone.</b> '.$sale["personnel_phone"].'</p>
						<p><b>E-Mail:</b> '.$sale["personnel_email"].' <br><b>line ID:</b> '.$sale["personnel_line"].'</p>
					</div>
                </div>  
                <div style="clear:both; padding-top:10px"> 
                    <b>รายละเอียดรถ</b>
                    <b style="color:#000;"><br>ยี่ห้อรถยนต์</b> '.$api["make"].' <b>รุ่นรถยนต์</b> '.$api["model"].' 
                    <b>ขนาดเครื่องยนต์</b> '.$api["cc"].' <b>ปีจดทะเบียน</b> '.$api["year"].'
                </div>
                <div style="margin-top:5px">
                ตามที่ท่านได้สอบถามเบี้ยประกันภัยรถยนต์ บริษัทฯ มีความยินดีที่จะเสนอรายละเอียดความคุ้มครองและเบี้ยประกันภัยดังนี้<br>
				 </div>';
		if($listapi){
			$html .= "
        		<table style='width:100%;margin-top: 10px;' border='1' cellspacing='0'>
                	<thead style='background-color: #55b7de;'>
						<tr>
							<th style='min-width:45%;padding: 2px;'>เงื่อนไขความคุ้มครอง</th> ";
					foreach ($listapi as $key => $data2) {
						$html .= "<th >ข้อเสนอ ".($key+1)."</th>";
					}
					$html .= "
						</tr>
					</thead>
					<tbody>
						<tr>
							<td style='text-align:right;padding: 3px;'><b>บริษัทประกัน</b></td>";
							foreach ($listapi as $key => $data2) {
								$html .= "<td style='padding: 3px;text-align: center;'>".$data2["insurername"]."</td>";
							}
					$html .= "
						</tr>
						<tr>
							<td style='text-align:right'><b>ประเภทประกัน</b></td>";
							foreach ($listapi as $key => $data2) {
								$html .= "<td style='padding: 3px;text-align: center;'> ประกันภัยชั้น ".$data2["class"]."</td>";
							}
					$html .= "
						</tr>
						<tr>
							<td style='text-align:right'><b>แพ็คเกจ</b></td>";
							foreach ($listapi as $key => $data2) {
								$html .= "<td style='padding: 3px;text-align: center;'>".$data2["package_name"]."</td>";
							}
					$html .= "
						</tr>
						<tr>
							<td style='text-align:right'><b>ประเภทการซ่อม</b></td>";
							foreach ($listapi as $key => $data2) { 
								$repair = ($data2["repairs"]=='hall') ? "ซ่อมห้าง" : "ซ่อมอู่";
								$html .= "<td style='padding: 3px;text-align: center;'>".$repair."</td>";
							}
					$html .= "
						</tr>
						<tr>
							<td style='text-align:center; padding: 3px;'><b>ความรับผิดต่อบุคคลภายนอก</b></td>
							<td colspan='3'></td>
						</tr>
						<tr>
							<td>1) ความเสียหายต่อชีวิต ร่างกาย หรืออนามัย (บาท/คน)</td>";
							foreach ($listapi as $key => $data2) { 
								$html .= "<td style='text-align:right; padding: 3px;'>".number_format($data2['c502'],2)."</td>";
							}
					$html .= "
						</tr>
						<tr>
							<td>ความเสียหายต่อชีวิต ร่างกาย หรืออนามัย (บาท/ครั้ง)</td>";
							foreach ($listapi as $key => $data2) { 
								$html .= "<td style='text-align:right; padding: 3px;'>".number_format($data2['c503'],2)."</td>";
							}
					$html .= "
						</tr>
						<tr>
							<td>2) ความเสียหายต่อทรัพย์สิน (บาท/ครั้ง)</td>";
							foreach ($listapi as $key => $data2) { 
								$html .= "<td style='text-align:right; padding: 3px;'>".number_format($data2['c504'],2)."</td>";
							}
					$html .= "
						</tr>
						<tr>
							<td>2.1) ความเสียหายส่วนแรก (บาท/ครั้ง)</td>";
							foreach ($listapi as $key => $data2) { 
								$html .= "<td style='text-align:right; padding: 3px;'>".number_format($data2['deduct'],2)."</td>";
							}
					$html .= "
						</tr>
						<tr>
							<td style='text-align:center; padding: 3px;'><b>รถยนต์เสียหาย สูญหาย ไฟไหม้</b></td>
							<td colspan='3'></td>
						</tr>";
					$html .= "
						</tr>
						<tr>
							<td>1) ความเสียหายต่อรถยนต์ (บาท/ครั้ง)</td>";
							foreach ($listapi as $key => $data2) { 
								$html .= "<td style='text-align:right; padding: 3px;'>".number_format($data2['c536'],2)."</td>";
							}
					$html .= "
						</tr>
						<tr>
							<td>2) ความเสียหายส่วนแรก (กรณีฝ่ายผิด) (บาท/ครั้ง)</td>";
							foreach ($listapi as $key => $data2) { 
								$html .= "<td style='text-align:right; padding: 3px;'>".number_format($data2['deduct'],2)."</td>";
							}
					$html .= "
						</tr>
						<tr>
							<td>3) รถยนต์สูญหาย/ไฟไหม้ (บาท)</td>";
							foreach ($listapi as $key => $data2) { 
								$html .= "<td style='text-align:right; padding: 3px;'>".number_format($data2['c507'],2)."</td>";
							}
					$html .= "
						</tr>
						<tr>
							<td style='text-align:center; padding: 3px;'><b>ความคุ้มครองตามเอกสารแนบท้าย</b></td>
							<td colspan='3'></td>
						</tr>";
					$html .= "
						</tr>
						<tr>
							<td>1) อุบัติเหตุส่วนบุคคล</td>";
							foreach ($listapi as $key => $data2) { 
								$html .= "<td style='text-align:right; padding: 3px;'>".number_format($data2['c517'],2)."</td>";
							}
					$html .= "
						</tr>
						<tr>
							<td>2) ค่ารักษาพยาบาล (คน) (บาท/คน)</td>";
							foreach ($listapi as $key => $data2) { 
								$html .= "<td style='text-align:right; padding: 3px;'>(".($data2['no_ofpax']+1).") ".number_format($data2['c525'],2)."</td>";
							}
					$html .= "
						</tr>
						<tr>
							<td>3) การประกันตัวผู้ขับขี่ (บาท/ครั้ง)</td>";
							foreach ($listapi as $key => $data2) { 
								$html .= "<td style='text-align:right; padding: 3px;'>".number_format($data2['c532'],2)."</td>";
							}
					$html .= "
						</tr>
						<tr class='bgea'>
							<td style='text-align:right;padding: 3px;'><b>เบี้ยสุทธิ</b></td>";
							foreach ($listapi as $key => $data2) { 
								$html .= "<td style='text-align:right; padding: 3px;'><b>".number_format($data2['netpremium'],2)."</b></td>";
							}
					$html .= "
						</tr>
						<tr class='bgea'>
							<td style='text-align:right;padding: 3px;'><b>เบี้ยประกันภัย</b></td>";
							foreach ($listapi as $key => $data2) { 
								$html .= "<td style='text-align:right; padding: 3px;'><b>".number_format($data2['premium'],2)."</b></td>";
							}
					$html .= "
						</tr>
						<tr class='bgea'>
							<td style='text-align:right;padding: 3px;'><b>ส่วนลด</b></td>";
							foreach ($listapi as $key => $data2) {
								if($data2['showdiscount'] == 1){
									$html .= "<td style='text-align:right; padding: 3px;'><b>".number_format($data2['discount'],2)."</b></td>";
								}else{
									$html .= "<td style='text-align:right; padding: 3px;'><b>0.00</b></td>";
								} 
								
							}
					$html .= "
						</tr>
						<tr class='bgea'>
							<td style='text-align:right;padding: 3px;'><b>เบี้ยประกันภัยรวมหลังหักส่วนลด</b></td>";
							foreach ($listapi as $key => $data2) { 
								$html .= "<td style='text-align:right; padding: 3px;'><b>".number_format($data2['taxamount'],2)."</b></td>";
							}
					$html .= "</tr>";

					if($countAct > 0){
						$html .= "
						<tr class='bgea'>
							<td style='text-align:right;padding: 3px;'><b>พรบ.</b></td>";
							foreach ($listapi as $key => $data2) { 
								$html .= "<td style='text-align:right; padding: 3px;'><b>".number_format($data2['actprice'],2)."</b></td>";
							}
						$html .= "</tr>";
					}

					$html .= "
						<tr class='bgea'>
							<td style='text-align:right;padding: 3px;'><b>เงินสด</b></td>";
							foreach ($listapi as $key => $data2) { 
								$html .= "<td style='text-align:right; padding: 3px;'><b>".number_format($data2['taxamount']+$data2['actprice'],2)."</b></td>";
							}
					$html .= "</tr>";

					// if($countdown > 0){
					// 	$html .= "
					// 	<tr class='bgea'>
					// 		<td style='text-align:right;padding: 3px;'><b>ผ่อนเงินสด (งวด) งวดที่ 1<br> งวดที่ 2-3</b></td>";
					// 		foreach ($listapi as $key => $data2) { 
					// 			if($data2['showdown'] > 0){
					// 				// echo getDown1($data2["apilists_id"]);
					// 				$html .= "<td style='text-align:right; padding: 3px;'><b>(".$data2['showdown'].") ".number_format(getDown1($data2["apilists_id"]),2)."<br>".number_format(getDown2($data2["apilists_id"]),2)."</b></td>";
					// 			}else{
					// 				$html .= "<td style='text-align:right; padding: 3px;'><b>-</b></td>";
					// 			}
					// 		}
					// 	$html .= "</tr>";
					// }
					if($countInstall>0){
						$html .= "
							<tr class='bgea'>
								<td style='text-align:right;padding: 3px;'><b>ผ่อน 0% สูงสุด (เดือน)</b></td>";
								foreach ($listapi as $key => $data2) { echo $data2['installment']; 
									if($data2['installment']){
										$html .= "<td style='text-align:right; padding: 3px;'><b>(".$data2['installment'].") ".number_format($data2['downs'],2)."</b></td>";
									}else{
										$html .= "<td style='text-align:right; padding: 3px;'><b>-</b></td>";
									}
								}
						$html .= "</tr>";
					}
					$html .= "
					</tbody>
                </table>   ";
        }
		$html .= '
			
			</div>
			<p style="margin: 10px 0px 0px 20px"> <b>หมายเหตุ</b>
			<br> - เอกสารประกอบการทำประกัน 1.สำเนากรมธรรม์เดิม 2.สำเนาบัตรประชาชน 3.สำเนาใบขับขี่(กรณีระบุชื่อ) 4.รูปถ่าย(กรณีประเภท1)
			<br> - หากลูกค้ามีการแจ้งเคลมหลังจากใบเสนอราคาฉบับนี้ เบี้ยประกันอาจมีการเปลี่ยนแปลง ซึ่งบริษัทจะแจ้งให้ทราบทันที
			<br> - แถมบริการช่วยเหลือฉุกเฉินบนท้องถนนฟรี 24 ชั่วโมง มูลค่าสูงสุด 2,900 บาท
			<br> - ผ่อนผ่านบัตรเครดิต กรุงไทย กสิกรไทย กรุงศรี โอมโปร เซ็นทรัล โลตัส เฟิร์สช๊อย
			<br> - กรณีผ่อนชำระ จะได้รับกรมธรรม์ฉบับจริงเมื่อชำระงวดสุดท้ายเรียบร้อยแล้ว
			<br> - บริษัทขอสงวนสิทธิ์ในการเปลี่ยนแปลงข้อมูล ทุนประกัน เบี้ยประกัน ความคุ้มครอง  โดยจะแจ้งให้ทราบในภายหลัง
			</p>
			<div></div>
		</div>
		';
// echo $html;
// echo $html;
$mpdf->SetWatermarkText('Asia Direct');
$mpdf->showWatermarkText = true;
$mpdf->WriteHTML($html);
$mpdf->Output();
exit;

?>