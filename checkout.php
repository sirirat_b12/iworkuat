<?php 
session_start();
header('Content-Type: text/html; charset=utf-8');
if(!isset($_SESSION["User"]['UserCode'])){
	echo "<script>alert('กรุณาเข้าสู่ระบบ!!!');</script>";
	echo '<META http-equiv="refresh" content="0;URL=login.php">';
	exit();
}

include "include/inc_header.php"; 
// include "include/inc_function.php"; 
include "include/inc_menu.php"; 
include "include/inc_function_offer.php"; 


// $class = getClass();
// $make = getMake();
// $insurer = getInsurer();
$ck_redbook = getRedbook($_GET["make"], $_GET["model"], $_GET["year"], $_GET["cc"]);
$goodretail = $ck_redbook["redbook_tks_goodretail"];
$province = getallProvince();
// echo "<pre>".print_r($_GET,1)."</pre>";
if($_GET){
	$getlistIns = getlistIns();
}
if($_GET["cpid"]){

	$getRowCPAllBycode = getRowCPAllBycode($_GET["cpid"]);
	$name = $getRowCPAllBycode["Customer_FName"]." ".$getRowCPAllBycode["Customer_LName"];
	$phone = ($getRowCPAllBycode["Tel_No"]) ? $getRowCPAllBycode["Tel_No"] : $getRowCPAllBycode["Mobile_No"];
	$getmodel = getGeneration($MakeMS); 
// echo "<pre>".print_r($getRowCPAllBycode,1)."</pre>";
}
// echo "<pre>".print_r($_SESSION['inslist'],1)."</pre>";
?> 
<div class="main">
			<!-- MAIN CONTENT -->
	<div class="main-content">
		<div class="container-fluid">
			<div class="panel panel-profile">
				<div class="clearfix">
					<form action="include/inc_action.php" method="POST" id="fromcheckout">
						<input type="hidden" name="action" id="action" value="sendleadnew" >
						<input type="hidden" name="make" value="<?php echo $_GET["make"]; ?>">
						<input type="hidden" name="model" value="<?php echo $_GET["model"]; ?>">
						<input type="hidden" name="cc" value="<?php echo $_GET["cc"]; ?>">
						<input type="hidden" name="year" value="<?php echo $_GET["year"]; ?>">
						<input type="hidden" name="class01" value="<?php echo $_GET["class"]; ?>">
						<div class="profile-left">
							
							<div class="boxSearch">
								<p class="text-center fs16 fwb cff8000">กรุณาเลือกกรอกข้อมูล</p>
								<!-- <div class="t_c">
									<input type="radio" name="customer" value="1" checked class="cus_ra">ลูกค้าใหม่
									<input type="radio" name="customer" value="2" class="cus_ra">ลูกค้าเก่า
								</div> -->
								<?php if($_SESSION["User"]['type'] != "Sale"){?>
								<div class="row">
									<div class="col-md-12 t_c">
										<div class="form-group">
											<input type="hidden" name="casenew"  value="casenew" />
											<input type="radio" name="callcase" value="1" /> ส่งข้อมูล
											<input type="radio" name="callcase" value="0" checked/> ลูกค้าตัวเอง 
										</div>
									</div>
								</div>
								<?php } ?>
								<div class="row mt15 " id="from02">
									<div class="col-md-6">
										<div class="form-group">
											<p class="fs14 c0aaaef">** กรณีลูกค้าเก่ามีชื่อในระบบ iBroker</p>
											<input class="form-control" type="text" name="cpid" id="cpid" placeholder="CPXXXXXXX" value="<?php echo $_GET["cpid"];?>" onkeyup="getDateONcp(this,event)">
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<p class="fs14 c0aaaef">เลือกเงื่อนไขส่งงาน</p>
											<input type="radio" name="isReplace" value="1" required checked/> แทนที่ทั้งหมด
											<input type="radio" name="isReplace" value="0" required/> เพิ่มรายการ 
										</div>
									</div>
								</div>
								<div class="row mt15" id="from01">
									<div class="col-md-6">
										<div class="form-group"> 
											<input class="form-control" type="text" name="name" id="name" placeholder="ชื่อ-นามสกุล*" value="<?php echo $name;?>" required >
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<input class="form-control" type="email" name="email" id="email" placeholder="อีเมล์"  >
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<input class="form-control" type="" name="phone" id="phone" value="<?php echo $phone;?>" placeholder="เบอร์โทรศัพท์*" maxlength="10" onkeyup="chknum(this,event)" required >
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<input class="form-control" type="text" name="line" id="line" placeholder="ไลน์" >
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<lable class="lable">เลขทะเบียน</lable>
											<input class="form-control" type="" name="plate_no" id="plate_no" placeholder="เลขทะเบียน"  >
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<lable class="lable">ทะเบียนจังหวัด</lable>
											<select id="plate_province" class="form-control formInput" name="plate_province" >
                        <option value="">ทะเบียนจังหวัด</option>
                        <?php foreach ($province as $key => $value) { ?>
                        	<option value="<?php echo $value["province_name"];?>"><?php echo $value["province_name"];?></option>
                        <?php } ?>
                      </select>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<lable class="lable">ประเภทตัวถัง</lable>
											<input class="form-control" type="" name="body_type" id="body_type" placeholder="ประเภทตัวถัง"  >
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<lable class="lable">เลขตัวถัง</lable>
											<input class="form-control" type="text" name="frame_no" id="frame_no" placeholder="เลขตัวถัง" >
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<lable class="lable">วันคุ้มครอง</lable>
											<input class="form-control" type="date" name="start_cover_date" id="start_cover_date" >
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<lable class="lable">ถึง</lable>
											<input class="form-control" type="date" name="end_cover_date" id="end_cover_date"  >
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<lable class="lable">แหล่งที่มา</lable>
											<select id="newfrom" class="form-control formInput" name="newfrom" required>
                        <option value="AsiaDirect">ไม่สามารถระบุได้</option>
		                    <option value="CallCenter">CallCenter</option>
		                    <option value="Line">Line</option>
		                    <option value="Interspace">Interspace</option>
		                    <option value="ไลน์พอยท์">LinePoint</option>
		                    <option value="Google">Google</option>
		                    <option value="WSM-walkIn">WSM-walkIn</option>
		                    <option value="Facebook">Facebook</option>
		                    <option value="FacebookBOT">Facebook BOT</option>
		                    <option value="Otherwebsites">เว็บไซต์อื่นๆ</option>
		                    <option value="Brochure">โบว์ชัวร์</option>
		                    <option value="Recommend">ผู้อื่นแนะนำ</option>
		                    <option value="Claim">เคลมประกัน</option>
		                    <option value="Internet Chat">Internet Chat</option>
		                    <option value="CHeroleads">CHeroleads</option>
		                    <option value="PopAds2000">PopAds2000</option>   
                    	</select>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<lable class="lable">หมายเหตุ</lable>
											<textarea class="form-control" rows="1" name="remark" placeholder="หมายเหตุ" ></textarea>
										</div>
									</div>
								</div>
								
							</div>
							<div class="boxList">
								<div id="divnMail" class="t_c">
									<h4 class="dn mss">กรุณารอ...</h4>
									<div class="fs12 text-left mb20">
										<b>หมายเหตุ</b>
										<br>- กรุณาตรวจสอบข้อมูลทุกครั้งก่อนทำการบันทึกข้อมูล เพื่อความถูกต้อง โดยเฉพาะข้อมูลลูกค้า จะไม่สามารถแก้ไขได้้จาก โปรแกรมเช็คเบี้ย
										<br>- ส่วนลด สามารถลดได้สูงสุด 8% เท่านั้น มากกว่านี้ต้องได้รับอนุมัตจาก sup
										<br>- ทุนประกันกลางเป็นราคากลางที่กำหนดไว้ ไม่สามารถใช้ได้ทุกบริษัทกรุณาตวจสอบอย่างละเอียด
									</div>
									<button type="button" class="btn btn btn-success btnFrom" onclick="submitFrom()" >ส่งใบเสนอราคา</button>
								</div>	
							</div>
						</div>
						<!-- END LEFT COLUMN -->
						<!-- RIGHT COLUMN -->
						<div class="profile-right">
							<h4 class="heading cff8000">ข้อเสนอประกันภัย</h4>
							<div class="panel-body">
								<div class="row">
									<?php 
										if($_SESSION['inslist']){ 
											foreach($_SESSION['inslist']['planid'] as $key => $value) {
												$money1 = 0; 
												$money2 = 0;
												$value = getRowInsur($_SESSION['inslist']['planid'][$key], $_SESSION['inslist']['costid'][$key]);
												$insurer = getRowInsurer($value["insurance_insurer"]);
												$insurPrine = ($value["inscost_maxamount"] == 0) ? $value["inscost_minamount"] : $goodretail;
												$protect1 = getProtect($value['insurance_id'], "ความรับผิดชอบต่อบุคคลภายนอก");
												$protect2 = getProtect($value['insurance_id'], "ความรับผิดต่อตัวรถยนต์");
												$protect3 = getProtect($value['insurance_id'], "ความคุ้มครองตามเอกสารแนบท้าย");
												// echo "<pre>".print_r($value,1)."</pre>";
												if($value["salecost"]){
											        $premium = $value["inscost_taxamount"] - $value["salecost"];
											        $discountSum = $value["salecost"];
												}else if($value["percen_sale"] != 0){
											        $discount = ($value["inscost_premamount"]  * $value["percen_sale"] ) / 100;
											        $premium = ceil(($value["inscost_taxamount"] - $discount) / 100) * 100;
											        $discountSum = $value["inscost_taxamount"] - $premium;
											    }else{
											        $premium = $value["inscost_taxamount"];
											    }

											    if($value["installment"]){
											    	$down = ceil($value["inscost_taxamount"]/$value["installment"]);
											    }
											    $money1 = ceil(($premium * 40 ) / 100);
												$money2 = ceil(($premium - $money1) / 2);

									?>
										<div class="fs14">
											<input type="hidden" name="ins[<?php echo $key?>][class]" value="<?php echo $value["insurance_type"]; ?>">
											<input type="hidden" name="ins[<?php echo $key?>][salecost]" id="salecost<?php echo $key?>" value="<?php echo $value["salecost"]; ?>">
											<input type="hidden" name="ins[<?php echo $key?>][percen_sale]" id="percen_sale<?php echo $key?>" value="<?php echo $value["percen_sale"]; ?>">
											<input type="hidden" name="ins[<?php echo $key?>][insurercode]" value="<?php echo $insurer["insurer_code"]; ?>">
											<input type="hidden" name="ins[<?php echo $key?>][insurername]" value="<?php echo $value["insurance_insurer"]; ?>">
											<input type="hidden" name="ins[<?php echo $key?>][cartype]" value="<?php echo $value["insurance_code"]; ?>">
											<input type="hidden" name="ins[<?php echo $key?>][insurance_id]" value="<?php echo $value["insurance_id"]; ?>">
											<input type="hidden" name="ins[<?php echo $key?>][insurance_cost_id]" value="<?php echo $value["inscost_id"]; ?>">
											<input type="hidden" name="ins[<?php echo $key?>][package_code]" value="<?php echo $value["insurance_Number"]; ?>">
											<!-- <input type="hidden" name="ins[<?php echo $key?>][package_name]" value="<?php echo $value["insurance_Name"]; ?>"> -->
											<input type="hidden" name="ins[<?php echo $key?>][repairs]" value="<?php echo $value["insurance_repair"]; ?>">
											<input type="hidden" name="ins[<?php echo $key?>][installment]" id="installment<?php echo $key?>" value="<?php echo $value["installment"]; ?>">
											<input type="hidden" name="ins[<?php echo $key?>][downs]" id="downs<?php echo $key?>" value="">
											<div class="col-md-4">
												<!-- <div class="bgffecd5 titleprochk "><b><?php echo $value["insurance_Name"]?></b></div> -->
												<div class="row">
													<div class="col-md-12 c0aaaef "><b>แพจเกต</b> </div>
													<div class="col-md-12">
														<div class="form-group">
															<input type="text" name="ins[<?php echo $key?>][package_name]" class="form-control " value="<?php echo $value["insurance_Name"]; ?>" readonly>
														</div>
													</div>
												</div>
												<div><b class="c0aaaef">บริษัท</b> <?php echo $value["insurance_insurer"];?></div>
												<div><b class="c0aaaef">รหัส</b> <?php echo $value["insurance_Number"] ? $value["insurance_Number"] : "-";?></div>
												<div><b class="c0aaaef">ช่วงทุนประกัน </b> <?php echo number_format($value["inscost_minamount"]).' - '.number_format($value["inscost_maxamount"]); ?> บาท 
												</div>
												<div class="row">
													<div class="col-md-7 t_r p5">ทุนประกัน</div>
													<div class="col-md-5">
														<div class="form-group">
															<input type="text" name="ins[<?php echo $key?>][inscost]" class="form-control inputChkout" value="<?php echo number_format($insurPrine,2); ?>" >
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-7 t_r p5">เบี้ยประกันสุทธิ</div>
													<div class="col-md-5">
														<div class="form-group">
															<input type="hidden" name="netpremium<?php echo $key?>" id="netpremium<?php echo $key?>" value="<?php echo $value["inscost_premamount"]; ?>">
															<input type="text" name="ins[<?php echo $key?>][netpremium]" class="form-control inputChkout"  value="<?php echo number_format($value["inscost_premamount"],2); ?>" id="netpremium_<?php echo $key?>" >
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-7 t_r p5">เบี้ยประกันรวม</div>
													<div class="col-md-5">
														<div class="form-group">
															<input type="hidden" name="premium<?php echo $key?>" id="premiumSum<?php echo $key?>" value="<?php echo $value["inscost_taxamount"]; ?>">
															<input type="text" name="ins[<?php echo $key?>][premium]" id="premium_<?php echo $key?>" class="form-control inputChkout calPremium" value="<?php echo number_format($value["inscost_taxamount"],2); ?>"  >
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-7 t_r p5">ส่วนลด</div>
													<div class="col-md-5">
														<div class="form-group">
															<input type="hidden" name="disSum<?php echo $key?>" id="disSum<?php echo $key?>" value="<?php echo $discountSum; ?>">
															<input type="text" name="ins[<?php echo $key?>][discount]" id="dis_<?php echo $key?>" class="form-control inputChkout discal" value="<?php echo number_format($discountSum,2); ?>" >
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-7 t_r p5">พรบ.</div>
													<div class="col-md-5">
														<div class="form-group">
															<select id="act<?php echo $key?>" class="form-control formInput actprice" name="ins[<?php echo $key?>][actprice]"  >
									                            <option value="0">ไม่มี</option>
									                            <option value="645.21">เก๋ง 645.21</option>
									                            <option value="967.28">กระบะ 967.28</option>
									                            <option value="1182.35">รถตู้ 1,182.35</option>
									                            <option value="2493.10">บรรทุก 2,493.10</option>
									                        </select>
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-7 t_r p5">จ่ายสุทธิ</div>
													<div class="col-md-5">
														<div class="form-group">
															<input type="text" name="ins[<?php echo $key?>][taxamount]" id="taxamount<?php echo $key?>" class="form-control inputChkout" value="<?php echo number_format($premium,2); ?>" >
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-7 t_r p5">แสดงส่วนลด</div>
													<div class="col-md-5">
														<div class="form-group">
															<input type="checkbox" name="ins[<?php echo $key?>][showdiscount]" id="" class="showDis" onclick="" value="1" checked>
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-7 t_r ">ผ่อนเงินสด</div>
													<div class="col-md-5">
														<div class="form-group">
															<select id="chkdownpay<?php echo $key?>" class="form-control formInput chkdownpay" name="ins[<?php echo $key?>][showdown]"  >
									                            <option value="0">ไม่มี</option>
									                            <option value="3">3 เดือน</option>
									                            <option value="6">6 เดือน</option>
									                        </select>
														</div>
													</div>
													<div class="row p15">
														<div id="showdowncash<?php echo $key?>"></div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-7 t_r ">ผ่อนบัตร 0%</div>
													<div class="col-md-5">
														<div class="form-group">
															<select id="install<?php echo $key?>" class="form-control formInput install" name="ins[<?php echo $key?>][install]"  >
									                            <option value="0">ไม่มี</option>
									                            <option value="3">3 เดือน</option>
									                            <option value="6">6 เดือน</option>
									                            <option value="10">10 เดือน</option>
									                            <option value="12">12 เดือน</option>
									                        </select>
														</div>
														<b>งวดละ</b> <span id="down_<?php echo $key?>">0</span> <b>บาท</b>
													</div>
												</div>
												<div class="bgd3f2ff titleprochk mt15"><b>ความรับผิดต่อบุคคลภายนอก</b></div>
												<?php foreach ($protect1 as $key1 => $valpro1) { ?>
													<div class="row">
														<div class="col-md-7 t_r p5"><?php echo $valpro1["protect_name"]; ?></div>
														<div class="col-md-5">
															<div class="form-group">
																<input type="text" name="ins[<?php echo $key?>][<?php echo $valpro1["protect_code"]; ?>]" class="form-control inputChkout" value="<?php echo number_format($valpro1["protect_cost"],2); ?>" >
															</div>
														</div>
													</div>
												<?php } ?>
												<div class="row">
													<div class="col-md-7 t_r p5">ความเสียหายส่วนแรก</div>
													<div class="col-md-5">
														<div class="form-group">
															<input type="text" name="ins[<?php echo $key?>][deduct]" class="form-control inputChkout" value="<?php echo number_format($value["insurance_deductible"],2); ?>" >
														</div>
													</div>
												</div>
												<div class="bgd3f2ff titleprochk"><b>รถยนต์เสียหาย สูญหาย ไฟไหม้</b></div>
												<?php 
													foreach ($protect2 as $key2 => $valpro2) { 
														if($valpro2["protect_code"] == "C508"){
															// $cost2 = ($valpro2["protect_cost"] == 1) ? $insurPrine : 0;
															if($valpro2["protect_cost"] == 1 ){
																$cost2 = $insurPrine;
															}else if($valpro2["protect_cost"] > 1){
																$cost2 = $valpro2["protect_cost"];
															}else{
																$cost2 = 0;
															}
														}else{
															$cost2 = ($valpro2["protect_cost"] == 0) ? $insurPrine : $valpro2["protect_cost"];
														}
												?>
													<div class="row">
														<div class="col-md-7 t_r p5"><?php echo $valpro2["protect_name"]; ?></div>
														<div class="col-md-5">
															<div class="form-group">
																<input type="text" name="ins[<?php echo $key?>][<?php echo $valpro2["protect_code"]; ?>]" class="form-control inputChkout" value="<?php echo number_format($cost2,2); ?>" >
															</div>
														</div>
													</div>
												<?php } ?>
												<div class="bgd3f2ff titleprochk"><b>ความคุ้มครองตามเอกสารแนบท้าย</b></div>
												<?php foreach ($protect3 as $key3 => $valpro3) { ?>
													<div class="row">
														<div class="col-md-7 t_r p5"><?php echo $valpro3["protect_name"]; ?></div>
														<div class="col-md-5">
															<div class="form-group">
																<input type="text" name="ins[<?php echo $key?>][<?php echo $valpro3["protect_code"]; ?>]" class="form-control inputChkout" value="<?php echo number_format($valpro3["protect_cost"],2); ?>" >
															</div>
														</div>
													</div>
												<?php } ?>
												<div class="row">
													<div class="col-md-7 t_r p5">คุ้มครองจำนวน(คน)</div>
													<div class="col-md-5">
														<div class="form-group">
															<input type="text" name="ins[<?php echo $key?>][no_ofpax]" class="form-control inputChkout" value="<?php echo $value["no_protect"]; ?>" >
														</div>
													</div>
												</div>
												<?php if($value["insurance_desc"]){ ?>
												<div class="row m15">
													<div class="col-md-12">
														<div class="fs12 pt10" style="border-top: 1px solid #c0c0c0; "><p class="fs14 cff8000">หมายเหตุ</p><?php echo nl2br($value["insurance_desc"]); ?></div>
													</div>
												</div>
												<?php } ?>
											</div>
										</div>
									<?php  }
									} ?>
								</div>
							<div class="mt20">
								<!-- <b>หมายเหตุ</b> ส่วนลดกรอกได้เป็นจำนวน บาท เท่านั้น และต้องไม่เกินจำนวน 8% ของเบี้ยรวมภาษี	 -->
							</div>
						</div>
						</div>
						<!-- END RIGHT COLUMN -->
					</form>
				</div>
			</div>
		</div>
	</div>
	<!-- END MAIN CONTENT -->
</div>
<?php include "include/inc_footer.php"; ?> 
<script type="text/javascript">

	function getDateONcp(val){
		cpid = $("#cpid").val();
		$.ajax({ 
			url: 'include/inc_action.php',
			type:'POST',
			data: {action: 'getDateONcp', cpid:cpid},
			success:function(rs){
				data = JSON.parse(rs);
				if(rs){
					$("#name").val(data.Customer_FName+" "+data.Customer_LName);
					$("#email").val(data.EMail);
					$("#phone").val(data.Tel_No);
					$("#line").val(data.Line_ID);
					$("#frame_no").val(data.Frame_No);
				  $("#body_type").val(data.Body_Type);
				  $("#plate_no").val(data.Plate_No);
				  $("#plate_province").val(data.Province_Name_TH);
				  CoverageStartDate = data.Coverage_Start_Date["date"].split(" ");
				  CoverageEndDate = data.Coverage_End_Date["date"].split(" ");
				  $("#start_cover_date").val(CoverageStartDate[0]);
					$("#end_cover_date").val(CoverageEndDate[0]);
				}
			}
		});
	}

	
	function calDown(key){
		downpay = parseFloat($("#chkdownpay"+key+" option:selected").val());
		netpremium = parseFloat($("#netpremium"+key).val());
    	premium = parseFloat($("#premium_"+key).val().replace(",",""));
    	act = parseFloat($("#act"+key+" option:selected").val());
    	install = parseFloat($("#install"+key+" option:selected").val());
    	dis = $("#dis_"+key).val().replace(",","");
    	taxamount = parseFloat(premium - dis);
		if(downpay){
			if(downpay == 3){
				percenDown = 5;
			}else if(downpay == 6){
				percenDown = 3;
			}
	    	discount = (netpremium  * percenDown ) / 100;
			discount2 = Math.ceil((premium - discount) / 100) * 100;
			discountSum = premium - discount2;
			sumpremium =  premium - discountSum;
	    	countDown = parseInt(downpay-1);
	    	sumDis1 = Math.ceil((sumpremium * 40 ) / 100) + act;
	    	$("#money1_"+key).val(sumDis1.toFixed(2));
	    }
	    if(install>0){
	    	installDown = Math.ceil(taxamount / install);
	   		$("#down_"+key).html(numCommas(installDown.toFixed(2)));
	   		$("#downs"+key).val(installDown.toFixed(2));
	   	}
			// console.log('chkdownpay:'+downpay);
		// $("#money1_"+key).val(money1+act);
		// $("#money2_"+key).val(money2);
		// $("#money3_"+key).val(money2);
	}
	function submitFrom(){
		name = $("#name").val();
		phone = $("#phone").val();
		if(name && phone){
			$(".btnFrom").slideUp();
			$(".mss").slideDown();
			$( "#fromcheckout").submit();
		}else{
			alert("กรุณากรอกชื่อและเบอร์โทร ");
		}
	}
	$(document).ready(function(){
	    $(document).on('blur', '.discal', function() {
	        var galutinis = $(this).attr('id');
	        key = galutinis.replace("dis_","");
					disMax = parseFloat($("#disSum"+key).val());
					dis = $("#dis_"+key).val().replace(",","");
					var act = parseFloat($("#act"+key+" option:selected").val());
					$("#dis_"+key).val(numCommas(dis));
					tax = (parseFloat($("#premiumSum"+key).val()) - dis) + parseFloat(act);
					$("#taxamount"+key).val(numCommas(tax.toFixed(2)));
					calDown(key);
	    });

	    $(document).on('blur', '.calPremium', function() {
	    		var premiumid = $(this).attr('id');
	        key = premiumid.replace("premium_","");
					dis = $("#dis_"+key).val().replace(",","");
					premium = parseFloat($("#premium_"+key).val().replace(",",""));
					act = parseFloat($("#act"+key+" option:selected").val());
					salecost = parseFloat($("#salecost"+key).val());
					percen_sale = parseFloat($("#percen_sale"+key).val());
					if(salecost > 0){
						discount = salecost;
					}else{
						discount = (premium  * percen_sale ) / 100;
					}
					tax = (premium - discount) + act ;
					netpremium = premium / 1.0743;
					$("#taxamount"+key).val(numCommas(tax.toFixed(2)));
					$("#netpremium_"+key).val(numCommas(netpremium.toFixed(2)));
					$("#dis_"+key).val(numCommas(discount.toFixed(2)));
					console.log('discount:'+discount);
					calDown(key);
	    });
	    $(document).on('blur', '#cpid', function() {
	    	var cpid = $("#cpid").val();
	    	// console.log('cpid:'+cpid);
	    	if(cpid){
	    		$('#cpid').prop('required',true);
	    		$('#phone').removeAttr('required');
	    		$('#email').attr("placeholder", "อีเมล์");
	    		$('#phone').attr("placeholder", "เบอร์โทรศัพท์");
	    	}else{
	    		$('#name, #phone').prop('required',true);
	    		$('#cpid').removeAttr('required');
	    		// $('#email').attr("placeholder", "อีเมล์");
	    		$('#phone').attr("placeholder", "เบอร์โทรศัพท์*");
	    	}
	    });	
	    $( ".actprice" ).change(function() {
	    	down = 0.00;
	    	var pid = $(this).attr('id');
	    	key = pid.replace("act","");
  			var act = parseFloat($("#"+pid+" option:selected").val());
  			premium = parseFloat($("#premium_"+key).val().replace(",",""));
  			dis = parseFloat($("#dis_"+key).val().replace(",",""));
  			money1 = parseFloat($("#money1_"+key).val());
  			// installment1 = parseFloat($("#installment"+key).val().replace(",",""));
  			// if(installment1){
  			// 	down = Math.ceil((premium+act)/installment1).toFixed(2);
  			// }
  			taxamount = premium - dis ;

  			if(act > 0 ){
  				var rs = taxamount + act;
  				$("#taxamount"+key).val(numCommas(rs.toFixed(2)));
  			}else{
  				$("#taxamount"+key).val(numCommas(taxamount.toFixed(2)));
  			}
  			calDown(key);
			// $("#money1_"+key).val(money1+act);
			});

	    $(document).on('blur', '#start_cover_date', function() {
	    	var start = $("#start_cover_date").val();
	    	if(start){
		    	var myDate = new Date(start);
		    	var rs = (myDate.getFullYear() + 1)+"-"+("0" + myDate.getMonth()).slice(-2)+"-"+myDate.getDate(); 
		    	$("#end_cover_date").val(rs);
	    	}
	    });	

	    $( ".cus_ra" ).change(function() {
	    	val = $('.cus_ra:checked').val();
	    	if(val==2){
	    		$("#from01").slideUp();
	    		$("#from02").slideDown();
	    		$('#cpid').prop('required',true);
	    		$('#name, #phone').removeAttr('required');
	    	}else{
	    		$("#from02").slideUp();
	    		$("#from01").slideDown();
	    		$('#cpid').removeAttr('required')
	    		$('#name, #phone').prop('required',true);
	    	}
	    	// console.log('from01:'+val);
		});
	    $('#chkdown').change(function() {
	       
	        if($(this).is(":checked")) {
            	// console.log("chkdown");  
        	}else{
        		// console.log("chkdown**");  
        	} 
	    });

	    $( ".install" ).change(function() {
	    	var rs = 0;
	    	var ins = $(this).attr('id');
	    	key = ins.replace("install","");
	    	var install = parseFloat($("#"+ins+" option:selected").val());
	    	if(install>0){
		    	var premium = parseFloat($("#premium_"+key).val().replace(",",""));
					var actprice = parseFloat($("#act"+key).val().replace(",",""));
					var rs = Math.ceil(parseFloat((premium+actprice)/install));
		    	$("#down_"+key).html(numCommas(rs));
		    	$("#downs"+key).val(rs);
	    	}else{
	    		$("#down_"+key).html(0);
	    		$("#downs"+key).val(0);	    		
	    	}
	    	// console.log(taxamount);  
	    });
	    $( ".chkdownpay" ).change(function() {
	    	var	rs = ''; 
	    	var ins = $(this).attr('id');
	    	key = ins.replace("chkdownpay","");
	    	netpremium = parseFloat($("#netpremium"+key).val());
	    	var downpay = parseFloat($("#"+ins+" option:selected").val());
	    	premium = parseFloat($("#premium_"+key).val().replace(",",""));
	    	act = parseFloat($("#act"+key+" option:selected").val());
	    	dis = $("#dis_"+key).val().replace(",","");
	    	taxamount = parseFloat(premium - dis);
	    	
			if(downpay == 3){
				percenDown = 5;
			}else if(downpay == 6){
				percenDown = 3;
			}
	    discount = (netpremium  * percenDown ) / 100;
			discount2 = Math.ceil((premium - discount) / 100) * 100;
			discountSum = premium - discount2;
			sumpremium =  premium - discountSum;
	    countDown = parseInt(downpay-1);
			// console.log("sumpremium:"+sumpremium);
			// console.log("countDown:"+countDown);
	    	for (var i = 1; i <= downpay; i++) {
	    		if(i==1){
	    			sumDis1 = Math.ceil((sumpremium * 40 ) / 100) + act;
		    		rs += '<div class="col-md-4">';
		    		rs += '<div class="form-group">';
		    		rs += '<span class="fs12">งวดที่ '+i+'</span>';
		    		rs += '<input type="text" name="ins['+key+'][downcash]['+i+']" class="form-control inputChkout" id="money'+i+'_'+key+'" value="'+sumDis1+'">';
		    		rs += '</div>';
		    		rs += '</div>';
	    		}else{
	    			money2 = Math.ceil((sumpremium - ((sumpremium * 40 ) / 100)) / countDown);
	    			rs += '<div class="col-md-4">';
		    		rs += '<div class="form-group">';
		    		rs += '<span class="fs12">งวดที่ '+i+'</span>';
		    		rs += '<input type="text" name="ins['+key+'][downcash]['+i+']" class="form-control inputChkout" id="money'+i+'_'+key+'" value="'+money2+'">';
		    		rs += '</div>';
		    		rs += '</div>';
	    		}
	    	}
	    	$("#showdowncash"+key).html(rs);
	    	// console.log(rs); 
	    });
	});


</script>