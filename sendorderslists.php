<?php 

session_start();
if(!isset($_SESSION["User"]['UserCode'])){
	echo "<script>alert('กรุณาเข้าสู่ระบบ!!!');</script>";
	echo '<META http-equiv="refresh" content="0;URL=login.php">';
	exit();
}
if(!$_GET["pocode"] && !$_GET["numcode"] && $_SESSION["User"]['type'] == "Sale" && $_SESSION["User"]['type'] == "Admin" && $_SESSION["User"]['type'] == "CallCenter"){
	echo '<META http-equiv="refresh" content="0;URL=sendorders.php">';
	exit();
}
header('Content-Type: text/html; charset=utf-8');
include "include/inc_header.php"; 
include "include/inc_menu.php"; 
$pocode = $_GET["pocode"];
$numcode = $_GET["numcode"];
$updateOpen = upDateQCOpenfile($numcode, $pocode);

$getPO = getPurchaseBycode($pocode);
$Cardetail = getCardetailBycode($pocode);
$getCommentlist = getCommentlist();
$getNoti = getNotiWorkByID($numcode);
$getFile = getNotiWorkFilesByID($numcode);
$getCoverageItem = getCoverageItem($pocode);
$getCheck = getNotiWorkCheck($numcode, $pocode);
$getPolicyDelivery = getPolicyDelivery($pocode);
$datakeyword = unserialize($getCheck["keyword"]);
$datadata = unserialize($getCheck["data"]);
$datachecklists = unserialize($getCheck["checklists"]);
$datacomments = unserialize($getCheck["comments"]);

// if($_SESSION["User"]['type'] == "SuperAdmin"){
// 	echo "<pre>".print_r($getPO,1)."</pre>";
// }


$addressCus =  $getPO["cusAddr1"]."<br><b>ตำบล/แขวง </b>".getSubdistrictBycode($getPO["cusSubdistrict"])."<b> อำเภอ/เขต </b>".getDistrictBycode($getPO["cusDistrict"])."<br><b> จังหวัด </b>".getProvinceBycode($getPO["cusProvince"])." <b>รหัสไปรษณีย์</b> ".$getPO["cusPost_Code"];

$addressCussend =  $getPO["cussendReceiver_Name"]."<br>".$getPO["cussendAddr1"]."<br><b>ตำบล/แขวง </b>".getSubdistrictBycode($getPO["cussendSubdistrict"])."<b> อำเภอ/เขต </b>".getDistrictBycode($getPO["cussendDistrict"])."<br><b> จังหวัด </b>".getProvinceBycode($getPO["cussendProvince"])." <b>รหัสไปรษณีย์</b> ".$getPO["cussendPost_Code"];
$addressPOsend =  $getPO["Receiver_Name"]."<br>".$getPO["posendAddr1"]."<br><b>ตำบล/แขวง </b>".getSubdistrictBycode($getPO["posendSubdistrict"])."<b> อำเภอ/เขต </b>".getDistrictBycode($getPO["posendDistrict"])."<br><b> จังหวัด </b>".getProvinceBycode($getPO["posendProvince"])." <b>รหัสไปรษณีย์</b> ".$getPO["posendPost_Code"];

if($getPO["Insurance_Name"] == "พรบ."){
	$netpremium = $getPO["Compulsory2"];
}else{
	$netpremium = $getPO["netpremium"];
}
?> 
<div class="main">
	<div class="main-content p20">
		<form action="include/inc_action_chk.php" method="post" id="frmsendAddlistchk"> 
			<input type="hidden" name="main[action]" id="action" value="sendAddlistchk">
			<input type="hidden" name="main[noti_id]" id="noti_id" value="<?php echo $getNoti["noti_work_id"]; ?>">
			<input type="hidden" name="main[pocode]" id="pocode" value="<?php echo $pocode; ?>">
			<input type="hidden" name="main[numcode]" id="numcode" value="<?php echo $numcode; ?>">
			<input type="hidden" name="main[insurer]" id="insurer" value="<?php echo $getNoti["insuere_company"]; ?>">
			<input type="hidden" name="main[personnel_code]" id="personnel_code" value="<?php echo $getNoti["personnel_code"]; ?>">
			<div class="p20">
				<div class="row">
					<div class="col-md-12">
						<div class="panel">
								<div class="panel-heading">
									<h4 class="panel-title"><b>ข้อมูลแจ้งงาน</b></h4>
									<span class="fs12"><b>รหัส : </b><?php echo $getNoti["noti_work_code"];?></span>
								</div>
								<div class="panel-body fs14">
									<div class="col-lg-8">
										<div class="row">
											<div class="col-md-6">
												<div class="row">
													<div class="col-md-4 t_r"><b>PO :</b></div>
													<div class="col-md-8"><?php echo $getNoti["po_code"];?></div>
												</div>
												<div class="row">
													<div class="col-md-4 t_r"><b>ประเภทประกัน :</b></div>
													<div class="col-md-8"><?php echo $getNoti["po_type"];?></div>
												</div>
												<div class="row">
													<div class="col-md-4 t_r"><b>ชื่อลูกค้า :</b></div>
													<div class="col-md-8"><?php echo $getNoti["cus_name"];?></div>
												</div>
												<div class="row">
													<div class="col-md-4 t_r"><b>วันเริ่มคุ้มครอง :</b></div>
													<div class="col-md-8"><?php echo $getNoti["start_cover_date"];?></div>
												</div>
												<div class="row">
													<div class="col-md-4 t_r"><b>ส่วนลดกล้อง :</b></div>
													<div class="col-md-8"><?php echo ($getNoti["discount_cctv"] == 1) ? "มี" : "ไม่มี";?></div>
												</div>
												<div class="row">
													<div class="col-md-4 t_r"><b>ของแถม :</b></div>
													<div class="col-md-8"><?php echo ($getNoti["giftvoucher"]) ? $getNoti["giftvoucher"] : "-";?></div>
												</div>
												<div class="row">
													<div class="col-md-4 t_r"><b>ของแถมให้ PO :</b></div>
													<div class="col-md-8"><?php echo ($getNoti["giftvoucher_po"]) ? $getNoti["giftvoucher_po"] : "-";?></div>
												</div>
												<!-- <div class="row">
													<div class="col-md-4 t_r"><b>การจัดส่งเอกสาร :</b></div>
													<div class="col-md-8"><?php echo ($getNoti["send_type"]) ? $getNoti["send_type"] : "-";?></div>
												</div>
											<?php if($getNoti["send_type"] == "พนักงานจัดส่ง"){?>
												<div class="row">
													<div class="col-md-4 t_r"><b>ที่อยู่ส่งเอกสาร :</b></div>
													<div class="col-md-8"><?php echo ($getNoti["send_addr"]) ? $getNoti["send_addr"] : "-";?></div>
												</div>
											<?php } ?> -->
												<div class="row">
													<div class="col-md-4 t_r"><b>หมายเหตุ :</b></div>
													<div class="col-md-8"><?php echo ($getNoti["notes"]) ? $getNoti["notes"] : "-";?></div>
												</div>
												<div class="row">
													<div class="col-md-4 t_r"><b>สลักหลัง :</b></div>
													<div class="col-md-8"><?php echo ($getNoti["endorse"]==1) ? "แจ้งสลักหลัง" : "-";?></div>
												</div>
												<div class="row">
													<div class="col-md-4 t_r"><b>ผู้แจ้งงาน :</b></div>
													<div class="col-md-8"><?php echo $getNoti["personnel_code"]." ". $getNoti["personnel_name"];?></div>
												</div>
												<?php if($getNoti["refer_po_code"]){ ?>
													<div class="row">
														<div class="col-md-4 t_r"><b>PO คู่ :</b></div>
														<div class="col-md-8">
															<a href="sendorderslists.php?pocode=<?php echo $getNoti["refer_po_code"];?>&numcode=<?php echo $getNoti["refer_work_code"];?>" target="_bank">
																<?php echo $getNoti["refer_po_code"];?>
															</a>
														</div>
													</div>
												<?php } ?>
												
											</div>
											<div class="col-md-6">
												<div class="row">
													<div class="col-md-4 t_r"><b>แพจเกต :</b></div>
													<div class="col-md-8"><?php echo $getNoti["package_name"];?></div>
												</div>
												<div class="row">
													<div class="col-md-4 t_r"><b>ประกันชั้น :</b></div>
													<div class="col-md-8"><?php echo $getNoti["insurance_type"];?></div>
												</div>
												<div class="row">
													<div class="col-md-4 t_r"><b>บริษัทประกัน :</b></div>
													<div class="col-md-8"><?php echo $getNoti["insuere_company"];?></div>
												</div>
												<div class="row">
													<div class="col-md-4 t_r"><b>ทุนประกัน :</b></div>
													<div class="col-md-4 t_r"><?php echo number_format($getNoti["insuere_cost"],2);?> <b>บาท</b></div>
												</div>
												<div class="row">
													<div class="col-md-4 t_r"><b>เบี้ยสุทธิ :</b></div>
													<div class="col-md-4 t_r"><?php echo number_format($getNoti["netpremium"],2);?> <b>บาท</b></div>
												</div>
												<div class="row">
													<div class="col-md-4 t_r"><b>เบี้ยรวมภาษี :</b></div>
													<div class="col-md-4 t_r"><?php echo number_format($getNoti["premium"],2);?> <b>บาท</b></div>
												</div>
												<div class="row">
													<div class="col-md-4 t_r"><b>ส่วนลด :</b></div>
													<div class="col-md-4 t_r"><?php echo number_format($getNoti["discount"],2);?> <b>บาท</b></div>
												</div>
												<div class="row">
													<div class="col-md-4 t_r"><b>รวม :</b></div>
													<div class="col-md-4 t_r"><?php echo number_format($getNoti["taxamount"],2);?> <b>บาท</b></div>
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-4" style="border-left: 1px solid #666;">
										<div class="mt15">
											<lable><b>สถานะ *</b></lable>
											<select class="form-control formInput2 fs12" name="main[status]" id="mainStatus" >
												<option value="0">รอตรวจสอบ</option>
												<option value="1">ไม่ผ่านตรวจสอบ</option>
												<option value="2">ผ่านการตรวจสอบ</option>
											</select>
										</div>
										<div class="mt15">
											<lable class="cff2da5"><b>ความเร่งด่วน *</b></lable>
											<input type="radio" name="main[urgent]" value="0" <?php  if($getNoti["urgent"] == 0){ echo 'checked'; } ?> > ไม่ด่วน
											<input type="radio" name="main[urgent]" value="1" <?php  if($getNoti["urgent"] == 1){ echo 'checked'; } ?>> ด่วน
										</div>
										<?php if($getCheck){ ?>
												<div class="mb0 mt15 fs12 fwb">หมายเหตุเดิม</div>
										<?php	foreach ($getCheck as $key => $value) { ?>
												<div class="fs10"><?php echo "<b>".date("d-m-Y H:i:s",strtotime($value["datetime"]))."</b> :: ".$value["notes"];?></div>
										<?php }
										} ?>
										<div class="mt15">
											<lable><b>หมายเหตุ *</b></lable>
											<textarea name="main[notes]" id="notes" style="width: 100%; font-size: 12px; height: 100px;" ></textarea>
										</div>
										<div class="t_c"><input type="button" value="ส่งข้อมูล" class="btn btn-success btnSendAdd" ></div>
									</div>
								</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="panel">
							<div class="panel-heading">
								<h4 class="panel-title"><b>ข้อมูลลูกค้า</b></h4>
								<div class="right">
									<button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
								</div>
							</div>
							<div class="panel-body fs12">
								<div class="row">
									<table class="table table-hover table-striped table-bordered">
										<thead>
											<tr>
												<th class="t_r" style="width: 25%;">รายการ</th>
												<th>ข้อมูล</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td class="t_r"><b>รหัส : </b></td>
												<td class="c1641ff">
													<?php echo $getPO["Customercode"];?>
													<input type="hidden" name="check[Customercode][data]" value="<?php echo $getPO["Customercode"];?>">
												</td>
											</tr>
											<tr>
												<td class="t_r"><b>การดำเนินการ : </b></td>
												<?php 
													$Reference = ($getPO["Reference_ID"]) ? " | ".$getPO["Reference_ID"] : "";
													$Operation = getOperationType($getPO["Operation_Type_ID"])." ".trim($Reference) ;
												?>
												<td class="c1641ff">
													<?php echo $Operation ;?>
													<input type="hidden" name="check[CustomerOperation][data]" value="<?php echo $Operation;?>">
												</td>
											</tr>
											<tr>
												<td class="t_r"><b>ประเภทลูกค้า : </b></td>
												<td class="c1641ff">
													<?php echo ($getPO["cus_type"] == "P") ? "Personal" : "Corporate";?>
													<input type="hidden" name="check[Customertype][data]" value="<?php echo ($getPO["cus_type"] == "P") ? "Personal" : "Corporate";?>">
												</td>
											</tr>
											<tr>
												<td class="t_r"><b>ชื่อ-นามสกุล TH : </b></td>
												<td class="c1641ff">
													<?php echo $getPO["Title_Name"]." ".$getPO["Customer_FName"]." ".$getPO["Customer_LName"];?>
													<input type="hidden" name="check[Customernameth][data]" value="<?php echo $getPO["Title_Name"]." ".$getPO["Customer_FName"]." ".$getPO["Customer_LName"] ;?> ">
												</td>
											</tr>
											<tr>
												<td class="t_r"><b>เลข ปชช. / เลขผู้ประกอบการ : </b></td>
												<td class="c1641ff">
													<?php echo $getPO["Customer_Folder"] ? $getPO["Customer_Folder"] : "-" ;?>
													<input type="hidden" name="check[cusFolder][data]" value="<?php echo $getPO["Customer_Folder"] ? $getPO["Customer_Folder"] : "" ;?>">
												</td>
											</tr>
											<tr>
												<td class="t_r"><b>ชื่อผู้ติดต่อ : </b></td>
												<td class="c1641ff">
													<?php echo $getPO["CustomerPerson"] ? $getPO["CustomerPerson"] : "" ;?>
													<input type="hidden" name="check[cusPerson][data]" value="<?php echo $getPO["CustomerPerson"] ? $getPO["CustomerPerson"] : "-" ;?>">
												</td>
											</tr>
											<tr>
												<td class="t_r"><b>โทรศัพท์ : </b></td>
												<td class="c1641ff">
													<?php echo $getPO["CustomerTel"] ? $getPO["CustomerTel"] : "-" ;?>
													<input type="hidden" name="check[cusTel][data]" value="<?php echo $getPO["CustomerTel"] ? $getPO["CustomerTel"] : "" ;?>">
												</td>
											</tr>
											<tr>
												<td class="t_r"><b>โทรสาร : </b></td>
												<td class="c1641ff">
													<?php echo $getPO["CustomerFax"] ? $getPO["CustomerFax"] : "-" ;?>
													<input type="hidden" name="check[cusFax][data]" value="<?php echo $getPO["CustomerFax"] ? $getPO["CustomerFax"] : "" ;?>">
												</td>
											</tr>
											<tr>
												<td class="t_r"><b>มือถือ : </b></td>
												<td class="c1641ff">
													<?php echo $getPO["CustomerMobile_No"] ? $getPO["CustomerMobile_No"] : "-" ;?>
													<input type="hidden" name="check[cusMobile][data]" value="<?php echo $getPO["CustomerMobile_No"] ? $getPO["CustomerMobile_No"] : "" ;?>">
													</td>
											</tr>
											</tr>
											<tr>
												<td class="t_r"><b>eMail : </b></td>
												<td class="c1641ff">
													<?php echo $getPO["EMail"] ? $getPO["EMail"] : "-" ;?>
													<input type="hidden" name="check[cusEmil][data]" value="<?php echo $getPO["EMail"] ? $getPO["EMail"] : "" ;?>">
												</td>
											</tr>
											</tr>
											<tr>
												<td class="t_r"><b>Mobile I : </b></td>
												<td class="c1641ff">
													<?php echo $getPO["Mobile_I"]? $getPO["Mobile_I"] : "-";?>
													<input type="hidden" name="check[cusMobileI][data]" value="<?php echo $getPO["Mobile_I"] ? $getPO["Mobile_I"] : "" ;?>">
												</td>
											</tr>
											<tr>
												<td class="t_r"><b>Mobile II : </b></td>
												<td class="c1641ff">
													<?php echo $getPO["Mobile_II"]? $getPO["Mobile_II"] : "-";?>
													<input type="hidden" name="check[cusMobileII][data]" value="<?php echo $getPO["Mobile_II"] ? $getPO["Mobile_II"] : "" ;?>">
												</td>
											</tr>
											<tr>
												<td class="t_r"><b>Land Line : </b></td>
												<td class="c1641ff">
													<?php echo $getPO["Land_Line"]? $getPO["Land_Line"] : "-";?>
													<input type="hidden" name="check[cusLand][data]" value="<?php echo $getPO["Land_Line"] ? $getPO["Land_Line"] : "" ;?>">
												</td>
											</tr>
											<tr>
												<td class="t_r"><b>Line ID : </b></td>
												<td class="c1641ff">
													<?php echo $getPO["Line_ID"]? $getPO["Line_ID"] : "-";?>
													<input type="hidden" name="check[cusLind][data]" value="<?php echo $getPO["Line_ID"] ? $getPO["Line_ID"] : "" ;?>">
												</td>
											</tr>
											<tr>
												<td class="t_r"><b>ที่อยู่ : </b></td>
												<td class="c1641ff">
													<?php echo $addressCus; ?>
													<input type="hidden" name="check[cusAddr][data]" value="<?php echo $addressCus ;?>">
												</td>
											</tr>
											<tr>
												<td class="t_r"><b>รายการเอาประกัน : </b></td>
												<td class="c1641ff">
													<?php echo $getPO["Properties_Insured"]; ?>
													<input type="hidden" name="check[cusProperties][data]" value="<?php echo $getPO["Properties_Insured"] ;?>">
												</td>
											</tr>
											<tr>
												<td class="t_r"><b>ที่อยู่ในการส่งเอกสาร : </b></td>
												<td class="c1641ff">
													<?php echo $addressCussend; ?>
													<input type="hidden" name="check[cusAddSend][data]" value="<?php echo $addressCussend ;?>">
												</td>
											</tr>
											<tr>
												<td class="t_r"><b>ประเภทการจัดส่ง : </b></td>
												<td class="c1641ff">
													<?php echo $getPolicyDelivery["Delivery_Type_Desc"];?>
													<input type="hidden" name="check[DeliveryTypeDesc][data]" value="<?php echo $getPolicyDelivery["Delivery_Type_Desc"] ;?>">
												</td>
											</tr>
											<?php if($getPolicyDelivery["Delivery_Type_ID"] == "002"){ ?>
											<tr>
												<td class="t_r"><b>พนักงานส่ง : </b></td>
												<td class="c1641ff">
													<?php echo $getPolicyDelivery["Messenger_Name"];?>
													<input type="hidden" name="check[MessengerName][data]" value="<?php echo $getPolicyDelivery["Messenger_Name"] ;?>">
												</td>
											</tr>
											<tr>
												<td class="t_r"><b>สถานที่พนักงานส่ง : </b></td>
												<td class="c1641ff">
													<?php echo $getPolicyDelivery["Delivery_Place"];?>
													<input type="hidden" name="check[DeliveryPlace][data]" value="<?php echo $getPolicyDelivery["Delivery_Place"] ;?>">
												</td>
											</tr>
											<?php } ?>
											<tr>
												<td class="t_r"><b>หมายเหตุในการส่งเอกสาร : </b></td>
												<td class="c1641ff">
													<?php echo $getPolicyDelivery["Remark"];?>
													<input type="hidden" name="check[DeliveryRemark][data]" value="<?php echo $getPolicyDelivery["Remark"] ;?>">
												</td>
											</tr>
											<tr>
												<td class="t_r"><b>สถานะการส่ง : </b></td>
												<td class="c1641ff">
													<?php echo $getPolicyDelivery["Delivery_Status_Desc"];?>
													<input type="hidden" name="check[DeliveryStatusDesc][data]" value="<?php echo $getPolicyDelivery["Delivery_Status_Desc"] ;?>">
												</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="panel">
							<div class="panel-heading">
								<h4 class="panel-title"><b>รายละเอียดประกัน</b></h4>
								<div class="right">
									<button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
								</div>
							</div>
							<div class="panel-body fs12">
								<div class="row">
									<table class="table table-hover table-striped table-bordered">
										<thead>
											<tr>
												<th class="t_r" style="width: 15%;">รายการ</th>
												<th>ข้อมูล</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td class="t_r"><b>แพจเกต</b></td>
												<td class="c1641ff">
													<?php echo $getPO["Insurance_Package_Name"];?>
													<input type="hidden" name="check[insurPackageName][data]" value="<?php echo  $getPO["Insurance_Package_Name"]; ?>">
												</td>
											</tr>
											<tr>
												<td class="t_r"><b>เริ่มวันคุ้มครอง</b></td>
												<td class="c1641ff">
													<?php echo $getPO["Coverage_Start_Date"]->format('d-m-Y');?>
													<input type="hidden" name="check[insurCoverageStart][data]" value="<?php echo $getPO["Coverage_Start_Date"]->format('d-m-Y');?>">
												</td>
											</tr>
											<tr>
												<td class="t_r"><b>สิ้นสุดวันคุ้มครอง</b></td>
												<td class="c1641ff">
													<?php echo $getPO["Coverage_End_Date"]->format('d-m-Y');?>
													<input type="hidden" name="check[insurCoverageEnd][data]" value="<?php echo $getPO["Coverage_End_Date"]->format('d-m-Y');?>">
												</td>
											</tr>
											<tr>
												<td class="t_r"><b>ประกันชั้น</b></td>
												<td class="c1641ff">
													<?php echo $getPO["Insurance_Name"];?>
													<input type="hidden" name="check[insurTypeIns][data]" value="<?php echo  $getPO["Insurance_Name"]; ?>">
												</td>
											</tr>
											<tr>
												<td class="t_r"><b>บรัษัท</b></td>
												<td class="c1641ff">
													<?php echo $getPO["Insurer_Name"];?>
													<input type="hidden" name="check[insurNameInsur][data]" value="<?php echo  $getPO["Insurer_Name"]; ?>">
												</td>
											</tr>
											<tr>
												<td class="t_r"><b>ทุนประกัน</b></td>
												<td class="c1641ff">
													<?php echo number_format($getPO["insuere_cost"],2);?>
													<input type="hidden" name="check[insurCost][data]" value="<?php echo  $getPO["insuere_cost"]; ?>">
												</td>
											</tr>
											<tr>
												<td class="t_r"><b>เบี้ยสุทธิ</b></td>
												<td class="c1641ff">
													<?php echo number_format($netpremium,2);?>
													<input type="hidden" name="check[insurNetpremium][data]" value="<?php echo  number_format($netpremium,2); ?>">
												</td>
											</tr>
											<tr>
												<td class="t_r"><b>เบี้ยรวมภาษี</b></td>
												<td class="c1641ff">
													<?php echo number_format($getPO["Total_Premium"],2);?>
													<input type="hidden" name="check[insurPremium][data]" value="<?php echo  number_format($getPO["Total_Premium"],2); ?>">
												</td>
											</tr>
											<tr>
												<td class="t_r"><b>ส่วนลด</b></td>
												<td class="c1641ff">
													<?php echo number_format($getPO["Discount"],2);?>
													<input type="hidden" name="check[insurDiscount][data]" value="<?php echo  number_format($getPO["Discount"],2); ?>">
												</td>
											</tr>
											<tr>
												<td class="t_r"><b>เบี้ยรวมภาษี</b></td>
												<td class="c1641ff">
													<?php echo number_format($getPO["Premium_After_Disc"],2);?>
													<input type="hidden" name="check[insurTaxamount][data]" value="<?php echo number_format($getPO["Premium_After_Disc"],2); ?>">
												</td>
											</tr>
											<tr>
												<td class="t_r"><b>คุ้มครอง</b></td>
												<td class="">
													<?php 
														$contxt = "";
														foreach ($getCoverageItem as $key => $value) {
														$covID = $value["Coverage_Item_ID"]; 
														$Coverage[$covID] = $value["Coverage_Value"] ;
														
														$contxt .=	"<div><span>".$value["Coverage_Item_Desc"]."</span>";
															if($value["Coverage_Value"]){ 
																$contxt .=	" <span><b> ".$value["Coverage_Value"]."</span> ".$value["Coverage_Item_Unit"]."</b>";
															} 
														$contxt .= "</div>";
													} 
													echo $contxt ; 
													?>
													<input type="hidden" name="check[insurCoverageItem][data]" value="<?php echo  $contxt; ?>">
												</td>
											</tr>
											<tr>
												<td class="t_r"><b>ที่อยู่ในการจัดส่งเอกสาร : </b></td>
												<td class="c1641ff">
													<?php echo $addressPOsend ;?>
													<input type="hidden" name="check[carPOAddrSend][data]" value="<?php echo $Cardetail["addressPOsend"] ;?>">
												</td>
											</tr>
											<tr>
												<td class="t_r"><b>หมายเหตุการจัดส่งเอกสาร : </b></td>
												<td class="c1641ff">
													<?php echo $getPO["posendPostRemark"];?>
													<input type="hidden" name="check[insurPOAddrSendRemark][data]" value="<?php echo $getPO["posendPostRemark"] ;?>">
												</td>
											</tr>
											
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="panel">
							<div class="panel-heading">
								<h4 class="panel-title"><b>รายละเอียดรถ</b></h4>
								<div class="right">
									<button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
								</div>
							</div>
							<div class="panel-body fs12">
								<div class="row">
									<table class="table table-hover table-striped table-bordered">
										<thead>
											<tr>
												<th class="t_r" style="width: 30%;">รายการ</th>
												<th>ข้อมูล</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td class="t_r"><b>เลขทะเบียน : </b></td>
												<td class="c1641ff">
													<?php echo $Cardetail["Plate_No"]." ".getProvinceBycode($Cardetail["Plate_Province"]);?>
													<input type="hidden" name="check[carPlate][data]" value="<?php echo $Cardetail["Plate_No"]." ".getProvinceBycode($Cardetail["Plate_Province"]);?>">
												</td>
											</tr>
											<tr>
												<td class="t_r"><b>ยี่ห้อรถ : </b></td>
												<td class="c1641ff">
													<?php echo $Cardetail["Make_Name_TH"];?>
													<input type="hidden" name="check[carMake][data]" value="<?php echo $Cardetail["Make_Name_TH"] ;?>">
												</td>
											</tr>
											<tr>
												<td class="t_r"><b>รุ่น : </b></td>
												<td class="c1641ff">
													<?php echo trim($Cardetail["Model_Desc"]);?>
													<input type="hidden" name="check[carModel][data]" value="<?php echo trim($Cardetail["Model_Desc"]) ;?>">
												</td>
											</tr>
											<tr>
												<td class="t_r"><b>รุ่นย่อย/ปี : </b></td>
												<td class="c1641ff">
													<?php echo $Cardetail["Sub_Model"] ? $Cardetail["Sub_Model"] : "-" ;?>
													<input type="hidden" name="check[carsubModel][data]" value="<?php echo $Cardetail["Sub_Model"] ;?>">
												</td>
											</tr>
											<tr>
												<td class="t_r"><b>รหัสรถยนต์ : </b></td>
												<td class="c1641ff">
													<?php echo $Cardetail["Car_Code"] ? $Cardetail["Car_Type_Desc"] : "-" ;?>
													<input type="hidden" name="check[carCodes][data]" value="<?php echo $Cardetail["Car_Type_Desc"] ;?>">
												</td>
											</tr>
											<tr>
												<td class="t_r"><b>ประเภทตัวถัง : </b></td>
												<td class="c1641ff">
													<?php echo $Cardetail["Body_Type"] ? $Cardetail["Body_Type"] : "-" ;?>
													<input type="hidden" name="check[carbodyType][data]" value="<?php echo $Cardetail["Body_Type"] ;?>">
												</td>
											</tr>
											<tr>
												<td class="t_r"><b>เลขตัวถัง : </b></td>
												<td class="c1641ff">
													<?php echo $Cardetail["Frame_No"] ? $Cardetail["Frame_No"] : "-" ;?>
													<input type="hidden" name="check[carFrame][data]" value="<?php echo $Cardetail["Frame_No"] ;?>">
												</td>
											</tr>
											<tr>
												<td class="t_r"><b>อุปกรณ์ตกแต่ง : </b></td>
												<td class="c1641ff">
													<?php echo $Cardetail["Accessory"] ? $Cardetail["Accessory"] : "-" ;?>
													<input type="hidden" name="check[carAccessory][data]" value="<?php echo $Cardetail["Accessory"] ;?>">
												</td>
											</tr>
											<tr>
												<td class="t_r"><b>ที่นั่ง/ซีซี/น้ำหนัก : </b></td>
												<td class="c1641ff">
													<?php echo $Cardetail["Seat_CC_Weight"] ? $Cardetail["Seat_CC_Weight"] : "-" ;?>
													<input type="hidden" name="check[carSeat][data]" value="<?php echo $Cardetail["Seat_CC_Weight"] ;?>">
												</td>
											</tr>
											<tr>
												<td class="t_r"><b>ประเภทการซ่อม : </b></td>
												<td class="c1641ff">
													<?php echo ($Cardetail["Repair_Type"] == "C") ? "ซ่อมห้าง" : "ซ่อมอู่" ;?>	
													<input type="hidden" name="check[carRepair][data]" value="<?php echo ($Cardetail["Repair_Type"] == "C") ? "ซ่อมห้าง" : "ซ่อมอู่" ;?>">
												</td>
											</tr>
											<tr>
												<td class="t_r"><b>จำนวนผู้ขับขี่ : </b></td>
												<td class="c1641ff">
													<?php echo $Cardetail["NoOf_Driver"] ? $Cardetail["NoOf_Driver"] : "-" ;?>
													<input type="hidden" name="check[carNoOf][data]" value="<?php echo $Cardetail["NoOf_Driver"] ;?>">
												</td>
											</tr>
											<tr>
												<?php 
													if($Cardetail["Driver_Name"]){
														$Driver1 = "<b>ชื่อ</b> ".$Cardetail["Driver_Name"]." <br><b>เลขบัตร ปชช.</b> ".$Cardetail["Receiver"]." <br><b>ว/ด/ป เกิด</b> ".$Cardetail["Driver_DOB"]->format('Y-m-d')." <br><b>เลขที่ใบขับขี่</b> ".$Cardetail["Driver_License_No"] ; 
													}
												?>
												<td class="t_r"><b>ผู้ขับขี 1 : </b></td>
												<td class="c1641ff">
													<?php echo $Cardetail["Driver_Name"] ? $Driver1 : "-";?>	
													<input type="hidden" name="check[carDriver][data]" value="<?php echo $Cardetail["Driver_Name"] ? $Driver1 : "";?>">
												</td>
											</tr>
											<tr>
												<?php 
													if($Cardetail["Driver_Name"]){
														$Driver2 = "<b>ชื่อ</b> ".$Cardetail["Co_Driver_Name"]." <br><b>เลขบัตร ปชช.</b> ".$Cardetail["Sender"]." <br><b>ว/ด/ป เกิด</b> ".$Cardetail["Co_Driver_DOB"]->format('Y-m-d H:i:s')." <br><b>เลขที่ใบขับขี่</b> ".$Cardetail["Co_Driver_License_No"]; 
													}
												?>
												<td class="t_r"><b>ผู้ขับขี 2 : </b></td>
												<td class="c1641ff">
													<?php echo $Cardetail["Co_Driver_Name"] ? $Driver2 : "-";?>
													<input type="hidden" name="check[carCoDriver][data]" value="<?php echo $Cardetail["Co_Driver_Name"] ? $Driver2 : ""; ;?>">
												</td>
											</tr>
											<tr>
												<td class="t_r"><b>ผู้รับผลประโยชน์ : </b></td>
												<td class="c1641ff">
													<?php echo $Cardetail["Beneficiary_Name"] ? $Cardetail["Beneficiary_Name"] : "-";?>
													<input type="hidden" name="check[carBeneficiary][data]" value="<?php echo $Cardetail["Beneficiary_Name"] ;?>">
												</td>
											</tr>
											<tr>
												<td class="t_r"><b>หมายเหตุการแจ้งงาน [ใน iBroker]: </b></td>
												<td class="c1641ff">
													<?php echo $getPO["Deductible"] ? $getPO["Deductible"] : "-";?>
													<input type="hidden" name="check[carDeductible][data]" value="<?php echo $getPO["Deductible"] ;?>">
												</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="panel">
							<div class="panel-heading">
								<h4 class="panel-title"><b>รูปและเอกสาร</b></h4>
								<div class="right">
									<button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
								</div>
							</div>
							<div class="panel-body fs12">
								<div class="row">
									<table class="table table-hover table-striped table-bordered">
										<thead>
											<tr>
												<th class="t_r" style="width: 30%;">รายการ</th>
												<th>ข้อมูล</th>
											</tr>
										</thead>
										<tbody>
											<?php 
												foreach ($getFile as $key => $value) { 
													$path  = ($value["pathfile"]) ? $value["pathfile"]."/" : ""; 
													$fileType = explode(".", $value["file_name"]);
												?>
												<tr>
													<td class="t_r"><b><?php echo caseTitle("file".$value["file_type"]);?> </b></td>
													<td class="c1641ff">
														<?php if($fileType[1] == "png" || $fileType[1] == "jpg"){ ?>
															<a href="myfile/<?php echo $path."".$value["file_name"]; ?>" data-fancybox="watermark" class="fancybox">
																<img src="myfile/<?php echo $path."".$value["file_name"]; ?>" alt="" class="img-responsive rounded" style="width: 150px;max-height: 150px;">
															</a>
														<?php }else{?>
															<a href="myfile/<?php echo $path."".$value["file_name"]; ?>" data-fancybox="watermark" class="fancybox">
																<?php echo $value["file_name"]; ?>
															</a>
														<?php } ?>
														<input type="hidden" name="check[file<?php echo $value["file_type"]; ?>][data]" value="<?php echo $path."".$value["file_name"]; ?>">
													</td>
												</tr>
											<?php } ?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
<?php include "include/inc_footer.php"; ?> 
<script src="fancybox/dist/jquery.fancybox.min.js"></script>
<script type="text/javascript">
	$(".rd_chk").click(function() {
		val = $('.rd_chk:checked').val();
		name = $(this).attr("name");
		repName = name.replace("chk_", "comment_");
		if(val == "0"){
				$("#comment_"+repName).show(); 
			}else{
				$("#comment_"+repName).hide();
			}
			
	});
	function funChk(name) {
		// console.log("input[name='check["+name+"][chk]']:checked");
		val = $("input[name='check["+name+"][chk]']:checked").val(); 
		console.log(val);
		if(val == 0){
				$("#comment_"+name).show();
			}else{
				$("#comment_"+name).hide();
			}

	}
$(document).ready(function() {
	$(".btnSendAdd").click(function() {  console.log("btnSendAdd");
		status = $("#mainStatus").val();
		if(status != 0){
			if( confirm("ตรวจสอบข้อมูลสมบูรณ์ ต้องการบันทึกผลการประเมิน !!!") ){
				// console.log("btnSendAdd");
				$("#frmsendAddlistchk").submit();
			}
		}else{
			alert("กรุณาเลือกสถานะ!!!");
		}
		
	});
});

$( '[data-fancybox="watermark"]' ).fancybox({
  buttons : [
    'download',
    'zoom',
    'close'
  ],
  protect : false,
  animationEffect : "zoom-in-out",
  transitionEffect : "fade",
  zoomOpacity : "auto",
  animationDuration : 500,
  zoomType: 'innerzoom',

});
</script>