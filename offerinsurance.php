<?php 
session_start();
date_default_timezone_set("Asia/Bangkok");
if(!isset($_SESSION["User"]['UserCode'])){
	echo "<script>alert('กรุณาเข้าสู่ระบบ!!!');</script>";
	echo '<META http-equiv="refresh" content="0;URL=login.php">';
	exit();
} 
header('Content-Type: text/html; charset=utf-8');
include "include/inc_header.php"; 
include "include/inc_menu.php"; 
include "include/inc_function_offer.php";

$UserCode = $_SESSION["User"]['UserCode'];
$getInsuranceType = getInsuranceType();
$getPersonel = getPersonnelByType();
$getCompareStatusAll = getCompareStatusAll();
// echo "<pre>".print_r($_POST,1)."</pre>";

if($_POST){
	$getCPoffer = getCPoffer($_POST["SearchOption"], $_POST["DefaultSearch"], $_POST["insurance_type"], $_POST["MyUser"], $_POST["CompareType"], $_POST["CompareStatus"], $_POST["CPDateFrom"], $_POST["CPDateTo"], $_POST["CEDateFrom"], $_POST["CEDateTo"], $_POST["CPCreateFrom"], $_POST["CPCreateTo"]);
}

// echo "<pre>".print_r($getCPoffer,1)."</pre>";
?> 
<div class="main">
	<div class="main-content p20">
		<div class=" bgff"> 
			<div class="p20">
				<div><h3 class="dib">รายการข้อเสนอประกันภัย </h3> <a href="offeradd.php" class="btn btn-danger">สร้างข้อเสนอ</a></div>
				<div style="background: #e9fcff;padding: 10px;margin-top: 15px;">
					<form action="offerinsurance.php" method="POST" >
						<div class="row">
							<div class="col-md-2">
								<div class="form-group">
									<label for="SearchOption">เลือก</label><br>
									<select class="form-control formInput2 formInput" name="SearchOption" id="SearchOption" >
			              <option value="ContactNo" <?php if($_POST["SearchOption"] == "ContactNo"){ echo "selected"; } ?> >หมายเลขติดต่อ</option>
			              <option value="CPID" <?php if($_POST["SearchOption"] == "CPID"){ echo "selected";} ?> >รหัสข้อเสนอ</option>
			              <option value="CustomerName" <?php if($_POST["SearchOption"] == "CustomerName"){ echo "selected";} ?> >ชื่อลูกค้า</option>
				          </select>
								</div>
							</div>
							<div class="col-md-2">
								<div class="form-group">
									<label for="DefaultSearch">ข้อมูล</label><br>
									<input class="form-control formInput2" type="text" name="DefaultSearch"  id="DefaultSearch" value="<?php echo $_POST["DefaultSearch"];  ?>" >
								</div>
							</div>
							<div class="col-md-2">
								<div class="form-group">
									<label for="MyUser">พนักงาน</label><br>
			              <select class="form-control formInput2 fs12" name="MyUser"  id="MyUser">
			              	<?php if($_SESSION["User"]['type'] == "Sale"){ ?>
			              		<option value="<?php echo $UserCode;  ?>"><?php echo $UserCode." ".$_SESSION["User"]['firstname']." ".$_SESSION["User"]['lastname'];  ?></option>
			              	<?php }else{ ?>
			              		<!-- <option value="ADB59014" selected>ADB59014</option>
			              		<option value="ADB59017" >ADB590 -->17</option>
												<option value="" >กรุณาเลือก</option>
												<?php foreach ($getPersonel as $key => $value) { ?>
													<option value="<?php echo  $value["personnel_code"]; ?>" <?php if($_POST["MyUser"] == $value["personnel_code"] ){ echo "selected"; } ?> >
														<?php echo $value["personnel_code"]." ".$value["personnel_firstname"]." ".$value["personnel_lastname"] ?>	
													</option>
												<?php } ?>
											<?php } ?>
										</select>
								</div>
							</div>
							<div class="col-md-2">
								<div class="form-group">
									<label for="start_cover_date">สถานะ</label><br>
			              <select class="form-control formInput2 fs12" name="insurance_type" id="insurance_type" >
											<option value="active" <?php if($_POST["insurance_type"] == "active"){ echo "selected";} ?>>ใช้งาน</option>
											<option value="all" <?php if($_POST["insurance_type"] == "all"){ echo "selected";} ?>>ทั้งหมด</option>
											<option value="done" <?php if($_POST["insurance_type"] == "done"){ echo "selected";} ?>>เรียบร้อย</option>
										</select>
								</div>
							</div>
							<div class="col-md-2">
								<div class="form-group">
									<label for="CompareType">ประเภทข้อเสนอ</label><br>
									<select id="CompareType" class="form-control formInput2 formInput" name="CompareType" id="CompareType" >
			              <option selected value="">กรุณาเลือกรายการ</option> 
			              <option value="N" <?php if($_POST["CompareType"] == "N"){ echo "selected";} ?>>ข้อเสนอใหม่</option>
			              <option value="R" <?php if($_POST["CompareType"] == "R"){ echo "selected";} ?>>งานต่ออายุ</option>
				          </select>
								</div>
							</div>
							<div class="col-md-2">
								<div class="form-group">
									<label for="CompareStatus">สถานะข้อเสนอ </label><br>
									<select id="CompareStatus" class="form-control formInput2 formInput" name="CompareStatus" id="CompareStatus" >
										<option value="">กรุณาเลือกรายการ</option>
										<?php foreach ($getCompareStatusAll as $key => $value) { ?>
												<option value="<?php echo  $value["Compare_Status_ID"] ?>" <?php if($_POST["CompareStatus"] ==  $value["Compare_Status_ID"]){ echo "selected"; } ?> >
													<?php echo $value["Compare_Status"]; ?>
												</option>
										<?php } ?>
						      </select>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-2">
								<div class="form-group">
									<label for="CPDateFrom">วันที่ออกข้อเสนอ เริ่ม</label><br>
									<input class="form-control formInput2" type="date" name="CPDateFrom" id="CPDateFrom" value="<?php echo ($_POST["CPDateFrom"]) ? $_POST["CPDateFrom"] : date("Y-m")."-01"; ?>" >
								</div>
							</div>
							<div class="col-md-2">
								<div class="form-group">
									<label for="CPDateTo">วันที่ออกข้อเสนอ ถึง</label><br>
									<input class="form-control formInput2" type="date" name="CPDateTo" id="CPDateTo" value="<?php echo ($_POST["CPDateTo"]) ? $_POST["CPDateFrom"] : ""; ?>">
								</div>
							</div>
							<div class="col-md-2">
								<div class="form-group">
									<label for="CEDateFrom">วันสิ้นสุดความคุ้มครอง  เริ่ม</label><br>
									<input class="form-control formInput2" type="date" name="CEDateFrom" id="CEDateFrom"  value="<?php echo ($_POST["CEDateFrom"]) ? $_POST["CEDateFrom"] : ""; ?>">
								</div>
							</div>
							<div class="col-md-2">
								<div class="form-group">
									<label for="CEDateTo">วันสิ้นสุดความคุ้มครอง  ถึง</label><br>
									<input class="form-control formInput2" type="date" name="CEDateTo" id="CEDateTo" value="<?php echo ($_POST["CEDateTo"]) ? $_POST["CEDateTo"] : ""; ?>">
								</div>
							</div>
						<div class="col-md-2">
								<div class="form-group">
									<label for="CEDateFrom">วันสร้าง CP  เริ่ม</label><br>
									<input class="form-control formInput2" type="date" name="CPCreateFrom" id="CPCreateFrom"  value="<?php echo ($_POST["CPCreateFrom"]) ? $_POST["CPCreateFrom"] : ""; ?>">
								</div>
							</div>
							<div class="col-md-2">
								<div class="form-group">
									<label for="CEDateTo">วันสร้าง CP  ถึง</label><br>
									<input class="form-control formInput2" type="date" name="CPCreateTo" id="CPCreateTo" value="<?php echo ($_POST["CPCreateTo"]) ? $_POST["CPCreateTo"] : ""; ?>">
								</div>
							</div>
						</div>
						<div class="t_c">
							<a href="offerinsurance.php" class="btn btn-primary">ล้างข้อมูล</a>
							<input type="submit" class="btn btn-success" value="ค้นหา">
							<!-- <button type="button" id="btnSearchoffer" class="btn btn-success "><i class="fa fa-search-plus" aria-hidden="true"></i> ค้นหา</button> -->
						</div>
					</form>
				</div>
				<div class="row">
					<div class="mt30">
						<table id="tablecp" class="table p5" data-toggle="table" data-pagination="true" data-page-size="50" data-page-list="[50, 100, 200, 250, 300]" data-detail-view="true">
								<thead class="fs13 c000000">
									<tr>
										<th class="t_c"><i class="fa fa-list-ul" aria-hidden="true"></i></th>
										<th class="t_c" data-field="CP_ID">เลขที่ข้อเสนอ</th>
										<th class="t_c" data-field="CP_Date" data-sortable="true" >วันที่ออกข้อเสนอ</th>
										<!-- <th class="t_c" data-field="CP_PO_ID">PO(เดิม)</th> -->
										<!-- <th class="t_c" data-field="Customer_ID">รหัสลูกค้า</th> -->
										<th class="t_c" data-field="CustomerName">ชื่อ-นามสกุล</th>
										<th class="t_c" data-field="Phone">เบอร์</th>
										<th class="t_c" data-field="Coverage_End_Date">สิ้นความคุ้มครอง</th>
										<th class="t_c" data-field="Make">รถยนต์</th>
										<!-- <th class="t_c" data-field="Plate">ทะเบียน</th> -->
										<th class="t_c" data-field="Remind_Date">นัดหมายครั้งถัดไป</th>
										<th class="t_c" data-field="Referral_Type">การติดตาม</th> 
										<th class="t_c" data-field="User">พนักงาน</th>
										<th class="t_c" data-field="Create_Date" data-sortable="true" >วันที่สร้าง</th>
										<th class="t_c" data-field="CompareStatus">สถานะ</th>
										<th class="t_c" data-field="Active">การติดต่อ</th>
									</tr>
								</thead>
								<tbody class="fs12 c000000">
									<?php 
										foreach ($getCPoffer as $key => $value) { 
											if($value["Compare_Status_ID"] == "OPN"){
												$bg = "class = 'bgOPN' ";
											}else if($value["Compare_Status_ID"] == "IPC"){
												$dateRemind = ($value["Remind_Date"]) ? $value["Remind_Date"]->format("Y-m-d H:i") : "" ;
												$date = date("Y-m-d H:i");
												// echo "<br>".$dateRemind."-".$date;
													if($dateRemind > $date || !$dateRemind){
														$bg = "class = 'bgIPC2' ";	
													}else{
														$bg = "class = 'bgIPC' ";
													}
											}else if($value["Compare_Status_ID"] == "NEW"){
												$bg = "class = 'bgNEW' ";
											}else if($value["Compare_Status_ID"] == "POI"){
												$bg = "class = 'bgPOI' ";
											}else if($value["Compare_Status_ID"] == "REJ"){
												$bg = "class = 'bgREJ' ";
											}
									?>
									<tr <?php echo $bg; ?>>
										<td class="t_c wpno" >
										<a href="offeredits.php?cpid=<?php echo $value["CP_ID"]; ?>" target="_bank"><i class="fa fa-edit fs16" style="color: #2457ff;"></i></a>
										 | <i class="fa fa-minus-square fs16" style="color: #ff0000;"></i></td>
										<td class="t_c c2457ff fwb"><?php echo $value["CP_ID"]; ?></td>
										<td class="t_c ">
											<div class="fwb"><?php echo $value["CP_Date"]->format("d/m/Y"); ?></div>
											<div><?php echo $value["CP_PO_ID"]; ?></div>		
										</td>
										<td class="t_l nowrap">
											<div><?php echo $value["Customer_ID"]; ?></div>
											<div class="fwb"><?php echo $value["Customer_FName"]." ".$value["Customer_LName"]; ?></div>
										</td>
										<td class="t_c c2457ff fwb"><?php echo ($value["Mobile_No"]) ? $value["Mobile_No"] : $value["Tel_No"] ; ?></td>
										<td class="t_c nowrap c2457ff">
											<div><?php echo $value["Coverage_Start_Date"]->format("d/m/Y"); ?></div>
											<div><?php echo $value["Coverage_End_Date"]->format("d/m/Y"); ?></div>
										</td>
										<td class="t_l nowrap">
											<div class="fwb"><?php echo $value["Plate_No"]." - ".$value["Province_Name_TH"]; ?></div>
											<div><?php echo $value["Make_Name_TH"] ? $value["Make_Name_TH"]." | ".$value["Model_Desc"] : ""; ?></div>
										</td>
										<!-- <td class="t_l"><?php echo $value["Plate_No"]." - ".$value["Province_Name_TH"]; ?></td> -->
										<td class="t_c c2457ff fwb nowrap"><?php echo ($value["Remind_Date"]) ? $value["Remind_Date"]->format("d/m/Y H:i") : "-"; ?></td> 
										<td class="t_l"><?php echo strlen($value["Followup"]) > 300 ? substr($value["Followup"],0,300)."..." : $value["Followup"]; ?></td> 
										<td class="t_c nowrap"><div><?php echo $value["User_FName"]." ".$value["User_LName"]; ?></div></td>
										<td class="t_c nowrap"><div><?php echo $value["Create_Date"]->format("d/m/Y H:i"); ?></div></td>
										<td class="t_c nowrap"><?php echo $value["Compare_Status"]; ?></td>
										<td class="t_c "><?php echo $value["Active"]; ?></td>
									</tr>
									<?php } ?>
								</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	.
</div>


<?php include "include/inc_footer.php"; ?> 
<!-- <script src="//code.jquery.com/jquery-3.2.1.min.js"></script> -->
<!-- <script src="fancybox/dist/jquery.fancybox.min.js"></script> -->
<script type="text/javascript">

var $tablecp = $('#tablecp');
$tablecp.on('expand-row.bs.table', function (e, index, row, $detail) {
  var cpid = row.CP_ID;
	$.ajax({ 
		url: 'include/inc_action_offer.php',
		type:'POST',
		data: {action:"getFollowup", cpid:cpid},
		success:function(rs){
			// console.log(rs);
			$detail.html(rs);
		}
	});

});

function changeStatus(cpid,iscase){
	if(iscase == 'Add'){
		var val = $('#FollowupStatusID'+cpid).val();
	}else{
		var val = $('#editFollowupStatusID'+cpid).val();
	}
	if(val == '009'){
		$("#Reject"+cpid).show();
		$("#dateRemind"+cpid).hide();
	}else if(val == '' || val == '010'){
		$("#Reject"+cpid).hide();
		$("#dateRemind"+cpid).hide();
	}else{
		$("#Reject"+cpid).hide();
		$("#dateRemind"+cpid).show();
	}
}

function addFollowup(cpid){
	customer = $("#customer"+cpid).val();
	emp = $("#emp"+cpid).val();
	pageaction = $("#pageaction"+cpid).val();
	FollowupDateTime = $("#FollowupDateTime"+cpid).val();
	FollowupTypeID = $("#FollowupTypeID"+cpid).val();
	FollowupDetail = $("#FollowupDetail"+cpid).val();
	FollowupStatusID = $("#FollowupStatusID"+cpid).val();
	RemindDate = $("#RemindDate"+cpid).val();
	Remindtime1 = $("#Remindtime1_"+cpid).val();
	Remindtime2 = $("#Remindtime2_"+cpid).val();
	RejectReason = $("#RejectReason"+cpid).val();
	Remind = RemindDate+" "+Remindtime1+":"+Remindtime2;
	// console.log(pageaction);
	// console.log(cpid+" | " +FollowupDateTime+" | "+FollowupTypeID+" | "+FollowupStatusID+" | "+RemindDate+" | "+Remindtime1+" | "+Remindtime2+" | "+FollowupDetail);
	if(!FollowupDetail){
		alert("กรุณากรอกรายละเอียด");
	}else if(!FollowupStatusID){
		alert("กรุณาเลือกสถานะการติดตาม");
	}else if(FollowupStatusID && FollowupStatusID != '009' && FollowupStatusID != '010' && !RemindDate){
		alert("กรุณาเลือก วัน-เวลาที่นัดหมาย");
	}else if(FollowupStatusID == '009' && !RejectReason){
		alert("กรุณาเลือกเหตุผลที่ยกเลิก");
	}else{
		$("#btnAddFollowup"+cpid).slideUp();
		$("#txtbtn"+cpid).html("กรุณารอ...");
		$.ajax({ 
			url: 'include/inc_action_offer.php',
			type:'POST',
			data: {action:"addFollowup", cpid:cpid, pageaction:pageaction, customer:customer, emp:emp, FollowupDateTime:FollowupDateTime, FollowupTypeID:FollowupTypeID, FollowupStatusID:FollowupStatusID, FollowupDetail:FollowupDetail, Remind:Remind, RejectReason:RejectReason},
			success:function(rs){
				// console.log(rs);
				if(rs != "0"){
					alert("เพิ่มการติดตาม  "+cpid+" เรียบร้อย");
					$("#txtbtn"+cpid).html("");
					$("#btnAddFollowup"+cpid).slideDown();
					$("#tablelist_"+cpid).empty();
					$("#tablelist_"+cpid).html(rs);
					$("#FollowupDetail"+cpid).val("");
					$("#FollowupStatusID"+cpid).val("");
					$("#Reject"+cpid).hide();
					$("#dateRemind"+cpid).hide();
				}
			}
		});	
	}
}

function closeFollowup(cpid){
	// cpid = $(e).attr('data-cpid');
	$("#boxEdit"+cpid).slideUp();
	$("#boxAdd"+cpid).slideDown();
}

function editFollow(flid, cpid){
	if(flid){
		$("#boxAdd"+cpid).slideUp();
		$("#boxEdit"+cpid).slideDown();
		$.ajax({ 
			url: 'include/inc_action_offer.php',
			type:'POST',
			dataType: 'json',
			data: {action:"geteditFollowup", flid:flid},
			success:function(rs){
				// console.log(rs);
				$("#editFollowid"+cpid).val(rs.Followup_ID);
				$("#editFollowupDetail"+cpid).val(rs.Followup_Detail);
				$("#editFollowupStatusID"+cpid).val(rs.Followup_Status_ID);
				$("#editFollowupTypeID"+cpid).val(rs.Followup_Type_ID);
				$("#editFollowupDateTime"+cpid).val(rs.FollowupDate);

				if(rs.Remind_Date){
					$("#editdateRemind"+cpid).show();
					$("#editReject"+cpid).hide();

					$("#editRemindDate"+cpid).val(rs.RemindDate);
					$("#editRemindtime1_"+cpid).val(rs.Remindtime1);
					$("#editRemindtime2_"+cpid).val(rs.Remindtime2);

				}else if(rs.Followup_Status_ID == "009"){
					$("#editReject"+cpid).show();
					$("#editdateRemind"+cpid).hide();
					$("#editRejectReason"+cpid).val(rs.RejectReason);

				}else if(rs.Followup_Status_ID == "010") {
					$("#editReject"+cpid).hide();
					$("#editdateRemind"+cpid).hide();
				}
			}
		});
	}
}

function updateFollowlist(cpid){
	// cpid = $(e).attr('data-cpid');
	Followupid = $("#editFollowid"+cpid).val();
	pageaction = $("#editpageaction"+cpid).val();
	FollowupDateTime = $("#FollowupDateTime"+cpid).val();
	FollowupTypeID = $("#editFollowupTypeID"+cpid).val();
	FollowupDetail = $("#editFollowupDetail"+cpid).val();
	FollowupStatusID = $("#editFollowupStatusID"+cpid).val();
	RemindDate = $("#editRemindDate"+cpid).val();
	Remindtime1 = $("#editRemindtime1_"+cpid).val();
	Remindtime2 = $("#editRemindtime2_"+cpid).val();
	RejectReason = $("#editRejectReason"+cpid).val();
	Remind = RemindDate+" "+Remindtime1+":"+Remindtime2;
	if(!FollowupDetail){
		alert("กรุณากรอกรายละเอียด");
	}else if(!FollowupStatusID){
		alert("กรุณาเลือกสถานะการติดตาม");
	}else if(FollowupStatusID && FollowupStatusID != '009' && FollowupStatusID != '010' && !RemindDate){
		alert("กรุณาเลือก วัน-เวลาที่นัดหมาย");
	}else{
		$.ajax({ 
			url: 'include/inc_action_offer.php',
			type:'POST',
			data: {action:"updateFollowuplist", cpid:cpid, pageaction:pageaction, Followupid:Followupid, FollowupDateTime:FollowupDateTime, FollowupTypeID:FollowupTypeID, FollowupStatusID:FollowupStatusID, FollowupDetail:FollowupDetail, Remind:Remind, RejectReason:RejectReason},
			success:function(rs){
				// console.log(rs);
				if(rs){
					alert("แก้ไขกาาติดตาม "+cpid+" เรียบร้อย");
					$("#tablelist_"+cpid).empty();
					$("#tablelist_"+cpid).html(rs);
					$("#boxEdit"+cpid).slideUp();
					$("#boxAdd"+cpid).slideDown();
					$("#editFollowupStatusID"+cpid).val("");
				}
			}
		});
	}
}
</script>