<?php
session_start();
date_default_timezone_set("Asia/Bangkok");
header('Content-Type: text/html; charset=utf-8');

include "../inc_config.php";
include("../include/sms.class.php");
include "inc_function_qr.php";

if($_POST["action"] == "GetListPayQR"){
	$getPOPayForCounter = getPOPayForQR($_POST["txtsearch"]);
	// echo "<pre>".print_r($getPOPayForCounter,1)."</pre>";
	$i=1; 
	$sum = 0; 
	foreach ($getPOPayForCounter as $key => $value) {
		$sum = $sum + $value["ISTM_Total_Amount"];
		$checkQR = checkQR($value["PO_ID"], $value["Installment_ID"]);

		if(!$checkQR){
			if(!$value["ISTM_Total_Amount"]){
				if($value["Insurance_ID"] == "029" || $value["ISTM_Total_Amount"] == 0){
					$case = '<input type="checkbox" class="inskey" id="qrPrice_'.$i.'" name="inskey[]" value="'.$value["PO_ID"]."|".$value["Installment_ID"].'">';
				}else{
					$case =  "<sapn class='c2457ff'>ยอดไม่พอ</sapn>";
				}
			}else if($value["Installment_Status"] != "Paid" && $value["ISTM_Total_Amount"] > 0 &&  $value["enable"] != 'Y' ){
				$case = '<input type="checkbox" class="inskey" id="qrPrice_'.$i.'" name="inskey[]" value="'.$value["PO_ID"]."|".$value["Installment_ID"].'">';
			}
		}else{
			$case =  "<sapn class='c2457ff'>สร้าง QR แล้ว</sapn>";
		}

		$txt .= '
		<tr >
			<td class="t_c">'.$case.'</td>
			<td class="wpno t_c" style="width: 5%;">'.$value["Installment_ID"].'</td>
			<td class="wpno t_c cff2da5" style="width: 10%;"><a class="cff2da5 fwb" href="export_pay.php?pocode='.$value["PO_ID"].'&insID='.$value["Installment_ID"].'" target="_blank" >'.$value["PO_ID"].'</a></td>
			<td class="wpno " >
					<div>'.$value["Customer_ID"].'</div>
					<div>'.$value["Customer_FName"]." ".$value["Customer_LName"].'</div>
				</td>
			<td class="t_c ">'.$value["Insurance_Name"].'</td>
			<td class="t_c ">'.$value["Installment_Due_Date"]->format("d/m/Y").'</td>
			<td class=" t_r"> <b>'.number_format($value["ISTM_Net_Premium"],2).'</b></td>
			<td class=" t_r"> <b>'.number_format($value["ISTM_Duty"],2).'</b></td>
			<td class=" t_r"> <b>'.number_format($value["ISTM_Tax"],2).'</b></td>
			<td class=" t_r"> <b>'.number_format($value["ISTM_Total_Premium"],2).'</b></td>
			<td class=" t_r"> <b>'.number_format($value["ISTM_Discount"],2).'</b></td>
			<td class=" t_r"> <b>'.number_format($value["ISTM_Total_Amount"],2).'</b></td>
			<td class=" t_c cff2da5"> <b>'.$value["Installment_Status"].'</b></td>
		</tr>
		<tr class="dn boxQRALL">
			<td class="wpno t_r" colspan="12">รวม</td>
			<td class="wpno t_c" ><div class="showQRAll">'.number_format($sum,2).'</div></td>
		</tr>';
		$i++;
	}
	echo $txt;
	exit();

}else if($_POST["action"] == "inserListpay"){
	$user = $_SESSION["User"]['UserCode']; 
	// echo "<pre>".print_r($_POST,1)."</pre>";
	$counter_type = "PASTPO";
	$replace = array("P","C");
	$reference_1 = getMaxIDRef1();
	$reference_2 = str_replace($replace, "", $_POST["customer_id"]);
	$sumAmount = 0.00;

	//develop QR
	// $billerCode = "LGN";
	// $billerID = "010555706234201"; //test
	// $storeID = "1000001";
	// $terminalID = "1022";
	// $merchantName = "Asiadirect";
	// $accessCode = "ADB";

	//production QR
	$billerCode = "LGN";
	$billerID = "024554000009711"; 
	$storeID = "2000400";
	$terminalID = "1001";
	$merchantName = "QRFormSale";
	$accessCode = "ADB";

// exit();
	if($user){

		$sqlRQ = "INSERT INTO [dbo].[qrcodes] ([qrcodes_type], [reference_1], [reference_2], [customer_id], [sumAmount], [status_pay], [enable], [created_date], [created_by], [update_date], [update_by], [billerCode], [billerID], [storeID], [terminalID], [merchantName], [accessCode])
     VALUES ('".$counter_type."', '".$reference_1."', '".$reference_2."', '".$_POST["customer_id"]."', '', '0', 'N', '".date('Y-m-d H:i:s')."', '".$user."', '".date('Y-m-d H:i:s')."', '".$user."', '".$billerCode."', '".$billerID."', '".$storeID."', '".$terminalID."', '".$merchantName."', '".$accessCode."'); SELECT SCOPE_IDENTITY()";
		$resource = sqlsrv_query( $connMS, $sqlRQ);
		sqlsrv_next_result($resource); 
		sqlsrv_fetch($resource); 
		$qrcodes_id = sqlsrv_get_field($resource, 0); 
		if($resource){
			foreach ($_POST["inskey"] as $key => $value) { 

				$val = explode("|", $value);
				$getIns = getPOListForQR($val[0], $val[1]);
				$replace = array("P","C");
				$status_pay = ($getIns["Installment_Status_ID"] == "001") ? 0 : 1 ; 
				$insur_type = ($getIns["Insurance_Name"] == "พรบ.") ? "compu" : "insur";  
				$sumAmount  +=  $getIns["ISTM_Total_Amount"];
				// $sumAmount = 1.00;
				$customer_name = $getIns["Customer_FName"]." ".$getIns["Customer_LName"];
				// echo "<pre>".print_r($getIns,1)."</pre>";

				$sqlDesc = "INSERT INTO [dbo].[qrcode_desc] ([qrcodes_id], [po_id], [insur_type], [installment_num], [installment_due_date], [amount], [customer_name]) VALUES ('".$qrcodes_id."', '".$getIns["PO_ID"]."', '".$insur_type."', '".$getIns["Installment_ID"]."', '".$getIns["Installment_Due_Date"]->format("Y-m-d")."', '".$getIns["ISTM_Total_Amount"]."', '".$customer_name."')";
		    $stmt = sqlsrv_query( $connMS, $sqlDesc);
			}
		
			$sumAmount = number_format($sumAmount, 2, '.', '');
			// $url  = 'https://develop.lugentpay.com:44093/cubems/msh/rest/lugentpay/thaiqrrequest';
			$url = "https://production.lugentpay.com:44083/cubems/msh/bpms/lugentpay/qrrequest/botqr";
			$ch = curl_init($url);
			$data = array(
			    'billerCode' => $billerCode,
			    'billerID' => $billerID,
			    'ref1' => $reference_1,
			    'ref2' => $reference_2,
			    'amount' => $sumAmount,
			    'storeID' => $storeID,
			    'terminalID' => $terminalID,
			    'merchantName' => $merchantName,
			    'accessCode' => $accessCode
			);
			$data_string = json_encode($data);
			// echo "<pre>".print_r($data_string,1)."</pre>";
			// curl_setopt($ch, CURLOPT_PORT, '44093');  // develop
			curl_setopt($ch, CURLOPT_PORT, '44083');  // production
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			$result = curl_exec($ch);
			$error_msg = curl_error($ch);
			curl_close($ch);
			$txtResult = json_decode($result);
			// echo "<pre>".print_r($error_msg,1)."</pre>";
			// echo "<pre>".print_r($txtResult,1)."</pre>";
			// exit();
			if($txtResult->transactionId && $txtResult->qrCode){
				$sqlupDate = "UPDATE [dbo].[qrcodes] SET [transactionId] = '".$txtResult->transactionId."', [qrCode] = '".$txtResult->qrCode."', [res_code] = '".$txtResult->res_code."', [res_desc] = '".$txtResult->res_desc."', [sumAmount] = '".$sumAmount."', [enable] = 'Y' WHERE qrcodes_id = '".$qrcodes_id."' ";
				$stmtup = sqlsrv_query( $connMS, $sqlupDate);

				// if($_POST["phone"]){
				// 	getSendSMS($_POST["phone"], $reference_1);
				// }

				echo "<script>alert('ทำรายการชำระเงินสำเร็จ โปรดตรวจสอบ');</script>";
				echo '<META http-equiv="refresh" content="0;URL=../pay_qrcode.php?txtse='.$reference_1.'">';
				exit();
			}else{
				$sqlDel1 = "DELETE FROM [qrcodes] WHERE qrcodes_id = '".$qrcodes_id."'";
				$stmt = sqlsrv_query( $connMS, $sqlDel1);
				$sqlDel2 = "DELETE FROM [qrcode_desc] WHERE qrcodes_id = '".$qrcodes_id."'";
				$stmt = sqlsrv_query( $connMS, $sqlDel2);
				
				echo "<script>alert('ไม่สามารถทำรายการได้ กรุณาลองอีกครั้ง');</script>";
				echo '<META http-equiv="refresh" content="0;URL=../pay_qrcode.php">';
				exit();
			}
		}else{
			echo "<script>alert('ไม่สามารถทำรายการได้ กรุณาลองอีกครั้ง');</script>";
			echo '<META http-equiv="refresh" content="0;URL=../pay_qrcode.php">';
			exit();
		}
	}else{
		echo "<script>alert('ไม่ได้ล็อคอิน กรุณา ล็อคอินใหม่');</script>";
		echo '<META http-equiv="refresh" content="0;URL=../login.php">';
		exit();
	}

}else if($_POST["action"] == "GetDataPhone"){
	
	$sql = "SELECT Tel_No,Mobile_No,EMail From [dbo].Customer WHERE Customer_ID = '".$_POST["txtsearch"]."' ";
	$stmt = sqlsrv_query( $connMS, $sql );
  $row = sqlsrv_fetch_array($stmt);
  $arr = array();
  $arr["phone"] = ($row["Tel_No"]) ? $row["Tel_No"] : $row["Mobile_No"];
  $arr["email"] = $row["EMail"];
  echo json_encode($arr);
  exit();

}else if($_POST["action"] == "deleteQR"){
	$qrcodes_id = $_POST["qrcodes_id"];
	$sqlupDate = "UPDATE [dbo].[qrcodes] SET [enable] = 'N' WHERE qrcodes_id = '".$qrcodes_id."' ";
	$stmtup = sqlsrv_query( $connMS, $sqlupDate);
	echo 1;
  exit();
}

?>