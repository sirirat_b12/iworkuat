<?php
session_start();
date_default_timezone_set("Asia/Bangkok");
header('Content-Type: text/html; charset=utf-8');

include "../inc_config.php";
include("../include/sms.class.php");
include "inc_function_qr.php";



$_GET["action"] = "inser";
// $_GET["user"] = "ADB59019";
// $_GET["customer_id"] = "P72412";
// $_GET["po"] = "PO61048443";
// $_GET["ins"] = "1";

if(!isset($_GET["user"])) exit();
if(!isset($_GET["customer_id"])) exit();
if(!isset($_GET["po"])) exit();
if(!isset($_GET["ins"])) exit();





if($_GET["action"] == "inser"){
	
	$user = $_GET["user"]; 
	// echo "<pre>".print_r($_GET,1)."</pre>";
	$counter_type = "PASTPO";
	$replace = array("P","C");
	$reference_1 = getMaxIDRef1();
	$reference_2 = str_replace($replace, "", $_GET["customer_id"]);
	$sumAmount = 0.00;

	//develop QR
	// $billerCode = "LGN";
	// $billerID = "010555706234201"; //test
	// $storeID = "1000001";
	// $terminalID = "1022";
	// $merchantName = "QRFormLine";
	// $accessCode = "ADB";

	//production QR
	$billerCode = "LGN";
	$billerID = "024554000009711"; 
	$storeID = "2000400";
	$terminalID = "1001";
	$merchantName = "QRFormLine";
	$accessCode = "ADB";
	$checkQR = checkQR($_GET["po"], $_GET["ins"]);
	if(!$checkQR){
		$sqlRQ = "INSERT INTO [dbo].[qrcodes] ([qrcodes_type], [reference_1], [reference_2], [customer_id], [sumAmount], [status_pay], [enable], [created_date], [created_by], [update_date], [update_by], [billerCode], [billerID], [storeID], [terminalID], [merchantName], [accessCode])
     VALUES ('".$counter_type."', '".$reference_1."', '".$reference_2."', '".$_GET["customer_id"]."', '', '0', 'N', '".date('Y-m-d H:i:s')."', '".$user."', '".date('Y-m-d H:i:s')."', '".$user."', '".$billerCode."', '".$billerID."', '".$storeID."', '".$terminalID."', '".$merchantName."', '".$accessCode."'); SELECT SCOPE_IDENTITY()";
		$resource = sqlsrv_query( $connMS, $sqlRQ);
		sqlsrv_next_result($resource); 
		sqlsrv_fetch($resource); 
		$qrcodes_id = sqlsrv_get_field($resource, 0); 
		
	 
			$getIns = getPOListForQR($_GET["po"], $_GET["ins"]);
			$replace = array("P","C");
			$status_pay = ($getIns["Installment_Status_ID"] == "001") ? 0 : 1 ; 
			$insur_type = ($getIns["Insurance_Name"] == "พรบ.") ? "compu" : "insur";  
			// echo "<pre>".print_r($getIns,1)."</pre>";
			$sumAmount  +=  $getIns["ISTM_Total_Amount"];
			// $sumAmount = 1.15;
			$customer_name = $getIns["Customer_FName"]." ".$getIns["Customer_LName"];

			$sqlDesc = "INSERT INTO [dbo].[qrcode_desc] ([qrcodes_id], [po_id], [insur_type], [installment_num], [installment_due_date], [amount], [customer_name]) VALUES ('".$qrcodes_id."', '".$getIns["PO_ID"]."', '".$insur_type."', '".$getIns["Installment_ID"]."', '".$getIns["Installment_Due_Date"]->format("Y-m-d")."', '".$getIns["ISTM_Total_Amount"]."', '".$customer_name."')";
	    $stmt = sqlsrv_query( $connMS, $sqlDesc);
			
	
			$sumAmount = number_format($sumAmount, 2, '.', '');
			// $url  = 'https://develop.lugentpay.com:44093/cubems/msh/rest/lugentpay/thaiqrrequest';
			$url = "https://production.lugentpay.com:44083/cubems/msh/bpms/lugentpay/qrrequest/botqr";
			$ch = curl_init($url);
			$data = array(
			    'billerCode' => $billerCode,
			    'billerID' => $billerID,
			    'ref1' => $reference_1,
			    'ref2' => $reference_2,
			    'amount' => $sumAmount,
			    'storeID' => $storeID,
			    'terminalID' => $terminalID,
			    'merchantName' => $merchantName,
			    'accessCode' => $accessCode
			);
			$data_string = json_encode($data);
			// echo "<pre>".print_r($data_string,1)."</pre>";
			// curl_setopt($ch, CURLOPT_PORT, '44093');  // develop
			curl_setopt($ch, CURLOPT_PORT, '44083');  // production
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			$result = curl_exec($ch);
			$error_msg = curl_error($ch);
			curl_close($ch);
			$txtResult = json_decode($result);
			// echo "<pre>".print_r($error_msg,1)."</pre>";
			// echo "<pre>".print_r($txtResult,1)."</pre>";
			// exit();
			if($txtResult->transactionId && $txtResult->qrCode){
				$sqlupDate = "UPDATE [dbo].[qrcodes] SET [transactionId] = '".$txtResult->transactionId."', [qrCode] = '".$txtResult->qrCode."', [res_code] = '".$txtResult->res_code."', [res_desc] = '".$txtResult->res_desc."', [sumAmount] = '".$sumAmount."', [enable] = 'Y' WHERE qrcodes_id = '".$qrcodes_id."' ";
				$stmtup = sqlsrv_query( $connMS, $sqlupDate);

				$msg = 'ลูกค้าสร้าง QR CODE ผ่าน Line@  \n'.$_GET['po'].' | '.$_GET['ins'].'\nREF1 = '.$reference_1.'\nวันที่สร้าง '.date('Y-m-d H:i:s');
				$curl = curlLineNoti($_GET["user"], $msg);
				
				echo '<META http-equiv="refresh" content="0;URL=../showqr.php?code='.base64_encode($reference_1).'">';
				exit();
			}else{
				$sqlDel1 = "DELETE FROM [qrcodes] WHERE qrcodes_id = '".$qrcodes_id."'";
				$stmt = sqlsrv_query( $connMS, $sqlDel1);
				$sqlDel2 = "DELETE FROM [qrcode_desc] WHERE qrcodes_id = '".$qrcodes_id."'";
				$stmt = sqlsrv_query( $connMS, $sqlDel2);
				
				echo "<script>alert('ไม่สามารถทำรายการได้ กรุณาลองอีกครั้ง');</script>";
				echo '<META http-equiv="refresh" content="0;URL=https://line.me/R/ti/p/%40znp6626t">';
				exit();
			}
	}else{
		echo $reference_1 = $checkQR[0]["reference_1"];
		echo '<META http-equiv="refresh" content="0;URL=../showqr.php?code='.base64_encode($reference_1).'">';
		exit();
	}

}


function curlLineNoti($user, $msg){
	$url  = 'https://www.asiadirect.co.th/line-at/ibroker/linemeapi.php';
	$data = array(
	    'user_id' => $user,
	    'status' => "QRpayment",
	    'messages' => $msg
	);
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$result = curl_exec($ch);
		$error_msg = curl_error($ch);
		curl_close($ch);
	return 1;
	exit();
}
?>