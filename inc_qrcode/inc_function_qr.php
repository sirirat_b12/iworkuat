<?php 


function getMaxIDRef1(){
	global $connMS;
	$year = date("Y");
	$sql = "SELECT MAX(reference_1) AS reference_1 From [dbo].qrcodes WHERE year(created_date) = '".$year."' AND MONTH(created_date) = '".date("m")."' ";
	$stmt = sqlsrv_query( $connMS, $sql );
	if(sqlsrv_has_rows($stmt)) {
		$row = sqlsrv_fetch_array($stmt);
		$code = substr($row["reference_1"],-7);
		$idMax = (date("y")+43)."".date("mdHi")."".str_pad(intval($code)+1,7,"0",STR_PAD_LEFT);
		return $idMax;
		exit();
	}  
}

function getPOPayForQR($txtSe){
	global $connMS;
	$resultarray = array();
	$sql = "SELECT   Purchase_Order.PO_ID, Insurance.Insurance_Name, Purchase_Order.PO_Date, Purchase_Order.Operation_Type_ID, Purchase_Order.Insurance_ID, Purchase_Order.Customer_ID, Purchase_Order.Agent_ID, Purchase_Order.Employee_ID, Purchase_Order.Status_ID,Status.Status_Desc,
	Installment.PO_ID, Installment.Installment_ID, Installment.Installment_Due_Date, Installment.ISTM_Net_Premium, Installment.ISTM_Duty, 
	Installment.ISTM_Tax, Installment.ISTM_Total_Premium, Installment.ISTM_Discount,Installment.ISTM_Total_Amount, Installment.Installment_Status_ID,
	Installment_Status.Installment_Status, Installment.Remark, Customer.Customer_ID, Customer.Customer_FName, Customer.Customer_LName, Customer.EMail, 
	Customer.Tel_No, Customer.Mobile_No
	FROM Purchase_Order 
	LEFT JOIN  Installment ON Purchase_Order.PO_ID = Installment.PO_ID
	INNER JOIN  Customer ON Purchase_Order.Customer_ID = Customer.Customer_ID
	LEFT JOIN  Status ON Purchase_Order.Status_ID = Status.Status_ID
	LEFT JOIN  Installment_Status ON Installment.Installment_Status_ID = Installment_Status.Installment_Status_ID
	LEFT JOIN  Insurance ON Purchase_Order.Insurance_ID = Insurance.Insurance_ID
	WHERE Purchase_Order.Customer_ID LIKE '%".$txtSe."%' 
	AND Installment.Installment_Status_ID = '001'
	AND Purchase_Order.Status_ID IN ('RCA','RVP','CLS')
	ORDER BY Purchase_Order.Insurance_ID,Installment.Installment_ID ASC";
	$stmt = sqlsrv_query( $connMS, $sql );
	if(sqlsrv_has_rows($stmt)) {
		while( $row = sqlsrv_fetch_array($stmt) ) { 
			$resultarray[] = $row;
		}
		return $resultarray;
		exit();
	}
}


function getPOListForQR($po, $inst){
	global $connMS;
	$resultarray = array();
	$sql = "SELECT    Purchase_Order.PO_ID, Insurance.Insurance_Name, Purchase_Order.PO_Date, Purchase_Order.Operation_Type_ID, Purchase_Order.Customer_ID, Purchase_Order.Agent_ID, Purchase_Order.Employee_ID, Purchase_Order.Status_ID,Status.Status_Desc,
	Installment.PO_ID, Installment.Installment_ID, Installment.Installment_Due_Date, Installment.ISTM_Net_Premium, Installment.ISTM_Duty, Installment.ISTM_Tax, Installment.ISTM_Total_Premium, Installment.ISTM_Discount,Installment.ISTM_Total_Amount, Installment.Installment_Status_ID,Installment_Status.Installment_Status, Installment.Remark, Customer.Customer_ID, Customer.Customer_FName, Customer.Customer_LName, Customer.EMail, Customer.Tel_No, Customer.Mobile_No
	FROM Purchase_Order 
	LEFT JOIN  Installment ON Purchase_Order.PO_ID = Installment.PO_ID
	INNER JOIN  Customer ON Purchase_Order.Customer_ID = Customer.Customer_ID
	LEFT JOIN  Status ON Purchase_Order.Status_ID = Status.Status_ID
	LEFT JOIN  Installment_Status ON Installment.Installment_Status_ID = Installment_Status.Installment_Status_ID
	LEFT JOIN  Insurance ON Purchase_Order.Insurance_ID = Insurance.Insurance_ID
	WHERE Installment.PO_ID = '".$po."'  AND Installment.Installment_ID = '".$inst."' ORDER BY Purchase_Order.Insurance_ID,Installment.Installment_ID ASC";
	$stmt = sqlsrv_query( $connMS, $sql );
	if(sqlsrv_has_rows($stmt)) {
		$row = sqlsrv_fetch_array($stmt);
		return $row;
		exit();
	}  
	exit();
}




function getListQRCode($txtse=""){
	global $connMS;
	$resultarray = array();
	$txtse = trim($txtse);
	$sql = "SELECT  * FROM qrcodes WHERE reference_1 = '".$txtse."' OR customer_id = '".$txtse."' AND enable = 'Y' " ;
	$stmt = sqlsrv_query( $connMS, $sql );
	if(sqlsrv_has_rows($stmt)) {
		$i = 0;
		while( $row = sqlsrv_fetch_array($stmt) ) { 
    //   $resultarray[] = $row;
    // }
    // $row = sqlsrv_fetch_array($stmt);
			$resultarray[$i]["dataMain"] =  $row;

			$sqlDesc = "SELECT * FROM qrcode_desc WHERE qrcodes_id = '".$row["qrcodes_id"]."' ORDER BY installment_num ASC" ;
			$stmtDesc = sqlsrv_query( $connMS, $sqlDesc );
			while( $rowDesc = sqlsrv_fetch_array($stmtDesc) ) { 
				$resultarray[$i]["dataDesc"][] =  $rowDesc;
			}
			$i++;
		}
// echo "<pre>".print_r($resultarray,1)."</pre>";
		return $resultarray;
		exit();
	}  
// exit();
}


function getListQRCodeShowPage($txtse=""){
	global $connMS;
	$resultarray = array();
	$txtse = trim($txtse);
	$sql = "SELECT  * FROM qrcodes WHERE reference_1 = '".$txtse."' " ;
	$stmt = sqlsrv_query( $connMS, $sql );
	if(sqlsrv_has_rows($stmt)) {
		$i = 0;
		$row = sqlsrv_fetch_array($stmt);
		$resultarray["dataMain"] =  $row;

		$sqlDesc = "SELECT * FROM qrcode_desc WHERE qrcodes_id = '".$row["qrcodes_id"]."' ORDER BY installment_num ASC" ;
		$stmtDesc = sqlsrv_query( $connMS, $sqlDesc );
		while( $rowDesc = sqlsrv_fetch_array($stmtDesc) ) { 
			$resultarray["dataDesc"][] =  $rowDesc;
		}
// echo "<pre>".print_r($resultarray,1)."</pre>";
		return $resultarray;
		exit();
	}  
// exit();
}


function getListQRCodeByDate($date, $txtse, $dateEnd){
	global $connMS;
	$resultarray = array();
	$i = 0;
	$dateStart = $date." 00:00:00";
	$dateEnd = $dateEnd." 23:59:59";
// echo $txtse;
	$sql = "SELECT * FROM qrcodes 
	WHERE qrcodes.status_pay = '1' ";
	if($txtse){
		$sql .= " AND qrcodes.reference_1 = '".$txtse."' OR qrcodes.customer_id = '".$txtse."'  OR qrcodes.receive_id = '".$txtse."' OR qrcodes.transactionId = '".$txtse."'" ;
	}else{
		$sql .= " AND qrcodes.paymentTime BETWEEN '".$dateStart."' AND '".$dateEnd."' " ;
	}
	$sql .= " ORDER BY qrcodes.paymentTime DESC";
// echo $sql;
	$stmt = sqlsrv_query( $connMS, $sql );
	if(sqlsrv_has_rows($stmt)) {
		while( $row = sqlsrv_fetch_array($stmt) ) { 
			$resultarray[$i] = $row;
			$sqlDesc = "SELECT qrcode_desc.* ,Insurer.Insurer_Initials
			FROM qrcode_desc 
			LEFT JOIN Purchase_Order_Insurer ON qrcode_desc.PO_ID = Purchase_Order_Insurer.PO_ID
			LEFT JOIN Insurer ON Purchase_Order_Insurer.Insurer_ID = Insurer.Insurer_ID 
			WHERE qrcodes_id = '".$row["qrcodes_id"]."' ORDER BY installment_num ASC" ;
			$stmtDesc = sqlsrv_query( $connMS, $sqlDesc );
			while( $rowDesc = sqlsrv_fetch_array($stmtDesc) ) { 
				$resultarray[$i]["dataDesc"][] =  $rowDesc;
			}
			$i++;
		}
	}
	return $resultarray;
	exit();
}



function getSendSMS($phone, $paycode){

// $long_url = "http://61.90.142.230/adbchk/showqr.php?code=".base64_encode($paycode);
	$long_url = "https://adbwecare.com/adbchk/showqr.php?code=".base64_encode($paycode);
	$shorturl = shorturl($long_url);
	$txt = 'โปรดชำระเงิน ผ่าน QRCode '.$shorturl." โทร. 020892000";

	if($txt){
		$result_sms = sms::send_sms('adbwecare','6251002','0894353549', $txt,'AsiaDirect','','');
		// $result_sms = sms::send_sms('adbwecare','6251002',$phone, $txt,'AsiaDirect','','');
	}
// $result_sms = 1;
	if($result_sms){
		return 1;
	}else{
		return 0;
	}
// exit();
}

function shorturl($long_url){
	$url = "https://asiadirect.co.th/shorturl/insert.php";
	$data = array(
		'action' => "inserUrl",
		'long_url' => $long_url
	);
	$ch = curl_init($url);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTREDIR, 3);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
	curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$result = curl_exec($ch);
	$error_msg = curl_error($ch);
	curl_close($ch);

	return $result;
}


function checkQR($po, $ins){
	global $connMS;
	$resultarray = array();
	$sql = "SELECT  qrcodes.reference_1, qrcodes.customer_id
	FROM qrcodes LEFT JOIN  qrcode_desc ON qrcodes.qrcodes_id = qrcode_desc.qrcodes_id
	WHERE qrcode_desc.po_id = '".$po."'  AND qrcode_desc.installment_num = '".$ins."' AND qrcodes.enable = 'Y' ORDER BY qrcodes.qrcodes_id DESC";
	$stmt = sqlsrv_query( $connMS, $sql );
	if(sqlsrv_has_rows($stmt)) {
		while( $row = sqlsrv_fetch_array($stmt) ) { 
			$resultarray[] = $row;
		}
		return $resultarray;
		exit();
	}
}

?>