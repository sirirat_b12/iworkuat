<?php
ini_set("allow_url_fopen", 1);
date_default_timezone_set("Asia/Bangkok");
header('Content-Type: text/html; charset=utf-8');

include "../inc_configtest.php";
include "inc_function_qr.php";


function getQrcode($transactionId){
	global $connMS;
	$sql = "SELECT * FROM qrcodes WHERE transactionId = '".$transactionId."' AND status_pay = '0' AND enable = 'Y' " ;
	$stmt = sqlsrv_query( $connMS, $sql );
	$rowqQrcodes = sqlsrv_fetch_array($stmt);
	return $rowqQrcodes;
  exit();
}


function getQrcodeDesc($qrcodes_id){
	global $connMS;
	$sql = "SELECT * FROM qrcode_desc WHERE qrcodes_id = '".$qrcodes_id."' " ;
	$stmt = sqlsrv_query( $connMS, $sql );
    if(sqlsrv_has_rows($stmt)) {
	    while( $row = sqlsrv_fetch_array($stmt) ) { 
	      $resultarray[] = $row;
	    }
	    return $resultarray;
			exit();
		}
  exit();
}

function getInstallment($poId, $Installment_ID){
	global $connMS;
	$sql = "SELECT * FROM Installment WHERE PO_ID = '".$poId."' AND Installment_ID = '".$Installment_ID."' " ;
	$stmt = sqlsrv_query( $connMS, $sql );
	$row = sqlsrv_fetch_array($stmt);
	return $row;
  exit();
}


function getMaxIDRV(){
	global $connMS;
	$year = date("Y");
	$sql = "SELECT max(Receive_ID) AS count From [dbo].Receive ";
	$stmt = sqlsrv_query( $connMS, $sql );
  if(sqlsrv_has_rows($stmt)) {
    $row = sqlsrv_fetch_array($stmt);
    $txt = "RV".(date("y")+43);
    $idex = explode($txt, $row["count"]);
    $idMax = $txt."".str_pad(intval($idex[1])+1,6,"0",STR_PAD_LEFT);
    return $idMax;
    exit();
  }  
}


function insertReceive($getMaxIDRV,$getQrcode, $paymentId){
	global $connMS;

	$Cost = 15;
	$sumAmount = (float)$getQrcode["sumAmount"] - $Cost;
	$sqlRV = "INSERT INTO [dbo].[Receive] ([Receive_ID], [Receive_Date], [Customer_Group_ID], [Customer_ID], [Receive_Type_ID], [Receive_Method_ID], [Receivable_Amount], [Receive_Amount], [Receive_Currency], [Broker_Account_ID], [Receive_Status_ID], [Deposit_Date], [Remark], [Active], [Create_Date], [Create_By], [Update_Date], [Update_By]) VALUES ('".$getMaxIDRV."', '".date('Y-m-d H:i:s')."', '', '".$getQrcode["customer_id"]."', 'B', 'QRL', '".$sumAmount."', '".$sumAmount."', 'THB', '016', '003', '".date("Y-m-d H:i:s")."', 'QR: ".$getQrcode["reference_1"]."', 'Y', '".date("Y-m-d H:i:s")."', 'QR System', '".date("Y-m-d H:i:s")."', 'QR System' )";
	$rsRV = sqlsrv_query( $connMS, $sqlRV);
// $rsRV = 1;
	if($rsRV){
		$sqlRVDetail = "INSERT INTO [dbo].[Receive_Detail] ([Receive_ID], [Cheque_No], [Cheque_Date], [Cheque_Bank_ID], [Cheque_Branch], [Credit_Card_No], [Approve_Code], [Transfer_Date], [Remark], [Active], [Create_Date], [Create_By], [Update_Date], [Update_By])  
		VALUES ('".$getMaxIDRV."', '', '', '', '', '', '', '".date("Y-m-d H:i:s")."', 'QR: ".$getQrcode["reference_1"]."', 'Y', '".date("Y-m-d H:i:s")."', 'QR System', '".date("Y-m-d H:i:s")."', 'QR System')";
		sqlsrv_query( $connMS, $sqlRVDetail);

		$sqlRVCost = "INSERT INTO [dbo].[Receive_Operation_Cost] ([Receive_ID], [Operation_Cost_ID], [Amount], [Remark], [Active], [Create_Date], [Create_By], [Update_Date], [Update_By])
		VALUES ('".$getMaxIDRV."', '007', '".$Cost."', 'QR: ".$getQrcode["reference_1"]."', 'Y', '".date("Y-m-d H:i:s")."', 'QR System', '".date("Y-m-d H:i:s")."', 'QR System')";
		sqlsrv_query( $connMS, $sqlRVCost);

				$getQrcodeDesc = getQrcodeDesc($getQrcode["qrcodes_id"]);
				foreach ($getQrcodeDesc as $key => $valQrcodeDesc) {
					$getInstallment = getInstallment($valQrcodeDesc["po_id"], $valQrcodeDesc["installment_num"]);
	    			$sqlRVInsta = "INSERT INTO [dbo].[Receive_Installment] ([Receive_ID], [PO_ID], [Installment_ID], [Total_Premium], [Discount], [WHT_Amount], [WHT_Usage_ID], [Receivable_Amount], [Remark], [Active], [Create_Date], [Create_By], [Update_Date], [Update_By]) VALUES ('".$getMaxIDRV."', '".$valQrcodeDesc["po_id"]."', '".$valQrcodeDesc["installment_num"]."', '".$valQrcodeDesc["amount"]."', '".$getInstallment["ISTM_Discount"]."', '0', 'A', '".$getInstallment["ISTM_Total_Amount"]."', 'QR: ".$getQrcode["reference_1"]."', 'Y', '".date("Y-m-d H:i:s")."', 'QR System', '".date("Y-m-d H:i:s")."', 'QR System')";
			   	$rsRVIns = sqlsrv_query( $connMS, $sqlRVInsta);
	    		$rsRVIns = 1;
			   	if($rsRVIns){
			   			upDateStatusInst("009", $valQrcodeDesc["po_id"], $valQrcodeDesc["installment_num"], $getQrcode["reference_1"], $getMaxIDRV, "paid");
			   			CheckStatusPO($valQrcodeDesc["po_id"]);
			   	}
				   	$arrData = array();
			  		$arrData["paycode"] = $getQrcode["reference_1"];
			  		$arrData["receive_method_id"] = "QRL";
			  		$arrData["campaign"] = "cashQRPO";
			  		$arrData["customer_id"] = $getQrcode["customer_id"];
			  		$arrData["name"] = $valQrcodeDesc["customer_name"];
			  		$arrData["phone"] = "";
			  		$arrData["email"] = "";
			  		$arrData["price"] = $valQrcodeDesc["amount"];
			  		$arrData["employee_id"] = $getQrcode["created_by"];
			  		$arrData["po_id"] = $valQrcodeDesc["po_id"];
			  		$arrData["installment_id"] = $valQrcodeDesc["installment_num"];
			  		$arrData["receive_id"] = $getMaxIDRV;
			  		$arrData["reference_1"] = $getQrcode["reference_1"];
			  		$arrData["reference_2"] = $getQrcode["reference_2"];
			  		$arrData["transaction_id"] = $paymentId;
			  		$arrData["payment_date"] = date("Y-m-d H:i:s");
			  		$arrData["remark"] = "QR ชำระมี PO ";
			  		insertReportPayment($arrData);
		    }

		return 1;
		exit();
	}else{
		return 0;
		exit();
	}
}


function insertReportPayment($arr){
	global $connMS;
	$year = date("Y");
	$sql = "SELECT TOP 1 report_payment_code AS count From [dbo].report_payment WHERE YEAR(report_date) = '". $year."' ORDER BY report_payment_code DESC ";
	$stmt = sqlsrv_query( $connMS, $sql );
	$row = sqlsrv_fetch_array($stmt);
	$txt = "RP".(date("y")+43);
	$idex = explode($txt, $row["count"]);;
	$report_payment = $txt."".str_pad(intval($idex[1])+1,6,"0",STR_PAD_LEFT); 


	$sqlRP = "INSERT INTO [dbo].[report_payment]
	([report_payment_code], [report_date], [receive_method_id], [paycode], [campaign], [customer_id], [name], [phone], [email], [price], [employee_id], [po_id], [installment_id], [receive_id], [reference_1], [reference_2], [transaction_id], [payment_date], [remark])
	VALUES ('".$report_payment."', '".date('Y-m-d H:i:s')."', '".$arr["receive_method_id"]."', '".$arr["paycode"]."', '".$arr["campaign"]."', '".$arr["customer_id"]."', '".$arr["name"]."', '".$arr["phone"]."', '".$arr["email"]."', '".$arr["price"]."', '".$arr["employee_id"]."', '".$arr["po_id"]."', '".$arr["installment_id"]."', '".$arr["receive_id"]."', '".$arr["reference_1"]."', '".$arr["reference_2"]."', '".$arr["transaction_id"]."', '".$arr["payment_date"]."', '".$arr["remark"]."')";
	$stmt = sqlsrv_query($connMS, $sqlRP);
}




function upDateStatusInst($status, $poid, $Installment_ID, $reference_1, $rv_id, $case){
	global $connMS;
	$sqlUpdate = " UPDATE [dbo].[Installment] SET [Installment_Status_ID] = '".$status."', [update_date] = '".date('Y-m-d H:i:s')."', [update_by] = 'QR System',  [Remark] = 'Receive ID :: ".$rv_id." [QR:".$reference_1."]' WHERE PO_ID = '".$poid."' AND Installment_ID = '".$Installment_ID."' ";
	$rs = sqlsrv_query( $connMS, $sqlUpdate);
	return $rs;
	exit();
}


function CheckStatusPO($poid){
	global $connMS;

	$sqlCountPaid = "SELECT count(PO_ID) AS num FROM Installment  WHERE Installment_Status_ID = '009' AND PO_ID = '".$poid."'";
	$stmt = sqlsrv_query( $connMS, $sqlCountPaid );
	if(sqlsrv_has_rows($stmt)) {
		$rowPaid = sqlsrv_fetch_array($stmt);
	}

	$sqlCountNum = "SELECT count(PO_ID) AS num FROM Installment  WHERE PO_ID = '".$poid."'";
	$stmt2 = sqlsrv_query( $connMS, $sqlCountNum );
	if(sqlsrv_has_rows($stmt2)) {
		$rowNum = sqlsrv_fetch_array($stmt2);
	}

	if($rowPaid["num"] == $rowNum["num"]){
		$sqlUpdate = " UPDATE [dbo].[Purchase_Order] SET [Status_ID] = 'RV', [Finance_Status_ID] = 'RV', [RV_Date] = '".date('Y-m-d H:i:s')."', [update_date] = '".date('Y-m-d H:i:s')."', [update_by] = 'QR System' WHERE PO_ID = '".$poid."' AND Status_ID != 'RV' ";	
	}elseif($rowPaid["num"] != $rowNum["num"] && $rowNum["num"] == 1){
		$sqlUpdate = " UPDATE [dbo].[Purchase_Order] SET [Status_ID] = 'RCA', [Finance_Status_ID] = 'RCA', [update_date] = '".date('Y-m-d H:i:s')."', [update_by] = 'QR System' WHERE PO_ID = '".$poid."' ";
	}else{
		$sqlUpdate = " UPDATE [dbo].[Purchase_Order] SET [Status_ID] = 'RVP', [Finance_Status_ID] = 'RVP', [update_date] = '".date('Y-m-d H:i:s')."', [update_by] = 'QR System' WHERE PO_ID = '".$poid."' ";
	}
	// echo "<br>".$sqlUpdate;
	$rs = sqlsrv_query( $connMS, $sqlUpdate);
	return $rs;
	exit();
}




function insertQrcodelog($transactionId, $paymentId){
	global $connMS;
	$sqlQrcodelog = "INSERT INTO [dbo].[qrcode_log] ([transactionId], [paymentId], [created_date]) VALUES ('".$transactionId."', '".$paymentId."','".date("Y-m-d H:i:s")."'); SELECT SCOPE_IDENTITY()";
	$rs = sqlsrv_query( $connMS, $sqlQrcodelog);
	sqlsrv_next_result($rs); 
	sqlsrv_fetch($rs); 
	$qrcodes_id = sqlsrv_get_field($rs, 0); 
	return $qrcodes_id;
	exit();
}


function upDateQrcodelog($res_code, $res_desc, $confirmId, $logid){
	global $connMS;
	$sqlupDate = "UPDATE [dbo].[qrcode_log] SET [res_code] = '".$res_code."', [res_desc] = '".$res_desc."',[confirmId] = '".$confirmId."' WHERE qrcode_log_id = '".$logid."' ";
	$rs = sqlsrv_query( $connMS, $sqlupDate);
	return $rs;
	exit();
}


function curlLineNoti($transactionId){
	global $connMS;
	$sql = "SELECT * FROM qrcodes WHERE transactionId = '".$transactionId."'" ;
	$stmt = sqlsrv_query( $connMS, $sql );
	$rowqQrcodes = sqlsrv_fetch_array($stmt);
	$getQrcodeDesc = getQrcodeDesc($rowqQrcodes["qrcodes_id"]);
	
	$user_id = $rowqQrcodes["created_by"];
	$messages = '';
	$messages .= 'แจ้งชำระเงินผ่าน QRCODE';
	$messages .= '\nรหัสชำระ '.$rowqQrcodes["receive_id"];
	$messages .= '\nเวลาชำระ '.$rowqQrcodes["paymentTime"]->format("d/m/Y H:i:s");
	$messages .= '\nรายละเอียด PO  ดังนี้';
	foreach ($getQrcodeDesc as $key => $value) {
		$messages .= '\n'.$value["po_id"]." | ".$value["installment_num"]." | ".number_format($value["amount"],2);
	}
	$messages .= '\nRef.1 : '.$rowqQrcodes["reference_1"];
	$messages .= '\nRef.2 : '.$rowqQrcodes["reference_2"];

	$url  = 'https://www.asiadirect.co.th/line-at/ibroker/linemeapi.php';
	$data = array(
	    'user_id' => $user_id,
	    'status' => "QRpayment",
	    'messages' => $messages
	);
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$result = curl_exec($ch);
		$error_msg = curl_error($ch);
		curl_close($ch);
	return 1;
	exit();
}

$jsonTxt = json_decode(file_get_contents('php://input'), true);
$getQrcode = getQrcode(trim($jsonTxt["transactionId"]));
$logid = insertQrcodelog($jsonTxt["transactionId"], $jsonTxt["paymentId"]);

	if($getQrcode){
		$getMaxIDRV = getMaxIDRV();
		$confirmId = "ADB".date('Ymd')."".$getQrcode["reference_1"]."".$getMaxIDRV;
		$insertReceive = insertReceive($getMaxIDRV,$getQrcode, $jsonTxt["paymentId"]);
// $insertReceive = 1;
		if($insertReceive){
				$sqlupDate = "UPDATE [dbo].[qrcodes] SET [status_pay] = '1', [receive_id] = '".$getMaxIDRV."', [paymentId] = '".$jsonTxt["paymentId"]."', [paymentTime] = '".date('Y-m-d H:i:s')."', [confirmId] = '".$confirmId."', [confirmTime] = '".date('Y-m-d H:i:s')."', [update_date] = '".date('Y-m-d H:i:s')."', [update_by] = 'QR System' WHERE qrcodes_id = '".$getQrcode["qrcodes_id"]."' ";
				$stmtup = sqlsrv_query( $connMS, $sqlupDate);

				$respornse = array();
				$respornse["res_code"] = '00'; 
				$respornse["res_desc"] = 'success'; 
				$respornse["paymentId"] = $jsonTxt["paymentId"]; 
				$respornse["confirmId"] = $confirmId; 

				curlLineNoti(trim($jsonTxt["transactionId"]));

		}else{
				$respornse = array();
				$respornse["res_code"] = '99'; 
				$respornse["res_desc"] = 'FALSE'; 
				$respornse["paymentId"] = $jsonTxt["paymentId"]; 
				$respornse["confirmId"] = $confirmId; 
			}
			upDateQrcodelog($respornse["res_code"], $respornse["res_desc"], $confirmId, $logid);
			echo json_encode($respornse);
			exit();
	}else{
			$respornse = array();
			$respornse["res_code"] = '99'; 
			$respornse["res_desc"] = 'FALSE'; 
			$respornse["paymentId"] = $jsonTxt["paymentId"]; 
			$respornse["confirmId"] = $confirmId;
			upDateQrcodelog($respornse["res_code"], $respornse["res_desc"], $confirmId, $logid);
			echo json_encode($respornse);
			exit(); 
	}





?>