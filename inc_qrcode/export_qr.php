
<?php

date_default_timezone_set('Asia/Bangkok');
header('Content-Type: text/html; charset=utf-8');

$strExcelFileName = "ReportQR".date('YmdHis').".xls";

header("Content-Type: application/vnd.ms-excel; name=\"$strExcelFileName\"");
header("Content-Disposition: inline; filename=\"$strExcelFileName\"");
header("Pragma:no-cache");

include "../inc_config.php"; 
include "inc_function_qr.php";

$date = ($_GET["date"]) ? $_GET["date"] : date("Y-m-d");
$dateEnd = ($_GET["dateend"]) ? $_GET["dateend"] : date("Y-m-d");
if($_GET["date"]){
   $getListQRCode = getListQRCodeByDate($date, $_GET["txtse"], $dateEnd);
}
// $reportInformByAdmin = reportInformByAdmin($_GET["ds"], $_GET["de"], $_GET["insurers"]);
// echo "<pre>".print_r($reportInformByAdmin,1)."</pre>";

?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>
    <div id="SiXhEaD_Excel" align=center x:publishsource="Excel">
        <table x:str border=1 cellpadding=1 cellspacing=1 width=100% style="border-collapse:collapse">
           <tr class="table p5">
                <tr>
					<th class="t_c">RV</th>
					<th class="t_c">วันชำระ</th>
					<th class="t_c">Ref1</th>
					<th class="t_c">Ref2</th>
					<th class="t_c">รวมจ่าย</th>
					<th class="t_c">รหัสลูกค้า</th>
					<th class="t_c">PO</th>
					<th class="t_c">ชื่อลูกค้า</th>
					<th class="t_c">ประเภท</th>
					<th class="t_c">ประกัน</th>
					<th class="t_c">งวดที่</th>
					<th class="t_c">จำนวน</th>
				</tr>
            </tr>
            <?php 
                $i = 1;
                foreach ($getListQRCode as $key => $value) {
                	$countDataDesc = count($value["dataDesc"]);
					$stausPay = ($value["status_pay"]== 1) ? "ชำระ" : "รอชำระ" ;
					$sumtotal += $value["sumAmount"];
            ?>
                <tr>
					<td class="t_l" rowspan="<?php echo $countDataDesc;?>"><?php echo $value["receive_id"]; ?></td>
					<td class="t_l" rowspan="<?php echo $countDataDesc;?>"><?php echo $value["paymentTime"]->format("d/m/Y H:i:s"); ?></td>
					<td class="t_l" rowspan="<?php echo $countDataDesc;?>"><?php echo $value["reference_1"]; ?></td>
					<td class="t_l" rowspan="<?php echo $countDataDesc;?>"><?php echo $value["reference_2"]; ?></td>
					<td class="t_r c9c00c8 fwb" rowspan="<?php echo $countDataDesc;?>"><?php echo number_format($value["sumAmount"],2); ?></td>
					<td class="t_c c2457ff fwb" rowspan="<?php echo $countDataDesc;?>"><?php echo $value["customer_id"]; ?></td>
					<?php foreach ($value["dataDesc"] as $key => $valDesc) { ?>
							<td class="t_c cff2da5 fwb"><?php echo $valDesc["po_id"]; ?></td>
							<td class="t_c "><?php echo $valDesc["customer_name"]; ?></td>
							<td class="t_c ">
								<div><?php echo ($valDesc["insur_type"] == "insur") ? "ประกันภัย" : "พรบ."; ?></div>
							</td>
							<td><div><?php echo $valDesc["Insurer_Initials"]; ?></div></td>
							<td class="t_c "><?php echo $valDesc["installment_num"]; ?></td>
							<td class="t_r cff2da5 fwb"><?php echo number_format($valDesc["amount"],2); ?></td>
						</tr>
					<?php } ?>
				</tr>
				<?php } ?>
        </table>
    </div>

    <script>
        window.onbeforeunload = function(){return false;};
        setTimeout(function(){window.close();}, 10000);
   
    </script>
</body>
</html>
