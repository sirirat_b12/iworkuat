<?php 

session_start();
$UserCode = $_SESSION["User"]['UserCode'];
$userType = $_SESSION["User"]['type'];

if(!isset($UserCode)){
	echo "<script>alert('กรุณาเข้าสู่ระบบ!!!');</script>";
	echo '<META http-equiv="refresh" content="0;URL=login.php">';
	exit();
}

header('Content-Type: text/html; charset=utf-8');
include "include/inc_header.php"; 
include "include/inc_menu.php"; 
include "inc_qrcode/inc_function_qr.php";
include('qrcode/qrcode.class.php');

if(isset($_GET["txtse"])){
	$getListQRCode = getListQRCode($_GET["txtse"]);

}

?>
<div class="main">
	<div class="">
		<div class="p20">
				<div class="row">
					<div class="col-md-12">
						<div class="panel">
								<div class="panel-heading">
									<h4 class="panel-title"><b>ระบบชำระเงิน QR CODE</b></h4>
								</div>
								<div class="panel-body fs14">
									<div class="row">
										<div class="col-md-5 t_r"><h4>รหัสลูกค้า :</h4></div>
										<div class="col-md-2">
											<input class="form-control formInput2" type="text" name="txtSearch" id="txtSearch"  value="<?php echo $_GET["txtsearch"] ?>" required>
										</div>
										<div class="col-md-2">
											<button type="button" id="btn_send_order" class="btn  btn-info"><i class="fa fa-search-plus" aria-hidden="true"></i> เช็คข้อมูล</button>
										</div>
									</div>
									<div class="row mt20">
										<div class="col-md-12">
											<div class="row">
												<div class="col-md-2"></div><div class="col-md-10"><?php echo $value["PO_ID"]; ?></div>
											</div>
											<form action="inc_qrcode/inc_action_qr.php" method="POST" id="frmListPay">
												<input type="hidden" name="action" value="inserListpay">
												<input type="hidden" name="customer_id" id="customer_id" >
												<table class="table table-hover" id="tableMoveQC">
													<thead>
														<tr>
															<th class="t_c">
																<div><input type="checkbox" id="chkInsAll" name="chkInsAll" ></div>
															</th>
															<th class="t_c">งวดที่</th>
															<th class="t_c">PO</th>
															<th class="t_c">ชื่อ-นามสกุล</th>
															<th class="t_c">ประเภทประกัน</th>
															<th class="t_c">กำหนดชำระ</th>
															<th class="t_r">เบี้ยสุทธิ</th>
															<th class="t_r">อากร</th>
															<th class="t_r">ภาษี</th>
															<th class="t_r">เบี้ยรวม</th>
															<th class="t_r">ส่วนลด</th>
															<th class="t_r">จ่ายสุทธิ</th>
															<th class="t_c">สถานะของงวดชำระ</th>
														</tr>
													</thead>
													<tbody id="table_data" class="table-list fs12"><tr><td colspan="13" class="fs18 cf40053 fwb mt20 t_c">กรุณาเลือกข้อมูล</td></tr></tbody>
												</table>	
												<p class="cff2da5">****** ใบชำระธนาคารให้กดตรง รหัส PO ******</p>	
													<div class="row">
														<div class="col-md-4">
															<p class="fwb">Phone</p>
															<input type="text" name="phone" id="txtphone" class="form-control cff2da5" >
														</div>
													</div>												
													<div class="row">
														<div id="Waittxt" class="fs20 t_c  fwb mt25 dn">กรุณารอ..</div>
														<div class="col-md-12 mt20 t_c">
															<input type="button" class="btn btn-success" id="btnsub" value="ตกลง" onclick="btnsubmit()">
															<a href="pay_qrcode.php" class="btn btn-danger" id="btncancel">ยกเลิก</a>
														</div>
													</div>
												</div>
											</form>
										</div>
									</div>
								</div>
						</div>
					</div>
				</div>
		</div>
	</div>
	<div class="">
		<div class="p20">
			<div class="row">
				<div class="col-md-12">
					<div class="panel">
							<div class="panel-heading">
								<div class="row mt15">
									<div class="col-md-2 cff2da5">
										<label for="filter" >Ref.1 | รหัสลูกค้า</label>
										<input type="text" class="cff2da5 form-control fs12" id="txtse" value="<?php echo trim($_GET["txtse"]); ?>">
									</div>
									<div class="col-md-2 ">
										<span class="btn btn-success mt20" onclick="filterPOcode()">ค้นหา</span>
										<a href="pay_qrcode.php" class="btn btn-danger mt20">รีเช็ต</a>
									</div>
								</div>
							</div>
						<?php if($getListQRCode){ ?>
							<div class="panel-body fs14">
									<table class="table table-bordered" >
										<thead class="fs13 c000000">
											<tr>
										<!-- 	<?php if($dataMain["status_pay"]== 0){?>
												<th class="t_c">QR Code</th>
											<?php } ?> -->
												<th class="t_c">สถานะ</th>
												<th class="t_c">Ref</th>
												<th class="t_c">รวมจ่าย</th>
												<th class="t_c">รหัสลูกค้า</th>
												<th class="t_c">PO</th>
												<th class="t_c">ชื่อลูกค้า</th>
												<th class="t_c">ประเภท</th>
												<th class="t_c">งวดที่</th>
												<th class="t_c">วันครบชำระ</th>
												<th class="t_c">จำนวน</th>
												<!-- <th class="t_c">ผู้สร้าง</th> -->
											</tr>
										</thead>
										<tbody class="fs12 c000000">
											<?php 
												foreach ($getListQRCode as $key => $value) { 	
													$countDataDesc = count($value["dataDesc"]);
													$stausPay = ($value["dataMain"]["status_pay"]== 1) ? "ชำระ" : "รอชำระ" ;
													if (!in_array($err, array('L', 'M', 'Q', 'H'))) $err = 'L';
													$code = $value["dataMain"]["qrCode"];
													$installment = str_pad($value["dataDesc"][0]["installment_num"], 2, "0", STR_PAD_LEFT);
													$due_date = $value["dataDesc"][0]["installment_due_date"]->format("d/m/Y");
													$title = $value["dataMain"]["reference_1"]." ".$value["dataMain"]["reference_2"]." ".$value["dataMain"]["sumAmount"]." ".$installment." ".$due_date;   
												  $url = "inc_qrcode/qrcode/image.php?msg=".urlencode($code)."&amp;err=".urlencode($err)."&amp;title=".urlencode($title);
													$paycose = $getListCounterAll[0]["paycode"];
											?>
												<tr>
												<!-- <?php if($value["dataMain"]["status_pay"] == 0){?>
													<td class="t_c c9c00c8" rowspan="<?php echo $countDataDesc;?>">
													</td>
												<?php } ?> -->
													<td class="t_l" rowspan="<?php echo $countDataDesc;?>">
														<div class="  t_c fwb fs16">
															<span class="c00ac0a" ><?php echo ($value["dataMain"]["enable"]=='Y') ? "ใช้งาน" : "ยกเลิก" ?></span> | <span class="c2457ff"><?php echo $stausPay; ?></span>
														</div>
														<?php if($value["dataMain"]["receive_id"]){?>
															<div class="cff8000 fs16 fwb t_c"><?php echo $value["dataMain"]["receive_id"]; ?></div>
															<div class="fs10 t_c"><?php echo $value["dataMain"]["paymentTime"]->format("d/m/Y H:i:s"); ?></div>
															<div class="fs12"><b>PayCode:</b> <?php echo $value["dataMain"]["paymentId"]; ?></div>
															<div class="fs12"><b>ConfirmId:</b> <?php echo $value["dataMain"]["confirmId"]; ?></div>
															<div class="fs10"><b>PayCodeTime:</b> <?php echo $value["dataMain"]["paymentTime"]->format("d/m/Y H:i:s"); ?></div>
														<?php }else{ ?>
															<?php if($value["dataMain"]["status_pay"] == 0 && $value["dataMain"]["enable"] == 'Y'){?> 
																<div class="text-center p10">
																	<a href="<?php echo $url ?>" data-fancybox="QRCode" data-magnify="gallery" data-src="" data-group="a" title="<?php echo $value["dataMain"]["reference_1"] ?>">
																		<img src="<?php echo $url ?>" alt="generation qr-code" title="<?php echo $value["dataMain"]["reference_1"] ?>" name="<?php echo $value["dataMain"]["reference_1"] ?>" style="border: 1px solid #4CAF50;">
																	</a>
																	<p><a download="<?php echo $value["dataMain"]["reference_1"].".png" ?>" href="<?php echo $url ?>" title="<?php echo $value["dataMain"]["reference_1"] ?>" class="fs18 fwb cff2da5" ><b>Download</b></a></p>
																</div>
															<?php } ?>
														<?php } ?>
													</td>
													<td class="t_l" rowspan="<?php echo $countDataDesc;?>">
															<div class="cff2da5"><b>Ref.1</b> <?php echo $value["dataMain"]["reference_1"]; ?></div>
															<div class="cff2da5"><b>Ref.2</b> <?php echo $value["dataMain"]["reference_2"]; ?></div>
															<div class="cff2da5"><b>Transaction</b> <?php echo $value["dataMain"]["transactionId"]; ?></div>
															<div><b>เวลาสร้าง: </b><?php echo $value["dataMain"]["created_date"]->format("d/m/Y H:i:s"); ?></div>
															<div class="fs10"><b>โดย: </b><?php echo $value["dataMain"]["created_by"]; ?></div>
															<div class="fs16 mt20 ">
																<a href="showqr.php?code='<?php echo base64_encode($value["dataMain"]["reference_1"]) ?>'" target="_bank" class="c2457ff  ">ลิ้งลูกค้า</a>
																<?php if($value["dataMain"]["status_pay"] == 0 && $value["dataMain"]["enable"] == 'Y'){?> 
																	| <span class="cf80000 cursorPoin" onclick="fnDelQR(<?php echo $value["dataMain"]["qrcodes_id"]; ?>)">ยกเลิก QR</span>
																<?php } ?>
															</div>
													</td>
													<td class="t_r c9c00c8 fwb" rowspan="<?php echo $countDataDesc;?>"><?php echo number_format($value["dataMain"]["sumAmount"],2); ?></td>
													<td class="t_c c2457ff fwb" rowspan="<?php echo $countDataDesc;?>"><?php echo $value["dataMain"]["customer_id"]; ?></td>
													<?php foreach ($value["dataDesc"] as $key => $value) { ?>
															<td class="t_c cff2da5 fwb"><?php echo $value["po_id"]; ?></td>
															<td class="t_c "><?php echo $value["customer_name"]; ?></td>
															<td class="t_c "><?php echo ($value["insur_type"] == "insur") ? "ประกันภัย" : "พรบ."; ?></td>
															<td class="t_c "><?php echo $value["installment_num"]; ?></td>
															<td class="t_c "><?php echo $value["installment_due_date"]->format("d/m/Y"); ?></td>
															<td class="t_r cff2da5 fwb"><?php echo number_format($value["amount"],2); ?></td>
														</tr>
													<?php } ?>
											<?php } ?>
										</tbody>
								</table>
							</div>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</div> 
</div>
<?php include "include/inc_footer.php"; ?> 
<!-- Magnify Image Viewer CSS -->
<link href="magnifybox/css/jquery.magnify.css" rel="stylesheet">
<!-- Magnify Image Viewer JS -->
<script src="magnifybox/js/jquery.magnify.js"></script>
<script type="text/javascript">
	function fnDelQR(qrcodes_id){
		// console.log(qrcodes_id);
		txtse = $("#txtse").val();
		$.ajax({ 
				url: 'inc_qrcode/inc_action_qr.php',
				type:'POST',
				data: {action: 'deleteQR', qrcodes_id:qrcodes_id},
				success:function(rs){ 
					window.location.href = "pay_qrcode.php?txtse="+txtse;
				}
		});
	}
	
	function btnsubmit(){
		var countCK = $('input[name="inskey[]"]:checked').length;
  	if(countCK > 0){
  		if( confirm("คุณต้องการสร้างการชำระเงินผ่านระบบ Qr Code?")){
  			$("#btnsub, #btncancel").hide();
  			$("#Waittxt").show();
				$("#frmListPay").submit();
  		}
  	}else{
  		alert("กรุณาเลือกงวดที่ชำระ");
  		$("#btnsub, #btncancel").show();
  		$("#Waittxt").hide();
  	}
	}
	$("#chkInsAll").click(function() {
    if($('#chkInsAll').is(':checked')){
      $(".inskey").prop('checked', true);
      
    }else{
      $(".inskey").prop('checked', false);
      
    }
  });

	$("#btn_send_order").click(function() {
			$("#table_data").empty();
    	var txtsearch = $("#txtSearch").val();
    	console.log(txtsearch);
    	if(txtsearch){
	    	$.ajax({ 
						url: 'inc_qrcode/inc_action_qr.php',
						type:'POST',
						data: {action: 'GetListPayQR', txtsearch:txtsearch},
						success:function(rs){ 
							$("#customer_id").val(txtsearch)
							if(rs){
								$('#table_data').append(rs);
							}else{
								$('#table_data').append("<td colspan='13'><div class='fs18 cf40053 fwb mt20 t_c'>ไม่พบข้อมูลกรุณาตรวจสอบ</div></td>");
							}
						}
				});
				$.ajax({ 
						url: 'inc_qrcode/inc_action_qr.php',
						type:'POST',
						dataType: 'json',
						data: {action: 'GetDataPhone', txtsearch:txtsearch},
						success:function(rs){ 
							// console.log(rs.phone);
							$("#txtphone").val(rs.phone);
							$("#txtemail").val(rs.email);
							// $('#table_data').append(rs);
						}
				});
			}else{
				alert("กรุณากรอกรหัสลูกค้าหรือเบอร์โทรศัพท์ลูกค้า");
			}

  });

  

function filterPOcode(){
	txtse = $("#txtse").val();
	if(txtse){
		window.location.href = "pay_qrcode.php?txtse="+txtse;
	}else{
		alert("กรุณากรอก ข้อมูล เพื่อค้นหา");
	}
}


// $( '[data-fancybox="QRCode"]' ).fancybox({
//   buttons : [
//     'download',
//     'zoom',
//     'close'
//   ],
//   protect : false,
//   animationEffect : "zoom-in-out",
//   transitionEffect : "fade",
//   zoomOpacity : "auto",
//   animationDuration : 500,
//   zoomType: 'innerzoom',

// });
</script>