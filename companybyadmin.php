<?php 

session_start();
if(!isset($_SESSION["User"]['UserCode'])){
	echo "<script>alert('กรุณาเข้าสู่ระบบ!!!');</script>";
	echo '<META http-equiv="refresh" content="0;URL=login.php">';
	exit();
}
if(!$_SESSION["User"]['type'] == "SuperAdmin" || !$_SESSION["User"]['type'] == "Admin"  ){
	echo '<META http-equiv="refresh" content="0;URL=sendorders.php">';
	exit();
}
header('Content-Type: text/html; charset=utf-8');
include "include/inc_header.php"; 
include "include/inc_menu.php"; 

$getInsurer = getInsurer();
$getPersonnelAdmin = getPersonnelByType("Admin");
// echo "<pre>".print_r($getPersonnelAdmin,1)."</pre>";
?>
<div class="main">
	<div class="main-content p20">
		<div class="p20">
				<div class="row">
					<div class="col-md-12">
						<div class="panel">
								<div class="panel-heading">
									<h4 class="panel-title"><b>การรับผิดชอบบริษัทแจ้งงานของ Admin</b></h4>
								</div>
								<div class="panel-body fs14">
									<?php $i=1;
									foreach ($getInsurer as $key => $value) { ?>
										<div class="row mt15">
											<div class="col-md-4 t_r">
											 	<b class="c1641ff"><?php echo $value["insurer_name"]; ?></b>
											</div>
											<div class="col-md-4 t_l">
												<select name="adminInsur_<?php echo $key ?>" id="adminInsur_<?php echo $key ?> " class="form-control " onchange="ftChangAdmin(<?php echo $key ?>)">
													<?php foreach ($getPersonnelAdmin as $key => $val) { ?>
														<option value="<?php echo $val["personnel_code"]?>" <?php echo ($val["personnel_code"] == $value["insurer_admin"]) ? "selected" : "" ;?>>
															<?php echo $val["personnel_code"]." - ".$val["personnel_firstname"]." ".$val["personnel_lastname"]?>
														</option>
													<?php } ?>
												</select>
											</div>
										</div>
									<?php } ?>
								</div>
						</div>
					</div>
				</div>
		</div>
	</div>
</div>
<?php include "include/inc_footer.php"; ?> 
<script type="text/javascript">
	function ftChangAdmin(id) {
		if(confirm("ต้องการเปลี่ยนผู้รับผิดชอบ?")){
			// console.log("select[name=adminInsur_"+id+"]");
			admin = $("select[name=adminInsur_"+id+"]").val();
			// console.log("#adminInsur_"+admin);
			$.ajax({ 
				url: 'include/inc_action_chk.php',
				type:'POST',
				data: {action: 'updateAdminInser', insurer_admin:admin, insurer_id:id},
				success:function(rs){
					if(rs == 0){
						alert("ไม่สามารถทำรายได้ กรุณาลองใหม่");
					}else{
						alert("เปลี่ยนข้อมูลเรียบร้อย");
						window.location.reload(true);
					}
				}
			});
		}
	}
</script>