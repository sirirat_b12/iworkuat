<?php 

session_start();
if(!isset($_SESSION["User"]['UserCode'])){
	echo "<script>alert('กรุณาเข้าสู่ระบบ!!!');</script>";
	echo '<META http-equiv="refresh" content="0;URL=login.php">';
	exit();
}
if(!$_SESSION["User"]['type'] == "SuperAdmin" || !$_SESSION["User"]['type'] == "Admin"  ){
	echo '<META http-equiv="refresh" content="0;URL=sendorders.php">';
	exit();
}
header('Content-Type: text/html; charset=utf-8');
include "include/inc_header.php"; 
include "include/inc_menu.php"; 

$getInsurer = getInsurer();
$getQualityControl = getPersonnelByType("QualityControl");
$getPersonnelAdmin = getPersonnelByType("Admin");
// echo "<pre>".print_r($getPersonnelAdmin,1)."</pre>";
?>
<div class="main">
	<div class="main-content p20">
		<div class="p20">
			<?php if($_SESSION["User"]['type'] == "SuperAdmin" || $_SESSION["User"]['type'] == "QualityControl"  ){ ?>
				<div class="row">
					<div class="col-md-12">
						<div class="panel">
								<div class="panel-heading">
									<h4 class="panel-title"><b>การเข้างานของ Quality Control</b></h4>
								</div>
								<div class="panel-body fs14">
									<?php $i=1;
									foreach ($getQualityControl as $key => $value) { ?>
										<div class="row mt15">
											<div class="col-md-3 t_l">
											 	<b class="c1641ff"><?php echo $value["personnel_code"]." - ".$value["personnel_firstname"]." ".$value["personnel_lastname"]?></b>
											</div>
											<div class="col-md-4 t_l">
												<select name="adminInsur_<?php echo $value["personnel_code"]; ?>" id="adminInsur_<?php echo $value["personnel_code"]; ?> " class="form-control " onchange="ftChangInOut('<?php echo $value["personnel_code"]; ?>')">
													<option value="0" <?php echo ($value["personnel_workin"] == 0) ? "selected" : "" ;?>>ไม่อยู่</option>
													<option value="1" <?php echo ($value["personnel_workin"] == 1) ? "selected" : "" ;?>>อยู่</option>
												</select>
											</div>
										</div>
									<?php } ?>
								</div>
						</div>
					</div>
				</div>
			<?php } ?>
			<?php if($_SESSION["User"]['type'] == "SuperAdmin" || $_SESSION["User"]['type'] == "Admin"  ){ ?>
				<div class="row">
					<div class="col-md-12">
						<div class="panel">
								<div class="panel-heading">
									<h4 class="panel-title"><b>การเข้างานของ Admin</b></h4>
								</div>
								<div class="panel-body fs14">
									<?php $i=1;
									foreach ($getPersonnelAdmin as $key => $value) { ?>
										<div class="row mt15">
											<div class="col-md-3 t_l">
											 	<b class="c1641ff"><?php echo $value["personnel_code"]." - ".$value["personnel_firstname"]." ".$value["personnel_lastname"]?></b>
											</div>
											<div class="col-md-4 t_l">
												<select name="adminInsur_<?php echo $value["personnel_code"]; ?>" id="adminInsur_<?php echo $value["personnel_code"]; ?> " class="form-control " onchange="ftChangInOut('<?php echo $value["personnel_code"]; ?>')">
													<option value="0" <?php echo ($value["personnel_workin"] == 0) ? "selected" : "" ;?>>ไม่อยู่</option>
													<option value="1" <?php echo ($value["personnel_workin"] == 1) ? "selected" : "" ;?>>อยู่</option>
												</select>
											</div>
										</div>
									<?php } ?>
								</div>
						</div>
					</div>
				</div>
			<?php } ?>
		</div>
	</div>
</div>
<?php include "include/inc_footer.php"; ?> 
<script type="text/javascript">
	function ftChangInOut(code) {
		if(confirm("ต้องการเปลี่ยนผล")){
			work = $("select[name=adminInsur_"+code+"]").val();
			$.ajax({ 
				url: 'include/inc_action_chk.php',
				type:'POST',
				data: {action: 'updateWorkInOut', personnel_workin:work, personnel_code:code},
				success:function(rs){
					if(rs == 0){
						alert("ไม่สามารถทำรายได้ กรุณาลองใหม่");
					}else{
						alert("เปลี่ยนข้อมูลเรียบร้อย");
						window.location.reload(true);
					}
				}
			});
		}
	}
</script>