<?php 
session_start();
$UserCode = $_SESSION["User"]['UserCode'];
$userType = $_SESSION["User"]['type'];

if(!isset($UserCode)){
	echo "<script>alert('กรุณาเข้าสู่ระบบ!!!');</script>";
	echo '<META http-equiv="refresh" content="0;URL=login.php">';
	exit();
}

header('Content-Type: text/html; charset=utf-8');
include "include/inc_header.php"; 
include "include/inc_menu.php"; 
include "reports/inc_function_report.php";

$dateStart = ($_GET["ds"]) ? $_GET["ds"] : date("Y-m-d");
$dateEnd = ($_GET["de"]) ? $_GET["de"] : date("Y-m-d");

$insurer = getInsurerAll();

if($dateStart){
	$getinsurer = $_GET["insurers"];
	$reportInformByAdmin = reportInformByAdmin($dateStart, $dateEnd, $getinsurer);
}
// echo "<pre>".print_r($reportInformByAdmin,1)."</pre>";
?>

<div class="main">
	<div class="row p20">
		<div class="col-md-12">
			<div class="panel">
					<div class="panel-heading">
						<div class="row mt15">
							<div class="col-md-2 cff2da5">
								<label for="filter" >เริ่ม</label>
								<input type="date" class=" form-control fs12" id="dateStart" value="<?php echo $dateStart; ?>">
							</div>
							<div class="col-md-2 cff2da5">
								<label for="filter" >ถึง</label>
								<input type="date" class=" form-control fs12" id="dateEnd" value="<?php echo $dateEnd; ?>">
							</div>
							<div class="col-md-2 cff2da5">
								<label for="filter" >บริษัทประกัน</label>
								<select class="form-control formInput2" id="insurers" name="insurer">
						  		<option value="0">--- กรุณาเลือก ---</option>
						  		<?php foreach ($insurer as $key => $value) { ?>
						  			<option value="<?php echo $value['insurer_initials']; ?>" <?php if($getinsurer == $value['insurer_initials']){ echo "selected"; } ?>><?php echo $value['insurer_name']; ?></option>
						  		<?php } ?>
						    </select>
							</div>
							<div class="col-md-2 ">
								<a href="report_inform.php" class="btn btn-danger mt20">รีเช็ต</a>
								<span class="btn btn-success mt20" onclick="filterInform()">ค้นหา</span>
								<sapn class="btn btn-info mt20" onclick="btnExport()">Export</sapn>
							</div>
						</div>
					</div>
					<div class="panel-body fs14">
						<div>
							<table id="table" class="table table-striped p5 table-hover" data-toggle="table">
								<thead>
									<tr>
										<th class="t_c">PO</th>
										<th class="t_c">วันที่แจ้งงาน</th>
										<th class="t_c">สถานะ</th> 
										<th class="t_c">ชื่อ-นามสกุล</th>
										<th class="t_c">ประเภทประกัน</th>
										<th class="t_c">บริษัท</th>
										<th class="t_c">แพจเกต</th>
										<th class="t_c">วันคุ้มครอง</th>
										<th class="t_c">เลขกรม</th>
										<th class="t_c">ส่งกรม</th>
										<th class="t_c">ผู้ขาย</th>
									</tr>
								</thead>
								<tbody class="fs12">
									<?php 
										foreach ($reportInformByAdmin as $key => $value) { 
											$endorse = ($value["endorse"] == "1") ? "สลักหลัง" : "กรมธรรม์ใหม่";
									?>
										<tr>
											<td class="cff2da5 fwb t_c"><?php echo $value["po_code"];?></td>
											<td class="t_c">
												<div class=""><?php echo $value["adminFName"]." ".$value["adminLName"]; ?></div>
												<div class="cf40053 "><?php echo $value["datetime_send"]; ?></div>
											</td>
											<td class="t_c"><?php echo $endorse ?></td> 
											<td class="fwb c9c00c8"><?php echo $value["cus_name"]; ?></td>
											<td class="fwb c2457ff"><?php echo $value["insurance_type"]; ?></td>
											<td class="fwb cf40053"><?php echo $value["insuere_company"]; ?></td>
											<td class="t_l">
												<div class="clearfix c9c00c8"><?php echo $value["package_name"]; ?></div>
											</td>
											<td class="t_c"><?php echo date("d-m-Y",strtotime($value["start_cover_date"])); ?></td>
											<td class="t_c c2457ff"><?php echo $value["policy_number"]; ?></td>
											<td class="t_c fwb"><?php echo $value["send_type"]; ?></td>
											<td class="t_c">
												<div class="clearfix c2457ff "><?php echo $value["personnel_name"]; ?></div>
												<div class="fs10 cf80000"><?php echo $value["created_date"]; ?></div>
											</td>
										</tr>
									<?php } ?>
								</tbody>
							</table>

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php include "include/inc_footer.php"; ?> 

<script type="text/javascript">

	function filterInform(){
		var dateStart = $("#dateStart").val();
		var dateEnd = $("#dateEnd").val();
		var insurers = $("#insurers").val();
		if(dateStart && !insurers){
			window.location.href = "report_inform.php?ds="+dateStart+"&de="+dateEnd;
		}else if(dateStart && insurers){
			window.location.href = "report_inform.php?ds="+dateStart+"&de="+dateEnd+"&insurers="+insurers;
		}
	}


	function btnExport(){
		var dateStart = $("#dateStart").val();
		var dateEnd = $("#dateEnd").val();
		var insurers = $("#insurers").val();
		if(dateStart){
			// window.open("reports/export_inform.php?date="+date+"&insurers="+insurers);
			window.location.href = "reports/export_inform.php?ds="+dateStart+"&de="+dateEnd+"&insurers="+insurers;
		}
	}
</script>