<?php 
session_start();
ini_set('max_execution_time', 186);
ini_set('memory_limit', '2048M');
$UserCode = $_SESSION["User"]['UserCode'];
$userType = $_SESSION["User"]['type'];

if(!isset($UserCode)){
	echo "<script>alert('กรุณาเข้าสู่ระบบ!!!');</script>";
	echo '<META http-equiv="refresh" content="0;URL=login.php">';
	exit();
}

header('Content-Type: text/html; charset=utf-8');
include "include/inc_header.php"; 
include "include/inc_menu.php"; 
include "reports/inc_function_report.php";
// include('qrcode/qrcode.class.php');

$year = date("Y");

$getDepartment = getDepartment();
$getFollowUpSale = getFollowUpSale($_GET["depar"]);

?>
<div class="main">
	<div class="">
		<div class="p20">
				<div class="row">
					<div class="col-md-12">
						<div class="panel">
								<div class="panel-heading"><h4 class="panel-title"><b>รายงานการติดตาม</b></h4></div>
								<div class="panel-body fs14">
									<div class="row">
										<div class="col-md-4 ">
											<span class="fs18 cff2da5 mr15">ทีม</span>
											<select name="Department" id="Department" class="form-control formInput2 dib" style="width: 80%" >
												<option value="">:: กรุณาเลือก ::</option>
												<?php foreach ($getDepartment as $key => $value) {?>
													<option value="<?php echo $value["Department_ID"];?>" <?php if($_GET["depar"] ==  $value["Department_ID"]){ echo "selected"; } ?>>
														<?php echo $value["Department_Name"]." | ".$value["Remark"];?>
													</option>
												<?php } ?>
											</select>
										</div>
									</div>
									<div class="row mt20">
										<div class="col-md-12">
										<p class="cff2da5">จำนวน CP ทั้งหมด เริ่มตั้งแต่วันที่ <?php echo date("Y")."-01-01" ?></p>
											<div class="row">
												<div class="col-md-2"></div><div class="col-md-10"><?php echo $value["PO_ID"]; ?></div>
											</div>
											<?php if($getFollowUpSale){?>
												<table class="table table-hover table-bordered" id="indextable" >
													<thead>
														<tr class="bgbaf4bc">
															<th class="t_c">รหัสพนักงาน</th>
															<th class="t_c">ชื่อ-นามสกุล</th>
															<th class="t_c">ทีม</th>
															<th class="t_c"><a href="javascript:SortTable(3,'N');">ติดตามวันนี้</a></th>
															<th class="t_c"><a href="javascript:SortTable(4,'N');">ติดตามวันพรุ่งนี้</a></th>
															<th class="t_c"><a href="javascript:SortTable(5,'N');">ยังไม่ติดตาม ถึงปัจจุบัน</a></th>
															<th class="t_c"><a href="javascript:SortTable(6,'N');">CP ดำเนินการ/งานใหม่</a></th>
															<th class="t_c"><a href="javascript:SortTable(6,'N');">CP ดำเนินการ/ต่ออายุ</a></th>
															<th class="t_c"><a href="javascript:SortTable(7,'N');">CP ยกเลิก</a></th>
															<th class="t_c"><a href="javascript:SortTable(8,'N');">CP ReAssign</a></th>
															<th class="t_c"><a href="javascript:SortTable(9,'N');">CP ออกใบสั่งซื้อ</a></th>
															<th class="t_c"><a href="javascript:SortTable(10,'N');">CP รวม</a></th>
														</tr>
													</thead>
													<tbody >
														<?php 
															$sumAll = 0;
															foreach ($getFollowUpSale as $key => $value) {
																$totalDateNowFU += $value["DateNowFU"];
																$totalTomorrowFU += $value["TomorrowFU"];
																$totalLastFU += $value["LastFU"];
																$totalCountCPYearActiveNew += $value["CountCPYearActiveNew"];
																$totalCountCPYearActiveOld += $value["CountCPYearActiveOld"];
																$totalCountCPYearREJ += $value["CountCPYearREJ"];
																$totalCountReAssign += $value["CountReAssign"];
																$totalCountCPYearPOI += $value["CountCPYearPOI"];
																$totalCountCPYearAll += $value["CountCPYearAll"];
														?>
															<tr>
																<td class="t_c"><?php echo $value["User_ID"]; ?></td>
																<td class="t_l"><?php echo $value["User_FName"]." ".$value["User_LName"]; ?></td>
																<td class="t_c"><?php echo $value["Department_Name"]; ?></td>
																<td class="t_r fwb c2457ff "><?php echo $value["DateNowFU"]; ?></td>
																<td class="t_r fwb c2457ff "><?php echo $value["TomorrowFU"]; ?></td>
																<td class="t_r fwb cf40053 "><?php echo $value["LastFU"]; ?></td>
																<td class="t_r fwb cff2da5 "><?php echo $value["CountCPYearActiveNew"]; ?></td>
																<td class="t_r fwb cff2da5 "><?php echo $value["CountCPYearActiveOld"]; ?></td>
																<td class="t_r fwb cff2da5 "><?php echo $value["CountCPYearREJ"]; ?></td>
																<td class="t_r fwb cff2da5 "><?php echo $value["CountReAssign"]; ?></td>
																<td class="t_r fwb cff2da5 "><?php echo $value["CountCPYearPOI"]; ?></td>
																<td class="t_r fwb cff2da5 "><?php echo $value["CountCPYearAll"]; ?></td>
															</tr>
															<?php } ?>
													</tbody>
													<tfoot>
														<tr class="fwb fs16 bgfffbd8">
															<td class="t_c" colspan="3">รวม</td>
															<td class="t_r c2457ff "><?php echo $totalDateNowFU; ?></td>
															<td class="t_r c2457ff "><?php echo $totalTomorrowFU; ?></td>
															<td class="t_r cf40053 "><?php echo number_format($totalLastFU); ?></td>
															<td class="t_r fwb cff2da5 "><?php echo number_format($totalCountCPYearActiveNew); ?></td>
															<td class="t_r fwb cff2da5 "><?php echo number_format($totalCountCPYearActiveOld); ?></td>
															<td class="t_r fwb cff2da5 "><?php echo number_format($totalCountCPYearREJ); ?></td>
															<td class="t_r fwb cff2da5 "><?php echo number_format($totalCountReAssign); ?></td>
															<td class="t_r fwb cff2da5 "><?php echo number_format($totalCountCPYearPOI); ?></td>
															<td class="t_r fwb cff2da5 "><?php echo number_format($totalCountCPYearAll); ?></td>
														</tr>
													</tfoot>
												</table>
											<?php } ?>
										</div>
									</div>
								</div>
						</div>
					</div>
				</div>
		</div>
	</div>
</div>
<?php include "include/inc_footer.php"; ?> 
<?php include "include/inc_function_tablesort.php"; ?>
<script type="text/javascript">


  $("#Department").on('change', function() {
  	txtse = $("#txtse").val();
		if(this.value){
			window.location.href = "report_fu.php?depar="+this.value;
		}

  });


</script>



