<?php 
session_start();
ini_set('max_execution_time', 186);
ini_set('memory_limit', '2048M');
$UserCode = $_SESSION["User"]['UserCode'];
$userType = $_SESSION["User"]['type'];

if(!isset($UserCode)){
	echo "<script>alert('กรุณาเข้าสู่ระบบ!!!');</script>";
	echo '<META http-equiv="refresh" content="0;URL=login.php">';
	exit();
}

header('Content-Type: text/html; charset=utf-8');
include "include/inc_header.php"; 
include "include/inc_menu.php"; 
include "reports/inc_function_report.php";
// include('qrcode/qrcode.class.php');

$year = date("Y");
$_GET["txtse"] = ($_GET["txtse"]) ? $_GET["txtse"] : date("m") ;
$_GET["year"] = ($_GET["year"]) ? $_GET["year"] : date("Y") ;

$condi = $_GET["year"].'-'.$_GET["txtse"];


$getDepartment = getDepartment();
if($_GET["txtse"]){
	$getSaleFromRenew = getSaleFromRenew($_GET["txtse"], $_GET["year"], $_GET["depar"]);
}

// echo "<pre>".print_r($getSaleFromRenew,1)."</pre>";
?>
<div class="main">
	<div class="">
		<div class="p20">
				<div class="row">
					<div class="col-md-12">
						<div class="panel">
								<div class="panel-heading">
									<h4 class="panel-title"><b>รายงานยอดขายเดือน <?php echo date("F");?></b></h4>
								</div>
								<div class="panel-body fs14">
									<div class="row">
										<div class="col-md-4 ">
											<span class="fs18 cff2da5 mr15">เดือน</span>
											<select name="mounth" id="mounthOpt" class="form-control formInput2 dib" style="width: 80%" >
												<option value="">:: กรุณาเลือก ::</option>
													<?php 
															$dateLast = ( date("Y") - 1)."-".date("m");
															for($i=1; $i<=15; $i++) { 
																$txtM = "+".$i." month";
																$dateForValue = date("Y-m",strtotime($txtM, strtotime($dateLast)));
														?>
														<option value="<?php echo $dateForValue ;?>" <?php if($condi ==  $dateForValue){ echo "selected"; } ?>>
															<?php echo date("Y m F",strtotime($dateForValue)) ?>
														</option>
													<?php } ?>
											</select>
										</div>
										<div class="col-md-4 ">
											<span class="fs18 cff2da5 mr15">ทีม</span>
											<select name="Department" id="Department" class="form-control formInput2 dib" style="width: 80%" >
												<option value="">:: กรุณาเลือก ::</option>
												<?php foreach ($getDepartment as $key => $value) {?>
													<option value="<?php echo $value["Department_ID"];?>" <?php if($_GET["depar"] ==  $value["Department_ID"]){ echo "selected"; } ?>>
														<?php echo $value["Department_Name"]." | ".$value["Remark"];?>
													</option>
												<?php } ?>
											</select>
										</div>
									</div>
									<div class="row mt20">
										<div class="col-md-12">
											<div class="row">
												<div class="col-md-2"></div><div class="col-md-10"><?php echo $value["PO_ID"]; ?></div>
											</div>
											<?php if($getSaleFromRenew){?>
												<table class="table table-hover table-bordered" id="indextable" >
													<thead>
														<tr class="bgbaf4bc">
															<th class="t_c">รหัสพนักงาน</th>
															<th class="t_c">ทีม</th>
															<th class="t_c">ชื่อ-นามสกุล</th>
															<th class="t_c">จำนวน CP</th>
															<th class="t_c">PO Paid</th>
															<th class="t_c">Paid(คัน)</th>
															<th class="t_c">PO (รอ)</th>
															<th class="t_c">%RWคาด (คัน)</th>
															<th class="t_c"><a href="javascript:SortTable(8,'N');">ยอดตั้งต้น</a></th>
															<th class="t_c"><a href="javascript:SortTable(9,'N');">Paid(ยอด)</a></th>
															<th class="t_c"><a href="javascript:SortTable(10,'N');">PV</a></th>
															<th class="t_c"><a href="javascript:SortTable(11,'N');">%ต่ออายุ</a></th>
															<th class="t_c"><a href="javascript:SortTable(12,'N');">RCA</a></th>
															<th class="t_c"><a href="javascript:SortTable(13,'N');">เปิดPOแล้ว</a></th>
															<th class="t_c"><a href="javascript:SortTable(14,'N');">เป้า 73%</a></th>
															<th class="t_c"><a href="javascript:SortTable(15,'N');">ขาดยอดถึงเป้า</a></th>
															<th class="t_c"><a href="javascript:SortTable(16,'N');">%คาดจบ(ยอด)</a></th>
														</tr>
													</thead>
													<tbody >
														<?php 
															$sumAll = 0;
															
															foreach ($getSaleFromRenew as $key => $value) {
																$perRenew = 0;
																$openPO = 0;
																$goal73per = 0;
																$missingTarget = 0;
																$perClosePO = 0;
																$sumPaidPV = 0;
																$countPOPaidPV = 0;

																$sumPaidPV = $value["sumPaid"] + $value["sumPV"];
																$countPOPaidPV = $value["countPOPaid"] + $value["countPOPV"];
																$perRenew = ($sumPaidPV * 100 ) / $value["startPrice"];
																$openPO = $value["sumRCA"] + $sumPaidPV;
																$goal73per = ($value["startPrice"] / 100 ) * 73;
																$missingTarget = $openPO - $goal73per;
																$perClosePO = ($openPO * 100 ) / $value["startPrice"];
																$perPaid = ( $countPOPaidPV * 100) / $value["countCP"];
																$perPORate = (($countPOPaidPV + $value["countRCA"]) * 100) / $value["countCP"];
																
																$target60per = (($value["startPrice"] / 100 ) * 60) - $sumPaidPV ;
																$target70per = (($value["startPrice"] / 100 ) * 70) - $sumPaidPV ;
																$target80per = (($value["startPrice"] / 100 ) * 80) - $sumPaidPV ;



																$totalcountCP += $value["countCP"]; 
																$totalstartPrice += $value["startPrice"];
																$totalcountPOPaid += $countPOPaidPV; 
																$totalsumPaid +=  $value["sumPaid"];
																$totalsumPV +=  $value["sumPV"];
																$totalsumRCA +=  $value["sumRCA"];
																$totalopenPO += $openPO;
																$totalgoal73per +=  $goal73per;
																$totalmissingTarget +=  $missingTarget;
																$totalPerPaid += $perPaid;
																$totalCountRCA += $value["countRCA"];
																// $totaltarget60per +=  $target60per;
																// $totaltarget70per +=  $target70per;
																// $totaltarget80per +=  $target80per;
															// if($sumAll){
														?>
															<tr>
																<td class="t_c"><?php echo $value["Employee_ID"]; ?></td>
																<td class="t_c"><?php echo $value["Department_Name"]; ?></td>
																<td class="t_l"><?php echo $value["User_FName"]." ".$value["User_LName"]; ?></td>
																<td class="t_c c00ac0a"><?php echo $value["countCP"]; ?></td>
																<td class="t_c c00ac0a"><?php echo $countPOPaidPV; ?></td>
																<td class="t_r c00ac0a"><?php echo number_format($perPaid,2); ?></td>
																<td class="t_r c00ac0a"><?php echo $value["countRCA"]; ?></td>
																<td class="t_r c00ac0a"><?php echo number_format($perPORate,2); ?></td>
																<td class="t_r cff2da5 "><?php echo number_format($value["startPrice"],2); ?></td>
																<td class="t_r cff2da5 "><?php echo number_format($value["sumPaid"],2); ?></td>
																<td class="t_r cff2da5 "><?php echo number_format($value["sumPV"],2); ?></td>
																<td class="t_r fwb cff2da5 "><?php echo number_format($perRenew,2); ?></td>
																<td class="t_r cff2da5 "><?php echo number_format($value["sumRCA"],2); ?></td>
																<td class="t_r c2457ff"><?php echo number_format($openPO,2); ?></td>
																<td class="t_r fwb c2457ff"><?php echo number_format($goal73per,2); ?></td>
																<td class="t_r c2457ff"><?php echo number_format($missingTarget ,2); ?></td>
																<td class="t_r fwb c2457ff "><?php echo number_format($perClosePO,2); ?></td>
															</tr>
															<?php //}
															} ?>
														<tfoot>
															<tr class="fwb fs16 bgfffbd8">
																<td class="t_c" colspan="3">รวม</td>
																<td class="t_c fwb c00ac0a"><?php echo $totalcountCP; ?></td>
																<td class="t_r fwb c00ac0a"><?php echo $totalcountPOPaid; ?></td>
																<td class="t_r fwb c00ac0a"><?php echo number_format(($totalcountPOPaid*100)/$totalcountCP,2); ?></td>
																<td class="t_r fwb c00ac0a"><?php echo $totalCountRCA; ?></td>
																<td class="t_r fwb c00ac0a"><?php echo number_format((($totalcountPOPaid + $totalCountRCA)*100)/$totalcountCP,2); ?></td>
																<td class="t_r fwb cff2da5 "><?php echo number_format($totalstartPrice,2); ?></td>
																<td class="t_r fwb cff2da5 "><?php echo number_format($totalsumPaid,2); ?></td>
																<td class="t_r fwb cff2da5 "><?php echo number_format($totalsumPV,2); ?></td>
																<td class="t_r fwb cff2da5 "><?php echo number_format(($totalsumPaid*100)/$totalstartPrice,2); ?></td>
																<td class="t_r fwb cff2da5 "><?php echo number_format($totalsumRCA,2); ?></td>
																<td class="t_r fwb c2457ff"><?php echo number_format($totalopenPO,2); ?></td>
																<td class="t_r fwb c2457ff"><?php echo number_format($totalgoal73per,2); ?></td>
																<td class="t_r fwb c2457ff"><?php echo number_format($totalmissingTarget,2); ?></td>
																<td class="t_r fwb c2457ff "><?php echo number_format(($totalopenPO*100)/$totalstartPrice,2); ?></td>
														</tfoot>
												</table>
											<?php } ?>
										</div>
									</div>
								</div>
						</div>
					</div>
				</div>
		</div>
	</div>
</div>
<?php include "include/inc_footer.php"; ?> 
<?php include "include/inc_function_tablesort.php"; ?>
<script type="text/javascript">

	$("#mounthOpt").on('change', function() {
		rs = this.value;
		myarr = rs.split("-");
		if(this.value){
			window.location.href = "report_renews.php?txtse="+myarr[1]+"&year="+myarr[0];
		}

  });
  $("#Department").on('change', function() {
  	txtse = <?php echo $_GET["txtse"]; ?>;
  	year = <?php echo $_GET["year"]; ?>;
		if(this.value){
			window.location.href = "report_renews.php?txtse="+txtse+"&year="+year+"&depar="+this.value;
		}else{
			window.location.href = "report_renews.php?txtse="+txtse+"&year="+year;
		}

  });

// <?php if($_GET["txtse"]){?>
// function getDetailPO(user, date){
// 	txtse = <?php echo $_GET["txtse"]; ?>;
// 	if(txtse){
// 		window.location.href = "report_renews.php?txtse="+txtse+"&user="+user+"&date="+date;
// 	}
// }
// <?php } ?>
  

function filterPOcode(){
	txtse = $("#txtse").val();
	if(txtse){
		window.location.href = "counters.php?txtse="+txtse;
	}else{
		alert("กรุณากรอก ข้อมูล เพื่อค้นหา");
	}
}

function random_rgba() {
    var o = Math.round, r = Math.random, s = 255;
    return 'rgba(' + o(r()*s) + ',' + o(r()*s) + ',' + o(r()*s) + ',' + r().toFixed(2) + ')';
}


</script>



