<?php 

session_start();
if(!isset($_SESSION["User"]['UserCode'])){
	echo "<script>alert('กรุณาเข้าสู่ระบบ!!!');</script>";
	echo '<META http-equiv="refresh" content="0;URL=login.php">';
	exit();
} 
if($_SESSION["User"]['type'] != "Sale" && $_SESSION["User"]['type'] != "SuperAdmin"){
	echo '<META http-equiv="refresh" content="0;URL=index.php">';
	exit();
}
header('Content-Type: text/html; charset=utf-8');
include "include/inc_header.php"; 
include "include/inc_menu.php"; 
include "include/inc_function_offer.php"; 

$UserCode = $_SESSION["User"]['UserCode'];
$getMyUserAll = getMyUserAll();
$getInsuranceGroup = getInsuranceGroup();
$getTitle = getTitle();
$getProvinceAll = getProvinceAll();
$getReferralType = getReferralType();
// echo "<pre>".print_r($getMyUserAll,1)."</pre>";
?> 
<div class="main">
	<div class="main-content p20">
		<div class=" bgff"> 
			<div class="p20">	
				<div class="row">
					<div class="col-md-4" style="background-color: #fffce6;padding: 20px 15px;">
						<div class="t_c cff2da5"><h4>สร้างข้อเสนอประกันภัย</h4> </div>
						<form action="include/inc_action_offer.php" method="post" name="frmAddCP" id="frmAddCP">
							<input type="hidden" name="action" value="addCP">
							<div class="row">
								<div class="col-md-10">
									<label for="CPDate">วันที่ทำข้อเสนอ</label>
									<input type="date" value="<?php echo date("Y-m-d"); ?>" class="form-control formInput2" name="CPDate" required>
								</div>
							</div>
							<div class="row mt15">
								<div class="col-md-10">
									<label for="POID">จากเลขที่ใบสั่งซื้อ</label>
									<input type="text" class="form-control formInput2" name="POID">
								</div>
							</div>
							<div class="row mt15">
								<div class="col-md-10">
									<label for="CustomerFilter">ลูกค้า</label>
									<input type="text" class="form-control formInput2" name="CustomerFilter" id="CustomerFilter">
								</div>
								<div class="col-md-1">
									<span class="cursorPoin fs20"  onclick="filterCustomer()"><i class="fa fa-search-plus mt30 c2457ff"></i></span>
								</div>
							</div>
							<div class="row mt15">
								<div class="col-md-10">
									<label for="CustomerID">เลือก</label>
									<select name="CustomerID" id="CustomerID" class="form-control formInput2" onchange="selectCus()" required>
										<option value="">:: กรุณาเลือก ::</option>
									</select>
								</div>
								<div class="col-md-1">
									<span class="cursorPoin fs20" onclick="ShowAddCus(1)"><i class="fa fa-user-plus mt30 c2457ff"></i></span>
								</div>
							</div>
							<div class="row mt15">
								<div class="col-md-10">
									<label for="EmployeeID">พนักงาน</label>
									<select name="EmployeeID" id="EmployeeID" class="form-control formInput2" required>
										<option value="">:: กรุณาเลือก ::</option>
										<?php foreach ($getMyUserAll as $key => $value) { ?>
												<option value="<?php echo  $value["User_ID"] ?>" <?php if($UserCode == $value["User_ID"]){ echo "selected"; } ?>><?php echo  $value["User_ID"]." ".$value["User_FName"]." ".$value["User_LName"]; ?></option>
										<?php } ?>
									</select>
								</div>
							</div>
							<div class="row mt15">
								<div class="col-md-10">
									<label for="InsuranceGroupID">กลุ่มประเภทประกัน</label>
									<select name="InsuranceGroupID" id="InsuranceGroupID" class="form-control formInput2">
										<option value="">:: กรุณาเลือก ::</option>
										<?php foreach ($getInsuranceGroup as $key => $value) { ?>
												<option value="<?php echo  $value["Insurance_Group_ID"] ?>" <?php if($value["Insurance_Group_ID"] == "MT"){ echo "selected"; } ?> >
													<?php echo  $value["Insurance_Group_Initials"]." ".$value["Insurance_Group_Detail"]; ?>
												</option>
										<?php } ?>
									</select>
								</div>
							</div>
							<div class="row mt15">
								<div class="col-md-5">
									<label for="CoverageStartDate">เริ่มคุ้มครอง</label>
									<input type="date" class="form-control formInput2" name="CoverageStartDate" value="<?php echo date("Y-m-d"); ?>">
								</div>
								<div class="col-md-5">
									<label for="CoverageEndDate">สิ้นความคุ้มครอง</label>
									<input type="date" class="form-control formInput2" name="CoverageEndDate" value="<?php echo date('Y-m-d', strtotime('+1 year')); ?>" >
								</div>
							</div>
							<div class="row mt15 t_c">
								<button type="reset" class="btn btn-info" >ยกเลิก</button>
								<button type="submit" class="btn btn-success" >บันทึก</button>
							</div> 
						</form>
					</div>
					<div class="col-md-6 col-md-offset-1 dn" id="BoxAddCustomer" style="background-color: #f1fffe;padding: 20px 15px;">
						<div class="t_c cff2da5"><h4>สร้างลูกค้าใหม่</h4> </div>
						<form action="" method="post" name="frmAddCustomer" id="frmAddCustomer">
							<div class="row">
								<div class="col-md-2">
									<label for="Customer_Type" class="c2457ff">ประเภทลูกค้า</label><br>
									<input type="radio" name="Customer_Type" value="P" > ส่วนบุคคล <br>
									<input type="radio" name="Customer_Type" value="C" > บริษัท
								</div>
								<div class="col-md-4">
									<label for="Customer_Title" class="c2457ff">ชื่อลูกค้า</label>
									<select name="Customer_Title" id="Customer_Title" class="form-control formInput2">
										<option value="">:: กรุณาเลือก ::</option>
										<?php foreach ($getTitle as $key => $value) { ?>
												<option value="<?php echo  $value["Title_ID"] ?>"><?php echo  $value["Title_Name"]; ?></option>
										<?php } ?>
									</select>
								</div>
								<div class="col-md-3">
									<label for="Customer_FName" class="c2457ff">ชื่อ</label>
									<input type="text" name="Customer_FName" class="form-control formInput2" required>
								</div>
								<div class="col-md-3">
									<label for="Customer_LName" class="c2457ff">นามสกุล</label>
									<input type="text" name="Customer_LName" class="form-control formInput2" required>
								</div>
							</div>
							<div class="row mt15">
								<div class="col-md-6">
									<label for="CustomerFolder" class="c2457ff">เลขบัตรประชาชน/เลขทะเบียนผู้ประกอบการ</label>
									<input type="text" name="CustomerFolder" class="form-control formInput2" >
								</div>
								<div class="col-md-6">
									<label for="ContactPerson" class="c2457ff">ชื่อผู้ติดต่อ</label>
									<input type="text" name="ContactPerson" class="form-control formInput2" >
								</div>
							</div>
							<div class="row mt15">
								<div class="col-md-3">
									<label for="TelNo" class="c2457ff">โทรศัพท์</label>
									<input type="text" name="TelNo" class="form-control formInput2" required onkeyup="chknum(this,event)">
								</div>
								<div class="col-md-3">
									<label for="Fax" class="c2457ff">โทรสาร</label>
									<input type="text" name="Fax" class="form-control formInput2" onkeyup="chknum(this,event)">
								</div>
								<div class="col-md-3">
									<label for="Mobile_No" class="c2457ff">มือถือ</label>
									<input type="text" name="Mobile_No" class="form-control formInput2" onkeyup="chknum(this,event)">
								</div>
								<div class="col-md-3">
									<label for="EMail" class="c2457ff">eMail</label>
									<input type="text" name="EMail" class="form-control formInput2" >
								</div>
							</div>
							<div class="row mt15">
								<div class="col-md-3">
									<label for="Mobile_I" class="c2457ff">Mobile I</label>
									<input type="text" name="Mobile_I" class="form-control formInput2" onkeyup="chknum(this,event)">
								</div>
								<div class="col-md-3">
									<label for="Mobile_II" class="c2457ff">Mobile II</label>
									<input type="text" name="Mobile_II" class="form-control formInput2" onkeyup="chknum(this,event)">
								</div>
								<div class="col-md-3">
									<label for="Land_Line" class="c2457ff">Land Line</label>
									<input type="text" name="Land_Line" class="form-control formInput2" >
								</div>
								<div class="col-md-3">
									<label for="Line_ID" class="c2457ff">Line ID</label>
									<input type="text" name="Line_ID" class="form-control formInput2" >
								</div>
							</div>
							<div class="row mt15">
								<div class="col-md-6">
									<h4 class="c2457ff">ที่อยู่</h4>
									<div class="row">
										<div class="col-md-12">
											<textarea name="Addr1" id="Addr1_1" class="form-control "></textarea>
										</div>
									</div>
									<div class="row mt15">
										<div class="col-md-6">
											<label for="ProvinceID_1" class="c2457ff">จังหวัด</label>
											<select name="ProvinceID" id="ProvinceID_1" class="form-control formInput2 " onchange="getDistrict(1)">
												<option value="">:: กรุณาเลือก ::</option>
												<?php foreach ($getProvinceAll as $key => $value) { ?>
														<option value="<?php echo  $value["Province_ID"] ?>"><?php echo  $value["Province_Name_TH"]; ?></option>
												<?php } ?>
											</select>
										</div>
										<div class="col-md-6">
											<label for="District_ID" class="c2457ff">อำเภอ/เขต</label>
											<select name="District_ID" id="District_ID_1" class="form-control formInput2 " onchange="getSubdistrict(1)">
												<option value="">:: กรุณาเลือก ::</option>
											</select>
										</div>
									</div>
									<div class="row mt15">
										<div class="col-md-6">
											<label for="Subdistrict_ID" class="c2457ff" >ตำบล/แขวง</label>
											<select name="Subdistrict_ID" id="Subdistrict_ID_1" class="form-control formInput2 " onchange="getPostCode(1)">
												<option value="">:: กรุณาเลือก ::</option>
											</select>
										</div>
										<div class="col-md-6">
											<label for="Post_Code" class="c2457ff">รหัสไปรษณีย์</label>
											<input type="text" name="Post_Code" id="Post_Code_1"  class="form-control formInput2 " >
										</div>
									</div>
								</div>
								<div class="col-md-6" style="border-left: 1px solid #9d9d9d;">
									<h4 class="fl c2457ff">ที่อยู่ในการส่งเอกสาร</h4>
									<span class="cursorPoin fs12 mt10 ml10 dib c3ca512" onclick="copyAdd();"><i class="fa fa-copy "></i> คัดลอกที่อยู่</span>
									<div class="row">
										<div class="col-md-12">
											<label for="Receiver_Name" class="c2457ff">ชื่อผู้รับ</label>
											<input type="text" name="Receiver_Name" id="Receiver_Name"  class="form-control formInput2" >
										</div>
									</div>
									<div class="row mt15">
										<div class="col-md-12">
											<textarea name="Addr1_2" id="Addr1_2" class="form-control"></textarea>
										</div>
									</div>
									<div class="row mt15">
										<div class="col-md-6">
											<label for="ProvinceID_2" class="c2457ff">จังหวัด</label>
											<select name="ProvinceID_2" id="ProvinceID_2" class="form-control formInput2" onchange="getDistrict(2)">
												<option value="">:: กรุณาเลือก ::</option>
												<?php foreach ($getProvinceAll as $key => $value) { ?>
														<option value="<?php echo  $value["Province_ID"] ?>"><?php echo  $value["Province_Name_TH"]; ?></option>
												<?php } ?>
											</select>
										</div>
										<div class="col-md-6">
											<label for="District_ID_2" class="c2457ff">อำเภอ/เขต</label>
											<select name="District_ID_2" id="District_ID_2" class="form-control formInput2" onchange="getSubdistrict(2)">
												<option value="">:: กรุณาเลือก ::</option>
											</select>
										</div>
									</div>
									<div class="row mt15">
										<div class="col-md-6">
											<label for="Subdistrict_ID_2" class="c2457ff" >ตำบล/แขวง</label>
											<select name="Subdistrict_ID_2" id="Subdistrict_ID_2" class="form-control formInput2" onchange="getPostCode(2)">
												<option value="">:: กรุณาเลือก ::</option>
											</select>
										</div>
										<div class="col-md-6">
											<label for="Post_Code_2" class="c2457ff">รหัสไปรษณีย์</label>
											<input type="text" name="Post_Code_2" id="Post_Code_2"  class="form-control formInput2" >
										</div>
									</div>
								</div>
							</div>
							<div class="row mt15">
								<div class="col-md-4">
									<label for="ReferralTypeID" class="c2457ff">การอ้างอิง</label>
									<select name="ReferralTypeID" id="ReferralTypeID" class="form-control formInput2">
										<option value="">:: กรุณาเลือก ::</option>
										<?php foreach ($getReferralType as $key => $value) { ?>
												<option value="<?php echo  $value["Referral_Type_ID"] ?>" <?php if($value["Referral_Type_ID"] == "IN3"){echo "selected";} ?> >
													<?php echo  $value["Referral_Type"]; ?>
												</option>
										<?php } ?>
									</select>
								</div>
								<div class="col-md-4">
									<label for="ReferralDetail" class="c2457ff">รายละเอียดการอ้างอิง</label>
									<input type="text" name="ReferralDetail" id="ReferralDetail"  class="form-control formInput2" >
								</div>
								<div class="col-md-4">
									<label for="UserID" class="c2457ff">พนักงาน</label>
									<select name="UserID" id="UserID" class="form-control formInput2">
										<option value="">:: กรุณาเลือก ::</option>
										<?php foreach ($getMyUserAll as $key => $value) { ?>
												<option value="<?php echo  $value["User_ID"] ?>" <?php if($UserCode == $value["User_ID"]){echo "selected";} ?>>
													<?php echo  $value["User_ID"]." ".$value["User_FName"]." ".$value["User_LName"]; ?>
												</option>
										<?php } ?>
									</select>
								</div>
							</div>
							<div class="row mt15 t_c">
								<button type="button" class="btn btn-info" onclick="ShowAddCus(0)">ยกเลิก</button>
								<button type="button" class="btn btn-success" onclick="frmAddCus()">บันทึก</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	.
</div>


<?php include "include/inc_footer.php"; ?> 
<!-- <script src="//code.jquery.com/jquery-3.2.1.min.js"></script> -->
<!-- <script src="fancybox/dist/jquery.fancybox.min.js"></script> -->
<script type="text/javascript">
function chknum(ip,ek) {
		if(!isNaN(ip.value)){
			return true;
		}
		ip.value="";
		alert('กรุณากรอกเป็นหมายเลขเท่านั้น');
		return true;
	}

function ShowAddCus(rs){
	console.log(rs);
	if(rs == 1){
		$("#BoxAddCustomer").slideDown();
	}else{
		$("#BoxAddCustomer").slideUp();
	}
}

function filterCustomer(){
	val = $("#CustomerFilter").val();
	// console.log(val);
	if(val){
		$.ajax({ 
			url: 'include/inc_action_offer.php',
			type:'POST',
			dataType: 'json',
			data: {action:"CustomerFilter", val:val},
			success:function(rs){
				// console.log(rs);
				$("#CustomerID").empty();
				$("#CustomerID").append('<option value="0">:: กรุณาเลือก ::</option>');
				$.each( rs, function( key, val ) {
			    $("#CustomerID").append('<option value="'+val.Customer_ID+'">'+val.Customer_ID+' '+val.Customer_FName+' '+val.Customer_LName+'</option>');
			  });
			}
		});
	}else{
		alert("กรุณาใส่ข้อมูลที่ต้องการค้นหา");
	}
}

function selectCus(){
	cusid = $("#CustomerID").val();console.log(cusid);
	$.ajax({ 
		url: 'include/inc_action_offer.php',
		type:'POST',
		data: {action:"CustomerRegistration", cusid:cusid},
		success:function(rs){
			// console.log(rs);
			$("#EmployeeID").val(rs);
		}
	});
}

function getDistrict(k){ 
	ProvinceID = $("#ProvinceID_"+k).val(); 
	$.ajax({ 
		url: 'include/inc_action_offer.php',
		type:'POST',
		dataType: 'json',
		data: {action:"getDistrict", ProvinceID:ProvinceID},
		success:function(rs){
			$("#District_ID_"+k).empty();
			$("#District_ID_"+k).append('<option value="0">:: กรุณาเลือก ::</option>');
			$.each( rs, function( key, val ) {
		    $("#District_ID_"+k).append('<option value="'+val.District_ID+'">'+val.District_Name_TH+'</option>');
		  });
		}
	});
}

function getSubdistrict(k){
	District_ID = $("#District_ID_"+k).val(); 
	$.ajax({ 
		url: 'include/inc_action_offer.php',
		type:'POST',
		dataType: 'json',
		data: {action:"getSubdistrict", District_ID:District_ID},
		success:function(rs){
			$("#Subdistrict_ID_"+k).empty();
			$("#Subdistrict_ID_"+k).append('<option value="0">:: กรุณาเลือก ::</option>');
			$.each( rs, function( key, val ) {
		    $("#Subdistrict_ID_"+k).append('<option value="'+val.Subdistrict_ID+'">'+val.Subdistrict_Name_TH+'</option>');
		  });
		}
	});
}

function getPostCode(k){
	Subdistrict_ID = $("#Subdistrict_ID_"+k).val(); 
	$.ajax({ 
		url: 'include/inc_action_offer.php',
		type:'POST',
		data: {action:"getPostCode", Subdistrict_ID:Subdistrict_ID},
		success:function(rs){
			$("#Post_Code_"+k).val(rs);
		}
	});
}

function copyAdd(){
	Addr1_1 = $("#Addr1_1").val(); 
	ProvinceID = $("#ProvinceID_1").val(); 
	District_ID = $("#District_ID_1").val();
	Subdistrict_ID = $("#Subdistrict_ID_1").val();
	Post_Code = $("#Post_Code_1").val();

	$("#Addr1_2").val($("#Addr1_1").val()); 
	$("#ProvinceID_2").val($("#ProvinceID_1").val());
	$("#Post_Code_2").val($("#Post_Code_1").val());

	$.ajax({ 
		url: 'include/inc_action_offer.php',
		type:'POST',
		dataType: 'json',
		data: {action:"getDistrict", ProvinceID:ProvinceID},
		success:function(rs){
			$("#District_ID_2").empty();
			$("#District_ID_2").append('<option value="0">:: กรุณาเลือก ::</option>');
			$.each( rs, function( key, val ) {
				if(val.District_ID == District_ID){
		    	$("#District_ID_2").append('<option value="'+val.District_ID+'" selected>'+val.District_Name_TH+'</option>');
		  	}else{
		  		$("#District_ID_2").append('<option value="'+val.District_ID+'">'+val.District_Name_TH+'</option>');
		  	}
		  });
		}
	});

	$.ajax({ 
		url: 'include/inc_action_offer.php',
		type:'POST',
		dataType: 'json',
		data: {action:"getSubdistrict", District_ID:District_ID},
		success:function(rs){
			$("#Subdistrict_ID_2").empty();
			$("#Subdistrict_ID_2").append('<option value="0">:: กรุณาเลือก ::</option>');
			$.each( rs, function( key, val ) { 
		    if(val.Subdistrict_ID == Subdistrict_ID){
		    	$("#Subdistrict_ID_2").append('<option value="'+val.Subdistrict_ID+'" selected>'+val.Subdistrict_Name_TH+'</option>');
		  	}else{
		  		$("#Subdistrict_ID_2").append('<option value="'+val.Subdistrict_ID+'">'+val.Subdistrict_Name_TH+'</option>');
		  	}
		  });
		}
	});
}

function frmAddCus(){
	var Customer_Type = $('[name="Customer_Type"]:checked').val();
	var Customer_FName = $('[name="Customer_FName"]').val();
	var Customer_LName = $('[name="Customer_LName"]').val();
	var TelNo = $('[name="TelNo"]').val();
	// console.log($("#frmAddCustomer").serializeArray());
	if(!Customer_Type){
		alert("กรุณาเลือกประเภทลูกค้า");
		$('[name="Customer_Type"]').focus();
	}else if(!Customer_FName){
		alert("กรุณาเลือกชื่อลูกค้า");
		$('[name="Customer_FName"]').focus();
	}else if(!Customer_LName){
		alert("กรุณาเลือกนามสกุล");
		$('[name="Customer_LName"]').focus();
	}else if(!TelNo){
		alert("กรุณาเบอร์โทรศัพท์");
		$('[name="TelNo"]').focus();
	}else{
		$.ajax({ 
			url: 'include/inc_action_offer.php',
			type:'POST',
			dataType: 'json',
			data: {action:"addCustomer", frm:$("#frmAddCustomer").serializeArray()},
			success:function(rs){
				// console.log(rs);
				// console.log(rs.UserCode);
				alert("เพิ่มข้อมูลคุณ"+rs.Customer_FName+' '+rs.Customer_LName+"เรียบร้อย ");
				$("#CustomerFilter").val(rs.Customer_ID);
				$("#CustomerID").append('<option value="'+rs.Customer_ID+'" selected>'+rs.Customer_FName+' '+rs.Customer_LName+'</option>');
				$("#EmployeeID").val(rs.UserCode);
				$("#BoxAddCustomer").slideUp();
			}
		});
	 }
}
</script>