<?php 

session_start();
if(!isset($_SESSION["User"]['UserCode'])){
	echo "<script>alert('กรุณาเข้าสู่ระบบ!!!');</script>";
	echo '<META http-equiv="refresh" content="0;URL=login.php">';
	exit();
} 
if($_SESSION["User"]['type'] != "Admin" && $_SESSION["User"]['type'] != "SuperAdmin"){
	echo '<META http-equiv="refresh" content="0;URL=chkinser.php">';
	exit();
}
header('Content-Type: text/html; charset=utf-8');
include "include/inc_header.php"; 
include "include/inc_menu.php";
include "insfollow/inc_function_insfollow.php"; 


$getRowPO = getRowPOByid($_GET["poid"]);
$getEmailInsur = getEmailInsur($getRowPO["Insurer_Initials"]);
$getPersonnelCode = getPersonnelCode($getRowPO["Employee_ID"]);
if($getPersonnelCode["personnel_supervisor"]){
	$getSupervisor = getSupervisor($getPersonnelCode["personnel_supervisor"]);
}
// echo "<pre>".print_r($getPersonnelCode,1)."</pre>";

$subject = "แจ้งยกเลิกกรมธรรม์ ".$getRowPO["Insurer_Initials"] ." | คุณ ".$getRowPO["Customer_FName"]." ".$getRowPO["Customer_LName"]." | ทะเบียน ".$getRowPO["Plate_No"]." ".$getRowPO["Province_Name_TH"];
?>
<div class="main">
	<div class="p20">
		<div class="bgff row"> 
				<div class="p20">
					<h4>อีเมล์ถึงบริษัทประกัน <a href="export_request.php?pocode=<?php echo $pocode; ?>&amp;case=D" target="_bank" class="c2457ff fwb"><?php echo $pocode; ?></a><span class=""></span></h4>
					<form action="insfollow/inc_action_insfollow.php" enctype="multipart/form-data" method="post" id="frmSendMail">
							<input type="hidden" value="sendMailInsurCancel" name="action">
							<input type="hidden" value="<?php echo $_GET["key"]; ?>" name="Installment_Followup_id">
							<input type="hidden" value="<?php echo $_GET["poid"]; ?>" name="po_code">
							<div class="col-md-6">
								<div class=" mt10">
									<div class="form-group">
									  <label for="subject">หัวข้อ <span class="cf80000"> *</span></label>
									  <input type="text" name="subject" class="form-control formInput2" value="<?php echo $subject ?>">
									</div>
								</div>
								<div class="mt20 clearfix">
									<div class="form-group">
									  <div for="subject">อีเมล์ <span class="cf80000"> *</span> </div>
									  <?php if($getPersonnelCode["personnel_supervisor"]){?>
									  	<div class="col-md-6">
									  		<input type="checkbox" name="mailsend[]" value="<?php echo $getSupervisor["personnel_email"]."| Supervisor | CC";?>" checked>
									  		<span class="ml5"><?php echo $getSupervisor["personnel_email"]." | Supervisor" ;?></span>
									  	</div>
									  <?php } ?>
									  	<div class="col-md-6">
									  		<input type="checkbox" name="mailsend[]" value="<?php echo $_SESSION["User"]['email']."|".$_SESSION["User"]['firstname']."  | CC" ;?>" checked>
									  		<span class="ml5"><?php echo $_SESSION["User"]['email']." | Admin" ;?></span>
									  	</div>
									  	<div class="col-md-6">
									  		<input type="checkbox" name="mailsend[]" value="anchalee.i@asiadirect.co.th|อัญชลี|CC" checked>
									  		<span class="ml5">anchalee.i@asiadirect.co.th | Admin</span>
									  	</div>
									  	<div class="col-md-6">
									  		<input type="checkbox" name="mailsend[]" value="tanawut.j@asiadirect.co.th|ธนาวุฒิ|CC" checked>
									  		<span class="ml5">tanawut.j@asiadirect.co.th | Admin</span>
									  	</div>
									  	<div class="col-md-6">
									  		<input type="checkbox" name="mailsend[]" value="jintana.y@asiadirect.co.th|จินตนา|CC" checked>
									  		<span class="ml5">jintana.y@asiadirect.co.th | Accounting</span>
									  	</div>
									  	<div class="col-md-6">
									  		<input type="checkbox" name="mailsend[]" value="adb2070@asiadirect.co.th|พิพัฒน์|CC" checked>
									  		<span class="ml5">adb2070@asiadirect.co.th | Accounting</span>
									  	</div>
									  <?php foreach ($getEmailInsur as $key => $value) { 
									  		$typeSend = ($value["setting_cc"] == 1) ? "TO" : "CC";
									  	?>
									  	<div class="col-md-6">
									  		<input type="checkbox" name="mailsend[]" value="<?php echo $value["email"]."|".$value["name"]." | ".$typeSend ;?>" checked>
									  		<span class="ml5"><?php echo $value["email"]." | ".$value["name"] ;?></span>
									  	</div>
									  <?php } ?>
									  <div id="addEmail" class="clearfix"></div>
									  <div class="mt15 cursorPoin fwb cf40053 clearfix dib btn " onclick="fnaddEmail()">เพิ่มอีเมล์</div> 
									</div>
								</div>
								<div class="mt20 clearfix">
									<div class="form-group">
									  <label for="bodymail">รายละเอียด <span class="cf80000"> *</span></label>
									 	<div>
						        	<textarea name="bodymail" id="editorFollow">
											  <strong>เรียน เจ้าหน้าที่ <?php echo $getRowPO["Insurer_Name"] ?></strong>
											  <br><strong> เรื่อง แจ้งดำเนินการยกเลิกกรมธรรม์</strong>
											  <hr />
											  <strong>กรมธรรม์เลขที่ : </strong><?php echo $getRowPO["Policy_No"] ? $getRowPO["Policy_No"] : $getRowPO["Compulsory_No"]  ?>
											  <br><strong>ลูกค้า : </strong><?php echo $getRowPO["Customer_FName"]." ".$getRowPO["Customer_LName"] ?>
											  <br><strong>ทะเบียน : </strong><?php echo $getRowPO["Plate_No"]." ".$getRowPO["Province_Name_TH"];?>
											  <br><strong>เริ่มความคุ้มครอง : </strong><?php echo $getRowPO["Coverage_Start_Date"]->format('d/m/Y');?>
											  <br><strong>สิ้นความคุ้มครอง : </strong><?php echo $getRowPO["Coverage_End_Date"]->format('d/m/Y');?>
											  <br><strong>หมายเหตุ : </strong><?php echo $getRowPO["PO_ID"] ?>
											  <hr />
											  <?php if($_GET["ins"] == 1){?> 
											 		<h3><span class="marker"><big><strong>** ยกเลิกเนื่องจาก ลูกค้าไม่ชำระเบี้ยประกัน **</font></strong></big></h3>
											  <?php }else{ ?>
											  	<h3><span class="marker"><big><strong>** ยกเลิกเนื่องจาก ลูกค้าไม่ชำระเบี้ยประกัน รบกวนขอทำเรื่องคืนเงินทางเอเชียไดเร็ค ขออนุโลมคิดแบบ  PRORATA ***</font></strong></big></h3>
											  <?php } ?>
											  <hr />
											  	<strong><?php echo $_SESSION["User"]['firstname']." ".$_SESSION["User"]['lastname'] ?> | Sales Admin</strong>
													<br>บริษัท เอเชียไดเร็ค อินชัวรันส์ โบรคเกอร์ จำกัด
													<br>626 อาคารบีบีดี (พระราม4) ชั้น&nbsp;11 ถนนพระรามที่ 4 แขวงมหาพฤฒาราม เขตบางรัก กรุงเทพฯ 10500
													<br>อีเมล์: <?php echo $_SESSION["User"]['email'] ?> | โทร: <?php echo $_SESSION["User"]['phone'] ?> | แฟ๊กซ์:0-2089-2088
													<br>เวปไซต์: <a href="www.asiadirect.co.th" target="_blank">www.asiadirect.co.th</a> | <a href="www.facebook.com/AsiaDirectBroker" target="_blank">www.facebook.com/AsiaDirectBroker</a>
											</textarea>
									 </div>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class=" mt10">
									<div class="form-group clearfix">
									  <div for="subject">เอกสารประกอบ <span class="cf80000"> *</span></div>
								  <div class="col-md-6"><input type="file" name="files[]" class="form-control "></div>
								  <div class="col-md-6"><input type="file" name="files[]" class="form-control "></div>
								  <div class="col-md-6"><input type="file" name="files[]" class="form-control "></div>
								  <div class="col-md-6"><input type="file" name="files[]" class="form-control "></div>
									<div id="addFile" class="clearfix"></div>
									<div class="clearfix mt15 cursorPoin fwb cf40053 clearfix dib btn" onclick="fnaddfile()">เพิ่มเอกสารประกอบ</div> 
									<div class="c00ac0a mt15 fs12">***ชื่อไฟล์ภาษาอังกฤษเท่านั้น***</div>
								  </div>
								</div>
								<hr>
								<div class="fs18 t_c c00ac0a dn" id="txtWait"> ... กรุณารอ ...</div>
								<div class="t_c clearb mt50"><input type="button" value="ส่งอีเมล์" class="btn btn-success" id="btnSubmit"></div>
							</div>
					</form>
				</div>
			
		</div>
	</div>
</div>

<?php include "include/inc_footer.php"; ?> 
<script>
  CKEDITOR.replace( 'editorFollow', {
    language: 'th',
    height: 400
	});
</script>
<script type="text/javascript">
	$( "#btnSubmit" ).click(function() {
		$("#txtWait").show();
		$("#btnSubmit").hide();
  	$( "#frmSendMail" ).submit();
	});
	function fnaddEmail(){
		
		$("#addEmail").append('<div class="col-md-6"><input type="text" name="mailsend[]" class="form-control formInput2"></div>');
	}
	
	function fnaddfile(){
		$("#addFile").append('<div class="col-md-6"><input type="file" name="files[]" class="form-control "></div>');
	}
</script>