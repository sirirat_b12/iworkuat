<?php include "include/inc_header.php"; ?> 
	<!-- WRAPPER -->
	<div id="wrapper">
		<div class="vertical-align-wrap">
			<div class="vertical-align-middle">
				<div class="loginBox">
					<div class="left">
						<div class="content">
							<div class="header text-center">
								<div class="logo"><img src="img/logo_110.png" style="width: 100px;"></div>
								<p class="mt20 lead fwb">กรุณาเข้าสู่ระบบ</p>
							</div>
							<form class="form-auth-small" action="include/inc_action.php" method="post">
								<input type="hidden" name="actions" value="login">
								<div class="form-group">
									<label for="personnel_code" class="control-label sr-only">รหัสพนักงาน</label>
									<input type="personnel_code" class="form-control" name="personnel_code" placeholder="รหัสพนักงาน" required>
								</div>
								<div class="form-group">
									<label for="password" class="control-label sr-only">รหัสผ่าน</label>
									<input type="password" class="form-control" name="password" placeholder="รหัสผ่าน" required>
								</div>
								<button type="submit" class="btn btn-primary btn-lg btn-block">เข้าสู่ระบบ</button>
								<!-- <div class="bottom">
									<span class="helper-text"><i class="fa fa-lock"></i> <a href="#">Forgot password?</a></span>
								</div> -->
							</form>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>
	<!-- END WRAPPER -->
<?php include "include/inc_footer.php"; ?> 