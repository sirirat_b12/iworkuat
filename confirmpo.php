<?php


session_start();
ini_set('max_execution_time', 186);
ini_set('memory_limit', '2048M');
$UserCode = $_SESSION["User"]['UserCode'];
$userType = $_SESSION["User"]['type'];

if(!isset($UserCode)){
	echo "<script>alert('กรุณาเข้าสู่ระบบ!!!');</script>";
	echo '<META http-equiv="refresh" content="0;URL=login.php">';
	exit();
}

header('Content-Type: text/html; charset=utf-8');
include "include/inc_header.php"; 
include "include/inc_menu.php"; 

$pocode = $_GET["po"];
if($pocode){
	$getPO = getPurchaseBycode($pocode);
	$Cardetail = getCardetailBycode($pocode);
	// $getCommentlist = getCommentlist();
	$getIR = getInsuranceRequest($pocode);
	$getCoverageItem = getCoverageItem($pocode);
	$getPolicyDelivery = getPolicyDelivery($pocode);
	$getInstallment = getInstallmentlist($pocode);
	$addressPOsend =  $getPO["Receiver_Name"]."<br>".$getPO["posendAddr1"]."<br><b>ตำบล/แขวง </b>".getSubdistrictBycode($getPO["posendSubdistrict"])."<b> อำเภอ/เขต </b>".getDistrictBycode($getPO["posendDistrict"])."<br><b> จังหวัด </b>".getProvinceBycode($getPO["posendProvince"])." <b>รหัสไปรษณีย์</b> ".$getPO["posendPost_Code"];
}
// echo "<pre>".print_r($Cardetail,1)."</pre>";
?>

<div class="main">
	<div class="main-content p20">
		<div class="bgff"> 
			<div class="row p20">
				<div class="col-md-6 bgfffbd8 p15">
					<div class="col-md-4" class="text-r">
						<div class="form-group">
							<input class="form-control formInput2" type="text" name="po_code_search" id="po_code_search" placeholder="เลข PO" >
						</div>
					</div>
					<div class="col-md-1 text-r">
						<div class="form-group" class="text-c">
							<button type="button" class="btn btn-success" onclick="filterPOcode()"><i class="fa fa-search-plus" aria-hidden="true"></i>  ค้นข้อมูล</button>
						</div>
					</div>
				</div>
			</div>
			<?php if($getPO){?> 
				<div class="row p30">
					<div class="mb10 ml15 fs14  fwb">
						<div class="cff2da5 mt1">เรียน <?php echo "คุณ ".$getPO["Customer_FName"]." ".$getPO["Customer_LName"];?></div> 
						<div class="mt10 ml15">บริษัท เอเชียไดเร็ค อินชัวรันส์ โบรคเกอร์ จำกัด ขอแจ้งรายละเอียดในการตกลงทำประกันภัย และรบกวนตรวจสอบ  ข้อมูลรถ วันคุ้มครอง และที่อยู่เพื่อจัดส่งกรมธรรม์ เพื่อความถูกต้อง <br>หากถูกต้องรบกวนแจ้งกลับว่า <b>"ตกลงทำประกัน"</b> รายละเอียดดังต่อไปนี้</div>
					</div>
					<div class="col-md-4">	
						<table class="table fs12">
							<thead>
								<tr>
									<th class="t_r" >หัวข้อ</th>
									<th>ข้อมูล</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td class="t_r"><b>ชื่อ-นามสกุล: </b></td>
									<td class="c2457ff">
										<div><?php echo $getPO["Customer_Folder"] ? $getPO["Customer_Folder"] : "-" ;?>
										<?php echo $getPO["Title_Name"]." ".$getPO["Customer_FName"]." ".$getPO["Customer_LName"];?></div> 
									</td>
								</tr>
								<tr>
									<td class="t_r"><b>การติดต่อ : </b></td>
									<td class="c2457ff">
										<?php echo $getPO["CustomerTel"] ? $getPO["CustomerTel"] : "-" ;?> <?php echo $getPO["EMail"] ? $getPO["EMail"] : "-" ;?>
									</td>
								</tr>
								<tr >
									<td class="t_r"><b>รายละเอียดรถ : </b></td>
									<td class="c2457ff">
										<?php echo $Cardetail["Make_Name_TH"];?> <?php echo trim($Cardetail["Model_Desc"]);?> <?php echo $Cardetail["Sub_Model"] ? $Cardetail["Sub_Model"] : "-" ;?> |
										<?php echo $Cardetail["Plate_No"]." ".getProvinceBycode($Cardetail["Plate_Province"]);?> | 
										<?php echo $Cardetail["Frame_No"] ? $Cardetail["Frame_No"] : "-" ;?>
									</td>
								</tr>
								<tr>
									<td class="t_r"><b>รายละเอียดประกัน : </b></td>
									<td class="c2457ff">
										<div>
											<b> ประกัน : </b><?php echo $getPO["Insurance_Name"];?> 
											<?php if($getPO["Insurance_Name"] != "พรบ."){?>
												| <?php echo ($Cardetail["Repair_Type"] == "C") ? "ซ่อมห้าง" : "ซ่อมอู่" ;?>
											<?php } ?>
											</div>
										<div><b> บริษัท : </b><?php echo $getPO["Insurer_Name"];?></div>
										<div><b> แพจเกต : </b><?php echo $getPO["Insurance_Package_Name"] ? $getPO["Insurance_Package_Name"] : "-";?></div>
										<div>
											<b> คุ้มครองในวันที่ : </b><?php echo $getPO["Coverage_Start_Date"]->format('d/m/Y');?> ถึง <?php echo $getPO["Coverage_End_Date"]->format('d/m/Y');?>
											|  <b> ทุนประกัน : </b><?php echo number_format($getPO["insuere_cost"],2);?>
											|  <b> เบี้ยรวมภาษี : </b><?php echo number_format($getPO["Premium_After_Disc"],2);?>
										</div>
									</td>
								</tr>
								<?php if($getCoverageItem){?>
								<tr>
									<td class="t_r"><b>คุ้มครอง</b></td>
									<td>
										<?php 
											$contxt = "";
											foreach ($getCoverageItem as $key => $value) {
											$covID = $value["Coverage_Item_ID"]; 
											$Coverage[$covID] = $value["Coverage_Value"] ;
											
											$contxt .=	"<div><span>".$value["Coverage_Item_Desc"]."</span>";
												if($value["Coverage_Value"]){ 
													$contxt .=	" <span><b> ".$value["Coverage_Value"]."</span> ".$value["Coverage_Item_Unit"]."</b>";
												} 
											$contxt .= "</div>";
										} 
										echo $contxt ; 
										?>
										<input type="hidden" name="check[insurCoverageItem][data]" value="<?php echo  $contxt; ?>">
									</td>
								</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
					<div class="col-md-5">	
						<table class="table fs12">
							<thead>
								<tr>
									<th class="t_r" >หัวข้อ</th>
									<th>ข้อมูล</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td class="t_r"><b>รหัสข้อเสนอ: </b></td>
									<td class="c1641ff">
										<input type="text" id="txtpoid" name="txtpoid" value="<?php echo $getPO["PO_ID"];?>" readonly> 
										<input type="hidden" id="txtAccessoryOld" name="txtpoid" value="<?php echo $Cardetail["Accessory"];;?>" readonly>
									</td>
								</tr>
								<tr>
									<td class="t_r"><b>ผู้ขับขี: </b></td>
									<td class="c1641ff">
										<?php echo $Cardetail["Driver_Name"] ? "ระบุผู้ขับขี่" : "ไม่ระบุผู้ขับขี่";?>
									</td>
								</tr>
								<?php if($Cardetail["Driver_Name"] ){?>
								<tr>
									<?php 
										if($Cardetail["Driver_Name"]){
											$Driver1 = $Cardetail["Receiver"]." ".$Cardetail["Driver_Name"]." <br><b>ว/ด/ป เกิด</b> ".$Cardetail["Driver_DOB"]->format('Y-m-d')." <b>เลขที่ใบขับขี่</b> ".$Cardetail["Driver_License_No"] ; 
										}
									?>
									<td class="t_r"><b>ผู้ขับขี 1 : </b></td>
									<td class="c1641ff">
										<?php echo $Cardetail["Driver_Name"] ? $Driver1 : "-";?>	
									</td>
								</tr>
								<tr>
									<?php 
										if($Cardetail["Driver_Name"]){
											$Driver2 = $Cardetail["Sender"]." ".$Cardetail["Co_Driver_Name"]." <br><b>ว/ด/ป เกิด</b> ".$Cardetail["Co_Driver_DOB"]->format('Y-m-d')." <b>เลขที่ใบขับขี่</b> ".$Cardetail["Co_Driver_License_No"] ;
										}
									?>
									<td class="t_r"><b>ผู้ขับขี 2 : </b></td>
									<td class="c1641ff">
										<?php echo $Cardetail["Co_Driver_Name"] ? $Driver2 : "-";?>
									</td>
								</tr>
								<?php } ?>
								<tr>
									<td class="t_r"><b>ที่อยู่จัดส่ง : </b></td>
									<td>
										<div><?php echo $getPolicyDelivery["Delivery_Type_Desc"];?></div>
										<div><?php echo $addressPOsend ;?></div>
									</td>
								</tr>
								<tr>
									<td class="t_r">
										<b>อุปกรณ์ตกแต่ง : </b>
										<div class="fs20 cursorPoin cf40053 fwb" onclick="updateAccessory()"><i class="fas fa-save"></i></div>
									</td>
									<td>
										<textarea name="Accessory" id="txtAccessory" style="margin: 0px;height: 100px;width: 100%;"><?php echo $Cardetail["Accessory"];?></textarea>
									</td>
								</tr>
								<tr>
									<td class="t_r"><b>การชำระเงิน : </b></td>
									<td>
										<?php 
											foreach ($getInstallment as $key => $value) {
												if($value["Installment_Status_ID"] == "001"){ 
													$total += $value["ISTM_Total_Amount"];
												}
										?>
											<div class="<?php echo ($value["Installment_Status_ID"] == "009") ? 'c00ac0a' : "cf40053 "; ?>"  >
											งวดที่ <?php echo $value["Installment_ID"];?> 
										    | กำหนด <?php echo $value["Installment_Due_Date"]->format("d/m/Y");?>
											| <b class="fs16"><?php echo number_format($value["ISTM_Total_Amount"],2);?></b> บ.
											| <?php echo ($value["Installment_Status_ID"] == "001") ? "รอชำระ" : "ชำระแล้ว"; ?>
											</div>
										<?php } ?>
										<div class="fwb fs16">ยอดรวมค้างชำระ <?php echo number_format($total,2); ?> บาท</div>
									</td>
								</tr>
								<tr>
									<td class="t_r"><b>เพิ่มเติม : </b></td>
									<td>
										<textarea name="" id="" style="margin: 0px;height: 100px;width: 100%;"></textarea>
									</td>
								</tr>
							</tbody>
						</table>
						
					</div>
				</div>
			<?php } ?>
		</div>
	</div>
</div>

<?php include "include/inc_footer.php"; ?> 
<script type="text/javascript">
	function filterPOcode(){
		pocodeSe = $("#po_code_search").val();
		if(pocodeSe){
			window.location.href = "confirmpo.php?po="+pocodeSe;
		}else{
			alert("กรุณากรอก PO เพื่อค้นหา");
		}
	}

	function updateAccessory(){
		txtAccessory = $("#txtAccessory").val();
		txtpoid = $("#txtpoid").val();
		txtAccessoryOld = $("#txtAccessoryOld").val();
		console.log("txtAccessory "+txtAccessory);
		if( confirm("คุณต้องการแก้ไข อุปกรณ์ตกแต่ง ?")){
		$.ajax({ 
				url: 'include/inc_action_ms.php',
				type:'POST',
				// dataType: 'json',
				data: {action:"txtAccessory", txtAccessory:txtAccessory, txtpoid:txtpoid, txtAccessoryOld:txtAccessoryOld},
				success:function(rs){
					console.log(rs);
					if(rs == 1){
						alert("ทำรายการสำเร็จ");
						window.location.reload(true);
					}
				}
			});
		}
	}
</script>