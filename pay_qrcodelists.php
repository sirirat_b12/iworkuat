<?php 

session_start();
$UserCode = $_SESSION["User"]['UserCode'];
$userType = $_SESSION["User"]['type'];

if(!isset($UserCode)){
	echo "<script>alert('กรุณาเข้าสู่ระบบ!!!');</script>";
	echo '<META http-equiv="refresh" content="0;URL=login.php">';
	exit();
}

header('Content-Type: text/html; charset=utf-8');
include "include/inc_header.php"; 
include "include/inc_menu.php"; 
include "inc_qrcode/inc_function_qr.php";
include('qrcode/qrcode.class.php');

$date = ($_GET["date"]) ? $_GET["date"] : date("Y-m-d");
$dateEnd = ($_GET["dateend"]) ? $_GET["dateend"] : date("Y-m-d");
if($date){
	$getListQRCode = getListQRCodeByDate($date, $_GET["txtse"], $dateEnd);
	
}


// echo "<pre>".print_r($getListQRCode,1)."</pre>";
?>
<div class="main">
	<div class="">
		<div class="p20">
			<div class="row">
				<div class="col-md-12">
					<div class="panel">
							<div class="panel-heading">
								<div class="row mt15">
									<div class="col-md-2 cff2da5">
										<label for="filter" >วันที่</label>
										<input type="date" class=" form-control fs12" id="date" value="<?php echo $date; ?>">
									</div>
									<div class="col-md-2 cff2da5">
										<label for="filter" >ถึงวันที่</label>
										<input type="date" class=" form-control fs12" id="dateEnd" name="dateEnd" value="<?php echo $dateEnd; ?>">
									</div>
									<div class="col-md-2 cff2da5">
										<label for="filter" >REF1 | Customet ID  | RVID | TransactionId</label>
										<input type="text" class="cff2da5 form-control fs12" id="txtse" value="<?php echo trim($_GET["txtse"]); ?>">
									</div>
									<div class="col-md-2 ">
										<span class="btn btn-success mt20" onclick="filterPOcode()">ค้นหา</span>
										<a href="pay_qrcodelists.php" class="btn btn-danger mt20">รีเช็ต</a>
									</div>
									<div class="col-md-1">
										<sapn class="btn btn-info mt20" onclick="btnExport()">Export</sapn>
									</div>
								</div>
							</div>
						<?php if($getListQRCode){ ?>
							<div class="panel-body fs14">
									<table class="table table-bordered" >
										<thead class="fs13 c000000">
											<tr>
												<th class="t_c">สถานะ</th>
												<th class="t_c">Ref</th>
												<th class="t_c">รวมจ่าย</th>
												<th class="t_c">รหัสลูกค้า</th>
												<th class="t_c">PO</th>
												<th class="t_c">ชื่อลูกค้า</th>
												<th class="t_c">ประเภท</th>
												<th class="t_c">งวดที่</th>
												<th class="t_c">วันครบชำระ</th>
												<th class="t_c">จำนวน</th>
												<!-- <th class="t_c">ผู้สร้าง</th> -->
											</tr>
										</thead>
										<tbody class="fs12 c000000">
											<?php 
												$sumtotal = 0;
												foreach ($getListQRCode as $key => $value) { 
													$countDataDesc = count($value["dataDesc"]);
													$stausPay = ($value["status_pay"]== 1) ? "ชำระ" : "รอชำระ" ;
													$sumtotal += $value["sumAmount"];
													// $paycose = $getListCounterAll[0]["paycode"];
											?>
												<tr>
													<td class="t_l" rowspan="<?php echo $countDataDesc;?>">
															<div class="cff8000 fs16 fwb t_c"><?php echo $value["receive_id"]; ?></div>
															<div class="fs10 t_c"><?php echo $value["paymentTime"]->format("d/m/Y H:i:s"); ?></div>
															<div class="fs12"><b>PayCode:</b> <?php echo $value["paymentId"]; ?></div>
															<div class="fs12"><b>ConfirmId:</b> <?php echo $value["confirmId"]; ?></div>
															<div class="fs10"><b>PayCodeTime:</b> <?php echo $value["paymentTime"]->format("d/m/Y H:i:s"); ?></div>
													</td>
													<td class="t_l" rowspan="<?php echo $countDataDesc;?>">
															<div class="cff2da5"><b>Ref.1</b> <?php echo $value["reference_1"]; ?></div>
															<div class="cff2da5"><b>Ref.2</b> <?php echo $value["reference_2"]; ?></div>
															<div class="cff2da5"><b>Transaction</b> <?php echo $value["transactionId"]; ?></div>
															<div><b>เวลาสร้าง: </b><?php echo $value["created_date"]->format("d/m/Y H:i:s"); ?></div>
															<div class="fs10"><b>โดย: </b><?php echo $value["created_by"]; ?></div>
													</td>
													<td class="t_r c9c00c8 fwb" rowspan="<?php echo $countDataDesc;?>"><?php echo number_format($value["sumAmount"],2); ?></td>
													<td class="t_c c2457ff fwb" rowspan="<?php echo $countDataDesc;?>"><?php echo $value["customer_id"]; ?></td>
													<?php foreach ($value["dataDesc"] as $key => $valDesc) { ?>
															<td class="t_c cff2da5 fwb"><?php echo $valDesc["po_id"]; ?></td>
															<td class="t_c "><?php echo $valDesc["customer_name"]; ?></td>
															<td class="t_c ">
																<div><?php echo ($valDesc["insur_type"] == "insur") ? "ประกันภัย" : "พรบ."; ?></div>
																<div><?php echo $valDesc["Insurer_Initials"]; ?></div>
															</td>
															<td class="t_c "><?php echo $valDesc["installment_num"]; ?></td>
															<td class="t_c "><?php echo $valDesc["installment_due_date"]->format("d/m/Y"); ?></td>
															<td class="t_r cff2da5 fwb"><?php echo number_format($valDesc["amount"],2); ?></td>
														</tr>
													<?php } ?>
												</tr>
												<?php } ?>
												<tr>
														<th class="t_c fwb fs18 bgbaf4bc cff8000 " >รวมจ่าย</th>
														<th class="t_c fwb fs18 bgbaf4bc cff8000 " ><?php echo count($getListQRCode)?></th>
														<th class="t_c fwb fs18 bgbaf4bc cff8000 " colspan="8"><?php echo number_format($sumtotal,2); ?></th>
												</tr>
										</tbody>
								</table>
							</div>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</div> 
</div>
<?php include "include/inc_footer.php"; ?> 
<script src="fancybox/dist/jquery.fancybox.min.js"></script>
<script type="text/javascript">

	function btnsubmit(){
		var countCK = $('input[name="inskey[]"]:checked').length;
  	if(countCK > 0){
  		if( confirm("คุณต้องการสร้างการชำระเงินผ่านระบบ Qr Code?")){
  			$("#btnsub, #btncancel").hide();
  			$("#Waittxt").show();
				$("#frmListPay").submit();
  		}
  	}else{
  		alert("กรุณาเลือกงวดที่ชำระ");
  		$("#btnsub, #btncancel").show();
  		$("#Waittxt").hide();
  	}
	}
	$("#chkInsAll").click(function() {
    if($('#chkInsAll').is(':checked')){
      $(".inskey").prop('checked', true);
      
    }else{
      $(".inskey").prop('checked', false);
      
    }
  });

	$("#btn_send_order").click(function() {
			$("#table_data").empty();
    	var txtsearch = $("#txtSearch").val();
    	if(txtsearch){
	    	$.ajax({ 
						url: 'inc_qrcode/inc_action_qr.php',
						type:'POST',
						data: {action: 'GetListPayQR', txtsearch:txtsearch},
						success:function(rs){ 
							$("#customer_id").val(txtsearch)
							if(rs){
								$('#table_data').append(rs);
							}else{
								$('#table_data').append("<td colspan='13'><div class='fs18 cf40053 fwb mt20 t_c'>ไม่พบข้อมูลกรุณาตรวจสอบ</div></td>");
							}
						}
				});
			}else{
				alert("กรุณากรอกรหัสลูกค้าหรือเบอร์โทรศัพท์ลูกค้า");
			}

  });

  

function filterPOcode(){
	txtse = $("#txtse").val();
	date = $("#date").val();
	dateEnd = $("#dateEnd").val();
	window.location.href = "pay_qrcodelists.php?txtse="+txtse+"&date="+date+"&dateend="+dateEnd;
}


$( '[data-fancybox="QRCode"]' ).fancybox({
  buttons : [
    'download',
    'zoom',
    'close'
  ],
  protect : false,
  animationEffect : "zoom-in-out",
  transitionEffect : "fade",
  zoomOpacity : "auto",
  animationDuration : 500,
  zoomType: 'innerzoom',

});

function btnExport(){
		txtse = $("#txtse").val();
		date = $("#date").val();
		dateEnd = $("#dateEnd").val();
		if(date){
			// window.open("callcenters/export_inform.php?date="+date+"&insurers="+insurers);
			window.location.href = "inc_qrcode/export_qr.php?txtse="+txtse+"&date="+date+"&dateend="+dateEnd;
		}
	}
</script>