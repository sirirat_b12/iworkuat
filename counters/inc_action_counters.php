<?php
session_start();
date_default_timezone_set("Asia/Bangkok");
header('Content-Type: text/html; charset=utf-8');

include "../inc_config.php";
include("../include/sms.class.php");

require '../phpmailer6/src/PHPMailer.php';
require '../phpmailer6/src/SMTP.php';
require '../phpmailer6/src/Exception.php';


use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

include "inc_function_counters.php"; 

if($_POST["action"] == "inserListpay"){
	$user =$_SESSION["User"]['UserCode']; 
	// echo "<pre>".print_r($_POST,1)."</pre>";
	$counter_type = "PASTPO";
	$paycode1 = "1007600".getMaxIDCS();
	$paycode = $paycode1."".func_name($paycode1);
// exit();
	if($user){
		foreach ($_POST["inskey"] as $key => $value) { 
			$val = explode("|", $value);

			$getIns = getPOListCounter($val[0], $val[1]);
			$number_period = $key+1;
			$replace = array("P","C");
			$reference_2 = str_replace($replace, "", $getIns["Customer_ID"]);
			$status_pay = ($getIns["Installment_Status_ID"] == "001") ? 0 : 1 ; 
			$insur_type = ($getIns["Insurance_Name"] == "พรบ.") ? "compu" : "insur";  
			// echo "<pre>".print_r($getIns,1)."</pre>";
			$sql = " INSERT INTO [dbo].[counter_service] ([counter_type], [paycode], [reference_1], [reference_2], [po_id], [customer_id], [insur_type], [installment_num], [number_period], [installment_due_date], [amount], [email], [tel_no], [customer_name], [status_pay], [status_cs], [status_email], [status_sms], [enable], [created_date], [created_by], [update_date], [update_by], [remark], [receive_id], [transaction_id], [log_id], [vendor_id], [zone], [service_id], [service_run_no], [counter_no], [payment_date])
	     VALUES ('".$counter_type."', '".$paycode."', '".$paycode."', '".$reference_2."', '".$getIns["PO_ID"]."', '".$getIns["Customer_ID"]."', '".$insur_type."', '".$getIns["Installment_ID"]."', '".$number_period."', '".$getIns["Installment_Due_Date"]->format("Y-m-d")."', '".$getIns["ISTM_Total_Amount"]."', '".$_POST["email"]."', '".$_POST["phone"]."', '".$getIns["Customer_FName"]." ".$getIns["Customer_LName"]."', '".$status_pay."', 0, 0, 0, 'Y', '".date('Y-m-d H:i:s')."', '".$user."', '".date('Y-m-d H:i:s')."', '".$user."', '".$_POST["remark"]."', '', '', '', '', '', '', '', '', '') ";
	    $stmt = sqlsrv_query( $connMS, $sql);
		}

		// $mail = sendMailForCounterService($email);

		if($stmt){
			$email["paycode"] = $paycode;
			$email["reference_2"] = $reference_2;
			$email["name"] = $getIns["Customer_FName"]." ".$getIns["Customer_LName"];
			$email["phone"] = $_POST["phone"];
			$email["remark"] = $_POST["remark"];
			// $mail = sendMailForCounterService($email);
			// if($_POST["phone"]){
			// 	$getSendSMS =  getSendSMS($email["phone"], $paycode, "", "", "", "","insert");
			// }else{
			// 	$getSendSMS = 0;
			// }
			$sqlup = "UPDATE [dbo].[counter_service] SET [status_email] = '".$mail."', [status_sms] = '".$getSendSMS."' WHERE paycode = '".$paycode."' ";
			$stmtup = sqlsrv_query( $connMS, $sqlup);
			
			echo "<script>alert('ทำรายการชำระเงินสำเร็จ โปรดตรวจสอบ');</script>";
			echo '<META http-equiv="refresh" content="0;URL=../counters.php?txtse='.$paycode.'">';
			exit();
		}else{
			echo "<script>alert('ไม่สามารถทำรายการได้ กรุณาลองอีกครั้ง');</script>";
			echo '<META http-equiv="refresh" content="0;URL=../counters.php">';
			exit();
		}
	}else{
		echo "<script>alert('ไม่ได้ล็อคอิน กรุณา ล็อคอินใหม่');</script>";
		echo '<META http-equiv="refresh" content="0;URL=../login.php">';
		exit();
	}

}else if($_POST["action"] == "GetListCS"){
	$getPOPayForCounter = getPOPayForCounter($_POST["txtsearch"]);
	// echo "<pre>".print_r($getPOPayForCounter,1)."</pre>";
	$i=1; 
	$sum = 0; 
	foreach ($getPOPayForCounter as $key => $value) {
		$sum = $sum + $value["ISTM_Total_Amount"];
		if(!$value["ISTM_Total_Amount"]){
			if($value["Insurance_ID"] == "029" || $value["ISTM_Total_Amount"] == 0){
				$case = '<input type="checkbox" class="inskey" id="qrPrice_'.$i.'" name="inskey[]" value="'.$value["PO_ID"]."|".$value["Installment_ID"].'">';
			}else{
				$case =  "<sapn class='c2457ff'>ยอดไม่พอ</sapn>";
			}
		}else if($value["Installment_Status"] != "Paid" && $value["ISTM_Total_Amount"] > 0 &&  $value["enable"] != 'Y' ){
			$case = '<input type="checkbox" class="inskey" id="qrPrice_'.$i.'" name="inskey[]" value="'.$value["PO_ID"]."|".$value["Installment_ID"].'">';
		}else{
			$case =  "<sapn class='c2457ff'>สร้างชำระแล้ว</sapn>";
		}
		$txt .= '
		<tr >
			<td class="t_c">'.$case.'</td>
			<td class="wpno t_c" style="width: 5%;">'.$value["Installment_ID"].'</td>
			<td class="wpno t_c cff2da5" style="width: 10%;">'.$value["PO_ID"].'</td>
			<td class="wpno " >
					<div>'.$value["Customer_ID"].'</div>
					<div>'.$value["Customer_FName"]." ".$value["Customer_LName"].'</div>
				</td>
			<td class="t_c ">'.$value["Insurance_Name"].'</td>
			<td class="t_c ">'.$value["Installment_Due_Date"]->format("d/m/Y").'</td>
			<td class=" t_r"> <b>'.number_format($value["ISTM_Net_Premium"],2).'</b></td>
			<td class=" t_r"> <b>'.number_format($value["ISTM_Duty"],2).'</b></td>
			<td class=" t_r"> <b>'.number_format($value["ISTM_Tax"],2).'</b></td>
			<td class=" t_r"> <b>'.number_format($value["ISTM_Total_Premium"],2).'</b></td>
			<td class=" t_r"> <b>'.number_format($value["ISTM_Discount"],2).'</b></td>
			<td class=" t_r"> <b>'.number_format($value["ISTM_Total_Amount"],2).'</b></td>
			<td class=" t_c cff2da5"> <b>'.$value["Installment_Status"].'</b></td>
			<!-- <td class=" t_c"> <b><a data-fancybox data-type="iframe" href="http://61.90.142.230/adbchk/ibroker-attach/1POPUP/?poid='.$value["PO_ID"].'">เอกสารผ่อน</a></b></td> -->
		</tr>
		<tr class="dn boxQRALL">
			<td class="wpno t_r" colspan="12">รวม</td>
			<td class="wpno t_c" ><div class="showQRAll">'.number_format($sum,2).'</div></td>
		</tr>';
		$i++;
	}
	echo $txt;
	exit();

}else if($_POST["action"] == "GetDataPhone"){
	
	$sql = "SELECT Tel_No,Mobile_No,EMail From [dbo].Customer WHERE Customer_ID = '".$_POST["txtsearch"]."' ";
	$stmt = sqlsrv_query( $connMS, $sql );
  $row = sqlsrv_fetch_array($stmt);
  $arr = array();
  $arr["phone"] = ($row["Tel_No"]) ? $row["Tel_No"] : $row["Mobile_No"];
  $arr["email"] = $row["EMail"];
  echo json_encode($arr);
  exit();
  // echo "<pre>".print_r($arr,1)."</pre>";

}else if($_POST["action"] == "deleteListcs"){
		$user =$_SESSION["User"]['UserCode']; 
		$sql = "UPDATE [dbo].[counter_service] SET [enable] = 'N', [remark] = 'Delete:".date('Y-m-d H:i:s')." | BY:".$user."' WHERE counter_id = '".$_POST["cs_id"]."' ";
	 	$stmt = sqlsrv_query( $connMS, $sql);
	 	if($sql){
	 		echo "1";
	 	}else{
	 		echo "0";
	 	}
	 exit();
}

?>