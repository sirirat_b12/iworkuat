<?php
session_start();
date_default_timezone_set("Asia/Bangkok");
header('Content-Type: text/html; charset=utf-8');

include "../inc_config.php";
include "../include/sms.class.php";
include "inc_function_counters.php"; 


// $_GET["action"] = "inser";
// $_GET["po"] = "PO61029448";
// $_GET["ins"] = "5"; //งวด

if($_GET["action"] == "inser"){

	$checkCS = checkCS($_GET["po"], $_GET["ins"]);

	$paycode1 = "1007600".getMaxIDCS();
	$paycode = $paycode1."".func_name($paycode1);
	$getIns = getPOListCounter($_GET["po"], $_GET["ins"]);
	// echo "<pre>".print_r($getIns,1)."</pre>";
	// exit();
  if(!$checkCS){

	  if($getIns["Installment_Status_ID"] == "001"){
		  $number_period = $key+1;
		  $replace = array("P","C");
		  $reference_2 = str_replace($replace, "", $getIns["Customer_ID"]);
		  $status_pay = ($getIns["Installment_Status_ID"] == "001") ? 0 : 1 ; 
		  $insur_type = ($getIns["Insurance_Name"] == "พรบ.") ? "compu" : "insur";  
		  $remark = "LineAPI";
		  $sql = "
		  	INSERT INTO [dbo].[counter_service] ([counter_type], [paycode], [reference_1], [reference_2], [po_id], [customer_id], [insur_type], [installment_num], [number_period], [installment_due_date], [amount], [email], [tel_no], [customer_name], [status_pay], [status_cs], [status_email], [status_sms], [enable], [created_date], [created_by], [update_date], [update_by], [remark], [receive_id], [transaction_id], [log_id], [vendor_id], [zone], [service_id], [service_run_no], [counter_no], [payment_date])
		  
		  	VALUES ('PASTPO', '".$paycode."', '".$paycode."', '".$reference_2."', '".$getIns["PO_ID"]."', '".$getIns["Customer_ID"]."', '".$insur_type."', '".$getIns["Installment_ID"]."', '".$number_period."', '".$getIns["Installment_Due_Date"]->format("Y-m-d")."', '".$getIns["ISTM_Total_Amount"]."', '".$_POST["email"]."', '".$_POST["phone"]."', '".$getIns["Customer_FName"]." ".$getIns["Customer_LName"]."', '".$status_pay."', 0, 0, 0, 'Y', '".date('Y-m-d H:i:s')."', '".$getIns["Employee_ID"]."', '".date('Y-m-d H:i:s')."', '".$getIns["Employee_ID"]."', '".$remark."', '', '', '', '', '', '', '', '', '') ";
		  $stmt = sqlsrv_query( $connMS, $sql);
		  	// echo "<script>alert('ไม่สามารถทำรายการได้ กรุณาลองอีกครั้ง');</script>";

		  $msg = 'ลูกค้าสร้าง Counter Service ผ่าน Line@  \n'.$_GET['po'].' | '.$_GET['ins'].'\nรหัสชำระ = '.$paycode.'\nวันที่สร้าง '.date('Y-m-d H:i:s');
			$curl = curlLineNoti($getIns["Employee_ID"], $msg);

			
			echo '<META http-equiv="refresh" content="0;URL=../export_counterservices.php?paycode='.base64_encode($paycode).'">';
			exit();		  
		}else{
			// $ddd =print_r($checkCS,1);
			// echo "<script>alert('ไม่สามารถทำรายการได้ กรุณาลองอีกครั้ง".$ddd."');</script>";
			echo "<script>alert('ไม่สามารถทำรายการได้ กรุณาลองอีกครั้ง');</script>";

			// echo '<META http-equiv="refresh" content="0;URL=https://line.me/R/ti/p/%40znp6626t">';
			exit();
		}
	}elseif ($checkCS["status_pay"] == 0) {
		// echo $checkCS["status_pay"];
		echo '<META http-equiv="refresh" content="0;URL=../export_counterservices.php?paycode='.base64_encode($checkCS["paycode"]).'">';
		exit();
	}else{
		echo "<script>alert('ไม่สามารถทำรายการได้ กรุณาลองอีกครั้ง2');</script>";
		// echo '<META http-equiv="refresh" content="0;URL=https://line.me/R/ti/p/%40znp6626t">';
		exit();
	}

}


function curlLineNoti($user, $msg){
	$url  = 'https://www.asiadirect.co.th/line-at/ibroker/linemeapi.php';
	$data = array(
	    'user_id' => $user,
	    'status' => "QRpayment",
	    'messages' => $msg
	);
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$result = curl_exec($ch);
		$error_msg = curl_error($ch);
		curl_close($ch);
	return 1;
	exit();
}

?>