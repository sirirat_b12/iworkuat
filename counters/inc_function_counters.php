<?php 

function getPOPayForCounter($txtSe){
	global $connMS;
	$resultarray = array();
  $sql = "SELECT   Purchase_Order.PO_ID, counter_service.paycode,counter_service.enable, Insurance.Insurance_Name, Purchase_Order.PO_Date, Purchase_Order.Operation_Type_ID, Purchase_Order.Insurance_ID, Purchase_Order.Customer_ID, Purchase_Order.Agent_ID, Purchase_Order.Employee_ID, Purchase_Order.Status_ID,Status.Status_Desc,
		Installment.PO_ID, Installment.Installment_ID, Installment.Installment_Due_Date, Installment.ISTM_Net_Premium, Installment.ISTM_Duty, 
		Installment.ISTM_Tax, Installment.ISTM_Total_Premium, Installment.ISTM_Discount,Installment.ISTM_Total_Amount, Installment.Installment_Status_ID,
		Installment_Status.Installment_Status, Installment.Remark, Customer.Customer_ID, Customer.Customer_FName, Customer.Customer_LName, Customer.EMail, 
		Customer.Tel_No, Customer.Mobile_No
		FROM Purchase_Order 
		LEFT JOIN  Installment ON Purchase_Order.PO_ID = Installment.PO_ID
		LEFT JOIN  counter_service ON Installment.PO_ID = counter_service.PO_ID AND Installment.Installment_ID = counter_service.installment_num and counter_service.enable='Y'
		INNER JOIN  Customer ON Purchase_Order.Customer_ID = Customer.Customer_ID
		LEFT JOIN  Status ON Purchase_Order.Status_ID = Status.Status_ID
		LEFT JOIN  Installment_Status ON Installment.Installment_Status_ID = Installment_Status.Installment_Status_ID
		LEFT JOIN  Insurance ON Purchase_Order.Insurance_ID = Insurance.Insurance_ID
		WHERE Purchase_Order.Customer_ID LIKE '%".$txtSe."%'
		AND Installment.Installment_Status_ID = '001'
		AND Purchase_Order.Status_ID IN ('RCA','RVP','CLS')
		ORDER BY Purchase_Order.Insurance_ID,Installment.Installment_ID ASC";
	  $stmt = sqlsrv_query( $connMS, $sql );
    if(sqlsrv_has_rows($stmt)) {
	    while( $row = sqlsrv_fetch_array($stmt) ) { 
	      $resultarray[] = $row;
	    }
	    return $resultarray;
			exit();
		}
}


function getPORow($poid){
	global $connMS;
	$resultarray = array();
  $sql = "SELECT  * FROM Purchase_Order WHERE Purchase_Order.PO_ID = '".$poid."'";
	  $stmt = sqlsrv_query( $connMS, $sql );
		  if(sqlsrv_has_rows($stmt)) {
		    $row = sqlsrv_fetch_array($stmt);
		    return $row["Status_ID"];
		    exit();
			}  
			exit();
}

function getMaxIDCS(){
	global $connMS;
	$year = date("Y");
	$sql = "SELECT MAX(paycode) AS paycode From [dbo].counter_service WHERE year(created_date) = '".$year."' AND MONTH(created_date) = '".date("m")."' ";
	$stmt = sqlsrv_query( $connMS, $sql );
  if(sqlsrv_has_rows($stmt)) {
    $row = sqlsrv_fetch_array($stmt);
		$code = substr(substr($row["paycode"], 11), 0, -2);
    $idMax = (date("y")+43)."".date("m")."".str_pad(intval($code)+1,4,"0",STR_PAD_LEFT);
    return $idMax;
    exit();
  }  
}

function getMaxIDRV(){
	global $connMS;
	$year = date("Y");
	$sql = "SELECT max(Receive_ID) AS count From [dbo].Receive ";
	$stmt = sqlsrv_query( $connMS, $sql );
  if(sqlsrv_has_rows($stmt)) {
    $row = sqlsrv_fetch_array($stmt);
    $txt = "RV".(date("y")+43);
    $idex = explode($txt, $row["count"]);
    $idMax = $txt."".str_pad(intval($idex[1])+1,6,"0",STR_PAD_LEFT);
    return $idMax;
    exit();
  }  
}


function func_name($docnum){
    $docnum = preg_replace("/[^0-9]/","",$docnum);  
    $vals = array_reverse(str_split($docnum));
    $vals2 = str_split($docnum);
    $num = array(1,2,3,7,6);
    $total = 0;
    $i = 1;
    foreach($vals2 as $k => $v){
    	if($i <= 3){
    		$vals[$k] = $v * $i;
    	}else if($i == 4){
    		$vals[$k] = $v * 7;
    	}else if($i == 5){
    		$vals[$k] = $v * 6;
    		$i = 0;
    	}
    	$total += $vals[$k];
      $i++;
    }
    $md = $total % 100;
    return str_pad(intval($md),2,"0",STR_PAD_LEFT);
}

function getListCounterAll($UserCode, $txtse="", $case){
	global $connMS;
	$resultarray = array();
	$txtse = trim($txtse);
	// if($_SESSION["User"]['type'] == "SuperAdmin"){
	// 	$sql = "SELECT  * FROM counter_service  " ;
	// }else{
	// 	$sql = "SELECT  * FROM counter_service  WHERE created_by = '".$user."' " ;
	// }
	$sql = "SELECT  * FROM counter_service " ;
	if($txtse){
		if($case == "page"){
			$sql .="WHERE (paycode = '".$txtse."' OR po_id = '".$txtse."'  OR tel_no = '".$txtse."') ORDER BY number_period ASC";
		}else if($case == "pdf"){
			$sql .=" WHERE paycode = '".$txtse."' AND enable = 'Y' ORDER BY number_period ASC";
		}
	}else{
		$sql .="	ORDER BY paycode DESC,number_period ASC";
	}
	// echo $sql;
	  $stmt = sqlsrv_query( $connMS, $sql );
    if(sqlsrv_has_rows($stmt)) {
	    while( $row = sqlsrv_fetch_array($stmt) ) { 
	      $resultarray[] = $row;
	    }
	    return $resultarray;
			exit();
		}
}


function getPOListCounter($po, $inst){
	global $connMS;
	$resultarray = array();
  $sql = "SELECT    Purchase_Order.PO_ID, Insurance.Insurance_Name, Purchase_Order.PO_Date, Purchase_Order.Operation_Type_ID, Purchase_Order.Customer_ID, Purchase_Order.Agent_ID, Purchase_Order.Employee_ID, Purchase_Order.Status_ID,Status.Status_Desc,
		Installment.PO_ID, Installment.Installment_ID, Installment.Installment_Due_Date, Installment.ISTM_Net_Premium, Installment.ISTM_Duty, Installment.ISTM_Tax, Installment.ISTM_Total_Premium, Installment.ISTM_Discount,Installment.ISTM_Total_Amount, Installment.Installment_Status_ID,Installment_Status.Installment_Status, Installment.Remark, Customer.Customer_ID, Customer.Customer_FName, Customer.Customer_LName, Customer.EMail, Customer.Tel_No, Customer.Mobile_No
		FROM Purchase_Order 
		LEFT JOIN  Installment ON Purchase_Order.PO_ID = Installment.PO_ID
		INNER JOIN  Customer ON Purchase_Order.Customer_ID = Customer.Customer_ID
		LEFT JOIN  Status ON Purchase_Order.Status_ID = Status.Status_ID
		LEFT JOIN  Installment_Status ON Installment.Installment_Status_ID = Installment_Status.Installment_Status_ID
		LEFT JOIN  Insurance ON Purchase_Order.Insurance_ID = Insurance.Insurance_ID
		WHERE Installment.PO_ID = '".$po."'  AND Installment.Installment_ID = '".$inst."' ORDER BY Purchase_Order.Insurance_ID,Installment.Installment_ID ASC";
	  	$stmt = sqlsrv_query( $connMS, $sql );
		  if(sqlsrv_has_rows($stmt)) {
		    $row = sqlsrv_fetch_array($stmt);
		    return $row;
		    exit();
			}  
			exit();
}


function shorturl($long_url){
  $url = "https://asiadirect.co.th/shorturl/insert.php";
  $data = array(
    'action' => "inserUrl",
    'long_url' => $long_url
  );
  $ch = curl_init($url);
  curl_setopt($ch, CURLOPT_POST, 1);
  curl_setopt($ch, CURLOPT_POSTREDIR, 3);
  curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
  curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
  curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  $result = curl_exec($ch);
  $error_msg = curl_error($ch);
  curl_close($ch);

  return $result;
}


function getSendSMS($phone, $paycode, $numpay, $price, $type, $pocode, $case ){
	if($case == "insert"){
		// $txt = 'ชำระเงิน เคาน์เตอร์เซอร์วิส(7-11) รหัส '.$paycode.' โทร. 020892000';
		// $long_url = "http://61.90.142.230/adbchk/export_counterservices.php?paycode=".base64_encode($paycode);
		$long_url = "https://adbwecare.com/adbchk/export_counterservices.php?paycode=".base64_encode($paycode);
	    $shorturl = shorturl($long_url);
	    $txt = 'โปรดชำระเงินที่เคาน์เตอร์เซอร์วิส(7-11) รหัสชำระ '.$paycode.' '.$shorturl;
	}else if($case == "afterPay"){
		$paytype = ($type == "compu") ? "พรบ." : "ประกันภัย";
		$txt = 'ชำระค่า '.$paytype.' งวด '.$numpay.' ['.number_format($price).' บ.] จาก '.$pocode.' ref.'.$paycode;
	}else if($case == "cancel"){
		$paytype = ($type == "compu") ? "พรบ." : "ประกันภัย";
		$txt = 'ยกเลิกชำระ '.$paytype.' งวด '.$numpay.' ['.number_format($price).' บ.] จาก '.$pocode.' ref.'.$paycode;
	}

	if($txt){
		// $result_sms = sms::send_sms('adbwecare','6251002','0894353549', $txt,'AsiaDirect','','');
		$result_sms = sms::send_sms('adbwecare','6251002',$phone, $txt,'AsiaDirect','','');
	}
	// $result_sms = 0;
	if($result_sms){
		return 1;
	}else{
		return 0;
	}
	exit();
}

function genBarcode($po,$MyBarCode){
	include('../barcodeClsass/BCGFont.php');
	include('../barcodeClsass/BCGColor.php');
	include('../barcodeClsass/BCGDrawing.php'); 
	include('../barcodeClsass/BCGcode128.barcode.php');
	$font = new BCGFont('barcodeClsass/font/Arial.ttf', 10);
	$colorFront = new BCGColor(0, 0, 0);
	$colorBack = new BCGColor(255, 255, 255);
	$color_white = new BCGColor(255, 255, 255); 

	$barcode = $MyBarCode;
	$code = new BCGcode128();
	$code->setScale(1);
	$code->setThickness(30);
	$code->setForegroundColor($colorFront);
	$code->setBackgroundColor($colorBack);
	$code->setFont(0);
	$code->setStart(1);
	$code->setTilde(true);
	$code->parse($barcode);

	$purl = "../picBarcode/".$po.".png";
	$drawing = new BCGDrawing($purl, $color_white);
	$drawing->setBarcode($code);
	$drawing->draw();
	$drawing->finish(BCGDrawing::IMG_FORMAT_PNG);
}



function sendMailForCounterService($arr){ 

	// include "../phpmailer/class.phpmailer.php";
	$nameBarcode = $arr["paycode"]."_".date("ymdHsis");
	$genBarcode = "|024554000009700".chr(10)."".$arr["paycode"]."".chr(10)."".$arr["reference_2"]."".chr(10)."0";
	$MyBarCode = genBarcode($nameBarcode, $genBarcode);
	$mail = new PHPMailer\PHPMailer\PHPMailer(true);   
	// $mail = new PHPMailer();

	$bodyMail = "
        <div style='width:1200px;font-size: 13px; color:#000;'>
            <div style='border-bottom: 2px solid #f7941e; background-color: #0aaaef;color: #fff;padding: 15px;font-size: 15px;text-align: center;'>
                <div style='text-align: center; padding-top: 10px;'>
                    <img src='https://www.asiadirect.co.th/image/logo/logo_110.png' style='width: 70px;'>
                </div>
                <div>
                    <strong>บริษัท เอเชียไดเร็ค อินชัวร์รันส์ โบรคเกอร์ จำกัด</strong><br> เลขที่ 626 อาคารบีบีดี ชั้น 11 ซอยจินดาถวิล ถนนพระราม 4 แขวงมหาพฤฒาราม เขตบางรัก กรุงเทพฯ 10500<br> เปิดให้บริการ วันจันทร์-วันศุกร์ เวลา 8.30-17.00 น. โทร. 02-089-2000
                </div>
            </div>
        		<div style=''>
              <div style='clear: both; '>
              		<div style='margin-top:10px; font-size:  18px;font-weight: bold;'>ใบแจ้งการชำระเงิน เคาน์เตอร์เซอร์วิส (PaySlip)</div>

                  <div style='border-bottom: 2px solid #ffffff;padding:10px;'>
                      <div style='border-top: 2px solid #f7941e;padding: 10px 0;font-size: 16px;'>
                          <b style='color:#000;'>ข้อมูลการชะระเงิน </b>
                          <span style='float: right;'>".date("Y-m-d H:i:s")."</span>
                          <br><span style='font-size: 25px;'><b>รหัสชำระเงิน :</b> <span style='color: #E91E63;font-weight: bold;'>".$arr["paycode"]."</span></span> 
                          <br><b>คุณ</b> ".$arr["name"]." 
                         	<br><b>เบอร์ </b> ".$arr["phone"]."
                         	<br><b>หมายเหตุ </b> ".$arr["remark"]."
                         	<div style='margin-top:25px; font-size:20px;'>
                         		<a href='http://adbwecare.com/adbchk/export_counterservices.php?paycode=".base64_encode($arr["paycode"])."' target='_bank'>พิมพ์ใบชำระเงิน</a>
                         	</div>
                         	<div style='font-size:  16px;'>ท่านสามารถพิมพ์ใบชำระเงิน โดยกดปุ่ม <b>พิมพ์ใบชำระเงิน</b> เพื่อพิมพ์ใบชำระเงินโดยบาร์โค้ด หรือแสดง <b>รหัสชำระเงิน</b> ที่เคาน์เตอร์เซอร์วิส </div>
                      </div>
                  </div>
             	</div>
                        ";
 
        $bodyMail .= "
                <div style='text-align: center;font-size:14px;margin-top: 30px;margin-bottom: 30px;'><b>ขอบพระคุณที่ใช้บริการของเรา</b> <br>บริษัท เอเชียไดเร็ค อินชัวร์รันส์ โบรคเกอร์ จำกัด</div>
            </div>
            <div style='text-align: center;font-size: 14px;background-color: #0aaaef;color: #fff;border-top: 2px solid #f7941e;padding: 10px;'>
                02-625-1000 | info@asiadirect.co.th | <a href='https://www.asiadirect.co.th'>www.asiadirect.co.th</a>
                <br>Copyright © 2017 Asia Direct Insurance Broker
            </div>
        </div>
        ";
   // echo $bodyMail; 
  	$mail->CharSet = 'utf-8';
  	$mail->SMTPDebug = 0;                                 // Enable verbose debug output
    $mail->isSMTP();                                      // Set mailer to use SMTP
    $mail->Host = 'smtp.gmail.com'; // Specify main and backup SMTP servers
    $mail->SMTPAuth = true;  
    $mail->Username = "admin@asiadirect.co.th";
    $mail->Password = "@db6251000";
    $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
    $mail->Port = 587;                                    // TCP port to connect to

    $mail->setFrom('admin@asiadirect.co.th', 'Asia Direct');
	  if($arr["email"]){
	    $mail->AddAddress($arr["email"], $arr["name"]); 
	  }
	  $mail->AddAddress($_SESSION["User"]['email'], "Sale");
    // $mail->AddAddress('teerakan.s@asiadirect.co.th', 'IT');               // Name is optional
    $mail->isHTML(true);                                  // Set email format to HTML
    $mail->Subject = "แจ้งการชำระเงินผ่านระบบ เคาน์เตอร์เซอร์วิส (ร้าน 7-11)";
    $mail->Body    = $bodyMail;

    // $mail->send();
// exit();
	if(!$mail->Send()) {
		return 0;
	} else {
		return 1;
	}
}

function searchForId($id, $array) {
   foreach ($array as $key => $val) {
       if ($val['uid'] === $id) {
           return $key;
       }
   }
   return null;
}

function getCounterForXML($ref1="", $ref2=""){
	global $connMS;
	$resultarray = array();
	if($ref1 && $ref2){
		$sql = "SELECT  * FROM counter_service  WHERE status_pay = '0' AND reference_1 = '".$ref1."' AND reference_2 = '".$ref2."' ORDER BY number_period ASC " ;
	}else{
		$sql = "SELECT  * FROM counter_service  WHERE status_pay = '0' AND reference_1 = '".$ref1."' ORDER BY number_period ASC " ;
	}	
  $stmt = sqlsrv_query( $connMS, $sql );
  if(sqlsrv_has_rows($stmt)) {
    while( $row = sqlsrv_fetch_array($stmt) ) { 
      $resultarray[] = $row;
    }
    if($resultarray){
    	return $resultarray;
    }else{
    	return 0;
    }
		exit();
	}
}


function getListCsPay($txtse, $date, $dateend){
	global $connMS;
	$resultarray = array();
	$dateStart = $date." 00:00:00";
	$dateEnd = $dateend." 23:59:59";

 	$sql = "SELECT counter_service.*,Insurer.Insurer_Initials
		FROM counter_service 
		LEFT JOIN Purchase_Order_Insurer ON counter_service.PO_ID = Purchase_Order_Insurer.PO_ID
		LEFT JOIN Insurer ON Purchase_Order_Insurer.Insurer_ID = Insurer.Insurer_ID
		 WHERE counter_service.status_cs IN ('100','OR') ";
		if($txtse){
			$sql .= " AND paycode = '".$txtse."' OR po_id = '".$txtse."'  OR tel_no = '".$txtse."' ";
		}else{
			$sql .= " AND payment_date BETWEEN '".$dateStart."' AND '".$dateEnd."' ";
		}
		$sql .= "ORDER BY update_date DESC";

		// echo $sql;
	  	$stmt = sqlsrv_query( $connMS, $sql );
    if(sqlsrv_has_rows($stmt)) {
	    while( $row = sqlsrv_fetch_array($stmt) ) { 
	      $resultarray[] = $row;
	    }
	    return $resultarray;
			exit();
		}
}


function checkCS($po, $ins){
		global $connMS;
		$resultarray = array();
		$sql = "SELECT  *
		FROM counter_service 
		WHERE po_id = '".$po."'  AND installment_num = '".$ins."' AND enable = 'Y' ";
  	$stmt = sqlsrv_query( $connMS, $sql );
	  if(sqlsrv_has_rows($stmt)) {
	    $row = sqlsrv_fetch_array($stmt);
	    return $row;
	    exit();
		}else{
			return 0;
	    exit();
		}
		exit();
}
?>