
<?php

date_default_timezone_set('Asia/Bangkok');
header('Content-Type: text/html; charset=utf-8');

$strExcelFileName = "ReportCounters".date('YmdHis').".xls";

header("Content-Type: application/vnd.ms-excel; name=\"$strExcelFileName\"");
header("Content-Disposition: inline; filename=\"$strExcelFileName\"");
header("Pragma:no-cache");

include "../inc_config.php"; 
include "inc_function_counters.php";

$date = ($_GET["date"]) ? $_GET["date"] : date("Y-m-d");
$dateEnd = ($_GET["dateend"]) ? $_GET["dateend"] : date("Y-m-d");
if($_GET["date"]){
    $getListCsPay = getListCsPay(trim($_GET["txtse"]), $date, $dateEnd);
}
// $reportInformByAdmin = reportInformByAdmin($_GET["ds"], $_GET["de"], $_GET["insurers"]);
// echo "<pre>".print_r($reportInformByAdmin,1)."</pre>";

?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>
    <div id="SiXhEaD_Excel" align=center x:publishsource="Excel">
        <table x:str border=1 cellpadding=1 cellspacing=1 width=100% style="border-collapse:collapse">
           <tr class="table p5">
                <th class="t_c">Pay Code</th>
                <th class="t_c">สถานะ CS</th> 
                <th class="t_c">PO Code</th>
                <th class="t_c">RV</th>
                <th class="t_c">วันที่ชำระ</th>
                <th class="t_c">TX ID</th>
                <th class="t_c">Log</th>
                <th class="t_c">บริษัทประกัน</th>
                <th class="t_c">งวดที่</th>
                <th class="t_c">Ref.1</th>
                <th class="t_c">Ref.2</th>
                <th class="t_c">รหัสลูกค้า</th>
                <th class="t_c">ชื่อ-นามสกุล</th>
                <th class="t_c">เบอร์</th>
                <th class="t_c">ประเภท</th>
                <th class="t_c">วันครบชำระ</th>
                <th class="t_c">ผู้สร้าง</th>
                <th class="t_c">จำนวน</th>
            </tr>
            <?php 
                $i = 1;
                foreach ($getListCsPay as $key => $value) {
                $status_pay = ($value["status_pay"] == 0 ) ? "ตั้งหนี้" : "ชำระ"; 
                $dudDate = ($value["installment_due_date"]) ? $value["installment_due_date"]->format("d/m/Y") : "-";
                if($value["status_pay"] == 1){
                    $sum = $sum + $value["amount"];
                } 
            ?>
                <tr>
                    <td class="t_c c2457ff fwb"><?php echo $value["paycode"]; ?></td>
                    <td class="t_c fwb cff2da5">
                        <?php echo ($value["status_cs"]=='100') ? "ชำระ" : "ยกเลิก"; ?>
                    </td>
                    <td class="t_c fwb c0aaaef"><?php echo $value["po_id"]; ?></td>
                    <td class="t_c fwb cff8000"><?php echo $value["receive_id"]; ?></td>
                    <td class="t_c cff2da5 fwb"><?php echo $value["payment_date"]->format("d/m/Y H:i:s"); ?></td>
                    <td class="t_c c2457ff"><?php echo $value["transaction_id"]; ?></td>
                    <td class="t_c c2457ff"><?php echo $value["log_id"]; ?></td>
                    <td class="t_c "><?php echo $value["Insurer_Initials"]; ?></td>
                    <td class="t_c c2457ff"><?php echo $value["installment_num"]; ?></td>
                    <td class="t_c "><?php echo $value["reference_1"]; ?></td>
                    <td class="t_c "><?php echo $value["reference_2"]; ?></td>
                    <td class="t_c "><?php echo $value["customer_id"]; ?></td>
                    <td class="t_l cff2da5 fwb"><?php echo $value["customer_name"]; ?></td>
                    <td class="t_c "><?php echo $value["tel_no"]; ?></td>
                    <td class="t_c "><?php echo ($value["insur_type"] == "insur") ? "ประกันภัย" : "พรบ."; ?></td>
                    <td class="t_c "><?php echo $dudDate; ?></td>
                    <td class="t_c "><?php echo $value["created_by"]; ?></td>
                    <td class="t_r fwb <?php if($value["status_pay"] == 1){ echo "amount"; } ?>" data-amount="<?php echo $value["amount"]; ?>">
                        <?php echo number_format($value["amount"],2); ?>
                    </td>
                </tr>
            <?php } ?>
        </table>
    </div>

    <script>
        window.onbeforeunload = function(){return false;};
        setTimeout(function(){window.close();}, 10000);
   
    </script>
</body>
</html>
