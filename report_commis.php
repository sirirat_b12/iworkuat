<?php 
session_start();
ini_set('max_execution_time', 186);
ini_set('memory_limit', '2048M');
$UserCode = $_SESSION["User"]['UserCode'];
$userType = $_SESSION["User"]['type'];

if(!isset($UserCode)){
	echo "<script>alert('กรุณาเข้าสู่ระบบ!!!');</script>";
	echo '<META http-equiv="refresh" content="0;URL=login.php">';
	exit();
}else if($_SESSION["User"]['type'] != "SuperAdmin"){
	echo "<script>alert('เฉพาะผู้มีสิทธิ์ใช้งานเท่านั้น !!!');</script>";
	echo '<META http-equiv="refresh" content="0;URL=index.php">';
	exit();
}


header('Content-Type: text/html; charset=utf-8');
include "include/inc_header.php"; 
include "include/inc_menu.php"; 
include "reports/inc_function_report.php";
include "reports/targetdata.php";
// include('qrcode/qrcode.class.php');

$year = date("Y");

$_GET["txtse"] = ($_GET["txtse"]) ? $_GET["txtse"] : date("Y-m");
$getDepartment = getDepartment();

if($_GET["txtse"]){
	$getDataCount = getSaleSumforMounth($_GET["txtse"], $_GET["depar"]);
}

// echo "<pre>**".print_r($targetDay,1)."</pre>";
$targetDate = date("Ymd");
if($_GET["txtse"] == date("Y-m")){
	$target = $targetDay[$targetDate];
	$targetReAssign = $targetReAssign[$targetDate];
}else{
	$target = 650000;
	$targetReAssign = 650000;
}
?>
<div class="main">
	<div class="">
		<div class="p20">
				<div class="row">
					<div class="col-md-12">
						<div class="panel">
								<div class="panel-heading">
									<h4 class="panel-title"><b>Commission New เดือน <?php echo date("Y F", strtotime($_GET["txtse"]."-01"));?></b></h4>
								</div>
								<div class="panel-body fs14">
									<div class="row">
										<div class="col-md-4 ">
											<span class="fs18 cff2da5 mr15">เดือน</span>
											<input type="hidden" value="<?php echo $_GET["txtse"]; ?>" id="txtse">
											<select name="mounth" id="mounthOpt" class="form-control formInput2 dib" style="width: 80%" >
												<option value="">:: กรุณาเลือก ::</option>
													<?php 
														$dateLast = ( date("Y") - 1)."-".date("m");
														for($i=1; $i<=12; $i++) { 
															$txtM = "+".$i." month";
															$dateForValue = date("Y-m",strtotime($txtM, strtotime($dateLast)));
													?>
														<option value="<?php echo $dateForValue ;?>" <?php if($_GET["txtse"] ==  $dateForValue){ echo "selected"; } ?>>
															<?php echo date("Y m F",strtotime($dateForValue)) ?>
														</option>
													<?php } ?>
											</select>
										</div>
										<div class="col-md-4 ">
											<span class="fs18 cff2da5 mr15">ทีม</span>
											<select name="Department" id="Department" class="form-control formInput2 dib" style="width: 80%" >
												<option value="">:: กรุณาเลือก ::</option>
												<?php foreach ($getDepartment as $key => $value) {?>
													<option value="<?php echo $value["Department_ID"];?>" <?php if($_GET["depar"] ==  $value["Department_ID"]){ echo "selected"; } ?>>
														<?php echo $value["Department_Name"]." | ".$value["Remark"];?>
													</option>
												<?php } ?>
											</select>
										</div>
									</div>
									<div class="row mt20">
										<div class="col-md-12">
											<div class="row">
												<div class="col-md-2"></div><div class="col-md-10"><?php echo $value["PO_ID"]; ?></div>
											</div>
											<?php if($getDataCount){?>
												<table class="table table-hover table-bordered" id="indextable" >
													<thead>
														<tr class="bgbaf4bc">
															<th class="t_c">รหัสพนักงาน</th>
															<th class="t_c">ทีม</th>
															<th class="t_c">ชื่อ-นามสกุล</th>
															<th class="t_c"><a href="javascript:SortTable(4,'N');">งานใหม่</a></th>
															<th class="t_c"><a href="javascript:SortTable(5,'N');">งานต่ออายุ</a></th>
															<th class="t_c"><a href="javascript:SortTable(6,'N');">3M</a></th>
															<th class="t_c"><a href="javascript:SortTable(7,'N');">Paid</a></th>
															<th class="t_c"><a href="javascript:SortTable(8,'N');">รวม</a></th>
															<th class="t_c"><a href="javascript:SortTable(9,'N');">ยอดคิดค่า Com</a></th>
															<th class="t_c"><a href="javascript:SortTable(12,'N');">% ส่วนเกิน</a></th>
															<th class="t_c"><a href="javascript:SortTable(10,'N');">% Commission</a></th>
															<th class="t_c">Comm Start</th>
															<th class="t_c"><a href="javascript:SortTable(12,'N');">ค่าคอมฯ</a></th>
														</tr>
													</thead>
													<tbody >
														<?php 
															$sumAll = 0;
															foreach ($getDataCount as $key => $value) {
																$sumAll = $value["sumNew"] + $value["sumRenew"] + $value["sumPO3M"] + $value["sumPV"]; 
																$totalsumNew +=  $value["sumNew"];
																$totalsumRenew +=  $value["sumRenew"];
																$totalsumAll +=  $sumAll;
																$totalsumPO3M +=  $value["sumPO3M"];
																$totalsumPV +=  $value["sumPV"];

																// $sumAll = 550000;
																if($sumAll >= 400000){
																	$commCost = 400000;
																	$CommissionTotal = $sumAll - $commCost ; 
																	$CommissionSurplus = ($CommissionTotal / $commCost) * 100 ; 
																	$Percomm = ($CommissionSurplus < 100) ? ( $CommissionSurplus * 0.03 ) + 3: 6 ; 
																	$CommSum = ($CommissionTotal * $Percomm ) / 100 ;
																}else{
																	$CommissionTotal = 0; 
																	$CommissionSurplus = 0 ; 
																	$Percomm = 0 ; 
																	$CommSum =0;
																}

																$allCommissionTotal += $CommissionTotal;
																$allCommSum += $CommSum;

													

															if($sumAll){
														?>
															<tr>
																<td class="t_c"><?php echo $value["Employee_ID"]; ?></td>
																<td class="t_c"><?php echo $value["Department_Name"]; ?></td>
																<td class="t_l"><?php echo $value["User_FName"]." ".$value["User_LName"]; ?></td>
																<td class="t_r fwb "><?php echo number_format($value["sumNew"],2); ?></td>
																<td class="t_r fwb "><?php echo number_format($value["sumRenew"],2); ?></td>
																<td class="t_r fwb "><?php echo number_format($value["sumPO3M"],2); ?></td>
																<td class="t_r fwb "><?php echo number_format($value["sumPV"],2); ?></td>
																<td class="t_r fwb cff2da5"><?php echo number_format($sumAll,2); ?></td>
																<td class="t_r fwb c2457ff"><?php echo number_format($CommissionTotal,2); ?></td>
																<td class="t_r fwb "><?php echo number_format($CommissionSurplus,2); ?></td>
																<td class="t_r fwb "><?php echo number_format($Percomm,2); ?></td>
																<td class="t_r fwb ">3</td>
																<td class="t_r fwb c00ac0a"><?php echo number_format($CommSum,2); ?></td>
															</tr>
															<?php }
															} ?>
													</tbody>
													<tfoot>
														<tr class="fwb fs16 bgfffbd8">
															<td class="t_c" colspan="3">รวม</td>
															<td class="t_r fwb "><?php echo number_format($totalsumNew,2); ?></td>
															<td class="t_r fwb "><?php echo number_format($totalsumRenew,2); ?></td>
															<td class="t_r fwb "><?php echo number_format($totalsumPO3M,2); ?></td>
															<td class="t_r fwb "><?php echo number_format($totalsumPV,2); ?></td>
															<td class="t_r fwb cff2da5"><?php echo number_format($totalsumAll,2); ?></td>
															<td class="t_r fwb c2457ff"><?php echo number_format($allCommissionTotal,2); ?></td>
															<td class="t_c" colspan="3"></td>
															<td class="t_r fwb c00ac0a"><?php echo number_format($allCommSum,2); ?></td>
													</tfoot>
												</table>
											<?php } ?>
										</div>
									</div>
								</div>
						</div>
					</div>
				</div>
		</div>
	</div>
</div>
<?php include "include/inc_footer.php"; ?> 
<?php include "include/inc_function_tablesort.php"; ?>
<script type="text/javascript">

	$("#mounthOpt").on('change', function() {
		if(this.value){
			window.location.href = "report_commis.php?txtse="+this.value;
		}

  });

  $("#Department").on('change', function() {
  	txtse = $("#txtse").val();
		if(this.value){
			window.location.href = "report_commis.php?txtse="+txtse+"&depar="+this.value;
		}else{
			window.location.href = "report_commis.php?txtse="+txtse;
		}

  });

<?php if($_GET["txtse"]){?>
function getDetailPO(user, date){
	txtse = $("#txtse").val();
	if(txtse){
		window.location.href = "report_commis.php?txtse="+txtse+"&user="+user+"&date="+date;
	}
}
<?php } ?>
  


function random_rgba() {
    var o = Math.round, r = Math.random, s = 255;
    return 'rgba(' + o(r()*s) + ',' + o(r()*s) + ',' + o(r()*s) + ',' + r().toFixed(2) + ')';
}


</script>



