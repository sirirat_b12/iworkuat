<?php 

session_start();
if(!isset($_SESSION["User"]['UserCode'])){
	echo "<script>alert('กรุณาเข้าสู่ระบบ!!!');</script>";
	echo '<META http-equiv="refresh" content="0;URL=login.php">';
	exit();
}

if(!$_GET["pocode"] && !$_GET["numcode"] && $_SESSION["User"]['type'] == "QualityControl" && $_SESSION["User"]['type'] == "CallCenter"){
	echo '<META http-equiv="refresh" content="0;URL=sendorders.php">';
	exit();
}

header('Content-Type: text/html; charset=utf-8');
include "include/inc_header.php"; 
include "include/inc_menu.php"; 

$pocode = $_GET["pocode"];
$numcode = $_GET["numcode"];
$getPO = getPurchaseBycode($pocode);
$Cardetail = getCardetailBycode($pocode);
// $getCommentlist = getCommentlist();
$getIR = getInsuranceRequest($pocode);
$getNoti = getNotiWorkByID($numcode); 
$personnel = getRowpersonnelByCode($getNoti["personnel_code"]);
$getFile = getNotiWorkFilesByID($numcode);
// $getCheck = getNotiWorkCheck($numcode, $pocode);
// $datakeyword = unserialize($getCheck["keyword"]);
// $datadata = unserialize($getCheck["data"]);
// $datachecklists = unserialize($getCheck["checklists"]);
// $datacomments = unserialize($getCheck["comments"]);
$getCoverageItem = getCoverageItem($pocode);
$getPolicyDelivery = getPolicyDelivery($pocode);
$getPremium = getPremium();
$companyIns = getCompanyBysrv();
$getInsuranceType = getInsuranceType();
$getNotiComment = getNotiComment($numcode);
// $getCommenStatus = getCommenStatus();


$addressCus =  $getPO["cusAddr1"]."<br><b>ตำบล/แขวง </b>".getSubdistrictBycode($getPO["cusSubdistrict"])."<b> อำเภอ/เขต </b>".getDistrictBycode($getPO["cusDistrict"])."<br><b> จังหวัด </b>".getProvinceBycode($getPO["cusProvince"])." <b>รหัสไปรษณีย์</b> ".$getPO["cusPost_Code"];
$addressCussend =  $getPO["cussendReceiver_Name"]."<br>".$getPO["cussendAddr1"]."<br><b>ตำบล/แขวง </b>".getSubdistrictBycode($getPO["cussendSubdistrict"])."<b> อำเภอ/เขต </b>".getDistrictBycode($getPO["cussendDistrict"])."<br><b> จังหวัด </b>".getProvinceBycode($getPO["cussendProvince"])." <b>รหัสไปรษณีย์</b> ".$getPO["cussendPost_Code"];
$addressPOsend =  $getPO["Receiver_Name"]."<br>".$getPO["posendAddr1"]."<br><b>ตำบล/แขวง </b>".getSubdistrictBycode($getPO["posendSubdistrict"])."<b> อำเภอ/เขต </b>".getDistrictBycode($getPO["posendDistrict"])."<br><b> จังหวัด </b>".getProvinceBycode($getPO["posendProvince"])." <b>รหัสไปรษณีย์</b> ".$getPO["posendPost_Code"];


// echo "<pre>".print_r($getPO,1)."</pre>";
$arrFile = array('carleft', 'carright', 'carfront', 'carback', 'carbook', 'cardriver', 'carall1','carall2','carall3','carall4', 'company', 'companycard', 'reCheck1', 'reCheck2', 'reCheck3', 'reCheck4', 'reCheck5', 'reCheck6');
?> 
<div class="main">
<div class="main-content p20">
<form action="include/inc_action_chk.php" method="POST" id="form_send_order" enctype="multipart/form-data">
<input type="hidden" name="action" id="action" value="sendUpdatelistchk">
<input type="hidden" name="noti_id" id="noti_id" value="<?php echo $getNoti["noti_work_id"]; ?>">
<input type="hidden" name="pocode" id="pocode" value="<?php echo $pocode; ?>">
<input type="hidden" name="numcode" id="numcode" value="<?php echo $numcode; ?>">
<input type="hidden" name="insurer" id="insurer" value="<?php echo $getNoti["insuere_company"]; ?>">
<div class="p20">
<div class="row">
<div class="col-md-12">
<div class="panel">
<div class="panel-heading">
<h4 class="panel-title"><b>ข้อมูลแจ้งงาน</b></h4>
<span class="fs12"><b>รหัส : </b><?php echo $getNoti["noti_work_code"];?></span>
</div>
<div class="panel-body fs14">
<?php if($getNoti["status"] == 1 || $_SESSION["User"]['type'] == "QualityControl"){ ?>
<div class="col-lg-12">
<div class="row">
	<div class="col-md-6">
		<div class="row">
			<div class="col-lg-4 t_r lablen"><b>PO :</b></div>
			<div class="col-lg-8">
				<input class="form-control formInput2 dib" type="text" name="po_code" id="po_code"  value="<?php echo $getNoti["po_code"];?>" required >
			</div>
		</div>
		<div class="row mt10">
			<div class="col-lg-4 t_r lablen"><b>ประเภทประกัน :</b></div>
			<div class="col-lg-8">
				<input type="radio" name="po_type" id="po_type_p" value="ส่วนบุคคล" <?php if($getNoti["po_type"] =="ส่วนบุคคล"){ echo "checked"; }?> > ส่วนบุคคล | 
				<input class="ml10" type="radio" name="po_type" id="po_type_c" value="บริษัท" <?php if($getNoti["po_type"] =="บริษัท"){ echo "checked"; }?>> บริษัท
			</div>
		</div>
		<div class="row mt10">
			<div class="col-lg-4 t_r lablen"><b>ชื่อลูกค้า :</b></div>
			<div class="col-lg-8">
				<input class="form-control formInput2" type="text" name="cus_name" id="cus_name" value="<?php echo $getNoti["cus_name"];?>" required>
			</div>
		</div>
		<div class="row mt10">
			<div class="col-lg-4 t_r lablen"><b>วันเริ่มคุ้มครอง :</b></div>
			<div class="col-lg-8">
				<input type="date" name="start_cover_date" id="start_cover_date" class="form-control formInput2" value="<?php echo $getNoti["start_cover_date"];?>" required></div>
			</div>
			
			<div class="row mt10">
				<div class="col-lg-4 t_r lablen"><b>ส่วนลดกล้อง :</b></div>
				<div class="col-lg-8">
					<input type="checkbox" id="discount_cctv" name="discount_cctv" value="1" <?php if($getNoti["discount_cctv"] == 1){ echo "checked"; }?>> 
				</div>
			</div>
			<div class="row mt10">
				<div class="col-lg-4 t_r lablen"><b>ของแถม :</b></div>
				<div class="col-lg-8">
					<select class="form-control formInput2 fs12" name="giftvoucher" id="giftvoucher" >
						<option value="0">กรุณาเลือก</option>
						<?php foreach ($getPremium as $key => $value) { ?>
							<option value="<?php echo  $value["Premium_Desc"]; ?>" <?php echo ($getNoti["giftvoucher"] == $value["Premium_Desc"])? "selected" : "";?>>
								<?php echo  $value["Premium_Desc"]; ?>
							</option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="row mt10">
				<div class="col-lg-4 t_r lablen"><b>ประเภท PO</b></div>
				<div class="col-lg-8">
					<select name="operation_type" id="operation_type" class="form-control formInput2" >
						<option value="">:: กรุณาเลือก ::</option>
						<option value="งานใหม่" <?php echo ($getNoti["operation_type"] == "งานใหม่")? "selected" : "";?>>งานใหม่</option>
						<option value="งานต่ออายุ" <?php echo ($getNoti["operation_type"] == "งานต่ออายุ")? "selected" : "";?>>งานต่ออายุ</option>
						<option value="งานโอนโค้ด" <?php echo ($getNoti["operation_type"] == "งานโอนโค้ด")? "selected" : "";?>>งานโอนโค้ด</option>
						<option value="ต่ออายุเปลี่ยนประกัน" <?php echo ($getNoti["operation_type"] == "ต่ออายุเปลี่ยนประกัน")? "selected" : "";?>>ต่ออายุเปลี่ยนประกัน</option>
					</select>
				</div>
			</div>
			<div class="row mt10">
				<div class="col-lg-4 t_r lablen"><b>หมายเหตุ :</b></div>
				<div class="col-lg-8">
					<textarea name="notes" id="notes" style="width: 100%; font-size: 12px; height: 100px;" ><?php echo $getNoti["notes"];?></textarea>
				</div>
			</div>
			<div class="row mt10">
				<div class="col-lg-4 t_r lablen"><b>แพจเกต :</b></div>
				<div class="col-lg-8">
					<input class="form-control formInput2" type="hidden" name="package_id" id="packageID" value="<?php echo $getNoti["package_id"];?>"  >
					<input class="form-control formInput2" type="text" name="package_name" id="packageName" value="<?php echo $getNoti["package_name"];?>" >
				</div>
			</div>
			<div class="row mt10">
				<div class="col-lg-4 t_r lablen"><b>ประกันชั้น :</b></div>
				<div class="col-lg-8">
					<select class="form-control formInput2 fs12" name="insurance_type" id="InsuranceType" >
						<option value="0">กรุณาเลือก</option>
						<?php foreach ($getInsuranceType as $key => $value) { ?>
							<option value="<?php echo  $value["Insurance_Name"]; ?>" <?php echo ($getNoti["insurance_type"] == $value["Insurance_Name"])? "selected" : "";?>><?php echo  $value["Insurance_Name"]; ?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="row mt10">
				<div class="col-lg-4 t_r lablen"><b>บริษัทประกัน :</b></div>
				<div class="col-lg-8">
					<select id="insuere_company" class="form-control formInput2 formInput" name="insuere_company" >
						<option value="">กรุณาเลือก</option>
						<?php foreach ($companyIns as $compa) { ?>
							<option value="<?php echo $compa["Insurer_Initials"];?>" <?php echo ($getNoti["insuere_company"] == $compa["Insurer_Initials"])? "selected" : "";?>>
								<?php echo $compa["Insurer_Name"];?></option>
							<?php } ?>
						</select>
					</div>
				</div>
				<div class="row mt10">
					<div class="col-lg-4 t_r lablen"><b>ทุนประกัน :</b></div>
					<div class="col-lg-8 t_r">
						<input class="form-control formInput2" type="text" name="insuere_cost" id="insuere_cost" value="<?php echo $getNoti["insuere_cost"];?>" required>
					</div>
				</div>
				<div class="row mt10">
					<div class="col-lg-4 t_r lablen"><b>เบี้ยสุทธิ :</b></div>
					<div class="col-lg-8 t_r">
						<input class="form-control formInput2 t_r" type="text" name="netpremium" id="netpremium" value="<?php echo $getNoti["netpremium"];?>" required>
					</div>
				</div>
				<div class="row mt10">
					<div class="col-lg-4 t_r lablen"><b>เบี้ยรวมภาษี :</b></div>
					<div class="col-lg-8 t_r">
						<input class="form-control formInput2 t_r" type="text" name="premium" id="premium" value="<?php echo $getNoti["premium"];?>" required>
					</div>
				</div>
				<div class="row mt10">
					<div class="col-lg-4 t_r lablen"><b>ส่วนลด :</b></div>
					<div class="col-lg-8 t_r">
						<input class="form-control formInput2 t_r" type="text" name="discount" id="discount" value="<?php echo $getNoti["discount"];?>" >
					</div>
				</div>
				<div class="row mt10">
					<div class="col-lg-4 t_r lablen"><b>รวม :</b></div>
					<div class="col-lg-8 t_r">
						<input class="form-control formInput2 t_r" type="text" name="taxamount" id="taxamount" value="<?php echo $getNoti["taxamount"];?>" >
					</div>
				</div>

				<div class="row mt10">
					<div class="col-lg-4 t_r lablen"><b>สลักหลัง :</b></div>
					<div class="col-lg-8">
						<input type="checkbox" id="endorse" name="endorse" value="1" <?php if($getNoti["endorse"] == 1){ echo "checked"; }?>> 
					</div>
				</div>
				<div class="row mt10">
					<div class="col-lg-4 t_r lablen"><b>ผู้แจ้งงาน :</b></div>
					<div class="col-lg-4"><?php echo $getNoti["personnel_code"]." ". $getNoti["personnel_name"];?></div>
				</div>
				<div class="row mt10">
					<div class="col-lg-4 t_r lablen"><b>สถานะ :</b></div>
					<div class="col-lg-4"><?php echo setStatus($getNoti["status"]);?></div>
				</div>
				<div class="row mt10">
					<div class="col-lg-4 t_r lablen"><b>การใช้งาน :</b></div>
					<div class="col-lg-4"><?php echo ($getNoti["enable"] == 1) ? "ใช้งาน" : "ปิดใช้งาน" ; ?></div>
				</div>
				<div class="row mt10">
					<div style="background-color: #ffffed; padding: 10px;">
						<p class="cff0000"><b>หมายเหตุ</b></p>
						<div id="listMent">
							<?php foreach ($getNotiComment as $key => $value) { ?>
								<div style="color: #b0b0b0;font-size: 11px;"> <span ><?php echo $value["datetime"] ?></span> | <?php echo $value["comment_byname"] ?> </div>
								<div class="ml10 fs12 c1641ff"> >>> <?php echo $value["comment"] ; ?></div>
							<?php } ?>
						</div>
						<?php if($_SESSION["User"]['type'] == "Admin" || $_SESSION["User"]['type'] == "SuperAdmin"){ ?>
							<div class="mt15">
								<input type="text"  name="comment" id="comment"  class="form-control formInput fs12 mb5">
								<input type="button" class="btn btn-info" value="send"   onclick="btnComments('<?php echo $numcode; ?>')">
							</div>
						<?php } ?>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<?php $num = 1;
				foreach ($arrFile as $key => $value) {
					$getImageEdit =  getImageEdit($getNoti["noti_work_code"],$value); 
					$path  = ($getImageEdit["pathfile"]) ? $getImageEdit["pathfile"]."/" : "";
					?>
					<div class="col-md-4 mt15" style="height: 150px;">
						<div class="fl fs12"><b><?php echo caseTitle("file".$value); ?></b></div>
						<?php if($getImageEdit){ ?>
							<div class="col-md-4 clearb">
								<?php 
								if($getImageEdit["name_types"] !== "pdf"){ 
									echo '<a href="myfile/'.$path.''.$getImageEdit["file_name"].'" data-fancybox="watermark" class="fancybox">
									<img src="myfile/'.$path.''.$getImageEdit["file_name"].'" alt="" class="img-responsive rounded" style="height: 120px;max-width: 120px;">
									</a>';
								}else{
									echo '<a href="myfile/'.$path.''.$getImageEdit["file_name"].'" data-fancybox="watermark" class="fancybox">
									<i class="fa fa-file" style="font-size:  50px;color: #555;"></i></a>';
								} ?>
								<div class="fs12 cursorPoin cff0000" onclick="delfile('<?php echo $getImageEdit["noti_work_files_id"];?>','<?php echo $path.''.$getImageEdit["file_name"];?>');">
									<i class="fa fa-trash"></i> ลบไฟล์
								</div>

							</div>
						<?php } ?>
						<div class="col-md-8 clearb">
							<?php if(!$getImageEdit){ ?>
								<input type="file" name="filUpload[<?php echo $value;?>]" id="fileUp_<?php echo $num++;?>" class="fl form-control inputFile" 
								accept="image/gif, image/jpeg, .pdf" onchange="ValidateSingleInput(this);">
							<?php } ?>
						</div>
					</div>
				<?php } ?>
			</div>
		</div>
		<div class="row t_c mt20">
			<span class="btn btn-warning" id="btn_send_order">ดึงข้อมูลใหม่</span>
			<input type="submit" class="btn btn-info" value="บันทึกข้อมูล" >
			<span class="btn btn-success" onclick="btnChangStatus('<?php echo $getNoti["noti_work_code"];?>','<?php echo $getNoti["admin_code"];?>')">ส่งตรวจอีกครั้ง</span>
			<input type="reset" class="btn btn-danger" value="ยกเลิก" >
		</div>
	</div>
<?php }else{ ?>
	<div class="row mb20">
		<div class="col-md-3">
			<div class="row">
				<div class="col-lg-4 t_r"><b>PO :</b></div>
				<div class="col-lg-8"><?php echo $getNoti["po_code"];?></div>
			</div>
			<div class="row mt10">
				<div class="col-lg-4 t_r"><b>ประเภทประกัน :</b></div>
				<div class="col-lg-8"> <?php echo ($getNoti["po_type"] == "บริษัท")? "บริษัท" : "ส่วนบุคคล" ;?></div>
			</div>
			<div class="row mt10">
				<div class="col-lg-4 t_r"><b>ชื่อลูกค้า :</b></div>
				<div class="col-lg-8"><?php echo $getNoti["cus_name"]; ?></div>
			</div>
			<div class="row mt10">
				<div class="col-lg-4 t_r"><b>วันเริ่มคุ้มครอง :</b></div>
				<div class="col-lg-8"><?php echo $getNoti["start_cover_date"]; ?></div>
			</div>

			<div class="row mt10">
				<div class="col-lg-4 t_r"><b>ส่วนลดก้อง :</b></div>
				<div class="col-lg-8"><?php echo ($getNoti["discount_cctv"] == 1) ? "มีส่วนลเกล้อง" : "ไม่มี" ;?></div>
			</div>
			<div class="row mt10">
				<div class="col-lg-4 t_r"><b>ของแถม :</b></div>
				<div class="col-lg-8"> <?php echo $getNoti["giftvoucher"]; ?></div>
			</div>
			<div class="row mt10">
				<div class="col-lg-4 t_r"><b>บัตรน้ำมันให้ :</b></div>
				<div class="col-lg-8"> <?php echo $getNoti["giftvoucher_po"]; ?></div>
			</div>
			<div class="row mt10">
				<div class="col-md-4 t_r"><b>สลักหลัง :</b></div>
				<div class="col-md-8"><?php echo ($getNoti["endorse"]==1)  ? "แจ้งสลักหลัง" : "-";?></div>
			</div>
			<?php if($getNoti["send_type"] == "พนักงานจัดส่ง"){?>
				<div class="row mt10">
					<div class="col-md-4 t_r"><b>ที่อยู่ส่งเอกสาร :</b></div>
					<div class="col-md-8"><?php echo ($getNoti["send_addr"]) ? $getNoti["send_addr"] : "-";?></div>
				</div>
			<?php } ?>
			<div class="row mt10">
				<div class="col-lg-4 t_r"><b>หมายเหตุ :</b></div>
				<div class="col-lg-8"><?php echo $getNoti["notes"];?></div>
			</div>
		</div>
		<div class="col-md-3">
			<div class="row mt10">
				<div class="col-lg-4 t_r"><b>แพจเกต :</b></div>
				<div class="col-lg-8"><?php echo $getNoti["package_name"];?></div>
			</div>
			<div class="row mt10">
				<div class="col-lg-4 t_r"><b>ประกันชั้น :</b></div>
				<div class="col-lg-8"><?php echo $getNoti["insurance_type"];?> </div>
			</div>
			<div class="row mt10">
				<div class="col-lg-4 t_r"><b>บริษัทประกัน :</b></div>
				<div class="col-lg-8"><?php echo $getNoti["insuere_company"];?></div>
			</div>
			<div class="row mt10">
				<div class="col-lg-4 t_r"><b>ทุนประกัน :</b></div>
				<div class="col-lg-8 t_r"><?php echo number_format($getNoti["insuere_cost"],2);?> <b>บาท</b></div>
			</div>
			<div class="row mt10">
				<div class="col-lg-4 t_r"><b>เบี้ยสุทธิ :</b></div>
				<div class="col-lg-8 t_r"><?php echo number_format($getNoti["netpremium"],2);?> <b>บาท</b></div>
			</div>
			<div class="row mt10">
				<div class="col-lg-4 t_r"><b>เบี้ยรวมภาษี :</b></div>
				<div class="col-lg-8 t_r"><?php echo number_format($getNoti["premium"],2);?> <b>บาท</b></div>
			</div>
			<div class="row mt10">
				<div class="col-lg-4 t_r"><b>ส่วนลด :</b></div>
				<div class="col-lg-8 t_r"><?php echo number_format($getNoti["discount"],2);?> <b>บาท</b></div>
			</div>
			<div class="row mt10">
				<div class="col-lg-4 t_r"><b>รวม :</b></div>
				<div class="col-lg-8 t_r"><?php echo number_format($getNoti["taxamount"],2);?> <b>บาท</b></div>
			</div>
		</div>
		<div class="col-md-3">
			<div class="row mt10">
				<div class="col-lg-4 t_r"><b>ผู้แจ้งงาน :</b></div>
				<div class="col-lg-8">
					<div><?php echo $getNoti["personnel_code"]." ". $getNoti["personnel_name"];?></div>
					<div><?php echo $personnel["personnel_email"];?></div>
				</div>
			</div>
			<div class="row mt10">
				<div class="col-lg-4 t_r"><b>สถานะ :</b></div>
				<div class="col-lg-4"><?php echo setStatus($getNoti["status"]);?></div>
			</div>
			<div class="row mt10">
				<div class="col-lg-4 t_r"><b>การใช้งาน :</b></div>
				<div class="col-lg-4"><?php echo ($getNoti["enable"] == 1) ? "ใช้งาน" : "ปิดใช้งาน" ; ?></div>
			</div>
			<?php if($getNoti["insurance_type"] != "พรบ." || $getNoti["status"] != 1){ ?>
				<div class="row mt10">
					<div class="col-lg-4 t_r"><b>ใบคำขอ :</b></div>
					<div class="col-lg-4">
						<?php if($getIR["Request_ID"]){ ?>
							<a href="export_request.php?pocode=<?php echo $pocode;?>" target="_bank" class="cff2da5"><?php echo $getIR["Request_ID"]; ?></a>
						<?php }else{?>
							<span class="cursorPoin cff2da5" onclick="addRequest()"><i class="fa fa-file-pdf-o fwb fs14" aria-hidden="true"></i> สร้าง</span>
						<?php } ?>
					</div>
				</div>
			<?php } ?>

			<?php if($_SESSION["User"]['type'] == "Admin" || $_SESSION["User"]['type'] == "SuperAdmin"){ ?>

				<div class="row mt30 t_c">
					<?php if($getNoti["status"] != 1){ ?>
						<span class="fs14 btnselecttitle btn btn-danger" title="ตีกลับ" onclick="changStatus('<?php echo $getNoti["noti_work_id"]?>','<?php echo $numcode; ?>',1);">ตีกลับ</span>  
					<?php }if($getNoti["status"] != 3){ ?>
						<span class="fs14 btnselecttitle btn btn-primary" title="กำลังแจ้งงาน" onclick="changStatus('<?php echo $getNoti["noti_work_id"]?>','<?php echo $numcode; ?>',3);">แจ้งงาน</span>
					<?php }if($getNoti["status"] != 4){ ?>
						<span class="fs14 btnselecttitle btn btn-success" title="แจ้งงานเสร็จสิ้น"onclick="changStatus('<?php echo $getNoti["noti_work_id"]?>','<?php echo $numcode; ?>',4);">เสร็จสิ้น</span>
					<?php } ?>
					<a href="admin_email.php?code=<?php echo $numcode; ?>" target="_blak" style="color: #FF5722;" class="fs14 btnselecttitle btn btn-warning" >ส่งเมล์</a>
					<?php	if($getNoti["insuere_company"]=='ไทยวิวัฒน์') { ?>
						<br><br><span class="fs14 btnselecttitle btn" title="web service" onclick="window.open('http://61.90.142.230/webservice/tvi/index.php?PO_ID=<?php echo $_GET['pocode']; ?>')">TVI Web Service</span>
					<?php } ?>
				</div>
			<?php } ?>
		</div>
		<div class="col-md-3">
			<div style="background-color: #ffffed; padding: 10px;">
				<p class="cff0000"><b>หมายเหตุ</b></p>
				<div id="listMent">
					<?php foreach ($getNotiComment as $key => $value) { ?>
						<div style="color: #b0b0b0;font-size: 11px;"> <span ><?php echo $value["datetime"] ?></span> | <?php echo $value["comment_byname"] ?> </div>
						<div class="ml10 fs12 c1641ff"> >>> <?php echo $value["comment"] ; ?></div>
					<?php } ?>
				</div>
				<?php if($_SESSION["User"]['type'] == "Admin" || $_SESSION["User"]['type'] == "SuperAdmin"){ ?>
					<div class="mt15">
						<input type="text"  name="comment" id="comment"  class="form-control formInput fs12 mb5">
						<input type="button" class="btn btn-info" value="send"   onclick="btnComments('<?php echo $numcode; ?>')">
					</div>
				<?php } ?>
			</div>
		</div>
	</div>
<?php } ?>
<?php

if($_SESSION["User"]['type'] != "Sale"){
	//$sql_tvi ="SELECT  TOP 1 * from [dbo].[webservice_tvi]  WHERE 1=1 and RE_PolicyNo!='' and  [dbo].[webservice_tvi].[PO_ID] = '".$_GET['pocode']."' ORDER BY DATE_update DESC";
	$sql_tvi ="SELECT  TOP 1 * from [dbo].[webservice_tvi]  WHERE  [dbo].[webservice_tvi].[PO_ID] = '".$_GET['pocode']."' ORDER BY DATE_save DESC ";
	$stmt_tvi = sqlsrv_query( $connMS, $sql_tvi );
	$row_tvi = sqlsrv_fetch_array($stmt_tvi) ;
	//print_r($row_tvi);
	$find_xox = "testws-ssl";
	if(!strpos($row_tvi['PDF_URL'],$find_xox)) {  
		if($row_tvi['PDF_URL']!=""){
			echo '<br><b>Policy No. : </b><span style="color:orange;"><b>'.$row_tvi['RE_PolicyNo']."</b></span>";
			echo htmlspecialchars_decode($row_tvi['PDF_URL']);
		} else { echo '<br><b>Error  : </b><span style="color:orange;"><b>'.$row_tvi['RE_ErrorMessage']."</b></span>";  }
	} 
} 
?>									
									</div>
								</div>

							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="panel">
										<div class="panel-heading">
											<h4 class="panel-title"><b>ข้อมูลลูกค้า</b></h4>
											<div class="right">
												<button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
											</div>
										</div>
										<div class="panel-body fs12">
											<div class="row">
												<table class="table table-hover table-striped table-bordered">
													<thead>
														<tr>
															<th class="t_r" style="width: 25%;">รายการ</th>
															<th>ข้อมูล</th>
														</tr>
													</thead>
													<tbody>
														<tr>
															<td class="t_r"><b>รหัส : </b></td>
															<td class="c1641ff">
																<?php echo $getPO["Customercode"];?>
																<input type="hidden" name="check[Customercode][data]" value="<?php echo $getPO["Customercode"];?>">
															</td>
														</tr>
														<tr>
															<td class="t_r"><b>การดำเนินการ : </b></td>
															<?php 
																$Reference = ($getPO["Reference_ID"]) ? " | ".$getPO["Reference_ID"] : "";
																$Operation = getOperationType($getPO["Operation_Type_ID"])." ".trim($Reference) ;
															?>
															<td class="c1641ff">
																<?php echo $Operation ;?>
																<input type="hidden" name="check[CustomerOperation][data]" value="<?php echo $Operation;?>">
															</td>
														</tr>
														<tr>
															<td class="t_r"><b>ประเภทลูกค้า : </b></td>
															<td class="c1641ff">
																<?php echo ($getPO["cus_type"] == "P") ? "Personal" : "Corporate";?>
																<input type="hidden" name="check[Customertype][data]" value="<?php echo ($getPO["cus_type"] == "P") ? "Personal" : "Corporate";?>">
															</td>
														</tr>
														<tr>
															<td class="t_r"><b>ชื่อ-นามสกุล TH : </b></td>
															<td class="c1641ff">
																<?php echo $getPO["Title_Name"]." ".$getPO["Customer_FName"]." ".$getPO["Customer_LName"];?>
																<input type="hidden" name="check[Customernameth][data]" value="<?php echo $getPO["Title_Name"]." ".$getPO["Customer_FName"]." ".$getPO["Customer_LName"] ;?> ">
															</td>
														</tr>
														<tr>
															<td class="t_r"><b>เลข ปชช. / เลขผู้ประกอบการ : </b></td>
															<td class="c1641ff">
																<?php echo $getPO["Customer_Folder"] ? $getPO["Customer_Folder"] : "-" ;?>
																<input type="hidden" name="check[cusFolder][data]" value="<?php echo $getPO["Customer_Folder"] ? $getPO["Customer_Folder"] : "" ;?>">
															</td>
														</tr>
														<tr>
															<td class="t_r"><b>ชื่อผู้ติดต่อ : </b></td>
															<td class="c1641ff">
																<?php echo $getPO["CustomerPerson"] ? $getPO["CustomerPerson"] : "" ;?>
																<input type="hidden" name="check[cusPerson][data]" value="<?php echo $getPO["CustomerPerson"] ? $getPO["CustomerPerson"] : "-" ;?>">
															</td>
														</tr>
														<tr>
															<td class="t_r"><b>โทรศัพท์ : </b></td>
															<td class="c1641ff">
																<?php echo $getPO["CustomerTel"] ? $getPO["CustomerTel"] : "-" ;?>
																<input type="hidden" name="check[cusTel][data]" value="<?php echo $getPO["CustomerTel"] ? $getPO["CustomerTel"] : "" ;?>">
															</td>
														</tr>
														<tr>
															<td class="t_r"><b>โทรสาร : </b></td>
															<td class="c1641ff">
																<?php echo $getPO["CustomerFax"] ? $getPO["CustomerFax"] : "-" ;?>
																<input type="hidden" name="check[cusFax][data]" value="<?php echo $getPO["CustomerFax"] ? $getPO["CustomerFax"] : "" ;?>">
															</td>
														</tr>
														<tr>
															<td class="t_r"><b>มือถือ : </b></td>
															<td class="c1641ff">
																<?php echo $getPO["CustomerMobile_No"] ? $getPO["CustomerMobile_No"] : "-" ;?>
																<input type="hidden" name="check[cusMobile][data]" value="<?php echo $getPO["CustomerMobile_No"] ? $getPO["CustomerMobile_No"] : "" ;?>">
																</td>
														</tr>
														</tr>
														<tr>
															<td class="t_r"><b>eMail : </b></td>
															<td class="c1641ff">
																<?php echo $getPO["EMail"] ? $getPO["EMail"] : "-" ;?>
																<input type="hidden" name="check[cusEmil][data]" value="<?php echo $getPO["EMail"] ? $getPO["EMail"] : "" ;?>">
															</td>
														</tr>
														</tr>
														<tr>
															<td class="t_r"><b>Mobile I : </b></td>
															<td class="c1641ff">
																<?php echo $getPO["Mobile_I"]? $getPO["Mobile_I"] : "-";?>
																<input type="hidden" name="check[cusMobileI][data]" value="<?php echo $getPO["Mobile_I"] ? $getPO["Mobile_I"] : "" ;?>">
															</td>
														</tr>
														<tr>
															<td class="t_r"><b>Mobile II : </b></td>
															<td class="c1641ff">
																<?php echo $getPO["Mobile_II"]? $getPO["Mobile_II"] : "-";?>
																<input type="hidden" name="check[cusMobileII][data]" value="<?php echo $getPO["Mobile_II"] ? $getPO["Mobile_II"] : "" ;?>">
															</td>
														</tr>
														<tr>
															<td class="t_r"><b>Land Line : </b></td>
															<td class="c1641ff">
																<?php echo $getPO["Land_Line"]? $getPO["Land_Line"] : "-";?>
																<input type="hidden" name="check[cusLand][data]" value="<?php echo $getPO["Land_Line"] ? $getPO["Land_Line"] : "" ;?>">
															</td>
														</tr>
														<tr>
															<td class="t_r"><b>Line ID : </b></td>
															<td class="c1641ff">
																<?php echo $getPO["Line_ID"]? $getPO["Line_ID"] : "-";?>
																<input type="hidden" name="check[cusLind][data]" value="<?php echo $getPO["Line_ID"] ? $getPO["Line_ID"] : "" ;?>">
															</td>
														</tr>
														<tr>
															<td class="t_r"><b>ที่อยู่ : </b></td>
															<td class="c1641ff">
																<?php echo $addressCus; ?>
																<input type="hidden" name="check[cusAddr][data]" value="<?php echo $addressCus ;?>">
															</td>
														</tr>
														<tr>
															<td class="t_r"><b>รายการเอาประกัน : </b></td>
															<td class="c1641ff">
																<?php echo $getPO["Properties_Insured"]; ?>
																<input type="hidden" name="check[cusProperties][data]" value="<?php echo $getPO["Properties_Insured"] ;?>">
															</td>
														</tr>
														<tr>
															<td class="t_r"><b>ที่อยู่ในการส่งเอกสาร : </b></td>
															<td class="c1641ff">
																<?php echo $addressCussend; ?>
																<input type="hidden" name="check[cusAddSend][data]" value="<?php echo $addressCussend ;?>">
															</td>
														</tr>
														<tr>
															<td class="t_r"><b>ประเภทการจัดส่ง : </b></td>
															<td class="c1641ff">
																<?php echo $getPolicyDelivery["Delivery_Type_Desc"];?>
																<input type="hidden" name="check[DeliveryTypeDesc][data]" value="<?php echo $getPolicyDelivery["Delivery_Type_Desc"] ;?>">
															</td>
														</tr>
														<?php if($getPolicyDelivery["Delivery_Type_ID"] == "002"){ ?>
														<tr>
															<td class="t_r"><b>พนักงานส่ง : </b></td>
															<td class="c1641ff">
																<?php echo $getPolicyDelivery["Messenger_Name"];?>
																<input type="hidden" name="check[MessengerName][data]" value="<?php echo $getPolicyDelivery["Messenger_Name"] ;?>">
															</td>
														</tr>
														<tr>
															<td class="t_r"><b>สถานที่พนักงานส่ง : </b></td>
															<td class="c1641ff">
																<?php echo $getPolicyDelivery["Delivery_Place"];?>
																<input type="hidden" name="check[DeliveryPlace][data]" value="<?php echo $getPolicyDelivery["Delivery_Place"] ;?>">
															</td>
														</tr>
														<?php } ?>
														<tr>
															<td class="t_r"><b>หมายเหตุในการส่งเอกสาร : </b></td>
															<td class="c1641ff">
																<?php echo $getPolicyDelivery["Remark"];?>
																<input type="hidden" name="check[DeliveryRemark][data]" value="<?php echo $getPolicyDelivery["Remark"] ;?>">
															</td>
														</tr>
														<tr>
															<td class="t_r"><b>สถานะการส่ง : </b></td>
															<td class="c1641ff">
																<?php echo $getPolicyDelivery["Delivery_Status_Desc"];?>
																<input type="hidden" name="check[DeliveryStatusDesc][data]" value="<?php echo $getPolicyDelivery["Delivery_Status_Desc"] ;?>">
															</td>
														</tr>
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="panel">
										<div class="panel-heading">
											<h4 class="panel-title"><b>รายละเอียดประกัน</b></h4>
											<div class="right">
												<button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
											</div>
										</div>
										<div class="panel-body fs12">
											<div class="row">
												<table class="table table-hover table-striped table-bordered">
													<thead>
														<tr>
															<th class="t_r" style="width: 15%;">รายการ</th>
															<th>ข้อมูล</th>
														</tr>
													</thead>
													<tbody>
														<tr>
															<td class="t_r"><b>แพจเกต</b></td>
															<td class="c1641ff">
																<?php echo $getPO["Insurance_Package_Name"];?>
																<input type="hidden" name="check[insurPackageName][data]" value="<?php echo  $getPO["Insurance_Package_Name"]; ?>">
															</td>
														</tr>
														<tr>
															<td class="t_r"><b>เริ่มวันคุ้มครอง</b></td>
															<td class="c1641ff">
																<?php echo $getPO["Coverage_Start_Date"]->format('d-m-Y');?>
																<input type="hidden" name="check[insurCoverageStart][data]" value="<?php echo $getPO["Coverage_Start_Date"]->format('d-m-Y');?>">
															</td>
														</tr>
														<tr>
															<td class="t_r"><b>สิ้นสุดวันคุ้มครอง</b></td>
															<td class="c1641ff">
																<?php echo $getPO["Coverage_End_Date"]->format('d-m-Y');?>
																<input type="hidden" name="check[insurCoverageEnd][data]" value="<?php echo $getPO["Coverage_End_Date"]->format('d-m-Y');?>">
															</td>
														</tr>
														<tr>
															<td class="t_r"><b>ประกันชั้น</b></td>
															<td class="c1641ff">
																<?php echo $getPO["Insurance_Name"];?>
																<input type="hidden" name="check[insurTypeIns][data]" value="<?php echo  $getPO["Insurance_Name"]; ?>">
															</td>
														</tr>
														<tr>
															<td class="t_r"><b>บรัษัท</b></td>
															<td class="c1641ff">
																<?php echo $getPO["Insurer_Name"];?>
																<input type="hidden" name="check[insurNameInsur][data]" value="<?php echo  $getPO["Insurer_Name"]; ?>">
															</td>
														</tr>
														<tr>
															<td class="t_r"><b>ทุนประกัน</b></td>
															<td class="c1641ff">
																<?php echo number_format($getPO["insuere_cost"],2);?>
																<input type="hidden" name="check[insurCost][data]" value="<?php echo  $getPO["insuere_cost"]; ?>">
															</td>
														</tr>
														<tr>
															<td class="t_r"><b>เบี้ยสุทธิ</b></td>
															<td class="c1641ff">
																<?php echo number_format($netpremium,2);?>
																<input type="hidden" name="check[insurNetpremium][data]" value="<?php echo  number_format($netpremium,2); ?>">
															</td>
														</tr>
														<tr>
															<td class="t_r"><b>เบี้ยรวมภาษี</b></td>
															<td class="c1641ff">
																<?php echo number_format($getPO["Total_Premium"],2);?>
																<input type="hidden" name="check[insurPremium][data]" value="<?php echo  number_format($getPO["Total_Premium"],2); ?>">
															</td>
														</tr>
														<tr>
															<td class="t_r"><b>ส่วนลด</b></td>
															<td class="c1641ff">
																<?php echo number_format($getPO["Discount"],2);?>
																<input type="hidden" name="check[insurDiscount][data]" value="<?php echo  number_format($getPO["Discount"],2); ?>">
															</td>
														</tr>
														<tr>
															<td class="t_r"><b>เบี้ยรวมภาษี</b></td>
															<td class="c1641ff">
																<?php echo number_format($getPO["Premium_After_Disc"],2);?>
																<input type="hidden" name="check[insurTaxamount][data]" value="<?php echo number_format($getPO["Premium_After_Disc"],2); ?>">
															</td>
														</tr>
														<tr>
															<td class="t_r"><b>คุ้มครอง</b></td>
															<td class="">
																<?php 
																	$contxt = "";
																	foreach ($getCoverageItem as $key => $value) {
																	$covID = $value["Coverage_Item_ID"]; 
																	$Coverage[$covID] = $value["Coverage_Value"] ;
																	
																	$contxt .=	"<div><span>".$value["Coverage_Item_Desc"]."</span>";
																		if($value["Coverage_Value"]){ 
																			$contxt .=	" <span><b> ".$value["Coverage_Value"]."</span> ".$value["Coverage_Item_Unit"]."</b>";
																		} 
																	$contxt .= "</div>";
																} 
																echo $contxt ; 
																?>
																<input type="hidden" name="check[insurCoverageItem][data]" value="<?php echo  $contxt; ?>">
															</td>
														</tr>
														<tr>
															<td class="t_r"><b>ที่อยู่ในการจัดส่งเอกสาร : </b></td>
															<td class="c1641ff">
																<?php echo $addressPOsend ;?>
																<input type="hidden" name="check[carPOAddrSend][data]" value="<?php echo $Cardetail["addressPOsend"] ;?>">
															</td>
														</tr>
														<tr>
															<td class="t_r"><b>หมายเหตุการจัดส่งเอกสาร : </b></td>
															<td class="c1641ff">
																<?php echo $getPO["posendPostRemark"];?>
																<input type="hidden" name="check[insurPOAddrSendRemark][data]" value="<?php echo $getPO["posendPostRemark"] ;?>">
															</td>
														</tr>
														
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="panel">
										<div class="panel-heading">
											<h4 class="panel-title"><b>รายละเอียดรถ</b></h4>
											<div class="right">
												<button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
											</div>
										</div>
										<div class="panel-body fs12">
											<div class="row">
												<table class="table table-hover table-striped table-bordered">
													<thead>
														<tr>
															<th class="t_r" style="width: 30%;">รายการ</th>
															<th>ข้อมูล</th>
														</tr>
													</thead>
													<tbody>
														<tr>
															<td class="t_r"><b>เลขทะเบียน : </b></td>
															<td class="c1641ff">
																<?php echo $Cardetail["Plate_No"]." ".getProvinceBycode($Cardetail["Plate_Province"]);?>
																<input type="hidden" name="check[carPlate][data]" value="<?php echo $Cardetail["Plate_No"]." ".getProvinceBycode($Cardetail["Plate_Province"]);?>">
															</td>
														</tr>
														<tr>
															<td class="t_r"><b>ยี่ห้อรถ : </b></td>
															<td class="c1641ff">
																<?php echo $Cardetail["Make_Name_TH"];?>
																<input type="hidden" name="check[carMake][data]" value="<?php echo $Cardetail["Make_Name_TH"] ;?>">
															</td>
														</tr>
														<tr>
															<td class="t_r"><b>รุ่น : </b></td>
															<td class="c1641ff">
																<?php echo trim($Cardetail["Model_Desc"]);?>
																<input type="hidden" name="check[carModel][data]" value="<?php echo trim($Cardetail["Model_Desc"]) ;?>">
															</td>
														</tr>
														<tr>
															<td class="t_r"><b>รุ่นย่อย/ปี : </b></td>
															<td class="c1641ff">
																<?php echo $Cardetail["Sub_Model"] ? $Cardetail["Sub_Model"] : "-" ;?>
																<input type="hidden" name="check[carsubModel][data]" value="<?php echo $Cardetail["Sub_Model"] ;?>">
															</td>
														</tr>
														<tr>
															<td class="t_r"><b>รหัสรถยนต์ : </b></td>
															<td class="c1641ff">
																<?php echo $Cardetail["Car_Code"] ? $Cardetail["Car_Type_Desc"] : "-" ;?>
																<input type="hidden" name="check[carCodes][data]" value="<?php echo $Cardetail["Car_Type_Desc"] ;?>">
															</td>
														</tr>
														<tr>
															<td class="t_r"><b>ประเภทตัวถัง : </b></td>
															<td class="c1641ff">
																<?php echo $Cardetail["Body_Type"] ? $Cardetail["Body_Type"] : "-" ;?>
																<input type="hidden" name="check[carbodyType][data]" value="<?php echo $Cardetail["Body_Type"] ;?>">
															</td>
														</tr>
														<tr>
															<td class="t_r"><b>เลขตัวถัง : </b></td>
															<td class="c1641ff">
																<?php echo $Cardetail["Frame_No"] ? $Cardetail["Frame_No"] : "-" ;?>
																<input type="hidden" name="check[carFrame][data]" value="<?php echo $Cardetail["Frame_No"] ;?>">
															</td>
														</tr>
														<tr>
															<td class="t_r"><b>อุปกรณ์ตกแต่ง : </b></td>
															<td class="c1641ff">
																<?php echo $Cardetail["Accessory"] ? $Cardetail["Accessory"] : "-" ;?>
																<input type="hidden" name="check[carAccessory][data]" value="<?php echo $Cardetail["Accessory"] ;?>">
															</td>
														</tr>
														<tr>
															<td class="t_r"><b>ที่นั่ง/ซีซี/น้ำหนัก : </b></td>
															<td class="c1641ff">
																<?php echo $Cardetail["Seat_CC_Weight"] ? $Cardetail["Seat_CC_Weight"] : "-" ;?>
																<input type="hidden" name="check[carSeat][data]" value="<?php echo $Cardetail["Seat_CC_Weight"] ;?>">
															</td>
														</tr>
														<tr>
															<td class="t_r"><b>ประเภทการซ่อม : </b></td>
															<td class="c1641ff">
																<?php echo ($Cardetail["Repair_Type"] == "C") ? "ซ่อมห้าง" : "ซ่อมอู่" ;?>	
																<input type="hidden" name="check[carRepair][data]" value="<?php echo ($Cardetail["Repair_Type"] == "C") ? "ซ่อมห้าง" : "ซ่อมอู่" ;?>">
															</td>
														</tr>
														<tr>
															<td class="t_r"><b>จำนวนผู้ขับขี่ : </b></td>
															<td class="c1641ff">
																<?php echo $Cardetail["NoOf_Driver"] ? $Cardetail["NoOf_Driver"] : "-" ;?>
																<input type="hidden" name="check[carNoOf][data]" value="<?php echo $Cardetail["NoOf_Driver"] ;?>">
															</td>
														</tr>
														<tr>
															<?php 
																if($Cardetail["Driver_Name"]){
																	$Driver1 = "<b>ชื่อ</b> ".$Cardetail["Driver_Name"]." <br><b>เลขบัตร ปชช.</b> ".$Cardetail["Receiver"]." <br><b>ว/ด/ป เกิด</b> ".$Cardetail["Driver_DOB"]->format('Y-m-d')." <br><b>เลขที่ใบขับขี่</b> ".$Cardetail["Driver_License_No"] ; 
																}
															?>
															<td class="t_r"><b>ผู้ขับขี 1 : </b></td>
															<td class="c1641ff">
																<?php echo $Cardetail["Driver_Name"] ? $Driver1 : "-";?>	
																<input type="hidden" name="check[carDriver][data]" value="<?php echo $Cardetail["Driver_Name"] ? $Driver1 : "";?>">
															</td>
														</tr>
														<tr>
															<?php 
																if($Cardetail["Driver_Name"]){
																	$Driver2 = "<b>ชื่อ</b> ".$Cardetail["Co_Driver_Name"]." <br><b>เลขบัตร ปชช.</b> ".$Cardetail["Sender"]." <br><b>ว/ด/ป เกิด</b> ".$Cardetail["Co_Driver_DOB"]->format('Y-m-d H:i:s')." <br><b>เลขที่ใบขับขี่</b> ".$Cardetail["Co_Driver_License_No"]; 
																}
															?>
															<td class="t_r"><b>ผู้ขับขี 2 : </b></td>
															<td class="c1641ff">
																<?php echo $Cardetail["Co_Driver_Name"] ? $Driver2 : "-";?>
																<input type="hidden" name="check[carCoDriver][data]" value="<?php echo $Cardetail["Co_Driver_Name"] ? $Driver2 : ""; ;?>">
															</td>
														</tr>
														<tr>
															<td class="t_r"><b>ผู้รับผลประโยชน์ : </b></td>
															<td class="c1641ff">
																<?php echo $Cardetail["Beneficiary_Name"] ? $Cardetail["Beneficiary_Name"] : "-";?>
																<input type="hidden" name="check[carBeneficiary][data]" value="<?php echo $Cardetail["Beneficiary_Name"] ;?>">
															</td>
														</tr>
														<tr>
															<td class="t_r"><b>หมายเหตุการแจ้งงาน [ใน iBroker]: </b></td>
															<td class="c1641ff">
																<?php echo $getPO["Deductible"] ? $getPO["Deductible"] : "-";?>
																<input type="hidden" name="check[carDeductible][data]" value="<?php echo $getPO["Deductible"] ;?>">
															</td>
														</tr>
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="panel">
										<div class="panel-heading">
											<h4 class="panel-title"><b>รูปและเอกสาร</b></h4>
											<div class="right">
												<button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
											</div>
										</div>
										<div class="panel-body fs12">
											<div class="row">
												<table class="table table-hover table-striped table-bordered">
													<thead>
														<tr>
															<th class="t_r" style="width: 30%;">รายการ</th>
															<th>ข้อมูล</th>
														</tr>
													</thead>
													<tbody>
														<?php 
															foreach ($getFile as $key => $value) { 
																$path  = ($value["pathfile"]) ? $value["pathfile"]."/" : ""; 
																$fileType = explode(".", $value["file_name"]);
															?>
															<tr>
																<td class="t_r"><b><?php echo caseTitle("file".$value["file_type"]);?> </b></td>
																<td class="c1641ff">
																	<?php if($fileType[1] == "png" || $fileType[1] == "jpg"){ ?>
																		<a data-magnify="gallery" data-src="" data-group="a" href="myfile/<?php echo $path."".$value["file_name"]; ?>">
																	        <img src="myfile/<?php echo $path."".$value["file_name"]; ?>" class="img-responsive rounded" style="width: 150px;max-height: 150px;">
																	     </a>
																	<?php }else{?>
																		<a href="myfile/<?php echo $path."".$value["file_name"]; ?>" data-fancybox="watermark" class="fancybox">
																			<?php echo $value["file_name"]; ?>
																		</a>
																	<?php } ?>
																	<input type="hidden" name="check[file<?php echo $value["file_type"]; ?>][data]" value="<?php echo $path."".$value["file_name"]; ?>">
																</td>
															</tr>
														<?php } ?>
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>


	<?php include "include/inc_footer.php"; ?> 
	<!-- <script src="fancybox/dist/jquery.fancybox.min.js"></script> -->
	<!-- Magnify Image Viewer CSS -->
	<link href="magnifybox/css/jquery.magnify.css" rel="stylesheet">
	<!-- Magnify Image Viewer JS -->
	<script src="magnifybox/js/jquery.magnify.js"></script>

	<script type="text/javascript">
		$("#btn_send_order").click(function() {
			var poCode = $("#po_code").val();
			$.ajax({ 
				url: 'include/inc_action_ms.php',
				type:'POST',
				dataType: 'json',
				data: {action: 'searchPO', pocode:poCode, search_type: 1},
				success:function(rs){ 
					var num = rs.length;
					var details = "";
					for (var i = 0; i < num; i++){
						var d = new Date(rs[i].Coverage_Start_Date.date);
						$("#po_code").val(rs[i].PO_ID);
						$("#cus_name").val(rs[i].Title_Name+" "+rs[i].FName+" "+rs[i].LName);
						$("#insuere_cost").val(rs[i].Capital);
						initials = rs[i].Insurer_Initials;
						cusType = rs[i].cus_type;
						if(cusType == "C"){
							$("#po_type_c").prop("checked", true);
						}else{
							$("#po_type_p").prop("checked", true);
						}
						$("#packageID").val(rs[i].Insurance_Package_ID);
						$("#packageName").val(rs[i].Insurance_Package_Name);
						$("#insuere_company").val(initials);

						if(rs[i].Net_Premium){
							$("#netpremium").val(rs[i].Net_Premium);
						}else{
							$("#netpremium").val(rs[i].Compulsory);
						}
						$("#discount").val(rs[i].Discount);
						$("#premium").val(rs[i].Total_Premium);
						$("#taxamount").val(rs[i].Premium_After_Disc);
						$("#giftvoucher").val(rs[i].Premium_Desc);
						$("#InsuranceType").val(rs[i].Insurance_Name);

						if(rs[i].Marine_Reference){
							$(".boxmarine").show();
							$("#marine").val(rs[i].Marine_Reference);
							$("#marinetxt").html(rs[i].Marine_Reference);
							$('#caseMarine').prop('checked', true);
						}else{
							$(".boxmarine").hide();
							$("#marine").val("");
							$('#caseMarine').prop('checked', false);
						}

						var now = new Date();
						var day = ("0" + d.getDate()).slice(-2);
						var month = ("0" + (d.getMonth() + 1)).slice(-2);
						var today = d.getFullYear()+"-"+month+"-"+day ;
						$("#start_cover_date").val(today);

					}

				}
			});
		});
		function getboxgift(){
			var gift = $("#giftvoucher").val();
			var po = $("#po_code_search").val();
			if(gift){
				$(".boxgift").show();
				$("#giftvoucher_po").val(po);
			}else{
				$(".boxgift").hide();
			}
// console.log(gift);
}
$(".rd_chk").click(function() {
val = $('.rd_chk:checked').val();
name = $(this).attr("name");
repName = name.replace("chk_", "comment_");
if(val == "0"){
$("#comment_"+repName).show(); 
}else{
$("#comment_"+repName).hide();
}

});
$(document).ready(function() {
$(".btnSendAdd").click(function() { 
status = $("#mainStatus").val();
if(status != 0){
	if( confirm("ตรวจสอบข้อมูลสมบูรณ์ ต้องการบันทึกผลการประเมิน !!!") ){
		// console.log("btnSendAdd");
		$("#frmsendAddlistchk").submit();
	}
}else{
	alert("กรุณาเลือกสถานะ!!!");
}

});
});

function ValidateSingleInput(oInput) {
var _validFile = [".jpg", ".jpeg", ".pdf",".png",];    
if (oInput.type == "file") {
var sFileName = oInput.value;
if (sFileName.length > 0) {
	var blnValid = false;
	for (var j = 0; j < _validFile.length; j++) {
		var sCurExtension = _validFile[j];
		if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
			blnValid = true;
			break;
		}
	}

	if (!blnValid) {
		alert("กรุณาเลือกไฟล์ ที่มีนามสกุล PNG | JPG | PDF เท่านั้น" );
		oInput.value = "";
		return false;
	}
}
}
return true;
}

function funChk(name) {
// console.log("input[name='check["+name+"][chk]']:checked");
val = $("input[name='check["+name+"][chk]']:checked").val(); 
// console.log(val);
if(val == 0){
	$("#comment_"+name).show();
}else{
	$("#comment_"+name).hide();
}

}
function changStatus(id, codes, status){
// console.log(codes);
if(status == 1){
txt = "ต้องการยกเลิก รายการแจ้งงานนี้";
}else if(status == 3){
txt = "ยืนยันการรับงาน";
}else if(status == 4){
txt = "ยืนยันการแจ้งงานสมบูรณ์";
}
if(confirm(txt)){
$.ajax({ 
	url: 'include/inc_action_chk.php',
	type:'POST',
	data: {action: 'upDateStatus', id:id, code:codes, status:status},
	success:function(rs){ 
		if(status == 1){
			window.location = "ordersadmins.php";
		}else{
			window.location.reload(true);
		}
	}
});
}

}
function addRequest(){ 
pocode = $("#pocode").val();
if( confirm("สร้างใบคำขอ?")){
$.ajax({ 
	url: 'include/inc_action_chk.php',
	type:'POST',
	data: {action: 'addRequest', pocode:pocode},
	success:function(rs){
		window.location.reload(true);
	}
});
}
}

function btnComments(code){
var val = code;
// console.log(val);
mentTxt = $("#comment").val();
// console.log("#listMent_"+mentTxt);
if(mentTxt != 0){
$.ajax({ 
	url: 'include/inc_action_chk.php',
	type:'POST',
	data: {action: 'addComments', noti_work_code:code, comment:mentTxt},
	success:function(rs){
		console.log(rs);
		$("#listMent").empty();
		$("#comment").val("");
		$("#listMent").append(rs);
	}
});
}else{
alert("กรุณาเลือก Comment");
}
}

function btnChangStatus(code,admin_code){
insuere = $("#insuere_company").val();
if( confirm("คุณต้องการส่งงานอีกครั้ง")){
$.ajax({ 
	url: 'include/inc_action_chk.php',
	type:'POST',
	data: {action: 'btnChangStatus', code:code, admin_code:admin_code, insuere:insuere},
	success:function(rs){  
		if(rs){
			alert("เปลี่ยนสถานะเรียบร้อย กรุณารอการตรวจเช็ค");
			window.location.href = 'sendorders.php';
		}else{
			alert("ไม่สามารถเปลี่ยนสถานะได้ กรุณาลองใหม่อีกครั้ง");
			window.location.reload(true);
		}
	}
});
}

}

function delfile(code, filetype, name){
// console.log(code+" - "+filetype+ "" + name );
if( confirm("ต้องการลบไฟล์ !!!")){
$.ajax({ 
	url: 'include/inc_action_chk.php',
	type:'POST',
	data: {action: 'delfileEdit', code:code, name:name},
	success:function(rs){ 
		// console.log(rs);
		if(rs == 0){
			alert("ไม่สามารถลบไฟล์ได้ กรุณาลองใหม่");
		}else{
			alert("ลบไฟล์เรียบร้อย");
			window.location.reload(true);
		}
	}
});
}
}
$( '[data-fancybox="watermark"]' ).fancybox({
buttons : [
'download',
'zoom',
'close'
],
protect : false,
animationEffect : "zoom-in-out",
transitionEffect : "fade",
zoomOpacity : "auto",
animationDuration : 500,
zoomType: 'innerzoom',

});
</script>