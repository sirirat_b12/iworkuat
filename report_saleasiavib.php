<?php 
session_start();
ini_set('max_execution_time', 186);
ini_set('memory_limit', '2048M');
$UserCode = $_SESSION["User"]['UserCode'];
$userType = $_SESSION["User"]['type'];

if(!isset($UserCode)){
	echo "<script>alert('กรุณาเข้าสู่ระบบ!!!');</script>";
	echo '<META http-equiv="refresh" content="0;URL=login.php">';
	exit();
}

header('Content-Type: text/html; charset=utf-8');
include "include/inc_header.php"; 
include "include/inc_menu.php"; 
include "reports/inc_function_report.php";
include "reports/targetdata.php";
// include('qrcode/qrcode.class.php');

$year = date("Y");

$_GET["txtse"] = ($_GET["txtse"]) ? $_GET["txtse"] : date("Y-m");
$getDepartment = getDepartment();

if($_GET["txtse"]){
	$getDataCount = getSaleCountAsiaVBI($_GET["txtse"], $_GET["depar"]);
}

?>
<div class="main">
	<div class="">
		<div class="p20">
				<div class="row">
					<div class="col-md-12">
						<div class="panel">
								<div class="panel-heading">
									<h4 class="panel-title"><b>รายงานยอด ASIA VIB เดือน <?php echo date("Y F", strtotime($_GET["txtse"]."-01"));?></b></h4>
								</div>
								<div class="panel-body fs14">
									<div class="row">
										<div class="col-md-4 ">
											<span class="fs18 cff2da5 mr15">เดือน</span>
											<input type="hidden" value="<?php echo $_GET["txtse"]; ?>" id="txtse">
											<select name="mounth" id="mounthOpt" class="form-control formInput2 dib" style="width: 80%" >
												<option value="">:: กรุณาเลือก ::</option>
													<?php 
														$dateLast = ( date("Y") - 1)."-".date("m");
														for($i=1; $i<=12; $i++) { 
															$txtM = "+".$i." month";
															$dateForValue = date("Y-m",strtotime($txtM, strtotime($dateLast)));
													?>
														<option value="<?php echo $dateForValue ;?>" <?php if($_GET["txtse"] ==  $dateForValue){ echo "selected"; } ?>>
															<?php echo date("Y m F",strtotime($dateForValue)) ?>
														</option>
													<?php } ?>
											</select>
										</div>
										<div class="col-md-4 ">
											<span class="fs18 cff2da5 mr15">ทีม</span>
											<select name="Department" id="Department" class="form-control formInput2 dib" style="width: 80%" >
												<option value="">:: กรุณาเลือก ::</option>
												<?php foreach ($getDepartment as $key => $value) {?>
													<option value="<?php echo $value["Department_ID"];?>" <?php if($_GET["depar"] ==  $value["Department_ID"]){ echo "selected"; } ?>>
														<?php echo $value["Department_Name"]." | ".$value["Remark"];?>
													</option>
												<?php } ?>
											</select>
										</div>
									</div>
									<div class="row mt20">
										<div class="col-md-12">
											<div class="row">
												<div class="col-md-2"></div><div class="col-md-10"><?php echo $value["PO_ID"]; ?></div>
											</div>
											<?php if($getDataCount){?>
												<table class="table table-hover table-bordered" id="indextable" >
													<thead>
														<tr class="bgbaf4bc">
															<th class="t_c">รหัสพนักงาน</th>
															<th class="t_c">ทีม</th>
															<th class="t_c">ชื่อ-นามสกุล</th>
															
															<th class="t_c"><a href="javascript:SortTable(4,'N');">วิริยะ ประกัน</a></th>
															<th class="t_c"><a href="javascript:SortTable(5,'N');">รู้ใจ ยอด</a></th>
															<th class="t_c"><a href="javascript:SortTable(6,'N');">รู้ใจ จำนวน</a></th>
															<th class="t_c"><a href="javascript:SortTable(7,'N');">SMK C72 ยอด</a></th>
															<th class="t_c"><a href="javascript:SortTable(8,'N');">SMK C72 จำนวน</a></th>
														</tr>
													</thead>
													<tbody >
														<?php 
															foreach ($getDataCount as $key => $value) {

																$AllPOPaidVBICaseIns +=  $value["POPaidVBICaseIns"];
															
																$AllPOPaidRoojaiSum +=  $value["POPaidRoojaiSum"];
																$AllPOPaidRoojaiCount +=  $value["POPaidRoojaiCount"];

																$AllPOPaidC72Sum +=  $value["POPaidSumC72"];
																$AllPOPaidC72Count +=  $value["POPaidCountC72"];

														?>
															<tr>
																<td class="t_c"><?php echo $value["Employee_ID"]; ?></td>
																<td class="t_c"><?php echo $value["Department_Name"]; ?></td>
																<td class="t_l"><?php echo $value["User_FName"]." ".$value["User_LName"]; ?></td>
																
																<td class="t_c cff2da5"><?php echo $value["POPaidVBICaseIns"]; ?></td>
																
																
																<td class="t_r c00ac0a"><?php echo number_format($value["POPaidRoojaiSum"],2); ?></td>
																<td class="t_r c00ac0a"><?php echo $value["POPaidRoojaiCount"]; ?></td>
																<td class="t_r c2457ff"><?php echo number_format($value["POPaidSumC72"],2); ?></td>
																<td class="t_r c2457ff"><?php echo $value["POPaidCountC72"]; ?></td>
															</tr>
															<?php } ?>
													</tbody>
													<tfoot>
														<tr class="fwb fs16 bgfffbd8">
															<td class="t_c" colspan="3">รวม</td>
															
															<td class="t_c cff2da5"><?php echo $AllPOPaidVBICaseIns; ?></td>
															
															<td class="t_c fwb c00ac0a"><?php echo number_format($AllPOPaidRoojaiSum,2); ?></td>
															<td class="t_c c00ac0a"><?php echo number_format($AllPOPaidRoojaiCount); ?></td>
															<td class="t_c fwb c2457ff"><?php echo number_format($AllPOPaidC72Sum,2); ?></td>
															<td class="t_c c2457ff"><?php echo number_format($AllPOPaidC72Count); ?></td>
														</tr>
													</tfoot>
												</table>
											<?php } ?>
										</div>
									</div>
								</div>
						</div>
					</div>
				</div>
		</div>
	</div>
</div>
<?php include "include/inc_footer.php"; ?> 
<?php include "include/inc_function_tablesort.php"; ?>
<script type="text/javascript">

	$("#mounthOpt").on('change', function() {
		if(this.value){
			window.location.href = "report_saleasiavib.php?txtse="+this.value;
		}

  });

  $("#Department").on('change', function() {
  	txtse = $("#txtse").val();
		if(this.value){
			window.location.href = "report_saleasiavib.php?txtse="+txtse+"&depar="+this.value;
		}else{
			window.location.href = "report_saleasiavib.php?txtse="+txtse;
		}

  });

<?php if($_GET["txtse"]){?>
function getDetailPO(user, date){
	txtse = $("#txtse").val();
	if(txtse){
		window.location.href = "report_saleasiavib.php?txtse="+txtse+"&user="+user+"&date="+date;
	}
}
<?php } ?>
  

function filterPOcode(){
	txtse = $("#txtse").val();
	if(txtse){
		window.location.href = "counters.php?txtse="+txtse;
	}else{
		alert("กรุณากรอก ข้อมูล เพื่อค้นหา");
	}
}

function random_rgba() {
    var o = Math.round, r = Math.random, s = 255;
    return 'rgba(' + o(r()*s) + ',' + o(r()*s) + ',' + o(r()*s) + ',' + r().toFixed(2) + ')';
}


</script>



