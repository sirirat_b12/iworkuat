<?php
session_start();
session_start();
ini_set('post_max_size', '512M');
ini_set('upload_max_filesize', '512M');
ini_set("memory_limit","512M");
ini_set('max_execution_time', 300);
date_default_timezone_set("Asia/Bangkok");
header('Content-Type: text/html; charset=utf-8');

include "../inc_config.php"; 
require '../phpmailer6/src/PHPMailer.php';
require '../phpmailer6/src/SMTP.php';
require '../phpmailer6/src/Exception.php';
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

include "../include/sms.class.php";
include "inc_function_insfollow.php"; 

// $_POST["action"] = "ModalFollowBody";

if($_POST["action"] == "ModalFollowBody"){

	$getInstallmentFollow = getInstallmentFollow($_POST["po"], $_POST["ins"]);
	$listfollow = listfollow($getInstallmentFollow);
	echo $listfollow ;


}else if($_POST["action"] == "AddModalFollow"){

	$_POST["Installment_Followup_id"] = getInstallmentFollowID();
	$_POST["user"] = $_SESSION["User"]['UserCode'];
	$_POST["Active"] = "Y" ;
	$_POST["Remind_Status"] = ($_POST["Remind_Status"] == 1 ) ? 1 : 0 ;
	if($_POST["Installment_Followup_Topic"] == "001"){
		$_POST["Postpone_Case"] = 1 ;
	}else{
		$_POST["Postpone_Case"] = 0 ;
	}

	if($_POST["Remind_Status"] == "1"){
		$_POST["Installment_Followup_Detail"] = $_POST["Installment_Followup_Detail"]." [นัดหมายครั้งหน้า:".$_POST["Remind_Date"]."]" ;
	}

	$rs = insertInstallmentFollow($_POST);
	if($rs){

		$getInstallmentFollow = getInstallmentFollow($_POST["PO_ID"], $_POST["Installment_ID"]);
		$listfollow = listfollow($getInstallmentFollow);

		$getRowPurchaseOrder = getRowPurchaseOrder($_POST["PO_ID"]);
		$userline = $getRowPurchaseOrder["Employee_ID"];
		// $userline = 'ADB59019';
		// $userline = $Installment["User_ID"];

		if($_POST["Installment_Followup_Topic"] == "001"){
			$textLine = 'แจ้งคำขอ เลื่อนวันชำระ \n'.$_POST["PO_ID"].' | งวดที่ '.$_POST["Installment_ID"].'\nเหตุผล: '.$_POST["Installment_Followup_Detail"].'  \nวันที่สร้าง '.date('Y-m-d H:i:s');
			curlLineNoti('ADB60056', $textLine);  //การเงิน

		}elseif($_POST["Installment_Followup_Topic"] == "004"){
			$textLine = 'แจ้งยกเลิกกรมธรรม์ โดยการเงิน \n'.$_POST["PO_ID"].'\nเหตุผล: '.$_POST["Installment_Followup_Detail"].'  \nวันที่สร้าง '.date('Y-m-d H:i:s');
			curlLineNoti($userline, $textLine); 

			$sqlupDate = "UPDATE [dbo].[Purchase_Order] SET [Status_ID] = 'RTE', [Update_Date] = '".date("Y-m-d H:i:s")."', [Update_By] = '".$_POST["user"]."' WHERE PO_ID = '".$_POST["PO_ID"]."' ";
			$stmtup = sqlsrv_query( $connMS, $sqlupDate);


			$Cancel_status = "CS02";
			$Remark = "ยกเลิกโดยการเงินเนื่องจากไม่ชำระเบี้ยงวดที่ ".$_POST["Installment_ID"];
			$sql = "INSERT INTO Purchase_Order_Cancel ([PO_ID], [Cancel_status], [Customer_sendto_date], [Customer_sendto_date_by], [Broker_sendto_date], [Broker_sendto_date_by], [Remark], [Endorse], [Endorse_date], [Endorse_by], [Active], [Create_Date], [Create_By], [Update_Date], [Update_By], [Status_Emai_Date], [Send_Email_by])
		     VALUES ('".$_POST["PO_ID"]."', '".$Cancel_status."', '".date("Y-m-d H:i:s")."', '".$_POST["user"]."', '', '', '".$Remark."', 'N', '', '', 'Y', '".date("Y-m-d H:i:s")."', '".$_POST["user"]."', '".date("Y-m-d H:i:s")."', '".$_POST["user"]."', '', '')";
			$rs = sqlsrv_query( $connMS, $sql);

			

		}elseif($_POST["Installment_Followup_Topic"] == "008"){
			$textLine = 'แจ้งยกเลิกการขอเลื่อนวันที่ชำระ \n'.$_POST["PO_ID"].' | งวดที่ '.$_POST["Installment_ID"].'\nเหตุผล: '.$_POST["Installment_Followup_Detail"].'  \nวันที่สร้าง '.date('Y-m-d H:i:s');
			curlLineNoti($userline, $textLine); 

		}elseif($_POST["Installment_Followup_Topic"] == "009"){
			$textLine = 'แจ้งติดต่อลูกค้าด่วน ก่อนยกเลิกกรมธรรม์ \n'.$_POST["PO_ID"].'\nเหตุผล: '.$_POST["Installment_Followup_Detail"].'  \nวันที่สร้าง '.date('Y-m-d H:i:s');
			curlLineNoti($userline, $textLine); 
		}

		echo $listfollow ;
	}else{
		echo 0;
	}

}else if($_POST["action"] == "sendSMS"){
	$arr = array();
	$Installment = getRowOneInstallment($_POST["po"], $_POST["ins"]);
	$Installment_Due_Date = $Installment["Installment_Due_Date"]->format("d/m/Y");
	$PolicyNumBer = $Installment["Policy_No"] ? $Installment["Policy_No"] : $Installment["Compulsory_No"]; 
	
	if($_POST["casesms"] == "C1"){

		$arr["Installment_Followup_Topic"] = "005";
		$txtSMS = 'ทะเบียนรถ '.$Installment["Plate_No"].' ค้างชำระเงิน งวดที่'.$Installment["Installment_ID"].' วันที่ '.$Installment_Due_Date.' จำนวน '.number_format($Installment["ISTM_Total_Amount"],2).' บาท โปรดติดต่อ '.$Installment["User_Phone"].' โดยด่วน ก่อนยกเลิกกรมธรรม์ ';

	}elseif($_POST["casesms"] == "C2"){
		$arr["Installment_Followup_Topic"] = "010";
		$txtSMS = 'ทะเบียนรถ '.$Installment["Plate_No"].' ครบชำระเงินงวดที่'.$Installment["Installment_ID"].' วันที่ '.$Installment_Due_Date.' จำนวน '.number_format($Installment["ISTM_Total_Amount"],2).' บาท '.$Installment["User_Phone"].' https://www.asiadirect.co.th/a/?p='.$Installment["PO_ID"].'I'.$Installment["Installment_ID"];

	}else{
		$arr["Installment_Followup_Topic"] = "011";
		$txtSMS = 'แจ้งให้ทราบ เนื่องจากท่านค้างชำระค่ากรมธรรม์ ทะเบียนรถ '.$Installment["Plate_No"].' จึงขอยกเลิกความคุ้มครองกรมธรรม์ ถ้าหากท่านชำระเงินหลังได้รับแจ้งข้อความ จะไม่มีผลใดๆต่อความคุ้มครอง ขออภัยในความไม่สะดวก 020892000';

	}
	echo $txtSMS;
	$getSendSMS = getSendSMS($_POST["phone"], $txtSMS);
	exit();

	if($getSendSMS){
		$arr["Installment_Followup_id"] = getInstallmentFollowID();
		$arr["Postpone_Case"] = 0 ;
		$arr["PO_ID"] = $_POST["po"] ;
		$arr["Installment_ID"] = $_POST["ins"] ;
		$arr["Installment_Followup_DateTime"] = date("Y-m-d H:i:s");
		$arr["Installment_Followup_Detail"] = $txtSMS;
		$arr["Active"] = "Y" ;

		$rs = insertInstallmentFollow($arr);
		echo 1;
		exit();
	}
	echo 0;
	exit();

}else if($_POST["action"] == "getPostpone"){
	$getRowPostpone = getRowPostpone($_POST["id"]);
	echo json_encode($getRowPostpone);
	exit();


}elseif($_POST["action"] == "UpdatePostpone"){


	$_POST["user"] = $_SESSION["User"]['UserCode'];
	$getRowPurchaseOrder = getRowPurchaseOrder($_POST["PO_ID"]);
	$userPO = $getRowPurchaseOrder["Employee_ID"];
	// $userPO = "ADB59019";

	if($_POST["caseFN"] == 1){
		// $updateInstallment = updateInstallment($_POST);

		// if($updateInstallment){

			$sqlupDate = "UPDATE [dbo].[Installment_Followup] SET [Postpone_Case] = '0', [Update_Date] = '".date("Y-m-d H:i:s")."', [Update_By] = '".$_POST["user"]."' WHERE Installment_Followup_id = '".$_POST["Installment_Followup_id"]."' ";
			$stmtup = sqlsrv_query( $connMS, $sqlupDate);

			$arr["Installment_Followup_id"] = getInstallmentFollowID();
			$arr["user"] = $_SESSION["User"]['UserCode'];
			$arr["PO_ID"] = $_POST["PO_ID"];
			$arr["Installment_ID"] = $_POST["Installment_ID"];
			$arr["Installment_Followup_DateTime"] = date("Y-m-d H:i:s");
			$arr["Installment_Followup_Topic"] = "007";
			$arr["Installment_Followup_Detail"] = "เลื่อนวันชำระเรียบร้อย เป็นวันที่ ".$_POST["Postpone_Date"];
			$arr["Postpone_Case"] = 0 ;
			$arr["Active"] = "Y" ;
			if($_POST["Remind_Status"] == "1"){
				$arr["Remind_Status"] = $_POST["Remind_Status"] ;
				$arr["Remind_Date"] = $_POST["Remind_Date"] ;
				$arr["Installment_Followup_Detail"] = $_POST["Installment_Followup_Detail"]." [นัดหมายครั้งหน้า:".$_POST["Remind_Date"]."]" ;
			}
			$rs = insertInstallmentFollow($arr);

			$textLine = 'แจ้งเลื่อนวันชำระเรียบร้อย \n'.$_POST["PO_ID"].' | งวดที่ '.$_POST["Installment_ID"].'\nกรุณาตรวจสอบความถูกต้อง \nวันที่สร้าง '.date('Y-m-d H:i:s');
			curlLineNoti($userPO, $textLine);
			
			echo "<script>alert('ทำรายการสำเร็จ');</script>";
			echo '<META http-equiv="refresh" content="0;URL=../ins_follow.php">';
			exit();
		// }else{
		// 	echo "<script>alert('ทำรายการไม่สำเร็จ');</script>";
		// 	echo '<META http-equiv="refresh" content="0;URL=../ins_follow.php">';
		// 	exit();
		// }
	}else{

		$sqlupDate = "UPDATE [dbo].[Installment_Followup] SET [Postpone_Case] = '0', [Update_Date] = '".date("Y-m-d H:i:s")."', [Update_By] = '".$_POST["user"]."' WHERE Installment_Followup_id = '".$_POST["Installment_Followup_id"]."' ";
		$stmtup = sqlsrv_query( $connMS, $sqlupDate);

		$arr["Installment_Followup_id"] = getInstallmentFollowID();
		$arr["user"] = $_SESSION["User"]['UserCode'];
		$arr["PO_ID"] = $_POST["PO_ID"];
		$arr["Installment_ID"] = $_POST["Installment_ID"];
		$arr["Installment_Followup_DateTime"] = date("Y-m-d H:i:s");
		$arr["Installment_Followup_Topic"] = "008";
		$arr["Installment_Followup_Detail"] = $_POST["Installment_Followup_Detail"];
		$arr["Postpone_Case"] = 0 ;
		$arr["Active"] = "Y" ;
		if($_POST["Remind_Status"] == "1"){
			$arr["Remind_Status"] = $_POST["Remind_Status"] ;
			$arr["Remind_Date"] = $_POST["Remind_Date"] ;
			$arr["Installment_Followup_Detail"] = $_POST["Installment_Followup_Detail"]." [นัดหมายครั้งหน้า:".$_POST["Remind_Date"]."]" ;
		}
		$rs = insertInstallmentFollow($arr);

		$textLine = 'ยกเลิกการขอเลื่อนวันชำระ \n'.$_POST["PO_ID"].' | งวดที่ '.$_POST["Installment_ID"].'\nเนื่องจาก '.$_POST["Installment_Followup_Detail"].'  \nวันที่สร้าง '.date('Y-m-d H:i:s');
		curlLineNoti($userPO, $textLine);

		echo "<script>alert('ทำรายการยกเลิกคำขอสำเร็จ');</script>";
		echo '<META http-equiv="refresh" content="0;URL=../ins_follow.php">';
		exit();

	}

}elseif ($_POST["action"] == "sendMailInsurCancel") {

	$mailInsur = mailInsCancel($_POST, $_FILES);
	$arr["datetime"] = date("Y-m-d H:i:s");
	$arr["noti_work_code"] = $_POST["noti_work_code"];
	$arr["po_code"] = $_POST["po_code"];
	$arr["user_send"] = $_SESSION["User"]['UserCode'];
	$arr["email"] = json_encode($_POST["mailsend"]);
	$arr["bodymail"] = $_POST["bodymail"];
	$arr["status_send"] = $mailInsur;
	$insertSQL = $conn2->AutoExecute("log_mail_admin", $arr, 'INSERT');

  if($mailInsur){
  	$sqlFollowup = "UPDATE [dbo].[Installment_Followup] SET [Active] = 'M' WHERE Installment_Followup_id = '".$_POST["Installment_Followup_id"]."' ";	
		$stmtFollowup = sqlsrv_query( $connMS, $sqlFollowup);

		echo "<script>alert('ส่งอีเมล์เรียบร้อย กรุณาเช็คอีเมล์อีกครั้งเพื่อความถูกต้อง');</script>";
		echo '<META http-equiv="refresh" content="0;URL=../ins_follow.php">';
		exit();
	}else{
		echo "<script>alert('ไม่สามารถส่งเมล์ได้ กรุณาลองใหม่');</script>";
		echo '<META http-equiv="refresh" content="0;URL=../ins_email.php?poid='.$_POST["po_code"].'">';
		exit();
	}
	
	exit();

}elseif ($_POST["action"] == "submitCancelINS") {
		$user = $_SESSION["User"]['UserCode'];
		$Installment = getRowOneInstallment($_POST["poid"], $_POST["ins"]);
		$phone = ($Installment["Tel_No"]) ? $Installment["Tel_No"] : $Installment["Mobile_No"] ;
	 	$sqlupDate = "UPDATE [dbo].[Purchase_Order] SET [Status_ID] = 'ERQ', [Update_Date] = '".date("Y-m-d H:i:s")."', [Update_By] = '".$user."' WHERE PO_ID = '".$_POST["poid"]."' ";	
		$stmtup = sqlsrv_query( $connMS, $sqlupDate);
		
		$Remark = 'แจ้งยกเลิกกรมธรรม์โดยแอดมิน | '.$_POST["poid"].' | '.date('Y-m-d H:i:s');
		$sqlFollowup = "UPDATE [dbo].[Installment_Followup] SET [Active] = 'N', [Remark] = '".$Remark."', [Update_Date] = '".date("Y-m-d H:i:s")."', [Update_By] = '".$user."' WHERE PO_ID = '".$_POST["poid"]."' ";	
		$stmtFollowup = sqlsrv_query( $connMS, $sqlFollowup);

		$txtSMS = 'แจ้งให้ทราบ เนื่องจากท่านค้างชำระค่ากรมธรรม์ ทะเบียนรถ '.$Installment["Plate_No"].' จึงขอยกเลิกความคุ้มครองกรมธรรม์ ถ้าหากท่านชำระเงินหลังได้รับแจ้งข้อความ จะไม่มีผลใดๆต่อความคุ้มครอง ขออภัยในความไม่สะดวก 020892000';
		$getSendSMS = getSendSMS($phone, $txtSMS);



		$arr["Installment_Followup_id"] = getInstallmentFollowID();
		$arr["user"] = $_SESSION["User"]['UserCode'];
		$arr["PO_ID"] = $_POST["poid"];
		$arr["Installment_ID"] = $_POST["ins"];
		$arr["Installment_Followup_DateTime"] = date("Y-m-d H:i:s");
		$arr["Installment_Followup_Topic"] = "011";
		$arr["Installment_Followup_Detail"] = $txtSMS;
		$arr["Postpone_Case"] = 0 ;
		$arr["Active"] = "N" ;
		$rs = insertInstallmentFollow($arr);
		
		$arr["Installment_Followup_id"] = getInstallmentFollowID();
		$arr["user"] = $_SESSION["User"]['UserCode'];
		$arr["PO_ID"] = $_POST["poid"];
		$arr["Installment_ID"] = $_POST["ins"];
		$arr["Installment_Followup_DateTime"] = date("Y-m-d H:i:s");
		$arr["Installment_Followup_Topic"] = "012";
		$arr["Installment_Followup_Detail"] = "แอดมินส่งอีเมล์แจ้งยกเลิกกรมธรรม์กับบริษัทประกันเรียบร้อย";
		$arr["Postpone_Case"] = 0 ;
		$arr["Active"] = "N" ;
		$rs = insertInstallmentFollow($arr);

		$stmtup = 1;
		if($stmtup) {
			$userPO = $_POST["empid"];
			$textLine = 'แจ้งยกเลิกกรมธรรม์โดยแอดมิน \n'.$_POST["poid"].'\nกรุณาตรวจสอบความถูกต้อง \nวันที่สร้าง '.date('Y-m-d H:i:s');
			curlLineNoti($userPO, $textLine);
	    echo 1;
	  } else {
	    echo 0;
	  }
	  exit();


}elseif ($_POST["action"] == "cancelFollow") {
		$user = $_SESSION["User"]['UserCode'];

		$Remark = 'ยกเลิกการติดตามโดย | '.$user.' | '.date('Y-m-d H:i:s');
		$sqlFollowup = "UPDATE [dbo].[Installment_Followup] SET [Remind_Status] = '0', [Remark] = '".$Remark."', [Update_Date] = '".date("Y-m-d H:i:s")."', [Update_By] = '".$user."' WHERE PO_ID = '".$_POST["poid"]."' AND Installment_ID = '".$_POST["ins"]."' ";	
		$stmtFollowup = sqlsrv_query( $connMS, $sqlFollowup);
		if($stmtFollowup) {
	    echo 1;
	  } else {
	    echo 0;
	  }
	  exit();

}elseif($_POST["action"] == "deleteComment"){
		$user = $_SESSION["User"]['UserCode'];
		$Remark = 'ลบการติดตามโดย | '.$user.' | '.date('Y-m-d H:i:s');
		$sqlFollowup = "DELETE [dbo].[Installment_Followup] WHERE Installment_Followup_id = '".$_POST["id"]."' ";	
		$stmtFollowup = sqlsrv_query( $connMS, $sqlFollowup);
		// $stmtFollowup = 1;
		if($stmtFollowup) {
	    $getInstallmentFollow = getInstallmentFollow($_POST["poid"], $_POST["ins"]);
			$listfollow = listfollow($getInstallmentFollow);
			echo $listfollow ;
	  } else {
	    echo 0;
	  }
	  exit();
}
?>
