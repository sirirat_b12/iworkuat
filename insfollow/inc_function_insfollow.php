<?php
function getInstallment($date, $txt, $case){
	global $connMS;
	$resultarray = array();
	$nameUser = $_SESSION["User"]['firstname'];
	$UserCode = $_SESSION["User"]['UserCode'];
	$getSuperSaleid = getSuperSaleid($nameUser);
		$sql = "SELECT Installment.PO_ID, last_follow_id, Installment_Followup.Remind_Date, Installment_Followup_Topic.Installment_Followup_Topic_ID , Installment_Followup_Topic.Installment_Followup_Topic, Installment_Followup.Installment_Followup_Detail
	, Installment.Installment_Status_ID, Installment.Installment_ID, Installment.ISTM_Total_Amount, Installment.Installment_Due_Date, Customer.Customer_FName, Customer.Customer_LName, Customer.Tel_No, Customer.Mobile_No, 
	My_User.User_FName, My_User.User_LName, Car_Detail.Plate_No,Installment_Status.Installment_Status, Purchase_Order.Employee_ID, Purchase_Order.Status_ID
	
	FROM installment
	LEFT JOIN [dbo].[Purchase_Order] ON Installment.PO_ID = Purchase_Order.PO_ID 
	LEFT OUTER JOIN 
	( SELECT  PO_ID, installment_ID, MAX(installment_followup_id) AS last_follow_id
	 FROM Installment_Followup group by PO_ID, installment_ID ) AS last_Installment_followup ON last_Installment_followup.PO_ID = Installment.PO_ID and last_Installment_followup.Installment_ID = installment.Installment_ID
	LEFT OUTER JOIN 
	Installment_Followup ON last_Installment_followup.PO_ID= Installment_Followup.PO_ID and last_Installment_followup.Installment_ID = Installment_Followup.Installment_id  and last_Installment_followup.last_follow_id = Installment_Followup.Installment_Followup_id

		LEFT JOIN [dbo].[Installment_Followup_Topic] ON Installment_Followup.Installment_Followup_Topic = Installment_Followup_Topic.Installment_Followup_Topic_ID
	  LEFT JOIN [dbo].[Installment_Status] ON Installment.Installment_Status_ID = Installment_Status.Installment_Status_ID
	  LEFT JOIN [dbo].[Car_Detail] ON Purchase_Order.PO_ID = Car_Detail.PO_ID
	  LEFT JOIN [dbo].[Customer] ON Purchase_Order.Customer_ID = Customer.Customer_ID
	  LEFT JOIN [dbo].[My_User] ON Purchase_Order.Employee_ID = My_User.User_ID";

	if($txt){
		$sql .= " WHERE Installment.PO_ID = '".$txt."'  ";
	}else if($date && !$txt){
		$datestart = $date;
		$dateEnd = $date." 23:59:59";
		$sql .= " WHERE Purchase_Order.Status_ID IN ('RCA','RVP', 'ERQ', 'RTE', 'CLS') AND installment.installment_ID > 1 AND Installment.Installment_Status_ID = '001' AND Installment.Installment_Due_Date >= '".$datestart."' AND Installment.Installment_Due_Date <= '".$dateEnd."' ";
		
		if($_SESSION["User"]['type'] == "Sale" && $getSuperSaleid["Department_ID"] == ""){
			$sql .= " AND Purchase_Order.Employee_ID = '".$UserCode."'  ";
		}
	}else{
		$sql .= " WHERE Purchase_Order.Status_ID IN ('RCA','RVP', 'ERQ', 'RTE', 'CLS') AND installment.installment_ID > 1 ";
	}

	if($getSuperSaleid["Department_ID"]){
		$sql .= "AND My_User.User_Dept_ID = '".$getSuperSaleid["Department_ID"]."' ";
	}
	if($case == 'Remind'){
		if($_SESSION["User"]['type'] == "Sale" && $getSuperSaleid["Department_ID"] == ""){
			$sql .= " AND Purchase_Order.Employee_ID = '".$UserCode."'  ";
		}
		$sql .= " AND Installment.Installment_Status_ID = '001'  AND Installment_Followup.Remind_Status = '1' ORDER BY  Installment_Followup.Remind_Date ASC  ";

	}else if($case == '014'){
		$sql .= " AND Installment_Followup.Installment_Followup_Topic = '014' ORDER BY Installment_Followup.Remind_Date ASC  ";
	}
// echo "<br><br>****".$sql ;
	
	$stmt = sqlsrv_query( $connMS, $sql );
  	if(sqlsrv_has_rows($stmt)) {
    while( $row = sqlsrv_fetch_array($stmt) ) { 
      $resultarray[] = $row;
    }
    return $resultarray;
		exit();
	}
}


function getInstallmentForAsia3M($dateStart, $dateEnd, $txt, $case){
	global $connMS;
	$resultarray = array();
	$nameUser = $_SESSION["User"]['firstname'];
	$UserCode = $_SESSION["User"]['UserCode'];
	$getSuperSaleid = getSuperSaleid($nameUser);
	$datestart = $dateStart;
	$dateEnd = $dateEnd." 23:59:59";

		$sql = "SELECT last_follow_id, Installment_Followup.Remind_Date, Installment_Followup_Topic.Installment_Followup_Topic_ID , Installment_Followup_Topic.Installment_Followup_Topic, Installment_Followup.Installment_Followup_Detail
	,Installment.*, Customer.Customer_FName, Customer.Customer_LName, Customer.Tel_No, Customer.Mobile_No, 
	My_User.User_FName, My_User.User_LName, Car_Detail.Plate_No,Installment_Status.Installment_Status, Purchase_Order.Employee_ID, Purchase_Order.Status_ID
	
	FROM installment
	LEFT JOIN [dbo].[Purchase_Order] ON Installment.PO_ID = Purchase_Order.PO_ID 
	LEFT OUTER JOIN 
	( SELECT  PO_ID, installment_ID, MAX(installment_followup_id) AS last_follow_id
	 FROM Installment_Followup group by PO_ID, installment_ID ) AS last_Installment_followup ON last_Installment_followup.PO_ID = Installment.PO_ID and last_Installment_followup.Installment_ID = installment.Installment_ID
	LEFT OUTER JOIN 
	Installment_Followup ON last_Installment_followup.PO_ID= Installment_Followup.PO_ID and last_Installment_followup.Installment_ID = Installment_Followup.Installment_id  and last_Installment_followup.last_follow_id = Installment_Followup.Installment_Followup_id

		LEFT JOIN [dbo].[Installment_Followup_Topic] ON Installment_Followup.Installment_Followup_Topic = Installment_Followup_Topic.Installment_Followup_Topic_ID
	  LEFT JOIN [dbo].[Installment_Status] ON Installment.Installment_Status_ID = Installment_Status.Installment_Status_ID
	  LEFT JOIN [dbo].[Car_Detail] ON Purchase_Order.PO_ID = Car_Detail.PO_ID
	  LEFT JOIN [dbo].[Customer] ON Purchase_Order.Customer_ID = Customer.Customer_ID
	  LEFT JOIN [dbo].[My_User] ON Purchase_Order.Employee_ID = My_User.User_ID";


	$sql .= " WHERE Purchase_Order.Status_ID IN ('RCA','RVP', 'ERQ', 'RTE', 'CLS') 
	AND Purchase_Order.Insurance_ID IN ('084','085')  AND Installment.Installment_Status_ID = '001' 
	AND Installment.Installment_Due_Date >= '".$dateStart."' AND Installment.Installment_Due_Date <= '".$dateEnd."' 
	ORDER BY  installment.Installment_Due_Date ASC ";

// echo "<br><br>".$sql ;
	
	$stmt = sqlsrv_query( $connMS, $sql );
  if(sqlsrv_has_rows($stmt)) {
    while( $row = sqlsrv_fetch_array($stmt) ) { 
      $resultarray[] = $row;
    }
    return $resultarray;
		exit();
	}
}


function getPOCancelRow($poid){
	global $connMS;
	$resultarray = array();
	$sql = "SELECT * FROM Purchase_Order_Cancel WHERE PO_ID = '".$poid."'  ";
	$stmt = sqlsrv_query( $connMS, $sql );
  if(sqlsrv_has_rows($stmt)) {
  	$row = sqlsrv_fetch_array($stmt);
  	return  $row;
    exit();
  }
}


function getSuperSaleid($name){
	global $connMS;
	$resultarray = array();
	$sql = "SELECT Department_ID FROM Department WHERE Department_Name LIKE '%".$name."%' AND  Department_ID > '100' AND Active = 'Y' ";
	$stmt = sqlsrv_query( $connMS, $sql );
  if(sqlsrv_has_rows($stmt)) {
  	$row = sqlsrv_fetch_array($stmt);
  	return  $row;
    exit();
  }
}


function getRowPOByid($poid){
	global $connMS;
	$resultarray = array();
	$sql = "SELECT Purchase_Order.*, Purchase_Order.Remark As PORemark,  Purchase_Order.Create_Date As POCreate_Date,  Purchase_Order.Create_By As POCreate_By,  Purchase_Order.Update_Date As POUpdate_Date,  Purchase_Order.Update_By As POUpdate_By, Customer.Customer_ID, Customer.Customer_FName, Customer.Customer_LName, My_User.User_FName, My_User.User_LName, Insurance.Insurance_Name, Insurance_Package.Insurance_Package_Name, Insurer.Insurer_Initials AS Insurer_Initials, Insurer.Insurer_Name, Car_Detail.*, Purchase_Order_Mail_Address.*, Province.Province_Name_TH, Car_Make.Make_Name_TH, Asia_Car_Model.Model_Desc, Purchase_Order_Insurer.Policy_No, Purchase_Order_Insurer.Compulsory_No

	FROM Purchase_Order LEFT JOIN Customer ON Purchase_Order.Customer_ID = Customer.Customer_ID
	LEFT JOIN My_User ON Purchase_Order.Employee_ID = My_User.User_ID
	LEFT JOIN [dbo].[Insurance] ON Purchase_Order.Insurance_ID = Insurance.Insurance_ID
	LEFT JOIN [dbo].Purchase_Order_Insurer ON Purchase_Order.PO_ID = Purchase_Order_Insurer.PO_ID
	LEFT JOIN [dbo].[Purchase_Order_Mail_Address] ON Purchase_Order.PO_ID = Purchase_Order_Mail_Address.PO_ID
  LEFT JOIN [dbo].[Insurer] ON Purchase_Order_Insurer.Insurer_ID = Insurer.Insurer_ID
	LEFT JOIN [dbo].[Insurance_Package] ON Purchase_Order.Insurance_Package_ID = Insurance_Package.Insurance_Package_ID
	LEFT JOIN [dbo].[Car_Detail] ON Purchase_Order.PO_ID = Car_Detail.PO_ID
	LEFT JOIN [dbo].[Car_Make] ON Car_Detail.Make_ID = Car_Make.Make_ID
  LEFT JOIN [dbo].[Car_Type] ON Car_Detail.Car_Code = Car_Type.Car_Type_ID 
  LEFT JOIN [dbo].[Asia_Car_Model] ON Car_Detail.Model = Asia_Car_Model.Model_ID 
  LEFT JOIN [dbo].[Province] ON Car_Detail.Plate_Province = Province.Province_ID
	WHERE Purchase_Order.PO_ID = '".$poid."' ";
	$stmt = sqlsrv_query( $connMS, $sql );
  if(sqlsrv_has_rows($stmt)) {
  	$row = sqlsrv_fetch_array($stmt);
  	return  $row;
    exit();
  }
}


function getRowOneInstallment($poid, $ins){
	global $connMS;
	$sql = "SELECT Installment.*, Customer.Customer_FName, Customer.Customer_LName, Customer.Tel_No, Customer.Mobile_No, My_User.User_ID, My_User.User_FName, My_User.User_LName, My_User.User_Phone, Car_Detail.Plate_No,Installment_Status.Installment_Status, Purchase_Order.Employee_ID, Purchase_Order_Insurer.Policy_No, Purchase_Order_Insurer.Compulsory_No
	FROM Installment 
  LEFT JOIN [dbo].[Purchase_Order] ON Installment.PO_ID = Purchase_Order.PO_ID
  LEFT JOIN [dbo].[Purchase_Order_Insurer] ON Purchase_Order.PO_ID = Purchase_Order_Insurer.PO_ID
  LEFT JOIN [dbo].[Installment_Status] ON Installment.Installment_Status_ID = Installment_Status.Installment_Status_ID
  LEFT JOIN [dbo].[Car_Detail] ON Purchase_Order.PO_ID = Car_Detail.PO_ID
  LEFT JOIN [dbo].[Customer] ON Purchase_Order.Customer_ID = Customer.Customer_ID
  LEFT JOIN [dbo].[My_User] ON Purchase_Order.Employee_ID = My_User.User_ID
	WHERE Installment.PO_ID ='".$poid."' AND Installment.Installment_ID ='".$ins."' ";

	$stmt = sqlsrv_query( $connMS, $sql );
 	if(sqlsrv_has_rows($stmt)) {
    $row = sqlsrv_fetch_array($stmt);
    return $row;
    exit();
  }  
  exit();
}


function getInstallmentFollowTopic($case){
	global $connMS;
	$resultarray = array();
	if($case == "Sale"){
		$sql = "SELECT *FROM Installment_Followup_Topic WHERE Installment_Followup_Topic.Active = 'Y' AND Installment_Followup_Topic.Enable = 'Y'  ORDER BY Installment_Followup_Topic_ID ASC";
	}else{
		$sql = "SELECT *FROM Installment_Followup_Topic WHERE Installment_Followup_Topic.Enable = 'Y' ORDER BY Installment_Followup_Topic_ID ASC";
	}
// echo $sql ;
	$stmt = sqlsrv_query( $connMS, $sql );
  if(sqlsrv_has_rows($stmt)) {
    while( $row = sqlsrv_fetch_array($stmt) ) { 
      $resultarray[] = $row;
    }
    return $resultarray;
		exit();
	}

}


function listfollow($getInstallmentFollow){
	foreach ($getInstallmentFollow as $key => $value) {
		$body .= '
		<div class="mt10">
			<div class="cff2da5 fwb fs14"> <i class="fas fa-th-large"></i> '.$value["Installment_Followup_Topic"].'</div>
			<div class="mt5 fs13">'.$value["Installment_Followup_Detail"].'</div>
			<div class="fs10 cb2">'.$value["Installment_Followup_DateTime"]->format("d-m-Y H:i:s").' | '.$value["User_FName"]." ".$value["User_LName"];
	if($_SESSION["User"]['type'] != "Sale"){
		$body .= '
			| <span class="cursorPoin c2457ff fs12" onclick="deleteComment(\''.$value["Installment_Followup_id"].'\',\''.$value["PO_ID"].'\',\''.$value["Installment_ID"].'\')">ลบ</span></div>';
	}
		$body .= '
		</div>';
	}
	return $body;
	exit();
}


function getInstallmentFollow($po, $ins){
	global $connMS;
	$resultarray = array();

	$sql = "SELECT Installment_Followup.*, Installment_Followup_Topic.Installment_Followup_Topic, My_User.User_FName, My_User.User_LName
	FROM Installment_Followup
	LEFT JOIN [dbo].[Installment_Followup_Topic] ON Installment_Followup.Installment_Followup_Topic = Installment_Followup_Topic.Installment_Followup_Topic_ID
	LEFT JOIN [dbo].[My_User] ON Installment_Followup.Create_By = My_User.User_ID
	WHERE Installment_Followup.PO_ID = '".$po."'  AND Installment_Followup.Installment_ID = '".$ins."'  ORDER BY Installment_Followup_DateTime ASC";
// echo $sql ;
	
	$stmt = sqlsrv_query( $connMS, $sql );
  if(sqlsrv_has_rows($stmt)) {
    while( $row = sqlsrv_fetch_array($stmt) ) { 
      $resultarray[] = $row;
    }
    return $resultarray;
		exit();
	}

}




function getPostpone(){
	global $connMS;
	$sql = "SELECT Installment_Followup.*, Installment.Installment_Due_Date, My_User.User_FName, My_User.User_LName
	From [dbo].Installment_Followup 
	JOIN Installment ON Installment_Followup.PO_ID = Installment.PO_ID AND Installment_Followup.Installment_ID = Installment.Installment_ID
	LEFT JOIN My_User ON Installment_Followup.Create_By = My_User.User_ID
	WHERE Installment_Followup.Postpone_Case = '1' 
	ORDER BY Installment_Followup.Installment_Followup_DateTime ASC ";
	$stmt = sqlsrv_query( $connMS, $sql );
  if(sqlsrv_has_rows($stmt)) {
    while( $row = sqlsrv_fetch_array($stmt) ) { 
      $resultarray[] = $row;
    }
    return $resultarray;
		exit();
	}
}

function getCancelByAccounting(){
	global $connMS;
	$sql = "SELECT Installment_Followup.*,Customer.Customer_FName, Customer.Customer_LName, Customer.Tel_No, Customer.Mobile_No, Insurer.Insurer_Name
 ,Purchase_Order_Insurer.Policy_No, Purchase_Order_Insurer.Compulsory_No, Insurance_Package.Insurance_Package_Name, My_User.User_FName, My_User.User_LName, Purchase_Order.Employee_ID
 From [dbo].Installment_Followup 
 LEFT JOIN [dbo].[Purchase_Order] ON Installment_Followup.PO_ID = Purchase_Order.PO_ID 
 LEFT JOIN [dbo].[Purchase_Order_Insurer] ON Purchase_Order.PO_ID = Purchase_Order_Insurer.PO_ID
 LEFT JOIN [dbo].[Insurance_Package] ON Purchase_Order.Insurance_Package_ID = Insurance_Package.Insurance_Package_ID 
 LEFT JOIN [dbo].[Insurer] ON Purchase_Order_Insurer.Insurer_ID = Insurer.Insurer_ID
 LEFT JOIN [dbo].[Customer] ON Purchase_Order.Customer_ID = Customer.Customer_ID
 LEFT JOIN [dbo].[My_User] ON Purchase_Order.Employee_ID = My_User.User_ID

WHERE Installment_Followup.Installment_Followup_Topic = '004' AND Installment_Followup.Active IN ('Y','M') 
ORDER BY Installment_Followup.Installment_Followup_DateTime ASC  ";
	$stmt = sqlsrv_query( $connMS, $sql );
  if(sqlsrv_has_rows($stmt)) {
    while( $row = sqlsrv_fetch_array($stmt) ) { 
      $resultarray[] = $row;
    }
    return $resultarray;
		exit();
	}
}


function getRowPostpone($id){
	global $connMS;
	$sql = "SELECT * From [dbo].Installment_Followup WHERE Installment_Followup_id = '".$id."' ";
	$stmt = sqlsrv_query( $connMS, $sql );
 	if(sqlsrv_has_rows($stmt)) {
    $row = sqlsrv_fetch_array($stmt);
    return $row;
    exit();
  }  
}


function getRowPurchaseOrder($PO_ID){
	global $connMS;
	$sql = "SELECT * From [dbo].Purchase_Order WHERE PO_ID = '".$PO_ID."' ";
	$stmt = sqlsrv_query( $connMS, $sql );
 	if(sqlsrv_has_rows($stmt)) {
    $row = sqlsrv_fetch_array($stmt);
    return $row;
    exit();
  }  
}



function getInstallmentFollowID(){
	global $connMS;
	$year = date("Y");
	// $sql = "SELECT MAX(Installment_Followup_id) AS Installment_Followup_id From [dbo].Installment_Followup WHERE year(Create_Date) = '".$year."' AND MONTH(Create_Date) = '".date("m")."' ";
	$sql = "SELECT TOP 1 Installment_Followup_id AS Installment_Followup_id From [dbo].Installment_Followup ORDER BY Installment_Followup_id DESC ";
	$stmt = sqlsrv_query( $connMS, $sql );
  if(sqlsrv_has_rows($stmt)) {
    $row = sqlsrv_fetch_array($stmt);
    // echo substr($row["Installment_Followup_id"], 5);
		$code = substr($row["Installment_Followup_id"], 5);
   	$idMax = "INF".(date("y")+43)."".str_pad(intval($code)+1,7,"0",STR_PAD_LEFT);
    return $idMax;
    exit();
  }  
}

function insertInstallmentFollow($val){
	global $connMS;
	$sql = "INSERT INTO [dbo].[Installment_Followup]
           ([Installment_Followup_id], [PO_ID], [Installment_ID], [Installment_Followup_DateTime], [Installment_Followup_Topic], [Installment_Followup_Detail], [Postpone_Case], [Postpone_Date], [Remind_Date], [Remind_Status], [Active], [Create_Date], [Create_By], [Update_Date], [Update_By])
     VALUES ('".$val["Installment_Followup_id"]."', '".$val["PO_ID"]."', '".$val["Installment_ID"]."', '".$val["Installment_Followup_DateTime"]."', '".$val["Installment_Followup_Topic"]."', '".$val["Installment_Followup_Detail"]."', '".$val["Postpone_Case"]."', '".$val["Postpone_Date"]."', '".$val["Remind_Date"]."', '".$val["Remind_Status"]."', '".$val["Active"]."', '".date("Y-m-d H:i:s")."', '".$val["user"]."', '".date("Y-m-d H:i:s")."', '".$val["user"]."') ";

   $stmt = sqlsrv_query( $connMS, $sql);
 	return $stmt;
  exit();

}



function getSendSMS($phone, $txtSMS){

	if($txtSMS){ 
		// $phone = '0970705484';
		// $result_sms = sms::send_sms('adbwecare','6251002','0894353549', $txtSMS,'AsiaDirect','','');
		// $result_sms = sms::send_sms('adbwecare','6251002',$phone, $txtSMS,'AsiaDirect','','');

		$source = "ADBCHK";
		$phone =  "0894353549";
		$messages = $txtSMS;

		$tel=preg_replace('/^\+66/', '0', $old_tel);
		$url  = 'https://www.asiadirect.co.th/truesms/truesms.class.php';
		$data = array(
		    'phone' => $phone,
		    'source' => $source,
		    'messages' => $messages
		);

			$ch = curl_init($url);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
			curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			$result = curl_exec($ch);
			$error_msg = curl_error($ch);
			curl_close($ch);

			// echo "info<pre>".print_r($result,1)."</pre>";

		
	}
	$result_sms = 1;
	if($result_sms){
		return 1;
	}else{
		return 0;
	}
	exit();
}


function updateInstallment($arr){
	global $connMS;
	$sqlupDate = "UPDATE [dbo].[Installment] SET [Installment_Due_Date] = '".$arr["Postpone_Date"]."', [Update_Date] = '".date("Y-m-d H:i:s")."', [Update_By] = '".$arr["user"]."' WHERE PO_ID = '".$arr["PO_ID"]."' AND Installment_ID = '".$arr["Installment_ID"]."' ";
	$stmtup = sqlsrv_query( $connMS, $sqlupDate);
	$stmtup = 1;
	if($stmtup){
		return 1;
	}else{
		return 0;
	}
	exit();
}

function curlLineNoti($user, $msg){
	$url  = 'https://www.asiadirect.co.th/line-at/ibroker/linemeapi.php';
	$data = array(
	    'user_id' => $user,
	    'status' => "Installment",
	    'messages' => $msg
	);
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$result = curl_exec($ch);
		$error_msg = curl_error($ch);
		curl_close($ch);
	return 1;
	exit();
}



function mailInsCancel($arr, $files){

  // echo "<pre>".print_r($files,1)."</pre>";
  $mail = new PHPMailer\PHPMailer\PHPMailer(true);
  $mail->CharSet = 'utf-8';
  $mail->SMTPDebug = 0;   
  $mail->Timeout   = 90;                              // Enable verbose debug output
  $mail->isSMTP();                                      // Set mailer to use SMTP
  $mail->Host = 'smtp.gmail.com'; // Specify main and backup SMTP servers
  $mail->SMTPAuth = true;  
  $mail->Username = "admin@asiadirect.co.th";
  $mail->Password = "@db6251000";
  $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
  $mail->Port = 587;                                    // TCP port to connect to
  // $mail->setFrom('admin@asiadirect.co.th', 'Asia Direct Broker');
  $mail->setFrom($_SESSION["User"]['email'], $_SESSION["User"]['firstname']." Asia Direct" );
  $mail->AddReplyTo($_SESSION["User"]['email'], $_SESSION["User"]['firstname']." Asia Direct" );
  if($_POST["mailsend"]){ 
    foreach ($_POST["mailsend"] as $key => $valMail) {
      $nameMail = explode("|", $valMail);
      $nameemail = $nameMail[1] ? $nameMail[1] : $nameMail[0];
      if(ereg_replace('[[:space:]]+', ' ', trim($nameMail[2])) == "CC"){
        $mail->AddCC($nameMail[0], $nameemail);
      }else{
        $mail->AddAddress($nameMail[0], $nameemail); //ลุกค้า
      }
    }
  }

  // $mail->AddAddress('teerakan.s@asiadirect.co.th', 'IT');               // Name is optional
  $mail->isHTML(true);                                  // Set email format to HTML
  $mail->Subject = $arr["subject"];
  $mail->Body  = $arr["bodymail"];

//   Attachments
  if($files){
    foreach ($files["files"]['tmp_name'] as $key => $value) {
      $uploadfile = tempnam(sys_get_temp_dir(), sha1($_FILES['files']['name'][$key]));
      $filename = $_FILES['files']['name'][$key];
      if (move_uploaded_file($_FILES['files']['tmp_name'][$key], $uploadfile)) {
          $mail->addAttachment($uploadfile, $filename);
      }
    }
 }

  if(!$mail->Send()) {
    return 0;
  } else {
    return 1;
  }
  exit();
}

