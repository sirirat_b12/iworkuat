
<?php

date_default_timezone_set('Asia/Bangkok');
header('Content-Type: text/html; charset=utf-8');

$strExcelFileName = "ReportCallcanter".date('YmdHis').".xls";

header("Content-Type: application/vnd.ms-excel; name=\"$strExcelFileName\"");
header("Content-Disposition: inline; filename=\"$strExcelFileName\"");
header("Pragma:no-cache");

include "../inc_config.php"; 
include "inc_function_report.php";
include "../callcenters/inc_function_call.php";

$_GET["date"] = ($_GET["date"]) ? $_GET["date"] : date("Y-m");
if($_GET["date"]){
    $getCallcenterBySubject = getCallcenterBySubject($_GET["date"]);
    $getCallcenterByEmp = getCallcenterByEmp($_GET["date"]);
    $getcallcenters = getcallcenters($_GET["date"]);
    $getEmpofMonth = getEmpofMonth($_GET["date"]);
}
// $reportInformByAdmin = reportInformByAdmin($_GET["ds"], $_GET["de"], $_GET["insurers"]);
// echo "<pre>".print_r($reportInformByAdmin,1)."</pre>";

?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>
    <input type="hidden" value="<?php echo $_GET["date"];?>" id="dateSe">
    <div>รายงาน ประจำเดือน <?php echo date("F Y");?></div>
    <div id="SiXhEaD_Excel" align=center x:publishsource="Excel">
        <table x:str border=1 cellpadding=1 cellspacing=1 width=100% style="border-collapse:collapse">
            <tr>
                <th valign="middle">#</th>
                <th valign="middle">เรื่องทีติดต่อ</th>
                <th valign="middle">จำนวนสาย</th>
            </tr>
            <?php 
                $sumAll = 0;
                $i = 1;
                foreach ($getCallcenterBySubject as $key => $value) {
                    $sumAll +=  $value["sumCall"];
            ?>
                <tr>
                    <td valign="middle"><?php echo $i++; ?></td>
                    <td valign="middle"><?php echo $value["Call_Subject"]; ?></td>
                    <td class="t_c fwb "><?php echo $value["sumCall"]; ?></td>
                </tr>
            <?php } ?>
            <tfoot>
                <tr class="fwb fs16 bgfffbd8">
                    <th valign="middle" colspan="2">รวม</th>
                    <th valign="middle"><?php echo $sumAll; ?></th>
                </tr>
            </tfoot>
        </table>
    </div>
    <div> สรุปแยกตามผู้รับสาย</div>
    <?php if($getCallcenterBySubject){?>
     <div id="SiXhEaD_Excel" align=center x:publishsource="Excel">
        <table x:str border=1 cellpadding=1 cellspacing=1 width=100% style="border-collapse:collapse">
            <tr>
                <th valign="middle">ผู้รับสาย</th>
                <?php foreach ($getEmpofMonth as $key => $value) { ?>
                    <th class="middle"><?php echo $value["Create_By"]." ".$value["User_FName"]." ".$value["User_LName"] ?></th>
                <?php } ?>
            </tr>
            <?php 
                $sumAll = 0;
                $i = 1;
                foreach ($getCallcenterByEmp as $key => $value) {
            ?>
                <tr>
                    <td class="t_c fwb "><?php echo $value["dateList"]->format("d/m/Y"); ?></td>
                    <?php foreach ($getEmpofMonth as $key => $valueEmp) { ?>
                        <td class="t_c"><?php echo $value[$valueEmp["Create_By"]]; ?></td>
                    <?php } ?>
                </tr>
            <?php } ?>
            <tfoot>
                <tr class="fwb fs16 bgfffbd8">
                    <th class="t_c" >รวม</th>
                    <?php foreach ($getEmpofMonth as $key => $valueEmp) { 
                        $first_names = array_column($getCallcenterByEmp, $valueEmp["Create_By"]);
                        $sumArr = array_sum($first_names);
                    ?>                                                          
                        <td class="t_c fwb "><?php echo $sumArr; ?></td>
                    <?php } ?>
                </tr>
            </tfoot>
        </table>
    </div>
    <?php } ?>

    <div id="SiXhEaD_Excel" align=center x:publishsource="Excel">
        <table x:str border=1 cellpadding=1 cellspacing=1 width=100% style="border-collapse:collapse">
           <tr class="table p5">
                <th class="t_c">ลำดับ</th>
                <th class="t_c">วันที่ติดต่อ</th>
                <th class="t_c">PO</th>
                <th class="t_c">ชื่อลูกค้า</th>
                <th class="t_c">ชื่อพนักงาน</th>
                <th class="t_c">ช่องทางติดต่อ</th>
                <th class="t_c">โทรศัพท์</th>
                <th class="t_c">เรื่องที่ติดต่อ</th>
                <th class="t_c">รายละเอียด</th>
                <th class="t_c">ผู้รับสาย</th>
            </tr>
            <?php 
                $i = 1;
                foreach ($getcallcenters as $key => $value) { 
            ?>
                <tr>
                    <td class="text-center"><?php echo $i++; ?></td>
                    <td class="fwb t_c nowrap cff2da5">
                        <div><?php echo $value["Call_Date"]->format("d-m-Y"); ?></div>
                    </td>
                    <td><?php echo $value["PO_ID"]; ?></td>
                    <td class="fwb c2457ff"><?php echo $value["Customer_Name"]; ?></td>
                    <td class="fwb "><?php echo $value["Employee_Neme"]; ?></td>
                    <td class="text-center c9c00c8"><?php echo $value["Channel"]; ?></td>
                    <td class="text-center"><?php echo $value["Phone"]; ?></td>
                    <td><?php echo $value["Call_Subject"]; ?></td>
                    <td><?php echo $value["Call_Detail"]; ?></td>
                    <td class="text-center"><?php echo $value["User_FName"]." ".$value["User_LName"]; ?></td>
                </tr>
            <?php } ?>
        </table>
    </div>

    <script>
        window.onbeforeunload = function(){return false;};
        setTimeout(function(){window.close();}, 10000);
   
    </script>
</body>
</html>
