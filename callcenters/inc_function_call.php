<?php



function getcallcenters($txtse){
	global $connMS;
	$resultarray = array();
	$exDate = explode("-", $txtse);
	$year = $exDate[0];
	$mounth = $exDate[1];
	if($mounth){
		$datestart =  $year."-".$mounth."-01";
	    $dateNow = $year."-".$mounth."-".date('t',strtotime($year."-".$mounth))." 23:59:59";
	}
	$sql = "SELECT Callcenters.* , My_User.User_FName, My_User.User_LName
	FROM Callcenters 
	LEFT JOIN My_User ON Callcenters.Create_By = My_User.User_ID
	WHERE Callcenters.Call_Date >= '".$datestart."' AND Callcenters.Call_Date <= '".$dateNow."' ORDER BY Call_Date DESC";
	$stmt = sqlsrv_query( $connMS, $sql );
	if(sqlsrv_has_rows($stmt)) {
	    while( $row = sqlsrv_fetch_array($stmt) ) { 
	        $resultarray[] = $row;
	    }
	    return $resultarray;
	    exit();
	}
}


function getMaxIDCallID(){
	global $connMS;
	$sql = "SELECT MAX(Call_ID) AS Call_ID From Callcenters WHERE year(Create_Date) = '".date("Y")."' ";
	$stmt = sqlsrv_query( $connMS, $sql );
	  if(sqlsrv_has_rows($stmt)) {
	    $row = sqlsrv_fetch_array($stmt);
		$code = substr($row["Call_ID"],-6);
	    $idMax = "CL".(date("y")+43)."".str_pad(intval($code)+1,6,"0",STR_PAD_LEFT);
	    return $idMax;
	    exit();
	  }  
}




function getCallcenterBySubject($date){
	global $connMS;
	$resultarray = array();
	$dateStart = date("Y-m-d", strtotime($date))." 00:00:00";
	$dateEnd = date("Y-m-t", strtotime($date))." 23:59:59";

	$sql = "SELECT Call_Subject,count(Call_ID) AS sumCall
	FROM Callcenters
	WHERE Call_Date >= '".$dateStart."'  AND Call_Date <= '".$dateEnd."'
	GROUP BY Call_Subject
	ORDER BY sumcall DESC";

	$stmt = sqlsrv_query( $connMS, $sql );
	if(sqlsrv_has_rows($stmt)) {
	    while( $row = sqlsrv_fetch_array($stmt) ) { 
	      $resultarray[] = $row;
	    }
	    return $resultarray;
	    exit();
	} 
}


function getEmpofMonth($date){
	global $connMS;
	$resultarray = array();
	$dateStart = date("Y-m-d", strtotime($date))." 00:00:00";
	$dateEnd = date("Y-m-t", strtotime($date))." 23:59:59";
	$sqlEmp  = "SELECT Callcenters.Create_By , My_User.User_FName, My_User.User_LName 
 	from Callcenters  join My_User ON Callcenters.Create_By = My_User.User_ID
	WHERE Callcenters.Call_Date >= '".$dateStart."' AND  Callcenters.Call_Date <= '".$dateEnd."'
	GROUP BY Callcenters.Create_By, My_User.User_FName, My_User.User_LName  ORDER BY Callcenters.Create_By ASC";
	$stmt = sqlsrv_query( $connMS, $sqlEmp );
	if(sqlsrv_has_rows($stmt)) {
	    while( $row = sqlsrv_fetch_array($stmt) ) { 
	      $resultarray[] = $row;
	    }
	    return $resultarray;
	    exit();
	} 


}
function getCallcenterByEmp($date){
	global $connMS;
	$resultarray = array();
	$dateStart = date("Y-m-d", strtotime($date))." 00:00:00";
	$dateEnd = date("Y-m-t", strtotime($date))." 23:59:59";

	$getEmpofMonth = getEmpofMonth($date);




	$sql = "SELECT convert(date, Callcenters.Call_Date, 103) AS dateList";
	foreach ($getEmpofMonth as $key => $value) {
		$sql .= " ,COUNT(case when Callcenters.Create_By = '".$value["Create_By"]."'  then Callcenters.Call_ID end) AS ".$value["Create_By"];
	}
	
	$sql .= " FROM Callcenters LEFT JOIN My_User ON Callcenters.Create_By = My_User.User_ID
	WHERE Callcenters.Call_Date >= '".$dateStart."' AND  Callcenters.Call_Date <= '".$dateEnd."'
	GROUP BY convert(date, Callcenters.Call_Date, 103)";
echo $sql;
	$stmt = sqlsrv_query( $connMS, $sql );
	if(sqlsrv_has_rows($stmt)) {
	    while( $row = sqlsrv_fetch_array($stmt) ) { 
	      $resultarray[] = $row;
	    }
	    return $resultarray;
	    exit();
	} 
}

?>