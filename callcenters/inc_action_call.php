<?php
session_start();
date_default_timezone_set("Asia/Bangkok");
header('Content-Type: text/html; charset=utf-8');

include "../inc_config.php";
include "inc_function_call.php";

if($_POST["action"] == "getPOonPhone"){
	$txt_search = trim($_POST["txt_search"]);
	$sql = "SELECT Purchase_Order.PO_ID, Compare_Policy.CP_ID, Customer.Customer_ID, Customer.Customer_FName, Customer.Customer_LName, Customer.Tel_No, My_User.User_ID, My_User.User_FName, My_User.User_LName, Customer.Mobile_No, Customer.Tel_No, Customer.EMail, Car_Detail.Plate_No
	  FROM Purchase_Order
	  LEFT JOIN Compare_Policy ON Purchase_Order.PO_ID = Compare_Policy.CP_PO_ID
	  LEFT JOIN Customer ON Purchase_Order.Customer_ID = Customer.Customer_ID  
	  LEFT JOIN My_User ON Purchase_Order.Employee_ID = My_User.User_ID 
	  LEFT JOIN Car_Detail ON Purchase_Order.PO_ID = Car_Detail.PO_ID  
	  WHERE Customer.Tel_No = '".$txt_search."' 
	  OR Customer.Mobile_No = '".$txt_search."' 
	  OR Car_Detail.Plate_No = '".$txt_search."'
	  ORDER BY Purchase_Order.PO_ID DESC  ";
	$stmt = sqlsrv_query( $connMS, $sql );
	if(sqlsrv_has_rows($stmt)) {
		$row = sqlsrv_fetch_array($stmt);
		echo json_encode($row);
		exit();
	}

}else if($_POST["action"] == "addCallDateil"){
	// echo "<pre>".print_r($_POST,1)."</pre>"; 
	$user = $_SESSION["User"]['UserCode']; 
	$getMaxIDCallID = getMaxIDCallID();

	$sql = "INSERT INTO [Callcenters] ([Call_ID], [Call_Date], [PO_ID], [Customer_ID], [Customer_Name], [Employee_ID], [Employee_Neme], [Channel], [Phone], [Email], [Call_Subject], [Call_Detail], [Create_Date], [Create_By], [Update_Date], [Update_By])
     	VALUES ('".$getMaxIDCallID."', '".$_POST["Call_Date"]."', '".$_POST["PO_ID"]."', '".$_POST["Customer_ID"]."', '".$_POST["Customer_Name"]."', '".$_POST["Employee_ID"]."', '".$_POST["Employee_Neme"]."', '".$_POST["Channel"]."', '".$_POST["Phone"]."', '".$_POST["Email"]."', '".$_POST["Call_Subject"]."', '".$_POST["Call_Detail"]."', '".date('Y-m-d H:i:s')."', '".$user."', '".date('Y-m-d H:i:s')."', '".$user."')";
    $resource = sqlsrv_query( $connMS, $sql);
    if($resource){
    	echo "<script>alert('ทำรายการสำเร็จ');</script>";
		echo '<META http-equiv="refresh" content="0;URL=../callcenters.php">';
		exit();
    }else{
    	echo "<script>alert('ไม่สามารถทำรายการได้ กรุณาลองอีกครั้ง');</script>";
		echo '<META http-equiv="refresh" content="0;URL=../callcenters.php">';
		exit();
    }
	exit();


}else if($_POST["action"] == "deleteCallcenter"){
	// echo "<pre>".print_r($_POST,1)."</pre>"; 
	// exit();
	$sql = "DELETE FROM [Callcenters] WHERE Call_ID = '".$_POST["Call_ID"]."' ";
 	$stmt = sqlsrv_query( $connMS, $sql);
 	if($stmt){
 		echo "1";
 	}else{
 		echo "0";
 	}
 exit();

}else if($_POST["action"] == "reportCharCallcenterOne"){

	$resultarray = array();
	$getCallcenterBySubject = getCallcenterBySubject($_POST["mounth"]);
	// echo "<pre>".print_r($getCallcenterBySubject,1)."</pre>";
	foreach ($getCallcenterBySubject as $key => $value) {
    	$resultarray[$key]["subject"] = $value["Call_Subject"];
    	$resultarray[$key]["sumCase"] = $value["sumCall"];
 	}
 	echo json_encode($resultarray);
	exit();

}else if($_POST["action"] == "reportCharBydate"){

	$resultarray = array();
	$getCallcenterByEmp = getCallcenterByEmp($_POST["mounth"]);
	// echo "<pre>".print_r($getCallcenterByEmp,1)."</pre>";
	foreach ($getCallcenterByEmp as $key => $value) {
		// $date = $value["dateList"]->format("d");
		$resultarray[$key]["date"] = $value["dateList"]->format("d");
    	$resultarray[$key]["emp1"] = $value["emp1"];
    	$resultarray[$key]["emp2"] = $value["emp2"];
    	$resultarray[$key]["emp3"] = $value["emp3"];
 	}
 	// echo "<pre>".print_r($resultarray,1)."</pre>";
 	echo json_encode($resultarray);
	exit();

}


?>