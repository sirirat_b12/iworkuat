<?php
session_start();
date_default_timezone_set("Asia/Bangkok");
header('Content-Type: text/html; charset=utf-8');

include "../inc_config.php";

// $po_id = $_POST["poCode"];

?>

<div class="main-content p20 fs12">
	<div class="row">
		<div class="col-md-8">
			<input type="text" class="form-control formInput2" name="txt_search" id="txt_search" placeholder="เบอร์โทร หรือ ทะเบียนรถ">
		</div>
		<div class="col-md-4 text-center">
			<button class="btn btn-info " onclick="fnSearch()">ค้นหา</button>
		</div>
	</div>
	<form action="callcenters/inc_action_call.php" method="POST" id="form_send_order" >
		<input type="hidden" class="form-control formInput2" name="action" value="addCallDateil" >
		
		<div class="row mt15">
			<div class="col-md-12 mt10">
				<p class="fwb">วันที่ติดต่อ *</p>
				<input type="text" value="<?php echo date("Y-m-d H:i:s"); ?>" class="form-control formInput2" name="Call_Date" readonly>
			</div>
			<div class="col-md-6 mt10">
				<p class="fwb">ช่องทางติดต่อ *</p>
				<select name="Channel" id="Channel" class="form-control formInput2" required>
					<option value="">กรุณาเลือก</option>
					<option value="Phone">Phone</option>  
					<option value="Line">Line</option> 
					<option value="Email">Email</option>
					<option value="Voice Mail">Voice Mail</option> 
				</select>
			</div>
			<div class="col-md-6 mt10">
				<p class="fwb">PO</p>
				<input type="text" class="form-control formInput2" name="PO_ID" id="PO_ID" >
			</div>
			<div class="col-md-6 mt10">
				<p class="fwb">เบอร์โทร</p>
				<input type="text" class="form-control formInput2" name="Phone" id="Call_Phone">
			</div>
			<div class="col-md-6 mt10">
				<p class="fwb">อีเมล์</p>
				<input type="text" class="form-control formInput2" name="Email" id="Call_Email" >
			</div>
			<div class="col-md-6 mt10">
				<p class="fwb">ลูกค้า * </p>
				<input type="hidden" class="form-control formInput2" name="Customer_ID" id="Customer_ID" >
				<input type="text" class="form-control formInput2" name="Customer_Name" id="Customer_Name" >
			</div>
			<div class="col-md-6 mt10">
				<p class="fwb">พนักงานขาย</p>
				<input type="hidden" class="form-control formInput2" name="Employee_ID" id="Employee_ID" >
				<input type="text" class="form-control formInput2" name="Employee_Neme" id="Employee_Neme" >
			</div>
			<div class="clearb">
				<div class="col-md-12 mt10">
					<p class="fwb">หัวข้อการติดต่อ *</p>
					<select name="Call_Subject" id="Call_Subject" class="form-control formInput2" required>
						<option value="">กรุณาเลือก</option>
						<option value="ติดต่อเซลล์/ฝ่ายขาย">ติดต่อเซลล์/ฝ่ายขาย</option>  
						<option value="ติดต่อฝ่ายบัญชี/การเงิน">ติดต่อฝ่ายบัญชี/การเงิน</option> 
						<option value="ติดต่อเรื่องเคลม">ติดต่อเรื่องเคลม</option>
						<option value="ติดต่อเรื่องเคลม">ติดต่อเรื่องเคลม ADB</option>
						<option value="ติดต่อฝ่ายแอดมิน">ติดต่อฝ่ายแอดมิน</option>
						<option value="ติดต่อฝ่ายบุคคล">ติดต่อฝ่ายบุคคล</option>
						<option value="ติดต่อฝ่ายการตลาด">ติดต่อฝ่ายการตลาด</option>
						<option value="ติดต่อฝ่ายIT">ติดต่อฝ่ายIT</option>
						<option value="สอบถามสิทธิประโยชน์ต่างๆ">สอบถามสิทธิประโยชน์ต่างๆ</option> 
						<option value="ติดต่อ Care Center">ติดต่อ Care Center</option> 
						<option value="Sales Support">Sales Support</option> 
						<option value="เรื่องอื่นๆ">เรื่องอื่นๆ</option>  
					</select>
				</div>
				<div class="col-md-12 mt10">
					<p class="fwb">รายละเอียด</p>
					<textarea name="Call_Detail" style="width: 100%; height: 100px;"></textarea>
				</div>
			</div>

			<div class="clearb t_c mt15"><input type="submit" value="บันทึก" class="btn btn-success"></div>
		</div>
	</form>
</div>