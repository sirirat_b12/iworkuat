<?php 
	session_start();
	ini_set('upload_max_filesize', '512M');
	ini_set("memory_limit","512M");
	if(!isset($_SESSION["User"]['UserCode'])){
		echo "<script>alert('กรุณาเข้าสู่ระบบ!!!');</script>";
		echo '<META http-equiv="refresh" content="0;URL=login.php">';
		exit();
	} 
	// if($_SESSION["User"]['type'] == "CallCenter"){
	// 	echo '<META http-equiv="refresh" content="0;URL=chkinser.php">';
	// 	exit();
	// }
	header('Content-Type: text/html; charset=utf-8');
	include "include/inc_header.php"; 
	include "include/inc_menu.php"; 
	$UserCode = $_SESSION["User"]['UserCode'];

	$status = $_GET["filter"] ? $_GET["filter"] :2;
	$sup_code = $_GET["sup_code"] ? $_GET["sup_code"] :"";
	
	if($_SESSION["User"]['type'] == "QualityControl"){
		$send_orders = getAllSendPO($UserCode, "QualityControl", $status, $_GET["pocode"], $sup_code);

	}else if($_SESSION["User"]['type'] == "Sale" || $_SESSION["User"]['type'] == "Accounting"){
		if($_GET["pocode"]){
			// $send_orders = getAllSendPO($UserCode, "", $status, $_GET["pocode"]);
			$send_orders = getAllSendPO($UserCode, "", $status, $_GET["pocode"], $sup_code);
		}else{
			// $status = $_GET["filter"] ? $_GET["filter"] :2;
			// $send_orders = getAllSendPO($UserCode, "", $status, $_GET["pocode"]);
			$send_orders = getAllSendPO($UserCode, "", $status, "", $sup_code);
		}

	}else if($_GET["pocode"]){
		$send_orders = getAllSendPO("", "", $status, $_GET["pocode"], $sup_code);

	}else{
		// $status = $_GET["filter"] ? $_GET["filter"] :2;
		$send_orders = getAllSendPO("", "", $status, "", $sup_code);
	}
	// echo "<pre>".print_r($send_orders,1)."</pre>";
	$companyIns = getCompanyBysrv();
	$getPremium = getPremium();
	$getInsuranceType = getInsuranceType();

	$getCompanyBysrv = getCompanyBysrv();
	$suplist = getsuplist();

	$sup_dropdown = "";	
	foreach($suplist as $key_s => $sup){
		$selected = "";
		if($sup["personnel_code"] == $sup_code){
			$selected = 'selected';
		}
		$sup_dropdown .= "<option value='".$sup["personnel_code"]."' $selected>".$sup["sup_name"]."</option>";
	}
?> 
<div class="main">
	<div class="main-content p20">
		<div class=" bgff"> 
			<div class="p20">
				<div>
					<!-- <h3 >รายการแจ้งงาน</h3> -->
					<div class="" id="fromSendWork">
						<div class="row p15">
							<!-- <div class="col-md-2 p15">
								<div class="fs22 fwb">รายการแจ้งงาน</div>
							</div> -->
							<div class="col-md-6 bgfffbd8 p15">
								<div class="col-md-4" class="text-r">
									<div class="form-group">
										<input class="form-control formInput2" type="text" name="po_code_search" id="po_code_search" placeholder="เลข PO หรือ ลูกค้า" required>
									</div>
								</div>
								<div class="col-md-3" class="text-r">
									<div class="form-group">
										<input type="radio" name="search_type" value="1" checked> รหัส PO | 
										<input type="radio" name="search_type" value="2" > รหัสลูกค้า
									</div>
								</div>
								<div class="col-md-1 text-r">
									<div class="form-group" class="text-c">
										<button type="button" class="btn btn-success" data-toggle="modal" data-target="#ModalSend"><i class="fa fa-search-plus" aria-hidden="true"></i>  ค้นข้อมูล</button>
									</div>
								</div>
							</div>
						</div>
						
					</div>

					<div class="clearb">
						<div class="mt15">
							<div class="col-md-2 mb15">
								<label for="filterStatus">สถานะ</label>
								<select class="filterStatus form-control fs12" data-tableId="table1" onchange="filterTable()">
									<?php if($_SESSION["User"]['type'] == "QualityControl"){ ?>
										<!-- <option value="0" <?php if($status == 0){ echo "selected";} ?>>รอตรวจ</option> -->
										<option value="1" <?php if($status == 1){ echo "selected";} ?>>ไม่ผ่าน</option>
									<?php }else{ ?>
										<!-- <option value="0" <?php if($status == 0){ echo "selected";} ?>>รอตรวจ</option> -->
										<option value="1" <?php if($status == 1){ echo "selected";} ?>>ไม่ผ่าน</option>
										<option value="2" <?php if($status == 2){ echo "selected";} ?>>รอแจ้งงาน</option>
										<option value="3" <?php if($status == 3){ echo "selected";} ?>>แจ้งงาน</option>    
										<option value="4" <?php if($status == 4){ echo "selected";} ?>>เสร็จ</option>
									<?php } ?>
								</select>
							</div>
							
							<div class="col-md-2 mb15">
								<label for="filter">Supervisor</label>
								<select class="form-control fs12" id="sup_code" onchange="filterTable()">
									<option value="">select</option>
									<?php echo $sup_dropdown;	?>			
								</select>
							</div>
							
							<div class="col-md-2 mb15">
								<label for="filter">PO</label>
								<input type="text" class=" form-control fs12" id="pocodeSe" value="<?php echo $_GET["pocode"];  ?>">
							</div>
							<div class="col-md-2 mb15">
								<a href="sendorders.php" class="btn btn-danger mt20">รีเช็ต</a>
								<span class="btn btn-success mt20" onclick="filterPOcode()">ค้นหา</span>
							</div>
						</div>
						<div class="mt15">
							<table id="table" class="table table-striped p5" data-toggle="table" <?php if($_SESSION["User"]['type'] != "QualityControl"){ ?> data-detail-view="true" <?php } ?>  data-toggle="table" data-pagination="true" data-page-size="100" data-page-list="[100, 150, 200, 250, 300]">
								<thead>
									<tr>
										<th class="t_c"><i class="fa fa-list-ul" aria-hidden="true"></i></th>
										<th data-field="statusType" class="t_c">สถานะ</th>
										<th data-field="noti_work_code" class="t_c dn"></th>
										<th data-field="pocode" data-sortable="true" class="t_c">PO</th>
										<th data-field="start_cover_date" data-sortable="true" class="t_c">วันคุ้มครอง</th>
										<th class="t_c">ชื่อ-นามสกุล</th>
										<th class="t_c">แพจเกต</th>
										<th class="t_c">ทุนประกัน</th>
										<th class="t_c">ชำระ</th>
										<th class="t_c">ผู้ขาย</th> 
										<th class="t_c">ผู้ตรวจ</th>
									</tr>

								</thead>
								<tbody class="fs12">
									<?php 
									//if($apiMain){
									foreach ($send_orders as $key => $value) { 
										if($value["urgent"] == "1"){
											$bgColor = "class='bgNEW'"; 
										}else if($value["start_cover_date"] == date("Y-m-d")){
											$bgColor = "class='bgbaf4bc'";
										}else if($value["status"] == "1"){
											$bgColor = "class='bgffeee8'";
										}else{
											$bgColor = "";
										}

										if($value["enable"] == "0"){
											$enable = "<span style='color: #f44;'>ยกเลิก</span>";
										}else{
											$enable = "<span style='color: #009710;'>เปิดใช้</span>";
										}

										if($_SESSION["User"]['type'] != "Sale"){
											if($value["status"] == 0 && $value["datetime_chk"]){
												$bgColor = "class='bgfffbd8'";
											}
										}

										if($value["endorse"] == "1"){
											$endorse = "<span style='color: #f44;'>*สลักหลัง*</span>";
										}else{
											$endorse ="";
										}
										?>
										<tr <?php echo $bgColor; ?> data-status="<?php echo $value["status"]; ?>" data-inser="<?php echo $value["insuere_company"]; ?>">
											<td class="wpno t_c"> 
												<?php if($_SESSION["User"]['type'] != "Admin"){ ?>
													<?php if($value["status"] == 1 ){ ?>
														<a href="workout.php?pocode=<?php echo $value["po_code"];  ?>&numcode=<?php echo $value["noti_work_code"];  ?>" style="color: #003cff;">
															<i class="fa fa-cog"></i></a> | 
															<span class="fs14 btnselecttitle" style="color: #ff0000;" onclick="changEnable('<?php echo $value["noti_work_id"]?>','<?php echo $value["noti_work_code"]; ?>',0);"><i class="fa fa-trash"></i></span> 
														<?php }else{ ?>
															<a href="workout.php?pocode=<?php echo $value["po_code"];  ?>&numcode=<?php echo $value["noti_work_code"];  ?>" style="color: #003cff;"><i class="fa fa-search-plus" aria-hidden="true"></i></a>
															<?php if($value["status"] == 0 ){ ?>
																| <span class="fs14 btnselecttitle" style="color: #ff0000;" onclick="changEnable('<?php echo $value["noti_work_id"]?>','<?php echo $value["noti_work_code"]; ?>',0);"><i class="fa fa-trash"></i></span>
															<?php } ?>

														<?php } ?>

													<?php }else if($_SESSION["User"]['type'] == "QualityControl"){ ?>
														<?php if($value["status"] == 0 && $UserCode == $value["chk_code"]){ ?>
															<a href="sendorderslists.php?pocode=<?php echo $value["po_code"];  ?>&numcode=<?php echo $value["noti_work_code"];  ?>"><i class="fa fa-list-ul" aria-hidden="true"></i></a> | 
															<a href="sendordersviews.php?pocode=<?php echo $value["po_code"];  ?>&numcode=<?php echo $value["noti_work_code"];  ?>" style="color: #003cff;"><i class="fa fa-search-plus" aria-hidden="true"></i></a>
														<?php }else{ ?>
															<a href="sendordersviews.php?pocode=<?php echo $value["po_code"];  ?>&numcode=<?php echo $value["noti_work_code"];  ?>" style="color: #003cff;"><i class="fa fa-search-plus" aria-hidden="true"></i></a>
														<?php } ?>
														
														<?php if($UserCode == $value["personnel_code"]){ ?>
															| <a href="sendordersviews.php?pocode=<?php echo $value["po_code"];  ?>&numcode=<?php echo $value["noti_work_code"];  ?>" style="color: #003cff;"><i class="fa fa-search-plus" aria-hidden="true"></i></a>
															| <span class="fs14 btnselecttitle" style="color: #ff0000;" onclick="changEnable('<?php echo $value["noti_work_id"]?>','<?php echo $value["noti_work_code"]; ?>',0);"><i class="fa fa-trash"></i></span> 
														<?php } ?>

													<?php }else{ ?>
														<a href="workout.php?pocode=<?php echo $value["po_code"];  ?>&numcode=<?php echo $value["noti_work_code"];  ?>" style="color: #003cff;"><i class="fa fa-search-plus" aria-hidden="true"></i></a> | 
														<a href="sendorderslists.php?pocode=<?php echo $value["po_code"];  ?>&numcode=<?php echo $value["noti_work_code"];  ?>" style="color: #ac056c;"><i class="fa fa-list-ul" aria-hidden="true"></i></a>
													<?php } ?>
												</td>
												<td class="wpno t_c rowStatus" data-type="status_<?php echo $value["status"]; ?>">
													<div><?php echo setStatus($value["status"]); ?></div>
													<div style="color: #ff0000;" class="fs14 fwb" ><?php echo $endorse; ?></div>
												</td>
												<td class="t_l dn"><?php echo $value["noti_work_code"]; ?></td>
												<td class="t_l rowpo"> 
													<b ><a href="listpayment.php?po=<?php echo $value["po_code"]; ?>" target="_bank" style="color: #ff2da5;"><?php echo $value["po_code"]; ?></a></b>
													<div class="clearfix fs10"><?php echo $value["noti_work_code"]; ?></div>
													<div class="fs12"><?php echo ($value["enable"]==1) ? "<span class='c00ac0a'>ใช้งาน</span>" : "<span class='cf80000'>ยกเลิก</span>"; ?></div>
												</td>
												<td><span class="cf40053 fwb"><?php echo date("d-m-Y",strtotime($value["start_cover_date"])); ?></span></td>
												<td class="t_l <?php echo $bgColor; ?>">
													<div>
														<b><?php echo $value["cus_name"]; ?></b><?php if($value["status"] == 4) { echo "<div class='c2457ff'>".$value["policy_number"]."</div>"; } ?>
													</div>
													<div class="cff2da5 fwb fs14"><?php echo $value["operation_type"] ? "++".$value["operation_type"]."++" : ""; ?></div>
												</td>
												<td class="t_l rowInser" data-type="<?php echo $value["insuere_company"]; ?>">
													<span class="c2457ff"><?php echo $value["insuere_company"]; ?></span> | <span class="cf40053"><?php echo $value["insurance_type"]; ?></span></div>
													<div class="clearfix c9c00c8"><?php echo $value["package_name"]; ?></div>
													<div class="clearfix "><?php echo $value["send_type"] ? "<b>การส่ง :</b> ".$value["send_type"] : ""; ?></div>
												</td>
												<td class="t_r"><?php echo number_format($value["insuere_cost"],2); ?></td>
												<td class="t_r">
													<div class="clearfix "><b>สุทธิ: </b><?php echo number_format($value["netpremium"],2); ?></div>
													<div class="clearfix "><b>รวมภาษี: </b><?php echo number_format($value["premium"],2); ?></div>
													<div class="clearfix "><b>จ่าย: </b><?php echo number_format($value["taxamount"],2); ?></div>
												</td>
												<td class="t_l wpno">
													<div ><span class="c2457ff "><?php echo $value["personnel_name"]; ?></span></div>
													<div><span style="color: #ff2da5;"><?php echo date("d-m-Y H:i:m",strtotime($value["created_date"])); ?></span></div>
													
												</td>
												<!-- <td ><span style="color: #ff2da5;"><?php echo date("d-m-Y H:i:m",strtotime($value["created_date"])); ?></span></td> -->
												<td class="t_l wpno">
													<div class="clearfix "><?php echo $value["chk_name"]." ".$value["chk_lname"]; ?></div>
													<div class="clearfix fs10 c9c00c8">เริ่ม: <?php echo $value["datetime_chk_open"]; ?></div>
													<div class="clearfix fs10 cff2da5">เสร็จ: <?php echo $value["datetime_chk"]; ?></div>
												</td>
											</tr>
										<?php }
								//} ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>

<div class="modal fade" id="ModalSend" tabindex="-1" role="dialog" aria-labelledby="memberModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-send">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title" id="memberModalLabel">เลือก PO แจ้งงาน</h4>
			</div>
			<div class="dash"></div>
		</div>
	</div>
</div>

<?php include "include/inc_footer.php"; ?> 
<!-- <script src="//code.jquery.com/jquery-3.2.1.min.js"></script> -->
<!-- <script src="//code.jquery.com/jquery-3.2.1.min.js"></script> -->
<!-- <script src="fancybox/dist/jquery.fancybox.min.js"></script> -->
<!-- Magnify Image Viewer CSS -->
<link href="magnifybox/css/jquery.magnify.css" rel="stylesheet">
<!-- Magnify Image Viewer JS -->
<script src="magnifybox/js/jquery.magnify.js"></script>
<style>
	body .modal-send {
		width: 70%;
		/*margin-left: -375px;*/
	}
</style>
<script type="text/javascript">

	$('#ModalSend').on('show.bs.modal', function (event) {
		var poCode = $("#po_code_search").val();
		var search_type = $("input[name=search_type]:checked").val();
		var button = $(event.relatedTarget) // Button that triggered the modal
		var recipient = button.data('whatever') // Extract info from data-* attributes
		var modal = $(this);
		var dataString = 'poCode='+poCode+"&search_type="+search_type;

		$.ajax({
			type: "POST",
			url: "include/popup_send.php",
			data: dataString,
			cache: false,
			success: function (data) {
		          // console.log(data);
		          modal.find('.dash').html(data);
		      },
		      error: function(err) {
		          // console.log(err);
		      }
		  });  
		});

	var $table = $('#table');
	$table.on('expand-row.bs.table', function (e, index, row, $detail) {
		var noti_id = row.noti_work_code; 
		var res = $("#desc" + index).html(); 
		$.ajax({ 
			url: 'include/inc_action_chk.php',
			type:'POST',
			data: {action:"getImageOrNote", noti_id:noti_id},
			success:function(rs){
			// console.log(rs);
			$detail.html(rs);
		}
	});
	});

	// $('[data-fancybox="imgTable"]').fancybox({
	// 	buttons : [
	// 	'download',
	// 	'zoom',
	// 	'close'
	// 	]
	// });

	function chknum(ip,ek) {
		if (ek.code == "Comma") { 
			ip.value = ip.value.replace(/,/g, "");
			return true;
		}
		if(isNaN(ip.value)){
			alert('กรุณากรอกเป็นหมายเลขเท่านั้น');
			ip.value = "";
			return true;
		}
		return true;
	}

	function filterTable(val){
		var filterStatus 	= 	$(".filterStatus").val();
		var sup_code		=	$("#sup_code").val();
		if(filterStatus){
			window.location.href = "sendorders.php?filter="+filterStatus+"&sup_code="+sup_code;
		}
	}

	function queryParams() {
		return {
			type: 'owner',
			sort: 'updated',
			direction: 'desc',
			per_page: 1,
			page: 1
		};
	}


	function filterPOcode(){
		pocodeSe = $("#pocodeSe").val();
		if(pocodeSe){
			window.location.href = "sendorders.php?pocode="+pocodeSe;
		}else{
			alert("กรุณากรอก PO เพื่อค้นหา");
		}
	}

	function filterInser(){
		var filterInser = $(".filterInser").val();  
		var row = $('tbody tr'); 
		if(filterInser){ 
			row.hide();
			row.each(function(i, el) {
				datainser = $(el).attr('data-inser');
				if(datainser == filterInser) {
					$(el).show();
				}
			});

		}else{
			row.show();
		} 
	}

	$(".filterPO").keyup(function() {
		var searchText = $(this).val().toLowerCase();
		$.each($("#table tbody tr .rowpo"), function() {
			if($(this).text().toLowerCase().indexOf(searchText) === -1)
				$(this).parent().hide();
			else
				$(this).parent().show();                
		});
	});

	$(document).ready(function() {
		$(".btnOrders").click(function() {
			var countCK = $('.chkpo:checked').length;
			if(countCK > 0){
				$(".btnOrders").hide();
				$("#form_send_order").submit();
			}else{
				alert("กรุณาเลือก PO ที่ต้องการแจ้งงาน");
			}
		});
	});


	function changEnable(id, codes, status){
		if(confirm("ต้องการยกเลิกการแจ้งงานนี้?")){
			$.ajax({ 
				url: 'include/inc_action_chk.php',
				type:'POST',
				data: {action: 'delPONOti', id:id, code:codes, status:status},
				success:function(rs){
					window.location.reload(true);
				}
			});
		}
	}

function ValidateSingleInput(oInput) {
	var _validFile = [".jpg", ".jpeg", ".pdf",".png",];    
	if (oInput.type == "file") {
		var sFileName = oInput.value;
		if (sFileName.length > 0) {
			var blnValid = false;
			for (var j = 0; j < _validFile.length; j++) {
				var sCurExtension = _validFile[j];
				if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
					blnValid = true;
					break;
				}
			}

			if (!blnValid) {
				alert("กรุณาเลือกไฟล์ ที่มีนามสกุล PNG | JPG | PDF เท่านั้น" );
				oInput.value = "";
				return false;
			}
		}
	}
	return true;
}

function isImage(filename) {
	var ext = getExtension(filename);
	switch (ext.toLowerCase()) {
		case 'jpg':
		case 'png':
		case 'pdf':
		return true;
	}
	return false;
}

function btnComments(e){
	var val = e.id;
	console.log(e);
	code = val.split("ments_");
	mentID = code[1];
	mentTxt = $("#comment_"+mentID).val();
	console.log("#listMent_"+mentTxt);
	if(mentTxt != 0){
		$.ajax({ 
			url: 'include/inc_action_chk.php',
			type:'POST',
			data: {action: 'addComments', noti_work_code:mentID, comment:mentTxt},
			success:function(rs){
			// console.log("#listMent_"+mentID);
			$("#listMent_"+mentID).empty();
			$("#comment_"+mentID).val(0);
			$("#listMent_"+mentID).append(rs);
		}
	});
	}else{
		alert("กรุณาเลือก Comment");
	}
}

</script>