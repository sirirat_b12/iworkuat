<?php 
ini_set("memory_limit","1200M");
session_start();
if(!isset($_SESSION["User"]['UserCode'])){
	echo "<script>alert('กรุณาเข้าสู่ระบบ!!!');</script>";
	echo '<META http-equiv="refresh" content="0;URL=login.php">';
	exit();
}
header('Content-Type: text/html; charset=utf-8');
// include "include/inc_header.php"; 
include "inc_config.php";
// include "include/inc_function.php";
include "include/inc_function_chk.php"; 

function genBarcode($po,$MyBarCode){
	$font = new BCGFont('barcodeClsass/font/Arial.ttf', 10);
	$colorFront = new BCGColor(0, 0, 0);
	$colorBack = new BCGColor(255, 255, 255);
	$color_white = new BCGColor(255, 255, 255); 

	$barcode = $MyBarCode;
	$code = new BCGcode128();
	$code->setScale(1);
	$code->setThickness(30);
	$code->setForegroundColor($colorFront);
	$code->setBackgroundColor($colorBack);
	$code->setFont(0);
	$code->setStart(1);
	$code->setTilde(true);
	$code->parse($barcode);

	$purl = "picBarcode/".$po.".png";
	$drawing = new BCGDrawing($purl, $color_white);
	$drawing->setBarcode($code);
	$drawing->draw();
	$drawing->finish(BCGDrawing::IMG_FORMAT_PNG);
}

$pocode = $_GET["pocode"];
$insID = $_GET["insID"];
$getPO = getPurchaseBycode($pocode);
if(!$getPO){
	echo '<META http-equiv="refresh" content="0;URL=ordersadmins.php">';
	exit();
}

include("mpdf/mpdf.php");
include('barcodeClsass/BCGFont.php');
include('barcodeClsass/BCGColor.php');
include('barcodeClsass/BCGDrawing.php'); 
include('barcodeClsass/BCGcode128.barcode.php');

delMyPicBarcode();


$Installment = getRowInstallment($pocode, $insID);
$poRe = str_replace("PO", "", $pocode);
$cusID = str_replace(array("P","C"), "", $getPO["Customercode"]);
$price = number_format($Installment["ISTM_Total_Amount"],2,".","");
$po_ins = $poRe."0".$insID;
$genBarcode = "|024554000009700".chr(10)."".$po_ins."".chr(10)."".$cusID."".chr(10)."".str_replace(".", "", $price);
$MyBarCode = genBarcode($po_ins, $genBarcode);
$barcode = "|024554000009700 ".$po_ins." ".$cusID." ".str_replace(".", "", $price);
// echo "<pre>".print_r($Installment,1)."</pre>";


$mpdf = new mPDF('UTF-8','A4','','',5,5,5,5,10,10);
$mpdf->autoScriptToLang = true;

$html = '
<style>
	.container{
		font-family: thsaraban;
	    font-size: 16px;
	    line-height: 18px;
	    color:#000;
	}
	table{
		font-family: thsaraban;
	  font-size: 16px;
	  line-height: 15px;
	  color: #000;
	}
	p{
	    text-align: justify;
	}
	h1{
	    text-align: center;
	}
	.tableCar th{
		padding: 5px;
    border-bottom: 1px solid #000;
    border-top: 1px solid #000;
    border-right:1px solid #000;
    text-align:center;
  }
	.tableCar td {
		line-height: 15px;
		border-bottom: 1px solid #000;
		border-right:1px solid #000;
	}
	.tableCar th:last-child, .tableCar td:last-child{ border-right:none;}
	.{ font-size:10px;}
	.fs12{ font-size:12px;}
	.fs14{ font-size:14px;}
	.fs16{ font-size:16px;}
	.fs18{ font-size:18px;}
	.fs20{ font-size:20px;}
	.fwb{font-weight: bold;}
	.t_c{text-align: center;}
	.t_l{text-align: left;}
	.t_r{text-align: right;}
	.t_c{text-align: center;}
	.t_l{text-align: left;}
	.t_r{text-align: right;}
	.mt5{margin-top:5px;}
	.mt10{margin-top:10px;}
	.mt15{margin-top:15px;}
	.mt20{margin-top:20px;}
	.mt30{margin-top:30px;}
	.mt40{margin-top:40px;}
	.mt50{margin-top:50px;}
	.mb5{margin-bottom:5px;}
	.mb10{margin-bottom:10px;}
	.mb15{margin-bottom:15px;}
	.ml5{margin-left:5px;}
	.ml10{margin-left:10px;}
	.ml15{margin-left:15px;}
	.ml20{margin-left:20px;}
	.ml52{margin-left:52px;}
	.m0{margin:0px}
	p{margin:0}
	.fl{float: left; }
	.p5{padding:5px}
	.p10{padding:10px}
	.pt5{padding-top:5px}
	.pb10{padding-bottom:10px;}
	.bgea{background-color: #eaeaea;}
	.bor_1{border:1px solid #000;}
	.bor_t{border-top:1px solid #000;}
	.bor_l{border-left:1px solid #000;}
	.bor_r{border-right:1px solid #000;}
	.bor_b{border-bottom:1px solid #000;}
	.boxCoverage{
		line-height: 18px;
		float: left; 
		text-align:center; 
		width: 33.20%;
	}
</style>';
$html .= '
		<div class="container fs16" style="margin-top: 50px;">
			<p class="t_r">'.date("d/m/Y H:i:s").'</p>
			<div class="bor_l bor_r bor_b" style="line-height:18px;">
				<div class="bor_t">
					<div class="fl" style="width:60%">
						<div class="p10">
							<div style="float: left;margin-right: 5px; width:100px;"><img src="img/logo_110.png" alt="" width="70px;"></div>
							<div class="fl ">
								<p><b>เลขประจำตัวผู้เสียภาษี 0245540000097</b></p>
								<p>บริษัท เอเชียไดเร็ค อินชัวรันส์ โบรคเกอร์ จำกัด</p>
								<p>Asia Direct Insurance Broker Co.,Ltd.</p>
								<p>เลขที่ 626 อาคารบีบีดี (พระราม 4) ชั้น 11 ถนนพระรามที่ 4</p>
								<p>แขวงมหาพฤฒาราม เขตบางรัก กรุงเทพฯ 10500</p>
							</div>	
						</div>
					</div>
					<div class="fl bor_l">
						<div class="p5 mt5">
							<div >
								<p><b>สาขา</b>.......................................... <b>วันที่</b> .............................</p>
								<p><b>SERVICE CODE : ADB</b></p>
								<p><b>ชื่อ-นามสกุล /Name</b> : '.$getPO["Customer_FName"]." ".$getPO["Customer_LName"].'</p>
								<p><b>หมายเลขใบสั่งซื้อ / Ref. 1</b> : '.$poRe.' </p>
								<p><b>รหัสประจำตัวลูกค้า / Ref. 2</b> : '.$cusID.'</p>
								<p><b>ค่าประกันงวดที่ '.$insID.' ชำระภายในวันที่ '.$Installment["Installment_Due_Date"]->format("d/m/Y").'</p>
								<p><b>ยอดชำระ</b> '.number_format($price,2).' <b>บาท</b></p>
							</div>	
						</div>
					</div>
				</div>
			</div>
			<div class="bor_l bor_r " style="clear: both;">
				<div class="p5">
					<div>
						<input id="checkBox" type="checkbox">
						<span style="margin-left:15px" ><img src="img/logo_bangkok.png" width="15px;"></span><span style="margin-left:15px">&nbsp;&nbsp;บมจ. ธนาคารกรุงเทพ / Bangkok Bank (Br.no. 1161) (Comcode : 34811)</span>
					</div>

					<div>
						<input id="checkBox" type="checkbox">
						<span style="margin-left:15px" ><img src="img/logo_kbank.png" width="15px;"></span><span style="margin-left:15px">&nbsp;&nbsp;บมจ.ธนาคารกสิกรไทย / Kasikorn Bank เลขที่บัญชี 630-1-00382-8 สาขา จามจุรีสแควร์</span>
					</div>
					<div>
						<input id="checkBox" type="checkbox">
						<span style="margin-left:15px" ><img src="img/cd-icon-tbank.png" width="15px;"></span><span style="margin-left:15px">&nbsp;&nbsp;บมจ.ธนาคารธนชาต / Thanachart Bank Service Code : 7315</span>
					</div>
					<div class="mt5">
						ชำระโดย &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
						<input id="checkBox" type="checkbox"> เงินสด/Cash &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
						<input id="checkBox" type="checkbox"> เช็ค/Cheque
					</div>
				</div>
			</div>
			<div class="bor_l bor_r bor_b" style="clear: both;">
				<div>
					<table class="tableCar fs12" style="width:100%;" border="0" cellspacing="0">
						<thead>
							<tr >
								<th>เช็คธนาคาร/สาขา Drawee Bank/Branch</th>
								<th>หมายเลขเช็ค/Cheque No.</th>
								<th>เช็คลงวันที่/Date</th>
								<th style="border-right:none;">จำนวนเงิน/Amount</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td style="width:30%; height:25px;" ></td>
								<td style="width:20%;"></td>
								<td style="width:20%;"></td>
								<td style="width:30%; text-align:center; border-right:none; font-size:12px" rowspan="2" ><b>'.number_format($price,2).'</b></td>
							</tr>
							<tr>
								<td colspan="3" class="p5 fs12"><p style="margin-top: 10px;"><b>เงินสด :</b></p></td>
							</tr>
						</tbody>
					</table>
					<p class="p5" style="margin-top: 5px;"><b>จำนวนเงิน(ตัวหนังสือ) /Amount in Words :</b></p>
				</div>
			</div>
			<div>
				<div class="mt10 t_c">
					<img src="picBarcode/'.$po_ins.'.png" alt="" width="300x" height="30px">
					<p class="t_c">'.$barcode.'</p>
				</div>
			</div>
			<p class="fs18 t_c mt20"><b>กรุณาส่งหลักฐานการชำระเงิน มาที่ แฟกซ์ : 02-089-2088 หรือส่งอีเมล์ : adb@asiadirect.co.th หรือ Line ID : @asiadirect</b></p>
		</div>
	';

// echo $html;
// echo $html;

$mpdf->WriteHTML($html);
$mpdf->Output("ADBIR-".$po_ins.".pdf","I");
exit;

?>