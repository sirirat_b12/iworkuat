<?php 

session_start();
if(!isset($_SESSION["User"]['UserCode'])){
	echo "<script>alert('กรุณาเข้าสู่ระบบ!!!');</script>";
	echo '<META http-equiv="refresh" content="0;URL=login.php">';
	exit();
} 
if($_SESSION["User"]['type'] != "Admin" && $_SESSION["User"]['type'] != "SuperAdmin"){
	echo '<META http-equiv="refresh" content="0;URL=chkinser.php">';
	exit();
}
header('Content-Type: text/html; charset=utf-8');
include "include/inc_header.php"; 
include "include/inc_menu.php";;
include "export_request_email.php"; 



//$code = "N180521530713";
$code = $_GET["code"];
$getNotiWorkByID = getNotiWorkByID($code);
$pocode = $getNotiWorkByID["po_code"];
$getNotiWorkFilesByID = getNotiWorkFilesByID($code);
$getPO = getPurchaseBycode($pocode);
$Cardetail = getCardetailBycode($pocode);
$getEmailInsur = getEmailInsur($getNotiWorkByID["insuere_company"]);
$getPersonnelCode = getPersonnelCode($getNotiWorkByID["personnel_code"]);
if($getPersonnelCode["personnel_supervisor"]){
	$getSupervisor = getSupervisor($getPersonnelCode["personnel_supervisor"]);
}

if($getNotiWorkByID["insurance_type"] != "พรบ."){
	$pdfFile = "pdffile/".pdf_export($pocode);
}

// echo "<pre>".print_r($getPersonnelCode,1)."</pre>";
$Operation_Type_ID = ($getPO["Operation_Type_ID"] == "N") ? "ใหม่" : "ต่ออายุ";
$subject = "แจ้งงาน...".$getNotiWorkByID["insuere_company"]." | ".$getNotiWorkByID["cus_name"]." | ทะเบียน ".$Cardetail["Plate_No"]." ".getProvinceBycode($Cardetail["Plate_Province"]);
?>
<div class="main">
	<div class="p20">
		<div class="bgff row"> 
				<div class="p20">
					<h4>อีเมล์ถึงบริษัทประกัน <a href="export_request.php?pocode=<?php echo $pocode; ?>&amp;case=D" target="_bank" class="c2457ff fwb"><?php echo $pocode; ?></a><span class=""></span></h4>
					<form action="include/inc_action_chk.php" enctype="multipart/form-data" method="post" id="frmSendMail">
							<input type="hidden" value="sendMailInsur" name="action">
							<input type="hidden" value="<?php echo $code; ?>" name="noti_work_code">
							<input type="hidden" value="<?php echo $getNotiWorkByID["po_code"]; ?>" name="po_code">
							<div class="col-md-6">
								<div class=" mt10">
									<div class="form-group">
									  <label for="subject">หัวข้อ <span class="cf80000"> *</span></label>
									  <input type="text" name="subject" class="form-control formInput2" value="<?php echo $subject ?>">
									</div>
								</div>
								<div class="mt20 clearfix">
									<div class="form-group">
									  <div for="subject">อีเมล์ <span class="cf80000"> *</span> </div>
									  	<div class="col-md-6">
									  		<input type="checkbox" name="mailsend[]" value="<?php echo $getPersonnelCode["personnel_email"]."| Sale | CC";?>" checked>
									  		<span class="ml5"><?php echo $getPersonnelCode["personnel_email"]." | Sale" ;?></span>
									  	</div>
									  <?php if($getPersonnelCode["personnel_supervisor"]){?>
									  	<div class="col-md-6">
									  		<input type="checkbox" name="mailsend[]" value="<?php echo $getSupervisor["personnel_email"]."| Supervisor | CC";?>" checked>
									  		<span class="ml5"><?php echo $getSupervisor["personnel_email"]." | Supervisor" ;?></span>
									  	</div>
									  <?php } ?>
									  	<div class="col-md-6">
									  		<input type="checkbox" name="mailsend[]" value="<?php echo $_SESSION["User"]['email']."|".$_SESSION["User"]['firstname']."  | CC" ;?>" checked>
									  		<span class="ml5"><?php echo $_SESSION["User"]['email']." | Admin" ;?></span>
									  	</div>
									  <?php foreach ($getEmailInsur as $key => $value) { 
									  		$typeSend = ($value["setting_cc"] == 1) ? "TO" : "CC";
									  	?>
									  	<div class="col-md-6">
									  		<input type="checkbox" name="mailsend[]" value="<?php echo $value["email"]."|".$value["name"]." | ".$typeSend ;?>" checked>
									  		<span class="ml5"><?php echo $value["email"]." | ".$value["name"] ;?></span>
									  	</div>
									  <?php } ?>
									  <div id="addEmail" class="clearfix"></div>
									  <div class="mt15 cursorPoin fwb cf40053 clearfix dib btn " onclick="fnaddEmail()">เพิ่มอีเมล์</div> 
									</div>
								</div>
								<div class="mt20 clearfix">
									<div class="form-group">
									  <label for="bodymail">รายละเอียด <span class="cf80000"> *</span></label>
									 	<div>
						        	<textarea name="bodymail" id="editor">
											  <strong>เรียน เจ้าหน้าที่ บริษัท <?php echo $getNotiWorkByID["insuere_company"] ?></strong>
											  <br><strong> เรื่อง แจ้งดำเนินการออกกรมธรรม์ <?php echo $getNotiWorkByID["insurance_type"] ?> <?php echo ($Cardetail["Repair_Type"] == "C") ? "ซ่อมห้าง" : "ซ่อมอู่" ;?></strong>
											  <hr />
											  <strong>แพจเกต : </strong><?php echo $getNotiWorkByID["package_name"] ? $getNotiWorkByID["package_name"] : "-"  ?>
											  <br><strong>เริ่มความคุ้มครอง : </strong><?php echo $getPO["Coverage_Start_Date"]->format('d/m/Y');?>
											  <br><strong>สิ้นความคุ้มครอง : </strong><?php echo $getPO["Coverage_End_Date"]->format('d/m/Y');?>
											  <br><strong>ลูกค้า : </strong><?php echo $getNotiWorkByID["cus_name"] ?>
											  <br><strong>ทะเบียน : </strong><?php echo $Cardetail["Plate_No"]." ".getProvinceBycode($Cardetail["Plate_Province"]);?>
											  <br><strong>หมายเหตุ : </strong><?php echo $getPO["PO_ID"] ?>
											  <hr />
											  <p>เอกสารแนบ</p>
											  <ul>
											  	<li>ใบคำขอ</li>
											  	<li>รายการจดทะเบียน</li>
											  	<li>ใบตรวจสภาพรถ</li>
											  	<li>สำเนาบัตรประชาชน</li>
											  	<li>รูปรถ</li>
											  	<li>รูปกล้อง</li>
											  </ul>
											 	<!-- <h2><big><strong><font color="#ff9900">** รบกวนขอเลขรับแจ้งกลับด้วย **</font></strong></big></h2> -->
											 	<a href="http://61.90.142.230/adbchk/notice_call.php?p=<?php echo base64_encode($getPO["PO_ID"]);?>" target="_bank"><h2><big><strong><font color="#ff9900"> >>> กดที่นี่ เพื่อบันทึกเลขรับแจ้ง <<< </font></strong></big></h2></a>
											  <hr />
											  	<strong><?php echo $_SESSION["User"]['firstname']." ".$_SESSION["User"]['lastname'] ?> | Sales Admin</strong>
													<br>บริษัท เอเชียไดเร็ค อินชัวรันส์ โบรคเกอร์ จำกัด
													<br>626 อาคารบีบีดี (พระราม4) ชั้น&nbsp;11 ถนนพระรามที่ 4 แขวงมหาพฤฒาราม เขตบางรัก กรุงเทพฯ 10500
													<br>อีเมล์: <?php echo $_SESSION["User"]['email'] ?> | โทร: <?php echo $_SESSION["User"]['phone'] ?> | แฟ๊กซ์:0-2089-2088
													<br>เวปไซต์: <a href="www.asiadirect.co.th" target="_blank">www.asiadirect.co.th</a> | <a href="www.facebook.com/AsiaDirectBroker" target="_blank">www.facebook.com/AsiaDirectBroker</a>
											</textarea>
									 </div>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class=" mt10">
									<div class="form-group clearfix">
									  <div for="subject">เอกสารประกอบ <span class="cf80000"> *</span></div>
									  <?php 
									  	foreach ($getNotiWorkFilesByID as $key => $value) { 
										  		$fileType = explode(".", $value["file_name"]);
												$path  = ($value["pathfile"]) ? $value["pathfile"]."/" : ""; 

										  	if($fileType[1] != "pdf"){
									  ?>
										  	<div class="mt10 col-md-3">
										  		<input type="checkbox" name="files[]" value="<?php echo 'myfile/'.$path.''.$value['file_name'] ;?>" checked>
										  		<a href="myfile/<?php echo $path."".$value["file_name"] ;?>" data-magnify="gallery" data-src="" data-group="a">
										  		  <img src="myfile/<?php echo $path."".$value["file_name"] ;?>" class="img-responsive rounded" style="height: 100px;display: inline-block;">
										  		</a>
										  	</div>
									  <?php }else{ ?>
										  	<div class="mt10 col-md-3">
									  			<input type="checkbox" name="files[]" value="<?php echo 'myfile/'.$path.''.$value['file_name'] ;?>" checked>
													<a href="myfile/<?php echo $path."".$value["file_name"] ;?>" target='_bank'><?php echo $value["file_name"] ;?></a>
												</div>
									  <?php }
									  } ?>
									  
									</div>
									<div id="addFile" class="clearfix"></div>
									<?php if($getNotiWorkByID["insurance_type"] != "พรบ."){ ?>
										<div class="clearfix mt15">
									  	<div>ใบคำขอ</div>
									  	<div class="mt10">
								  			<input type="checkbox" name="files[]" value="<?php echo $pdfFile ;?>" checked>
												<a href="<?php echo $pdfFile ;?>" target='_bank'>ADBIR-<?php echo $pocode ;?>.pdf</a>
											</div>
										</div>
									<?php } ?>
									<div class="clearfix mt15 cursorPoin fwb cf40053 clearfix dib btn" onclick="fnaddfile()">เพิ่มเอกสารประกอบ</div> 
									<div class="c00ac0a mt15 fs12">***ชื่อไฟล์ภาษาอังกฤษเท่านั้น***</div>
								  </div>
								</div>
								<hr>
								<div class="fs18 t_c c00ac0a dn" id="txtWait"> ... กรุณารอ ...</div>
								<div class="t_c clearb mt50"><input type="button" value="ส่งอีเมล์" class="btn btn-success" id="btnSubmit"></div>
							</div>
					</form>
				</div>
			
		</div>
	</div>
</div>

<?php include "include/inc_footer.php"; ?> 
<!-- Magnify Image Viewer CSS -->
<link href="magnifybox/css/jquery.magnify.css" rel="stylesheet">
<!-- Magnify Image Viewer JS -->
<script src="magnifybox/js/jquery.magnify.js"></script>
<script>
  CKEDITOR.replace( 'editor', {
    language: 'th',
    height: 400
	});
</script>
<script type="text/javascript">
	$( "#btnSubmit" ).click(function() {
		$("#txtWait").show();
		$("#btnSubmit").hide();
  	$( "#frmSendMail" ).submit();
	});
	function fnaddEmail(){
		
		$("#addEmail").append('<div class="col-md-6"><input type="text" name="mailsend[]" class="form-control formInput2"></div>');
	}
	
	function fnaddfile(){
		$("#addFile").append('<div class="col-md-6"><input type="file" name="files[]" class="form-control "></div>');
	}
</script>