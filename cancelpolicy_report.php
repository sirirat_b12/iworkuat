<?php 
session_start();
$UserCode = $_SESSION["User"]['UserCode'];
$userType = $_SESSION["User"]['type'];

if(!isset($UserCode)){
	echo "<script>alert('กรุณาเข้าสู่ระบบ!!!');</script>";
	echo '<META http-equiv="refresh" content="0;URL=login.php">';
	exit();
}

header('Content-Type: text/html; charset=utf-8');
include "include/inc_header.php"; 
include "include/inc_menu.php"; 
include "inscancel/inc_function_inscancel.php"; 


// echo "<pre>".print_r($getDataToAdmin,1)."</pre>";
$txtse = $_GET["txtse"] ? $_GET["txtse"] : date("Y-m");
if($txtse){
	$getPOCancelReport = getPOCancelReport($txtse);
}
?>

<div class="main">
	<div class="">
		<div class="p20">
			<div class="row">
				<div class="col-md-12">
					<div class="panel">
							<div class="panel-heading">
								<h4 class="panel-title"><b>รายงานยกเลิกกรมธรรม์ <?php echo date("F Y",strtotime($txtse));?></b></h4>
							</div>
							<div class="panel-body fs14">
								<div class="row">
									<div class="col-md-4 ">
										<span class="fs18 cff2da5 mr15">เดือน</span>
										<select name="mounth" id="mounthOpt" class="form-control formInput2 dib" style="width: 80%" >
											<option value="">:: กรุณาเลือก ::</option>
												<?php 
													$dateLast = ( date("Y") - 1)."-".date("m");
													for($i=1; $i<=12; $i++) { 
														$txtM = "+".$i." month";
														$dateForValue = date("Y-m",strtotime($txtM, strtotime($dateLast)));
												?>
													<option value="<?php echo $dateForValue ;?>" <?php if($txtse ==  $dateForValue){ echo "selected"; } ?>>
														<?php echo date("Y m F",strtotime($dateForValue)) ?>
													</option>
												<?php } ?>
										</select>
									</div>
									<div><sapn class="btn btn-info" onclick="btnExport()">Export</sapn></div>
								</div>
								<div class="row mt20">
									<div class="panel-body fs14">
										<table id="tablecp" class="table p5" data-toggle="table" data-pagination="true" data-page-size="200" data-page-list="[200, 300, 400]">
											<thead class="fs13 c000000">
												<tr>
													<th class="t_c">PO</th>	
													<th class="t_c">เริ่มคุ้มครอง</th>
													<th class="t_c">สิ้นคุ้มครอง</th>		
													<th class="t_c">เวลาทำรายการ</th>
													<th class="t_c">รับกรมลูกค้า</th>	
													<th class="t_c">แจ้งยกเลิก</th>
													<th class="t_c">ส่งกรมบ.ประกัน</th>
													<th class="t_c">รับสลักหลัง</th>
													<th class="t_c">รายละเอียดยกเลิก</th>
													<th class="t_c">ชื่อลูกค้า</th>	
													<th class="t_c">บริษัทประกัน</th>
													<!-- <th class="t_c">รายละเอียดประกัน</th>									 -->
													<th class="t_c">ทะเบียนรถ</th>	
													<th class="t_c">เบี้ยสุทธิ</th>
													<th class="t_c">ส่วนลด</th>
													<th class="t_c">เบี้ยรวม</th>	
													<th class="t_c">ลูกค้าจ่าย</th>	
													<th class="t_c">จ่ายมา</th>
												</tr>
											</thead>
											<tbody class="fs12">
												<?php 
													foreach ($getPOCancelReport as $key => $value) { 
														$Net_Premium = $value["Net_Premium"] ? $value["Net_Premium"] : $value["Compulsory"];
														$Create_Date = ($value["Create_Date"]->format("Y-m-d") != "1900-01-01") ? $value["Create_Date"]->format("d/m/Y") : "";
														$Broker_sendto_date = ($value["Broker_sendto_date"]->format("Y-m-d") != "1900-01-01") ? $value["Broker_sendto_date"]->format("d/m/Y") : "";
														$Endorse_date = ($value["Endorse_date"]->format("Y-m-d") != "1900-01-01") ? $value["Endorse_date"]->format("d/m/Y") : "";
													?>
													<tr >
														<td class="t_c c2457ff fwb"><?php echo $value["PO_ID"]; ?></td>
														<td class="t_l cf40053 "><?php echo $value["Coverage_Start_Date"]->format("d/m/Y"); ?></td>
														<td class="t_l cf40053 "><?php echo $value["Coverage_End_Date"]->format("d/m/Y"); ?></td>
														<td class="t_l">
															<div class="cff2da5"><?php echo $value["Create_Date"]->format("d/m/Y H:i:s"); ?></div>
															<div><?php echo $value["User_FName"]." ".$value["User_LName"]; ?></div>
														</td>
														<td class="t_l">
															<div class="c2457ff"><?php echo $value["Customer_sendto_date"]->format("d/m/Y H:i:s"); ?></div>
															<div><?php echo $value["Customer_sendto_date_by"]; ?></div>
														</td>
														<td class="t_l">
															<div class="c2457ff"><?php echo $value["Status_Emai_Date"]->format("d/m/Y H:i:s"); ?></div>
															<div><?php echo $value["Send_Email_by"]; ?></div>
														</td>
														<td class="t_l">
															<?php if($value["Broker_sendto_date"]->format("d/m/Y") != "01/01/1900"){ ?>
																<div class="c2457ff"><?php echo $value["Broker_sendto_date"]->format("d/m/Y H:i:s"); ?></div>
																<div><?php echo $value["Broker_sendto_date_by"]; ?></div>
															<?php } ?>
														</td>
														<td class="t_l">
															<?php if($value["Endorse_date"]->format("d/m/Y") != "01/01/1900"){ ?>
																<div class="c2457ff"><?php echo $value["Endorse_date"]->format("d/m/Y H:i:s"); ?></div>
																<div><?php echo $value["Endorse_by"]; ?></div>
															<?php } ?>
														</td>
														<td class="t_l">
															<div class="c00ac0a"><?php echo $value["Cancel_Status"]; ?></div>
															<?php if($value["Remark"]){?>
																<div><?php echo $value["Remark"]; ?></div>
															<?php } ?>
														</td>
														<td class="t_l fwb"><?php echo $value["Customer_FName"]." ".$value["Customer_LName"]; ?></td>
														<td class="t_l">
															<div class="cff2da5"><?php echo $value["Insurer_Name"]; ?></div>
															<div class="c2457ff"><?php echo $value["Insurance_Name"]; ?></div>
															<div><?php echo $value["Policy_No"]; ?></div>
														</td>
														<td class="t_l">
															<div class="c2457ff"><?php echo $value["Make_Name_TH"]." ".$value["Model_Desc"]; ?></div>
															<div class=""><?php echo $value["Plate_No"]." ".$value["Province_Name_TH"]; ?></div>
														</td>
														<td class="t_r c2457ff"><?php echo number_format($Net_Premium,2); ?></td>
														<td class="t_r c2457ff"><?php echo number_format($value["Discount"],2); ?></td>
														<td class="t_r cff2da5"><?php echo number_format($value["Premium_After_Disc"],2); ?></td>
														<td class="t_r c9c00c8"><?php echo number_format($value["SumPayment"],2); ?></td>
														<td class="t_c c2457ff"><?php echo $value["countIns"]."/".$value["countPayment"]; ?></td>
													</tr>
												<?php  } ?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php include "include/inc_footer.php"; ?> 
<script src="js/Chart.bundle.js"></script>
<script src="js/utils.js"></script>
<script type="text/javascript">

	$("#mounthOpt").on('change', function() {
		val = this.value;
		if(this.value){
			window.location.href = "cancelpolicy_report.php?txtse="+val;
		}
  });

  function btnExport(){
		txtse = $("#mounthOpt").val();
		if(txtse){
			window.open("inscancel/inscancel_export.php?txtse="+txtse);
			// window.location.href = "inscancel/inscancel_export.php?txtse="+txtse;
		}
	}
</script>