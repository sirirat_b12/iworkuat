<?php 
session_start();
ini_set('max_execution_time', 186);
ini_set('memory_limit', '2048M');
$UserCode = $_SESSION["User"]['UserCode'];
$userType = $_SESSION["User"]['type'];

if(!isset($UserCode)){
	echo "<script>alert('กรุณาเข้าสู่ระบบ!!!');</script>";
	echo '<META http-equiv="refresh" content="0;URL=login.php">';
	exit();
}

header('Content-Type: text/html; charset=utf-8');
include "include/inc_header.php"; 
include "include/inc_menu.php"; 
include "reports/inc_function_report.php";
// include('qrcode/qrcode.class.php');

$getMonthSelect = getMonthSelect();
$year = date("Y");
if($_GET["txtse"]){
	// $getMountListdate = getMountListdate($_GET["txtse"]);
	$getListQC = getListQC();
	$getDataCount = getDataCount($_GET["txtse"]);
}

if($_GET["date"]){
	$getDetailPOByQC = getDetailPOByQC($_GET["user"], $_GET["date"]);
}
// echo "<pre>".print_r($getDetailPOByQC,1)."</pre>";
?>
<div class="main">
	<div class="">
		<div class="p20">
				<div class="row">
					<div class="col-md-12">
						<div class="panel">
								<div class="panel-heading">
									<h4 class="panel-title"><b>รายงาน QC <?php echo "ปี ".$year;?></b></h4>
								</div>
								<div class="panel-body fs14">
									<div class="row">
										<div class="col-md-4 ">
											<span class="fs18 cff2da5 mr15">เดือน</span>
											<select name="mounth" id="mounthOpt" class="form-control formInput2 dib" style="width: 80%" >
												<option value="">:: กรุณาเลือก ::</option>
												<?php foreach ($getMonthSelect as $key => $value) { ?>
													<option value="<?php echo $value["months"];?>" <?php if($_GET["txtse"] ==  $value["months"]){ echo "selected"; } ?>>
														<?php echo date("F", strtotime($year."-".$value["months"]."-01")) ?>
													</option>
												<?php } ?>
											</select>
										</div>
									</div>
									<div class="row mt20">
										<div class="col-md-12">
											<div class="row">
												<div class="col-md-2"></div><div class="col-md-10"><?php echo $value["PO_ID"]; ?></div>
											</div>
											<?php if($getDataCount){?>
												<table class="table table-hover table-bordered" id="tableMoveQC">
													<thead>
														<tr class="bgbaf4bc">
															<th class="t_c">วันที่</th>
															<?php foreach ($getListQC as $key => $value) { ?>
																<th class="t_c"><?php echo $value["personnel_code"];?></th>
															<?php } ?>
															<th class="t_c">รวม</th>
														</tr>
													</thead>
													<tbody id="table_data" class="table-list fs13">
														<?php 
															foreach ($getDataCount as $key => $valDate) {
															$sumrow = 0; 
															$sumAllTime = "";
															$date = date("d F Y",strtotime($key));
															?>
															<tr>
																<td class="text-center"><?php echo $date;?></td>
																<?php 
																	foreach ($getListQC as $keyQC => $valQC) { 
																		$countNum = $getDataCount[$key][$valQC["personnel_code"]];
																		$Timesecond = $getDataCount[$key]["sumTime"][$valQC["personnel_code"]];
																		$sumTime = floor($Timesecond / 3600).":".floor(($Timesecond / 60) % 60).":".($Timesecond%60);
																		$sumrow = $sumrow+$countNum;
																		if($countNum){
																?>
																	<td class="t_c">
																		<span class="cursorPoin cff2da5" onclick="getDetailPO('<?php echo $valQC["personnel_code"];?>','<?php echo $key?>')"><?php echo $countNum."|".$sumTime; ?></span>
																	</td>
																<?php }else{ 
																				echo '<td class="t_c">-</td>';
																			}
																	} ?>
																<td class="text-center bgNEW fwb"><?php echo $sumrow;?></td>
															</tr>
														<?php }  ?>
														<?php $sumrowTotal =0;
																foreach ($getDataCount as $key => $valDate) {
																	foreach ($getListQC as $keyQC => $valQC) { 
																		$countNum = $getDataCount[$key][$valQC["personnel_code"]];
																		$sumAllTime[$valQC["personnel_code"]] += $getDataCount[$key]["sumTime"][$valQC["personnel_code"]];
																		$sumrowAll[$valQC["personnel_code"]] = $sumrowAll[$valQC["personnel_code"]] + $countNum;
																		$sumrowTotal = $sumrowTotal + $countNum;
																	} 
																} 
														?>
														<tr class="bgNEW fwb">
															<td class="t_c">รวม</td>
																<?php 
																	foreach ($getListQC as $keyQC => $valQC) { 
																?>
																<td class="t_c"><?php echo number_format($sumrowAll[$valQC["personnel_code"]])." | ".floor($sumAllTime[$valQC["personnel_code"]] / 3600).":".floor(($sumAllTime[$valQC["personnel_code"]] / 60) % 60).":".($sumAllTime[$valQC["personnel_code"]]%60) ;?></td>
																<?php } ?>
															<td class="t_c"><?php echo number_format($sumrowTotal);?></td>
														</tr>
													</tbody>
												</table>
											<?php } ?>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12">
											<div class="p15"><div id="headline-chart" class="ct-chart"></div></div>
										</div>
										<div class="col-md-12">
											<canvas id="canvas" height="80"></canvas>
										</div>
									</div>
									<?php if($_GET["date"]){?>
										<div class="row mt20">
											<table class="table table-hover" >
														<thead>
															<tr>
																<th class="t_c">#</th>
																<th class="t_c">PO</th>
																<th class="t_c" style="width: 10%">สถานะ</th>
																<th class="t_c" style="width: 20%">ลูกค้า</th>
																<th class="t_c" style="width: 10%">พนักงาน</th>
																<th class="t_c" style="width: 10%">ผู้ตรวจ</th>
																<th class="t_c" style="width: 25%">สรุปขั้นตอนตรวจ</th>
															</tr>
														</thead>
														<tbody id="tableListDetail" class="table-list fs13">
															<?php $i=1; foreach ($getDetailPOByQC as $key => $value) { 
																$status = ($value["enable"]==1) ? "<span class='fwb c00ac0a'>ใช้งาน</span>" : "<span class='fwb cf80000'>ยกเลิก</span>";;
															?>
																<tr>
																	<td><?php echo $i++; ?></td>
																	<td class="t_l">
																		<div class="fwb cff2da5">
																			<a target="_Blank" class="cff2da5" href="sendordersviews.php?pocode=<?php echo $value["po_code"] ?>&numcode=<?php echo $value["noti_work_code"] ?>"><?php echo $value["po_code"] ?> | <?php echo $value["noti_work_code"] ?></a>
																		</div>
																		<div><?php echo $value["insurance_type"] ?> | <?php echo $value["insuere_company"] ?></div>
																		<div class="c2457ff"><?php echo $value["package_name"] ?></div>
																	</td>
																	<td class="t_c"><?php echo setStatusPO($value["status"])." | ".$status; ?></td>
																	<td class="t_l"><?php echo $value["cus_name"] ?></td>
																	<td class="t_t">
																		<div><?php echo $value["personnel_name"] ?></div>
																		<div><?php echo date("d-m-Y H:i:m",strtotime($value["created_date"])) ?></div>
																	</td>
																	<td class="t_t">
																		<div><?php echo $value["chk_code"] ?></div>
																		<div class="clearfix fs10 c9c00c8">เริ่ม: <?php echo $value["datetime_chk_open"]; ?></div>
																		<div class="clearfix fs10 cff2da5">เสร็จ: <?php echo $value["datetime_chk"]; ?></div>
																		<div class="clearfix fs10 c2457ff fwb">
																			สรุป: <?php echo $value["datetime_chk_open"] ? diff2time($value["datetime_chk"],$value["datetime_chk_open"]) : "-"; ?>
																		</div>
																	</td>
																	<td class="t_t">
																		<?php foreach ($value["steplists"] as $key => $valStep) { ?>
																			<div class=" c2457ff"> >> <?php echo $valStep["comment"] ?></div>
																			<div class="fs10"><b><?php echo $valStep["datetime"] ?></b> | <?php echo $valStep["comment_byname"] ?></div>
																		<?php } ?>
																	</td>
																</tr>
															<?php } ?>
														</tbody>
													</table>
										</div>
									<?php } ?>
								</div>
						</div>
					</div>
				</div>
		</div>
	</div>
</div>
<?php include "include/inc_footer.php"; ?> 
<!-- <script src="fancybox/dist/jquery.fancybox.min.js"></script> -->
<script src="js/Chart.bundle.js"></script>
<script src="js/utils.js"></script>
<script type="text/javascript">

	$("#mounthOpt").on('change', function() {
		if(this.value){
			window.location.href = "report_qc.php?txtse="+this.value;
		}

  });

<?php if($_GET["txtse"]){?>
function getDetailPO(user, date){
	txtse = <?php echo $_GET["txtse"]; ?>;
	if(txtse){
		window.location.href = "report_qc.php?txtse="+txtse+"&user="+user+"&date="+date;
	}
}
<?php } ?>
  

function filterPOcode(){
	txtse = $("#txtse").val();
	if(txtse){
		window.location.href = "counters.php?txtse="+txtse;
	}else{
		alert("กรุณากรอก ข้อมูล เพื่อค้นหา");
	}
}

function random_rgba() {
    var o = Math.round, r = Math.random, s = 255;
    return 'rgba(' + o(r()*s) + ',' + o(r()*s) + ',' + o(r()*s) + ',' + r().toFixed(2) + ')';
}
<?php if($_GET["txtse"]){ ?>
window.onload = function() {
		var ctx = document.getElementById('canvas').getContext('2d');
		var seriescount = [];
		var data, options;
		var totalCount = 0;
		var totalPrice = 0.00;
		var mounth = $("#mounthOpt").val();
		$.ajax({ 
			url: 'reports/inc_action_report.php',
			type:'POST',
			dataType: 'json',
			data: {action: 'reportCharQC', mounth:mounth},
			success:function(rs){  
			var config = {
				type: 'line',
				data: {
					labels: rs.date,
					datasets: [
					<?php foreach ($getListQC as $keyQC => $valQC) { 
							echo "{ label: '".$valQC["personnel_code"]."', data: rs.".$valQC["personnel_code"].", 
							backgroundColor: random_rgba(),
							borderColor: random_rgba(), fill: false, },";
					}
					?>]
				},
				options: {
					responsive: true,
					legend: {
						position: 'bottom',
					},
					tooltips: {
						mode: 'index',
						intersect: false,
					},
					hover: {
						mode: 'nearest',
						intersect: true
					},
					elements: {
						point: {
							pointStyle: 'rectRot'
						}
					},
					scales: {
						xAxes: [{
							display: true,
							scaleLabel: {
								display: true,
								labelString: 'วันที่'
							}
						}],
						yAxes: [{
							display: true,
							scaleLabel: {
								display: true,
								labelString: 'จำนวน'
							}
						}]
					},
					title: {
						display: true,
						text: 'สถิติข้อมูล QC <?php echo date("F", strtotime($txtse))." ".date("Y") ?>'
					}
				}
			};
			window.myLine = new Chart(ctx, config);
		}
	});

};
<?php } ?>
</script>