<?php
session_start();
date_default_timezone_set("Asia/Bangkok");
header('Content-Type: text/html; charset=utf-8');
include "../inc_config.php";
include("../include/sms.class.php");
include "inc_function_kbank.php"; 


//config kbank 
$table_pay_installment  = 'pay_installment'; // ใช้เหมือนกัน omise , 2c2p , pay[kbank]
$table_pay_type			= 'pay_kbank'; // สร้างใหม่แล้วแต่ config 2c2p ได้เลย pay_omise 
$customer_path			= 'http://adbwecare.com/pay_kbank/';
$po_id_list = array();
$po_id_obj_list = array();
if($_POST["action"] == "inserListpay"){
	$reference_1 = getMaxIDRef1($table_pay_type);

	foreach ($_POST["inskey"] as $key => $value) {

		$val = explode("|", $value);
		$getInstallment= getInstallment($val[0],$val[1]);
		// echo "<pre>".print_r($getInstallment,1)."</pre>";exit;
 
		$ins_data["reference_1"]	= $reference_1;
		$ins_data["pay_type"]		= $table_pay_type;
		$ins_data["PO_ID"]			= $val[0];
		$ins_data["Installment_ID"]	= $val[1];
		$ins_data["Total_Premium"]	= $getInstallment["ISTM_Total_Premium"];
		$ins_data["Discount"]		= $getInstallment["ISTM_Discount"];
		$ins_data["WHT"]			= $getInstallment["ISTM_WHT"];
		$ins_data["Total_Amount"]	= $getInstallment["ISTM_Total_Amount"];
		$ins_data["Installment_Due_Date"]	= $getInstallment["Installment_Due_Date"]->format("Y-m-d");
		$po_id_list[] = str_replace("PO","",$val[0]);
		$po_id_obj_list[] = "PO:".$val[0]." งวดที่:".$val[1];

		$sql_install = " INSERT INTO [dbo].".$table_pay_installment." 
				([reference_1],[pay_type],[PO_ID],
				[Installment_ID],[Total_Premium],[Discount],
				[WHT],[Total_Amount],[Installment_Due_Date] )
				 VALUES ('".$ins_data["reference_1"]."', '".$ins_data["pay_type"]."', '".$ins_data["PO_ID"]."', 
				'".$ins_data["Installment_ID"]."', '".$ins_data["Total_Premium"]."', '".$ins_data["Discount"]."', 
				'".$ins_data["WHT"]."', '".$ins_data["Total_Amount"]."', '".$ins_data["Installment_Due_Date"]."'); " ;

		$rs_insert = sqlsrv_query( $connMS, $sql_install);

		$pay_amount += $getInstallment["ISTM_Total_Amount"]; // for next table

		// echo $sql_install; echo "<hr>";
	}



	$po_id_list = array_unique($po_id_list);
	// echo 'ย้าย table pay ที่ mysql มา sql server นะ'; echo "<hr>";

	// echo "<pre>".print_r($_POST,1)."</pre>";exit;  /////////////////////////////////////////////////////////////////////////////

	//

	$table_pay   = 'pay';
	$data_pay = array();
	$data_pay["pay_inv"] = date("ymdHis");
	$data_pay["pay_name"] = $_POST['name'];
	$data_pay["pay_discription"] = implode(",",$po_id_obj_list);
	$data_pay["pay_amount"] = str_replace(',','',$pay_amount);
	$data_pay["pay_from"] = $_SESSION["User"]['email'];
	$data_pay["pay_to"] = $_POST['email'];
	$data_pay["pay_subject"] = "แจ้งการชำระเงิน";
	$data_pay["pay_date"] = date('Y-m-d H:i:s');
	$data_pay["pay_ip"] = $_SERVER['REMOTE_ADDR'];
	$data_pay["pay_status"] = '0';
	$data_pay["pay_date_success"] =  '0000-00-00 00:00:00';
	$data_pay["pay_card_number"] =  " ";
	
	$rd = $conn3->autoExecute($table_pay ,$data_pay,'INSERT');
	$PayId = $conn3->Insert_ID();
 

	
	// for next table 
	$cus_id 	= $val[2];
	$create_by 	=$_SESSION["User"]['UserCode']; 
	$next_month = date("Y-m-d", strtotime ( '+1 month' , strtotime ( date("Y-m-d")))) ;
	$short_url  = shorturl($customer_path.'pay.php?action=pay&pcode='.base64_encode($PayId).'&ref1='.base64_encode($reference_1));
	$tel_sms	= $_POST["phone"];
 

	// ส่วนนี้เป็นของ omise -  , 2c2p , pay[kbank] สร้างใหม่ table นะ  Update pay_inv into pay_kplus
	$sql_type = "INSERT INTO [dbo].".$table_pay_type." ( 
					[reference_1],[pay_description],[amount],
					[Customer_ID],[email],[tel_sms],
					[expire_date],[create_date],[create_by],
					[link_ref],[short_url],[return_json] , [pay_inv] , [enable]) 
				VALUES (
					'".$ins_data["reference_1"]."', '".$_POST["remark"]."', '".$pay_amount."', 
					'".$cus_id."', '".$_POST["email"]."', '".$tel_sms."', 
					'".$next_month." ".date("H:i:s")."', '".date("Y-m-d H:i:s")."', '".$create_by."', 
					'".$ins_data["reference_1"]."', '".$short_url."', '".$ins_data["null"]."', '".$data_pay["pay_inv"]."' ,'Y'); ";
	$rs_type = sqlsrv_query($connMS, $sql_type);

	if($rs_type && ( $PayId > 0) ) {
		//  send sms
		$txt_sms = 'Asiadirect ยอดชำระ '.number_format($pay_amount,2).' บาท โปรดคลิก '.$short_url.' เพื่อชำระเงิน ติดต่อสอบถาม โทร. 020892000';
		$rs_sms = actionSendSMS($tel_sms, $txt_sms);
		echo "<script>alert('ดำเนินการเรียบร้อย โปรดตรวจสอบ');</script>";
		echo '<META http-equiv="refresh" content="0;URL=../pay_kbank.php?txtse='.$_POST["txtsearch_hidden"].'">';
		exit();

	} else {
		echo "<script>alert('ไม่สามารถทำรายการได้ กรุณาลองอีกครั้ง');</script>";
		echo '<META http-equiv="refresh" content="0;URL=../pay_kbank.php">';
		exit();
	}













} else if($_POST["action"] == "GetListCS"){
	$getPOPayForCounter = getPOPayForCounter($_POST["txtsearch"]);
	// echo "<pre>".print_r($getPOPayForCounter,1)."</pre>";
	$i=1;  
	$sum = 0; 
	foreach ($getPOPayForCounter as $key => $value) {
		$sum = $sum + $value["ISTM_Total_Amount"];
		if(!$value["ISTM_Total_Amount"]){
			if($value["Insurance_ID"] == "029" || $value["ISTM_Total_Amount"] == 0){ 
				$case = '<input type="checkbox" class="inskey" id="qrPrice_'.$i.'" name="inskey[]" value="'.$value["PO_ID"]."|".$value["Installment_ID"]."|".$value["Customer_ID"].'">';
			}else{
				$case =  "<sapn class='c2457ff'>ยอดไม่พอ</sapn>";
			}
		}else if($value["Installment_Status"] != "Paid" && $value["ISTM_Total_Amount"] > 0 &&  $value["enable"] != 'Y' ){
			$case = '<input type="checkbox" class="inskey" id="qrPrice_'.$i.'" name="inskey[]" value="'.$value["PO_ID"]."|".$value["Installment_ID"]."|".$value["Customer_ID"].'">';
		}else{
			$case =  "<sapn class='c2457ff'>สร้างชำระแล้ว</sapn>";
		}
		$txt .= '
		<tr >
			<td class="t_c">'.$case.'</td>
			<td class="wpno t_c" style="width: 5%;">'.$value["Installment_ID"].'</td>
			<td class="wpno t_c cff2da5" style="width: 10%;">'.$value["PO_ID"].'</td>
			<td class="wpno " >
					<div>'.$value["Customer_ID"].'</div>
					<div>'.$value["Customer_FName"]." ".$value["Customer_LName"].'</div>
				</td>
			<td class="t_c ">'.$value["Insurance_Name"].'</td>
			<td class="t_c ">'.$value["Installment_Due_Date"]->format("d/m/Y").'</td>
			<td class=" t_r"> <b>'.number_format($value["ISTM_Net_Premium"],2).'</b></td>
			<td class=" t_r"> <b>'.number_format($value["ISTM_Duty"],2).'</b></td>
			<td class=" t_r"> <b>'.number_format($value["ISTM_Tax"],2).'</b></td>
			<td class=" t_r"> <b>'.number_format($value["ISTM_Total_Premium"],2).'</b></td>
			<td class=" t_r"> <b>'.number_format($value["ISTM_Discount"],2).'</b></td>
			<td class=" t_r"> <b>'.number_format($value["ISTM_Total_Amount"],2).'</b></td>
			<td class=" t_c cff2da5"> <b>'.$value["Installment_Status"].'</b></td>
		</tr>
		<tr class="dn boxQRALL">
			<td class="wpno t_r" colspan="12">รวม</td>
			<td class="wpno t_c" ><div class="showQRAll">'.number_format($sum,2).'</div></td>
		</tr>';
		$i++;
	}
	echo $txt;
	exit();

}else if($_POST["action"] == "GetDataPhone"){
	$sql = "SELECT Customer_ID,Tel_No, Mobile_No, EMail,Customer_FName ,Customer_LName From [dbo].Customer WHERE Customer_ID = '".$_POST["txtsearch"]."' ";
	$stmt = sqlsrv_query( $connMS, $sql );
	$row = sqlsrv_fetch_array($stmt);
	$arr = array();
	$arr["phone"] 	= ($row["Tel_No"]) ? $row["Tel_No"] : $row["Mobile_No"];
	$arr["email"] 	= $row["EMail"];
	$arr["remark"] 	= 'เบี้ยประกันภัยรถยนต์ '.$row["Customer_ID"];
	$arr["name"] 	= $row["Customer_FName"].' '.$row["Customer_LName"];
	echo json_encode($arr);
	exit();
  // echo "<pre>".print_r($arr,1)."</pre>";
}


?>