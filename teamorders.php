<?php 

session_start();
if(!isset($_SESSION["User"]['UserCode'])){
	echo "<script>alert('กรุณาเข้าสู่ระบบ!!!');</script>";
	echo '<META http-equiv="refresh" content="0;URL=login.php">';
	exit();
} 
// if($_SESSION["User"]['type'] == "CallCenter"){
// 	echo '<META http-equiv="refresh" content="0;URL=chkinser.php">';
// 	exit();
// }


header('Content-Type: text/html; charset=utf-8');
include "include/inc_header.php"; 
include "include/inc_menu.php"; 
$UserCode = $_SESSION["User"]['UserCode'];
$status = $_GET["filter"] ? $_GET["filter"] : 0 ;
$send_orders = getAllSendPO($UserCode, "",$status, $_GET["pocode"]);
// echo "<pre>".print_r($send_orders,1)."</pre>";
$companyIns = getCompanyBysrv();
$getPremium = getPremium();
$getInsuranceType = getInsuranceType();

?> 
<div class="main">
	<div class="main-content p20">
		<div class=" bgff"> 
			<div class="p20">
				<div>
					<h3 >รายการแจ้งงาน Team (<?php echo $_SESSION["User"]['UserCode']; ?>)</h3>
				</div>
					<!-- <table id="table" class="table table-striped p5" data-toggle="table" data-pagination="true" data-search="true"  data-page-size="100" data-page-list="[100, 150, 200, 250, 300]" data-height="750"  data-width="750"  <?php if($_SESSION["User"]['type'] != "QualityControl"){ ?> data-detail-view="true" <?php } ?>>
					 -->	
					 <div class="col-md-2 mb15">
							<label for="filterStatus">สถานะ</label>
							<select class="filterStatus form-control fs12" data-tableId="table1" onchange="filterTable()">
							  <option value="0" <?php if($status == 0){ echo "selected";} ?>>รอตรวจ</option>
							  <option value="1" <?php if($status == 1){ echo "selected";} ?>>ไม่ผ่าน</option>
							  <option value="2" <?php if($status == 2){ echo "selected";} ?>>รอแจ้งงาน</option>
							  <option value="3" <?php if($status == 3){ echo "selected";} ?>>แจ้งงาน</option>    
							  <option value="4" <?php if($status == 4){ echo "selected";} ?>>เสร็จ</option>
							</select>
						</div>
						<div class="col-md-2 mb15">
							<label for="filter">PO</label>
							<input type="text" class=" form-control fs12" id="pocodeSe" value="<?php echo $_GET["pocode"];  ?>">
						</div>
						<div class="col-md-2 mb15">
							<a href="sendorders.php" class="btn btn-danger mt20">รีเช็ต</a>
							<span class="btn btn-success mt20" onclick="filterPOcode()">ค้นหา</span>
						</div>
<table class="js-dynamitable table table-bordered">


							<thead>
							<tr>
								<th class="t_c"><i class="fa fa-list-ul" aria-hidden="true"></i></th>
								<th data-field="statusType" class="t_c" data-sortable="true" class="t_c" >สถานะ</th>								
								<th data-field="noti_work_code" class="t_c dn"></th>
								<th data-field="pocode" data-sortable="true" class="t_c">PO</th>
								<th class="t_c">ชื่อ-นามสกุล</th>
								<th class="t_c">แพจเกต</th>
								<th data-field="start_cover_date" data-sortable="true" class="t_c">วันคุ้มครอง</th>
								<th class="t_c">ทุนประกัน</th>
								<th class="t_c">ชำระ</th>
								<th class="t_c">ผู้ขาย</th> 
								<th data-field="created_date" data-sortable="true" class="t_c">วันที่ทำรายการ</th>
								<th class="t_c">ผู้ตรวจ</th>
							</tr>
						</thead>
						<tbody class="fs12">
							<?php 
								//if($apiMain){
									foreach ($send_orders as $key => $value) {  
										if($value["start_cover_date"] == date("Y-m-d")){
											$bgColor = "class='bgbaf4bc'";
										}else if($value["status"] == "1"){
											$bgColor = "class='bgffeee8'";
										}else{
											$bgColor = "";
										}

										if($value["enable"] == "0"){
											$enable = "<span style='color: #f44;'>ยกเลิก</span>";
										}else{
											$enable = "<span style='color: #009710;'>เปิดใช้</span>";
										}
							?>
								<tr <?php echo $bgColor; ?> >
									<td class="wpno t_c"> 
												<a href="sendordersviews.php?pocode=<?php echo $value["po_code"];  ?>&numcode=<?php echo $value["noti_work_code"];  ?>" style="color: #003cff;">
													<i class="fa fa-cog"></i></a>
								
									</td>
									<td class="wpno t_c"><?php echo setStatus($value["status"]); ?></td>
									<td class="t_l dn"><?php echo $value["noti_work_code"]; ?></td>
									<td class=" t_l" style="width: 10%;"> 
										<b><?php echo $value["po_code"]; ?></b>
										<div class="clearfix fs10"><?php echo $value["noti_work_code"]; ?></div>
									</td>
									<td class="t_l <?php echo $bgColor; ?>"><b><?php echo $value["cus_name"]; ?></b></td>
									<td class="t_l">
										<div class="clearfix fs10 c2457ff"><?php echo $value["insuere_company"]; ?></div>
										<div class="clearfix c9c00c8"><?php echo $value["package_name"]; ?></div>
										<div class="clearfix fs10 "><?php echo $value["insurance_type"]; ?></div>
									</td>
									<td><?php echo date("d-m-Y",strtotime($value["start_cover_date"])); ?></td>
									<td class="t_r"><?php echo number_format($value["insuere_cost"],2); ?></td>
									<td class="t_r">
										<div class="clearfix "><b>สุทธิ: </b><?php echo number_format($value["netpremium"],2); ?></div>
										<div class="clearfix "><b>รวมภาษี: </b><?php echo number_format($value["premium"],2); ?></div>
										<div class="clearfix "><b>จ่าย: </b><?php echo number_format($value["taxamount"],2); ?></div>
									</td>
									<td class="t_l wpno">
											<div class="clearfix c2457ff "><?php echo $value["personnel_name"]; ?></div>
											<div class="clearfix fs10 "><?php echo $value["send_type"] ? "<b>การส่ง :</b> ".$value["send_type"] : ""; ?></div>
									</td>
									<td ><?php echo $value["created_date"]; ?></td>
									<td class="t_l">
										<div class="clearfix "><b>ชื่อ: </b><?php echo $value["chk_name"]." ".$value["chk_lname"]; ?></div>
										<div class="clearfix "><b>เวลา: </b><?php echo $value["datetime_chk"]; ?></div>
									</td>
									<span style="display: none;" id="desc<?php echo $key; ?>">
	                  <div class="row" id="dataBox<?php echo $key; ?>"></div>
	                </span>
								</tr>
							<?php }
							//} ?>
						</tbody>
					</table>
			</div>
		</div>
	</div>
</div>


<?php include "include/inc_footer.php"; ?> 
<!-- <script src="//code.jquery.com/jquery-3.2.1.min.js"></script> -->
<script src="fancybox/dist/jquery.fancybox.min.js"></script>
<script type="text/javascript">
	var $table = $('#table');
	$table.on('expand-row.bs.table', function (e, index, row, $detail) {
    var noti_id = row.noti_work_code; 
  	var res = $("#desc" + index).html(); 
  	$.ajax({ 
			url: 'include/inc_action_chk.php',
			type:'POST',
			data: {action:"getImageOrNote", noti_id:noti_id},
			success:function(rs){
				// console.log(rs);
				$detail.html(rs);
			}
		});
	});
function queryParams() {
    return {
        type: 'owner',
        sort: 'updated',
        direction: 'desc',
        per_page: 1,
        page: 1
    };
}

function filterTable(val){
	var filterStatus = $(".filterStatus").val();
	if(filterStatus){
		 window.location.href = "teamorders.php?filter="+filterStatus;
	}
}


function filterPOcode(){
	pocodeSe = $("#pocodeSe").val();
	console.log(pocodeSe);
	if(pocodeSe){
		window.location.href = "teamorders.php?pocode="+pocodeSe;
	}else{
		alert("กรุณากรอก PO เพื่อค้นหา");
	}
}



</script>
<!-- <script src="./js/dynamitable.jquery.js"></script> -->
<script src="./js/dynamitable.jquery.min.js"></script>