<?php 
session_start();
ini_set('max_execution_time', 186);
ini_set('memory_limit', '2048M');
$UserCode = $_SESSION["User"]['UserCode'];
$userType = $_SESSION["User"]['type'];

if(!isset($UserCode)){
	echo "<script>alert('กรุณาเข้าสู่ระบบ!!!');</script>";
	echo '<META http-equiv="refresh" content="0;URL=login.php">';
	exit();
}

header('Content-Type: text/html; charset=utf-8');
include "include/inc_header.php"; 
include "include/inc_menu.php"; 
include "reports/inc_function_report.php";
include "reports/targetdata.php";
// include('qrcode/qrcode.class.php');

$year = date("Y");

$_GET["txtse"] = ($_GET["txtse"]) ? $_GET["txtse"] : date("Y-m");
$getDepartment = getDepartment();

if($_GET["txtse"]){
	$getDataCount = getSaleSumforMounth($_GET["txtse"], $_GET["depar"]);
}

// echo "<pre>**".print_r($targetDay,1)."</pre>";
$targetDate = date("Ymd");
if($_GET["txtse"] == date("Y-m")){
	$target = $targetDay[$targetDate];
	$targetReAssign = $targetReAssign[$targetDate];
}else{
	$target = 650000;
	$targetReAssign = 650000;
}
?>
<div class="main">
	<div class="">
		<div class="p20">
				<div class="row">
					<div class="col-md-12">
						<div class="panel">
								<div class="panel-heading">
									<h4 class="panel-title"><b>รายงานยอดขายเดือน <?php echo date("Y F", strtotime($_GET["txtse"]."-01"));?></b></h4>
								</div>
								<div class="panel-body fs14">
									<div class="row">
										<div class="col-md-4 ">
											<span class="fs18 cff2da5 mr15">เดือน</span>
											<input type="hidden" value="<?php echo $_GET["txtse"]; ?>" id="txtse">
											<select name="mounth" id="mounthOpt" class="form-control formInput2 dib" style="width: 80%" >
												<option value="">:: กรุณาเลือก ::</option>
													<?php 
														$dateLast = ( date("Y") - 1)."-".date("m");
														for($i=1; $i<=12; $i++) { 
															$txtM = "+".$i." month";
															$dateForValue = date("Y-m",strtotime($txtM, strtotime($dateLast)));
													?>
														<option value="<?php echo $dateForValue ;?>" <?php if($_GET["txtse"] ==  $dateForValue){ echo "selected"; } ?>>
															<?php echo date("Y m F",strtotime($dateForValue)) ?>
														</option>
													<?php } ?>
											</select>
										</div>
										<div class="col-md-4 ">
											<span class="fs18 cff2da5 mr15">ทีม</span>
											<select name="Department" id="Department" class="form-control formInput2 dib" style="width: 80%" >
												<option value="">:: กรุณาเลือก ::</option>
												<?php foreach ($getDepartment as $key => $value) {?>
													<option value="<?php echo $value["Department_ID"];?>" <?php if($_GET["depar"] ==  $value["Department_ID"]){ echo "selected"; } ?>>
														<?php echo $value["Department_Name"]." | ".$value["Remark"];?>
													</option>
												<?php } ?>
											</select>
										</div>
									</div>
									<div class="row mt20">
										<div class="col-md-12">
											<div class="row">
												<div class="col-md-2"></div><div class="col-md-10"><?php echo $value["PO_ID"]; ?></div>
											</div>
											<?php if($getDataCount){?>
												<table class="table table-hover table-bordered" id="indextable" >
													<thead>
														<tr class="bgbaf4bc">
															<th class="t_c">รหัสพนักงาน</th>
															<th class="t_c">ทีม</th>
															<th class="t_c">ชื่อ-นามสกุล</th>
															<th class="t_c"><a href="javascript:SortTable(3,'N');">CP(N)</a></th>
															<th class="t_c">หัก 10%</th>
															<th class="t_c"><a href="javascript:SortTable(5,'N');">PO Piad</a></th>
															<th class="t_c"><a href="javascript:SortTable(6,'N');">%CVT</a></th>
															<th class="t_c">PO RCA</th>
															<th class="t_c">%CVT+RCA</th>
															<th class="t_c">Target</th>
															<th class="t_c">Accumulate</th>
															<th class="t_c">% Of Target</th>
															<!-- <th class="t_c">CP REJ(คัน)</th>
															<th class="t_c">CP REJ(บาท)</th> -->
															<th class="t_c"><a href="javascript:SortTable(12,'N');">งานใหม่</a></th>
															<th class="t_c"><a href="javascript:SortTable(13,'N');">งานต่ออายุ</a></th>
															<th class="t_c"><a href="javascript:SortTable(14,'N');">3M</a></th>
															<th class="t_c"><a href="javascript:SortTable(15,'N');">Paid</a></th>
															<th class="t_c"><a href="javascript:SortTable(16,'N');">รวม</a></th>
															<th class="t_c"><a href="javascript:SortTable(17,'N');">ดำเนินการ</a></th>
															<!-- <th class="t_c">งานยกเลิก</th>
															<th class="t_c">ลดหนี้</th>
															<th class="t_c">เพิ่มหนี้</th> -->
														</tr>
													</thead>
													<tbody >
														<?php 
															$sumAll = 0;
															foreach ($getDataCount as $key => $value) {
															$sumAll = $value["sumNew"] + $value["sumRenew"] + $value["sumPO3M"] + $value["sumPV"]; 
															$perLass10 = round(( $value["countCPNew"] * 90) / 100);
															$perCVT = $value["countCPNew"] ? ($value["countPOPaid"] * 100 ) / $perLass10 : 0;
															$percVTandRCA = $value["countCPNew"] ? (($value["countPOPaid"] + $value["countPORCA"]) * 100) / $perLass10 : 0; 
															$priceCPNewREJ = $value["countCPNewREJ"] * 160;


														if($value["Employee_ID"] != "ADB61030" && $value["Employee_ID"] != "ADB60052" && $value["Department_ID"] != "109"){
															$Accumulate = $sumAll - $target;
															$perOfTarget = round(($value["sumNew"]/$target) * 100 ); 
															$sumTarget = $value["sumNew"] + $target + $Accumulate ;
															$targetResult = $target;
														}else{
															$Accumulate = $sumAll - $targetReAssign;
															$perOfTarget = round(($value["sumNew"]/$targetReAssign) * 100 ); 
															$sumTarget = $value["sumNew"] + $targetReAssign + $Accumulate ;
															$targetResult = $targetReAssign;
														}
															$totalsumNew +=  $value["sumNew"];
															$totalsumRenew +=  $value["sumRenew"];
															$totalsumRej +=  $value["sumRej"];
															$totalsumC +=  $value["sumC"];
															$totalsumD +=  $value["sumD"];
															$totalsumPaying +=  $value["sumPaying"];
															$totalsumAll +=  $sumAll;
															$totalsumPO3M +=  $value["sumPO3M"];
															$totalsumPV +=  $value["sumPV"];

															$totalCountCPNew += $value["countCPNew"];
															$totalperLass10 += $perLass10;
															$totalcountPOPaid += $value["countPOPaid"];
															$totalperCVT += $perCVT;
															$totalcountPORCA += $value["countPORCA"];
															$totalpercVTandRCA += $percVTandRCA;
															$totalcountCPNewREJ += $value["countCPNewREJ"];
															$totalpriceCPNewREJ += $priceCPNewREJ;
														if($sumAll && $value["Employee_ID"] != "ADB61030" && $value["Employee_ID"] != "ADB60052"){
															$totalsumTarget += $targetResult;
														}

															if($sumAll){
														?>
															<tr>
																<td class="t_c"><?php echo $value["Employee_ID"]; ?></td>
																<td class="t_c"><?php echo $value["Department_Name"]; ?></td>
																<td class="t_l"><?php echo $value["User_FName"]." ".$value["User_LName"]; ?></td>
																<td class="t_r fwb cff8000"><?php echo $value["countCPNew"]; ?></td>
																<td class="t_r fwb c2457ff"><?php echo number_format($perLass10); ?></td>
																<td class="t_r fwb cff8000"><?php echo $value["countPOPaid"]; ?></td>
																<td class="t_r fwb c2457ff"><?php echo number_format($perCVT,2); ?></td>
																<td class="t_r fwb cff8000"><?php echo $value["countPORCA"]; ?></td>
																<td class="t_r fwb c2457ff"><?php echo number_format($percVTandRCA,2); ?></td>
																<!-- <td class="t_r fwb cff8000"><?php echo $value["countCPNewREJ"]; ?></td>
																<td class="t_r c9c00c8"><?php echo number_format($priceCPNewREJ,2); ?></td> -->
															<?php if($value["Employee_ID"] != "ADB61030" && $value["Employee_ID"] != "ADB60052"){ ?>
																<td class="t_r"><?php echo number_format($targetResult); ?></td>
																<td class="t_r"><?php echo number_format($Accumulate,2); ?></td>
																<td class="t_r"><?php echo $perOfTarget; ?></td>
															<?php }else{ ?>
																<td class="t_r">-</td>
																<td class="t_r">-</td>
																<td class="t_r">-</td>
															<?php } ?>
																
																<td class="t_r fwb c9c00c8"><?php echo number_format($value["sumNew"],2); ?></td>
																<td class="t_r fwb c00ac0a"><?php echo number_format($value["sumRenew"],2); ?></td>
																<td class="t_r fwb c2457ff"><?php echo number_format($value["sumPO3M"],2); ?></td>
																<td class="t_r fwb cff8000"><?php echo number_format($value["sumPV"],2); ?></td>
																<td class="t_r fwb cff2da5"><?php echo number_format($sumAll,2); ?></td>
																<td class="t_r fwb c2457ff"><?php echo number_format($value["sumPaying"],2); ?></td>
																<!-- <td class="t_r fwb "><?php echo number_format($value["sumRej"],2); ?></td>
																<td class="t_r fwb "><?php echo number_format($value["sumC"],2); ?></td>
																<td class="t_r fwb "><?php echo number_format($value["sumD"],2); ?></td> -->
															</tr>
															<?php }
															} ?>
													</tbody>
													<tfoot>
														<tr class="fwb fs16 bgfffbd8">
															<td class="t_c" colspan="3">รวม</td>
															<td class="t_r cff8000"><?php echo $totalCountCPNew; ?></td>
															<td class="t_r c2457ff"><?php echo $totalperLass10; ?></td>
															<td class="t_r cff8000"><?php echo $totalcountPOPaid; ?></td>
															<td class="t_r c2457ff"><?php echo number_format( ($totalcountPOPaid*100) / $totalperLass10,2); ?></td>
															<td class="t_r cff8000"><?php echo $totalcountPORCA; ?></td>
															<td class="t_r c2457ff">
																<?php echo number_format((($totalcountPOPaid + $totalcountPORCA)*100) / $totalperLass10,2); ?>	
															</td>
															<td class="t_c" ><?php echo number_format( $totalsumTarget,2); ?></td>
															<td class="t_c" ><?php echo number_format( $totalsumNew - $totalsumTarget,2); ?></td>
															<td class="t_r" >
																<?php echo number_format( (($totalsumNew/$totalsumTarget))*100,2); ?></td>
															<!-- <td class="t_r fwb c9c00c8"><?php echo $totalcountCPNewREJ; ?></td>
															<td class="t_r fwb c9c00c8"><?php echo number_format($totalpriceCPNewREJ,2); ?></td> -->
															<td class="t_r fwb c9c00c8"><?php echo number_format($totalsumNew,2); ?></td>
															<td class="t_r fwb c00ac0a"><?php echo number_format($totalsumRenew,2); ?></td>
															<td class="t_r fwb c2457ff"><?php echo number_format($totalsumPO3M,2); ?></td>
															<td class="t_r fwb cff8000"><?php echo number_format($totalsumPV,2); ?></td>
															<td class="t_r fwb cff2da5"><?php echo number_format($totalsumAll,2); ?></td>
															<td class="t_r fwb c2457ff"><?php echo number_format($totalsumPaying,2); ?></td>
															<!-- <td class="t_r fwb "><?php echo number_format($totalsumRej,2); ?></td>
															<td class="t_r fwb "><?php echo number_format($totalsumC,2); ?></td>
															<td class="t_r fwb "><?php echo number_format($totalsumD,2); ?></td> -->
														</tr>
													</tfoot>
												</table>
											<?php } ?>
										</div>
									</div>
								</div>
						</div>
					</div>
				</div>
		</div>
	</div>
</div>
<?php include "include/inc_footer.php"; ?> 
<?php include "include/inc_function_tablesort.php"; ?>
<script type="text/javascript">

	$("#mounthOpt").on('change', function() {
		if(this.value){
			window.location.href = "report_salesum.php?txtse="+this.value;
		}

  });

  $("#Department").on('change', function() {
  	txtse = $("#txtse").val();
		if(this.value){
			window.location.href = "report_salesum.php?txtse="+txtse+"&depar="+this.value;
		}else{
			window.location.href = "report_salesum.php?txtse="+txtse;
		}

  });

<?php if($_GET["txtse"]){?>
function getDetailPO(user, date){
	txtse = $("#txtse").val();
	if(txtse){
		window.location.href = "report_salesum.php?txtse="+txtse+"&user="+user+"&date="+date;
	}
}
<?php } ?>
  

function filterPOcode(){
	txtse = $("#txtse").val();
	if(txtse){
		window.location.href = "counters.php?txtse="+txtse;
	}else{
		alert("กรุณากรอก ข้อมูล เพื่อค้นหา");
	}
}

function random_rgba() {
    var o = Math.round, r = Math.random, s = 255;
    return 'rgba(' + o(r()*s) + ',' + o(r()*s) + ',' + o(r()*s) + ',' + r().toFixed(2) + ')';
}


</script>



