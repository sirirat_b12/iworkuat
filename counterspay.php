<?php 

session_start();
$UserCode = $_SESSION["User"]['UserCode'];
$userType = $_SESSION["User"]['type'];

if(!isset($UserCode)){
	echo "<script>alert('กรุณาเข้าสู่ระบบ!!!');</script>";
	echo '<META http-equiv="refresh" content="0;URL=login.php">';
	exit();
}

header('Content-Type: text/html; charset=utf-8');
include "include/inc_header.php"; 
include "include/inc_menu.php"; 
include "counters/inc_function_counters.php";
// include('qrcode/qrcode.class.php');

$date = ($_GET["date"]) ? $_GET["date"] : date("Y-m-d");
$dateEnd = ($_GET["dateend"]) ? $_GET["dateend"] : date("Y-m-d");
$getListCsPay = getListCsPay(trim($_GET["txtse"]), $date, $dateEnd);

// echo "<pre>".print_r($getListCsPay,1)."</pre>";
?>
<div class="main">
	<div class="">
		<div class="p20">
			<div class="row">
				<div class="col-md-12">
					<div class="panel">
							<div class="panel-heading">
								<div class="row mt15">
									<div class="col-md-2 cff2da5">
										<label for="filter" >วันที่</label>
										<input type="date" class=" form-control fs12" id="date" value="<?php echo $date; ?>">
									</div>
									<div class="col-md-2 cff2da5">
										<label for="filter" >ถึงวันที่</label>
										<input type="date" class=" form-control fs12" id="dateEnd" name="dateEnd" value="<?php echo $dateEnd; ?>">
									</div>
									<div class="col-md-2 cff2da5">
										<label for="filter" >PO Code | Pay Code | เบอร์</label>
										<input type="text" class="cff2da5 form-control fs12" id="txtse" value="<?php echo trim($_GET["txtse"]); ?>">
									</div>
									<div class="col-md-2 ">
										<a href="counterspay.php" class="btn btn-danger mt20">รีเช็ต</a>
										<span class="btn btn-success mt20" onclick="filterPOcode()">ค้นหา</span>
									</div>
									<div class="col-md-1">
										<sapn class="btn btn-info mt20" onclick="btnExport()">Export</sapn>
									</div>
								</div>
							</div>
							<div class="panel-body fs14">
									<input type="hidden" value="<?php echo count($getListCsPay); ?>" name="countData">
									<table id="tablecp" class="table p5" data-toggle="table" data-pagination="true" data-page-size="200" data-page-list="[200, 300, 400]">
										<thead class="fs13 c000000">
											<tr>
												<th class="t_c">Pay Code</th>
												<th class="t_c">สถานะ CS</th> 
												<th class="t_c">PO Code</th>
												<th class="t_c">RV</th>
												<th class="t_c">วันที่ชำระ</th>
												<!-- <th class="t_c">TX ID</th>
												<th class="t_c">Log</th> -->
												<th class="t_c">บริษัทประกัน</th>
												<th class="t_c">งวดที่</th>
												<th class="t_c">Ref.1</th>
												<th class="t_c">Ref.2</th>
												<th class="t_c">รหัสลูกค้า</th>
												<th class="t_c">ชื่อ-นามสกุล</th>
												<th class="t_c">เบอร์</th>
												<th class="t_c">ประเภท</th>
												<th class="t_c">วันครบชำระ</th>
												<th class="t_c">ผู้สร้าง</th>
												<th class="t_c">จำนวน</th>
											</tr>
										</thead>
										<tbody class="fs12 c000000">
											<tr >
												<td colspan="15" class="t_r fwb">รวม(ชำระ)</td>
												<td class="t_r fwb" id="sumAmount"></td>
											</tr>
											<?php 
												if($getListCsPay){
													$sum = 0.00;
													foreach ($getListCsPay as $key => $value) {
													$bg = '';
													$status_pay = ($value["status_pay"] == 0 ) ? "ตั้งหนี้" : "ชำระ"; 
													$dudDate = ($value["installment_due_date"]) ? $value["installment_due_date"]->format("d/m/Y") : "-";
													if($value["status_pay"] == 1){
														$bg = 'class="bgbaf4bc"';
													}

													if($value["status_pay"] == 1){
														$sum = $sum + $value["amount"];
													}
											 ?>
											<tr <?php echo $bg; ?> >
												<td class="t_c c2457ff fwb"><?php echo $value["paycode"]; ?></td>
												<td class="t_c fwb cff2da5">
													<?php echo ($value["status_cs"]=='100') ? "ชำระ" : "ยกเลิก"; ?>
												</td>
												<td class="t_c fwb c0aaaef"><?php echo $value["po_id"]; ?></td>
												<td class="t_c fwb cff8000"><?php echo $value["receive_id"]; ?></td>
												<td class="t_c cff2da5 fwb"><?php echo $value["payment_date"]->format("d/m/Y H:i:s"); ?></td>
												<!-- <td class="t_c c2457ff"><?php echo $value["transaction_id"]; ?></td>
												<td class="t_c c2457ff"><?php echo $value["log_id"]; ?></td> -->
												<td class="t_c "><?php echo $value["Insurer_Initials"]; ?></td>
												<td class="t_c c2457ff"><?php echo $value["installment_num"]; ?></td>
												<td class="t_c "><?php echo $value["reference_1"]; ?></td>
												<td class="t_c "><?php echo $value["reference_2"]; ?></td>
												<td class="t_c "><?php echo $value["customer_id"]; ?></td>
												<td class="t_l cff2da5 fwb"><?php echo $value["customer_name"]; ?></td>
												<td class="t_c "><?php echo $value["tel_no"]; ?></td>
												<td class="t_c "><?php echo ($value["insur_type"] == "insur") ? "ประกันภัย" : "พรบ."; ?></td>
												<td class="t_c "><?php echo $dudDate; ?></td>
												<td class="t_c "><?php echo $value["created_by"]; ?></td>
												<td class="t_r fwb <?php if($value["status_pay"] == 1){ echo "amount"; } ?>" data-amount="<?php echo $value["amount"]; ?>"><?php echo number_format($value["amount"],2); ?></td>
											</tr>

											<?php }
											} ?>
											<tr >
												<td colspan="15" class="t_r fwb">รวม(ชำระ)</td>
												<td class="t_r fwb"><?php echo number_format($sum,2); ?></td>
											</tr>
										</tbody>
								</table>
							</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php include "include/inc_footer.php"; ?> 
<script src="fancybox/dist/jquery.fancybox.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	var TotalValue = 0;
	var i = 0;
    $(".amount").each(function(){
       TotalValue += parseFloat($(this).data("amount"));
    });
    $('#sumAmount').html(numCommas(TotalValue.toFixed(2)));
});


function filterPOcode(){
	txtse = $("#txtse").val();
	date = $("#date").val();
	dateEnd = $("#dateEnd").val();
	window.location.href = "counterspay.php?txtse="+txtse+"&date="+date+"&dateend="+dateEnd;
	// if(txtse){
	// 	window.location.href = "counterspay.php?txtse="+txtse;
	// }else{
	// 	alert("กรุณากรอก ข้อมูล เพื่อค้นหา");
	// }
}
function btnExport(){
		txtse = $("#txtse").val();
		date = $("#date").val();
		dateEnd = $("#dateEnd").val();
		if(date){
			// window.open("callcenters/export_inform.php?date="+date+"&insurers="+insurers);
			window.location.href = "counters/export_counters.php?txtse="+txtse+"&date="+date+"&dateend="+dateEnd;
		}
	}
</script>