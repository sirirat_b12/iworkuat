<?php 
session_start();
$UserCode = $_SESSION["User"]['UserCode'];
$userType = $_SESSION["User"]['type'];
if(!isset($UserCode)){
	echo "<script>alert('กรุณาเข้าสู่ระบบ!!!');</script>";
	echo '<META http-equiv="refresh" content="0;URL=login.php">';
	exit();
}
header('Content-Type: text/html; charset=utf-8');
include "include/inc_header.php"; 
include "include/inc_menu.php"; 
include "inc_2c2p/inc_function_2c2p.php";
?>
<div class="main">
	<div class="">
		<div class="p20">
				<div class="row">
					<div class="col-md-12">
						<div class="panel">
								<div class="panel-heading">
									<h4 class="panel-title"><b>ระบบชำระเงิน 2c2p</b></h4>
								</div>
								<div class="panel-body fs14">
									<div class="row">
										<div class="col-md-5 t_r"><h4>รหัสลูกค้า :</h4></div>
										<div class="col-md-2">
											<input class="form-control formInput2" type="text" name="txtSearch" id="txtSearch"  value="<?php echo $_GET["txtsearch"] ?>" required>
										</div>
										<div class="col-md-2">
											<button type="button" id="btn_send_order" class="btn  btn-info"><i class="fa fa-search-plus" aria-hidden="true"></i> เช็คข้อมูล</button>
										</div>
										<div class="col-md-3 ">
											<div class="fs25 cf40053 fwb">สำหรับสร้างการชำระ</div>
										</div> 
									</div>
									<div class="row mt20">
										<div class="col-md-12">
											<div class="row">
												<div class="col-md-2"></div><div class="col-md-10"><?php echo $value["PO_ID"]; ?></div>
											</div>
											<form action="inc_2c2p/inc_action_2c2p.php" method="POST" id="frmListPay">
												<input type="hidden" name="action" value="inserListpay">
												<input type="hidden" name="txtsearch_hidden" id="txtsearch_hidden" value="">
												<table class="table table-hover" id="tableMoveQC">
													<thead>
														<tr>
															<th class="t_c">
																<div><input type="checkbox" id="chkInsAll" name="chkInsAll" ></div>
															</th>
															<th class="t_c">งวดที่</th>
															<th class="t_c">PO</th>
															<th class="t_c">ชื่อ-นามสกุล</th>
															<th class="t_c">ประเภทประกัน</th>
															<th class="t_c">กำหนดชำระ</th>
															<th class="t_r">เบี้ยสุทธิ</th>
															<th class="t_r">อากร</th>
															<th class="t_r">ภาษี</th>
															<th class="t_r">เบี้ยรวม</th>
															<th class="t_r">ส่วนลด</th>
															<th class="t_r">จ่ายสุทธิ</th>
															<th class="t_c">สถานะของงวดชำระ</th>
														</tr>
													</thead>
													<tbody id="table_data" class="table-list fs13"><tr><td colspan="12" class="text-center">กรุณาเลือกข้อมูล</td></tr></tbody>
												</table>
												<div class="p10 cff2da5" style="background-color: #ebebeb;">
													<div class="row">
														<div class="col-md-4">
															<p class="fwb">หมายเหตุ</p>
															<input type="text" name="remark" id="txtremark"  class="form-control cff2da5" value="">
														</div>														
														<div class="col-md-4">
															<p class="fwb">Phone</p>
															<input type="text" name="phone" id="txtphone" class="form-control cff2da5" >
														</div>
														<div class="col-md-4">
															<p class="fwb">Email</p>
															<input type="text" name="email" id="txtemail" class="form-control cff2da5" >
														</div> 
													</div>
													<div class="row">
														<div id="Waittxt" class="fs20 t_c  fwb mt25 dn">กรุณารอ..</div>
														<div class="col-md-12 mt20 t_c">
															<input type="button" class="btn btn-success" id="btnsub" value="ตกลง" onclick="btnsubmit()">
															<a href="counters.php" class="btn btn-danger" id="btncancel">ยกเลิก</a>
														</div>
													</div>
												</div>
											</form>
										</div>
									</div>
								</div>
						</div>
					</div>
				</div>
		</div>
	</div> 


 












</div>
<?php include "include/inc_footer.php"; ?> 
<script src="fancybox/dist/jquery.fancybox.min.js"></script>
<script type="text/javascript">
$('[data-fancybox]').fancybox({
	toolbar  : false,
	smallBtn : true,
	iframe : {
		preload : false
	}
})


 
function filterPOcode(){
	txtse = $("#txtse").val();
	if(txtse){
		window.location.href = "pay_2c2p.php?txtse="+txtse;
	}else{
		alert("กรุณากรอก ข้อมูล เพื่อค้นหา");
	}
}

 
// -- tor 28-09-2021
$("#btn_send_order").click(function() {
		$("#table_data").empty();
  	var txtsearch = $("#txtSearch").val();
  	if(txtsearch){
    	$.ajax({ 
					url: 'inc_2c2p/inc_action_2c2p.php',
					type:'POST',
					data: {action: 'GetListCS', txtsearch:txtsearch},
					success:function(rs){ 
						// console.log(rs);
						$('#txtsearch_hidden').val(txtsearch);
						$('#table_data').append(rs);					
					}
			});
			$.ajax({ 
					url: 'inc_2c2p/inc_action_2c2p.php',
					type:'POST',
					dataType: 'json',
					data: {action: 'GetDataPhone', txtsearch:txtsearch},
					success:function(rs){ 
						// console.log(rs.phone);
						$("#txtphone").val(rs.phone);
						$("#txtemail").val(rs.email);
						$('#txtremark').val(rs.remark);
					}
			});
		}else{
			alert("กรุณากรอกรหัสลูกค้า");
		}
});

//check all 
$("#chkInsAll").click(function() {
  if($('#chkInsAll').is(':checked')){
    $(".inskey").prop('checked', true);
    
  }else{
    $(".inskey").prop('checked', false);    
  }
});




// function -- 
function btnsubmit(){
	var countCK = $('input[name="inskey[]"]:checked').length;
	if(countCK > 0){ 
		if( confirm("ระบบกำลังจะส่งข้อความไปที่โทรศัพท์ของลูกค้าเพื่อชำระเงิน")){
			$("#btnsub, #btncancel").hide();
			$("#Waittxt").show();
			$("#frmListPay").submit();
		}
	}else{
		alert("กรุณาเลือกงวดที่ชำระ");
		$("#btnsub, #btncancel").show();
		$("#Waittxt").hide();
	}
}





</script>