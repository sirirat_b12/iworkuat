<?php 

function diff2time($time_a,$time_b){
    $now_time1=strtotime($time_a);
    $now_time2=strtotime($time_b);
    $time_diff=abs($now_time2-$now_time1);
    $time_diff_h=floor($time_diff/3600); // จำนวนชั่วโมงที่ต่างกัน
    $time_diff_m=floor(($time_diff%3600)/60); // จำวนวนนาทีที่ต่างกัน
    $time_diff_s=($time_diff%3600)%60; // จำนวนวินาทีที่ต่างกัน
    return $time_diff_h." ช. ".$time_diff_m." น. ".$time_diff_s." ว.";
}

function setStatusPO($name){
  switch ($name) {
    case "0": return "<b style='color: #2457ff;'>รอตรวจ</b>"; break;
    case "1": return "<b style='color: #f44;'>ไม่ผ่าน</b>"; break;
    case "2": return "<b style='color: #ff8000;'>รอแจ้งงาน</b>"; break;
    case "3": return "<b style='color: #c600c4;'>แจ้งงาน</b>"; break;
    case "4": return "<b style='color: #00ac0a;'>เสร็จ</b>"; break;
  }
}


function getMonthSelect(){
  global $conn2;
  $sql = $conn2->GetAll("SELECT DISTINCT MONTH(noti_work.created_date) AS months FROM noti_work GROUP BY noti_work.created_date");
  return $sql;   
  exit();
}


function getMountListdate($months){
	global $conn2; 
	$sql = $conn2->GetAll( "SELECT date(noti_work.created_date) AS dates FROM  noti_work 
	where MONTH(noti_work.created_date) = '".$months."' AND Year(noti_work.created_date) = '".date("Y")."'
	GROUP BY date(noti_work.created_date) 
	ORDER BY date(noti_work.created_date) ASC");
	return $sql;  
	exit();
}

function getListQC(){
	global $conn2;
	$sql = $conn2->GetAll( "SELECT * FROM  ck_personnel where personnel_type = 'QualityControl' ");
	return $sql; 
	exit(); 
}


function getListAdmin($months, $year){
	global $conn2;
	$sql = $conn2->GetAll( "SELECT noti_work.admin_code FROM noti_work WHERE admin_code !='' AND	MONTH (noti_work.created_date) = '".$months."' AND YEAR (noti_work.created_date) = '".$year."' GROUP BY admin_code ");
	return $sql; 
	exit(); 
}

function getDataCount($months){
	global $conn2; 
	// $getListQC = getListQC();
	$sql = $conn2->GetAll( "SELECT date(noti_work.created_date) AS dates, noti_work.chk_code, count(noti_work_id) AS counts , SUM(TIMESTAMPDIFF(SECOND,datetime_chk_open, datetime_chk)) AS sumTime
	FROM  noti_work 
	where MONTH(noti_work.created_date) = '".$months."' AND Year(noti_work.created_date) = '".date("Y")."'  
	GROUP BY date(noti_work.created_date), noti_work.chk_code
	ORDER BY date(noti_work.created_date) ASC");
	$data = array();
	foreach ($sql as $key => $value) {
			$data[$value["dates"]][$value["chk_code"]] = $value["counts"];
			$data[$value["dates"]]["sumTime"][$value["chk_code"]] = $value["sumTime"];
	}
	// echo "<pre>".print_r($data,1)."</pre>";
	return $data; 
	exit(); 
}

function getDataToAdmin($months, $year){
	global $conn2; 
	// $getListQC = getListQC();
	$sql = $conn2->GetAll( "SELECT date(noti_work.created_date) AS dates, noti_work.admin_code, count(noti_work_id) AS counts FROM  noti_work 
	where admin_code !='' AND MONTH(noti_work.created_date) = '".$months."' AND Year(noti_work.created_date) = '".$year."'  
	GROUP BY date(noti_work.created_date), noti_work.admin_code
	ORDER BY date(noti_work.created_date) ASC");
	$data = array();
	foreach ($sql as $key => $value) {
			$data[$value["dates"]][$value["admin_code"]] = $value["counts"];
	}
	// echo "<pre>".print_r($data,1)."</pre>";
	return $data; 
	exit(); 
}

function getDetailPOByQC($user, $date ){
	global $conn2; 
	$data = array();
	$i = 0;
	$sql = $conn2->GetAssoc( "SELECT noti_work_code, po_code, insurance_type, package_name, personnel_name, insuere_company, status, created_date, cus_name, chk_code,datetime_chk_open, datetime_chk, enable FROM  noti_work where chk_code = '".$user."' AND date(created_date) = '".$date."' ");
	foreach ($sql as $key => $value) {
		$steplists = $conn2->GetAll( "SELECT noti_comment_id, comment, datetime, comment_byname FROM  noti_comment where noti_work_code = '".$value["noti_work_code"]."' ORDER BY datetime");
		array_push($data, $value);
		$data[$i]["steplists"] = $steplists;
		$i++;
	}
	return $data; 
	exit(); 
}


function getstaticSale($months){
	global $conn2; 
	
	$sql = $conn2->GetAssoc("SELECT nw.personnel_code, nw.personnel_name,
	(SELECT COUNT(n1.noti_work_id) FROM noti_work n1 WHERE n1.personnel_code = nw.personnel_code AND MONTH(n1.created_date) = '".$months."' AND YEAR(n1.created_date) = '".date("Y")."') AS numall,
	(SELECT COUNT(n2.noti_work_id) FROM noti_work n2 WHERE n2.personnel_code = nw.personnel_code AND n2.enable = '1' AND MONTH(n2.created_date) = '".$months."' AND YEAR(n2.created_date) = '".date("Y")."') AS enable_1,
	(SELECT COUNT(n3.noti_work_id) FROM noti_work n3 WHERE n3.personnel_code = nw.personnel_code AND n3.enable = '0' AND MONTH(n3.created_date) = '".$months."' AND YEAR(n3.created_date) = '".date("Y")."') AS enable_0,
	(SELECT COUNT(n4.noti_work_id) FROM noti_work n4 WHERE n4.personnel_code = nw.personnel_code AND n4.status = '4' AND MONTH(n4.created_date) = '".$months."' AND YEAR(n4.created_date) = '".date("Y")."') AS status_4,
	(SELECT SUM(n5.discount) FROM noti_work n5 WHERE n5.personnel_code = nw.personnel_code AND MONTH(n5.created_date) = '".$months."' AND YEAR(n5.created_date) = '".date("Y")."' AND n5.enable = '1') AS discount,
	(SELECT SUM(n6.taxamount) FROM noti_work n6 WHERE n6.personnel_code = nw.personnel_code AND MONTH(n6.created_date) = '".$months."' AND YEAR(n6.created_date) = '".date("Y")."' AND n6.enable = '1') AS taxamount
	FROM noti_work nw WHERE MONTH(nw.created_date) = '".$months."' AND YEAR(nw.created_date) = '".date("Y")."' GROUP BY nw.personnel_code
	");
	echo "SELECT nw.personnel_code, nw.personnel_name,
	(SELECT COUNT(n1.noti_work_id) FROM noti_work n1 WHERE n1.personnel_code = nw.personnel_code AND MONTH(n1.created_date) = '".$months."' AND YEAR(n1.created_date) = '".date("Y")."') AS numall,
	(SELECT COUNT(n2.noti_work_id) FROM noti_work n2 WHERE n2.personnel_code = nw.personnel_code AND n2.enable = '1' AND MONTH(n2.created_date) = '".$months."' AND YEAR(n2.created_date) = '".date("Y")."') AS enable_1,
	(SELECT COUNT(n3.noti_work_id) FROM noti_work n3 WHERE n3.personnel_code = nw.personnel_code AND n3.enable = '0' AND MONTH(n3.created_date) = '".$months."' AND YEAR(n3.created_date) = '".date("Y")."') AS enable_0,
	(SELECT COUNT(n4.noti_work_id) FROM noti_work n4 WHERE n4.personnel_code = nw.personnel_code AND n4.status = '4' AND MONTH(n4.created_date) = '".$months."' AND YEAR(n4.created_date) = '".date("Y")."') AS status_4,
	(SELECT SUM(n5.discount) FROM noti_work n5 WHERE n5.personnel_code = nw.personnel_code AND MONTH(n5.created_date) = '".$months."' AND YEAR(n5.created_date) = '".date("Y")."' AND n5.enable = '1') AS discount,
	(SELECT SUM(n6.taxamount) FROM noti_work n6 WHERE n6.personnel_code = nw.personnel_code AND MONTH(n6.created_date) = '".$months."' AND YEAR(n6.created_date) = '".date("Y")."' AND n6.enable = '1') AS taxamount
	FROM noti_work nw WHERE MONTH(nw.created_date) = '".$months."' AND YEAR(nw.created_date) = '".date("Y")."' GROUP BY nw.personnel_code
	";
	return $sql; 
	exit(); 
}


function getDetailPOBySale($user, $months ){
	global $conn2; 
	$data = array();
	$i = 0;
	$sql = $conn2->GetAssoc( "SELECT noti_work_code, po_code, insurance_type, package_name, personnel_name, insuere_company, status, created_date, cus_name, chk_code, datetime_chk, enable FROM  noti_work where personnel_code = '".$user."' AND MONTH(created_date) = '".$months."' AND YEAR(created_date) = '".date("Y")."' " );
	foreach ($sql as $key => $value) {
		$steplists = $conn2->GetAll( "SELECT noti_comment_id, comment, datetime, comment_byname FROM  noti_comment where noti_work_code = '".$value["noti_work_code"]."' ORDER BY datetime");
		array_push($data, $value);
		$data[$i]["steplists"] = $steplists;
		$i++;
	}
	return $data; 
	exit(); 
}


function getDetailPOByAdmin($user, $date ){
	global $conn2; 
	$data = array();
	$i = 0;
	$sql = $conn2->GetAssoc( "SELECT noti_work_code, po_code, insurance_type, package_name, personnel_name, insuere_company, status, created_date, cus_name, chk_code, admin_code, datetime_chk, datetime_chk_open, datetime_send_open, datetime_send, enable FROM  noti_work where admin_code = '".$user."' AND date(created_date) = '".$date."' ");
	foreach ($sql as $key => $value) {
		$steplists = $conn2->GetAll( "SELECT noti_comment_id, comment, datetime, comment_byname FROM  noti_comment where noti_work_code = '".$value["noti_work_code"]."' ORDER BY datetime");
		array_push($data, $value);
		$data[$i]["steplists"] = $steplists;
		$i++;
	}
	return $data; 
	exit(); 
}

function getSaleSumforMounth($date, $depar){
	global $connMS;
	$exDate = explode("-", $date);
	$year = $exDate[0];
	$mounth = $exDate[1];
  	$resultarray = array();
 	$datestart =  $year."-".$mounth."-01";
 	$dateNowLast = $year."-".$mounth."-".date('t',strtotime($year."-".$mounth))." 23:59:59";
 	$dateNowLast = $year."-".$mounth."-".date('t',strtotime($year."-".$mounth))." 23:59:59";
 	$datestartPO = date("Y-m-d",strtotime($datestart.'-3 month'));

  if($year == date("Y") && $mounth == date("n")){
    $dateNow = date("Y-m-d")." 23:59:59";
  }else{
    $dateNow = $year."-".$mounth."-".date('t',strtotime($year."-".$mounth))." 23:59:59";
  }
	$sql = "SELECT Purchase_Order.Employee_ID, My_User.User_ID, My_User.User_FName, My_User.User_LName, Department.Department_Name, Department.Department_ID
			,SUM(case when Purchase_Order.Operation_Type_ID = 'N' AND Purchase_Order.Status_ID IN ('RV','RVP','CLS','REJ') AND Purchase_Order.Insurance_ID NOT IN ('084','085') then Purchase_Order.Premium_After_Disc end) AS sumNew
			,SUM(case when Purchase_Order.Operation_Type_ID = 'R' AND Purchase_Order.Status_ID IN ('RV','RVP','CLS','REJ') AND Purchase_Order.Insurance_ID NOT IN ('084','085') then Purchase_Order.Premium_After_Disc end) AS sumRenew
			,SUM(case when Purchase_Order.Operation_Type_ID = 'N' AND Purchase_Order.Status_ID IN ('REJ')  then Purchase_Order.Premium_After_Disc end) AS sumRej
			,SUM(case when Purchase_Order.Operation_Type_ID = 'C' then Purchase_Order.Premium_After_Disc end) AS sumC
			,SUM(case when Purchase_Order.Operation_Type_ID = 'D' then Purchase_Order.Premium_After_Disc end) AS sumD
			,SUM(case when Purchase_Order.Operation_Type_ID IN ('N','R') AND Purchase_Order.Status_ID IN ('RV','RVP','CLS','REJ') AND Purchase_Order.Insurance_ID IN ('084','085')  then Purchase_Order.Premium_After_Disc*4 end) AS sumPO3M
			
			,(select SUM(Purchase_Order.Premium_After_Disc) FROM [dbo].[Purchase_Order]
		    WHERE Purchase_Order.Agent_ID = '00001' AND Purchase_Order.Active = 'Y' AND  Purchase_Order.Operation_Type_ID IN ('R','N') AND Purchase_Order.Status_ID IN ('OPN','RCA','RTA','NEW') AND Purchase_Order.Insurance_ID NOT IN ('084','085') AND Purchase_Order.PO_Date >= '".$datestartPO."' AND Purchase_Order.Employee_ID = My_User.User_ID ) AS sumPaying
		    ,(select SUM(Purchase_Order.Premium_After_Disc) FROM [dbo].[Purchase_Order]
		    WHERE Purchase_Order.Agent_ID = '00001' AND Purchase_Order.Active = 'Y' AND  Purchase_Order.Operation_Type_ID IN ('R','N') AND Purchase_Order.Status_ID = 'PV' AND Purchase_Order.Coverage_Start_Date >= '".$datestart."' AND Purchase_Order.Coverage_Start_Date <= '".$dateNowLast."' 
			AND Purchase_Order.Employee_ID = My_User.User_ID ) AS sumPV

			,(SELECT COUNT(Compare_Policy.CP_ID) FROM Compare_Policy LEFT JOIN Customer_Registration ON Compare_Policy.Customer_ID = Customer_Registration.Customer_ID 
			WHERE Compare_Policy.CP_PO_ID = '' AND Compare_Policy.Employee_ID = My_User.User_ID
			AND Compare_Policy.CP_Date >= '".$datestart."' AND Compare_Policy.CP_Date <= '".$dateNow."' 
			AND ( CP_ID NOT LIKE '%B%' AND CP_ID NOT LIKE '%R%' ) AND Customer_Registration.Referral_Type_ID NOT IN ( '002', '2ND' )  ) AS countCPNew
			,(SELECT COUNT(Compare_Policy.CP_ID) FROM Compare_Policy WHERE Compare_Policy.CP_PO_ID = '' AND Compare_Policy.Compare_Status_ID = 'REJ' AND Compare_Policy.Employee_ID = My_User.User_ID
			AND Compare_Policy.CP_Date >= '".$datestart."' AND Compare_Policy.CP_Date <= '".$dateNow."' ) AS countCPNewREJ
			,(SELECT COUNT(Purchase_Order.PO_ID)  FROM Purchase_Order WHERE Purchase_Order.PO_Date >= '".$datestartPO."' 
			 AND Purchase_Order.Operation_Type_ID = 'N' 
			 AND Purchase_Order.Status_ID IN ('RCA','NEW','OPN','RTA') AND Purchase_Order.Employee_ID = My_User.User_ID) AS countPORCA
			 ,COUNT(case when Purchase_Order.Operation_Type_ID = 'N' AND Purchase_Order.Status_ID IN ('RV','RVP','CLS')  then Purchase_Order.PO_ID end) AS countPOPaid

			FROM [dbo].[Purchase_Order] 
			LEFT JOIN [dbo].[Installment] ON Purchase_Order.PO_ID = Installment.PO_ID
			LEFT JOIN [dbo].Receive_Installment ON Installment.PO_ID = Receive_Installment.PO_ID and Receive_Installment.Installment_ID= Installment.Installment_ID 
			LEFT JOIN [dbo].[Receive] ON Receive_Installment.Receive_ID = Receive.Receive_ID
			LEFT JOIN [dbo].My_User ON Purchase_Order.Employee_ID = My_User.User_ID
			LEFT JOIN [dbo].Department ON My_User.User_Dept_ID = Department.Department_ID 
			WHERE 
			Purchase_Order.Active = 'Y' 
			AND Purchase_Order.Agent_ID = '00001' 
			AND Receive.Receive_Status_ID = '003' 
			AND Receive_Installment.Installment_ID = '1'
			AND Purchase_Order.PO_Date >= '".$datestartPO."' 
			AND Receive.Receive_Date >= '".$datestart."' AND Receive.Receive_Date <= '".$dateNow."' 
			AND Purchase_Order.Employee_ID = My_User.User_ID" ;
			if($depar){
				$sql .= " AND My_User.User_Dept_ID = '".$depar."' ";
			}
			$sql .= " 
			GROUP BY Purchase_Order.Employee_ID, My_User.User_ID, My_User.User_FName, My_User.User_LName, Department.Department_Name, Department.Department_ID
			ORDER BY Department.Department_ID";
// echo $sql ;
		$stmt = sqlsrv_query( $connMS, $sql );
	  if(sqlsrv_has_rows($stmt)) {
	      while( $row = sqlsrv_fetch_array($stmt) ) { 
	        $resultarray[] = $row;
	      }
	      return $resultarray;
	      exit();
	  } 
}




function getSaleAgentforMounth($date){
		global $connMS;
	  $resultarray = array();
	  $exDate = explode("-", $date);
		$year = $exDate[0];
		$mounth = $exDate[1];
	 	$datestart =  $year."-".$mounth."-01";
	  if($year == date("Y") && $mounth == date("n")){
	    $dateNow = date("Y-m-d")." 23:59:59";
	  }else{
	    $dateNow = $year."-".$mounth."-".date('t',strtotime($year."-".$mounth))." 23:59:59";
	  }
		$sql = "SELECT Purchase_Order.Agent_ID, Agent.Agent_Name, Agent.Agent_Surname
		,SUM(case when Purchase_Order.Status_ID IN ('PV','RV','RVP','CLS','REJ')  
		AND Receive.Receive_Status_ID = '003' 
		AND Receive_Installment.Installment_ID = '1'
		AND Purchase_Order.PO_Date >= '".$datestartPO."' 
		AND Receive.Receive_Date >= '".$datestart."' AND Receive.Receive_Date <= '".$dateNow."'   
		then Purchase_Order.Premium_After_Disc end) AS sumNew
		,(SELECT SUM(Purchase_Order.Premium_After_Disc) FROM Purchase_Order WHERE Purchase_Order.Status_ID = 'RCA' 
		AND Purchase_Order.PO_Date >= '".$datestart."' AND Purchase_Order.PO_Date <= '".$dateNow."'  AND Purchase_Order.Agent_ID = Agent.Agent_ID ) AS sumRCA
		
		FROM [dbo].[Purchase_Order] 
		LEFT JOIN [dbo].[Installment] ON Purchase_Order.PO_ID = Installment.PO_ID
		LEFT JOIN [dbo].Receive_Installment ON Installment.PO_ID = Receive_Installment.PO_ID and Receive_Installment.Installment_ID= Installment.Installment_ID 
		LEFT JOIN [dbo].[Receive] ON Receive_Installment.Receive_ID = Receive.Receive_ID
		LEFT JOIN [dbo].[Agent] ON Purchase_Order.Agent_ID = Agent.Agent_ID
		WHERE 
		Purchase_Order.Active = 'Y' 
		AND Purchase_Order.Agent_ID != '00001'
		GROUP BY Purchase_Order.Agent_ID, Agent.Agent_ID, Agent.Agent_Name, Agent.Agent_Surname
		ORDER BY sumNew DESC";

			$stmt = sqlsrv_query( $connMS, $sql );
		  if(sqlsrv_has_rows($stmt)) {
		      while( $row = sqlsrv_fetch_array($stmt) ) { 
		        $resultarray[] = $row;
		      }
		      return $resultarray;
		      exit();
		  } 
}

function getSaleFromRenew($mounth, $year, $depar){
		global $connMS;
	  $resultarray = array();
	 	$datestartNow =  date("Y-m-d",strtotime($year."-".$mounth."-01"));
	  $dateNowLast = $year."-".$mounth."-".date('t',strtotime($year."-".$mounth))." 23:59:59";
	 	$datestart = date("Y-m-d",strtotime($year."-".$mounth."-01".'+1 years'));
		$dateNow = date("Y-m-t",strtotime($year."-".$mounth."-01".'+1 years'));
		$sql = "SELECT Compare_Policy.Employee_ID, My_User.User_ID, My_User.User_FName, My_User.User_LName, Department.Department_Name, Department.Department_ID ,COUNT(Compare_Policy.CP_ID) AS countCP ,SUM(Purchase_Order.Net_Premium) AS startPrice
			,(select SUM(Purchase_Order.Net_Premium) FROM Purchase_Order 
			WHERE Purchase_Order.Operation_Type_ID = 'R'
			AND Purchase_Order.Insurance_ID NOT IN ('029','055','056') 
			AND Purchase_Order.Status_ID IN ('RV','RVP','CLS') 
			AND Purchase_Order.Net_Premium > 0 
			AND Purchase_Order.Total_Premium <> 999.03 
			and Purchase_Order.Total_Premium <> 1099.19 
			and Purchase_Order.Total_Premium <> 4999.03 
			AND Purchase_Order.Coverage_End_Date >= '".$datestart."' AND Purchase_Order.Coverage_End_Date <= '".$dateNow."'
			and Purchase_Order.Employee_ID = Compare_Policy.Employee_ID 
			 )AS sumPaid

			,(select SUM(Purchase_Order.Net_Premium) FROM Purchase_Order 
			WHERE Purchase_Order.Operation_Type_ID = 'R'
			AND Purchase_Order.Insurance_ID NOT IN ('029','055','056') 
			AND Purchase_Order.Status_ID = 'PV' 
			AND Purchase_Order.Employee_ID = Compare_Policy.Employee_ID 
			AND Purchase_Order.Coverage_Start_Date >= '".$datestartNow."' AND Purchase_Order.Coverage_Start_Date <= '".$dateNowLast."'
			 )AS sumPV

			 ,(select COUNT(Purchase_Order.PO_ID) FROM Purchase_Order 
			WHERE Purchase_Order.Operation_Type_ID = 'R'
			AND Purchase_Order.Insurance_ID NOT IN ('029','055','056') 
			AND Purchase_Order.Status_ID = 'PV' 
			and Purchase_Order.Employee_ID = Compare_Policy.Employee_ID 
			AND Purchase_Order.Coverage_Start_Date >= '".$datestartNow."' AND Purchase_Order.Coverage_Start_Date <= '".$dateNowLast."'
			 )AS countPOPV

			 ,(select COUNT(Purchase_Order.Net_Premium) FROM Purchase_Order 
			WHERE Purchase_Order.Operation_Type_ID = 'R' 
			AND Purchase_Order.Insurance_ID NOT IN ('029','055','056')
			AND Purchase_Order.Status_ID IN ('RV','RVP','CLS') 
			AND Purchase_Order.Net_Premium > 0 
			AND Purchase_Order.Total_Premium <> 999.03 
			and Purchase_Order.Total_Premium <> 1099.19 
			and Purchase_Order.Total_Premium <> 4999.03 
			AND Purchase_Order.Coverage_Start_Date >= '".$datestartNow."' AND Purchase_Order.Coverage_Start_Date <= '".$dateNowLast."'
			and Purchase_Order.Employee_ID = Compare_Policy.Employee_ID 
			 )AS countPOPaid
			 
			 ,(select SUM(Purchase_Order.Net_Premium) FROM Purchase_Order 
			WHERE Purchase_Order.Operation_Type_ID = 'R'
			AND Purchase_Order.Insurance_ID NOT IN ('029','055','056')
			AND Purchase_Order.Status_ID IN ('OPN','RCA','RTA','NEW')
			AND Purchase_Order.Coverage_End_Date >= '".$datestart."' AND Purchase_Order.Coverage_End_Date <= '".$dateNow."'
			and Purchase_Order.Employee_ID = Compare_Policy.Employee_ID 
			 )AS sumRCA

			 ,(select COUNT(Purchase_Order.Net_Premium) FROM Purchase_Order 
			WHERE Purchase_Order.Operation_Type_ID = 'R'
			AND Purchase_Order.Insurance_ID NOT IN ('029','055','056')
			AND Purchase_Order.Status_ID IN ('OPN','RCA','RTA','NEW')
			AND Purchase_Order.Coverage_End_Date >= '".$datestart."' AND Purchase_Order.Coverage_End_Date <= '".$dateNow."'
			and Purchase_Order.Employee_ID = Compare_Policy.Employee_ID 
			 )AS countRCA

			FROM Compare_Policy
			INNER JOIN [dbo].Purchase_Order ON Compare_Policy.CP_PO_ID = Purchase_Order.PO_ID
			INNER JOIN [dbo].My_User ON Compare_Policy.Employee_ID = My_User.User_ID
			LEFT JOIN [dbo].Department ON My_User.User_Dept_ID = Department.Department_ID 

			WHERE Compare_Policy.CP_PO_ID != '' AND Purchase_Order.Active = 'Y'
			AND Compare_Policy.CP_ID NOT LIKE '%B%'
			AND Compare_Policy.Coverage_End_Date >= '".$datestart."' AND Compare_Policy.Coverage_End_Date <= '".$dateNow."'";
			// AND Compare_Policy.CP_ID NOT LIKE '%R%' ;
			if($depar){
				$sql .= " AND My_User.User_Dept_ID = '".$depar."' ";
			}
			$sql .= " GROUP BY Compare_Policy.Employee_ID, My_User.User_ID, My_User.User_FName, My_User.User_LName, Department.Department_Name, Department.Department_ID ORDER BY Department.Department_ID ASC , countCP DESC";
// echo $sql;
			$stmt = sqlsrv_query( $connMS, $sql );
		  if(sqlsrv_has_rows($stmt)) {
		      while( $row = sqlsrv_fetch_array($stmt) ) { 
		        $resultarray[] = $row;
		      }
		      return $resultarray;
		      exit();
		  } 
}

function getDepartment(){
	global $connMS;
	$resultarray = array();
	
	$sql = "SELECT * FROM Department WHERE  Department_ID >= 100  AND Active  = 'Y' ";
	$stmt = sqlsrv_query( $connMS, $sql );
  if(sqlsrv_has_rows($stmt)) {
    while( $row = sqlsrv_fetch_array($stmt) ) { 
      $resultarray[] = $row;
    }
    return $resultarray;
    exit();
  } 
}


function getSearch($year, $mounth){
	global $conn2;
	$sql = "SELECT log_statics.*,COUNT(id) AS search FROM log_statics";
	
	if($year){
		$sql .= " WHERE YEAR(dates) = '".$year."' AND MONTH(dates) = '".$mounth."' " ;
	}
	
	$sql .= " GROUP BY log_statics.make, log_statics.model, log_statics.year
	ORDER BY log_statics.make ASC, log_statics.model ASC, log_statics.year ASC ";
	// echo $sql;
  $act = $conn2->GetAssoc($sql);
  return $act;
}


function reportInformByAdmin($dateStart, $dateEnd, $insurer){
	global $conn2;
	$sql = "SELECT noti_work.*, ck_personnel.personnel_firstname AS adminFName, ck_personnel.personnel_lastname AS adminLName FROM noti_work 
	LEFT JOIN ck_personnel ON noti_work.admin_code = ck_personnel.personnel_code
	WHERE date(noti_work.datetime_send) BETWEEN '".$dateStart."' AND  '".$dateEnd."' AND noti_work.status = '4' AND noti_work.enable = '1' ";
	if($insurer){
		$sql .= " AND noti_work.insuere_company = '".$insurer."'";
	}
		$sql .= " ORDER BY noti_work.datetime_send ASC";
		// echo $sql;
	 	$act = $conn2->GetAssoc($sql);
  	return $act;
  	exit();
}



function getFollowUpSale($depar){
	global $connMS;
	$resultarray = array();

	$dateNowStart = date("Y-m-d")." 00:00:00";
	$dateNowEnd = date("Y-m-d")." 23:59:59";
	$dateNextOneStart = date("Y-m-d", strtotime($date . "+1 days"))." 00:00:00";
	$dateNextOneEnd = date("Y-m-d", strtotime($date . "+1 days"))." 23:59:59";

	$dateAllYear = date("Y")."-01-01";
	$sql = "SELECT My_User.User_ID, My_User.User_FName, My_User.User_LName, Department.Department_Name, Department.Department_ID
		,COUNT(case when Remind.Remind_Date BETWEEN '".$dateNowStart."' AND '".$dateNowEnd."' then Followup.Followup_ID end) AS DateNowFU
		,COUNT(case when Remind.Remind_Date BETWEEN '".$dateNextOneStart."' AND '".$dateNextOneEnd."' then Followup.Followup_ID end) AS TomorrowFU
		,COUNT(case when Remind.Remind_Date <= '".$dateNowEnd."' then Followup.Followup_ID end) AS LastFU
		,(SELECT COUNT(Compare_Policy.CP_ID) FROM Compare_Policy WHERE CP_PO_ID ='' AND CP_Date >= '".$dateAllYear."'
			AND Employee_ID = My_User.User_ID ) AS CountCPYearAll
		,(SELECT COUNT(Compare_Policy.CP_ID) FROM Compare_Policy WHERE CP_PO_ID ='' AND CP_Date <= '".$dateNowEnd."'
			AND Compare_Status_ID IN ('IPC','OPN','NEW') AND Employee_ID = My_User.User_ID ) AS CountCPYearActiveNew
		,(SELECT COUNT(Compare_Policy.CP_ID) FROM Compare_Policy WHERE CP_Date >= '".$dateAllYear."' AND CP_Date <= '".$dateNowEnd."'
			AND Compare_Status_ID IN ('IPC','OPN','NEW') AND Employee_ID = My_User.User_ID ) AS CountCPYearActiveOld
		,(SELECT COUNT(Compare_Policy.CP_ID) FROM Compare_Policy WHERE CP_PO_ID ='' AND CP_Date >= '".$dateAllYear."'
			AND Compare_Status_ID = 'POI' AND Employee_ID = My_User.User_ID ) AS CountCPYearPOI
		,(SELECT COUNT(Compare_Policy.CP_ID) FROM Compare_Policy WHERE CP_PO_ID ='' AND CP_Date >= '".$dateAllYear."'
			AND Compare_Status_ID IN ('REJ','SMS','REF') AND Employee_ID = My_User.User_ID ) AS CountCPYearREJ
		,(SELECT COUNT(Compare_Policy.CP_ID) FROM Compare_Policy WHERE CP_PO_ID ='' AND CP_Date >= '".$dateAllYear."'
			AND Compare_Status_ID IN ('REA','REB') AND Employee_ID = My_User.User_ID ) AS CountReAssign
		from Followup
		LEFT JOIN Compare_Policy ON Followup.Compare_Policy_ID = Compare_Policy.CP_ID
		LEFT JOIN My_User ON My_User.User_ID = Compare_Policy.Employee_ID
		LEFT JOIN Department ON My_User.User_Dept_ID = Department.Department_ID 
		LEFT JOIN Remind ON Followup.Followup_ID = Remind.Remark  

		WHERE 
		My_User.Active = 'Y' AND My_User.User_Group_ID = '004'
		AND Followup.Followup_ID = (SELECT MAX(Followup_ID) FROM Followup WHERE Compare_Policy_ID = Compare_Policy.CP_ID  )
		AND Followup.Followup_DateTime = (SELECT MAX(Followup_DateTime) FROM Followup WHERE Compare_Policy_ID = Compare_Policy.CP_ID  )
		AND Followup.Followup_Status_ID not in ('009', '010')
		AND Compare_Policy.Compare_Status_ID NOT IN ('POI','REF','REJ','SMS')";
	
	if($depar){
		$sql .= " AND My_User.User_Dept_ID = '".$depar."' ";
	}
		$sql .= "
		GROUP BY  My_User.User_ID, My_User.User_FName, My_User.User_LName, Department.Department_Name, Department.Department_ID
		ORDER BY Department.Department_ID DESC";
// echo $sql;
	$stmt = sqlsrv_query( $connMS, $sql );
	if(sqlsrv_has_rows($stmt)) {
	    while( $row = sqlsrv_fetch_array($stmt) ) { 
	      $resultarray[] = $row;
	    }
	    return $resultarray;
	    exit();
	} 
}


function getCallcenterBySubject($date){
	global $connMS;
	$resultarray = array();
	$dateStart = date("Y-m-d", strtotime($date))." 00:00:00";
	$dateEnd = date("Y-m-t", strtotime($date))." 23:59:59";

	$sql = "SELECT Call_Subject,count(Call_ID) AS sumCall
	FROM Callcenters
	WHERE Call_Date >= '".$dateStart."'  AND Call_Date <= '".$dateEnd."'
	GROUP BY Call_Subject
	ORDER BY sumcall DESC";

	$stmt = sqlsrv_query( $connMS, $sql );
	if(sqlsrv_has_rows($stmt)) {
	    while( $row = sqlsrv_fetch_array($stmt) ) { 
	      $resultarray[] = $row;
	    }
	    return $resultarray;
	    exit();
	} 
}


function getCallcenterByEmp($date){
	global $connMS;
	$resultarray = array();
	$dateStart = date("Y-m-d", strtotime($date))." 00:00:00";
	$dateEnd = date("Y-m-t", strtotime($date))." 23:59:59";

	$sql = "SELECT convert(date, Callcenters.Call_Date, 103) AS dateList
	,COUNT(case when Callcenters.Create_By = 'ADB60015'  then Callcenters.Call_ID end) AS emp1
	,COUNT(case when Callcenters.Create_By = 'ADB58018'  then Callcenters.Call_ID end) AS emp2
	FROM Callcenters LEFT JOIN My_User ON Callcenters.Create_By = My_User.User_ID
	WHERE Callcenters.Call_Date >= '".$dateStart."' AND  Callcenters.Call_Date <= '".$dateEnd."'
	GROUP BY convert(date, Callcenters.Call_Date, 103)";

	$stmt = sqlsrv_query( $connMS, $sql );
	if(sqlsrv_has_rows($stmt)) {
	    while( $row = sqlsrv_fetch_array($stmt) ) { 
	      $resultarray[] = $row;
	    }
	    return $resultarray;
	    exit();
	} 
}


function getReportFollow7Day($depart){
	global $connMS;
	$resultarray = array();
	$dateNow = date("Y-m-d");
	$dateNowStart = date("Y-m-d")." 00:00:00";
	$dateNowEnd = date("Y-m-d")." 23:59:59";
	$dateNextOneStart = date("Y-m-d", strtotime($date . "+1 days"))." 00:00:00";
	$dateNextOneEnd = date("Y-m-d", strtotime($date . "+1 days"))." 23:59:59";

	$sql = "SELECT My_User.User_ID, My_User.User_FName, My_User.User_LName, Department.Department_Name, Department.Department_ID
		,COUNT(case when Remind.Remind_Date BETWEEN '".date("Y-m-d", strtotime($dateNow . "-7 days"))." 00:00:00' AND '".date("Y-m-d", strtotime($dateNow . "-1 days"))." 23:59:59' then Followup.Followup_ID end) AS SumAfterDay
		,COUNT(case when Remind.Remind_Date BETWEEN '".date("Y-m-d", strtotime($dateNow . "-7 days"))." 00:00:00' AND '".date("Y-m-d", strtotime($dateNow . "-7 days"))." 23:59:59' then Followup.Followup_ID end) AS Day1
		,COUNT(case when Remind.Remind_Date BETWEEN '".date("Y-m-d", strtotime($dateNow . "-6 days"))." 00:00:00' AND '".date("Y-m-d", strtotime($dateNow . "-6 days"))." 23:59:59' then Followup.Followup_ID end) AS Day2
		,COUNT(case when Remind.Remind_Date BETWEEN '".date("Y-m-d", strtotime($dateNow . "-5 days"))." 00:00:00' AND '".date("Y-m-d", strtotime($dateNow . "-5 days"))." 23:59:59' then Followup.Followup_ID end) AS Day3
		,COUNT(case when Remind.Remind_Date BETWEEN '".date("Y-m-d", strtotime($dateNow . "-4 days"))." 00:00:00' AND '".date("Y-m-d", strtotime($dateNow . "-4 days"))." 23:59:59' then Followup.Followup_ID end) AS Day4
		,COUNT(case when Remind.Remind_Date BETWEEN '".date("Y-m-d", strtotime($dateNow . "-3 days"))." 00:00:00' AND '".date("Y-m-d", strtotime($dateNow . "-3 days"))." 23:59:59' then Followup.Followup_ID end) AS Day5
		,COUNT(case when Remind.Remind_Date BETWEEN '".date("Y-m-d", strtotime($dateNow . "-2 days"))." 00:00:00' AND '".date("Y-m-d", strtotime($dateNow . "-2 days"))." 23:59:59' then Followup.Followup_ID end) AS Day6
		,COUNT(case when Remind.Remind_Date BETWEEN '".date("Y-m-d", strtotime($dateNow . "-1 days"))." 00:00:00' AND '".date("Y-m-d", strtotime($dateNow . "-1 days"))." 23:59:59' then Followup.Followup_ID end) AS Day7
		,COUNT(case when Remind.Remind_Date BETWEEN '".$dateNow." 00:00:00' AND '".$dateNow." 23:59:59' then Followup.Followup_ID end) AS Day8
		,COUNT(case when Remind.Remind_Date BETWEEN '".date("Y-m-d", strtotime($dateNow . "+1 days"))." 00:00:00' AND '".date("Y-m-d", strtotime($dateNow . "+1 days"))." 23:59:59' then Followup.Followup_ID end) AS Day9
		,COUNT(case when Remind.Remind_Date BETWEEN '".date("Y-m-d", strtotime($dateNow . "+2 days"))." 00:00:00' AND '".date("Y-m-d", strtotime($dateNow . "+2 days"))." 23:59:59' then Followup.Followup_ID end) AS Day10
		,COUNT(case when Remind.Remind_Date BETWEEN '".date("Y-m-d", strtotime($dateNow . "+3 days"))." 00:00:00' AND '".date("Y-m-d", strtotime($dateNow . "+3 days"))." 23:59:59' then Followup.Followup_ID end) AS Day11
		,COUNT(case when Remind.Remind_Date BETWEEN '".date("Y-m-d", strtotime($dateNow . "+4 days"))." 00:00:00' AND '".date("Y-m-d", strtotime($dateNow . "+4 days"))." 23:59:59' then Followup.Followup_ID end) AS Day12
		,COUNT(case when Remind.Remind_Date BETWEEN '".date("Y-m-d", strtotime($dateNow . "+5 days"))." 00:00:00' AND '".date("Y-m-d", strtotime($dateNow . "+5 days"))." 23:59:59' then Followup.Followup_ID end) AS Day13
		,COUNT(case when Remind.Remind_Date BETWEEN '".date("Y-m-d", strtotime($dateNow . "+6 days"))." 00:00:00' AND '".date("Y-m-d", strtotime($dateNow . "+6 days"))." 23:59:59' then Followup.Followup_ID end) AS Day14
		,COUNT(case when Remind.Remind_Date BETWEEN '".date("Y-m-d", strtotime($dateNow . "+7 days"))." 00:00:00' AND '".date("Y-m-d", strtotime($dateNow . "+7 days"))." 23:59:59' then Followup.Followup_ID end) AS Day15
		from Followup
		LEFT JOIN Compare_Policy ON Followup.Compare_Policy_ID = Compare_Policy.CP_ID
		LEFT JOIN My_User ON My_User.User_ID = Compare_Policy.Employee_ID
		LEFT JOIN Department ON My_User.User_Dept_ID = Department.Department_ID 
		LEFT JOIN Remind ON Followup.Followup_ID = Remind.Remark  

		WHERE 
		My_User.Active = 'Y' AND My_User.User_Group_ID = '004'
		AND Followup.Followup_ID = (SELECT MAX(Followup_ID) FROM Followup WHERE Compare_Policy_ID = Compare_Policy.CP_ID  )
		AND Followup.Followup_DateTime = (SELECT MAX(Followup_DateTime) FROM Followup WHERE Compare_Policy_ID = Compare_Policy.CP_ID  )
		AND Followup.Followup_Status_ID not in ('009', '010')
		AND Compare_Policy.Compare_Status_ID NOT IN ('POI','REF','REJ','SMS') 
		AND Department.Department_ID = '".$depart."'
		GROUP BY  My_User.User_ID, My_User.User_FName, My_User.User_LName, Department.Department_Name, Department.Department_ID
		ORDER BY My_User.User_ID ASC";

		$stmt = sqlsrv_query( $connMS, $sql );
		if(sqlsrv_has_rows($stmt)) {
		    while( $row = sqlsrv_fetch_array($stmt) ) { 
		      $resultarray[] = $row;
		    }
		    return $resultarray;
		    exit();
		} 

}


function getFollowUpOutstand($userid){
	global $connMS;
	$resultarray = array();
	$dateNow = date("Y-m-d");
	$dateNowStart = date("Y-m-d")." 00:00:00";
	$dateNowEnd = date("Y-m-d")." 23:59:59";
	$dateNextOneStart = date("Y-m-d", strtotime($date . "+1 days"))." 00:00:00";
	$dateNextOneEnd = date("Y-m-d", strtotime($date . "+1 days"))." 23:59:59";

	$sql = "SELECT Followup.Followup_ID ,Followup.Compare_Policy_ID ,Followup.Followup_DateTime, Followup.Followup_Detail, Followup.Create_By ,Remind.Remind_Date
		from Followup
		LEFT JOIN Compare_Policy ON Followup.Compare_Policy_ID = Compare_Policy.CP_ID
		LEFT JOIN My_User ON My_User.User_ID = Compare_Policy.Employee_ID
		LEFT JOIN Department ON My_User.User_Dept_ID = Department.Department_ID 
		LEFT JOIN Remind ON Followup.Followup_ID = Remind.Remark  

		WHERE 
		My_User.User_ID = '".$userid."' 
		AND Remind.Remind_Date BETWEEN '".date("Y-m-d", strtotime($dateNow . "-7 days"))." 00:00:00' AND '".date("Y-m-d", strtotime($dateNow . "-1 days"))." 23:59:59'
		AND Followup.Followup_ID = (SELECT MAX(Followup_ID) FROM Followup WHERE Compare_Policy_ID = Compare_Policy.CP_ID  )
		AND Followup.Followup_DateTime = (SELECT MAX(Followup_DateTime) FROM Followup WHERE Compare_Policy_ID = Compare_Policy.CP_ID  )
		AND Followup.Followup_Status_ID not in ('009', '010')
		AND Compare_Policy.Compare_Status_ID NOT IN ('POI','REF','REJ','SMS') 
		ORDER BY Remind.Remind_Date ASC";


	$stmt = sqlsrv_query( $connMS, $sql );
	if(sqlsrv_has_rows($stmt)) {
	    while( $row = sqlsrv_fetch_array($stmt) ) { 
	      $resultarray[] = $row;
	    }
	    return $resultarray;
	    exit();
	} 

}

function getSaleCountAsiaVBI($date, $depar){
	global $connMS;
	$exDate = explode("-", $date);
	$year = $exDate[0];
	$mounth = $exDate[1];
  $resultarray = array();
 	$datestart =  $year."-".$mounth."-01";

 	$datestartPO = date("Y-m-d",strtotime($datestart.'-3 month'));

  if($year == date("Y") && $mounth == date("n")){
    $dateNow = date("Y-m-d")." 23:59:59";
  }else{
    $dateNow = $year."-".$mounth."-".date('t',strtotime($year."-".$mounth))." 23:59:59";
  }
	$sql = "SELECT Purchase_Order.Employee_ID, My_User.User_ID, My_User.User_FName, My_User.User_LName, Department.Department_Name, Department.Department_ID
			,COUNT(case when Purchase_Order_Insurer.Insurer_ID = '017' AND Purchase_Order.Insurance_ID NOT IN ('029','068','069') then Purchase_Order.PO_ID end) AS POPaidVBICaseIns
			,COUNT(case when Purchase_Order_Insurer.Insurer_ID = '017' AND Purchase_Order.Insurance_ID IN ('029') then Purchase_Order.PO_ID end) AS POPaidVBICaseCon

			,SUM(case when Purchase_Order_Insurer.Insurer_ID = '071' then Purchase_Order.Premium_After_Disc end) AS POPaidRoojaiSum
			,COUNT(case when Purchase_Order_Insurer.Insurer_ID = '071' then Purchase_Order.PO_ID end) AS POPaidRoojaiCount
			,SUM(case when Purchase_Order.Insurance_Package_ID IN ('6462','6463','6464','6466') then Purchase_Order.Premium_After_Disc end) AS POPaidSumC72
			,COUNT(case when Purchase_Order.Insurance_Package_ID IN ('6462','6463','6464','6466') then Purchase_Order.PO_ID end) AS POPaidCountC72


			FROM [dbo].[Purchase_Order] 
			LEFT JOIN [dbo].Purchase_Order_Insurer ON Purchase_Order.PO_ID = Purchase_Order_Insurer.PO_ID
			LEFT JOIN [dbo].[Installment] ON Purchase_Order.PO_ID = Installment.PO_ID
			LEFT JOIN [dbo].Receive_Installment ON Installment.PO_ID = Receive_Installment.PO_ID and Receive_Installment.Installment_ID= Installment.Installment_ID 
			LEFT JOIN [dbo].[Receive] ON Receive_Installment.Receive_ID = Receive.Receive_ID
			LEFT JOIN [dbo].My_User ON Purchase_Order.Employee_ID = My_User.User_ID
			LEFT JOIN [dbo].Department ON My_User.User_Dept_ID = Department.Department_ID 
			LEFT JOIN [dbo].Car_Detail ON Purchase_Order.PO_ID = Car_Detail.PO_ID
			WHERE 
			Purchase_Order.Active = 'Y' 
			AND Purchase_Order.Agent_ID = '00001' 
			AND Receive.Receive_Status_ID = '003' 
			AND Receive_Installment.Installment_ID = '1'
			AND Purchase_Order.PO_Date >= '".$datestartPO."' 
			AND Receive.Receive_Date >= '".$datestart."' AND Receive.Receive_Date <= '".$dateNow."' 
			AND Purchase_Order.Status_ID IN ('RV','RVP','CLS')
			AND Purchase_Order.Employee_ID = My_User.User_ID" ;
			if($depar){
				$sql .= " AND My_User.User_Dept_ID = '".$depar."' ";
			}
			$sql .= " 
			GROUP BY Purchase_Order.Employee_ID, My_User.User_ID, My_User.User_FName, My_User.User_LName, Department.Department_Name, Department.Department_ID
			ORDER BY Department.Department_ID";
// echo $sql ;
		$stmt = sqlsrv_query( $connMS, $sql );
	  if(sqlsrv_has_rows($stmt)) {
	      while( $row = sqlsrv_fetch_array($stmt) ) { 
	        $resultarray[] = $row;
	      }
	      return $resultarray;
	      exit();
	  } 
}



?>