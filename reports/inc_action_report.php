<?php
session_start();
date_default_timezone_set("Asia/Bangkok");
header('Content-Type: text/html; charset=utf-8');

include "../inc_config.php";
include "inc_function_report.php"; 


if($_POST["action"] == "reportCharQC"){
	$resultarray = array();
	// $i = 0;
	$getListQC = getListQC();
	$getDataCount = getDataCount($_POST["mounth"]);

 	$date = array();
 	foreach ($getDataCount as $key => $value) {
    $date[] = date("d",strtotime($key));
 	}

 	for($i=0; $i < count($getListQC); $i++){
 		$qccode = $getListQC[$i]["personnel_code"];
 		foreach ($getDataCount as $key => $value) {
 			$countNum = $getDataCount[$key][$qccode] ? $getDataCount[$key][$qccode] : 0; 
 			${$qccode}[] = $countNum;
	 	}
	 	$resultarray[$qccode] =  ${$qccode};
 	}

 	$resultarray["date"] = $date;
 	echo json_encode($resultarray);
  exit();


 }else if($_POST["action"] == "reportCharCallcenterOne"){

	$resultarray = array();
	$getCallcenterBySubject = getCallcenterBySubject($_POST["mounth"]);
	// echo "<pre>".print_r($getCallcenterBySubject,1)."</pre>";
	foreach ($getCallcenterBySubject as $key => $value) {
    	$resultarray[$key]["subject"] = $value["Call_Subject"];
    	$resultarray[$key]["sumCase"] = $value["sumCall"];
 	}
 	echo json_encode($resultarray);
	exit();

}else if($_POST["action"] == "reportCharBydate"){

	$resultarray = array();
	$getCallcenterByEmp = getCallcenterByEmp($_POST["mounth"]);
	// echo "<pre>".print_r($getCallcenterByEmp,1)."</pre>";
	foreach ($getCallcenterByEmp as $key => $value) {
		// $date = $value["dateList"]->format("d");
		$resultarray[$key]["date"] = $value["dateList"]->format("d");
    	$resultarray[$key]["emp1"] = $value["emp1"];
    	$resultarray[$key]["emp2"] = $value["emp2"];
 	}
 	// echo "<pre>".print_r($resultarray,1)."</pre>";
 	echo json_encode($resultarray);
	exit();

}else if($_POST["action"] == "getFollowUpOutstand"){
	$getFollowUpOutstand = getFollowUpOutstand($_POST["userid"]);

	// echo "<pre>".print_r($getFollowUpOutstand,1)."</pre>";
	if($getFollowUpOutstand){
		foreach ($getFollowUpOutstand as $key => $value) {
			$html .= 
			'<tr>
				<td>'.$value["Compare_Policy_ID"].'</td>
				<td>'.$value["Followup_DateTime"]->format('d-m-Y').'</td>
				<td>'.$value["Followup_Detail"].'</td>
				<td>'.$value["Remind_Date"]->format('d-m-Y H:i:s').'</td>
				<td>'.$value["Create_By"].'</td>
			</tr>';
		}
	}
	echo $html;
	exit();
}


?>