
<?php

date_default_timezone_set('Asia/Bangkok');
header('Content-Type: text/html; charset=utf-8');

$strExcelFileName = "ReportCallcanter".date('YmdHis').".xls";

header("Content-Type: application/vnd.ms-excel; name=\"$strExcelFileName\"");
header("Content-Disposition: inline; filename=\"$strExcelFileName\"");
header("Pragma:no-cache");

include "../inc_config.php"; 
include "inc_function_report.php";
$year = date("Y");
$_GET["date"] = ($_GET["date"]) ? $_GET["date"] : date("Y-m");
if($_GET["date"]){
    $getCallcenterBySubject = getCallcenterBySubject($_GET["date"]);
    $getCallcenterByEmp = getCallcenterByEmp($_GET["date"]);
}
// $reportInformByAdmin = reportInformByAdmin($_GET["ds"], $_GET["de"], $_GET["insurers"]);
// echo "<pre>".print_r($reportInformByAdmin,1)."</pre>";

?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>
    <div>รายงาน ประจำเดือน <?php echo date("F Y");?></div>
    <div id="SiXhEaD_Excel" align=center x:publishsource="Excel">
        <table x:str border=1 cellpadding=1 cellspacing=1 width=100% style="border-collapse:collapse">
            <tr>
                <th valign="middle">#</th>
                <th valign="middle">เรื่องทีติดต่อ</th>
                <th valign="middle">จำนวนสาย</th>
            </tr>
            <?php 
                $sumAll = 0;
                $i = 1;
                foreach ($getCallcenterBySubject as $key => $value) {
                    $sumAll +=  $value["sumCall"];
            ?>
                <tr>
                    <td valign="middle"><?php echo $i++; ?></td>
                    <td valign="middle"><?php echo $value["Call_Subject"]; ?></td>
                    <td class="t_c fwb "><?php echo $value["sumCall"]; ?></td>
                </tr>
            <?php } ?>
            <tfoot>
                <tr class="fwb fs16 bgfffbd8">
                    <th valign="middle" colspan="2">รวม</th>
                    <th valign="middle"><?php echo $sumAll; ?></th>
                </tr>
            </tfoot>
        </table>
    </div>
    <div> สรุปแยกตามผู้รับสาย</div>
    <?php if($getCallcenterBySubject){?>
     <div id="SiXhEaD_Excel" align=center x:publishsource="Excel">
        <table x:str border=1 cellpadding=1 cellspacing=1 width=100% style="border-collapse:collapse">
            <tr>
                <th valign="middle">ผู้รับสาย</th>
                <th valign="middle">อัมพกา ปัญญาบุญ</th>
                <th valign="middle">ธนวิชญ์ แก้วแกมทอง</th>
            </tr>
            <?php 
                $sumAll = 0;
                $i = 1;
                foreach ($getCallcenterByEmp as $key => $value) {
                    $sumEmp1 +=  $value["emp1"];
                    $sumEmp2 +=  $value["emp2"];
            ?>
                <tr>
                    <td class="t_c fwb "><?php echo $value["dateList"]->format("d/m/Y"); ?></td>
                    <td class="t_c"><?php echo $value["emp1"]; ?></td>
                    <td class="t_c"><?php echo $value["emp2"]; ?></td>
                </tr>
            <?php } ?>
            <tfoot>
                <tr class="fwb fs16 bgfffbd8">
                    <th class="t_c" >รวม</th>
                    <th class="t_c fwb "><?php echo $sumEmp1; ?></th>
                    <th class="t_c fwb "><?php echo $sumEmp2; ?></th>
                </tr>
            </tfoot>
        </table>
    </div>
    <?php } ?>
    <script>
        window.onbeforeunload = function(){return false;};
        setTimeout(function(){window.close();}, 10000);
    </script>
</body>
</html>
