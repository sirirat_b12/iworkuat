
<?php

date_default_timezone_set('Asia/Bangkok');
header('Content-Type: text/html; charset=utf-8');

$strExcelFileName = "ReportInform".date('YmdHis').".xls";

header("Content-Type: application/vnd.ms-excel; name=\"$strExcelFileName\"");
header("Content-Disposition: inline; filename=\"$strExcelFileName\"");
header("Pragma:no-cache");

include "../inc_config.php"; 
include "inc_function_report.php";
$reportInformByAdmin = reportInformByAdmin($_GET["ds"], $_GET["de"], $_GET["insurers"]);
// echo "<pre>".print_r($reportInformByAdmin,1)."</pre>";

?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>
    <div id="SiXhEaD_Excel" align=center x:publishsource="Excel">
        <table x:str border=1 cellpadding=1 cellspacing=1 width=100% style="border-collapse:collapse">
            <tr>
                <th valign="middle">PO</th>
                <th valign="middle">วันที่แจ้งงาน</th>
                <th valign="middle">ผู้แจ้งงาน</th>
                <th valign="middle">สถานะ</th> 
                <th valign="middle">ชื่อ-นามสกุล</th>
                <th valign="middle">ประเภทประกัน</th>
                <th valign="middle">บริษัท</th>
                <th valign="middle">แพจเกต</th>
                <th valign="middle">วันคุ้มครอง</th>
                <th valign="middle">เลขกรม</th>
                <th valign="middle">ส่งกรม</th>
                <th valign="middle">ผู้ขาย</th>
                <th valign="middle">ทำรายการ</th>
            </tr>
            <?php 
                $i = 1;
                foreach ($reportInformByAdmin as $key => $value) {  
                    $endorse = ($value["endorse"] == "1") ? "สลักหลัง" : "กรมธรรม์ใหม่"; 
            ?>
                    <tr>
                        <td valign="middle"><b><?php echo $value["po_code"];?></b></td>
                        <td><b><?php echo $value["datetime_send"]; ?></b></td>
                        <td valign="middle"><?php echo $value["adminFName"]." ".$value["adminLName"]; ?></td>
                        <td valign="middle"><?php echo $endorse ?></td> 
                        <td valign="middle"><b><?php echo $value["cus_name"]; ?></b></td>
                        <td valign="middle"><?php echo $value["insurance_type"]; ?></td>
                        <td valign="middle"><b><?php echo $value["insuere_company"]; ?></b></td>
                        <td valign="middle"><?php echo $value["package_name"]; ?></td>
                        <td valign="middle"><?php echo date("d-m-Y",strtotime($value["start_cover_date"])); ?></td>
                        <td valign="middle"><b><?php echo $value["policy_number"]; ?></b></td>
                        <td valign="middle"><?php echo $value["send_type"]; ?></td>
                        <td valign="middle"><?php echo $value["personnel_name"]; ?></td>
                        <td valign="middle"><?php echo $value["created_date"]; ?></td>
                    </tr>
                    <?php
                }
            ?>
        </table>
    </div>
    <script>
        window.onbeforeunload = function(){return false;};
        setTimeout(function(){window.close();}, 10000);
    </script>
</body>
</html>
