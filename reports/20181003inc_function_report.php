<?php 

function getPersonProfile($code,$column='0') { // by tor 14-09-2018
	  global $conn2;
	  $sql = $conn2->GetAll( "SELECT `ck_personnel`.* FROM  `ck_personnel` WHERE  `ck_personnel`.`personnel_code` =  '".$code."' LIMIT 0 , 1");	  
	  return $sql[0][$column];
	  exit();
}

function diff2time($time_a,$time_b){
    $now_time1=strtotime($time_a);
    $now_time2=strtotime($time_b);
    $time_diff=abs($now_time2-$now_time1);
    $time_diff_h=floor($time_diff/3600); // จำนวนชั่วโมงที่ต่างกัน
    $time_diff_m=floor(($time_diff%3600)/60); // จำวนวนนาทีที่ต่างกัน
    $time_diff_s=($time_diff%3600)%60; // จำนวนวินาทีที่ต่างกัน
    return $time_diff_h." ช. ".$time_diff_m." น. ".$time_diff_s." ว.";
}


function setStatusPO($name){
  switch ($name) {
    case "0": return "<b style='color: #2457ff;'>รอตรวจ</b>"; break;
    case "1": return "<b style='color: #f44;'>ไม่ผ่าน</b>"; break;
    case "2": return "<b style='color: #ff8000;'>รอแจ้งงาน</b>"; break;
    case "3": return "<b style='color: #c600c4;'>แจ้งงาน</b>"; break;
    case "4": return "<b style='color: #00ac0a;'>เสร็จ</b>"; break;
  }
}


function getMonthSelect(){
  global $conn2;
  $sql = $conn2->GetAll( "SELECT DISTINCT MONTH(noti_work.created_date) AS months FROM noti_work GROUP BY noti_work.created_date");
  return $sql;   
  exit();
}


function getMountListdate($months){
	global $conn2; 
	$sql = $conn2->GetAll( "SELECT date(noti_work.created_date) AS dates FROM  noti_work 
	where MONTH(noti_work.created_date) = '".$months."' AND Year(noti_work.created_date) = '".date("Y")."'
	GROUP BY date(noti_work.created_date) 
	ORDER BY date(noti_work.created_date) ASC");
	return $sql;  
	exit();
}

function getListQC(){
	global $conn2;
	$sql = $conn2->GetAll( "SELECT * FROM  ck_personnel where personnel_type = 'QualityControl' ");
	return $sql; 
	exit(); 
}


function getListAdmin($months){
	global $conn2;
	$sql = $conn2->GetAll( "SELECT noti_work.admin_code FROM noti_work WHERE admin_code !='' AND	MONTH (noti_work.datetime_chk) = '".$months."' AND YEAR (noti_work.datetime_chk) = '".date("Y")."' GROUP BY admin_code ");
	return $sql; 
	exit(); 
}

function getDataCount($months){
	global $conn2; 
	// $getListQC = getListQC();
	$sql = $conn2->GetAll( "SELECT date(noti_work.created_date) AS dates, noti_work.chk_code, count(noti_work_id) AS counts , SUM(TIMESTAMPDIFF(SECOND,datetime_chk_open, datetime_chk)) AS sumTime
	FROM  noti_work 
	where MONTH(noti_work.created_date) = '".$months."' AND Year(noti_work.created_date) = '".date("Y")."'  
	GROUP BY date(noti_work.created_date), noti_work.chk_code
	ORDER BY date(noti_work.created_date) ASC");
	$data = array();
	foreach ($sql as $key => $value) {
			$data[$value["dates"]][$value["chk_code"]] = $value["counts"];
			$data[$value["dates"]]["sumTime"][$value["chk_code"]] = $value["sumTime"];
	}
	// echo "<pre>".print_r($data,1)."</pre>";
	return $data; 
	exit(); 
}
function getDataToAdmin($months){
	global $conn2; 
	// $getListQC = getListQC();
	$sql = $conn2->GetAll( "SELECT date(noti_work.datetime_chk) AS dates, noti_work.admin_code, count(noti_work_id) AS counts FROM  noti_work 
	where admin_code !='' AND MONTH(noti_work.datetime_chk) = '".$months."' AND Year(noti_work.datetime_chk) = '".date("Y")."'  
	GROUP BY date(noti_work.datetime_chk), noti_work.admin_code
	ORDER BY date(noti_work.datetime_chk) ASC");
	$data = array();
	foreach ($sql as $key => $value) {
			$data[$value["dates"]][$value["admin_code"]] = $value["counts"];
	}
	// echo "<pre>".print_r($data,1)."</pre>";
	return $data; 
	exit(); 
}

function getDetailPOByQC($user, $date ){
	global $conn2; 
	$data = array();
	$i = 0;
	$sql = $conn2->GetAssoc( "SELECT noti_work_code, po_code, insurance_type, package_name, personnel_name, insuere_company, status, created_date, cus_name, chk_code,datetime_chk_open, datetime_chk, enable FROM  noti_work where chk_code = '".$user."' AND date(created_date) = '".$date."' ");
	foreach ($sql as $key => $value) {
		$steplists = $conn2->GetAll( "SELECT noti_comment_id, comment, datetime, comment_byname FROM  noti_comment where noti_work_code = '".$value["noti_work_code"]."' ORDER BY datetime");
		array_push($data, $value);
		$data[$i]["steplists"] = $steplists;
		$i++;
	}
	return $data; 
	exit(); 
}


function getstaticSale($months){
	global $conn2; 
	
	$sql = $conn2->GetAssoc("SELECT nw.personnel_code, nw.personnel_name,
	(SELECT COUNT(n1.noti_work_id) FROM noti_work n1 WHERE n1.personnel_code = nw.personnel_code AND MONTH(n1.created_date) = '".$months."' AND YEAR(n1.created_date) = '".date("Y")."') AS numall,
	(SELECT COUNT(n2.noti_work_id) FROM noti_work n2 WHERE n2.personnel_code = nw.personnel_code AND n2.enable = '1' AND MONTH(n2.created_date) = '".$months."' AND YEAR(n2.created_date) = '".date("Y")."') AS enable_1,
	(SELECT COUNT(n3.noti_work_id) FROM noti_work n3 WHERE n3.personnel_code = nw.personnel_code AND n3.enable = '0' AND MONTH(n3.created_date) = '".$months."' AND YEAR(n3.created_date) = '".date("Y")."') AS enable_0,
	(SELECT COUNT(n4.noti_work_id) FROM noti_work n4 WHERE n4.personnel_code = nw.personnel_code AND n4.status = '4' AND MONTH(n4.created_date) = '".$months."' AND YEAR(n4.created_date) = '".date("Y")."') AS status_4
	FROM noti_work nw WHERE MONTH(nw.created_date) = '".$months."' AND YEAR(nw.created_date) = '".date("Y")."' GROUP BY nw.personnel_code
	");
	return $sql; 
	exit(); 
}


function getDetailPOBySale($user, $months ){
	global $conn2; 
	$data = array();
	$i = 0;
	$sql = $conn2->GetAssoc( "SELECT noti_work_code, po_code, insurance_type, package_name, personnel_name, insuere_company, status, created_date, cus_name, chk_code, datetime_chk, enable FROM  noti_work where personnel_code = '".$user."' AND MONTH(created_date) = '".$months."' AND YEAR(created_date) = '".date("Y")."' " );
	foreach ($sql as $key => $value) {
		$steplists = $conn2->GetAll( "SELECT noti_comment_id, comment, datetime, comment_byname FROM  noti_comment where noti_work_code = '".$value["noti_work_code"]."' ORDER BY datetime");
		array_push($data, $value);
		$data[$i]["steplists"] = $steplists;
		$i++;
	}
	return $data; 
	exit(); 
}


function getDetailPOByAdmin($user, $date ){
	global $conn2; 
	$data = array();
	$i = 0;
	$sql = $conn2->GetAssoc( "SELECT noti_work_code, po_code, insurance_type, package_name, personnel_name, insuere_company, status, created_date, cus_name, chk_code, admin_code, datetime_chk, datetime_chk_open, datetime_send_open, datetime_send, enable FROM  noti_work where admin_code = '".$user."' AND date(datetime_chk) = '".$date."' ");
	foreach ($sql as $key => $value) {
		$steplists = $conn2->GetAll( "SELECT noti_comment_id, comment, datetime, comment_byname FROM  noti_comment where noti_work_code = '".$value["noti_work_code"]."' ORDER BY datetime");
		array_push($data, $value);
		$data[$i]["steplists"] = $steplists;
		$i++;
	}
	return $data; 
	exit(); 
}


function getSaleSumforMounth($mounth){
	global $connMS;
  $resultarray = array();
 	$datestart =  date("Y")."-".$mounth."-01";
  if($mounth == date("n")){
    $dateNow = date("Y-m-d")." 23:59:59";
  }else{
    $dateNow = date("Y")."-".$mounth."-".date('t',strtotime(date("Y")."-".$mounth))." 23:59:59";
  }
	$sql = "SELECT  My_User.User_ID, My_User.User_FName, My_User.User_LName,Department.Department_Name,
		(SELECT sum(Purchase_Order.Premium_After_Disc) 
			FROM [dbo].[Purchase_Order] 
		    LEFT JOIN  [dbo].[Installment] ON Purchase_Order.PO_ID = Installment.PO_ID
		    LEFT JOIN [dbo].Receive_Installment ON Installment.PO_ID = Receive_Installment.PO_ID and Receive_Installment.Installment_ID= Installment.Installment_ID 
		    LEFT JOIN [dbo].[Receive] ON Receive_Installment.Receive_ID = Receive.Receive_ID
		    WHERE Receive.Receive_Date >= '".$datestart."' AND Receive.Receive_Date <= '".$dateNow."' 
		    AND Purchase_Order.Active = 'Y' 
		    AND Purchase_Order.Agent_ID = '00001' 
		    AND Purchase_Order.Operation_Type_ID = 'N'
		    AND Purchase_Order.Status_ID IN ('RV','RVP','CLS','REJ')
		    AND Receive.Receive_Status_ID = '003' 
		    AND Receive_Installment.Installment_ID = '1'
		    AND Purchase_Order.Employee_ID = My_User.User_ID
		    ) 
		 AS sumNew,
		(SELECT sum(Purchase_Order.Premium_After_Disc) 
			FROM [dbo].[Purchase_Order] 
		    LEFT JOIN  [dbo].[Installment] ON Purchase_Order.PO_ID = Installment.PO_ID
		    LEFT JOIN [dbo].Receive_Installment ON Installment.PO_ID = Receive_Installment.PO_ID and Receive_Installment.Installment_ID= Installment.Installment_ID 
		    LEFT JOIN [dbo].[Receive] ON Receive_Installment.Receive_ID = Receive.Receive_ID
		    WHERE Receive.Receive_Date >= '".$datestart."' AND Receive.Receive_Date <= '".$dateNow."' 
		    AND Purchase_Order.Active = 'Y' 
		    AND Purchase_Order.Agent_ID = '00001' 
		    AND Purchase_Order.Operation_Type_ID = 'R'
		    AND Purchase_Order.Status_ID IN ('RV','RVP','CLS','REJ')
		    AND Receive.Receive_Status_ID = '003' 
		    AND Receive_Installment.Installment_ID = '1'
		    AND Purchase_Order.Employee_ID = My_User.User_ID
		    ) 
		 AS sumReNew
		 ,
		(SELECT sum(Purchase_Order.Premium_After_Disc) 
			FROM [dbo].[Purchase_Order] 
		    LEFT JOIN  [dbo].[Installment] ON Purchase_Order.PO_ID = Installment.PO_ID
		    LEFT JOIN [dbo].Receive_Installment ON Installment.PO_ID = Receive_Installment.PO_ID and Receive_Installment.Installment_ID= Installment.Installment_ID 
		    LEFT JOIN [dbo].[Receive] ON Receive_Installment.Receive_ID = Receive.Receive_ID
		    WHERE Receive.Receive_Date >= '".$datestart."' AND Receive.Receive_Date <= '".$dateNow."' 
		    AND Purchase_Order.Active = 'Y' 
		    AND Purchase_Order.Agent_ID = '00001' 
		    AND Purchase_Order.Status_ID IN ('REJ')
		    AND Receive.Receive_Status_ID = '003' 
		    AND Receive_Installment.Installment_ID = '1'
		    AND Purchase_Order.Employee_ID = My_User.User_ID
		    ) 
		 AS sumREJ
		 ,
		(SELECT sum(Purchase_Order.Premium_After_Disc) 
			FROM [dbo].[Purchase_Order] 
		    LEFT JOIN  [dbo].[Installment] ON Purchase_Order.PO_ID = Installment.PO_ID
		    LEFT JOIN [dbo].Receive_Installment ON Installment.PO_ID = Receive_Installment.PO_ID and Receive_Installment.Installment_ID= Installment.Installment_ID 
		    LEFT JOIN [dbo].[Receive] ON Receive_Installment.Receive_ID = Receive.Receive_ID
		    WHERE Receive.Receive_Date >= '".$datestart."' AND Receive.Receive_Date <= '".$dateNow."' 
		    AND Purchase_Order.Active = 'Y' 
		    AND Purchase_Order.Agent_ID = '00001' 
		    AND Purchase_Order.Operation_Type_ID = 'C'
		    AND Receive.Receive_Status_ID = '003' 
		    AND Receive_Installment.Installment_ID = '1'
		    AND Purchase_Order.Employee_ID = My_User.User_ID
		    ) 
		 AS sumC 
		  ,
		(SELECT sum(Purchase_Order.Premium_After_Disc) 
			FROM [dbo].[Purchase_Order] 
		    LEFT JOIN  [dbo].[Installment] ON Purchase_Order.PO_ID = Installment.PO_ID
		    LEFT JOIN [dbo].Receive_Installment ON Installment.PO_ID = Receive_Installment.PO_ID and Receive_Installment.Installment_ID= Installment.Installment_ID 
		    LEFT JOIN [dbo].[Receive] ON Receive_Installment.Receive_ID = Receive.Receive_ID
		    WHERE Receive.Receive_Date >= '".$datestart."' AND Receive.Receive_Date <= '".$dateNow."' 
		    AND Purchase_Order.Active = 'Y' 
		    AND Purchase_Order.Agent_ID = '00001' 
		    AND Purchase_Order.Operation_Type_ID = 'D'
		    AND Receive.Receive_Status_ID = '003' 
		    AND Receive_Installment.Installment_ID = '1'
		    AND Purchase_Order.Employee_ID = My_User.User_ID
		    ) 
		 AS sumD 
		FROM [dbo].My_User 
		LEFT JOIN  [dbo].Department ON My_User.User_Dept_ID = Department.Department_ID 
		WHERE Department.Department_ID >= 100 AND My_User.Active = 'Y' 
		ORDER BY Department.Department_ID";

		 $stmt = sqlsrv_query( $connMS, $sql );
	  if(sqlsrv_has_rows($stmt)) {
	      while( $row = sqlsrv_fetch_array($stmt) ) { 
	        $resultarray[] = $row;
	      }
	      return $resultarray;
	      exit();
	  } 
}
?>