<?php 

session_start();
$UserCode = $_SESSION["User"]['UserCode'];
$userType = $_SESSION["User"]['type'];

if(!isset($UserCode)){
	echo "<script>alert('กรุณาเข้าสู่ระบบ!!!');</script>";
	echo '<META http-equiv="refresh" content="0;URL=login.php">';
	exit();
}

header('Content-Type: text/html; charset=utf-8');
include "include/inc_header.php"; 
include "include/inc_menu.php"; 
include('qrcode/qrcode.class.php');

if(isset($_GET["po"])){
 $getInstallmentPO = getInstallmentPO($_GET["po"]);
}
$replace = array("P","C");

// echo "<pre>".print_r($getInstallmentPO,1)."</pre>";
?>
<div class="main">
	<div class="main-content p20">
		<div class="p20">
				<div class="row">
					<div class="col-md-12">
						<div class="panel">
								<div class="panel-heading">
									<h4 class="panel-title"><b>QR Code</b></h4>
									<div>
										<div>ธนาคารที่รองรับ Qr Code </div>
										<img src="img/banklogo.jpg" alt="">
									</div>
								</div>
								<div class="panel-body fs14">
									<div class="row">
										<div class="col-md-1 t_r"><h4>PO :</h4></div>
										<div class="col-md-2">
											<input class="form-control formInput2" type="text" name="po_code_search" id="po_code_search"  value="<?php echo $_GET["po"] ?>" required>
										</div>
										<div class="col-md-2">
											<button type="button" id="btn_send_order" class="btn  btn-info"><i class="fa fa-search-plus" aria-hidden="true"></i> เช็คข้อมูล</button>
										</div>
										<!-- <div class="col-md-6 t_r"> <input type="text" id="search" placeholder="ค้นหา" class="form-control " style="width: 30%;float: right;"/></div> -->
									</div>
									<div class="row mt20">
										<div class="col-md-12">
											<div class="row">
												<div class="col-md-2"></div><div class="col-md-10"><?php echo $value["PO_ID"]; ?></div>
											</div>
											<form action="include/inc_action_chk.php" method="POST" id="frmMoveQC">
												<input type="hidden" name="action" value="addMoveQC">
												<input type="hidden" name="newqcCode" id="newqcCode" >
												<input type="hidden" name="cusID" id="cusID"  value="<?php echo str_replace($replace, "", $getInstallmentPO[0]["Customer_ID"])?>">
												<table class="table table-hover" id="tableMoveQC">
													<thead>
														<tr>
															<th class="t_c">รวม QR Code</th>
															<th class="t_c">งวดที่</th>
															<th data-field="noti_work_code" class="t_c">PO</th>
															<th data-field="pocode" data-sortable="true" class="t_c">ประเภทประกัน</th>
															<th class="t_c">กำหนดชำระ</th>
															<th class="t_r">เบี้ยสุทธิ</th>
															<th class="t_r">อากร</th>
															<th class="t_r">ภาษี</th>
															<th class="t_r">เบี้ยรวม</th>
															<th class="t_r">ส่วนลด</th>
															<th class="t_r">จ่ายสุทธิ</th>
															<th class="t_c">สถานะของงวดชำระ</th>
															<th class="t_c">QR Code</th>
															<!-- <th class="t_c" >หมายเหตุ</th> -->
														</tr>
													</thead>
													<tbody>
														<?php 
															$i=1;  
															foreach ($getInstallmentPO as $key => $value) {
																	if (!in_array($err, array('L', 'M', 'Q', 'H'))) $err = 'L';
																		$code_cp = trim($value["PO_ID"],"PO")."0".$value["Installment_ID"];
																		// $replace = array("P","C");
																		$code_customer = str_replace($replace, "", $value["Customer_ID"]);
																		// $code_customer = trim($value["Customer_ID"],"P");
																		//$price = number_format($value["ISTM_Total_Amount"], 2, '', '');
																		$value["ISTM_Total_Amount"]=1;
																		$price = number_format($value["ISTM_Total_Amount"], 2, '', '');
																		$title = $value["PO_ID"]." 0".$value["Installment_ID"]." ".$code_customer." ".$value["ISTM_Total_Amount"]; 
																		 $code = "|024554000009700".chr(10)."".$code_cp."".chr(10)."".$code_customer."".chr(10).str_replace(".", "", $price);
																		$url = "qrcode/image.php?msg=".urlencode($code)."&amp;err=".urlencode($err)."&amp;title=".urlencode($title);
															?>
																<tr >
																	<td class="t_c">
																		<?php if($value["Installment_Status"] != "Paid" && $value["ISTM_Total_Amount"] > 0){ ?>
																			<input type="checkbox" class="qrPrice" id="qrPrice_<?php echo $i ?>" name="qrPrice" value="<?php echo number_format($value["ISTM_Total_Amount"], 2, '.', '') ?>">
																			<input type="hidden" id="txt_<?php echo $i ?>" value="<?php echo substr($value["PO_ID"], -6) ?>" name="txtPO">
																			<input type="hidden" id="insid_<?php echo $i ?>" value="<?php echo "0".$value["Installment_ID"] ?>" name="insid">
																		<?php $i++;  } ?>
																	</td>
																	<td class="wpno t_c" style="width: 5%;"><?php echo $value["Installment_ID"]; ?></td>
																	<td class="wpno t_c" style="width: 10%;">
																		<?php if($value["Installment_Status"] != "Paid" && $value["ISTM_Total_Amount"] > 0){ ?>
																			<a class="cff2da5" href="export_pay.php?pocode=<?php echo $value["PO_ID"]; ?>&insID=<?php echo $value["Installment_ID"]; ?>" target="_blank"><?php echo $value["PO_ID"]; ?></a>
																		<?php }else{ ?>
																			<?php echo $value["PO_ID"]; ?>
																		<?php } ?>
																	</td>
																	<td class="t_c "><?php echo $value["Insurance_Name"]; ?></td>
																	<td class="t_c "><?php echo $value["Installment_Due_Date"]->format("d/m/Y"); ?></td>
																	<td class=" t_r"> <b><?php echo number_format($value["ISTM_Net_Premium"],2); ?></b></td>
																	<td class=" t_r"> <b><?php echo number_format($value["ISTM_Duty"],2); ?></b></td>
																	<td class=" t_r"> <b><?php echo number_format($value["ISTM_Tax"],2); ?></b></td>
																	<td class=" t_r"> <b><?php echo number_format($value["ISTM_Total_Premium"],2); ?></b></td>
																	<td class=" t_r"> <b><?php echo number_format($value["ISTM_Discount"],2); ?></b></td>
																	<td class=" t_r"> <b><?php echo number_format($value["ISTM_Total_Amount"],2); ?></b></td>
																	<td class=" t_c"> <b><?php echo $value["Installment_Status"]; ?></b></td>
																	<td class=" t_c">
																		<?php if($value["Installment_Status"] != "Paid" && $value["ISTM_Total_Amount"] > 0){ ?>
																		<div style="padding:10 px;">
																			<a href="<?php echo $url ?>" data-fancybox="QR--**--Code" class="fancybox">
																				<img src="<?php echo $url ?>" alt="generation qr-code" style="width: 100px">
																			</a>
																			<p><a download="<?php echo $value["PO_ID"]."-0".$value["Installment_ID"].".png" ?>" href="<?php echo $url ?>" title="<?php echo $title ?>" class="fs12" style="color: #ff2da5;"><?php echo $code; ?></a></p>
																		</div>
																		<?php } ?>
																	</td>	
																</tr>
														<?php  } ?>
																<tr class="dn boxQRALL">
																	<td class="wpno t_r" colspan="12">รวม</td>
																	<td class="wpno t_c" ><div class="showQRAll"></div></td>
																</tr>
													</tbody>
												</table>
											</form>
										</div>
									</div>
								</div>
						</div>
					</div>
				</div>
		</div>
	</div>
</div>
<?php include "include/inc_footer.php"; ?> 
<script src="fancybox/dist/jquery.fancybox.min.js"></script>
<script type="text/javascript">
	$("#btn_send_order").click(function() {
    	var poCode = $("#po_code_search").val();
    	if(poCode){
    		window.location.href = "listpayment.php?po="+poCode;
    	}else{
    		alert("กรุณากรอกข้อมูล PO ");
    	}

  });
  
$(".qrPrice").click(function() {
		var price = 0.00;
		var poCode = $("#po_code_search").val();
		qrPrice = $(this).val();
		qrPrice_id = $(this).attr('id').replace("qrPrice_", "");
		txt = $("#txt_"+qrPrice_id).val();
		cusid = $("#cusID").val();
		var arrPO = [];
		title = "";
		ins_id = "";
		i = 1;
		$.each($('input[name="qrPrice"]'), function() { 

			if($('#qrPrice_'+i).is(':checked')){
				qrPrice = $('#qrPrice_'+i).val();
				txtPO = $('#txt_'+i).val();
				insid = $('#insid_'+i).val();
				price = parseFloat(price) + parseFloat(qrPrice);
				if(arrPO.indexOf(txtPO) == -1){
					arrPO.push(txtPO);
					title += txtPO;
				}
				ins_id += insid;
			}
        i++;        
	  });
	 // console.log("title "+title); 
		msg = "|024554000009700%0A"+title+"%0A"+cusid+"%0A"+price+"00";
		name = "|024554000009700 "+title+" "+cusid+" "+price+"00";
		titles = title+" "+ins_id+" "+cusid+" "+price;
		$(".boxQRALL").show();
		$(".showQRAll").html('<a href="qrcode/image.php?msg='+msg+'&amp;err=L&amp;title='+titles+'" data-fancybox="QRCode" class="fancybox"><img src="qrcode/image.php?msg='+msg+'&amp;err=L&amp;title='+titles+'" alt="generation qr-code" style="width: 100px"></a><p><a download="'+poCode+''+cusid+'.png" href="qrcode/image.php?msg='+msg+'&amp;err=L&amp;title='+titles+'" title="" class="fs12" style="color: #ff2da5;">'+name+'</a></p>');
			
});


$( '[data-fancybox="watermark"]' ).fancybox({
  buttons : [
    'download',
    'zoom',
    'close'
  ],
  protect : false,
  animationEffect : "zoom-in-out",
  transitionEffect : "fade",
  zoomOpacity : "auto",
  animationDuration : 500,
  zoomType: 'innerzoom',

});
</script>