<?php 

session_start();
if(!isset($_SESSION["User"]['UserCode'])){
	echo "<script>alert('กรุณาเข้าสู่ระบบ!!!');</script>";
	echo '<META http-equiv="refresh" content="0;URL=login.php">';
	exit();
} 
// if($_SESSION["User"]['type'] == "CallCenter"){
// 	echo '<META http-equiv="refresh" content="0;URL=chkinser.php">';
// 	exit();
// }


header('Content-Type: text/html; charset=utf-8');
include "include/inc_header.php"; 
include "include/inc_menu.php"; 
$UserCode = $_SESSION["User"]['UserCode'];

if($_SESSION["User"]['type'] == "QualityControl"){
	$send_orders = getAllSendPO($UserCode, "QualityControl");

} else if($_SESSION["User"]['type'] == "Sale" || $_SESSION["User"]['type'] == "CallCenter"){
	$send_orders = getAllSendPO($UserCode, "");
} else {
	$send_orders = getAllSendPO($UserCode);
}
// echo "<pre>".print_r($send_orders,1)."</pre>";
$companyIns = getCompanyBysrv();
$getPremium = getPremium();
$getInsuranceType = getInsuranceType();

?> 
<div class="main">
	<div class="main-content p20">
		<div class=" bgff"> 
			<div class="p20">
				<div>
					<h3 >รายการแจ้งงาน Team (<?php echo $_SESSION["User"]['UserCode']; ?>)</h3>
					<?php //if($_SESSION["User"]['type'] == "QualityControl"){ ?>
						<!-- <span><a href="#" title="แจ้งงาน" class="btn btn-danger " id="btn_open_send_order"><i class="fa fa-paper-plane-o" aria-hidden="true"> แจ้งงาน</i></a></span>-->
						<div class="mt30 dn" id="fromSendWork">
							<div class="row">
								<div class="col-md-6">
									<div class="col-md-6">
										<div class="form-group">
											<input class="form-control formInput2" type="text" name="po_code_search" id="po_code_search"  required>
										</div>
									</div>
									<div class="col-md-6" class="text-c">
										<div class="form-group" class="text-c">
											<button type="button" id="btn_send_order" class="btn  btn-info"><i class="fa fa-search-plus" aria-hidden="true"></i> เช็คข้อมูล</button>
										</div>
									</div>
								</div>
							</div>
							<div style="border-top: 1px solid #5d5d5d;border-bottom: 3px solid #5d5d5d;" class="pb15 mt20">
								<form action="include/inc_action_chk.php" method="POST" id="form_send_order" enctype="multipart/form-data">
									<input type="hidden" value="sendOrderAdmin" name="action" id="action"> 
									<input type="hidden" value="<?php echo $_SESSION["User"]['UserCode'] ?>" name="personnel_code" id="personnel_code"> 
									<input type="hidden" value="<?php echo $_SESSION["User"]['firstname']." ".$_SESSION["User"]['lastname'] ?>" name="personnel_name" id="personnel_name">
									<h4 class="text-center cff8000">ข้อมูลการแจ้งงาน</h4>
									<div class="row" style="background-color: #ebfeff;">
										<div class="col-md-6" >
											<div class="row mt15">
												<div class="col-md-6">
													<div class="form-group">
														<label for="po_code">รหัส PO  <b>*</b></label>
														<input class="form-control formInput2" type="text" name="po_code" id="po_code"  required>
													</div>
												</div>
												<div class="col-md-3 boxmarine dn">
													<div class="form-group">
														<input class="form-control formInput2" type="hidden" name="marine" id="marine"  required>
														<input type="checkbox" id="caseMarine" name="caseMarine" value="1" > แจ้งงาน PO. คู่กัน 
														<div class="text-center cff8000 fwb" id="marinetxt"></div>
													</div>
												</div>
												<div class="col-md-3">
													<div class="form-group mt5">
														<label for="po_type">ประเภทประกัน <b>*</b></label>
														<div class="mt5 ml10">
								              <input type="radio" name="po_type" id="po_type_p" value="ส่วนบุคคล"> ส่วนบุคคล | 
															<input class="ml10" type="radio" name="po_type" id="po_type_c" value="บริษัท" > บริษัท
														</div>
													</div>
												</div>	
											</div>
											<div class="row">
												<div class="col-md-6">
													<div class="form-group">
														<label for="cus_name">ชื่อลูกค้า <b>*</b></label>
														<input class="form-control formInput2" type="text" name="cus_name" id="cus_name"  required>
													</div>
												</div>
												<div class="col-md-6">
													<div class="form-group">
														<label for="start_cover_date">วันเริ่มคุ้มครอง <b>*</b></label><br>
								              <input type="date" name="start_cover_date" id="start_cover_date" class="form-control formInput2"  required>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-md-6">
													<div class="form-group">
														<label for="cus_name">แพจเกต <b>*</b></label>
														<input class="form-control formInput2" type="hidden" name="package_id" id="packageID"  >
														<input class="form-control formInput2" type="text" name="package_name" id="packageName"  required>
													</div>
												</div>
												<div class="col-md-6">
													<div class="form-group">
														<label for="start_cover_date">ประกันชั้น</label><br>
								              <select class="form-control formInput2 fs12" name="insurance_type" id="InsuranceType" required>
																<option value="0">กรุณาเลือก</option>
																<?php foreach ($getInsuranceType as $key => $value) { ?>
																	<option value="<?php echo  $value["Insurance_Name"]; ?>"><?php echo  $value["Insurance_Name"]; ?></option>
																<?php } ?>
															</select>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-md-6">
													<div class="form-group">
														<label for="insuere_company">บริษัทประกัน <b>*</b></label>
														<select id="insuere_company" class="form-control formInput2 formInput" name="insuere_company" required>
								              <option value="">กรุณาเลือก</option>
								              <?php foreach ($companyIns as $compa) { ?>
								              	 <option value="<?php echo $compa["Insurer_Initials"];?>"><?php echo $compa["Insurer_Name"];?></option>
								              <?php } ?>
									          </select>
													</div>
												</div>
												<div class="col-md-6">
													<div class="form-group">
														<label for="insuere_cost">ทุนประกัน <b>*</b></label>
														<input class="form-control formInput2" type="text" name="insuere_cost" id="insuere_cost"  required>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-md-6" >
														<label for="taxamount">ของแถม</label>
														<select class="form-control formInput2 fs12" name="giftvoucher" id="giftvoucher" >
															<option value="0">กรุณาเลือก</option>
															<?php foreach ($getPremium as $key => $value) { ?>
																<option value="<?php echo  $value["Premium_Desc"]; ?>"><?php echo  $value["Premium_Desc"]; ?></option>
															<?php } ?>
														</select>
												</div>
												<div class="col-md-6">
													<div class="form-group mt20">
														<label for="insuere_cost"></label>
														<input type="checkbox" id="discount_cctv" name="discount_cctv" value="1"> ส่วนลดกล้อง CCTV
													</div>
												</div>
											</div>
											<div class="row mt10">
												<div class="col-md-3">
													<div class="form-group">
														<label for="netpremium">เบี้ยสุทธิ </label><br>
								              <input type="text" name="netpremium" id="netpremium" class="form-control formInput2" >
													</div>
												</div>
												<div class="col-md-3">
													<div class="form-group">
														<label for="premium">เบี้ยรวมภาษี </label>
														<input class="form-control formInput2" type="text" name="premium" id="premium"  >
													</div>
												</div>
												<div class="col-md-3">
													<div class="form-group">
														<label for="discount">ส่วนลด </label><br>
								              <input type="text" name="discount" id="discount" class="form-control formInput2" >
													</div>
												</div>
												<div class="col-md-3">
													<div class="form-group">
														<label for="taxamount">รวม <b>*</b></label>
														<input class="form-control formInput2" type="text" name="taxamount" id="taxamount"  required>
													</div>
												</div>
											</div>
										</div>
										<div class="col-md-6" style="background-color: #fafafa;padding-bottom: 15px;">
											<div class="row imgCarBox dn mt20">
												<div class="col-md-3">
													<span class="fl fs12">รูปรถด้านซ้าย</span><span>
														<input type="file" name="filUpload[carleft]" id="fileUp_1" class="fl form-control inputFile" onchange="ValidateSingleInput(this);" accept="image/gif, image/jpeg"></span>
												</div>
												<div class="col-md-3">
													<span class="fl fs12">รูปรถด้านขวา</span><span>
														<input type="file" name="filUpload[carright]" id="fileUp_2" class="fl form-control inputFile" onchange="ValidateSingleInput(this);" accept="image/gif, image/jpeg"></span>
												</div>
											
												<div class="col-md-3">
													<span class="fl fs12">รูปรถด้านหน้า</span><span>
														<input type="file" name="filUpload[carfront]" id="fileUp_3" class="fl form-control inputFile" onchange="ValidateSingleInput(this);" accept="image/gif, image/jpeg"></span>
												</div>
												<div class="col-md-3">
													<span class="fl fs12">รูปรถด้านท้าย</span><span>
														<input type="file" name="filUpload[carback]" id="fileUp_4" class="fl form-control inputFile" onchange="ValidateSingleInput(this);" accept="image/gif, image/jpeg"></span>
												</div>
											</div>
											<div class="row mt15">
												<div class="col-md-3">
													<span class="fl fs12">สำเนารถ</span><span>
														<input type="file" name="filUpload[carbook]" id="fileUp_5" class="fl form-control inputFile" onchange="ValidateSingleInput(this);" accept="image/gif, image/jpeg , .pdf" ></span>
												</div>
												<div class="col-md-3">
													<span class="fl fs12">ใบขับบี่</span><span>
														<input type="file" name="filUpload[cardriver]" id="fileUp_6" class="fl form-control inputFile" onchange="ValidateSingleInput(this);" accept="image/gif, image/jpeg , .pdf"></span>
												</div>
												<div class="col-md-3 filecompanyBox dn">
													<span class="fl fs12">หนังสือรับรองบริษัท</span><span>
														<input type="file" name="filUpload[company]" id="fileUp_7" class="fl form-control inputFile" onchange="ValidateSingleInput(this);" accept="image/gif, image/jpeg , .pdf"></span>
												</div>
												<div class="col-md-3 filecompanyBox dn">
													<span class="fl fs12">บัตร ปชช. กรรมการ</span><span>
														<input type="file" name="filUpload[companycard]" id="fileUp_8" class="fl form-control inputFile" onchange="ValidateSingleInput(this);" accept="image/gif, image/jpeg , .pdf"></span>
												</div>
											</div>
											<div class="row mt15">
												<div class="col-md-3">
													<span class="fl fs12">เอกสารอื่นๆ 1</span><span>
														<input type="file" name="filUpload[carall1]" id="fileUp_9" class="fl form-control inputFile" onchange="ValidateSingleInput(this);" accept="image/gif, image/jpeg , .pdf"></span>
												</div>
												<div class="col-md-3">
													<span class="fl fs12">เอกสารอื่นๆ 2</span><span>
														<input type="file" name="filUpload[carall2]" id="fileUp_10" class="fl form-control inputFile" onchange="ValidateSingleInput(this);" accept="image/gif, image/jpeg , .pdf"></span>
												</div>
												<div class="col-md-3">
													<span class="fl fs12">เอกสารอื่นๆ 3</span><span>
														<input type="file" name="filUpload[carall3]" id="fileUp_11" class="fl form-control inputFile" onchange="ValidateSingleInput(this);" accept="image/gif, image/jpeg , .pdf"></span>
												</div>
												<div class="col-md-3">
													<span class="fl fs12">เอกสารอื่นๆ 4</span><span>
														<input type="file" name="filUpload[carall4]" id="fileUp_12" class="fl form-control inputFile" onchange="ValidateSingleInput(this);" accept="image/gif, image/jpeg , .pdf"></span>
												</div>
											</div>
											<div class="row mt15">
												<div class="col-md-3">
													<span class="fl fs12">หลักฐานประกอบตรวจ 1</span><span>
														<input type="file" name="filUpload[reCheck1]" id="fileUp_13" class="fl form-control inputFile" onchange="ValidateSingleInput(this);" accept="image/gif, image/jpeg , .pdf" ></span>
												</div>
												<div class="col-md-3">
													<span class="fl fs12">หลักฐานประกอบตรวจ 2</span><span>
														<input type="file" name="filUpload[reCheck2]" id="fileUp_14" class="fl form-control inputFile" onchange="ValidateSingleInput(this);" accept="image/gif, image/jpeg , .pdf"></span>
												</div>
												<div class="col-md-3">
													<span class="fl fs12">หลักฐานประกอบตรวจ 3</span><span>
														<input type="file" name="filUpload[reCheck3]" id="fileUp_15" class="fl form-control inputFile" onchange="ValidateSingleInput(this);" accept="image/gif, image/jpeg , .pdf" ></span>
												</div>
												<div class="col-md-3">
													<span class="fl fs12">หลักฐานประกอบตรวจ 4</span><span>
														<input type="file" name="filUpload[reCheck4]" id="fileUp_16" class="fl form-control inputFile" onchange="ValidateSingleInput(this);" accept="image/gif, image/jpeg , .pdf"></span>
												</div>
											</div>
											<p class="fs12">*** ไฟล์นามสกุล PNG | JPG | PDF  เท่านั้น *** </p>
											<div class="row mt15">
												<div class="col-md-12" >
														<label for="taxamount">การจัดส่งเอกสาร *</label>
														<select id="send_type" class="form-control formInput2 formInput" name="send_type" onchange="fnSendType();" required>
								              <option value="">กรุณาเลือก</option>
								              <option value="ลูกค้ารับเอง">ลูกค้ารับเอง</option>
								              <option value="พนักงานจัดส่ง">พนักงานจัดส่ง</option>
								              <option value="ลงทะเบียน">ลงทะเบียน</option>
								              <option value="EMS">EMS</option>
									          </select>
												</div>
												<div class="col-md-12 mt15 dn" id="addressSend">
													<label for="taxamount">ที่อยู่ส่งเอกสาร </label>
													<input type="text" name="send_addr" id="send_addr" class="form-control formInput2 " >
												</div>
											</div>
											<div class="row mt15">
												<div class="col-md-12" >
														<label for="taxamount">หมายเหตุ</label>
														<textarea name="notes" id="notes" style="width: 100%; font-size: 12px; height: 100px;" ></textarea>
												</div>
											</div>
										</div>
									</div>
									<div class="row pt20">						
										<div class="t_c clearfix "><button type="button" class="btn btn-success btnOrders"><i class="fa fa-check-circle"></i> ส่งงาน</button></div>
									</div>
								</form>
							</div>
						</div>
					<?php// } ?>
				</div>
					<table id="table" class="table table-striped p5" data-toggle="table" data-pagination="true" data-search="true"  data-page-size="100" data-page-list="[100, 150, 200, 250, 300]" data-height="750"  data-width="750"  <?php if($_SESSION["User"]['type'] != "QualityControl"){ ?> data-detail-view="true" <?php } ?>>
						<thead>
							<tr>
								<th class="t_c"><i class="fa fa-list-ul" aria-hidden="true"></i></th>
								<th data-field="statusType" class="t_c" data-sortable="true">สถานะ</th>
								<!-- <th class="t_c">ใช้งาน</th> -->
								<th data-field="noti_work_code" class="t_c dn"></th>
								<th data-field="pocode" data-sortable="true" class="t_c">PO</th>
								<th class="t_c">ชื่อ-นามสกุล</th>
								<th class="t_c">แพจเกต</th>
								<th data-field="start_cover_date" data-sortable="true" class="t_c">วันคุ้มครอง</th>
								<th class="t_c">ทุนประกัน</th>
								<th class="t_c">ชำระ</th>
								<th class="t_c">ผู้ขาย</th> 
								<th data-field="created_date" data-sortable="true" class="t_c">วันที่ทำรายการ</th>
								<th class="t_c">ผู้ตรวจ</th>
								<!-- <th class="t_c" >หมายเหตุ</th> -->
							</tr>
						</thead>
						<tbody class="fs12">
							<?php 
								//if($apiMain){
									foreach ($send_orders as $key => $value) {  
										if($value["start_cover_date"] == date("Y-m-d")){
											$bgColor = "class='bgbaf4bc'";
										}else if($value["status"] == "1"){
											$bgColor = "class='bgffeee8'";
										}else{
											$bgColor = "";
										}

										if($value["enable"] == "0"){
											$enable = "<span style='color: #f44;'>ยกเลิก</span>";
										}else{
											$enable = "<span style='color: #009710;'>เปิดใช้</span>";
										}
							?>
								<tr <?php echo $bgColor; ?> >
									<td class="wpno t_c"> 
										<?php if($_SESSION["User"]['type'] == "Sale" || $_SESSION["User"]['type'] == "CallCenter"){ ?>
											<?php if($value["status"] == 1 ){ ?>
												<a href="sendordersviews.php?pocode=<?php echo $value["po_code"];  ?>&numcode=<?php echo $value["noti_work_code"];  ?>" style="color: #003cff;">
													<i class="fa fa-cog"></i></a> | 
												<span class="fs14 btnselecttitle" style="color: #ff0000;" onclick="changEnable('<?php echo $value["noti_work_id"]?>','<?php echo $value["noti_work_code"]; ?>',0);"><i class="fa fa-trash"></i></span> 
											<?php }else{ ?>
												<a href="sendordersviews.php?pocode=<?php echo $value["po_code"];  ?>&numcode=<?php echo $value["noti_work_code"];  ?>" style="color: #003cff;"><i class="fa fa-search-plus" aria-hidden="true"></i></a>
												<?php if($value["status"] == 0 ){ ?>
													| <span class="fs14 btnselecttitle" style="color: #ff0000;" onclick="changEnable('<?php echo $value["noti_work_id"]?>','<?php echo $value["noti_work_code"]; ?>',0);"><i class="fa fa-trash"></i></span>
												<?php } ?>

											<?php } ?>

										<?php }else if($_SESSION["User"]['type'] == "QualityControl"){ ?>
												<a href="sendorderslists.php?pocode=<?php echo $value["po_code"];  ?>&numcode=<?php echo $value["noti_work_code"];  ?>"><i class="fa fa-list-ul" aria-hidden="true"></i></a>
											<?php if($UserCode == $value["personnel_code"]){ ?>
													| <a href="sendordersviews.php?pocode=<?php echo $value["po_code"];  ?>&numcode=<?php echo $value["noti_work_code"];  ?>" style="color: #003cff;"><i class="fa fa-search-plus" aria-hidden="true"></i></a>
													| <span class="fs14 btnselecttitle" style="color: #ff0000;" onclick="changEnable('<?php echo $value["noti_work_id"]?>','<?php echo $value["noti_work_code"]; ?>',0);"><i class="fa fa-trash"></i></span> 
											<?php } ?>

										<?php }else{ ?>
												<a href="sendordersviews.php?pocode=<?php echo $value["po_code"];  ?>&numcode=<?php echo $value["noti_work_code"];  ?>" style="color: #003cff;"><i class="fa fa-search-plus" aria-hidden="true"></i></a> | 
												<a href="sendorderslists.php?pocode=<?php echo $value["po_code"];  ?>&numcode=<?php echo $value["noti_work_code"];  ?>" style="color: #ac056c;"><i class="fa fa-list-ul" aria-hidden="true"></i></a>
										<?php } ?>
									</td>
									<td class="wpno t_c"><?php echo setStatus($value["status"]); ?></td>
									<td class="t_l dn"><?php echo $value["noti_work_code"]; ?></td>
									<td class=" t_l"> 
										<b><?php echo $value["po_code"]; ?></b>
										<div class="clearfix fs10"><?php echo $value["noti_work_code"]; ?></div>
									</td>
									<td class="t_l <?php echo $bgColor; ?>"><b><?php echo $value["cus_name"]; ?></b></td>
									<td class="t_l">
										<div class="clearfix fs10 c2457ff"><?php echo $value["insuere_company"]; ?></div>
										<div class="clearfix c9c00c8"><?php echo $value["package_name"]; ?></div>
										<div class="clearfix fs10 "><?php echo $value["insurance_type"]; ?></div>
									</td>
									<td><?php echo date("d-m-Y",strtotime($value["start_cover_date"])); ?></td>
									<td class="t_r"><?php echo number_format($value["insuere_cost"],2); ?></td>
									<td class="t_r">
										<div class="clearfix "><b>สุทธิ: </b><?php echo number_format($value["netpremium"],2); ?></div>
										<div class="clearfix "><b>รวมภาษี: </b><?php echo number_format($value["premium"],2); ?></div>
										<div class="clearfix "><b>จ่าย: </b><?php echo number_format($value["taxamount"],2); ?></div>
									</td>
									<td class="t_l wpno">
											<div class="clearfix c2457ff "><?php echo $value["personnel_name"]; ?></div>
											<div class="clearfix fs10 "><?php echo $value["send_type"] ? "<b>การส่ง :</b> ".$value["send_type"] : ""; ?></div>
									</td>
									<td ><?php echo $value["created_date"]; ?></td>
									<td class="t_l">
										<div class="clearfix "><b>ชื่อ: </b><?php echo $value["chk_name"]." ".$value["chk_lname"]; ?></div>
										<div class="clearfix "><b>เวลา: </b><?php echo $value["datetime_chk"]; ?></div>
									</td>
									<span style="display: none;" id="desc<?php echo $key; ?>">
	                  <div class="row" id="dataBox<?php echo $key; ?>"></div>
	                </span>
								</tr>
							<?php }
							//} ?>
						</tbody>
					</table>
			</div>
		</div>
	</div>
</div>


<?php include "include/inc_footer.php"; ?> 
<!-- <script src="//code.jquery.com/jquery-3.2.1.min.js"></script> -->
<script src="fancybox/dist/jquery.fancybox.min.js"></script>
<script type="text/javascript">
	var $table = $('#table');
	$table.on('expand-row.bs.table', function (e, index, row, $detail) {
    var noti_id = row.noti_work_code; 
  	var res = $("#desc" + index).html(); 
  	$.ajax({ 
			url: 'include/inc_action_chk.php',
			type:'POST',
			data: {action:"getImageOrNote", noti_id:noti_id},
			success:function(rs){
				// console.log(rs);
				$detail.html(rs);
			}
		});
	});
function queryParams() {
    return {
        type: 'owner',
        sort: 'updated',
        direction: 'desc',
        per_page: 1,
        page: 1
    };
}


$(document).ready(function() {
		$("#btn_open_send_order").click(function() {
				$( "#fromSendWork" ).toggle("slow");
		});	

		$("input[name=po_type]").click(function() {
			name = $('input[name=po_type]:checked').val();
			if(name == "บริษัท"){
				$(".filecompanyBox").show();
			}else{
				$(".filecompanyBox").hide();
			}
		});


		$("#insuere_company").change(function() {
			var insuere = $("#insuere_company").val();
			frncmp = setimgCompany(insuere);
			if(frncmp == 1){
				$(".imgCarBox").show();
			}else{
				$(".imgCarBox").hide();
			}
		});

    $("#btn_send_order").click(function() {
    	var poCode = $("#po_code_search").val();
        $.ajax({ 
					url: 'include/inc_action_ms.php',
					type:'POST',
					dataType: 'json',
					data: {action: 'searchPO', pocode:poCode},
					success:function(rs){ console.log("btn_send_order"+rs);
							var num = rs.length;
							var details = "";
							for (var i = 0; i < num; i++){
						// console.log("btn_send_order"+rs[i].Premium_Desc);
								var d = new Date(rs[i].Coverage_Start_Date.date);
        // console.log("btn_send_order"+rs[i].Coverage_Start);
								$("#po_code").val(rs[i].PO_ID);
								$("#cus_name").val(rs[i].FName+" "+rs[i].LName);
								$("#insuere_cost").val(rs[i].Capital);
								initials = rs[i].Insurer_Initials;
								cusType = rs[i].cus_type;
								if(cusType == "C"){
									$("#po_type_c").prop("checked", true);
								}else{
									$("#po_type_p").prop("checked", true);
								}
								$("#packageID").val(rs[i].Insurance_Package_ID);
								$("#packageName").val(rs[i].Insurance_Package_Name);
								$("#insuere_company").val(initials);
								
								if(rs[i].Net_Premium){
									$("#netpremium").val(rs[i].Net_Premium);
								}else{
									$("#netpremium").val(rs[i].Compulsory);
								}
								$("#discount").val(rs[i].Discount);
								$("#premium").val(rs[i].Total_Premium);
								$("#taxamount").val(rs[i].Premium_After_Disc);
								$("#giftvoucher").val(rs[i].Premium_Desc);
								$("#InsuranceType").val(rs[i].Insurance_Name);

								if(rs[i].Marine_Reference){
									$(".boxmarine").show();
									$("#marine").val(rs[i].Marine_Reference);
									$("#marinetxt").html(rs[i].Marine_Reference);
									$('#caseMarine').prop('checked', true);
								}else{
									$(".boxmarine").hide();
									$("#marine").val("");
									$('#caseMarine').prop('checked', false);
								}
								var now = new Date();
								var day = ("0" + d.getDate()).slice(-2);
								var month = ("0" + (d.getMonth() + 1)).slice(-2);
								var today = d.getFullYear()+"-"+(month)+"-"+(day) ;
								$("#start_cover_date").val(today);
								
							}
								frncmp = setimgCompany(initials);
								if(frncmp == 1){
									$(".imgCarBox").show();
								}else{
									$(".imgCarBox").hide();
								}
								if(cusType == "C"){
									$(".filecompanyBox").show();
								}else{
									$(".filecompanyBox").hide();
								}
						}
				});
    });

    $(".btnOrders").click(function() {
    	if(!$("#po_code").val()){
    		alert("กรุณากรอก รหัส PO"); 
    		$( "#po_code" ).focus();
    	}else if(!$("#cus_name").val()){
    		alert("กรุณากรอก ชื่อลูกค้า"); 
    		$( "#cus_name" ).focus();
    	
    	}else if(!$("#start_cover_date").val()){
    		alert("กรุณากรอก วันเริ่มคุ้มครอง"); 
    		$( "#start_cover_date" ).focus();
    	
    	}else if(!$('input[name=po_type]:checked').val()){
    		alert("กรุณากรอก ประเภทประกัน"); 
    		$( "input[name=po_type]" ).focus();
    	
    	}else if(!$('#insuere_company').val()){
    		alert("กรุณากรอก บริษัทประกัน"); 
    		$( "#insuere_company" ).focus();
    	
    	}else if(!$('#insuere_cost').val()){
    		alert("กรุณากรอก ทุนประกัน"); 
    		$("#insuere_cost").focus();
    	
    	}else if(!$('#taxamount').val()){
    		alert("กรุณากรอก รวม"); 
    		$("#taxamount").focus();
    	
    	}else{
    		$("#form_send_order").submit();
    	}

		});
});

function fnSendType(){
	send = $("#send_type").val();
	if(send == "พนักงานจัดส่ง"){
		$("#addressSend").show();
	}else{
		$("#addressSend").hide();
	}
}
function changEnable(id, codes, status){
	// console.log(id);console.log(codes);console.log(status);
	if(confirm("ต้องการยกเลิกการแจ้งงานนี้?")){
		$.ajax({ 
			url: 'include/inc_action_chk.php',
			type:'POST',
			data: {action: 'delPONOti', id:id, code:codes, status:status},
			success:function(rs){
				window.location.reload(true);
			}
		});
	}
}

function ValidateSingleInput(oInput) {
var _validFile = [".jpg", ".jpeg", ".pdf",".png",];    
    if (oInput.type == "file") {
        var sFileName = oInput.value;
         if (sFileName.length > 0) {
            var blnValid = false;
            for (var j = 0; j < _validFile.length; j++) {
                var sCurExtension = _validFile[j];
                if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                    blnValid = true;
                    break;
                }
            }
             
            if (!blnValid) {
                alert("กรุณาเลือกไฟล์ ที่มีนามสกุล PNG | JPG | PDF เท่านั้น" );
                oInput.value = "";
                return false;
            }
        }
    }
    return true;
}

function isImage(filename) {
    var ext = getExtension(filename);
    switch (ext.toLowerCase()) {
	    case 'jpg':
	    case 'png':
	    case 'pdf':
	      return true;
    }
    return false;
}

function setimgCompany(cmp){
	InsurArr = ["เอเชีย", "อาคเนย์", "สินทรัพย์", "วิริยะ", "นวกิจ", "ไทยไพบูลย์", "ไทยประกัน", "กรุงไทย" , "กรุงเทพ", "KSK"];
	rs = InsurArr.indexOf(cmp);
	if(rs == -1){
		return 0;
	}else{
		return 1;
	}
}
function btnComments(e){
	var val = e.id;
	console.log(e);
	code = val.split("ments_");
	mentID = code[1];
	mentTxt = $("#comment_"+mentID).val();
	console.log("#listMent_"+mentTxt);
	if(mentTxt != 0){
	 $.ajax({ 
			url: 'include/inc_action_chk.php',
			type:'POST',
			data: {action: 'addComments', noti_work_code:mentID, comment:mentTxt},
			success:function(rs){
				// console.log("#listMent_"+mentID);
				$("#listMent_"+mentID).empty();
				$("#comment_"+mentID).val(0);
				$("#listMent_"+mentID).append(rs);
			}
		});
	}else{
		alert("กรุณาเลือก Comment");
	}
}

</script>