<?php 

session_start();
if(!isset($_SESSION["User"]['UserCode'])){
	echo "<script>alert('กรุณาเข้าสู่ระบบ!!!');</script>";
	echo '<META http-equiv="refresh" content="0;URL=login.php">';
	exit();
}

if(!$_GET["pocode"] && !$_GET["numcode"] && $_SESSION["User"]['type'] == "QualityControl" && $_SESSION["User"]['type'] == "CallCenter"){
	echo '<META http-equiv="refresh" content="0;URL=sendorders.php">';
	exit();
}
header('Content-Type: text/html; charset=utf-8');
include "include/inc_header.php"; 
include "include/inc_menu.php"; 

$pocode = $_GET["pocode"];
$numcode = $_GET["numcode"];
$getPO = getPurchaseBycode($pocode);
$Cardetail = getCardetailBycode($pocode);
$getCommentlist = getCommentlist();
$getNoti = getNotiWorkByID($numcode);
$getFile = getNotiWorkFilesByID($numcode);
$getCheck = getNotiWorkCheck($numcode, $pocode);
$datakeyword = unserialize($getCheck["keyword"]);
$datadata = unserialize($getCheck["data"]);
$datachecklists = unserialize($getCheck["checklists"]);
$datacomments = unserialize($getCheck["comments"]);

?> 
<div class="main">
	<div class="main-content p20">
		<form action="include/inc_action_chk.php" method="post" id="frmsendAddlistchk"> 
			<input type="hidden" name="main[action]" id="action" value="sendAddlistchk">
			<input type="hidden" name="main[noti_id]" id="action" value="<?php echo $getNoti["noti_work_id"]; ?>">
			<input type="hidden" name="main[pocode]" id="action" value="<?php echo $pocode; ?>">
			<input type="hidden" name="main[numcode]" id="action" value="<?php echo $numcode; ?>">
			<input type="hidden" name="main[insurer]" id="action" value="<?php echo $getNoti["insuere_company"]; ?>">
			<div class="p20">
				<div class="row">
					<div class="col-md-12">
						<div class="panel">
								<div class="panel-heading">
									<h4 class="panel-title"><b>ข้อมูลแจ้งงาน</b></h4>
									<span class="fs12"><b>รหัส : </b><?php echo $getNoti["noti_work_code"];?></span>
								</div>
								<div class="panel-body fs14">
									<div class="col-lg-12">
										<div class="row">
											<div class="col-md-4">
												<div class="row">
													<div class="col-lg-4 t_r"><b>PO :</b></div>
													<div class="col-lg-4"><?php echo $getNoti["po_code"];?></div>
												</div>
												<div class="row">
													<div class="col-lg-4 t_r"><b>ประเภทประกัน :</b></div>
													<div class="col-lg-4"><?php echo $getNoti["po_type"];?></div>
												</div>
												<div class="row">
													<div class="col-lg-4 t_r"><b>ชื่อลูกค้า :</b></div>
													<div class="col-lg-4"><?php echo $getNoti["cus_name"];?></div>
												</div>
												<div class="row">
													<div class="col-lg-4 t_r"><b>วันเริ่มคุ้มครอง :</b></div>
													<div class="col-lg-4"><?php echo $getNoti["start_cover_date"];?></div>
												</div>
												<div class="row">
													<div class="col-lg-4 t_r"><b>บริษัทประกัน :</b></div>
													<div class="col-lg-4"><?php echo $getNoti["insuere_company"];?></div>
												</div>
												<div class="row">
													<div class="col-lg-4 t_r"><b>ส่วนลดก้อง :</b></div>
													<div class="col-lg-4"><?php echo ($getNoti["discount_cctv"] == 1) ? "มี" : "ไม่มี";?></div>
												</div>
												<div class="row">
													<div class="col-lg-4 t_r"><b>ของแถม :</b></div>
													<div class="col-lg-4"><?php echo ($getNoti["giftvoucher"]) ? $getNoti["giftvoucher"] : "-";?></div>
												</div>
												<div class="row">
													<div class="col-lg-4 t_r"><b>หมายเหตุ :</b></div>
													<div class="col-lg-4"><?php echo ($getNoti["notes"]) ? $getNoti["notes"] : "-";?></div>
												</div>
												
											</div>
											<div class="col-md-4">
												<div class="row">
													<div class="col-lg-4 t_r"><b>ทุนประกัน :</b></div>
													<div class="col-lg-4 t_r"><?php echo number_format($getNoti["insuere_cost"],2);?> บาท </div>
												</div>
												<div class="row">
													<div class="col-lg-4 t_r"><b>เบี้ยสุทธิ :</b></div>
													<div class="col-lg-4 t_r"><?php echo number_format($getNoti["netpremium"],2);?> บาท </div>
												</div>
												<div class="row">
													<div class="col-lg-4 t_r"><b>เบี้ยรวมภาษี :</b></div>
													<div class="col-lg-4 t_r"><?php echo number_format($getNoti["premium"],2);?> บาท </div>
												</div>
												<div class="row">
													<div class="col-lg-4 t_r"><b>ส่วนลด :</b></div>
													<div class="col-lg-4 t_r"><?php echo number_format($getNoti["discount"],2);?> บาท </div>
												</div>
												<div class="row">
													<div class="col-lg-4 t_r"><b>รวม :</b></div>
													<div class="col-lg-4 t_r"><?php echo number_format($getNoti["taxamount"],2);?> บาท </div>
												</div>
											</div>
											<div class="col-lg-4">
												<div class="row">
													<div class="col-lg-4 t_r"><b>ผู้แจ้งงาน :</b></div>
													<div class="col-lg-4"><?php echo $getNoti["personnel_code"]." ". $getNoti["personnel_name"];?></div>
												</div>
												<div class="row">
													<div class="col-lg-4 t_r"><b>สถานะ :</b></div>
													<div class="col-lg-4"><?php echo setStatus($getNoti["status"]);?></div>
												</div>
												<div class="row">
													<div class="col-lg-4 t_r"><b>การใช้งาน :</b></div>
													<div class="col-lg-4"><?php echo ($getNoti["enable"] == 1) ? "ใช้งาน" : "ปิดใช้งาน" ; ?></div>
												</div>
											</div>
										</div>
									</div>
								</div>
						</div>
					</div>
					
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="panel">
							<div class="panel-heading">
								<h4 class="panel-title"><b>ข้อมูลลูกค้า</b></h4>
								<div class="right">
									<button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
								</div>
							</div>
							<div class="panel-body fs12">
								<div class="row">
									<table class="table table-hover table-striped table-bordered">
										<thead>
											<tr>
												<th class="t_r" style="width: 20%;">รายการ</th>
												<th>ข้อมูล</th>
												<th class="t_c" style="width: 15%;">ตรวจข้อมูล</th>
												<th class="t_c" >หมายเหตุ</th>
											</tr>
										</thead>
										<tbody>
											<?php 
												$dataDetail = "";
												foreach ($datakeyword as $key => $value) {
													$file = strpos($value,"file");
													if($file !== false){
															if (strpos($datadata[$key], "pdf") == true) {
															  $dataDetail  = '<a href="myfile/'.$datadata[$key].'" data-fancybox="watermark" class="fancybox">'.$datadata[$key].'</a>';
															} else if(strpos($datadata[$key], "png")) {
															  $dataDetail  = '<a href="myfile/'.$datadata[$key].'" data-fancybox="watermark" class="fancybox"><img src="myfile/'.$datadata[$key].'" alt="" class="img-responsive rounded" style="width: 150px;"></a>';
															} else if(strpos($datadata[$key], "jpg")) {
																$dataDetail  = '<a href="myfile/'.$datadata[$key].'" data-fancybox="watermark" class="fancybox"><img src="myfile/'.$datadata[$key].'" alt="" class="img-responsive rounded" style="width: 150px;"></a>';
															}
													}else{
														$dataDetail = $datadata[$key];
													}
											?>
											<tr>
												<td class="t_r"><b><?php echo caseTitle($value);?></b></td>
												<td class="c1641ff"><?php echo $dataDetail ? $dataDetail :"-";?></td>
												<td class="t_c"><?php echo ($datachecklists[$key]) ? "ผ่าน" : "ไม่ผ่าน";?> </td>
												<td class="t_c"><?php echo $datacomments[$key] ? $datacomments[$key] : "-"; ?></td>
											</tr>
											<?php } ?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
<?php include "include/inc_footer.php"; ?> 
<script src="fancybox/dist/jquery.fancybox.min.js"></script>
<style type="text/css">
.panel .table > thead > tr > th:last-child,
.panel .table > tbody > tr > td:last-child{
	padding-left: 0px
}
</style>
<script type="text/javascript">
	$(".rd_chk").click(function() {
		val = $('.rd_chk:checked').val();
		name = $(this).attr("name");
		repName = name.replace("chk_", "comment_");
		if(val == "0"){
				$("#comment_"+repName).show(); 
			}else{
				$("#comment_"+repName).hide();
			}
			
	});
	function funChk(name) {
		// console.log("input[name='check["+name+"][chk]']:checked");
		val = $("input[name='check["+name+"][chk]']:checked").val(); 
		console.log(val);
		if(val == 0){
				$("#comment_"+name).show();
			}else{
				$("#comment_"+name).hide();
			}

	}
$(document).ready(function() {
	$(".btnSendAdd").click(function() { 
		status = $("#mainStatus").val();
		if(status != 0){
			if( confirm("ตรวจสอบข้อมูลสมบูรณ์ ต้องการบันทึกผลการประเมิน !!!") ){
				// console.log("btnSendAdd");
				$("#frmsendAddlistchk").submit();
			}
		}else{
			alert("กรุณาเลือกสถานะ!!!");
		}
		
	});
});
</script>