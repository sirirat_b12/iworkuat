<?php



function getPOData($txtSe){
	global $connMS;
	$resultarray = array();
  $sql = "SELECT  Purchase_Order.PO_ID, Purchase_Order.PO_Date, Purchase_Order.Operation_Type_ID, Purchase_Order.Insurance_ID, Purchase_Order.Customer_ID, Purchase_Order.Agent_ID, Purchase_Order.Employee_ID, Purchase_Order.Status_ID, Policy_Delivery.Policy_No,  Customer.Customer_ID, Customer.Customer_FName, Customer.Customer_LName, Customer.EMail, Customer.Tel_No, Customer.Mobile_No, Car_Detail.Plate_No, Car_Make.Make_Name_TH, Asia_Car_Model.Model_Desc, Province.Province_Name_TH ,Delivery_Status.Delivery_Status_Desc, Status.Status_ID, Status.Status_Desc
		FROM Purchase_Order 
		LEFT JOIN Status ON Purchase_Order.Status_ID = Status.Status_ID
		LEFT JOIN Policy_Delivery ON Purchase_Order.PO_ID = Policy_Delivery.PO_ID
		LEFT JOIN Delivery_Status ON Policy_Delivery.Delivery_Status_ID = Delivery_Status.Delivery_Status_ID
		LEFT JOIN Customer ON Purchase_Order.Customer_ID = Customer.Customer_ID
		LEFT JOIN Car_Detail ON Car_Detail.PO_ID = Purchase_Order.PO_ID
		LEFT JOIN Car_Make ON Car_Detail.Make_ID = Car_Make.Make_ID
  	LEFT JOIN Asia_Car_Model ON Car_Detail.Model = Asia_Car_Model.Model_ID 
  	LEFT JOIN Province ON Car_Detail.Plate_Province = Province.Province_ID
		WHERE Purchase_Order.PO_ID = '".$txtSe."'" ;

    $stmt = sqlsrv_query( $connMS, $sql );
	  if(sqlsrv_has_rows($stmt)) {
	    $row = sqlsrv_fetch_array($stmt);
	    return $row;
			exit();
		}
}



function getPurchaseOrderCancelRow($poid){
	global $connMS;
	$resultarray = array();
  $sql = "SELECT * FROM Purchase_Order_Cancel WHERE Purchase_Order_Cancel.PO_ID = '".$poid."' " ;
	$stmt = sqlsrv_query( $connMS, $sql );
 	if(sqlsrv_has_rows($stmt)) {
    $row = sqlsrv_fetch_array($stmt);
    return $row;
    exit();
  }  
}


function getPurchaseOrderCancelStatus(){
	global $connMS;
	$resultarray = array();
  $sql = "SELECT * FROM Purchase_Order_Cancel_Status WHERE Active = 'Y' ORDER BY Cancel_Status_ID ASC" ;
	$stmt = sqlsrv_query( $connMS, $sql );
	  if(sqlsrv_has_rows($stmt)) {
	    while( $row = sqlsrv_fetch_array($stmt) ) { 
	      $resultarray[] = $row;
	    }
	    return $resultarray;
			exit();
		}
}

function getPurchaseOrderCancelFiles($poid)
{
    global $connMS;
    $resultarray = array();
    $sql         = "SELECT * FROM Purchase_Order_Cancel_File WHERE Purchase_Order_Cancel_File.PO_ID = '" . $poid . "' AND Active = 'Y' ";
    $stmt        = sqlsrv_query($connMS, $sql);
    if (sqlsrv_has_rows($stmt)) {
        while ($row = sqlsrv_fetch_array($stmt)) {
            $resultarray[] = $row;
        }
        return $resultarray;
        exit();
    }
}

function addPurchaseOrderCancel($arr){
	global $connMS;
	$UserCode = $_SESSION["User"]['UserCode'];

	$sql = "INSERT INTO Purchase_Order_Cancel ([PO_ID], [Cancel_status], [Customer_sendto_date], [Customer_sendto_date_by], [Broker_sendto_date], [Broker_sendto_date_by], [Remark], [Endorse], [Endorse_date], [Endorse_by], [Active], [Create_Date], [Create_By], [Update_Date], [Update_By], [Status_Emai_Date], [Send_Email_by])
     VALUES ('".$arr["poid"]."', '".$arr["Cancel_status"]."', '', '', '', '', '".$arr["Remark"]."', 'N', '', '', 'Y', '".date("Y-m-d H:i:s")."', '".$UserCode."', '".date("Y-m-d H:i:s")."', '".$UserCode."', '', '')";

	$rs = sqlsrv_query( $connMS, $sql);
	return $rs;
	exit();

}

function addPurchaseOrderCancelFile($poid, $arr)
{
    global $connMS;
    $UserCode = $_SESSION["User"]['UserCode'];

    foreach ($arr as $key => $value) {
	    $sql = "INSERT INTO Purchase_Order_Cancel_File ([PO_ID], [PO_Cancel_file], [Active], [Create_Date], [Create_By])
	      VALUES ('" . $poid . "', '" .$value. "', 'Y', '" . date("Y-m-d H:i:s") . "', '" . $UserCode . "')";
    	$rs = sqlsrv_query($connMS, $sql);
    }

    return $rs;
    exit();

}


function getDepartmentIDSuperSale($UserCode){
	global $connMS;
	$resultarray = array();
	$sql = "SELECT User_Dept_ID FROM My_User WHERE User_ID = '".$UserCode."'  ";
	$stmt = sqlsrv_query( $connMS, $sql );
  if(sqlsrv_has_rows($stmt)) {
  	$row = sqlsrv_fetch_array($stmt);
  	return  $row["User_Dept_ID"];
    exit();
  }
}


function getPOCancel($case, $SearchPO){
	global $connMS;
	$UserCode = $_SESSION["User"]['UserCode'];
	$userType = $_SESSION["User"]['type'];

	if($userType == "SuperSale"){
  	$getDepartmentIDSuperSale = getDepartmentIDSuperSale($UserCode);
  }	

	$resultarray = array();
	$sql = "";
	if($case == 5){
		$sql .= "SELECT TOP 500";
	}else{
  	$sql .= "SELECT ";
  }

  $sql .= " Purchase_Order_Cancel.*, Purchase_Order_Cancel_Status.Cancel_Status, Purchase_Order.PO_Date, Purchase_Order.Operation_Type_ID, Purchase_Order.Insurance_ID, Purchase_Order.Customer_ID, Purchase_Order.Agent_ID, Purchase_Order.Employee_ID, Purchase_Order.Status_ID, Policy_Delivery.Policy_No, Customer.Customer_ID, Customer.Customer_FName, Customer.Customer_LName, Customer.EMail, Customer.Tel_No, Customer.Mobile_No, Car_Detail.Plate_No, Car_Make.Make_Name_TH, Asia_Car_Model.Model_Desc, Province.Province_Name_TH ,Delivery_Status.Delivery_Status_Desc, Status.Status_ID, Status.Status_Desc, Insurance_Package.Insurance_Package_Name, My_User.User_FName, My_User.User_LName, Insurance.Insurance_Name, Insurer.Insurer_Name,
  	Purchase_Order.Net_Premium, Purchase_Order.Compulsory, Purchase_Order.Discount, Purchase_Order.Premium_After_Disc, Purchase_Order.Status_ID
		FROM Purchase_Order_Cancel 
		LEFT JOIN Purchase_Order_Cancel_Status ON Purchase_Order_Cancel.Cancel_status = Purchase_Order_Cancel_Status.Cancel_Status_ID
		LEFT JOIN Purchase_Order ON Purchase_Order_Cancel.PO_ID = Purchase_Order.PO_ID
		LEFT JOIN Purchase_Order_Insurer ON Purchase_Order_Cancel.PO_ID = Purchase_Order_Insurer.PO_ID
		LEFT JOIN My_User ON Purchase_Order.Employee_ID = My_User.User_ID
		LEFT JOIN Insurer ON Purchase_Order_Insurer.Insurer_ID = Insurer.Insurer_ID
		LEFT JOIN Insurance ON Purchase_Order.Insurance_ID = Insurance.Insurance_ID
		LEFT JOIN Insurance_Package ON Purchase_Order.Insurance_Package_ID = Insurance_Package.Insurance_Package_ID
		LEFT JOIN Status ON Purchase_Order.Status_ID = Status.Status_ID
		LEFT JOIN Policy_Delivery ON Purchase_Order.PO_ID = Policy_Delivery.PO_ID
		LEFT JOIN Delivery_Status ON Policy_Delivery.Delivery_Status_ID = Delivery_Status.Delivery_Status_ID
		LEFT JOIN Customer ON Purchase_Order.Customer_ID = Customer.Customer_ID
		LEFT JOIN Car_Detail ON Car_Detail.PO_ID = Purchase_Order.PO_ID
		LEFT JOIN Car_Make ON Car_Detail.Make_ID = Car_Make.Make_ID
  	LEFT JOIN Asia_Car_Model ON Car_Detail.Model = Asia_Car_Model.Model_ID 
  	LEFT JOIN Province ON Car_Detail.Plate_Province = Province.Province_ID ";


		if($case == 1){
			$sql .= " WHERE Purchase_Order_Cancel.Customer_sendto_date = '1900-01-01 00:00:00' AND Purchase_Order_Cancel.Status_Emai_Date = '1900-01-01 00:00:00'" ;
		
		}else if($case == 2){
			$sql .= " WHERE Purchase_Order_Cancel.Customer_sendto_date != '1900-01-01 00:00:00' AND Purchase_Order_Cancel.Status_Emai_Date = '1900-01-01 00:00:00' AND Purchase_Order_Cancel.Send_Email_by = '' " ;

		}else if($case == 3){
			$sql .= " WHERE Purchase_Order_Cancel.Broker_sendto_date = '1900-01-01 00:00:00' AND Purchase_Order_Cancel.Status_Emai_Date != '1900-01-01 00:00:00' " ;
		
		}else if($case == 4){
			$sql .= " WHERE Purchase_Order_Cancel.Endorse = 'N' AND Purchase_Order_Cancel.Broker_sendto_date != '1900-01-01 00:00:00' " ;
		}else if($case == 5){
			if($userType == "Sale"){
				$sql .= " WHERE Purchase_Order_Cancel.Status_Emai_Date != '1900-01-01 00:00:00' " ;
			}else{
				$sql .= " WHERE Purchase_Order_Cancel.Endorse = 'Y' AND Purchase_Order_Cancel.Broker_sendto_date != '1900-01-01 00:00:00' " ;
			}
		}

  	if($userType == "Sale"){
  		$sql .= " AND  Purchase_Order.Employee_ID = '".$UserCode."' ";
  	}	

  	if($userType == "SuperSale"){
  		$sql .= " AND  My_User.User_Dept_ID = '".$getDepartmentIDSuperSale."' ";
  	}	

  	if($SearchPO){
  		$sql .= " AND Purchase_Order_Cancel.PO_ID = '".$SearchPO."' OR (Car_Detail.Plate_No = '".$SearchPO."' ) OR (Customer.Customer_FName = '".$SearchPO."' )
  		OR (Customer.Tel_No = '".$SearchPO."' ) OR (Customer.Mobile_No = '".$SearchPO."' )";
  	}

  	if($case == 5){
  		$sql .= " ORDER BY Broker_sendto_date DESC ";
  	}else{
  		$sql .= " ORDER BY Create_Date ASC ";
  	}
// echo $sql;
    $stmt = sqlsrv_query( $connMS, $sql );
	  if(sqlsrv_has_rows($stmt)) {
	    while( $row = sqlsrv_fetch_array($stmt) ) { 
	      $resultarray[] = $row;
	    }
	    return $resultarray;
			exit();
		}
}


function mailInsCancel($arr, $files){

  // echo "<pre>".print_r($files,1)."</pre>";
  $mail = new PHPMailer\PHPMailer\PHPMailer(true);
  $mail->CharSet = 'utf-8';
  $mail->SMTPDebug = 0;   
  $mail->Timeout   = 90;                              // Enable verbose debug output
  $mail->isSMTP();                                      // Set mailer to use SMTP
  $mail->Host = 'smtp.gmail.com'; // Specify main and backup SMTP servers
  $mail->SMTPAuth = true;  
  $mail->Username = "admin@asiadirect.co.th";
  $mail->Password = "@db6251000";
  $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
  $mail->Port = 587;                                    // TCP port to connect to
  // $mail->setFrom('admin@asiadirect.co.th', 'Asia Direct Broker');
  $mail->setFrom($_SESSION["User"]['email'], $_SESSION["User"]['firstname']." Asia Direct" );
  $mail->AddReplyTo($_SESSION["User"]['email'], $_SESSION["User"]['firstname']." Asia Direct" );
  if($_POST["mailsend"]){ 
    foreach ($_POST["mailsend"] as $key => $valMail) {
      $nameMail = explode("|", $valMail);
      $nameemail = $nameMail[1] ? $nameMail[1] : $nameMail[0];
      if(ereg_replace('[[:space:]]+', ' ', trim($nameMail[2])) == "CC"){
        $mail->AddCC($nameMail[0], $nameemail);
      }else{
        $mail->AddAddress($nameMail[0], $nameemail); //ลุกค้า
      }
    }
  }

  // $mail->AddAddress('teerakan.s@asiadirect.co.th', 'IT');               // Name is optional
  $mail->isHTML(true);                                  // Set email format to HTML
  $mail->Subject = $arr["subject"];
  $mail->Body  = $arr["bodymail"];

//   Attachments
  if($files){
    foreach ($files["files"]['tmp_name'] as $key => $value) {
      $uploadfile = tempnam(sys_get_temp_dir(), sha1($_FILES['files']['name'][$key]));
      $filename = $_FILES['files']['name'][$key];
      if (move_uploaded_file($_FILES['files']['tmp_name'][$key], $uploadfile)) {
          $mail->addAttachment($uploadfile, $filename);
      }
    }
 }

  if(!$mail->Send()) {
    return 0;
  } else {
    return 1;
  }
  exit();
}


// function curlLineNoti($user, $msg){
// 	$user = "ADB59019";
// 	$url  = 'https://www.asiadirect.co.th/line-at/ibroker/linemeapi.php';
// 	$data = array(
// 	    'user_id' => $user,
// 	    'status' => "CancalPO",
// 	    'messages' => $msg
// 	);
// 		$ch = curl_init($url);
// 		curl_setopt($ch, CURLOPT_POST, 1);
// 		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
// 		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
// 		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
// 		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
// 		$result = curl_exec($ch);
// 		$error_msg = curl_error($ch);
// 		curl_close($ch);
// 	return 1;
// 	exit();
// }


function getSendSMS($phone, $txtSMS){
// echo $phone;
// exit();
	
	// $phone =  "0894353549";
	$result_sms = sms::send_sms('adbwecare','6251002', $phone, $txtSMS,'AsiaDirect','','');
	
	if($result_sms){
		return 1;
	}else{
		return 0;
	}
	exit();
}


function getKeyIDFromDay($day){
	global $arrayA1;
	$keyID = 0;
	foreach ($arrayA1 as $key => $value) {
		if($value["min"] <= $day && $value["max"] >= $day){
			$keyID = $key ;
		}
	}
	return $keyID;
	exit();
}


function getRowPODetail($poid){
	global $connMS;
	$sql = "SELECT Purchase_Order.*, Customer.Customer_FName, Customer.Customer_LName, Customer.Tel_No, Customer.Mobile_No, My_User.User_ID, My_User.User_FName, My_User.User_LName, My_User.User_Phone, Car_Detail.Plate_No, Purchase_Order.Employee_ID, Purchase_Order_Insurer.Policy_No, Purchase_Order_Insurer.Compulsory_No
	FROM Purchase_Order
  LEFT JOIN [dbo].[Purchase_Order_Insurer] ON Purchase_Order.PO_ID = Purchase_Order_Insurer.PO_ID
  LEFT JOIN [dbo].[Car_Detail] ON Purchase_Order.PO_ID = Car_Detail.PO_ID
  LEFT JOIN [dbo].[Customer] ON Purchase_Order.Customer_ID = Customer.Customer_ID
  LEFT JOIN [dbo].[My_User] ON Purchase_Order.Employee_ID = My_User.User_ID
	WHERE Purchase_Order.PO_ID = '".$poid."' ";

	$stmt = sqlsrv_query( $connMS, $sql );
 	if(sqlsrv_has_rows($stmt)) {
    $row = sqlsrv_fetch_array($stmt);
    return $row;
    exit();
  }  
  exit();
}


function DateDiff($strDate1,$strDate2){
	return (strtotime($strDate2) - strtotime($strDate1))/  ( 60 * 60 * 24 );  // 1 day = 60*60*24
}

function getSumPayment($poid){
	global $connMS;
	$sql = "SELECT SUM(ISTM_Total_Amount) AS payment FROM Installment WHERE PO_ID = '".$poid."' AND Installment_Status_ID = '009'";
	$stmt = sqlsrv_query( $connMS, $sql );
 	if(sqlsrv_has_rows($stmt)) {
    $row = sqlsrv_fetch_array($stmt);
    return $row["payment"];
    exit();
  }  
  exit();
}


function getIntallmentPayment($poid){
	global $connMS;
	$sql = "SELECT Installment.PO_ID, Installment.Installment_ID, Installment.Installment_Status_ID,  Installment.ISTM_Total_Amount, Installment_Status.Installment_Status, Receive.Receive_Date, Receive_Installment.Receive_ID, Receive.Receive_Amount, Receive_Operation_Cost.Amount, Operation_Cost.Operation_Cost_Desc
	FROM Installment 
	LEFT JOIN Installment_Status ON Installment.Installment_Status_ID = Installment_Status.Installment_Status_ID
	LEFT JOIN Receive_Installment ON Installment.PO_ID = Receive_Installment.PO_ID AND Installment.Installment_ID = Receive_Installment.Installment_ID
	LEFT JOIN Receive ON Receive_Installment.Receive_ID = Receive.Receive_ID 
	LEFT JOIN Receive_Operation_Cost ON Receive.Receive_ID = Receive_Operation_Cost.Receive_ID 
	LEFT JOIN Operation_Cost ON Receive_Operation_Cost.Operation_Cost_ID = Operation_Cost.Operation_Cost_ID
	WHERE Receive.Receive_Status_ID != '009' AND Installment.PO_ID = '".$poid."' ";
	$stmt = sqlsrv_query( $connMS, $sql );
	  if(sqlsrv_has_rows($stmt)) {
	    while( $row = sqlsrv_fetch_array($stmt) ) { 
	      $resultarray[] = $row;
	    }
	    return $resultarray;
			exit();
		}
}

function getPOCancelReport($date){
	global $connMS;
	$exDate = explode("-", $date);
	$year = $exDate[0];
	$mounth = $exDate[1];
  $resultarray = array();
 	$datestart =  $year."-".$mounth."-01";

 	$datestartPO = date("Y-m-d",strtotime($datestart.'-3 month'));

  if($year == date("Y") && $mounth == date("n")){
    $dateNow = date("Y-m-d")." 23:59:59";
  }else{
    $dateNow = $year."-".$mounth."-".date('t',strtotime($year."-".$mounth))." 23:59:59";
  }


 $sql = "SELECT Purchase_Order_Cancel.*, Purchase_Order_Cancel_Status.Cancel_Status, Purchase_Order.PO_Date, Purchase_Order.Operation_Type_ID, Purchase_Order.Insurance_ID, Purchase_Order.Customer_ID, Purchase_Order.Agent_ID, Purchase_Order.Employee_ID, Purchase_Order.Status_ID, Policy_Delivery.Policy_No, Customer.Customer_ID, Customer.Customer_FName, Customer.Customer_LName, Customer.EMail, Customer.Tel_No, Customer.Mobile_No, Car_Detail.Plate_No, Car_Make.Make_Name_TH, Asia_Car_Model.Model_Desc, Province.Province_Name_TH ,Delivery_Status.Delivery_Status_Desc, Status.Status_ID, Status.Status_Desc, Insurance_Package.Insurance_Package_Name, My_User.User_FName, My_User.User_LName, Insurance.Insurance_Name, Insurer.Insurer_Initials, Insurer.Insurer_Name, Purchase_Order.Net_Premium, Purchase_Order.Compulsory, Purchase_Order.Discount, Purchase_Order.Premium_After_Disc, Purchase_Order.Coverage_Start_Date, Purchase_Order.Coverage_End_Date,
  (SELECT SUM(Installment.ISTM_Total_Amount) FROM Installment WHERE Installment.PO_ID = Purchase_Order.PO_ID AND Installment.Installment_Status_ID = '009' ) AS SumPayment,
  (SELECT count(Installment.PO_ID) FROM Installment WHERE Installment.PO_ID = Purchase_Order.PO_ID ) AS countIns,
  (SELECT count(Installment.PO_ID) FROM Installment WHERE Installment.PO_ID = Purchase_Order.PO_ID AND Installment.Installment_Status_ID = '009' ) AS countPayment
		FROM Purchase_Order_Cancel 
		LEFT JOIN Purchase_Order_Cancel_Status ON Purchase_Order_Cancel.Cancel_status = Purchase_Order_Cancel_Status.Cancel_Status_ID
		LEFT JOIN Purchase_Order ON Purchase_Order_Cancel.PO_ID = Purchase_Order.PO_ID
		LEFT JOIN Purchase_Order_Insurer ON Purchase_Order_Cancel.PO_ID = Purchase_Order_Insurer.PO_ID
		LEFT JOIN My_User ON Purchase_Order.Employee_ID = My_User.User_ID
		LEFT JOIN Insurer ON Purchase_Order_Insurer.Insurer_ID = Insurer.Insurer_ID
		LEFT JOIN Insurance ON Purchase_Order.Insurance_ID = Insurance.Insurance_ID
		LEFT JOIN Insurance_Package ON Purchase_Order.Insurance_Package_ID = Insurance_Package.Insurance_Package_ID
		LEFT JOIN Status ON Purchase_Order.Status_ID = Status.Status_ID
		LEFT JOIN Policy_Delivery ON Purchase_Order.PO_ID = Policy_Delivery.PO_ID
		LEFT JOIN Delivery_Status ON Policy_Delivery.Delivery_Status_ID = Delivery_Status.Delivery_Status_ID
		LEFT JOIN Customer ON Purchase_Order.Customer_ID = Customer.Customer_ID
		LEFT JOIN Car_Detail ON Car_Detail.PO_ID = Purchase_Order.PO_ID
		LEFT JOIN Car_Make ON Car_Detail.Make_ID = Car_Make.Make_ID
  	LEFT JOIN Asia_Car_Model ON Car_Detail.Model = Asia_Car_Model.Model_ID 
  	LEFT JOIN Province ON Car_Detail.Plate_Province = Province.Province_ID 
  	WHERE Purchase_Order_Cancel.Create_Date >= '".$datestart."' AND Purchase_Order_Cancel.Create_Date <= '".$dateNow."' 
  	ORDER BY Purchase_Order_Cancel.Create_Date ASC";
  	$stmt = sqlsrv_query( $connMS, $sql );
	  if(sqlsrv_has_rows($stmt)) {
	    while( $row = sqlsrv_fetch_array($stmt) ) { 
	      $resultarray[] = $row;
	    }
	    return $resultarray;
			exit();
		}
}

function getFilePOForCancel($poid){
	global $conn2;
    $noti_work_files = $conn2->GetAll("SELECT * FROM noti_work_files WHERE po_id ='" . $poid . "'");
    return $noti_work_files;

}