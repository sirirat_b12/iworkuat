<?php



function getPOData($txtSe){
	global $connMS;
	$resultarray = array();
  $sql = "SELECT  Purchase_Order.PO_ID, Purchase_Order.PO_Date, Purchase_Order.Operation_Type_ID, Purchase_Order.Insurance_ID, Purchase_Order.Customer_ID, Purchase_Order.Agent_ID, Purchase_Order.Employee_ID, Purchase_Order.Status_ID, Policy_Delivery.Policy_No,  Customer.Customer_ID, Customer.Customer_FName, Customer.Customer_LName, Customer.EMail, Customer.Tel_No, Customer.Mobile_No, Car_Detail.Plate_No, Car_Make.Make_Name_TH, Asia_Car_Model.Model_Desc, Province.Province_Name_TH ,Delivery_Status.Delivery_Status_Desc, Status.Status_ID, Status.Status_Desc
		FROM Purchase_Order 
		LEFT JOIN Status ON Purchase_Order.Status_ID = Status.Status_ID
		LEFT JOIN Policy_Delivery ON Purchase_Order.PO_ID = Policy_Delivery.PO_ID
		LEFT JOIN Delivery_Status ON Policy_Delivery.Delivery_Status_ID = Delivery_Status.Delivery_Status_ID
		LEFT JOIN Customer ON Purchase_Order.Customer_ID = Customer.Customer_ID
		LEFT JOIN Car_Detail ON Car_Detail.PO_ID = Purchase_Order.PO_ID
		LEFT JOIN Car_Make ON Car_Detail.Make_ID = Car_Make.Make_ID
  	LEFT JOIN Asia_Car_Model ON Car_Detail.Model = Asia_Car_Model.Model_ID 
  	LEFT JOIN Province ON Car_Detail.Plate_Province = Province.Province_ID
		WHERE Purchase_Order.PO_ID = '".$txtSe."'" ;

    $stmt = sqlsrv_query( $connMS, $sql );
	  if(sqlsrv_has_rows($stmt)) {
	    $row = sqlsrv_fetch_array($stmt);
	    return $row;
			exit();
		}
}



function getPurchaseOrderCancelStatus(){
	global $connMS;
	$resultarray = array();
  $sql = "SELECT * FROM Purchase_Order_Cancel_Status WHERE Active = 'Y' ORDER BY Cancel_Status_ID ASC" ;
	$stmt = sqlsrv_query( $connMS, $sql );
	  if(sqlsrv_has_rows($stmt)) {
	    while( $row = sqlsrv_fetch_array($stmt) ) { 
	      $resultarray[] = $row;
	    }
	    return $resultarray;
			exit();
		}
}

function addPurchaseOrderCancel($arr){
	global $connMS;
	$UserCode = $_SESSION["User"]['UserCode'];

	$sql = "INSERT INTO Purchase_Order_Cancel ([PO_ID], [Cancel_status], [Customer_sendto_date], [Customer_sendto_date_by], [Broker_sendto_date], [Broker_sendto_date_by], [Remark], [Endorse], [Endorse_date], [Endorse_by], [Active], [Create_Date], [Create_By], [Update_Date], [Update_By], [Status_Emai_Date], [Send_Email_by])
     VALUES ('".$arr["poid"]."', '".$arr["Cancel_status"]."', '', '', '', '', '".$arr["Remark"]."', 'N', '', '', 'Y', '".date("Y-m-d H:i:s")."', '".$UserCode."', '".date("Y-m-d H:i:s")."', '".$UserCode."', '', '')";

	$rs = sqlsrv_query( $connMS, $sql);
	return $rs;
	exit();

}


function getPOrCancel($case, $SearchPO){
	global $connMS;
	$UserCode = $_SESSION["User"]['UserCode'];
	$userType = $_SESSION["User"]['type'];

	$resultarray = array();
	$sql = "";
	if($case == 5){
		$sql .= "SELECT TOP 500";
	}else{
  	$sql .= "SELECT ";
  }

  $sql .= " Purchase_Order_Cancel.*, Purchase_Order_Cancel_Status.Cancel_Status, Purchase_Order.PO_Date, Purchase_Order.Operation_Type_ID, Purchase_Order.Insurance_ID, Purchase_Order.Customer_ID, Purchase_Order.Agent_ID, Purchase_Order.Employee_ID, Purchase_Order.Status_ID, Policy_Delivery.Policy_No, Customer.Customer_ID, Customer.Customer_FName, Customer.Customer_LName, Customer.EMail, Customer.Tel_No, Customer.Mobile_No, Car_Detail.Plate_No, Car_Make.Make_Name_TH, Asia_Car_Model.Model_Desc, Province.Province_Name_TH ,Delivery_Status.Delivery_Status_Desc, Status.Status_ID, Status.Status_Desc, Insurance_Package.Insurance_Package_Name, My_User.User_FName, My_User.User_LName, Insurance.Insurance_Name, Insurer.Insurer_Name
		FROM Purchase_Order_Cancel 
		LEFT JOIN Purchase_Order_Cancel_Status ON Purchase_Order_Cancel.Cancel_status = Purchase_Order_Cancel_Status.Cancel_Status_ID
		LEFT JOIN Purchase_Order ON Purchase_Order_Cancel.PO_ID = Purchase_Order.PO_ID
		LEFT JOIN Purchase_Order_Insurer ON Purchase_Order_Cancel.PO_ID = Purchase_Order_Insurer.PO_ID
		LEFT JOIN My_User ON Purchase_Order.Employee_ID = My_User.User_ID
		LEFT JOIN Insurer ON Purchase_Order_Insurer.Insurer_ID = Insurer.Insurer_ID
		LEFT JOIN Insurance ON Purchase_Order.Insurance_ID = Insurance.Insurance_ID
		LEFT JOIN Insurance_Package ON Purchase_Order.Insurance_Package_ID = Insurance_Package.Insurance_Package_ID
		LEFT JOIN Status ON Purchase_Order.Status_ID = Status.Status_ID
		LEFT JOIN Policy_Delivery ON Purchase_Order.PO_ID = Policy_Delivery.PO_ID
		LEFT JOIN Delivery_Status ON Policy_Delivery.Delivery_Status_ID = Delivery_Status.Delivery_Status_ID
		LEFT JOIN Customer ON Purchase_Order.Customer_ID = Customer.Customer_ID
		LEFT JOIN Car_Detail ON Car_Detail.PO_ID = Purchase_Order.PO_ID
		LEFT JOIN Car_Make ON Car_Detail.Make_ID = Car_Make.Make_ID
  	LEFT JOIN Asia_Car_Model ON Car_Detail.Model = Asia_Car_Model.Model_ID 
  	LEFT JOIN Province ON Car_Detail.Plate_Province = Province.Province_ID ";


		if($case == 1){
			$sql .= " WHERE Purchase_Order_Cancel.Status_Emai_Date = '1900-01-01 00:00:00' AND Purchase_Order_Cancel.Send_Email_by = '' " ;
		
		}else if($case == 2){
			$sql .= " WHERE Purchase_Order_Cancel.Customer_sendto_date = '1900-01-01 00:00:00' AND Purchase_Order_Cancel.Status_Emai_Date != '1900-01-01 00:00:00'" ;

		}else if($case == 3){
			$sql .= " WHERE Purchase_Order_Cancel.Broker_sendto_date = '1900-01-01 00:00:00' AND Purchase_Order_Cancel.Customer_sendto_date != '1900-01-01 00:00:00'" ;
		
		}else if($case == 4){
			$sql .= " WHERE Purchase_Order_Cancel.Endorse = 'N' AND Purchase_Order_Cancel.Broker_sendto_date != '1900-01-01 00:00:00' AND Purchase_Order_Cancel.Customer_sendto_date != '1900-01-01 00:00:00' " ;
		}else if($case == 5){
			if($userType == "Sale"){
				$sql .= " WHERE Purchase_Order_Cancel.Status_Emai_Date != '1900-01-01 00:00:00' " ;
			}else{
				$sql .= " WHERE Purchase_Order_Cancel.Endorse = 'Y' AND Purchase_Order_Cancel.Broker_sendto_date != '1900-01-01 00:00:00' AND Purchase_Order_Cancel.Customer_sendto_date != '1900-01-01 00:00:00' " ;
			}
		}

  	if($userType == "Sale"){
  		$sql .= " AND  Purchase_Order.Employee_ID = '".$UserCode."' ";
  	}	

  	if($SearchPO){
  		$sql .= " AND  Purchase_Order_Cancel.PO_ID = '".$SearchPO."' ";
  	}

  	if($case == 5){
  		$sql .= " ORDER BY Broker_sendto_date DESC ";
  	}else{
  		$sql .= " ORDER BY Create_Date ASC ";
  	}
// echo $sql;
    $stmt = sqlsrv_query( $connMS, $sql );
	  if(sqlsrv_has_rows($stmt)) {
	    while( $row = sqlsrv_fetch_array($stmt) ) { 
	      $resultarray[] = $row;
	    }
	    return $resultarray;
			exit();
		}
}


function mailInsCancel($arr, $files){

  // echo "<pre>".print_r($files,1)."</pre>";
  $mail = new PHPMailer\PHPMailer\PHPMailer(true);
  $mail->CharSet = 'utf-8';
  $mail->SMTPDebug = 0;   
  $mail->Timeout   = 90;                              // Enable verbose debug output
  $mail->isSMTP();                                      // Set mailer to use SMTP
  $mail->Host = 'smtp.gmail.com'; // Specify main and backup SMTP servers
  $mail->SMTPAuth = true;  
  $mail->Username = "admin@asiadirect.co.th";
  $mail->Password = "@db6251000";
  $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
  $mail->Port = 587;                                    // TCP port to connect to
  // $mail->setFrom('admin@asiadirect.co.th', 'Asia Direct Broker');
  $mail->setFrom($_SESSION["User"]['email'], $_SESSION["User"]['firstname']." Asia Direct" );
  $mail->AddReplyTo($_SESSION["User"]['email'], $_SESSION["User"]['firstname']." Asia Direct" );
  // if($_POST["mailsend"]){ 
  //   foreach ($_POST["mailsend"] as $key => $valMail) {
  //     $nameMail = explode("|", $valMail);
  //     $nameemail = $nameMail[1] ? $nameMail[1] : $nameMail[0];
  //     if(ereg_replace('[[:space:]]+', ' ', trim($nameMail[2])) == "CC"){
  //       $mail->AddCC($nameMail[0], $nameemail);
  //     }else{
  //       $mail->AddAddress($nameMail[0], $nameemail); //ลุกค้า
  //     }
  //   }
  // }

  $mail->AddAddress('teerakan.s@asiadirect.co.th', 'IT');               // Name is optional
  $mail->isHTML(true);                                  // Set email format to HTML
  $mail->Subject = $arr["subject"];
  $mail->Body  = $arr["bodymail"];

//   Attachments
  if($files){
    foreach ($files["files"]['tmp_name'] as $key => $value) {
      $uploadfile = tempnam(sys_get_temp_dir(), sha1($_FILES['files']['name'][$key]));
      $filename = $_FILES['files']['name'][$key];
      if (move_uploaded_file($_FILES['files']['tmp_name'][$key], $uploadfile)) {
          $mail->addAttachment($uploadfile, $filename);
      }
    }
 }

  if(!$mail->Send()) {
    return 0;
  } else {
    return 1;
  }
  exit();
}


function curlLineNoti($user, $msg){
	$user = "ADB59019";
	$url  = 'https://www.asiadirect.co.th/line-at/ibroker/linemeapi.php';
	$data = array(
	    'user_id' => $user,
	    'status' => "CancalPO",
	    'messages' => $msg
	);
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$result = curl_exec($ch);
		$error_msg = curl_error($ch);
		curl_close($ch);
	return 1;
	exit();
}
