<?php 
session_start();
$UserCode = $_SESSION["User"]['UserCode'];
$userType = $_SESSION["User"]['type'];

if(!isset($UserCode)){
	echo "<script>alert('กรุณาเข้าสู่ระบบ!!!');</script>";
	echo '<META http-equiv="refresh" content="0;URL=login.php">';
	exit();
}

header('Content-Type: text/html; charset=utf-8');
include "include/inc_header.php"; 
include "include/inc_menu.php"; 
include "inscancel/inc_function_inscancel.php"; 

$getPurchaseOrderCancelStatus = getPurchaseOrderCancelStatus();
$getPOrCancelT1 = getPOrCancel(1, $_GET["txtse"]);
$getPOrCancelT5 = getPOrCancel(5, $_GET["txtse"]);
if($userType == "SuperAdmin" || $userType == "Admin"  ){ 
	// $getPOrCancelT2 = getPOrCancel(2, $_GET["txtse"]);
	$getPOrCancelT3 = getPOrCancel(3, $_GET["txtse"]);
	$getPOrCancelT4 = getPOrCancel(4, $_GET["txtse"]);
}

// echo "<pre>".print_r($getPOrCancelT1,1)."</pre>";
?>

<div class="main">
	<div class="p20">
		<div class="panel">
			 <div class="row p0 m15">
			 		<div class="col-md-2">
			 			<input type="text" class="cff2da5 form-control fs12" id="txtCancelPO"  placeholder="กรอก PO">
			 		</div>
			 		<div class="col-md-6">
			 			<input type="button" class="btn btn-primary" id="btnModalCancelPO" value="แจ้งยกเลิก">
			 			<input type="button" class="btn btn-success" onclick="btnSearch()" name="txtse" value="ค้นหา PO">
			 			<a href="cancelpolicy.php" class="btn btn-danger">คืนค่า</a>
			 		</div>
			 </div>
			 <div class="main-content p20 clearb" style="border-top: 1px solid #FF9800;">
					<ul class="nav nav-tabs">
						<li class="active"><a data-toggle="tab" href="#menu1">รอแจ้งยกเลิก</a></li>
						<?php if($userType == "SuperAdmin" || $userType == "Admin"  ){ ?>
						<!-- <li ><a data-toggle="tab" href="#menu2">ยกเลิกแล้วรอกรมธรรม์จากลูกค้า</a></li> -->
						<li ><a data-toggle="tab" href="#menu3">ยกเลิกแล้วส่งกรมธรรม์คืนบริษัทประกัน</a></li>
						<li ><a data-toggle="tab" href="#menu4">รอสลักหลังบริษัทประกัน</a></li>
						<?php } ?>
						<li ><a data-toggle="tab" href="#menu5">เสร็จสิ้น</a></li>
					</ul>
				</div> 
				<div class="tab-content">
					<div id="menu1" class="tab-pane fade in active">
						<div class="panel-body fs14">
							<table id="tablecp" class="table p5" data-toggle="table" data-pagination="true" data-page-size="200" data-page-list="[200, 300, 400]">
								<thead class="fs13 c000000">
									<tr>
									<?php if($userType == "SuperAdmin" || $userType == "Admin"  ){ ?>
										<th class="t_c">#</th>
										<th class="t_c">ยกเลิก</th>
									<?php } ?>
										<th class="t_c">PO</th>			
										<th class="t_c">รายละเอียดยกเลิก</th>
										<th class="t_c">บริษัทประกัน</th>
										<th class="t_c">รายละเอียดประกัน</th>									
										<th class="t_c">เลขกรมธรรม์</th>	
										<th class="t_c">ลูกค้า</th>	
										<th class="t_c">เวลาทำรายการ</th>
										<th class="t_c">พนักงานขาย</th>
									</tr>
								</thead>
								<tbody class="fs12">
									<?php foreach ($getPOrCancelT1 as $key => $value) { ?>
										<tr >
										<?php if($userType == "SuperAdmin" || $userType == "Admin"  ){ ?>
											<td class="t_c c00ac0a fwb">
												<span class="cursorPoin cff2da5" onclick="submitDeleteList('<?php echo $value["PO_ID"]; ?>')"><i class="fas fa-trash fs16 fwb cff0000"></i> </span>
											</td>
											<td class="t_c fwb">
												<a href="cancelpolicy_email.php?poid=<?php echo $value["PO_ID"]; ?>" target="_Bank" class="c2457ff fwb"><i class="fas fa-envelope-open fs16 fwb"></i> </a> | 
												<span class="cursorPoin cff2da5" onclick="submitCancelINS('<?php echo $value["PO_ID"]; ?>', '<?php echo $value["Employee_ID"]; ?>')"><i class="far fa-bell-slash fs16 fwb "></i> </span>
											</td>
										<?php } ?>
											<td class="t_c c2457ff fwb"><?php echo $value["PO_ID"]; ?></td>
											<td class="t_l">
												<div class="c00ac0a"><?php echo $value["Cancel_Status"]; ?></div>
												<?php if($value["Remark"]){?>
													<div><?php echo $value["Remark"]; ?></div>
												<?php } ?>
											</td>
											<td class="t_l fwb"><?php echo $value["Insurer_Name"]; ?></td>
											<td class="t_l">
												<div class="c2457ff"><?php echo $value["Insurance_Name"]; ?></div>
												<div><?php echo $value["Insurance_Package_Name"]; ?></div>
											</td>
											<td class="t_l c2457ff"><?php echo $value["Policy_No"]; ?></td>
											<td class="t_l fwb"><?php echo $value["Customer_FName"]." ".$value["Customer_LName"]; ?></td>
											<td class="t_l"><?php echo $value["Create_Date"]->format("d/m/Y H:i:s"); ?></td>
											<td class="t_l fwb"><?php echo $value["User_FName"]." ".$value["User_LName"]; ?></td>
										</tr>
									<?php  } ?>
								</tbody>
							</table>
						</div>
					</div>
					<div id="menu2" class="tab-pane fade">
						<!-- <div class="panel-body fs14">
							<table id="tablecp" class="table p5" data-toggle="table" data-pagination="true" data-page-size="200" data-page-list="[200, 300, 400]">
								<thead class="fs13 c000000">
									<tr>
										<th class="t_c">#</th>
										<th class="t_c">PO</th>			
										<th class="t_c">ส่งยกเลิก</th>
										<th class="t_c">รายละเอียดยกเลิก</th>
										<th class="t_c">บริษัทประกัน</th>
										<th class="t_c">รายละเอียดประกัน</th>									
										<th class="t_c">เลขกรมธรรม์</th>	
										<th class="t_c">ลูกค้า</th>	
										<th class="t_c">เวลาทำรายการ</th>
										<th class="t_c">พนักงานขาย</th>
									</tr>
								</thead>
								<tbody class="fs12">
									<?php foreach ($getPOrCancelT2 as $key => $value) { 
										?>
										<tr>
											<td class="t_c ">
												<span class="cursorPoin cff2da5" onclick="onModalUpdateTime('<?php echo $value["PO_ID"]; ?>','1')"><i class="fas fa-clipboard-list fs16 fwb "></i> </span>
											</td>
											<td class="t_c c2457ff fwb"><?php echo $value["PO_ID"]; ?></td>
											<td class="t_l">
												<div class="c2457ff"><?php echo $value["Status_Emai_Date"]->format("d/m/Y H:i:s"); ?></div>
												<div><b>By: </b><?php echo $value["Send_Email_by"]; ?></div>
											</td>
											<td class="t_l">
												<div class="c00ac0a"><?php echo $value["Cancel_Status"]; ?></div>
												<?php if($value["Remark"]){?>
													<div><?php echo $value["Remark"]; ?></div>
												<?php } ?>
											</td>
											<td class="t_l fwb"><?php echo $value["Insurer_Name"]; ?></td>
											<td class="t_l">
												<div class="c2457ff"><?php echo $value["Insurance_Name"]; ?></div>
												<div><?php echo $value["Insurance_Package_Name"]; ?></div>
											</td>
											<td class="t_l c2457ff"><?php echo $value["Policy_No"]; ?></td>
											<td class="t_l fwb"><?php echo $value["Customer_FName"]." ".$value["Customer_LName"]; ?></td>
											<td class="t_l"><?php echo $value["Create_Date"]->format("d/m/Y H:i:s"); ?></td>
											<td class="t_l fwb"><?php echo $value["User_FName"]." ".$value["User_LName"]; ?></td>
										</tr>
									<?php  } ?>
								</tbody>
							</table>
						</div> -->
					</div>
					<div id="menu3" class="tab-pane fade">
						<div class="panel-body fs14">
							<table id="tablecp" class="table p5" data-toggle="table" data-pagination="true" data-page-size="200" data-page-list="[200, 300, 400]">
								<thead class="fs13 c000000">
									<tr>
										<th class="t_c">#</th>
										<th class="t_c">PO</th>			
										<th class="t_c">ส่งยกเลิก</th>
										<th class="t_c">รับกรมจากลูกค้า</th>
										<th class="t_c">รายละเอียดยกเลิก</th>
										<th class="t_c">บริษัทประกัน</th>
										<th class="t_c">รายละเอียดประกัน</th>									
										<th class="t_c">เลขกรมธรรม์</th>	
										<th class="t_c">ลูกค้า</th>	
										<th class="t_c">เวลาทำรายการ</th>
										<th class="t_c">พนักงานขาย</th>
									</tr>
								</thead>
								<tbody class="fs12">
									<?php foreach ($getPOrCancelT3 as $key => $value) { 
										?>
										<tr>
											<td class="t_c ">
												<span class="cursorPoin cff2da5" onclick="onModalUpdateTime('<?php echo $value["PO_ID"]; ?>','2')"><i class="fas fa-clipboard-list fs16 fwb "></i> </span>
											</td>
											<td class="t_c c2457ff fwb"><?php echo $value["PO_ID"]; ?></td>
											<td class="t_l">
												<div class="cff8000"><?php echo $value["Status_Emai_Date"]->format("d/m/Y H:i:s"); ?></div>
												<div><b>By: </b><?php echo $value["Send_Email_by"]; ?></div>
											</td>
											<td class="t_l">
												<div class="cff8000"><?php echo $value["Customer_sendto_date"]->format("d/m/Y H:i:s"); ?></div>
												<div><b>By: </b><?php echo $value["Customer_sendto_date_by"]; ?></div>
											</td>
											<td class="t_l">
												<div class="c00ac0a"><?php echo $value["Cancel_Status"]; ?></div>
												<?php if($value["Remark"]){?>
													<div><?php echo $value["Remark"]; ?></div>
												<?php } ?>
											</td>
											<td class="t_l fwb"><?php echo $value["Insurer_Name"]; ?></td>
											<td class="t_l">
												<div class="cff8000"><?php echo $value["Insurance_Name"]; ?></div>
												<div><?php echo $value["Insurance_Package_Name"]; ?></div>
											</td>
											<td class="t_l c2457ff"><?php echo $value["Policy_No"]; ?></td>
											<td class="t_l fwb"><?php echo $value["Customer_FName"]." ".$value["Customer_LName"]; ?></td>
											<td class="t_l"><?php echo $value["Create_Date"]->format("d/m/Y H:i:s"); ?></td>
											<td class="t_l fwb"><?php echo $value["User_FName"]." ".$value["User_LName"]; ?></td>
										</tr>
									<?php  } ?>
								</tbody>
							</table>
						</div>
					</div>
					<div id="menu4" class="tab-pane fade">
						<div class="panel-body fs14">
							<table id="tablecp" class="table p5" data-toggle="table" data-pagination="true" data-page-size="200" data-page-list="[200, 300, 400]">
								<thead class="fs13 c000000">
									<tr>
										<th class="t_c">#</th>
										<th class="t_c">PO</th>			
										<th class="t_c">ส่งยกเลิก</th>
										<th class="t_c">รับกรมจากลูกค้า</th>
										<th class="t_c">ส่งกรมกลับ บ.ประกัน</th>
										<th class="t_c">รายละเอียดยกเลิก</th>
										<th class="t_c">บริษัทประกัน</th>
										<th class="t_c">รายละเอียดประกัน</th>									
										<th class="t_c">ลูกค้า</th>	
										<th class="t_c">เวลาทำรายการ</th>
										<th class="t_c">พนักงานขาย</th>
									</tr>
								</thead>
								<tbody class="fs12">
									<?php foreach ($getPOrCancelT4 as $key => $value) { 
										?>
										<tr>
											<td class="t_c ">
												<span class="cursorPoin cff2da5" onclick="fnEndorse('<?php echo $value["PO_ID"]; ?>')"><i class="fas fa-exclamation-triangle fs16 fwb "></i></span>
											</td>
											<td class="t_c c2457ff fwb"><?php echo $value["PO_ID"]; ?></td>
											<td class="t_l">
												<div class="c2457ff"><?php echo $value["Status_Emai_Date"]->format("d/m/Y H:i:s"); ?></div>
												<div><b>By: </b><?php echo $value["Send_Email_by"]; ?></div>
											</td>
											<td class="t_l">
												<div class="c2457ff"><?php echo $value["Customer_sendto_date"]->format("d/m/Y H:i:s"); ?></div>
												<div><b>By: </b><?php echo $value["Customer_sendto_date_by"]; ?></div>
											</td>
											<td class="t_l">
												<div class="c2457ff"><?php echo $value["Broker_sendto_date"]->format("d/m/Y H:i:s"); ?></div>
												<div><b>By: </b><?php echo $value["Broker_sendto_date_by"]; ?></div>
											</td>
											<td class="t_l">
												<div class="c00ac0a"><?php echo $value["Cancel_Status"]; ?></div>
												<?php if($value["Remark"]){?>
													<div><?php echo $value["Remark"]; ?></div>
												<?php } ?>
											</td>
											<td class="t_l fwb"><?php echo $value["Insurer_Name"]; ?></td>
											<td class="t_l">
												<div class="cff8000"><?php echo $value["Insurance_Name"]; ?></div>
												<div><?php echo $value["Policy_No"]; ?></div>
											</td>
											<td class="t_l fwb"><?php echo $value["Customer_FName"]." ".$value["Customer_LName"]; ?></td>
											<td class="t_l"><?php echo $value["Create_Date"]->format("d/m/Y H:i:s"); ?></td>
											<td class="t_l fwb"><?php echo $value["User_FName"]." ".$value["User_LName"]; ?></td>
										</tr>
									<?php  } ?>
								</tbody>
							</table>
						</div>
					</div>
					<div id="menu5" class="tab-pane fade">
						<div class="panel-body fs14">
							<table id="tablecp" class="table p5" data-toggle="table" data-pagination="true" data-page-size="100" data-page-list="[200, 300, 400]">
								<thead class="fs13 c000000">
									<tr>
										<th class="t_c">PO</th>			
										<th class="t_c">ส่งยกเลิก</th>
										<th class="t_c">รับกรมจากลูกค้า</th>
										<th class="t_c">ส่งกรมกลับ บ.ประกัน</th>
										<th class="t_c">รับสลักหลัง</th>
										<th class="t_c">รายละเอียดยกเลิก</th>								
										<th class="t_c">เวลาทำรายการ</th>
										<th class="t_c">ผู้ขาย</th>
									</tr>
								</thead>
								<tbody class="fs12">
									<?php foreach ($getPOrCancelT5 as $key => $value) { 
										?>
										<tr <?php echo ($value["Endorse"] == "N")? "class='bgfffbd8 '":"" ?>>
											<td class="t_c c2457ff fwb"><?php echo $value["PO_ID"]; ?></td>
											<td class="t_l">
												<div><?php echo $value["Status_Emai_Date"]->format("d/m/Y"); ?> | <?php echo $value["Send_Email_by"]; ?></div>
											</td>
											<td class="t_l">
												<div><?php echo $value["Customer_sendto_date"]->format("d/m/Y"); ?> | <?php echo $value["Customer_sendto_date_by"]; ?></div>
											</td>
											<td class="t_l">
												<div><?php echo $value["Broker_sendto_date"]->format("d/m/Y"); ?> | <?php echo $value["Broker_sendto_date_by"]; ?></div>
											</td>
											<td class="t_l">
												<div><?php echo $value["Endorse_date"]->format("d/m/Y"); ?> | <?php echo $value["Endorse_by"]; ?></div>
											</td>
											<td class="t_l">
												<div class="c00ac0a"><?php echo $value["Cancel_Status"]; ?></div>
											</td>
											<td class="t_l">
												<div class="c2457ff"><?php echo $value["Create_Date"]->format("d/m/Y H:i:s"); ?></div>
											</td>
											<td class="t_l"><?php echo $value["User_FName"]." ".$value["User_LName"]; ?></td>
										</tr>
									<?php  } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
		</div>
	</div>
</div>

<div class="modal fade" id="ModalCancelPO" tabindex="-1" role="dialog" aria-labelledby="ModalFollowTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<p class="modal-title fs16 fwb" id="memberModalLabel"><b>ยกเลิกกรมธรรม์ </b> <span id="poidKey"></span> </p>
			</div>
			<div class="">
				<div class="p20">
					<div class="row">
						<div class="col-md-6 ModalFollowBody" style="background-color: #ffffed;"></div>
						<div class="col-md-6 boxSubmit">
							<form action="javascript:void(0);" name="fromAddCencal" id="fromAddCencal" >
								<input type="hidden" value="AddCencalPO" name="action">
								<input type="hidden" name="PO_ID" id="PO_ID">
								<div >
									<lable class="cff2da5 ">เหตุผลในการยกเลิก *</lable>	
									<select name="Cancel_status" id="Cancel_status" class="form-control" required> 
										<option value="">== กรุณาเลือก ==</option>
										<?php 
											foreach ($getPurchaseOrderCancelStatus as $key => $value) { 
												if($value["Cancel_Status_ID"] == "CS06" || $value["Cancel_Status_ID"] == "CS07"){
													$note = "(โปรดระบุหมายเหตุ)";
												}
										?>
											<option value="<?php echo $value["Cancel_Status_ID"]; ?>"><?php echo $value["Cancel_Status"]." ".$note; ?></option>
										<?php } ?>
									</select>
								</div>
								<div class="mt10">
									<lable class="cff2da5 ">หมายเหตุ</lable>	
									<textarea name="Remark" id="Remark"  class="form-control" style="height: 100px;" ></textarea>
								</div>
								<div class="t_c mt10 ">
									<button class="btn btn-success " onClick="submitAddCencalPO()">บันทึก</button>
									<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="Customersendtodate" tabindex="-1" role="dialog" aria-labelledby="ModalFollowTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered ">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<p class="modal-title fs16 fwb" id="memberModalLabel"><span id="poidName"></span> </p>
			</div>
			<div class="">
				<div class="p20">
					<div class="">
						<form action="inscancel/inc_action_inscancel.php" name="fromAddCencal"  method="POST">
							<input type="hidden" value="upDateTimeINS" name="action">
							<input type="hidden" name="poid" id="poidTime">
							<input type="hidden" name="caseType" id="caseType">
							<div class="mt10 ">
								<div class="mt10" id="timeCase1">
									<lable class="cff2da5 ostpone">วันที่รับกรมธรรม์จากลูกค้า *</lable>	
									<input type="date" name="Customer_sendto_date" id="Customer_sendto_date"  class="form-control">
								</div>
								<div class="mt10" id="timeCase2">
									<lable class="cff2da5 ostpone">วันที่ส่งกรมธรรม์ให้บริษัทประกัน *</lable>	
									<input type="date" name="Broker_sendto_date" id="Broker_sendto_date"  class="form-control" >
								</div>
							</div>
							<div class="t_c mt10 ">
								<input type="submit" value="บันทึก" class="btn btn-success ">
								<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php include "include/inc_footer.php"; ?> 
<!-- <script src="fancybox/dist/jquery.fancybox.min.js"></script> -->
<script type="text/javascript">
	$( "#btnModalCancelPO" ).click(function() {
		poid = $("#txtCancelPO").val();
		if(poid){
			$.ajax({ 
					url: 'inscancel/inc_action_inscancel.php',
					type:'POST',
					data: {action: 'getPOData', poid:poid},
					success:function(rs){
						$("#poidKey").html(poid);
						$("#PO_ID").val(poid);
						$(".ModalFollowBody").html(rs);
					}
				});
		  $('#ModalCancelPO').modal('toggle');
		}else{
			alert("กรุณากรอก PO");
		}
	});

	function btnSearch(){
		txtse = $("#txtCancelPO").val();
		if(txtse){
			window.location.href = "cancelpolicy.php?txtse="+txtse;
		}else{
			alert("กรุณากรอก PO");
		}
	}

	function onModalUpdateTime(po, caseType){
		$("#poidTime").val(po);
		$("#caseType").val(caseType);
		$("#poidName").html(po);
		if(caseType == 1){
			$("#timeCase2").hide();
			$("#Customer_sendto_date").prop('required',true);
		}else{
			$("#timeCase1").hide();
			$("#Broker_sendto_date").prop('required',true);
		}
		$('#Customersendtodate').modal();
	}

	function submitDeleteList(poid){
		// console.log(poid);
		if(confirm("ต้องการลบรายการแจ้งขอยกเลิกกรมธรรม์ "+poid)){
			$.ajax({ 
				url: 'inscancel/inc_action_inscancel.php',
				type:'POST',
				data: {action: 'submitDeleteList', poid:poid},
				success:function(rs){
					if(rs){
						alert("ลบรายการแจ้งขอยกเลิกกรมธรรม์ เรียบร้อย");
					}else{
						alert("ไม่สามารถทำรายการได้ กรุณาลองใหม่อีกครั้ง");
					}
					window.location.reload(true);
				}
			});
		}
	}

	function fnEndorse(poid){
		// console.log(poid);
		if(confirm("ยันยันการรับสลักหลัง "+poid)){
			$.ajax({ 
				url: 'inscancel/inc_action_inscancel.php',
				type:'POST',
				data: {action: 'confrimEndorse', poid:poid},
				success:function(rs){
					if(rs){
						alert("ยืนยันการรับสลักหลังเรียบร้อย");
					}else{
						alert("ไม่สามารถทำรายการได้ กรุณาลองใหม่อีกครั้ง");
					}
					window.location.reload(true);
				}
			});
		}
	}

	function submitCancelINS(poid, empid){
		// console.log(poid);
		if(confirm("ต้องการยกเลิกกรมธรรม์ "+poid)){
			$.ajax({ 
				url: 'inscancel/inc_action_inscancel.php',
				type:'POST',
				data: {action: 'submitCancelINS', poid:poid , empid:empid},
				success:function(rs){
					console.log(rs);
					if(rs){
						alert("แก้ไขสถานะ PO "+poid+ " เรียบร้อย");
					}else{
						alert("กรุณาลองใหม่อีกครั้ง");
					}
					window.location.reload(true);
				}
			});
		}
	}

	function submitAddCencalPO(){
		poid = $("#PO_ID").val();
		Cancel_status = $("#Cancel_status").val();
		Remark = $("#Remark").val();
		if(Cancel_status){
			$.ajax({ 
				url: 'inscancel/inc_action_inscancel.php',
				type:'POST',
				data: {action: 'AddCencalPO', poid:poid, Cancel_status:Cancel_status, Remark:Remark},
				success:function(rs){
					if(rs){
						alert("แจ้งยกเลิกสำเร็จ");
						window.location.reload(true);
					}else{
						alert("ไม่สาามารถแจ้งยกเลิกได้ กรุณาตรวจสอบ อาจจะมีการแจ้งงานแล้ว");
						window.location.reload(true);
					}
				}
			});
		}else{
			alert("กรุณาเลือกเหตุผลในการยกเลิก");
		}

	}
</script>