<?php
session_start();
ini_set('post_max_size', '512M');
ini_set('upload_max_filesize', '512M');
ini_set("memory_limit","512M");
ini_set('max_execution_time', 300);
date_default_timezone_set("Asia/Bangkok");
header('Content-Type: text/html; charset=utf-8');

include "../inc_config.php"; 
require '../phpmailer6/src/PHPMailer.php';
require '../phpmailer6/src/SMTP.php';
require '../phpmailer6/src/Exception.php';
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

include "../inc_config.php";
include "inc_function_inscancel.php"; 

$UserCode = $_SESSION["User"]['UserCode'];

if($_POST["action"] == "getPOData"){
	$getPOData = getPOData($_POST["poid"]);
	// echo "<pre>".print_r($getPOData,1)."</pre>";
	$html = "";
	echo $html .= "
	<div class='lh20 p20'>
		<div><b>คุณ </b>".$getPOData["Customer_FName"]." ".$getPOData["Customer_LName"]."</div>
		<div><b>สถานะ PO :</b> ".$getPOData["Status_ID"]." | ".$getPOData["Status_Desc"]."</div>
		<div><b>เลขกรมธรรม์ :</b> ".$getPOData["Policy_No"]."</div>
		<div><b>สถานะการจัดส่ง </b> : ".$getPOData["Delivery_Status_Desc"]."</div>
		<div><b>ยี่ห้อ </b> : ".$getPOData["Make_Name_TH"]." <b>รุ่น : </b>".$getPOData["Model_Desc"]."</div>
		<div><b>รถทะเบียน </b> : ".$getPOData["Plate_No"]." ".$getPOData["Province_Name_TH"]."</div>
	</div>";
	exit();

}elseif ($_POST["action"] == "AddCencalPO") {
	// echo "<pre>".print_r($_POST,1)."</pre>";
	echo addPurchaseOrderCancel($_POST);
	exit();

}elseif ($_POST["action"] == "submitDeleteList") {

		$sql= "DELETE Purchase_Order_Cancel WHERE PO_ID = '".$_POST["poid"]."' ";	
		echo $stmt = sqlsrv_query( $connMS, $sql);
		exit();


}elseif ($_POST["action"] == "sendMailInsurCancel") {
	// echo "<pre>".print_r($_POST,1)."</pre>";
	$mailInsur  = mailInsCancel($_POST, $_FILES);
  if($mailInsur){
  	$sql= "UPDATE Purchase_Order_Cancel SET Status_Emai_Date = '".date("Y-m-d H:i:s")."', Send_Email_by = '".$_SESSION["User"]['UserCode']."'  WHERE PO_ID = '".$_POST["po_code"]."' ";	
		$stmtFollowup = sqlsrv_query( $connMS, $sql);

		echo "<script>alert('ส่งอีเมล์เรียบร้อย กรุณาเช็คอีเมล์อีกครั้งเพื่อความถูกต้อง');</script>";
		echo '<META http-equiv="refresh" content="0;URL=../cancelpolicy.php">';
		exit();
	}else{
		echo "<script>alert('ไม่สามารถส่งเมล์ได้ กรุณาลองใหม่');</script>";
		echo '<META http-equiv="refresh" content="0;URL=../cancelpolicy_email.php?poid='.$_POST["po_code"].'">';
		exit();
	}
	
	exit();

}elseif ($_POST["action"] == "submitCancelINS") {

	$sqlupDate = "UPDATE [dbo].[Purchase_Order] SET [Status_ID] = 'ERQ', [Update_Date] = '".date("Y-m-d H:i:s")."', [Update_By] = '".$UserCode."' WHERE PO_ID = '".$_POST["poid"]."' ";	
		$stmtup1 = sqlsrv_query( $connMS, $sqlupDate);

	$sqlupDateCancal = "UPDATE [dbo].[Purchase_Order_Cancel] SET [Status_Emai_Date] = '".date("Y-m-d H:i:s")."', [Send_Email_by] = '".$UserCode."' WHERE PO_ID = '".$_POST["poid"]."' ";	

	echo $stmtup2 = sqlsrv_query( $connMS, $sqlupDateCancal);
	if($stmtup2){
		$textLine = 'แจ้งยกเลิกกรมธรรม์ \n'.$_POST["poid"].'\nกรุณาตรวจสอบความถูกต้อง\nรบกวนขอคืนกรมธรรม์จากลูกค้าเพื่อนำส่งบริษัทประกันในระยะเวลาที่กำหนด\nวันที่สร้าง '.date('Y-m-d H:i:s');
		curlLineNoti($_POST["empid"], $textLine);
	}
	exit();

}elseif ($_POST["action"] == "upDateTimeINS") {
	if($_POST["Customer_sendto_date"] || $_POST["Broker_sendto_date"]){

		if($_POST["caseType"] == 1){
			$sqlupDateCancal = "UPDATE [dbo].[Purchase_Order_Cancel] SET [Customer_sendto_date] = '".date("Y-m-d H:i:s")."', [Customer_sendto_date_by] = '".$UserCode."' WHERE PO_ID = '".$_POST["poid"]."' ";	

		}else if($_POST["caseType"] == 2){
			$sqlupDateCancal = "UPDATE [dbo].[Purchase_Order_Cancel] SET [Broker_sendto_date] = '".date("Y-m-d H:i:s")."', [Broker_sendto_date_by] = '".$UserCode."' WHERE PO_ID = '".$_POST["poid"]."' ";	
		}
		$stmtup2 = sqlsrv_query( $connMS, $sqlupDateCancal);
		echo "<script>alert('ทำรายการสำเร็จ');</script>";
		echo '<META http-equiv="refresh" content="0;URL=../cancelpolicy.php">';
		exit();
	}else{
		echo "<script>alert('ไม่สามารถทำรายได้การกรุณาลองใหม่');</script>";
		echo '<META http-equiv="refresh" content="0;URL=../cancelpolicy.php">';
		exit();
	}
	exit();

}elseif ($_POST["action"] == "confrimEndorse") {

	$sql = "UPDATE [dbo].[Purchase_Order_Cancel] SET [Endorse] = 'Y', [Endorse_date] = '".date("Y-m-d H:i:s")."', [Endorse_by] = '".$UserCode."' WHERE PO_ID = '".$_POST["poid"]."' ";	
	echo $stmtup2 = sqlsrv_query( $connMS, $sql);
	
	exit();


}