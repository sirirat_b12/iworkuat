
<?php
// require '../backoffice/init.php';
date_default_timezone_set('Asia/Bangkok');
header('Content-Type: text/html; charset=utf-8');

$strExcelFileName = "reportCancelPolicy".date('YmdHis').".xls";

header("Content-Type: application/vnd.ms-excel; name=\"$strExcelFileName\"");
header("Content-Disposition: inline; filename=\"$strExcelFileName\"");
header("Pragma:no-cache");

include "../inc_config.php"; 
include "inc_function_inscancel.php"; 
$getPOCancelReport = getPOCancelReport($_GET['txtse']);

// echo "<pre>".print_r($getPOCancelReport,1)."</pre>";

?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>
    <div id="SiXhEaD_Excel" align=center x:publishsource="Excel">
        <table x:str border=1 cellpadding=1 cellspacing=1 width=100% style="border-collapse:collapse">
            <tr>
                <th valign="middle">PO</th>
                <th class="t_c">เริ่มคุ้มครอง</th>
                <th class="t_c">สิ้นคุ้มครอง</th>
                <th class="t_c">เวลาทำรายการ</th>
                <th class="t_c">รับกรมลูกค้า</th>   
                <th class="t_c">แจ้งยกเลิก</th>
                <th class="t_c">ส่งกรมบ.ประกัน</th>
                <th class="t_c">รับสลักหลัง</th>
                <!-- <th valign="middle">วันที่ยกเลิก</th> -->
                <!-- <th valign="middle">ผู้แจ้งยกเลิก</th> -->
                <th valign="middle" >ชื่อลูกค้า</th>
                <th valign="middle" >บริษัท</th>
                <th valign="middle" >ประเภทประกัน</th>
                <th valign="middle" >เลขกรมธรรม์</th>
                <th valign="middle" >ยี่ห้อรถ</th>
                <th valign="middle" >รุ่นรถ</th>
                <th valign="middle" >ทะเบียนรถ</th>
                <th valign="middle" >เบี้ยสุทธิ</th>
                <th valign="middle" >ส่วนลด</th>
                <th valign="middle" >เบี้ยรวม</th>
                <th valign="middle" >ลูกค้าชำระ</th>
                <th valign="middle" >งวดชำระ</th>
                <th valign="middle" >ชำระมา</th>
                <th valign="middle" >ผู้ขาย</th>
                <th valign="middle" >เหตุผลยกเลิก</th>
                <th valign="middle" >อื่นๆ</th>
                
            </tr>
            <?php 
                $i = 1;
                foreach ($getPOCancelReport as $key => $objResult) {
                    $Net_Premium = $objResult["Net_Premium"] ? $objResult["Net_Premium"] : $objResult["Compulsory"];  
            ?>
                    <tr>
                        <td valign="middle"><b><?php echo $objResult["PO_ID"]; ?></b></td>
                        <td valign="middle"><b><?php echo $objResult["Coverage_Start_Date"]->format("Y-m-d"); ?></b></td>
                        <td valign="middle"><b><?php echo $objResult["Coverage_End_Date"]->format("Y-m-d"); ?></b></td>
                        <td valign="middle">
                            <div><b><?php echo $objResult["Create_Date"]->format("Y-m-d  H:i:s"); ?></b></div>
                            <div><b><?php echo $objResult["Create_By"]; ?></b></div>
                        </td>
                        <td valign="middle">
                            <div><b><?php echo $objResult["Customer_sendto_date"]->format("Y-m-d  H:i:s"); ?></b></div>
                            <div><b><?php echo $objResult["Customer_sendto_date_by"]; ?></b></div>
                        </td>
                        <td valign="middle">
                            <div><b><?php echo $objResult["Status_Emai_Date"]->format("Y-m-d  H:i:s"); ?></b></div>
                            <div><b><?php echo $objResult["Send_Email_by"]; ?></b></div>
                        </td>
                        <td valign="middle">
                            <?php if($objResult["Broker_sendto_date"]->format("Y-m-d") != "1900-01-01"){ ?>
                                <div><b><?php echo $objResult["Broker_sendto_date"]->format("Y-m-d  H:i:s"); ?></b></div>
                                <div><b><?php echo $objResult["Broker_sendto_date_by"]; ?></b></div>
                            <?php } ?>
                        </td>
                        <td valign="middle">
                            <?php if($objResult["Endorse_date"]->format("Y-m-d") != "1900-01-01"){ ?>
                                <div><b><?php echo $objResult["Endorse_date"]->format("Y-m-d  H:i:s"); ?></b></div>
                                <div><b><?php echo $objResult["Endorse_by"]; ?></b></div>
                            <?php } ?>
                        </td>
                        <td valign="middle"><?php echo $objResult["Customer_FName"]." ".$objResult["Customer_LName"]; ?></td>
                        <td valign="middle"><?php echo $objResult["Insurer_Initials"]; ?></td>
                        <td valign="middle"><?php echo $objResult["Insurance_Name"]; ?></td>
                        <td valign="middle"><?php echo (string)$objResult["Policy_No"]; ?></td>
                        <td valign="middle"><?php echo $objResult["Make_Name_TH"]; ?></td>
                        <td valign="middle"><?php echo $objResult["Model_Desc"]; ?></td>
                        <td valign="middle"><?php echo (string)$objResult["Plate_No"]." ".$objResult["Province_Name_TH"]; ?></td>
                        <td valign="middle"><?php echo number_format($Net_Premium,2); ?></td>
                        <td valign="middle"><?php echo number_format($objResult["Discount"],2); ?></td>
                        <td valign="middle"><b><?php echo number_format($objResult["Premium_After_Disc"],2); ?></b></td>
                        <td valign="middle"><b><?php echo number_format($objResult["SumPayment"],2); ?></b></td>
                        <td valign="middle"><?php echo $objResult["countIns"]; ?></td>
                        <td valign="middle"><?php echo $objResult["countPayment"]; ?></td>
                        <td valign="middle"><?php echo $objResult["User_FName"]." ".$objResult["User_LName"]; ?></td>
                        <td valign="middle"><b><?php echo $objResult["Cancel_Status"]; ?></b></td>
                        <td valign="middle"><?php echo $objResult["Remark"]; ?></td>
                    </tr>
                    <?php
                }
            ?>
        </table>
    </div>
    <script>
        window.onbeforeunload = function(){return false;};
        setTimeout(function(){window.close();}, 10000);
    </script>
</body>
</html>
