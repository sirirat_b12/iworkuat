<?php
session_start();
ini_set('post_max_size', '512M');
ini_set('upload_max_filesize', '512M');
ini_set("memory_limit","512M");
ini_set('max_execution_time', 300);
date_default_timezone_set("Asia/Bangkok");
header('Content-Type: text/html; charset=utf-8');

include "../inc_config.php"; 
require '../phpmailer6/src/PHPMailer.php';
require '../phpmailer6/src/SMTP.php';
require '../phpmailer6/src/Exception.php';
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

include "../inc_config.php";
include "../include/sms.class.php";
include "../include/inc_function_linenoti.php";
include "inc_function_inscancel.php"; 


$UserCode = $_SESSION["User"]['UserCode'];

if($_POST["action"] == "getPOData"){
	$getPOData = getPOData($_POST["poid"]);
	$getFilePOForCancel = getFilePOForCancel($_POST["poid"]);
	// echo "<pre>".print_r($getFilePOForCancel,1)."</pre>";
	$html = "";
	$html .= "
	<div class='lh20 p20'>
		<div><b>คุณ </b>".$getPOData["Customer_FName"]." ".$getPOData["Customer_LName"]."</div>
		<div><b>สถานะ PO :</b> ".$getPOData["Status_ID"]." | ".$getPOData["Status_Desc"]."</div>
		<div><b>เลขกรมธรรม์ :</b> ".$getPOData["Policy_No"]."</div>
		<div><b>สถานะการจัดส่ง </b> : ".$getPOData["Delivery_Status_Desc"]."</div>
		<div><b>ยี่ห้อ </b> : ".$getPOData["Make_Name_TH"]." <b>รุ่น : </b>".$getPOData["Model_Desc"]."</div>
		<div><b>รถทะเบียน </b> : ".$getPOData["Plate_No"]." ".$getPOData["Province_Name_TH"]."</div>
	</div>";

	if($getFilePOForCancel){
		$html .= "<div class=''>
		<div class='cff2da5'>เอกสารประกอบยกเลิก</div>";
		foreach ($getFilePOForCancel as $key => $value) {
			$pathFile = "../myfile/".$value["pathfile"]."/".$value["file_name"];
			$html .= "<div class='col-md-4'>
				<input type='checkbox' name='checkFileCN' value='".$pathFile."'>
				<a data-magnify='gallery' data-src='' data-group='a' href='".$pathFile."'>
					<img src='".$pathFile."' class='img-responsive rounded'>
				</a>
			</div>";
		}
		$html .= "</div>";
	}


	echo $html;
	exit();
}elseif ($_POST["action"] == "AddCencalPO") {
	// echo "<pre>".print_r($_POST,1)."</pre>";
	if($_POST["chkFile"]){
		addPurchaseOrderCancelFile($_POST["poid"], $_POST["chkFile"]);
	}
	echo addPurchaseOrderCancel($_POST);
	exit();

}elseif ($_POST["action"] == "submitDeleteList") {

		$sqlfile  = "DELETE Purchase_Order_Cancel_File WHERE PO_ID = '".$_POST["poid"]."' ";	
		$stmt = sqlsrv_query( $connMS, $sqlfile);
		
		$sql= "DELETE Purchase_Order_Cancel WHERE PO_ID = '".$_POST["poid"]."' ";	
		echo $stmt = sqlsrv_query( $connMS, $sql);


}elseif ($_POST["action"] == "sendMailInsurCancel") {
	// echo "<pre>".print_r($_POST,1)."</pre>";
	// $mailInsur  = mailInsCancel($_POST, $_FILES);
  if($mailInsur){
  // 	if($_POST["case"] != "Cancel"){
	 //  	$sql= "UPDATE Purchase_Order_Cancel SET Status_Emai_Date = '".date("Y-m-d H:i:s")."', Send_Email_by = '".$_SESSION["User"]['UserCode']."'  WHERE PO_ID = '".$_POST["po_code"]."' ";	
		// 	$stmtFollowup = sqlsrv_query( $connMS, $sql);
		// }
		echo "<script>alert('ส่งอีเมล์เรียบร้อย กรุณาเช็คอีเมล์อีกครั้งเพื่อความถูกต้อง');</script>";
		echo '<META http-equiv="refresh" content="0;URL=../cancelpolicy.php?txtse='.$_POST["po_code"].'">';
		exit();
	}else{
		echo "<script>alert('ไม่สามารถส่งเมล์ได้ กรุณาลองใหม่');</script>";
		echo '<META http-equiv="refresh" content="0;URL=../cancelpolicy_email.php?poid='.$_POST["po_code"].'">';
		exit();
	}
	
	
	exit();

}elseif ($_POST["action"] == "submitCancelINS") {

	$getRowPODetail = getRowPODetail($_POST["poid"]);
	$Properties_Insured = $getRowPODetail["Properties_Insured"]." \nยกเลิกกรมธรม์ โดย ".$UserCode." ".date("Y-m-d H:i:s") ;
	$phone = $getRowPODetail["Tel_No"] ? $getRowPODetail["Tel_No"] : $getRowPODetail["Mobile_No"] ;
	
	$sqlupDate = "UPDATE [dbo].[Purchase_Order] SET [Properties_Insured] = '".$Properties_Insured."', [Status_ID] = 'ERQ', [Update_Date] = '".date("Y-m-d H:i:s")."', [Update_By] = '".$UserCode."' WHERE PO_ID = '".$_POST["poid"]."' ";	
		$stmtup1 = sqlsrv_query( $connMS, $sqlupDate);

	$sqlupDateCancal = "UPDATE [dbo].[Purchase_Order_Cancel] SET [Status_Emai_Date] = '".date("Y-m-d H:i:s")."', [Send_Email_by] = '".$UserCode."' WHERE PO_ID = '".$_POST["poid"]."' ";	

	$stmtup2 = sqlsrv_query( $connMS, $sqlupDateCancal);

	if($stmtup2){

		$textLine = 'แจ้งยกเลิกกรมธรรม์ \n'.$_POST["poid"].'\nกรุณาตรวจสอบความถูกต้อง\nวันที่สร้าง '.date('Y-m-d H:i:s');
		curlLineNoti($_POST["empid"], $textLine);

		$insType = $getRowPODetail["Compulsory"] ? 'พ.ร.บ.' : 'ภาคสมัครใจ' ;
		$txtSMS = 'เรียนเจ้าของรถทะเบียนรถ '.$getRowPODetail["Plate_No"].' บริษัททำการยกเลิกความคุ้มครองกรมธรรม์ '.$insType.' หากท่านชำระเงินหลังได้รับแจ้งข้อความ จะไม่มีผลใดๆต่อความคุ้มครอง ขออภัยในความไม่สะดวก 020892000';
 		echo $getSendSMS = getSendSMS($phone, $txtSMS);

	}
	exit();

}elseif ($_POST["action"] == "upDateTimeINS") {

	if($_POST["Customer_sendto_date"] || $_POST["Broker_sendto_date"]){

		if($_POST["caseType"] == 1){
			$sqlupDateCancal = "UPDATE [dbo].[Purchase_Order_Cancel] SET [Customer_sendto_date] = '".$_POST["Customer_sendto_date"]."', [Customer_sendto_date_by] = '".$UserCode."' WHERE PO_ID = '".$_POST["poid"]."' ";	
			$textLine = 'ทำการรับกรมธรรม์จากลูกค้า \n'.$_POST["poid"].'\nโดย : '.$UserCode.'\nเมื่อวันที่ : '.$_POST["Customer_sendto_date"];

		}else if($_POST["caseType"] == 2){
			$sqlupDateCancal = "UPDATE [dbo].[Purchase_Order_Cancel] SET [Broker_sendto_date] = '".$_POST["Broker_sendto_date"]."', [Broker_sendto_date_by] = '".$UserCode."' WHERE PO_ID = '".$_POST["poid"]."' ";	
			$textLine = 'แอดมินทำการส่งกรมธรรม์ กลับคืนบริษัทประกัน  \n'.$_POST["poid"].'\nโดย : '.$UserCode.'\nส่งคืนวันที่ : '.$_POST["Broker_sendto_date"];
		}
		$stmtup2 = sqlsrv_query( $connMS, $sqlupDateCancal);
		
		if($stmtup2){
			curlLineNoti($_POST["empid"], $textLine);
		}

		echo "<script>alert('ทำรายการสำเร็จ');</script>";
		echo '<META http-equiv="refresh" content="0;URL=../cancelpolicy.php">';
		exit();
	}else{
		echo "<script>alert('ไม่สามารถทำรายได้การกรุณาลองใหม่');</script>";
		echo '<META http-equiv="refresh" content="0;URL=../cancelpolicy.php">';
		exit();
	}
	exit();

}elseif ($_POST["action"] == "confrimEndorse") {
	$sql = "UPDATE [dbo].[Purchase_Order_Cancel] SET [Endorse] = 'Y', [Endorse_date] = '".date("Y-m-d H:i:s")."', [Endorse_by] = '".$UserCode."' WHERE PO_ID = '".$_POST["poid"]."' ";	
	echo $stmtup2 = sqlsrv_query( $connMS, $sql);
	if($stmtup2){
		$textLine = 'ยืนยันการรับสลักหลัง จากบริษัทประกัน \n'.$_POST["poid"].'\nกรุณาตรวจสอบความถูกต้อง\nDate : '.date('Y-m-d H:i:s');
		curlLineNoti($_POST["empid"], $textLine);
	}
	exit();


}elseif ($_POST["action"] == "caseChkRefund") {

	$arrayA1 = array(
	  0 => array('min' => 0, 'max' => 9, 'per' => 72 ),
	  1 => array('min' => 10, 'max' => 19, 'per' => 68 ),
	  2 => array('min' => 20, 'max' => 29, 'per' => 65 ),
	  3 => array('min' => 30, 'max' => 39, 'per' => 63 ),
	  4 => array('min' => 40, 'max' => 49, 'per' => 61 ),
	  5 => array('min' => 50, 'max' => 59, 'per' => 59 ),
	  6 => array('min' => 60, 'max' => 69, 'per' => 56 ),
	  7 => array('min' => 70, 'max' => 79, 'per' => 54 ),
	  8 => array('min' => 80, 'max' => 89, 'per' => 52 ),
	  9 => array('min' => 90, 'max' => 99, 'per' => 50 ),
	  10 => array('min' => 100, 'max' => 109, 'per' => 48 ),
	  11 => array('min' => 110, 'max' => 119, 'per' => 46 ),
	  12 => array('min' => 120, 'max' => 129, 'per' => 44 ),
	  13 => array('min' => 130, 'max' => 139, 'per' => 41 ),
	  14 => array('min' => 140, 'max' => 149, 'per' => 39 ),
	  15 => array('min' => 150, 'max' => 159, 'per' => 37 ),
	  16 => array('min' => 160, 'max' => 169, 'per' => 35 ),
	  17 => array('min' => 170, 'max' => 179, 'per' => 32 ),
	  18 => array('min' => 180, 'max' => 189, 'per' => 30 ),
	  19 => array('min' => 190, 'max' => 199, 'per' => 29 ),
	  20 => array('min' => 200, 'max' => 209, 'per' => 27 ),
	  21 => array('min' => 210, 'max' => 219, 'per' => 25 ),
	  22 => array('min' => 220, 'max' => 229, 'per' => 23 ),
	  23 => array('min' => 230, 'max' => 239, 'per' => 22 ),
	  24 => array('min' => 240, 'max' => 249, 'per' => 20 ),
	  25 => array('min' => 250, 'max' => 259, 'per' => 18 ),
	  26 => array('min' => 260, 'max' => 269, 'per' => 16 ),
	  27 => array('min' => 270, 'max' => 279, 'per' => 15 ),
	  28 => array('min' => 280, 'max' => 289, 'per' => 13 ),
	  29 => array('min' => 290, 'max' => 299, 'per' => 12 ),
	  30 => array('min' => 300, 'max' => 309, 'per' => 10 ),
	  31 => array('min' => 310, 'max' => 319, 'per' => 8 ),
	  32 => array('min' => 320, 'max' => 329, 'per' => 6 ),
	  33 => array('min' => 330, 'max' => 339, 'per' => 4 ),
	  34 => array('min' => 340, 'max' => 349, 'per' => 3 ),
	  35 => array('min' => 350, 'max' => 359, 'per' => 1 ),
	  36 => array('min' => 360, 'max' => 366, 'per' => 0 ),
	);
	$poid = $_POST["poid"];
	$getRowPODetail = getRowPODetail($poid);
	$dateCancel = $_POST["dateCancel"] ? $_POST["dateCancel"] : date("Y-m-d");
	$Coverage_Start_Date = $getRowPODetail["Coverage_Start_Date"]->format("Y-m-d");
	$day = round(DateDiff($Coverage_Start_Date, $dateCancel));
	$keyID = getKeyIDFromDay($day);
	$getIntallmentPayment = getIntallmentPayment($poid);
	// echo "<pre>".print_r($getIntallmentPayment,1)."</pre>";

	$html = "";
	if($day >= 0){
		$html .= '
				<div class="cff2da5 fwb fs18">วันที่เริ่มคุ้มครอง '.$Coverage_Start_Date.' คุ้มครองไป '.$day.' วัน</div>
				<div class="cff2da5 fwb fs18 mt15">รายการชำระเงิน</div>
				<div class="table-responsive mt15">
					<table class="table table-striped p5 table-hover table-bordered">
						<thead class="fs13 c000000">
							<tr>
								<th class="t_c">งวดที่</th>			
								<th class="t_c">จำนวนเงิน</th>
								<th class="t_c">สถานะ</th>
								<th class="t_c">วันที่ชำระ</th>
								<th class="t_c">RV ID</th>
								<th class="t_c">ชำระเงิน</th>
								<th class="t_c">ค่าธรรมเนียม</th>
								<th class="t_c">หมายเหตุ</th>	
							</tr>
						</thead>
						<tbody class="fs12">';
								$sumcost = 0;
								foreach ($getIntallmentPayment as $key => $value) {
									$sumcost += $value["Amount"];
									$sumReceiveAmount += $value["Receive_Amount"];
									$Operation_Cost_Desc = ($value["Operation_Cost_Desc"]) ? $value["Operation_Cost_Desc"] : "-";
									$Receive_Date = ($value["Receive_Date"]) ? $value["Receive_Date"]->format("Y-m-d") : "-";
						$html .= '
								<tr>
									<td class="t_c">'.$value["Installment_ID"].'</td>			
									<td class="t_c">'.number_format($value["ISTM_Total_Amount"], 2).'</td>
									<td class="t_c">'.$value["Installment_Status"].'</td>
									<td class="t_c">'.$Receive_Date.'</td>
									<td class="t_c">'.$value["Receive_ID"].'</td>	
									<td class="t_c">'.number_format($value["Receive_Amount"], 2).'</td>
									<td class="t_c">'.number_format($value["Amount"], 2).'</td>
									<td class="t_c">'.$Operation_Cost_Desc.'</td>	
								</tr>';
								}
						$html .= '
							<tr>
									<td class="t_r fwb" colspan="5">รวม</td>		
									<td class="t_c fwb">'.number_format($sumReceiveAmount, 2).'</td>	
									<td class="t_c fwb">'.number_format($sumcost, 2).'</td>
									<td class="t_c">-</td>	
								</tr>
						</tbody>
					</table>
				</div>
		';

		$html .= '
			<div class="cff2da5 fwb fs18">รายการคืนเงิน</div>
			<div class="table-responsive mt15">
				<table class="table table-striped p5 table-hover table-bordered">
					<thead class="fs13 c000000">';
					if($_SESSION["User"]['type'] == "Sale"){
						$html .= ' <tr>
							<th class="t_c">#</th>			
							<th class="t_c">ช่วงเวลา(วัน)</th>
							<th class="t_c">ช่วงวัน</th>
							<th class="t_c">% คืนเงิน</th>
						</tr>';
					}else{
						$html .= ' <tr>
							<th class="t_c">#</th>			
							<th class="t_c">ช่วงเวลา</th>
							<th class="t_c">ช่วงวัน</th>
							<th class="t_c">% คืนเงิน</th>
							<th class="t_c">เบี้ยสุทธิ์</th>
							<th class="t_c">เบี้ยประกันคืน</th>
							<th class="t_c">ส่วนลด</th>							
							<th class="t_c">หักธรรมเนียม+ส่วนสด</th>
							<th class="t_c">ลูกค้าชำระ</th>
							<th class="t_c">เงินคืน</th>
						</tr>';
					}
				$html .= ' </thead>
					<tbody class="fs12">';
							$num = 1; 
							$NetPremium = ($getRowPODetail["Net_Premium"]) ? $getRowPODetail["Net_Premium"] : $getRowPODetail["Compulsory"];
							$Discount = $getRowPODetail["Discount"];
							$Total_Premium = $NetPremium + $getRowPODetail["Duty"] + $getRowPODetail["Tax"];
							$payment = getSumPayment($poid);
							
							for ($i=$keyID; $i <= ($keyID+5); $i++) {
								$MinDayTime = $day - $arrayA1[$i]["min"];
								$MaxDayTime = $arrayA1[$i]["max"] - $day;
								$per = $arrayA1[$i]["per"] / 100;
								$Vat = ($NetPremium * $per) * 0.07;
								$sumcost = ($getRowPODetail["Net_Premium"]) ? $sumcost : 0;
								$Fee = $Discount + $sumcost;
								$LastRefund = (($NetPremium * $per) + $Vat);
								$refund = $LastRefund - $Fee;
								$refundCusPay = $payment - (($Total_Premium - $Discount) - $refund );
								$dateStart = date('d/m/Y',strtotime($dateNow . "-".$MinDayTime." days"));
								$dateEnd = date('d/m/Y',strtotime($dateNow . "+".$MaxDayTime." days"));
						if($_SESSION["User"]['type'] == "Sale"){
							 $html .= '
								<tr>
									<td class="t_c">'.$num++.'</td>			
									<td class="t_c">'.$arrayA1[$i]["min"]."-".$arrayA1[$i]["max"].'</td>
									<td class="t_c c2457ff ">
										<div>'.$dateStart.' - '.$dateStart.'</div>
									</td>
									<td class="t_c cff2da5">'.$arrayA1[$i]["per"].'%</td>
								</tr> ';
						}else{
							$html .= '
								<tr>
									<td class="t_c">'.$num++.'</td>			
									<td class="t_c">'.$arrayA1[$i]["min"]."-".$arrayA1[$i]["max"].'</td>
									<td class="t_c c2457ff ">
										<div>'.$dateStart.'</div>
										<div>'.$dateEnd.'</div>
									</td>
									<td class="t_c cff2da5">'.$arrayA1[$i]["per"].'%</td>
									<td class="t_c c00ac0a">'.number_format($NetPremium, 2).'</td>
									<td class="t_c">'.number_format($LastRefund, 2).'</td>
									
									<td class="t_c">'.number_format($Discount, 2).'</td>							
									<td class="t_c c9c00c8 ">'.number_format($refund, 2).'</td>
									<td class="t_c">'.number_format($payment, 2).'</td>
									<td class="t_c fwb cff2da5 ">'.number_format($refundCusPay, 2).'</td>
								</tr> ';
								}
						}
						$html .= '
					</tbody>
				</table>
				<p class="t_c cff2da5 fwb">ตารางดังกล่าวเป็นการแสดงผลประเมินราคาเบื้องต้น <br> ตัวเลขที่แท้จริงต้องรอสลักหลังจากบริษัทประกัน เพื่อยืนยันราคาอีกครั้ง</p>
			</div>
		';
	}else{
		$html .= "<div class='fwb fs18 t_c cff8000'>กรมธรรม์ยังไม่ได้รับการคุ้มครองจากบริษัทประกัน</div>";
	}

	echo $html;

}elseif ($_POST["action"] == "getPOCancelFile") {
	$getPurchaseOrderCancelFiles = getPurchaseOrderCancelFiles($_POST["poid"]);
	if($getPurchaseOrderCancelFiles){
		$html .= "<div class='row'>";
			foreach ($getPurchaseOrderCancelFiles as $key => $value) {
				$pathFile = "../myfile/".$value["pathfile"]."/".$value["file_name"];
				$html .= "<div class='col-md-3'>
					<a data-magnify='gallery' data-src='' data-group='a' href='".$value["PO_Cancel_file"]."'>
						<img src='".$value["PO_Cancel_file"]."' class='img-responsive rounded' style='width: 50%;'>
					</a>
				</div>";
			}
			$html .= "</div>";
		echo $html;
	}else{
		echo "<spqn class='fs20 fwb t_c'>ไม่พบไฟล์ประกอบการยกเลิกกรมธรรม์</spqn>";
	}
	exit();

}
