<?php
session_start();
date_default_timezone_set("Asia/Bangkok");
header('Content-Type: text/html; charset=utf-8');

include "../inc_config.php";
include "../include/inc_function_chk.php"; 

if($_POST["action"] == "searchCustomers"){
  $resultarray = array();
	$tbx = trim($_POST["tbx"]);
	$caseMs = $_POST["caseMs"];
	$sql = "SELECT Compare_Policy.CP_PO_ID, Compare_Policy.CP_Date, Compare_Policy.CP_ID, Compare_Policy.Compare_Status_ID, Compare_Policy.Active, Customer.Customer_ID, Customer.Customer_FName, Customer.Customer_LName, Customer.Tel_No, My_User.User_FName, My_User.User_LName,Customer.Mobile_No
  FROM [dbo].[Compare_Policy] 
  INNER JOIN [dbo].[Customer] ON Compare_Policy.Customer_ID = Customer.Customer_ID  
  INNER JOIN [dbo].My_User ON Compare_Policy.Employee_ID = My_User.User_ID WHERE ";
	if($caseMs == "ContactNo"){
		// echo $sql = "SELECT *  FROM [dbo].[Customer] WHERE  Mobile_No = '".$tbx."' OR Tel_No = '".$tbx."'  ";
		$sql .= "Customer.Mobile_No = '".$tbx."' OR Customer.Tel_No = '".$tbx."'  ";
	}else if ($caseMs == "CustomerName") {
		$sql .= " Customer_FName LIKE '%".$tbx."%' ";
	}else if ($caseMs == "CPID") {
		$sql .= " Compare_Policy.CP_ID = '".$tbx."' ";
	}
  // echo $sql;
	$stmt = sqlsrv_query( $connMS, $sql );
	if(sqlsrv_has_rows($stmt)) {
			while( $row = sqlsrv_fetch_array($stmt) ) {	
				$resultarray[] = $row;
			}
			echo json_encode($resultarray);
			exit();
	}

// }else if($_POST["action"] == "searchPO"){
//   $pocode = trim($_POST["pocode"]);
//   $resultarray = array();
//   $sql = "SELECT 
//   Purchase_Order.PO_ID, Purchase_Order.Coverage_Start_Date, Purchase_Order.Capital, Purchase_Order.Insurance_Package_ID, Purchase_Order.Compulsory, Purchase_Order.Net_Premium, Purchase_Order.Discount, Purchase_Order.Total_Premium, Purchase_Order.Premium_After_Disc, Insurance_Package.Insurance_Package_Name, Purchase_Order.Marine_Reference, ADB_Premium.Premium_Desc, Insurance.Insurance_Name, Customer.Customer_Type AS cus_type, Title.Title_Name AS Title_Name, Customer.Customer_FName AS FName, Customer.Customer_LName AS LName, Insurer.Insurer_Initials AS Insurer_Initials,
//   Delivery_Type.Delivery_Type_Desc
//   FROM [dbo].[Purchase_Order] INNER JOIN [dbo].[Customer] ON Purchase_Order.Customer_ID = Customer.Customer_ID 
//   INNER JOIN [dbo].[Purchase_Order_Insurer] ON Purchase_Order.PO_ID = Purchase_Order_Insurer.PO_ID 
//   INNER JOIN [dbo].[Insurance] ON Purchase_Order.Insurance_ID = Insurance.Insurance_ID
//   INNER JOIN [dbo].[Insurer] ON Purchase_Order_Insurer.Insurer_ID = Insurer.Insurer_ID
//   LEFT JOIN [dbo].[Title] ON Customer.Customer_Title  = Title.Title_ID 
//   LEFT JOIN [dbo].[Insurance_Package] ON Purchase_Order.Insurance_Package_ID = Insurance_Package.Insurance_Package_ID
//   LEFT JOIN [dbo].[ADB_Premium] ON Purchase_Order.Premium_ID = ADB_Premium.Premium_ID 
//   LEFT JOIN [dbo].[Policy_Delivery] ON Purchase_Order.PO_ID = Policy_Delivery.PO_ID
//   LEFT JOIN [dbo].[Delivery_Type] ON Policy_Delivery.Delivery_Type_ID = Delivery_Type.Delivery_Type_ID
//   WHERE Purchase_Order.PO_ID = '".$pocode."' ";
//   $stmt = sqlsrv_query( $connMS, $sql );
//   if(sqlsrv_has_rows($stmt)) {
//       while( $row = sqlsrv_fetch_array($stmt) ) {
//         // echo strtotime($row["Coverage_Start_Date"]);
//         // echo "<pre>".print_r($row["Coverage_Start_Date"],1)."</pre>";
//         $resultarray[] = $row;
//       }
//       echo json_encode($resultarray);
//   // echo "<pre>".print_r($resultarray,1)."</pre>";
//    // $Package =  getInsurancePackageBYID($resultarray["Insurance_Package_ID"]);
//    // echo "<pre>".print_r($Package,1)."</pre>";
//       exit();
//   }
//   exit();


}else if($_POST["action"] == "searchPO"){

  $pocode = trim($_POST["pocode"]);
  $resultarray = array();
  $sql = "SELECT 
  Purchase_Order.PO_ID, Purchase_Order.Coverage_Start_Date, Purchase_Order.Capital, Purchase_Order.Insurance_Package_ID, Purchase_Order.Compulsory, Purchase_Order.Net_Premium, Purchase_Order.Discount, Purchase_Order.Total_Premium, Purchase_Order.Premium_After_Disc, Insurance_Package.Insurance_Package_Name, Purchase_Order.Marine_Reference, ADB_Premium.Premium_Desc, Insurance.Insurance_Name, Customer.Customer_Type AS cus_type, Title.Title_Name AS Title_Name, Customer.Customer_FName AS FName, Customer.Customer_LName AS LName, Insurer.Insurer_Initials AS Insurer_Initials,
  Delivery_Type.Delivery_Type_Desc, Insurer.Insurer_Name
  FROM [dbo].[Purchase_Order] INNER JOIN [dbo].[Customer] ON Purchase_Order.Customer_ID = Customer.Customer_ID 
  LEFT JOIN [dbo].[Purchase_Order_Insurer] ON Purchase_Order.PO_ID = Purchase_Order_Insurer.PO_ID 
  LEFT JOIN [dbo].[Insurance] ON Purchase_Order.Insurance_ID = Insurance.Insurance_ID
  LEFT JOIN [dbo].[Insurer] ON Purchase_Order_Insurer.Insurer_ID = Insurer.Insurer_ID
  LEFT JOIN [dbo].[Title] ON Customer.Customer_Title  = Title.Title_ID 
  LEFT JOIN [dbo].[Insurance_Package] ON Purchase_Order.Insurance_Package_ID = Insurance_Package.Insurance_Package_ID
  LEFT JOIN [dbo].[ADB_Premium] ON Purchase_Order.Premium_ID = ADB_Premium.Premium_ID 
  LEFT JOIN [dbo].[Policy_Delivery] ON Purchase_Order.PO_ID = Policy_Delivery.PO_ID
  LEFT JOIN [dbo].[Delivery_Type] ON Policy_Delivery.Delivery_Type_ID = Delivery_Type.Delivery_Type_ID";

  if($_POST["search_type"] == 1){
    $sql .= " WHERE Purchase_Order.PO_ID = '".$pocode."' ";
  }else{
    $sql .= " WHERE Purchase_Order.Customer_ID = '".$pocode."' ORDER BY Purchase_Order.Create_Date DESC";
  }
  // echo $sql;
  $stmt = sqlsrv_query( $connMS, $sql );
  if(sqlsrv_has_rows($stmt)) {
      while( $row = sqlsrv_fetch_array($stmt) ) {
        $resultarray[] = $row;
      }
      echo json_encode($resultarray);
      exit();
  }
  exit();

  
}else if($_POST["action"] == "getDatePO"){
  $resultarray = array();
  $datestart = date("Y")."-".$_POST["mounth"]."-01";
  if($_POST["mounth"] == date("n")){
    $dateNow = date("Y-m-d");
  }else{
     $dateNow = date("Y")."-".$_POST["mounth"]."-".date('t',strtotime(date("Y")."-".$_POST["mounth"]));
  }
  $user = $_POST["code"];
  $sql = "SELECT CAST([dbo].[Purchase_Order].[PO_Date] AS DATE) As PO_Date
  FROM [dbo].[Purchase_Order]
  WHERE PO_Date >= '".$datestart."' AND PO_Date <= '".$dateNow."' AND  Active = 'Y' ";
  if($_SESSION["User"]['type'] != "SuperAdmin"){
    $sql .= " AND Employee_ID >= '".$user."' "; 
  }  
  $sql .= " GROUP BY CAST([dbo].[Purchase_Order].[PO_Date] AS DATE) ORDER BY PO_Date ASC "; 
  // echo $sql;
  // echo "<pre>".print_r($make,1)."</pre>";
  $stmt = sqlsrv_query( $connMS, $sql );
  if(sqlsrv_has_rows($stmt)) {
    $i = 0;
     while( $row = sqlsrv_fetch_array($stmt) ) { 
     $day = $row["PO_Date"]->format("Y-m-d");
      $count = getCountPOAllDate($day, $user);
      $resultarray[$i]["PO_Date"] = $row["PO_Date"];
       $resultarray[$i]["count"] = $count["count"] ? $count["count"] : 0;
      $resultarray[$i]["sumTotal"] = $count["sumTotal"] ? $count["sumTotal"] : 0 ;
      $i++;
      // array_push($resultarray[],$count);
      // $resultarray[]["count"] = $count;
    }
    echo json_encode($resultarray);
    exit();
  }
  exit();

}else if($_POST["action"] == "txtAccessory"){

  $sqlPO = "UPDATE [dbo].[Car_Detail] SET [Accessory] = '".$_POST["txtAccessory"]."' WHERE PO_ID = '".$_POST["txtpoid"]."' ";
  $stmt1 = sqlsrv_query( $connMS, $sqlPO);

  $txtDetail = $_POST["txtpoid"]." | Old: ".$_POST["txtAccessoryOld"]." | New: ".$_POST["txtAccessory"];
  $sqlLog = "INSERT INTO [dbo].[User_Log]
           ([User_ID],[Log_Date_Time],[From_IP],[Operation],[Operation_Data],[Remark],[Active],[Create_Date],[Create_By],[Update_Date],[Update_By])
     VALUES
           ('".$_SESSION["User"]['UserCode']."','".date("Y-m-d H:i:s")."','".$_SERVER['REMOTE_HOST']."','UpDate Accessory','".$txtDetail."','From ADBCHK','Y','".date("Y-m-d H:i:s")."','".$_SESSION["User"]['UserCode']."','".date("Y-m-d H:i:s")."','".$_SESSION["User"]['UserCode']."')";
  $stmt2 = sqlsrv_query( $connMS, $sqlLog);


  if($stmt1){
    echo 1;
  }else{
    echo 0;
  }
  exit();

}
?>