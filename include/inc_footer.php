
<div class="modal fade  bs-example-modal-lg" id="userProfile" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
<div class="modal-dialog modal-lg" role="document">
	<div class="modal-content">
		<div class="modal-header text-c">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">ข้อมูลพนักงาน ตำแหน่ง <?php echo $userProfile["personnel_type"]; ?></h4>
		</div>
		<div class="modal-body">
			<form action="include/inc_action.php" method="POST"  id="listProfile" name="listProfile" >
				<input type="hidden" name="action"  value="updateProfile">
				<input type="hidden" name="userid" id="userid" value="<?php echo $userProfile["personnel_id"]; ?>">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label class="fwn fs12">รหัสพนักงาน</label>
							<input class="form-control formInput2" type="text" name="personnel_code"  value="<?php echo $userProfile["personnel_code"]; ?>" disabled>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="fwn fs12">รหัสเข้าระบบ</label>
							<input class="form-control formInput2" type="password" name="personnel_password" id="personnel_password" value="<?php echo $userProfile["personnel_password"]; ?>" >
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="fwn fs12">ชื่อ</label>
							<input class="form-control formInput2" type="text" name="personnel_firstname" id="personnel_firstname" value="<?php echo $userProfile["personnel_firstname"]; ?>" >
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="fwn fs12">นามสกุล</label>
							<input class="form-control formInput2" type="text" name="personnel_lastname" id="personnel_lastname" value="<?php echo $userProfile["personnel_lastname"]; ?>" >
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="fwn fs12">เบอร์</label>
							<input class="form-control formInput2" type="text" name="personnel_phone" id="personnel_phone" value="<?php echo $userProfile["personnel_phone"]; ?>" >
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="fwn fs12">อีเมล์</label>
							<input class="form-control formInput2" type="text" name="personnel_email" id="personnel_email" value="<?php echo $userProfile["personnel_email"]; ?>" >
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="fwn fs12">ไลน์</label>
							<input class="form-control formInput2" type="text" name="personnel_line" id="personnel_line" value="<?php echo $userProfile["personnel_line"]; ?>" >
						</div>
					</div>
				</div>
				<div class="t_c"><button type="submit" class="btn btn-info btnsendPOemail"><i class="fa fa-check-circle"></i> แก้ไขข้อมูล</button></div>
			</form>
		</div>
		<div class="modal-footer"><button type="button" class="btn btn-default" data-dismiss="modal">Close</button></div>

	</div>
</div>
</div>

<div class="modal fade  bs-example-modal-lg" id="sendPOCustomer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
<div class="modal-dialog modal-lg" role="document">
	<div class="modal-content">
		<div class="modal-header text-c">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">รายละเอียดข้อมูลประกันภัย</h4>
		</div>
		<div class="modal-body">
			<form action="include/inc_action.php" method="POST" >
				<input type="hidden" name="action" value="sendListsProduct" />
				<input type="hidden" name="casenew"  value="casenew" />
				<input type="hidden" name="make" id="call_make" />
				<input type="hidden" name="model" id="call_model" />
				<input type="hidden" name="cc" id="call_cc" />
				<input type="hidden" name="year" id="call_year" />
				<input type="hidden" name="class" id="call_class" />
				<input type="hidden" name="repair" id="call_repair" />
				<p class="fs16 t_c">กรุณากรอกข้อมูลลูกค้า</p>
				<div class="row">
					<div class="col-md-12 t_c">
						<div class="form-group">
							<input type="radio" name="callcase" value="1" checked/> ส่งเข้าระบบ
							<input type="radio" name="callcase" value="0" /> ลูกค้าตัวเอง 
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<input class="form-control formInput2" type="text" name="name" id="name" placeholder="ชื่อ-นามสกุล*" required >
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<input class="form-control formInput2" type="email" name="email" id="email" placeholder="อีเมล์"  >
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<input class="form-control formInput2" type="" name="phone" id="phone" placeholder="เบอร์โทรศัพท์*" maxlength="10" onkeyup="chknum(this,event)" required >
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<input class="form-control formInput2" type="text" name="line" id="line" placeholder="ไลน์" >
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<select id="newfrom" class="form-control formInput2 formInput" name="newfrom" required>
				                <option value="AsiaDirect">ไม่สามารถระบุได้</option>
				                <option value="CallCenter">CallCenter</option>
				                <option value="Line">Line</option>
				                <option value="Interspace">Interspace</option>
				                <option value="ไลน์พอยท์">LinePoint</option>
				                <option value="Google">Google</option>
				                <option value="WSM-walkIn">WSM-walkIn</option>
				                <option value="Facebook">Facebook</option>
				                <option value="FacebookBOT">Facebook BOT</option>
				                <option value="Other Internet web">เว็บไซต์อื่นๆ</option>
				                <option value="Brochure">โบว์ชัวร์</option>
				                <option value="Recommend">ผู้อื่นแนะนำ</option>
				                <option value="Claim">เคลมประกัน</option>
				                <option value="Internet Chat">Internet Chat</option>
				                <option value="CHeroleads">CHeroleads</option>
				                <option value="PopAds2000">PopAds2000</option>
				                <option value="งานพรบ.ออนไลน์ D100804">งานพรบ.ออนไลน์ D100804</option>
				                                
				            </select>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<textarea class="form-control formInput2" rows="1" name="remark" placeholder="หมายเหตุ" ></textarea>
						</div>
					</div>
				</div>
				<div class="t_c"><button type="submit" class="btn btn-success btnsendPOemail"><i class="fa fa-check-circle"></i> ส่งข้อเสนอ</button></div>
			</form>
			<!-- <div class="boxProtect"><div id="showid"></div></div>  -->

		</div>
		<div class="modal-footer"><button type="button" class="btn btn-default" data-dismiss="modal">Close</button></div>

	</div>
</div>
</div>


<div class="modal fade  bs-example-modal-lg" id="chkCustomerModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
<div class="modal-dialog modal-xl" role="document">
	<div class="modal-content">
		<div class="modal-header text-c">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">เช็คข้อมูลลูกค้า</h4>
		</div>
		<div class="modal-body">
			<form action="include/inc_action.php" method="POST" >
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<select id="case_ms" class="form-control formInput2 formInput" name="case_ms" required>
	              <option value="ContactNo">หมายเลขติดต่อ</option>
	              <option value="CPID">รหัสข้อเสนอ</option>
	              <option value="CustomerName">ชื่อลูกค้า</option>
		          </select>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<input class="form-control formInput2" type="text" name="tbxDefaultSearch" id="tbxDefaultSearch"  required>
						</div>
					</div>
				</div>
				<div class="t_c"><button type="button" id="chksendms" class="btn btn-success "><i class="fa fa-search-plus" aria-hidden="true"></i> เช็คข้อมูล</button></div>
			</form>
		<div style=" border-top: 1px solid #ff8000;margin-top: 10px;">
			<!-- <div id="listChk"></div> -->
			<table class="table table-striped" id="tableListChk">
				<thead>
					<tr>
						<td class="t_c">CP</td>
						<td class="t_c">PO</td>
						<td>วันที่ออกข้อเสนอ</td>
						<td>รหัสลูค้า</td>
						<td>ชื่อ-นามสกุล</td>
						<td>เบอร์โทร</td>
						<td class="t_c">พนักงาน</td>
						<td class="t_c">สถานะ</td>
						<td class="t_c">การติดต่อ</td>
					</tr>
				</thead>
				<tbody id="listChk" class="table-list">
					<tr>
						<td colspan="9" class="text-center">กรุณาเลือกข้อมูล</td>
					</tr>
			</table>
		</div>
		</div>
		<div class="modal-footer"><button type="button" class="btn btn-default" data-dismiss="modal">Close</button></div>

	</div>
</div>
</div>

<!-- tor_mmodal begin -->
<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true"  id="tor_mmodal">
	<div class="modal-dialog modal-lg" style="width:1250px;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Modal title</h4>
			</div>
			<div class="modal-body">
				<iframe src="https://adbwecare.com/omise/link_create/index.php" width="100%" 
				height="880" frameborder="0" 
				sandbox="allow-same-origin allow-scripts allow-popups allow-forms allow-top-navigation"
				allowtransparency="true"></iframe>
			</div>			
		</div>
	</div>
</div>
<!-- tor_mmodal end -->

<div class="modal fade  " id="addNotice" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
<div class="modal-dialog " role="document">
	<div class="modal-content">
		<div class="modal-header text-c">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">เพิ่มประกาศ</h4>
		</div>
		<div class="modal-body">
			<form action="include/inc_action.php" method="POST" >
				<input type="hidden" name="action" value="addNotice">
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<textarea class="form-control" id="editorAddNotice" name="notice_detail"></textarea>
						</div>
					</div>
				</div>
				<div class="t_c">
					<input type="submit" value="บันทึก" class="btn btn-success">
				</div>
			</form>
			<div class="modal-footer"><button type="button" class="btn btn-default" data-dismiss="modal">Close</button></div>

		</div>
	</div>
</div>
</div>
</body>

</html>
<!-- Javascript -->
<script src="assets/vendor/jquery/jquery.min.js"></script>
<script src="assets/vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- <script src="assets/vendor/jquery.easy-pie-chart/jquery.easypiechart.min.js"></script> -->
<!-- <script src="assets/vendor/chartist/js/chartist.min.js"></script> -->
<script src="assets/scripts/klorofil-common.js"></script>
<script src="table_js/dist/bootstrap-table.min.js"></script>	
<script src="table_js/dist/locale/bootstrap-table-th-TH.min.js"></script>
<script>
CKEDITOR.replace( 'editorAddNotice', {
language: 'th',
// extraPlugins: 'uploadimage'
height: 250
});
</script>
<script type="text/javascript">

$(document).ready(function() {
chkNumnoti();
setInterval("chkNumnoti()", 60000);
});
function chkNumnoti(){
	var code = "<?php echo $_SESSION["User"]['UserCode']; ?>";
$.ajax({ 
  url:'include/inc_action.php',
  type:'POST',
  async:true,
  data: {action: 'chkNumnoti', code:code},
  success:function(getData){
  	// console.log(getData);
  	if(getData){
  		$(".badge").show();
      $(".badge").html(getData);
    }
  }
});
}
function notificationTime(code){
	$.ajax({ 
		url: 'include/inc_action.php',
		type:'POST',
	async:true,
		data: {action: 'getNtificationTime', code:code},
		success:function(rs){
			// console.log(rs);
			$(".badge").hide();
			$("#liNoti").html(rs);
			// if(rs == 1){
			// 	window.location.href = "login.php"; 
			// }else{
			// 	alert("พบปัญหา กรุณาลองใหม่อีกครั้ง");
			// }
		}
	}); 
}

function logout(){
	if(confirm("ยืนยันการออกจากระบบ")){
		// console.log("logout");
		var id = $(this).data('id');
	    var price = $(this).attr('data-info');
	    $.ajax({ 
			url: 'include/inc_action.php',
			type:'POST',
			async:true,
			data: {action: 'logout'},
			success:function(rs){
				// console.log(rs);
				if(rs == 1){
					window.location.href = "login.php"; 
				}else{
					alert("พบปัญหา กรุณาลองใหม่อีกครั้ง");
				}
			}
		}); 
			}
}
function chknum(ip,ek) {
	if(!isNaN(ip.value)){
		return true;
	}
	ip.value="";
	alert('กรุณากรอกเป็นหมายเลขเท่านั้น');
	return true;
}
function numCommas(x) {
	if(x){
    	return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }else{
    	return 0.00;
    }
}

$(".btnsearchCustomer").click(function() {
    $('#searchCustomer').modal('show'); 
});
$(".btnUser").click(function() {
    $('#userProfile').modal('show'); 
});
$(".chkCustomer").click(function() {
    $('#chkCustomerModel').modal('show'); 
});
$(".btnaddNotice").click(function() {
	console.log("btnaddNotice");
    $('#addNotice').modal('show'); 
});

$(".btnCustomer").click(function() {
	var phone = $("#phoneCus").val(); console.log(phone);
	$.ajax({ 
		url: 'include/inc_action.php',
		type:'POST',
		dataType: 'json',
		data: {action: 'searchCustomers', phone:phone},
		success:function(rs){
			// console.log(rs);
		}
	}); 
});

$("#chksendms").click(function() {
	var caseMs = $("#case_ms").val();
	var tbx = $("#tbxDefaultSearch").val(); 
	// console.log(caseMs);
	// console.log(tbx);
	$("#listChk").empty();
	$.ajax({ 
		url: 'include/inc_action_ms.php',
		type:'POST',
		async:true,
		dataType: 'json',
		data: {action: 'searchCustomers', caseMs:caseMs, tbx:tbx},
		success:function(rs){
			// console.log(rs);
			var num = rs.length;
			var details = "";
			for (var i = 0; i < num; i++){
				var d = new Date(rs[i].CP_Date.date);
				// name += "<br> - "+rs[i].CP_ID+" "+rs[i].Customer_FName+" "+rs[i].Customer_LName;
				details += '<tr>';
				details += '<td style="text-align: center;">'+ rs[i].CP_ID +'</td>';
				details += '<td style="text-align: center;">'+ rs[i].CP_PO_ID +'</td>';
				details += '<td >'+ d.getDate()+"/"+ d.getMonth() +"/"+ d.getFullYear() +'.</td>';
				details += '<td>'+ rs[i].Customer_ID+'.</td>';
				details += '<td>'+ rs[i].Customer_FName+" "+rs[i].Customer_LName+'.</td>';
				details += '<td>'+ rs[i].Tel_No +'.</td>';
				details += '<td style="text-align: center;">'+ rs[i].User_FName +" "+rs[i].User_LName+'.</td>';
				details += '<td style="text-align: center;">'+ statusLead(rs[i].Compare_Status_ID) +'.</td>';
				details += '<td style="text-align: center;">'+ rs[i].Active +'.</td>';
				details += '</tr>';
			}
			$("#listChk").append(details);
		}
	}); 
});

function statusLead(rs){
	if(rs == "IPC"){
		val = "ดำเนินการ";
	}else if(rs == "NEW"){
		val = "ข้อเสนอใหม่";
	}else if(rs == "OPN"){
		val = "เปรียบเทียบ";
	}else if(rs == "POI"){
		val = "ออกใบสั่งซื้อ";
	}else if(rs == "REA"){
		val = "Re-Assign";
	}else if(rs == "REJ"){
		val = "ยกเลิกข้อเสนอ";
	}
	return val;
}


</script>

<style>
#tableListChk { overflow-x: scroll; }
</style>
