<?php
session_start();
ini_set('post_max_size', '256M');
ini_set('upload_max_filesize', '256M');
ini_set("memory_limit","256M");
date_default_timezone_set("Asia/Bangkok");
header('Content-Type: text/html; charset=utf-8');

include "../inc_config.php";
include "../include/inc_function.php"; 
include "../include/inc_function_chk.php"; 

if($_POST["action"] == "sendOrderAdmin"){
	// $getCompanyByID = getCompanyByID($_POST["insuere_company"]);
	

	$getQCcheck = getQCcheck();
	$rand = array_rand($getQCcheck,1);
	$chk_code = $getQCcheck[$rand]["personnel_code"];
	$_POST["chk_code"] = $chk_code;
	$_POST["created_date"] = date("Y-m-d H:i:s");
	$noti_work_code =  'N'.date("ymdis")."".substr($_POST["po_code"], 8, 9);
	$_POST["noti_work_code"] = $noti_work_code;
	$_POST["status"] = 0;

	// echo "<pre>".print_r($_POST,1)."</pre>";exit();
	$insertSQL2 = $conn2->AutoExecute("noti_work", $_POST, 'INSERT'); 
	$noti_work_id = $conn2->Insert_ID();

	if($_POST["caseMarine"] == '1'){
		$marine = array();
		$updatePO = array();
		$getRowPOByCode = getRowPOByCode($_POST["marine"]);
		$marinecode =  'N'.date("ymdsi")."".substr($_POST["marine"], 8, 9);
		$marine["noti_work_code"] = $marinecode;
		$marine["po_code"] = $getRowPOByCode["PO_ID"];
		$marine["po_type"] = ($getRowPOByCode["cus_type"] == "P") ? "ส่วนบุคคล" : "บริษัท";
		$marine["insuere_company"] = $getRowPOByCode["Insurer_Initials"];
		$marine["cus_name"] = $getRowPOByCode["FName"]." ".$getRowPOByCode["LName"];
		$marine["package_id"] = $getRowPOByCode["Insurance_Package_ID"];
		$marine["package_name"] = $getRowPOByCode["Insurance_Package_Name"];
		$marine["insurance_type"] = $getRowPOByCode["Insurance_Name"];
		$marine["start_cover_date"] = $getRowPOByCode["Coverage_Start_Date"]->format('Y-m-d');
		$marine["insuere_cost"] = $getRowPOByCode["Capital"];
		$marine["netpremium"] = $getRowPOByCode["Compulsory"];
		$marine["premium"] = $getRowPOByCode["Total_Premium"];
		$marine["discount"] = $getRowPOByCode["Discount"];
		$marine["taxamount"] = $getRowPOByCode["Premium_After_Disc"];
		$marine["send_type"] = $_POST["send_type"];
		$marine["send_addr"] = $_POST["send_addr"];
		$marine["personnel_code"] = $_POST["personnel_code"];
		$marine["personnel_name"] = $_POST["personnel_name"];
		$marine["chk_code"] = $chk_code;
		$marine["created_date"] = date("Y-m-d H:i:s");
		$marine["status"] = $_POST["personnel_code"];
		$marine["personnel_name"] = $_POST["personnel_name"];
		$marine["refer_po_code"] = $_POST["po_code"];
		$marine["refer_work_code"] = $noti_work_code;
		// echo "<pre>".print_r($marine,1)."</pre>"; 
		$insertSQL2 = $conn2->AutoExecute("noti_work", $marine, 'INSERT'); 

		// แก้ไข refer
		$updatePO["refer_po_code"] = $getRowPOByCode["PO_ID"];
		$updatePO["refer_work_code"] = $marinecode;
		$conn2->AutoExecute("noti_work", $updatePO, 'UPDATE', "noti_work_code = '".$noti_work_code."' AND po_code = '".$_POST["po_code"]."'"); 
		// echo "<pre>".print_r($marine,1)."</pre>"; 
	}
	// echo "<pre>".print_r($_POST,1)."</pre>"; 
	// exit(); 
	$comm["noti_work_id"] = $noti_work_id;
	$comm["noti_work_code"] = $noti_work_code;
	$comm["datetime"] = date("Y-m-d H:i:s");
	$comm["comment"] = "รอตรวจงาน";	
	$comm["comment_to"] = $_POST["chk_code"];
	$comm["comment_by"] = $_SESSION["User"]['UserCode'];
	$comm["comment_byname"] = $_SESSION["User"]['firstname']." ".$_SESSION["User"]['lastname'];
	// echo "<pre>".print_r($comm,1)."</pre>"; exit(); 
	$conn2->AutoExecute("noti_comment", $comm, 'INSERT');


	if(isset($_FILES["filUpload"])){
		$i = 1;
		foreach($_FILES['filUpload']['tmp_name'] as $key => $val){
			$nameTypes = "";
			if($val){
				// $exType = explode(".", $_FILES['filUpload']['name'][$key] );
				$file_size =$_FILES['filUpload']['size'][$key];
				$file_tmp = $_FILES['filUpload']['tmp_name'][$key];
				$file_type = $_FILES['filUpload']['type'][$key]; 
				
				if($file_type == "image/jpeg"){
					$image_create_func = 'imagecreatefromjpeg';
          $image_save_func = 'ImageJPEG';
          $nameTypes = 'jpg';
				}else if($file_type == "image/png"){
					$image_create_func = 'imagecreatefrompng';
          $image_save_func = 'ImagePNG';
          $nameTypes = 'png';
        }else if($file_type == "application/pdf"){
        	$nameTypes = 'pdf';
				}

				$newName = $_POST["po_code"]."_".rand()."_".$i++.".".$nameTypes;
				// echo "<br>file_type:".$file_type;
				if($file_type == "image/png" || $file_type == "image/jpeg"){
					$size = getimagesize($file_tmp);
					// echo "<br>size:".$size[0];
					if($size[0] > 1240 ){
							$width = 1240;
							$height = round($width * $size[1] / $size[0]);
							$images_orig = $image_create_func($file_tmp);
							$photoX = ImagesX($images_orig);
							$photoY = ImagesY($images_orig);
							$images_fin = ImageCreateTrueColor($width, $height);
							ImageCopyResampled($images_fin, $images_orig, 0, 0, 0, 0, $width+1, $height+1, $photoX, $photoY);
							$image_save_func($images_fin,"../myfile/".$newName);
							ImageDestroy($images_orig);
							ImageDestroy($images_fin);
					}else{
						move_uploaded_file($file_tmp,"../myfile/".$newName);
					}
				}else{
					move_uploaded_file($file_tmp,"../myfile/".$newName);
				}
				// echo "<pre>".print_r($_FILES,1)."</pre>";
				$fileUp = array();
				$fileUp["noti_work_id"] = $noti_work_id;
				$fileUp["file_name"] = $newName;
				$fileUp["name_types"] = $nameTypes;
				$fileUp["noti_work_code"] = $noti_work_code;
				$fileUp["po_id"] = $_POST["po_code"];
				$fileUp["file_type"] = $key;
				$fileUp["datetime"] = date("Y-m-d H:i:s");

				$insertSQL2 = $conn2->AutoExecute("noti_work_files", $fileUp, 'INSERT');
				// echo "<pre>".print_r($fileUp,1)."</pre>";  
			}

		} //exit();
	}
		echo "<script>alert('ทำรายการสำเร็จ กรุณารอการตรวจสอบ');</script>";
		echo '<META http-equiv="refresh" content="0;URL=../sendorders.php">';
		exit();

}else if($_POST["action"] == "sendUpdatelistchk"){
	// echo "<pre>".print_r($_POST,1)."</pre>"; 
 // echo "<pre>".print_r($_FILES,1)."</pre>";
	
	$noti_id = $_POST["noti_id"]; 
	$conn2->AutoExecute("noti_work", $_POST, 'UPDATE', "noti_work_id = ".$noti_id); 

	if(isset($_FILES["filUpload"])){
		$i = 1;
		foreach($_FILES['filUpload']['tmp_name'] as $key => $val){
			$nameTypes = "";
			if($val){
				// $exType = explode(".", $_FILES['filUpload']['name'][$key] );
				$file_size =$_FILES['filUpload']['size'][$key];
				$file_tmp = $_FILES['filUpload']['tmp_name'][$key];
				$file_type = $_FILES['filUpload']['type'][$key]; 
				
				if($file_type == "image/jpeg"){
					$image_create_func = 'imagecreatefromjpeg';
          $image_save_func = 'ImageJPEG';
          $nameTypes = 'jpg';
				}else if($file_type == "image/png"){
					$image_create_func = 'imagecreatefrompng';
          $image_save_func = 'ImagePNG';
          $nameTypes = 'png';
        }else if($file_type == "application/pdf"){
        	$nameTypes = 'pdf';
				}

				$newName = $_POST["po_code"]."_".rand()."_".$i++.".".$nameTypes;
				if($file_type == "image/png" || $file_type == "image/png"){
					$size = getimagesize($file_tmp);
					if($size[0] > 640 ){
							$width = 640;
							$height = round($width * $size[1] / $size[0]);
							$images_orig = $image_create_func($file_tmp);
							$photoX = ImagesX($images_orig);
							$photoY = ImagesY($images_orig);
							$images_fin = ImageCreateTrueColor($width, $height);
							ImageCopyResampled($images_fin, $images_orig, 0, 0, 0, 0, $width+1, $height+1, $photoX, $photoY);
							$image_save_func($images_fin,"../myfile/".$newName);
							ImageDestroy($images_orig);
							ImageDestroy($images_fin);
					}else{
						move_uploaded_file($file_tmp,"../myfile/".$newName);
					}
				}else{
					move_uploaded_file($file_tmp,"../myfile/".$newName);
				}
				$fileUp = array();
				$fileUp["noti_work_id"] = $_POST["noti_id"];
				$fileUp["file_name"] = $newName;
				$fileUp["name_types"] = $nameTypes;
				$fileUp["noti_work_code"] = $_POST["numcode"];
				$fileUp["po_id"] = $_POST["po_code"];
				$fileUp["file_type"] = $key;
				$fileUp["datetime"] = date("Y-m-d H:i:s");

				$insertSQL2 = $conn2->AutoExecute("noti_work_files", $fileUp, 'INSERT');
				// echo "<pre>".print_r($fileUp,1)."</pre>";  
			}

		} //exit();
	}	
		$url = "sendordersviews.php?pocode=".$_POST["po_code"]."&numcode=".$_POST["numcode"];
		echo "<script>alert('ทำการบันทึกข้อมูลเรียบร้อย');</script>";
		echo '<META http-equiv="refresh" content="0;URL=../'.$url.'">';
		exit();


}else if($_POST["action"] == "getImageOrNote"){
	$getNotiWorkByID = getNotiWorkByID($_POST["noti_id"]);
	$getNotiWorkFilesByID = getNotiWorkFilesByID($_POST["noti_id"]);
	$getNotiComment = getNotiComment($_POST["noti_id"]);
	$getCommenStatus = getCommenStatus();
	$html = "";
	$html .= '
		<div class="p15">
			<div class="row">
				<div class="col-md-8">
				<div>
					<p><b>หมายเหตุ</b></p>
					<p>'.$getNotiWorkByID["notes"].'</p>
				</div>
					<p><b>รูปภาพประกอบ</b></p>';
				foreach ($getNotiWorkFilesByID as $key => $value) {
					$fileType = explode(".", $value["file_name"]);
					if($fileType[1] != "pdf"){
						$html .= '
						<div class="col-md-1">
							<a href="myfile/'.$value["file_name"].'" data-fancybox="watermark" class="fancybox">
								<img src="myfile/'.$value["file_name"].'" alt="" class="img-responsive rounded">
							</a>
						</div>';
					}else{
						$html .= '
						<div class="col-md-2">
							<a href="myfile/'.$value["file_name"].'" data-fancybox="watermark" class="fancybox">'.$value["file_name"].'</a>
						</div>';
					}
				}
	$html .= '
				</div>
				<div class="col-md-4" style="background-color: #ffffed; padding: 10px;">
					<p><b>Comment</b></p>
						<div id="listMent_'.$_POST["noti_id"].'">';
					foreach ($getNotiComment as $key => $value) {
						$html .= '
							<div style="color: #b0b0b0;font-size: 11px;"><span >'.$value["datetime"].' </span> | '.$value["comment_byname"].' </div>
							<div class="ml10"> >>> '.$value["comment"].'</div>
						';
					}
	$html .= '
						</div>
					<div>
						<input type="hidden" value="'.$_POST["noti_id"].'" name="noti_id">
						<input type="text"  name="comment" id="comment_'.$_POST["noti_id"].'" class="form-control formInput fs12 mb5">
						<input type="button" class="btn btn-info" value="send" id="ments_'.$_POST["noti_id"].'"  onclick="btnComments(this)">
					</div>
				</div>
			</div>
		</div>';
	echo $html;
	exit();

}else if($_POST["action"] == "addComments"){
	 // echo "<pre>".print_r($_POST,1)."</pre>";
	$getNotiWorkByID = getNotiWorkByID($_POST["noti_work_code"]);
	$_POST["noti_work_id"] = $getNotiWorkByID["noti_work_id"];
	$_POST["datetime"] = date("Y-m-d H:i:s");
	$_POST["comment_to"] = $getNotiWorkByID['personnel_code'];
	$_POST["comment_by"] = $_SESSION["User"]['UserCode'];
	$_POST["comment_byname"] = $_SESSION["User"]['firstname']." ".$_SESSION["User"]['lastname'];

	$insertSQL2 = $conn2->AutoExecute("noti_comment", $_POST, 'INSERT'); 
	$rsID = $conn2->Insert_ID();
	$getNotiComment = getNotiComment($_POST["noti_work_code"]);
	$html = "";
	foreach ($getNotiComment as $key => $value) {
						$html .= '
							<div style="color: #b0b0b0;font-size: 11px;"><span >'.$value["datetime"].' </span> | '.$value["comment_byname"].' </div>
							<div class="ml10"> >>> '.$value["comment"].'</div>
						';
					}
	echo $html;
	exit();

}else if($_POST["main"]["action"] == "sendAddlistchk"){
	// echo "<pre>".print_r($_POST,1)."</pre>";
	$check = $_POST["check"];
	$pocode = $_POST["main"]["pocode"];
	$numcode = $_POST["main"]["numcode"];
	$noti_id = $_POST["main"]["noti_id"];
	$keyword = array();
	$data = array();
	$checklists = array();
	$comments = array();
	foreach ($check as $key => $value) {
		// echo "<br>".$key." - ".preg_replace('#<br>#', '', $value["data"]);
		$valData = preg_replace('#<br>#', '', $value["data"]);
		array_push($keyword, $key);
		array_push($data, $valData);
		array_push($checklists, $value["chk"]);
		array_push($comments, $value["comment"]);
	}
	$dataArray["noti_work_id"]= $noti_id;
	$dataArray["noti_work_code"]= $numcode;
	$dataArray["po_id"]= $pocode;
	$dataArray["keyword"]= serialize($keyword);
	$dataArray["data"]= serialize($data);
	$dataArray["checklists"]= serialize($checklists);
	$dataArray["comments"]= serialize($comments);
	$dataArray["assessment"]= ($_POST["main"]["status"] == "1") ? "0" : "1";
	$dataArray["notes"]= $_POST["main"]["notes"];
	$dataArray["datetime"]= date("Y-m-d H:i:s");
	$dataArray["check_by"]= $_SESSION["User"]['UserCode'];
// echo "<pre>".print_r($dataArray,1)."</pre>";
	$insertSQL = $conn2->AutoExecute("noti_work_chk", $dataArray, 'INSERT'); 
  $api_id = $conn2->Insert_ID();

	if($_POST["main"]["status"] == "2"){
		$dataMain["admin_code"] = getAdminFromInsurer($_POST["main"]["insurer"]);
	}
	$dataMain["datetime_chk"]= date("Y-m-d H:i:s");
	$dataMain["status"] = $_POST["main"]["status"];
	// $dataMain["enable"] = ($_POST["main"]["status"] == "1") ? "0" : "1" ;
	$conn2->AutoExecute("noti_work", $dataMain, 'UPDATE', "noti_work_id = ".$noti_id); 

	if($_POST["main"]["status"] == "1"){
		$comm["comment"] = "ไม่ผ่าน กรุณาแก้ไข";	
	}else if($_POST["main"]["status"] == "2"){
		$comm["comment"] = "ผ่านการตรวจ รอการแจ้งงาน";	
	}
	$comm["noti_work_id"] = $noti_id;
	$comm["noti_work_code"] = $numcode;
	$comm["datetime"] = date("Y-m-d H:i:s");
	$comm["comment_to"] = $_POST["main"]['personnel_code'];
	$comm["comment_by"] = $_SESSION["User"]['UserCode'];
	$comm["comment_byname"] = $_SESSION["User"]['firstname']." ".$_SESSION["User"]['lastname'];
	// echo "<pre>".print_r($comm,1)."</pre>"; exit(); 
	$conn2->AutoExecute("noti_comment", $comm, 'INSERT'); 

  if($api_id){
  	echo "<script>alert('ระบบทำการบันทึกข้อมูลเรียบร้อย');</script>";
		echo '<META http-equiv="refresh" content="0;URL=../sendorders.php">';
		exit();
  }else{
  	echo "<script>alert('ระบบเกิดความผิดพลาดกรุณาทำใหม่');</script>";
		echo '<META http-equiv="refresh" content="0;URL=../sendorderslists.php?pocode='.$pocode.'&numcode='.$numcode.'">';
		exit();
  }
	exit();

}else if($_POST["action"] == "upDateStatus"){
	$data = array();
	$comm = array();
	$getNotiWorkByID = getNotiWorkByID($_POST["code"]);

	$data["status"] = $_POST["status"];
	if($_POST["status"]== 4){
		$data["datetime_send"] = date("Y-m-d H:i:s");
	}
	$rs = $conn2->AutoExecute("noti_work", $data, 'UPDATE', "noti_work_id = ".$_POST["id"]);
	if($_POST["status"] == 1){
		$comm["comment"] = "ยกเลิกโดย Admin ";	
	}else if($_POST["status"] == 3){
		$comm["comment"] = "กำลังแจ้งงาน";	
	}else if($_POST["status"] == 4){
		$comm["comment"] = "แจ้งงานเสร็จสมบูรณ์";	
	}
	$comm["noti_work_id"] = $getNotiWorkByID["noti_work_id"];
	$comm["noti_work_code"] = $_POST["code"];
	$comm["datetime"] = date("Y-m-d H:i:s");
	$comm["comment_to"] = $getNotiWorkByID['personnel_code'];
	$comm["comment_by"] = $_SESSION["User"]['UserCode'];
	$comm["comment_byname"] = $_SESSION["User"]['firstname']." ".$_SESSION["User"]['lastname'];
	// echo "<pre>".print_r($comm,1)."</pre>"; exit(); 
	$insertSQL = $conn2->AutoExecute("noti_comment", $comm, 'INSERT'); 
	echo $rs;

}else if($_POST["action"] == "delPONOti"){
	$data["enable"] = 0;
	$rs = $conn2->AutoExecute("noti_work", $data, 'UPDATE', "noti_work_id = ".$_POST["id"]);
	echo $rs;
	exit();

}else if($_POST["action"] == "delfileEdit"){
	// echo "<pre>".print_r($rs,1)."</pre>"; exit(); 
	// echo "../myfile/".$_POST["name"];
	$un = unlink("../myfile/".$_POST["name"]);
	// if($un){
		$rs = $conn2->Execute("DELETE FROM noti_work_files WHERE noti_work_code = '".$_POST["code"]."' AND file_type = '".$_POST["filetype"]."' AND file_name = '".$_POST["name"]."' " );
		echo 1;
	// }else{
	// 	echo 0;
	// }
	exit();

}else if($_POST["action"] == "btnChangStatus"){
	if($_POST["admin_code"]){
		$data["status"] = 2;
	}else{
		$data["status"] = 0;
	}
	$rs = $conn2->AutoExecute("noti_work", $data, 'UPDATE', "noti_work_code = '".$_POST["code"]."'");
	echo $rs;
	exit();

}else if($_POST["action"] == "updateAdminInser"){
	$rs = $conn2->AutoExecute("ck_insurer", $_POST, 'UPDATE', "insurer_id = '".$_POST["insurer_id"]."'");
	echo $rs;
	exit();
	
}else if($_POST["action"] == "updateWorkInOut"){
	$rs = $conn2->AutoExecute("ck_personnel", $_POST, 'UPDATE', "personnel_code = '".$_POST["personnel_code"]."'");
	echo $rs;
	exit();

}else if($_POST["action"] == "addCommentList"){
	$_POST["datetime"] = date("Y-m-d H:i:s");
	$_POST["post_by"] = $_SESSION["User"]['UserCode'];
	$insertSQL = $conn2->AutoExecute("comment_lists", $_POST, 'INSERT'); 
	echo $insertSQL;
	exit();

}else if($_POST["action"] == "delCommentList"){
	$rs = $conn2->Execute("DELETE FROM comment_lists WHERE comment_lists_id = '".$_POST["id"]."'" );
	echo 1;
	exit();

}else if($_POST["action"] == "updateCommentList"){
	$rs = $conn2->AutoExecute("comment_lists", $_POST, 'UPDATE', "comment_lists_id = '".$_POST["comment_lists_id"]."'");
	echo rs;
	exit();

}else if($_POST["action"] == "addCommentStatus"){
	$_POST["datetime"] = date("Y-m-d H:i:s");
	$_POST["post_by"] = $_SESSION["User"]['UserCode'];
	$insertSQL = $conn2->AutoExecute("comment_status", $_POST, 'INSERT'); 
	echo $insertSQL;
	exit();

}else if($_POST["action"] == "delCommentStatus"){
	$rs = $conn2->Execute("DELETE FROM comment_status WHERE comment_status_id = '".$_POST["id"]."'" );
	echo 1;
	exit();

}else if($_POST["action"] == "updateCommentStatus"){
	$rs = $conn2->AutoExecute("comment_status", $_POST, 'UPDATE', "comment_status_id = '".$_POST["comment_status_id"]."'");
	echo rs;
	exit();

}else if($_POST["action"] == "addMoveQC"){
	// echo "<pre>".print_r($_POST,1)."</pre>";
	$data = array();
	foreach ($_POST["chktxt"] as $key => $value) {
		$data["chk_code"] = $_POST["newqcCode"];
		$code = explode("_", $value);
		// echo "<br>".$code[0];
		$rs = $conn2->AutoExecute("noti_work", $data, 'UPDATE', "noti_work_id = '".$code[1]."' AND noti_work_code = '".$code[0]."' ");
	}
	echo "<script>alert('แก้ไขข้อมูลเรียบร้อย');</script>";
	echo '<META http-equiv="refresh" content="0;URL=../moveleadqc.php">';
	exit();

}


?>