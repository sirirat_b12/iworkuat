<?php
	session_start(); 
	date_default_timezone_set("Asia/Bangkok");
	include "inc_config.php";
	include "include/inc_function.php";
	include "include/inc_function_chk.php"; 
	// setSessionTime(60);
	$userProfile = getAllSale($_SESSION["User"]['UserCode']);
	
?>
<!doctype html>
<html lang="en" class="fullscreen-bg">

<head>
	<title>Service | เอเชียไดเร็ค</title>
	<!-- <meta charset="utf-8"> -->
	<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<!-- VENDOR CSS -->
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<!-- <link rel="stylesheet" href="assets/vendor/font-awesome/css/font-awesome.min.css"> -->
	<link rel="stylesheet" href="assets/vendor/fontawesome/web-fonts-with-css/css/fontawesome-all.css">
	<!-- <link rel="stylesheet" href="assets/vendor/linearicons/style.css"> -->
	<!-- <link rel="stylesheet" href="assets/vendor/chartist/css/chartist-custom.css"> -->
	<!-- MAIN CSS -->
	<link rel="stylesheet" href="assets/css/main.css">
	<!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
	<link rel="stylesheet" href="assets/css/style.css">
	<!-- GOOGLE FONTS -->
	<!-- <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet"> -->
	<!-- <link href="https://fonts.googleapis.com/css?family=Prompt" rel="stylesheet"> -->
	<!-- ICONS -->
	<link rel="apple-touch-icon" sizes="76x76" href="img/favicon.ico">
	<link rel="icon" type="image/png" sizes="96x96" href="img/favicon.ico">

	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="table_js/dist/bootstrap-table.min.css">
	<!-- <link rel="stylesheet" type="text/css" href="fancybox/dist/jquery.fancybox.min.css"> -->
	<script src="ckeditor/ckeditor.js"></script>
	
</head>

<body>