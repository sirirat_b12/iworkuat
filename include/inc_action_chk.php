<?php
session_start();
ini_set('post_max_size', '512M');
ini_set('upload_max_filesize', '512M');
ini_set("memory_limit","512M");
ini_set('max_execution_time', 300);
date_default_timezone_set("Asia/Bangkok");
header('Content-Type: text/html; charset=utf-8');

include "../inc_config.php";
require '../phpmailer6/src/PHPMailer.php';
require '../phpmailer6/src/SMTP.php';
require '../phpmailer6/src/Exception.php';
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;
include "inc_function.php"; 
include "inc_function_chk.php"; 

if($_POST["action"] == "sendOrderAdmin"){
	
	$getQCcheck = getQCcheck(); 
	$chk_code = $getQCcheck["personnel_code"];

 	if($_POST["personnel_code"] == "ADB61034"){
		$admin_code = "AdminNsn";
	}else{
		$getAdminforwork = getAdminforwork(); 
		$admin_code = $getAdminforwork["personnel_code"];
	}

	foreach ($_POST["select_po"] as $key => $value) {
		$poCode = $key;
		$getRowPOByCode = getRowPOByCode($poCode);

		if($getRowPOByCode["Insurance_ID"] == "029"){
			$netpremium = $getRowPOByCode["Compulsory"];
		}else{
			$netpremium = $getRowPOByCode["Net_Premium"];
		}
		$noti_work_code = getNotiCode($getRowPOByCode["Insurance_ID"], $poCode);

		$arr["noti_work_code"] = $noti_work_code;
		$arr["po_code"] = $getRowPOByCode["PO_ID"];
		$arr["po_type"] = ($getRowPOByCode["cus_type"] == "P") ? "ส่วนบุคคล" : "บริษัท";
		$arr["insuere_company"] = $getRowPOByCode["Insurer_Initials"];
		$arr["cus_name"] = $getRowPOByCode["Title_Name"]." ".$getRowPOByCode["FName"]." ".$getRowPOByCode["LName"];
		$arr["package_id"] = $getRowPOByCode["Insurance_Package_ID"];
		$arr["package_name"] = $getRowPOByCode["Insurance_Package_Name"];
		$arr["insurance_type"] = $getRowPOByCode["Insurance_Name"];
		$arr["start_cover_date"] = $getRowPOByCode["Coverage_Start_Date"]->format('Y-m-d');
		$arr["insuere_cost"] = $getRowPOByCode["Capital"];
		$arr["netpremium"] = $netpremium;
		$arr["premium"] = $getRowPOByCode["Total_Premium"];
		$arr["discount"] = $getRowPOByCode["Discount"];
		$arr["taxamount"] = $getRowPOByCode["Premium_After_Disc"];
		$arr["send_type"] = $getRowPOByCode["Delivery_Type_Desc"];
		$arr["personnel_code"] = $_POST["personnel_code"];
		$arr["personnel_name"] = $_POST["personnel_name"];
		$arr["chk_code"] = $admin_code;
		$arr["admin_code"] = $admin_code;
		$arr["created_date"] = date("Y-m-d H:i:s");
		// $arr["status"] = 0;
		$arr["status"] = 2; // v ใหม่ เข้า Admin โดยตรง
		$arr["personnel_name"] = $_POST["personnel_name"];

		if(array_key_exists( $getRowPOByCode["Marine_Reference"],$_POST["select_po"] )){
			$arr["refer_po_code"] = $getRowPOByCode["Marine_Reference"];
		}

		$arr["giftvoucher"] = $getRowPOByCode["Premium_Desc"];
		$arr["giftvoucher_po"] = $_POST["casePO"][$poCode]["giftvoucher_po"];
		$arr["discount_cctv"] = $_POST["casePO"][$poCode]["discount_cctv"] ? 1 : 0;
		$arr["endorse"] = $_POST["casePO"][$poCode]["endorse"] ? 1 : 0;
		$arr["urgent"] = $_POST["casePO"][$poCode]["urgent"] ? 1 : 0;
		$arr["notes"] = $_POST["casePO"][$poCode]["notes"]; 
		$arr["operation_type"] = $_POST["casePO"][$poCode]["operation_type"];
		$insertSQL2 = $conn2->AutoExecute("noti_work", $arr, 'INSERT'); 
		$noti_work_id = $conn2->Insert_ID();

		$comm["noti_work_id"] = $noti_work_id;
		$comm["noti_work_code"] = $noti_work_code;
		$comm["datetime"] = date("Y-m-d H:i:s");
		$comm["comment"] = "รอตรวจงาน";	
		$comm["comment_to"] = $chk_code;
		$comm["comment_by"] = $_POST["personnel_code"];
		$comm["comment_byname"] = $_POST["personnel_name"];
		$conn2->AutoExecute("noti_comment", $comm, 'INSERT');

		// if($getRowPOByCode["Insurance_ID"] != "029"){
			if(isset($_FILES["filUpload"])){
				$urlfolder = "../myfile/".date("Ym");
				if (!is_dir($urlfolder)) {
				    mkdir($urlfolder, 0777, true);
				}
				$i = 1;
				foreach($_FILES['filUpload']['tmp_name'][$poCode] as $key => $val){
					$nameTypes = "";
					if($val){
						$file_size = $_FILES['filUpload']['size'][$poCode][$key];
						$file_tmp = $_FILES['filUpload']['tmp_name'][$poCode][$key];
						$file_type = $_FILES['filUpload']['type'][$poCode][$key]; 
						
						if($file_type == "image/jpeg"){
							$image_create_func = 'imagecreatefromjpeg';
		          $image_save_func = 'ImageJPEG';
		          $nameTypes = 'jpg';
						}else if($file_type == "image/png"){
							$image_create_func = 'imagecreatefrompng';
		          $image_save_func = 'ImagePNG';
		          $nameTypes = 'png';
		        }else if($file_type == "application/pdf"){
		        	$nameTypes = 'pdf';
						}

						$newName = $poCode."_".rand()."_".$i++.".".$nameTypes;
						if($file_type == "image/png" || $file_type == "image/jpeg"){
							$size = getimagesize($file_tmp);
							if($size[0] > 1240 ){
									$width = 1240;
									$height = round($width * $size[1] / $size[0]);
									$images_orig = $image_create_func($file_tmp);
									$photoX = ImagesX($images_orig);
									$photoY = ImagesY($images_orig);
									$images_fin = ImageCreateTrueColor($width, $height);
									ImageCopyResampled($images_fin, $images_orig, 0, 0, 0, 0, $width+1, $height+1, $photoX, $photoY);
									$image_save_func($images_fin, $urlfolder."/".$newName);
									ImageDestroy($images_orig);
									ImageDestroy($images_fin);
							}else{
								move_uploaded_file($file_tmp, $urlfolder."/".$newName);
							}
						}else{
							move_uploaded_file($file_tmp, $urlfolder."/".$newName);
						}
						$fileUp = array();
						$fileUp["noti_work_id"] = $noti_work_id;
						$fileUp["file_name"] = $newName;
						$fileUp["name_types"] = $nameTypes;
						$fileUp["pathfile"] = date("Ym");
						$fileUp["noti_work_code"] = $noti_work_code;
						$fileUp["po_id"] = $poCode;
						$fileUp["file_type"] = $key;
						$fileUp["datetime"] = date("Y-m-d H:i:s");
						$insertSQL2 = $conn2->AutoExecute("noti_work_files", $fileUp, 'INSERT'); 
					}
				}
			}
		//}

	}
	
	// แก้ไข refer
	foreach ($_POST["select_po"] as $key => $value) {
		$refer = $conn2->GetRow("SELECT noti_work_code, po_code, refer_po_code FROM noti_work WHERE po_code = '".$key."' AND enable='1' ORDER BY noti_work.created_date DESC LIMIT 0, 1 ");

		if($refer){
			$refer2 = $conn2->GetRow("SELECT noti_work_code, po_code, refer_po_code FROM noti_work WHERE po_code = '".$refer["refer_po_code"]."' AND  refer_po_code = '".$refer["po_code"]."' AND enable='1' ORDER BY noti_work.created_date DESC LIMIT 0, 1 ");
			$updatePO["refer_work_code"] = $refer2["noti_work_code"];
			$conn2->AutoExecute("noti_work", $updatePO, 'UPDATE', "refer_po_code = '".$refer["refer_po_code"]."' AND enable='1' AND status='0' "); 
		}
	}
		echo "<script>alert('ทำรายการสำเร็จ กรุณารอการตรวจสอบ');</script>";
		echo '<META http-equiv="refresh" content="0;URL=../sendorders.php">';
		exit();


}else if($_POST["action"] == "sendUpdatelistchk"){

// echo "<pre>".print_r($_POST,1)."</pre>"; exit(); 

	$noti_id = $_POST["noti_id"]; 
	$conn2->AutoExecute("noti_work", $_POST, 'UPDATE', "noti_work_id = ".$noti_id); 

	if(isset($_FILES["filUpload"])){
		$urlfolder = "../myfile/".date("Ym");
		if (!is_dir($urlfolder)) {
		    mkdir($urlfolder, 0777, true);
		}
		// echo "<pre>".print_r($_FILES,1)."</pre>"; exit(); 
		$i = 1;
		foreach($_FILES['filUpload']['tmp_name'] as $key => $val){
			$nameTypes = "";
			if($val){
				// $exType = explode(".", $_FILES['filUpload']['name'][$key] );
				$file_size =$_FILES['filUpload']['size'][$key];
				$file_tmp = $_FILES['filUpload']['tmp_name'][$key];
				$file_type = $_FILES['filUpload']['type'][$key]; 
				
				if($file_type == "image/jpeg"){
					$image_create_func = 'imagecreatefromjpeg';
          $image_save_func = 'ImageJPEG';
          $nameTypes = 'jpg';
				}else if($file_type == "image/png"){
					$image_create_func = 'imagecreatefrompng';
          $image_save_func = 'ImagePNG';
          $nameTypes = 'png';
        }else if($file_type == "application/pdf"){
        	$nameTypes = 'pdf';
				}

				$newName = $_POST["po_code"]."_".rand()."_".$i++.".".$nameTypes;
				if($file_type == "image/png" || $file_type == "image/png"){
					$size = getimagesize($file_tmp);
					if($size[0] > 1240 ){
							$width = 1240;
							$height = round($width * $size[1] / $size[0]);
							$images_orig = $image_create_func($file_tmp);
							$photoX = ImagesX($images_orig);
							$photoY = ImagesY($images_orig);
							$images_fin = ImageCreateTrueColor($width, $height);
							ImageCopyResampled($images_fin, $images_orig, 0, 0, 0, 0, $width+1, $height+1, $photoX, $photoY);
							$image_save_func($images_fin, $urlfolder."/".$newName);
							ImageDestroy($images_orig);
							ImageDestroy($images_fin);
					}else{
						move_uploaded_file($file_tmp, $urlfolder."/".$newName);
					}
				}else{
					move_uploaded_file($file_tmp, $urlfolder."/".$newName);
				}
				$fileUp = array();
				$fileUp["noti_work_id"] = $_POST["noti_id"];
				$fileUp["file_name"] = $newName;
				$fileUp["name_types"] = $nameTypes;
				$fileUp["pathfile"] = date("Ym");
				$fileUp["noti_work_code"] = $_POST["numcode"];
				$fileUp["po_id"] = $_POST["po_code"];
				$fileUp["file_type"] = $key;
				$fileUp["datetime"] = date("Y-m-d H:i:s");

				$insertSQL2 = $conn2->AutoExecute("noti_work_files", $fileUp, 'INSERT');
				// echo "<pre>".print_r($fileUp,1)."</pre>";  
			}

		} //exit();
	}	
		$url = "workout.php?pocode=".$_POST["po_code"]."&numcode=".$_POST["numcode"];
		echo "<script>alert('ทำการบันทึกข้อมูลเรียบร้อย');</script>";
		echo '<META http-equiv="refresh" content="0;URL=../'.$url.'">';
		exit();


}else if($_POST["action"] == "getImageOrNote"){
	$getNotiWorkByID = getNotiWorkByID($_POST["noti_id"]);
	$getNotiWorkFilesByID = getNotiWorkFilesByID($_POST["noti_id"]);
	$getNotiComment = getNotiComment($_POST["noti_id"]);
	$getCommenStatus = getCommenStatus();
	$html = "";
	$html .= '
		<div class="p15">
			<div class="row">
				<div class="col-md-8">
				<div>
					<p><b>หมายเหตุ</b></p>
					<p>'.$getNotiWorkByID["notes"].'</p>
				</div>
					<p><b>รูปภาพประกอบ</b></p>';
				foreach ($getNotiWorkFilesByID as $key => $value) {
					$fileType = explode(".", $value["file_name"]);
					$path  = ($value["pathfile"]) ? $value["pathfile"]."/" : ""; 
					if($fileType[1] != "pdf"){
						$html .= '
						<div class="col-md-1">
							<a href="myfile/'.$path.''.$value["file_name"].'" data-magnify="gallery" data-src="" data-group="a">
								<img src="myfile/'.$path.''.$value["file_name"].'" alt="" class="img-responsive rounded">
							</a>
						</div>';
					}else{
						$html .= '
						<div class="col-md-2">
							<a href="myfile/'.$path.''.$value["file_name"].'" data-fancybox="watermark" class="fancybox">'.$value["file_name"].'</a>
						</div>';
					}
				}
	$html .= '
				</div>
				<div class="col-md-4" style="background-color: #ffffed; padding: 10px;">
					<p><b>Comment</b></p>
						<div id="listMent_'.$_POST["noti_id"].'">';
					foreach ($getNotiComment as $key => $value) {
						$html .= '
							<div style="color: #b0b0b0;font-size: 11px;"><span >'.$value["datetime"].' </span> | '.$value["comment_byname"].' </div>
							<div class="ml10"> >>> '.$value["comment"].'</div>
						';
					}
	$html .= '
						</div>
					<div>
						<input type="hidden" value="'.$_POST["noti_id"].'" name="noti_id">
						<input type="text"  name="comment" id="comment_'.$_POST["noti_id"].'" class="form-control formInput fs12 mb5">
						<input type="button" class="btn btn-info" value="send" id="ments_'.$_POST["noti_id"].'"  onclick="btnComments(this)">
					</div>
				</div>
			</div>
		</div>';
	echo $html;
	exit();

}else if($_POST["action"] == "addComments"){
	 // echo "<pre>".print_r($_POST,1)."</pre>";
	$getNotiWorkByID = getNotiWorkByID($_POST["noti_work_code"]);
	$_POST["noti_work_id"] = $getNotiWorkByID["noti_work_id"];
	$_POST["datetime"] = date("Y-m-d H:i:s");
	$_POST["comment_to"] = $getNotiWorkByID['personnel_code'];
	$_POST["comment_by"] = $_SESSION["User"]['UserCode'];
	$_POST["comment_byname"] = $_SESSION["User"]['firstname']." ".$_SESSION["User"]['lastname'];

	$insertSQL2 = $conn2->AutoExecute("noti_comment", $_POST, 'INSERT'); 
	$rsID = $conn2->Insert_ID();
	$getNotiComment = getNotiComment($_POST["noti_work_code"]);
	$html = "";
	foreach ($getNotiComment as $key => $value) {
						$html .= '
							<div style="color: #b0b0b0;font-size: 11px;"><span >'.$value["datetime"].' </span> | '.$value["comment_byname"].' </div>
							<div class="ml10"> >>> '.$value["comment"].'</div>
						';
					}
	echo $html;
	exit();

}else if($_POST["main"]["action"] == "sendAddlistchk"){
	// echo "<pre>".print_r($_POST,1)."</pre>";
	$check = $_POST["check"];
	$pocode = $_POST["main"]["pocode"];
	$numcode = $_POST["main"]["numcode"];
	$noti_id = $_POST["main"]["noti_id"];
	
	// $getRowpersonnelByCode = getRowpersonnelByCode($_POST["main"]["personnel_code"]);
	// if($getRowpersonnelByCode["personnel_branch"] == "สาขานครสวรรค์"){
	if($_POST["main"]["personnel_code"] == "ADB61034"){
		$admin_code = "AdminNsn";
	}else{
		$Concomitant = chkPOConcomitant($pocode, $numcode);
		if($Concomitant){
			$admin_code = $Concomitant;
		}else{
			$getAdminforwork = getAdminforwork(); 
			$admin_code = $getAdminforwork["personnel_code"];
		}
		// $admin_code = getAdminFromInsurer($_POST["main"]["insurer"]);
	}
	
	$keyword = array();
	$data = array();
	$checklists = array();
	$comments = array();
	foreach ($check as $key => $value) {
		// echo "<br>".$key." - ".preg_replace('#<br>#', '', $value["data"]);
		$valData = preg_replace('#<br>#', '', $value["data"]);
		array_push($keyword, $key);
		array_push($data, $valData);
		array_push($checklists, $value["chk"]);
		array_push($comments, $value["comment"]);
	}
	$dataArray["noti_work_id"]= $noti_id;
	$dataArray["noti_work_code"]= $numcode;
	$dataArray["po_id"]= $pocode;
	$dataArray["keyword"]= serialize($keyword);
	$dataArray["data"]= serialize($data);
	// $dataArray["checklists"]= serialize($checklists);
	// $dataArray["comments"]= serialize($comments);
	$dataArray["assessment"]= ($_POST["main"]["status"] == "1") ? "0" : "1";
	$dataArray["notes"]= $_POST["main"]["notes"];
	$dataArray["datetime"]= date("Y-m-d H:i:s");
	$dataArray["check_by"]= $_SESSION["User"]['UserCode'];
// echo "<pre>".print_r($dataArray,1)."</pre>";
	$insertSQL = $conn2->AutoExecute("noti_work_chk", $dataArray, 'INSERT'); 
  $api_id = $conn2->Insert_ID();

	if($_POST["main"]["status"] == "2"){
		$dataMain["admin_code"] = $admin_code;
	}
	$dataMain["datetime_chk"]= date("Y-m-d H:i:s");
	$dataMain["status"] = $_POST["main"]["status"];
	$dataMain["urgent"] = $_POST["main"]["urgent"];
	// $dataMain["enable"] = ($_POST["main"]["status"] == "1") ? "0" : "1" ;
	$conn2->AutoExecute("noti_work", $dataMain, 'UPDATE', "noti_work_id = ".$noti_id); 

	if($_POST["main"]["status"] == "1"){
		$comm["comment"] = "ไม่ผ่าน กรุณาแก้ไข";	
	}else if($_POST["main"]["status"] == "2"){
		$comm["comment"] = "ผ่านการตรวจ รอการแจ้งงาน";	
	}
	$comm["noti_work_id"] = $noti_id;
	$comm["noti_work_code"] = $numcode;
	$comm["datetime"] = date("Y-m-d H:i:s");
	$comm["comment_to"] = $_POST["main"]['personnel_code'];
	$comm["comment_by"] = $_SESSION["User"]['UserCode'];
	$comm["comment_byname"] = $_SESSION["User"]['firstname']." ".$_SESSION["User"]['lastname'];
	// echo "<pre>".print_r($comm,1)."</pre>"; exit(); 
	$conn2->AutoExecute("noti_comment", $comm, 'INSERT'); 

	if($_POST["main"]["status"] == "2"){
		$comm["comment_to"] = $admin_code;
		$conn2->AutoExecute("noti_comment", $comm, 'INSERT'); 
	}

  if($api_id){
  	echo "<script>alert('ระบบทำการบันทึกข้อมูลเรียบร้อย');</script>";
		echo '<META http-equiv="refresh" content="0;URL=../sendorders.php">';
		exit();
  }else{
  	echo "<script>alert('ระบบเกิดความผิดพลาดกรุณาทำใหม่');</script>";
		echo '<META http-equiv="refresh" content="0;URL=../sendorderslists.php?pocode='.$pocode.'&numcode='.$numcode.'">';
		exit();
  }
	exit();

}else if($_POST["action"] == "upDateStatus"){
	$data = array();
	$comm = array();
	$getNotiWorkByID = getNotiWorkByID($_POST["code"]);

	$data["status"] = $_POST["status"];
	if($_POST["status"]== 4){
		$getPolicyNumber = getPolicyNumber($getNotiWorkByID["po_code"]);
		$data["policy_number"] = $getPolicyNumber["Policy_No"] ? $getPolicyNumber["Policy_No"] : $getPolicyNumber["Compulsory_No"];
		$data["datetime_send"] = date("Y-m-d H:i:s");
	}elseif($_POST["status"]== 3){
	  $data["datetime_send_open"] = date("Y-m-d H:i:s");
	}
	 // echo "<pre>".print_r($data,1)."</pre>"; exit(); 
	$rs = $conn2->AutoExecute("noti_work", $data, 'UPDATE', "noti_work_id = ".$_POST["id"]);
	
	if($_POST["status"] == 1){
		$comm["comment"] = "ยกเลิกโดย Admin ";	
		$textLine = 'แอดมินแจ้งยกเลิกการแจ้งงาน \n'.$getNotiWorkByID["po_code"].'\nโดย '.$_SESSION["User"]['firstname'].' '.$_SESSION["User"]['lastname'].'  \nวันที่สร้าง '.date('Y-m-d H:i:s');
		curlLineNotiCHK($getNotiWorkByID["personnel_code"], $textLine); 

	}else if($_POST["status"] == 3){
		$comm["comment"] = "กำลังแจ้งงาน";	

	}else if($_POST["status"] == 4){
		$comm["comment"] = "แจ้งงานเสร็จสมบูรณ์";	
		$textLine = 'แอดมินแจ้งงานเสร็จสมบูรณ์ \n'.$getNotiWorkByID["po_code"].'\nโดย '.$_SESSION["User"]['firstname'].' '.$_SESSION["User"]['lastname'].'  \nวันที่สร้าง '.date('Y-m-d H:i:s');
		curlLineNotiCHK($getNotiWorkByID["personnel_code"], $textLine); 
	}
	
	$comm["noti_work_id"] = $getNotiWorkByID["noti_work_id"];
	$comm["noti_work_code"] = $_POST["code"];
	$comm["datetime"] = date("Y-m-d H:i:s");
	$comm["comment_to"] = $getNotiWorkByID['personnel_code'];
	$comm["comment_by"] = $_SESSION["User"]['UserCode'];
	$comm["comment_byname"] = $_SESSION["User"]['firstname']." ".$_SESSION["User"]['lastname'];
	// echo "<pre>".print_r($comm,1)."</pre>"; exit(); 
	$insertSQL = $conn2->AutoExecute("noti_comment", $comm, 'INSERT'); 
	echo $rs;

}else if($_POST["action"] == "delPONOti"){
	$data["enable"] = 0;
	$rs = $conn2->AutoExecute("noti_work", $data, 'UPDATE', "noti_work_id = ".$_POST["id"]);
	echo $rs;
	exit();

}else if($_POST["action"] == "delfileEdit"){
	// echo "<pre>".print_r($_POST,1)."</pre>"; exit(); 
	$un = unlink("../myfile/".$_POST["name"]);
	$rs = $conn2->Execute("DELETE FROM noti_work_files WHERE noti_work_files_id = '".$_POST["code"]."'" );
	echo 1;
	exit();

}else if($_POST["action"] == "btnChangStatus"){
	$noti = getNotiWorkByID($_POST["code"]);
// echo "<pre>".print_r($noti,1)."</pre>";
	if($_POST["admin_code"]){
		$data["status"] = 2;
		// $adminchk = getAdminFromInsurer($_POST["insuere"]); 
		// $data["admin_code"] = $adminchk;
		// $comm["comment_to"] = $adminchk;
		$data["admin_code"] = $noti["admin_code"];
		$comm["comment_to"] = $noti["admin_code"];
		$comm["comment"] = "แก้ไขงาน ส่งแจ้งงานอีกครั้ง";	
	}else{
		$data["status"] = 0;
		$data["admin_code"] = "";
		$comm["comment_to"] = $noti["chk_code"];
		$comm["comment"] = "แก้ไขงานส่งตรวจอีกครั้ง";	
	}
	$rs = $conn2->AutoExecute("noti_work", $data, 'UPDATE', "noti_work_code = '".$_POST["code"]."'");
	$comm["noti_work_id"] = $noti["noti_work_id"];
	$comm["noti_work_code"] = $_POST["code"];
	$comm["datetime"] = date("Y-m-d H:i:s");
	$comm["comment_by"] = $_SESSION["User"]['UserCode'];
	$comm["comment_byname"] = $_SESSION["User"]['firstname']." ".$_SESSION["User"]['lastname'];
	// echo "<pre>".print_r($comm,1)."</pre>"; exit(); 
	$conn2->AutoExecute("noti_comment", $comm, 'INSERT'); 


	echo $rs;
	exit();

}else if($_POST["action"] == "updateAdminInser"){
	$rs = $conn2->AutoExecute("ck_insurer", $_POST, 'UPDATE', "insurer_id = '".$_POST["insurer_id"]."'");
	echo $rs;
	exit();
	
}else if($_POST["action"] == "updateWorkInOut"){
	$rs = $conn2->AutoExecute("ck_personnel", $_POST, 'UPDATE', "personnel_code = '".$_POST["personnel_code"]."'");
	echo $rs;
	exit();

}else if($_POST["action"] == "addCommentList"){
	$_POST["datetime"] = date("Y-m-d H:i:s");
	$_POST["post_by"] = $_SESSION["User"]['UserCode'];
	$insertSQL = $conn2->AutoExecute("comment_lists", $_POST, 'INSERT'); 
	echo $insertSQL;
	exit();

}else if($_POST["action"] == "delCommentList"){
	$rs = $conn2->Execute("DELETE FROM comment_lists WHERE comment_lists_id = '".$_POST["id"]."'" );
	echo 1;
	exit();

}else if($_POST["action"] == "updateCommentList"){
	$rs = $conn2->AutoExecute("comment_lists", $_POST, 'UPDATE', "comment_lists_id = '".$_POST["comment_lists_id"]."'");
	echo rs;
	exit();

}else if($_POST["action"] == "addCommentStatus"){
	$_POST["datetime"] = date("Y-m-d H:i:s");
	$_POST["post_by"] = $_SESSION["User"]['UserCode'];
	$insertSQL = $conn2->AutoExecute("comment_status", $_POST, 'INSERT'); 
	echo $insertSQL;
	exit();

}else if($_POST["action"] == "delCommentStatus"){
	$rs = $conn2->Execute("DELETE FROM comment_status WHERE comment_status_id = '".$_POST["id"]."'" );
	echo 1;
	exit();

}else if($_POST["action"] == "updateCommentStatus"){
	$rs = $conn2->AutoExecute("comment_status", $_POST, 'UPDATE', "comment_status_id = '".$_POST["comment_status_id"]."'");
	echo rs;
	exit();

}else if($_POST["action"] == "addMoveQC"){
	// echo "<pre>".print_r($_POST,1)."</pre>";
	$data = array();
	foreach ($_POST["chktxt"] as $key => $value) {
		$code = explode("_", $value);
		if($code[2] == '2'){
			$data["admin_code"] = $_POST["newqcCode"];
		}else{
			$data["chk_code"] = $_POST["newqcCode"];
		}
		// echo "<br>".$code[1]." - ".$code[0]." - ".$code[2];
		$rs = $conn2->AutoExecute("noti_work", $data, 'UPDATE', "noti_work_id = '".$code[1]."' AND noti_work_code = '".$code[0]."' ");
	}
	echo "<script>alert('แก้ไขข้อมูลเรียบร้อย');</script>";
	echo '<META http-equiv="refresh" content="0;URL=../moveleadqc.php">';
	exit();

}else if($_POST["action"] == "changStatusGift"){
	$_POST["status_giftvoucher"] = "1";
	$rs = $conn2->AutoExecute("noti_work", $_POST, 'UPDATE', "noti_work_id = '".$_POST["id"]."'");
	echo rs;
	exit();

}elseif ($_POST["action"] == "addRequest") {
	// $CP_ID = getComparePolicyID();
	$year = date("Y");
	$date = date('Y-m-d H:i:s');
	$UserCode = $_SESSION["User"]['UserCode'];
	$sql = "SELECT MAX(Request_ID) AS Request_ID From [dbo].Insurance_Request WHERE year(Create_Date) = '".$year."'";
	$stmt = sqlsrv_query( $connMS, $sql );
  if(sqlsrv_has_rows($stmt)) {
    $row = sqlsrv_fetch_array($stmt);
    $row["Request_ID"];
    $txt = "IR".(date("y")+43);
    $idex = explode($txt, $row["Request_ID"]);
    $Request_ID = $txt."".str_pad(intval($idex[1])+1,6,"0",STR_PAD_LEFT);
  }  

	$sql = "INSERT INTO [dbo].[Insurance_Request] ([Request_ID], [Request_Date], [PO_ID], [Remark], [Active], [Create_Date], [Create_By], [Update_Date], [Update_By])
	VALUES ( '".$Request_ID."', '".date('Y-m-d')."', '".$_POST["pocode"]."', '', 'Y', '".$date."',  '".$UserCode."',  '".$date."', '".$UserCode."')";
  $stmt = sqlsrv_query( $connMS, $sql);
  echo $_POST["pocode"];
	exit();


}elseif ($_POST["action"] == "sendMailInsur") {
	 // echo "<pre>".print_r($_POST,1)."</pre>";
	 // echo "<pre>".print_r($_FILES,1)."</pre>";
	$mailInsur = mailInsur($_POST, $_FILES);
	$arr["datetime"] = date("Y-m-d H:i:s");
	$arr["noti_work_code"] = $_POST["noti_work_code"];
	$arr["po_code"] = $_POST["po_code"];
	$arr["user_send"] = $_SESSION["User"]['UserCode'];
	$arr["email"] = json_encode($_POST["mailsend"]);
	$arr["bodymail"] = $_POST["bodymail"];
	$arr["status_send"] = $mailInsur;
	$insertSQL = $conn2->AutoExecute("log_mail_admin", $arr, 'INSERT'); 

	$file = "../pdffile/ADBIR-".$_POST["po_code"].".pdf";
  unlink($file);
  
  if($mailInsur){
		echo "<script>alert('ส่งอีเมล์เรียบร้อย กรุณาเช็คอีเมล์อีกครั้งเพื่อความถูกต้อง');</script>";
		echo '<META http-equiv="refresh" content="0;URL=../ordersadmins.php">';
		exit();
	}else{
		echo "<script>alert('ไม่สามารถส่งเมล์ได้ กรุณาลองใหม่');</script>";
		echo '<META http-equiv="refresh" content="0;URL=../admin_email.php?code='.$_POST["noti_work_code"].'">';
		exit();
	}
	
	exit();

}elseif ($_POST["action"] == "addEmailInsur") {

	$arr["ck_insurer_key"] = $_POST["chk_code"];
	$arr["email"] = $_POST["email"];
	$arr["name"] = $_POST["name"];
	$arr["enable"] = 1;
	$insertSQL = $conn2->AutoExecute("insur_email", $arr, 'INSERT'); 
	echo $insertSQL;
	exit();

}elseif ($_POST["action"] == "updateStatusEmailInsur") {
	$arr["enable"] = $_POST["enable"] ;
	$rs = $conn2->AutoExecute("insur_email", $arr, 'UPDATE', "insur_email_id = '".$_POST["insur_email_id"]."'");
	echo $rs;
	exit();


}elseif ($_POST["action"] == "updateStatusCC") {
	$arr["setting_cc"] = $_POST["enable"] ;
	$rs = $conn2->AutoExecute("insur_email", $arr, 'UPDATE', "insur_email_id = '".$_POST["insur_email_id"]."'");
	echo $rs;
	exit();

}elseif ($_POST["action"] == "fnUpdateActiveSend") {
	$arr["active_send"] = $_POST["enable"] ;
	$rs = $conn2->AutoExecute("insur_email", $arr, 'UPDATE', "insur_email_id = '".$_POST["insur_email_id"]."'");
	echo $rs;
	exit();

}elseif ($_POST["action"] == "fnUpdateActiveCancel") {
	$arr["active_cancel"] = $_POST["enable"] ;
	$rs = $conn2->AutoExecute("insur_email", $arr, 'UPDATE', "insur_email_id = '".$_POST["insur_email_id"]."'");
	echo $rs;
	exit();
	
}elseif ($_POST["action"] == "delStatusEmailInsur") {
	$conn2->Execute("DELETE FROM insur_email WHERE insur_email_id = '".$_POST["id"]."'" );
	echo 1;
	exit();

}else if($_POST["action"] == "insercallNotice"){

	$getPO = getPurchaseBycode($_POST["poid"]);
	$Properties = $getPO["Properties_Insured"]." บริษัทประกันเพิ่ม เลขรับแจ้งวันที่ ".date('d/m/Y H:i:s')." เลขที่ : ".$_POST["Notice_No"];

	$sqlPO = "UPDATE [dbo].[Purchase_Order] SET [Properties_Insured] = '".$Properties."', [Admin_Status_ID] = 'IPI' WHERE PO_ID = '".$_POST["poid"]."' ";
	$stmt1 = sqlsrv_query( $connMS, $sqlPO);

	$sqlPOI = "UPDATE [dbo].[Purchase_Order_Insurer] SET [Get_Notice_Date] = '".date('Y-m-d H:i:s')."', [Notice_No] = '".$_POST["Notice_No"]."' WHERE PO_ID = '".$_POST["poid"]."' ";
	$stmt2 = sqlsrv_query( $connMS, $sqlPOI);
	if($stmt2){
		echo 1;
	}else{
		echo 0;
	}
	exit();

}


?>