<?php

// if(session_status() === PHP_SESSION_NONE){
  session_start();
// }
header('Content-Type: text/html; charset=utf-8');

function dateThai($date, $upMonth){

  $thaimonth = array("","มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม"); 
  $dayex = explode("-", $date);   
  if($upMonth){
    $date = date("Y-n-d", strtotime("+".$upMonth." day", strtotime($date) ));
  }else{
    $date;
  }
  // echo date("n",strtotime("+".$upMonth." day", $date));
  $day = date("j",strtotime($date));
  $month = $thaimonth[date("n",strtotime($date))];
  $year = $dayex[0]+543;
  return  "$day $month $year"; 
}

function dateThaiDown($date, $upMonth){

  $thaimonth = array("","มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม"); 
  $dayex = explode("-", $date);   
  if($upMonth){
    $date = date("Y-n-d", strtotime("+".$upMonth." day", strtotime($date) ));
  }else{
    $date;
  }
  // echo date("n",strtotime("+".$upMonth." day", $date));
  $day = date("j",strtotime($date));
  $month = $thaimonth[date("n",strtotime($date))];
  $year = $dayex[0]+543;
  
  if($day > 5 && $day <= 15 ){
    $day = 5;
  }else if($day >= 16 && $day <= 25){
    $day = 15;
  }else if($day >= 26 && $day <= 31){
    $day = 25;
  }else if($day >= 1 && $day <= 5){
    $day = 25;
    $m = date("n",strtotime($date))-1;
    if($m <= 0){
      $m = 12;
      $year = $year - 1 ;
    }
    $month =  $thaimonth[$m];
  }
  return  "$day $month $year"; 
}


function setSessionTime($_timeSecond){
 	if(!isset($_SESSION['ses_time_life'])){
  		$_SESSION['ses_time_life']=time();
	}
	if(isset($_SESSION['ses_time_life']) && time()-$_SESSION['ses_time_life']>$_timeSecond){
	  	if(count($_SESSION)>0){
		   foreach($_SESSION as $key=>$value){
		    	unset($key);
		    	unset($_SESSION[$key]);
		   	}
		   	// header("Refresh:0; url=".$_SERVER['PHP_SELF']);
		   	// echo '<meta http-equiv="Refresh" content="0;' .$_SERVER['PHP_SELF']. '">';
		   	// echo "<script>alert('เนื่องจากไม่ได้ใช้งานเป็นเวลานาน กรุณาเข้าระบบใหม่');</script>";
			// echo '<META http-equiv="refresh" content="0;URL=login.php">';
			exit();
		}
	}
}

function getsuplist(){
  global $conn2;
  $suplist = $conn2->GetAll("SELECT personnel_code, concat(personnel_firstname,' ', personnel_lastname) AS sup_name FROM ck_personnel WHERE personnel_type = 'SuperSale' AND personnel_status = '1' ORDER BY personnel_code ASC");
  return $suplist;
}

function insertLogin($arrlog){
    global $conn2;
   	$insertSQL = $conn2->AutoExecute("log_chkinout", $arrlog, 'INSERT'); 
    // echo "<pre>".print_r($class,1)."</pre>";
    return $insertSQL;
} 
function getClass(){
    global $conn2;
    $class = $conn2->GetAll("SELECT * FROM ck_insurance_class ");
    // echo "<pre>".print_r($class,1)."</pre>";
    return $class;
} 

function getMake(){
    global $conn2;
    $make = $conn2->GetAll("SELECT DISTINCT redbook_tks_make FROM ck_redbook ORDER BY redbook_tks_make ASC");
    return $make;
} 


function getGeneration($brand){
    global $conn2;
    $model = $conn2->GetAll("SELECT DISTINCT redbook_tks_model FROM ck_redbook WHERE redbook_tks_make = '".$brand."' ORDER BY redbook_tks_model ASC");
    return $model;
} 

function getInsurer(){
	global $conn2;
    $sql = "SELECT * FROM ck_insurer WHERE status = '1' ORDER BY insurer_name ASC" ;
    $act = $conn2->GetAssoc($sql);
    return $act;
} 

function getInsurerAll(){
  global $conn2;
    $sql = "SELECT * FROM ck_insurer ORDER BY insurer_name ASC" ;
    $act = $conn2->GetAssoc($sql);
    return $act;
} 

function getRowInsurer($name){
	global $conn2;
    $sql = "SELECT * FROM ck_insurer WHERE insurer_name = '".$name."' " ;
    $act = $conn2->GetRow($sql);
    return $act;
} 


function getRowInsurance($id){
	global $conn2;
    $sql = "SELECT * FROM ck_insurance WHERE insurance_id = '".$id."' " ;
    $act = $conn2->GetRow($sql);
    // echo "<pre>".print_r($act,1)."</pre>";
    return $act;
} 

function getRedbook($make, $model, $year, $cc ){
    global $conn2;
    $make = $conn2->GetRow("SELECT * FROM ck_redbook WHERE redbook_tks_make = '".$make."' AND redbook_tks_model = '".$model."' AND redbook_tks_yeargroup = '".$year."' AND redbook_tks_cc = '".$cc."' ");
    // echo "<pre>".print_r($make,1)."</pre>";
    return $make;
}

function getCC($brand, $model, $year){
	global $conn2;
    $sql = "SELECT DISTINCT(redbook_tks_cc) FROM ck_redbook WHERE redbook_tks_make='".$brand."' AND redbook_tks_model='".$model."' AND redbook_tks_yeargroup='".$year."' ORDER BY redbook_tks_cc ASC";
    $act = $conn2->GetAll($sql);
    return $act;
}

function calDiscount($tax, $premium, $val){
	$rs = ceil(($tax - (($premium * $val) / 100)) / 100) * 100;
	return $rs;
}

function getProtect($protect_id, $protect_type){
	global $conn2;
    $sql = "SELECT * FROM ck_insurance_protect INNER JOIN ck_protect ON ck_insurance_protect.protect_name = ck_protect.protect_id where ck_insurance_protect.insurance_id = '".$protect_id."' AND ck_protect.protect_type = '".$protect_type."' ";
    $act = $conn2->GetAll($sql);
    return $act;
}

function getRowInsur($planid, $costid){
	global $conn2;
    $sql = "SELECT ck_insurance.*, ck_insurance_cost.* FROM ck_insurance INNER JOIN ck_insurance_cost ON ck_insurance.insurance_id = ck_insurance_cost.insurance_id WHERE  ck_insurance.insurance_id = '".$planid."' AND ck_insurance_cost.inscost_id = '".$costid."'";
    $act = $conn2->GetRow($sql);
    return $act;
}

function getProtectForInsurid($insurance_id){
	global $conn2;
    $sql = "SELECT ck_protect.*, ck_insurance_protect.protect_cost FROM ck_insurance_protect INNER JOIN ck_protect ON ck_insurance_protect.protect_name = ck_protect.protect_id WHERE ck_insurance_protect.insurance_id = '".$insurance_id."' ";
    $act = $conn2->GetAll($sql);
    return $act;
}

function getAllCustomerMain($sale){
  global $conn2;
  $userType = $_SESSION["User"]['type'];

  if($userType == "Sale"){
      $sql = "SELECT * FROM log_api INNER JOIN ck_personnel ON log_api.personnel_code = ck_personnel.personnel_code WHERE log_api.personnel_code = '".$sale."' ORDER BY created_date DESC limit 0,300";
    }else{
      $sql = "SELECT * FROM log_api ORDER BY created_date DESC limit 0,300";
    }
    $act = $conn2->GetAll($sql);
    return $act;
}

function getAllCustomerCase($pocode){
	global $conn2;
    $sql = "SELECT * FROM log_api WHERE pocode = '".$pocode."' ";
    $act = $conn2->GetROW($sql);
    return $act;
}

function getAllSale($code){
	global $conn2;
    $sql = "SELECT * FROM ck_personnel WHERE personnel_code = '".$code."' ";
    $act = $conn2->GetROW($sql);
    return $act;
}

function getCountdown($api_id){
	global $conn2;
    $sql = "SELECT sum(showdown) FROM log_apilists WHERE api_id = '".$api_id."' ";
    $act = $conn2->GetOne($sql);
    return $act;
}


function getCountinstall($apiList){
	global $conn2;
    $sql = "SELECT sum(installment) FROM log_apilists WHERE api_id = '".$apiList."'  ";
    $act = $conn2->GetOne($sql);
    return $act;
}

function getCountACT($api_id){
	global $conn2;
    $sql = "SELECT sum(actprice) FROM log_apilists WHERE api_id = '".$api_id."' ";
    $act = $conn2->GetOne($sql);
    return $act;
}

function getCountapilist($api_id){
	global $conn2;
    $sql = "SELECT count(apilists_id) FROM log_apilists WHERE api_id = '".$api_id."' ";
    $act = $conn2->GetOne($sql);
    return $act;
}

function getDown1($id){
	global $conn2;
    $sql = "SELECT price FROM log_apil_dowsprice WHERE log_apilists_id = '".$id."' AND periods = '1' ";
    $act = $conn2->GetOne($sql);
    return $act;
}

function getDown2($id){
	global $conn2;
    $sql = "SELECT price FROM log_apil_dowsprice WHERE log_apilists_id = '".$id."' AND periods = '2' ";
    $act = $conn2->GetOne($sql);
    return $act;
}

function getAllCustomerlist($pocode, $api_id){
	global $conn2;
   	$log_apilists = $conn2->GetAll("SELECT * FROM log_apilists WHERE api_id = '".$api_id."' AND api_pocode = '".$pocode."' ");
    return $log_apilists;
}


function getAllPersonnel(){
	global $conn2;
  $ck_personnel = $conn2->GetAll("SELECT a.*, (SELECT CONCAT(personnel_firstname, ' ',personnel_lastname) FROM ck_personnel WHERE personnel_code = a.personnel_supervisor) AS sup_name
                                  FROM ck_personnel a 
                                  ORDER BY personnel_code DESC");
  // $ck_personnel = $conn2->GetAll("SELECT * FROM ck_personnel ORDER BY personnel_code DESC");
  return $ck_personnel;
}

function getPersonnel($id){
	global $conn2;
   	$ck_personnel = $conn2->GetAll("SELECT * FROM ck_personnel WHERE personnel_id = '".$id."' ");
    return $ck_personnel;
}

function getPersonnelCode($code){
	global $conn2;
   	$ck_personnel = $conn2->GetRow("SELECT * FROM ck_personnel WHERE personnel_code = '".$code."' ");
    return $ck_personnel;
}

function getlistapiDown($id){
    global $conn2; 
    $dowsprice = $conn2->GetAll("SELECT * FROM log_apil_dowsprice WHERE log_apilists_id = '".$id."' ORDER BY periods ASC");
    // echo "<pre>".print_r($make,1)."</pre>";
    return $dowsprice;
}

function getlistIns(){
	$otherData = array();
	$i = 0;
	foreach($_SESSION['inslist']['planid'] as $key => $value) {
		$value = getRowInsur($_SESSION['inslist']['planid'][$key], $_SESSION['inslist']['costid'][$key]);
		// echo "<pre>".print_r($value,1)."</pre>";
		$id = $value['insurance_id'];
		if($value["salecost"]){
	        $premium = $value["inscost_taxamount"] - $value["salecost"];
	        $discountSum = $value["salecost"];
		}else if($value["percen_sale"] != 0){
	        $discount = ($value["inscost_premamount"]  * $value["percen_sale"] ) / 100;
	        $premium = ceil(($value["inscost_taxamount"] - $discount) / 100) * 100;
	        $discountSum = $value["inscost_taxamount"] - $premium;
	    }else{
	        $premium = $value["inscost_taxamount"];
	    }
	    $otherData[$i]["insurance_id"] = $value['insurance_id'];
        $otherData[$i]["inscost_id"] = $value['inscost_id'];
		$otherData[$i]["insurance_Name"] = $value['insurance_Name'];
		$otherData[$i]["insurance_insurer"] = $value['insurance_insurer'];
		$otherData[$i]["inscost_minamount"] = $value['inscost_minamount'];
		$otherData[$i]["inscost_maxamount"] = $value['inscost_maxamount'];
		$otherData[$i]["inscost_premamount"] = $value['inscost_premamount'];
		$otherData[$i]["inscost_taxamount"] = $value['inscost_taxamount'];
		$otherData[$i]["discount"] = $discountSum;
		$otherData[$i]["payper"] = $premium;
    	// $resultarray['data']  	= $otherData;
    	$i++;
	}
	return $otherData;
}

function chkCodePO(){
	global $conn2;
	$act = $conn2->GetOne("SELECT count(api_id) AS count FROM log_api WHERE date(created_date) = '".date("Y-m-d")."' ");
	$num =  'A'.date("ymd")."".sprintf("%03d", $act+1);
	$ck = $conn2->GetOne("SELECT api_id FROM log_api WHERE pocode= '".$num."' ");
	if(!$ck){
		return $num;
	}else{
		return $num+1;
	}
}

function getTextBetweenTags($string, $tagname) {
    $pattern = "/<$tagname>(.*?)<\/$tagname>/";
    preg_match($pattern, $string, $matches);
    return $matches[1];
}

function getClassid($class){
  global $conn2;
    // if($class == "1"){
    //     $rs = "036";
    // }else if($class == "2"){
    //     $rs = "037"; 
    // }else if($class == "2+"){
    //     $rs = "039"; 
    // }else if($class == "3+"){
    //     $rs = "030"; 
    // }else if($class == "3"){
    //     $rs = "038"; 
    // }
    $ck = $conn2->GetOne("SELECT class_code FROM ck_insurance_class WHERE class_name = '".$class."' ");
    return $ck;
}

function curlSenddataInsur($arr){
    include "../lib/nusoap.php"; 
    // $client = new nusoap_client("http://103.13.30.141/iBrokerService/WebLeadService/WebLeadServiceII.asmx?wsdl",true); 
    // $client = new nusoap_client("http://103.13.30.141/iBrokertest/WebLeadService/WebLeadServiceIII.asmx?wsdl",true); 
    $client = new nusoap_client("http://103.13.30.141/iBrokerService/WebLeadService/WebLeadServiceIII.asmx?wsdl ",true);
 // echo getClassid($arr["data1"]["class"]);
    $province = getallProvinceID($arr["data1"]["plate_province"]);
    $i = 1;
    // if($arr["data1"]["callnew"] == "casenew"){
    //   if($arr["data1"]["callcase"] == 0){
    //     $personnel = $arr["data1"]["personnel_code"];
    //   }else if($arr["data1"]["callcase"] == 1){
    //     $personnel = "";
    //   } 
    // }
    if($arr["data1"]["callnew"] == "casenew"){
      if($arr["data1"]["newfrom"] != "Claim"){
        if($arr["data1"]["callcase"] == 0){
          $personnel = $arr["data1"]["personnel_code"];
        }else if($arr["data1"]["callcase"] == 1){
          $personnel = "";
        } 
      }else{
          $personnel = "ADB61018";
      }
    }
 // echo "<pre>".print_r($arr,1)."</pre>"; exit();  
   	$params2 = "<SourceName>".$arr["data1"]["newfrom"]."</SourceName>
   	<Campaign>CheckPrice</Campaign>
   	<RefCPID>".$arr["data1"]["cpid"]."</RefCPID>
   	<Name>".$arr["data1"]["name"]."</Name>
   	<TelNo>".$arr["data1"]["phone"]."</TelNo>
   	<eMail>".$arr["data1"]["email"]."</eMail>
   	<LineID>".$arr["data1"]["line"]."</LineID>
   	<Registration>AsiaDirect</Registration>
   	<AssignTo>".$personnel."</AssignTo>
   	<CarMake>".$arr["data1"]["make"]."</CarMake>
   	<CarModel>".$arr["data1"]["model"]."</CarModel>
   	<CarModelDesc>".$arr["data1"]["CarModelDesc"]."</CarModelDesc>
   	<CarCC>".str_replace('cc', '', $arr["data1"]["cc"])."</CarCC>
   	<CarRegistYear>".$arr["data1"]["year"]."</CarRegistYear>
   	<CarType>".$arr["data1"]["cartype"]."</CarType>
    <Plate_No>".$arr["data1"]["plate_no"]."</Plate_No>
    <Plate_Province>".$province["province_id"]."</Plate_Province>
    <Body_Type>".$arr["data1"]["body_type"]."</Body_Type>
    <Frame_No>".$arr["data1"]["frame_no"]."</Frame_No>
    <Start_Cover_Date>".$arr["data1"]["start_cover_date"]."</Start_Cover_Date>
    <End_Cover_Date>".$arr["data1"]["end_cover_date"]."</End_Cover_Date>
    <Remark>".$arr["data1"]["remark"]."</Remark>
    <Is_Replace>".$arr["data1"]["isReplace"]."</Is_Replace>";

    if($arr["data2"]){
        foreach ($arr["data2"] as $key => $data2) {
            $repair = ($data2["repairs"]=='hall') ?  "C" : "G";

            $params2 .="<Insurer_".$i.">".$data2["insurername"]."</Insurer_".$i.">
            <Package_Code_".$i.">".$data2["package_code"]."</Package_Code_".$i.">
            <Insurance_ID_".$i.">".getClassid($data2["class"])."</Insurance_ID_".$i.">
            <Package_Name_".$i.">".$data2["package_name"]."</Package_Name_".$i.">
            <RepairType_".$i.">".$repair."</RepairType_".$i.">
      	    <SumInsure_".$i.">".$data2["inscost"]."</SumInsure_".$i.">
      	    <Deduct_".$i.">".$data2["deduct"]."</Deduct_".$i.">
      	    <NetPremium_".$i.">".$data2["netpremium"]."</NetPremium_".$i.">
      	    <Discount_".$i.">".$data2["discount"]."</Discount_".$i.">
      	    <PremiumAfterDisc_".$i.">".$data2["taxamount"]."</PremiumAfterDisc_".$i.">
      	    <Installment_".$i.">".$data2["installment"]."</Installment_".$i.">
      	    <PaymentAmountPerInst_".$i.">".$data2["premium"]."</PaymentAmountPerInst_".$i.">
      	    <RegistrationDateTime_".$i.">".date("Y-m-d H:i:s")."</RegistrationDateTime_".$i.">
      	    <NetCompulsory_".$i.">".$data2["actprice"]."</NetCompulsory_".$i.">
      	    <C502_".$i.">".$data2["C502"]."</C502_".$i.">
      	    <C503_".$i.">".$data2["C503"]."</C503_".$i.">
      	    <C504_".$i.">".$data2["C504"]."</C504_".$i.">
      	    <C536_".$i.">".$data2["C536"]."</C536_".$i.">
      	    <C537_".$i.">".$data2["C537"]."</C537_".$i.">
      	    <C507_".$i.">".$data2["C507"]."</C507_".$i.">
      	    <C517_".$i.">".$data2["C517"]."</C517_".$i.">
      	    <C525_".$i.">".$data2["C525"]."</C525_".$i.">
      	    <C532_".$i.">".$data2["C532"]."</C532_".$i.">
      	    <NoOfPax_".$i.">".$data2["no_ofpax"]."</NoOfPax_".$i.">";
	      	// echo $value["package_name"];
			$i++;
	    }
	}
    $params = array('myRequestString' => $params2); 
    $data = $client->Call("ImportWebLead", $params); 
    return $data ;
//    echo json_encode($params2);
//     echo "<pre>".print_r($data,1)."</pre>";
// exit(); 
}

function sendMailForContact($arr){ 
    $personnel_code = $arr["data1"]["personnel_code"];
    $sale = getAllSale($personnel_code);
	foreach ($arr["data2"] as $key => $data2) {
		$caseactprice += $data2['actprice'];
		$countdown += $data2['showdown'];
		$countInstall += $data2['installment'];
	}	
	// echo $caseactprice ;
	$mail = new PHPMailer\PHPMailer\PHPMailer(true);
    

// echo "<pre>".print_r($arr,1)."</pre>";

	$bodyMail = "
        <div style='width:1200px;font-size: 13px; color:#000;'>
            <div style='border-bottom: 2px solid #f7941e; background-color: #0aaaef;color: #fff;padding: 15px;font-size: 15px;text-align: center;'>
                <div style='text-align: center; padding-top: 10px;'>
                    <img src='https://www.asiadirect.co.th/image/logo/logo_110.png' style='width: 70px;'>
                </div>
                <div>
                    <strong>บริษัท เอเชียไดเร็ค อินชัวร์รันส์ โบรคเกอร์ จำกัด</strong><br> เลขที่ 626 อาคารบีบีดี ชั้น 11 ซอยจินดาถวิล ถนนพระราม 4 แขวงมหาพฤฒาราม เขตบางรัก กรุงเทพฯ 10500<br> เปิดให้บริการ วันจันทร์-วันศุกร์ เวลา 8.30-17.00 น.
                </div>
            </div>
            <div style='padding: 30px;font-size: 14px; color:#000;'>
                <div style='text-indent: 30px; font-size:14px;'>เรียนคุณ ".$arr["data1"]["name"]. " บริษัท เอเชียไดเร็ค อินชัวร์รันส์ โบรคเกอร์ จำกัด ขอขอบคุณท่านเป็นอย่างยิ่ง ที่ไว้วางใจใช้บริการของเรา ทางเจ้าหน้าที่ได้รับรายละเอียดข้อมูลรถของท่านเรียบร้อย ทางบริษัทมีความประสงค์ขอยื่นข้อเสนอประกันภัยให้แก่ท่านโดยมีรายละเอียดที่ปรากฏในอีเมล์ฉบับนี้ หากท่านมีความต้องการเปลี่ยนแปลงหรือพบข้อสงสัยในรายละเอียด สามารถติดต่อที่เบอร์ 02-089-2000 </div>
        		<div style=''>
                    <div style='clear: both; '>
                        <div style='margin-top: 20px;border-bottom: 2px solid #ffffff;padding:10px;'>
                            <div style='border-top: 2px solid #f7941e;padding: 10px 0;'>
                                <b style='color:#000;'>ข้อมูลประกันผู้เอาประกัน </b>
                                <span style='float: right;'>".date("Y-m-d H:i:s")."</span>
                                <br><b>คุณ</b> ".$arr["data1"]["name"]." 
                                <b>เบอร์ </b> ".$arr["data1"]["phone"]." <b>อีเมล</b> ".$arr["data1"]["email"]."
                                <b style='color:#000;'><br>ยี่ห้อรถยนต์</b> ".$arr["data1"]["make"]." <b>รุ่นรถยนต์</b> ".$arr["data1"]['model']." 
                                <b>ขนาดเครื่องยนต์</b> ".$arr["data1"]['cc']."cc <b>ปีจดทะเบียน</b> ".$arr["data1"]['year']."
                            </div>
                            <p style='background: #ffcc90;padding: 5px 0;margin-top: 0px;'>
                                <b>พนักงานขาย</b> ".$sale["personnel_firstname"]." ".$sale["personnel_lastname"]." 
                                <b>เบอร์</b> ".$sale["personnel_phone"]." <b>อีเมล์</b> ".$sale["personnel_email"]." <b>ไลน์</b> ".$sale["personnel_line"]."  
                            </p>
                        ";
                    if(!$arr["data2"]){
                    	$repair = ($arr["data1"]["repairs"]=='hall') ? "ซ่อมห้าง" : "ซ่อมอู่";
                        $bodyMail .= "<div style='padding: 15px;'><b>ประกันชั้น </b> ".$arr["data1"]['class']." <b>การซ่อม</b> ".$repair."<br><b>หมายเหตุ</b> ".$arr["data1"]['remark']."</div>";
                    }
                    $bodyMail .= "</div>";
            if($arr["data2"]){
        		$bodyMail .= "
        		<table style='font-size:14px;width:100%;background: #f9f8f8;' border='1' cellspacing='0'>
                	<thead>
						<tr style='background: #9e9e9e;'>
							<th style='width:40%;padding: 5px;'>เงื่อนไขความคุ้มครอง</th> ";
					foreach ($arr["data2"] as $key => $data2) {
						$bodyMail .= "<th style='width:20%'>ข้อเสนอ ".($key+1)."</th>";
					}
					$bodyMail .= "
						</tr>
					</thead>
					<tbody>
						<tr>
							<td style='text-align:right;padding: 5px;'><b>บริษัทประกัน</b></td>";
							foreach ($arr["data2"] as $key => $data2) {
								$bodyMail .= "<td style='padding: 5px;text-align: center;'>".$data2["insurername"]."</td>";
							}
					$bodyMail .= "
						</tr>
						<tr>
							<td style='text-align:right'><b>ประเภทประกัน</b></td>";
							foreach ($arr["data2"] as $key => $data2) {
								$bodyMail .= "<td style='padding: 5px;text-align: center;'> ประกันภัยชั้น ".$data2["class"]."</td>";
							}
					$bodyMail .= "
						</tr>
						<tr>
							<td style='text-align:right'><b>แพ็คเกจ</b></td>";
							foreach ($arr["data2"] as $key => $data2) {
								$bodyMail .= "<td style='padding: 5px;text-align: center;'>".$data2["package_name"]."</td>";
							}
					$bodyMail .= "
						</tr>
						<tr>
							<td style='text-align:right'><b>ประเภทการซ่อม</b></td>";
							foreach ($arr["data2"] as $key => $data2) { 
								$repair = ($data2["repairs"]=='hall') ? "ซ่อมห้าง" : "ซ่อมอู่";
								$bodyMail .= "<td style='padding: 5px;text-align: center;'>".$repair."</td>";
							}
					$bodyMail .= "
						</tr>
						<tr>
							<td style='text-align:center; padding: 5px;'><b>ความรับผิดต่อบุคคลภายนอก</b></td>
							<td colspan='3'></td>
						</tr>
						<tr>
							<td>1) ความเสียหายต่อชีวิต ร่างกาย หรืออนามัย (บาท/คน)</td>";
							foreach ($arr["data2"] as $key => $data2) { 
								$bodyMail .= "<td style='text-align:right; padding: 5px;'>".number_format($data2['C502'],2)."</td>";
							}
					$bodyMail .= "
						</tr>
						<tr>
							<td>ความเสียหายต่อชีวิต ร่างกาย หรืออนามัย (บาท/ครั้ง)</td>";
							foreach ($arr["data2"] as $key => $data2) { 
								$bodyMail .= "<td style='text-align:right; padding: 5px;'>".number_format($data2['C503'],2)."</td>";
							}
					$bodyMail .= "
						</tr>
						<tr>
							<td>2) ความเสียหายต่อทรัพย์สิน (บาท/ครั้ง)</td>";
							foreach ($arr["data2"] as $key => $data2) { 
								$bodyMail .= "<td style='text-align:right; padding: 5px;'>".number_format($data2['C504'],2)."</td>";
							}
					$bodyMail .= "
						</tr>
						<tr>
							<td>2.1) ความเสียหายส่วนแรก (บาท/ครั้ง)</td>";
							foreach ($arr["data2"] as $key => $data2) { 
								$bodyMail .= "<td style='text-align:right; padding: 5px;'>".number_format($data2['deduct'],2)."</td>";
							}
					$bodyMail .= "
						</tr>
						<tr>
							<td style='text-align:center; padding: 5px;'><b>รถยนต์เสียหาย สูญหาย ไฟไหม้</b></td>
							<td colspan='3'></td>
						</tr>";
					$bodyMail .= "
						</tr>
						<tr>
							<td>1) ความเสียหายต่อรถยนต์ (บาท/ครั้ง)</td>";
							foreach ($arr["data2"] as $key => $data2) { 
								$bodyMail .= "<td style='text-align:right; padding: 5px;'>".number_format($data2['C536'],2)."</td>";
							}
					$bodyMail .= "
						</tr>
						<tr>
							<td>2) ความเสียหายส่วนแรก (กรณีฝ่ายผิด) (บาท/ครั้ง)</td>";
							foreach ($arr["data2"] as $key => $data2) { 
								$bodyMail .= "<td style='text-align:right; padding: 5px;'>".number_format($data2['deduct'],2)."</td>";
							}
					$bodyMail .= "
						</tr>
						<tr>
							<td>3) รถยนต์สูญหาย/ไฟไหม้ (บาท)</td>";
							foreach ($arr["data2"] as $key => $data2) { 
								$bodyMail .= "<td style='text-align:right; padding: 5px;'>".number_format($data2['C507'],2)."</td>";
							}
					$bodyMail .= "
						</tr>
						<tr>
							<td style='text-align:center; padding: 5px;'><b>ความคุ้มครองตามเอกสารแนบท้าย</b></td>
							<td colspan='3'></td>
						</tr>";
					$bodyMail .= "
						</tr>
						<tr>
							<td>1) อุบัติเหตุส่วนบุคคล</td>";
							foreach ($arr["data2"] as $key => $data2) { 
								$bodyMail .= "<td style='text-align:right; padding: 5px;'>".number_format($data2['C517'],2)."</td>";
							}
					$bodyMail .= "
						</tr>
						<tr>
							<td>2) ค่ารักษาพยาบาล (คน) (บาท/คน)</td>";
							foreach ($arr["data2"] as $key => $data2) { 
								$bodyMail .= "<td style='text-align:right; padding: 5px;'>(".($data2['no_ofpax']+1).") ".number_format($data2['C525'],2)."</td>";
							}
					$bodyMail .= "
						</tr>
						<tr>
							<td>3) การประกันตัวผู้ขับขี่ (บาท/ครั้ง)</td>";
							foreach ($arr["data2"] as $key => $data2) { 
								$bodyMail .= "<td style='text-align:right; padding: 5px;'>".number_format($data2['C532'],2)."</td>";
							}
					$bodyMail .= "
						</tr>
						<tr class='bgea'>
							<td style='text-align:right;padding: 5px;'><b>เบี้ยสุทธิ</b></td>";
							foreach ($arr["data2"] as $key => $data2) { 
								$bodyMail .= "<td style='text-align:right; padding: 5px;'><b>".number_format($data2['netpremium'],2)."</b></td>";
							}
					$bodyMail .= "
						</tr>
						<tr>
							<td style='text-align:right;padding: 5px;'><b>เบี้ยประกันภัย</b></td>";
							foreach ($arr["data2"] as $key => $data2) { 
								$bodyMail .= "<td style='text-align:right; padding: 5px;'><b>".number_format($data2['premium'],2)."</b></td>";
							}
					$bodyMail .= "
						</tr>
						<tr>
							<td style='text-align:right;padding: 5px;'><b>ส่วนลด</b></td>";
							foreach ($arr["data2"] as $key => $data2) { 
								if($data2['showdiscount'] == 1){
									$bodyMail .= "<td style='text-align:right; padding: 5px;'><b>".number_format($data2['discount'],2)."</b></td>";
								}else{
									$bodyMail .= "<td style='text-align:right; padding: 5px;'><b>0.00</b></td>";
								} 
								
							}
					$bodyMail .= "
						</tr>
						<tr>
							<td style='text-align:right;padding: 5px;'><b>เบี้ยประกันภัยรวมหลังหักส่วนลด</b></td>";
							foreach ($arr["data2"] as $key => $data2) { 
								$bodyMail .= "<td style='text-align:right; padding: 5px;'><b>".number_format($data2['taxamount'],2)."</b></td>";
							}
					$bodyMail .= "</tr>";
					
					if($caseactprice>0){
					$bodyMail .= "<tr>
							<td style='text-align:right;padding: 5px;'><b>พรบ.</b></td>";
							foreach ($arr["data2"] as $key => $data2) { 
								$bodyMail .= "<td style='text-align:right; padding: 5px;'><b>".number_format($data2['actprice'],2)."</b></td>";
							}
						$bodyMail .= "</tr>";
					}

					$bodyMail .= "
						<tr>
							<td style='text-align:right;padding: 5px;'><b>เงินสด</b></td>";
							foreach ($arr["data2"] as $key => $data2) { 
								$bodyMail .= "<td style='text-align:right; padding: 5px;'><b>".number_format($data2['taxamount']+$data2['actprice'],2)."</b></td>";
							}
					$bodyMail .= "</tr>";
					// if($countdown>0){
					// 	$bodyMail .= "
					// 		<tr class='bgea'>
					// 			<td style='text-align:right;padding: 5px;'><b>ผ่อนเงินสด (งวด) งวดที่ 1<br> งวดที่ 2-3</b></td>";
					// 			foreach ($arr["data2"] as $key => $data2) { 
					// 				if($data2['showdown'] > 0){
					// 					// $bodyMail .= "<td style='text-align:right; padding: 5px;'><b>".number_format($data2['downcash_1'],2)."<br>".number_format($data2['downcash_2'],2)."</b></td>";
					// 					$bodyMail .= "<td style='text-align:right; padding: 3px;'><b>(".$data2['showdown'].") ".number_format($data2["downcash"][1]+$data2['actprice'],2)."<br>".number_format($data2["downcash"][2],2)."</b></td>";
					// 				}else{
					// 					$bodyMail .= "<td style='text-align:right; padding: 5px;'><b>-</b></td>";
					// 				}
					// 			}
					// 	$bodyMail .= "</tr>";
					// }
					if($countInstall>0){
						$bodyMail .= "
							</tr>
							<tr>
								<td style='text-align:right;padding: 5px;'><b>ผ่อน 0% สูงสุด (เดือน)</b></td>";
								foreach ($arr["data2"] as $key => $data2) { 
									if($data2['installment']>0){
										$bodyMail .= "<td style='text-align:right; padding: 5px;'><b>(".$data2['installment'].") ".number_format($data2['downs'],2)."</b></td>";
									}else{
										$bodyMail .= "<td style='text-align:right; padding: 5px;'><b>-</b></td>";
									}
								}
					}
					$bodyMail .= "
						</tr>
					</tbody>
                </table>   ";
                $bodyMail .= "</div>
        			<p style='padding:10px'> <b>หมายเหตุ</b>
	        			<br> - เอกสารประกอบการทำประกัน 1.สำเนากรมธรรม์เดิม 2.สำเนาบัตรประชาชน 3.สำเนาใบขับขี่(กรณีระบุชื่อ) 4.รูปถ่าย(กรณีประเภท1)
	        			<br> - หากลูกค้ามีการแจ้งเคลมหลังจากใบเสนอราคาฉบับนี้ เบี้ยประกันอาจมีการเปลี่ยนแปลง ซึ่งบริษัทจะแจ้งให้ทราบทันที
	        			<br> - แถมบริการช่วยเหลือฉุกเฉินบนท้องถนนฟรี 24 ชั่วโมง มูลค่าสูงสุด 2,900 บาท
	        			<br> - ผ่อนผ่านบัตรเครดิต กรุงไทย กสิกรไทย กรุงศรี โอมโปร เซ็นทรัล โลตัส เฟิร์สช๊อย
	        			<br> - กรณีผ่อนชำระ จะได้รับกรมธรรม์ฉบับจริงเมื่อชำระงวดสุดท้ายเรียบร้อยแล้ว
	        			<br> - บริษัทขอสงวนสิทธิ์ในการเปลี่ยนแปลงข้อมูล ทุนประกัน เบี้ยประกัน ความคุ้มครอง  โดยจะแจ้งให้ทราบในภายหลัง
        			</p>
                </div>";
	        }else{
	        	$bodyMail .= "</div>
	        	<div style='text-indent: 30px;font-size:14px;margin-top: 15px;'>เจ้าหน้าที่จาก เอเชียไดเร็คโบรคเกอร์ จะดำเนินการเช็คเบี้ยประกันตามข้อมูลที่ได้รับ หรือค้นหาเบี้ยประกันที่เหมาะสมกับ คุณ ".$arr["data1"]["name"]. " และติดต่อกลับโดยเร็วที่สุด ในวันและเวลาทำการ หรือสามารถติดต่อเราโดยตรงได้ที่เบอร์โทรศัพท์ 02-089-2000 </div>";
	        }
        $bodyMail .= "
                <div style='text-align: center;font-size:14px;margin-top: 30px;'><b>ขอบคุณที่ไว้วางใจ</b> <br>บริษัท เอเชียไดเร็ค อินชัวร์รันส์ โบรคเกอร์ จำกัด</div>
            </div>
            <div style='text-align: center;font-size: 14px;background-color: #0aaaef;color: #fff;border-top: 2px solid #f7941e;padding: 10px;'>
                <div>
                    <a href='https://www.facebook.com/AsiaDirectBroker/' style='margin-right: 5px;'><img src='https://www.asiadirect.co.th/image/icon/Facebook100.png' style='width: 100px'></a>
                    <a href='https://line.me/ti/p/%40bnl8398c' style='margin-right: 5px;'><img src='https://www.asiadirect.co.th/image/icon/Line100.png' style='width: 100px'></a>
                    <a href='https://www.asiadirect.co.th/adbnews.php?utm_source=Email&utm_campaign=CheckPrice'><img src='https://www.asiadirect.co.th/image/icon/Knowledge100.png' style='width: 100px'></a>
                </div>
                02-089-2000 | info@asiadirect.co.th | <a href='https://www.asiadirect.co.th'>www.asiadirect.co.th</a>
                <br>Copyright © 2017 Asia Direct Insurance Broker
            </div>
        </div>
        ";
   // echo $bodyMail; exit();
  $mail->CharSet = 'utf-8';
    $mail->SMTPDebug = 0;                                 // Enable verbose debug output
    $mail->isSMTP();                                      // Set mailer to use SMTP
    $mail->Host = 'smtp.gmail.com'; // Specify main and backup SMTP servers
    $mail->SMTPAuth = true;  
    $mail->Username = "admin@asiadirect.co.th";
    $mail->Password = "@db6251000";
    $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
    $mail->Port = 587;                                    // TCP port to connect to

    $emailSend = $_SESSION["User"]['email'] ? $_SESSION["User"]['email'] : "admin@asiadirect.co.th";
    $mail->setFrom($emailSend, 'Asia Direct');
    $mail->AddReplyTo("admin@asiadirect.co.th", "Asia Direct Info");
    $mail->AddReplyTo($emailSend, "Asia Direct Sale"); //sale

    if($arr["data1"]["email"]){
      $mail->AddAddress($arr["data1"]["email"], $arr["data1"]["name"]); //ลุกค้า
    }
    $mail->AddAddress($_SESSION["User"]['email'], "Asia Direct Sale"); //sale
    // $mail->AddBCC('teerakan.s@asiadirect.co.th', 'IT Asia');               // Name is optional
    $mail->isHTML(true);                                  // Set email format to HTML
    $mail->Subject = "ข้อเสนอประกันภัยรถยนต์จาก เอเชียไดเร็คโบรคเกอร์";
    $mail->Body    = $bodyMail;

	if(!$mail->Send()) {
		return 0;
	} else {
		return 1;
	}
}


function getRoadsideAll($insurance_Name){
  global $conn2;
    $roadsides = $conn2->GetAll("SELECT * FROM roadsides WHERE insurance_Name = '".$insurance_Name."' AND status = '1'  ORDER BY num ASC ");
    return $roadsides;
}


function getINSRoadside(){
  global $conn2;
    $roadsides = $conn2->GetAll("SELECT * FROM roadsides WHERE status = '1' GROUP BY insurance_Name ORDER BY insurance_Name ASC");
    return $roadsides;
}


/* *******************************  Function For MS SQL ****************************  */
function getTitle(){
  global $connMS;
  $year = date("Y");
  $sql = "SELECT * From [dbo].Title WHERE Active = 'Y' ORDER BY Title_ID ASC  ";
  $stmt = sqlsrv_query( $connMS, $sql );
  if(sqlsrv_has_rows($stmt)) {
      while( $row = sqlsrv_fetch_array($stmt) ) { 
        $resultarray[] = $row;
      }
    return $resultarray;
    exit();
  }
}


function getMyUserAll(){
  global $connMS;
  $year = date("Y");
  $sql = "SELECT * From [dbo].My_User ORDER BY User_ID ASC ";
  $stmt = sqlsrv_query( $connMS, $sql );
  if(sqlsrv_has_rows($stmt)) {
      while( $row = sqlsrv_fetch_array($stmt) ) { 
        $resultarray[] = $row;
      }
    return $resultarray;
    exit();
  }
}

function getInsuranceGroup(){
  global $connMS;
  $year = date("Y");
  $sql = "SELECT * From [dbo].Insurance_Group WHERE Active = 'Y'  ";
  $stmt = sqlsrv_query( $connMS, $sql );
  if(sqlsrv_has_rows($stmt)) {
      while( $row = sqlsrv_fetch_array($stmt) ) { 
        $resultarray[] = $row;
      }
    return $resultarray;
    exit();
  }
}

function getFollowupStatus(){
  global $connMS;
  $sql = "SELECT * From [dbo].Followup_Status ";
  $stmt = sqlsrv_query( $connMS, $sql );
  if(sqlsrv_has_rows($stmt)) {
      while( $row = sqlsrv_fetch_array($stmt) ) { 
        $resultarray[] = $row;
      }
      return $resultarray;
      exit();
  }
}

function getFollowupType(){
  global $connMS;
  $sql = "SELECT * From [dbo].Followup_Type WHERE Active = 'Y'";
  $stmt = sqlsrv_query( $connMS, $sql );
  if(sqlsrv_has_rows($stmt)) {
      while( $row = sqlsrv_fetch_array($stmt) ) { 
        $resultarray[] = $row;
      }
      return $resultarray;
      exit();
  }
}

function getRejectReason(){
  global $connMS;
  $sql = "SELECT * From [dbo].Reject_Reason WHERE Active = 'Y'";
  $stmt = sqlsrv_query( $connMS, $sql );
  if(sqlsrv_has_rows($stmt)) {
      while( $row = sqlsrv_fetch_array($stmt) ) { 
        $resultarray[] = $row;
      }
      return $resultarray;
      exit();
  }
}

function getProvinceAll(){
  global $connMS;
  $sql = "SELECT * From [dbo].Province WHERE Active = 'Y' ORDER BY Province_Name_TH ASC";
  $stmt = sqlsrv_query( $connMS, $sql );
  if(sqlsrv_has_rows($stmt)) {
      while( $row = sqlsrv_fetch_array($stmt) ) { 
        $resultarray[] = $row;
      }
      return $resultarray;
      exit();
  }
}

function getDistrictByProvince($Province){
  global $connMS;
  $sql = "SELECT * From [dbo].District WHERE Province_ID = '".$Province."' AND  Active = 'Y' ORDER BY District_Name_TH ASC ";
  $stmt = sqlsrv_query( $connMS, $sql );
  if(sqlsrv_has_rows($stmt)) {
      while( $row = sqlsrv_fetch_array($stmt) ) { 
        $resultarray[] = $row;
      }
      return $resultarray;
      exit();
  }
}

function getSubdistrictByDistrict($District){
  global $connMS;
  $sql = "SELECT * From [dbo].Subdistrict WHERE District_ID = '".$District."' AND  Active = 'Y' ORDER BY Subdistrict_Name_TH ASC ";
  $stmt = sqlsrv_query( $connMS, $sql );
  if(sqlsrv_has_rows($stmt)) {
      while( $row = sqlsrv_fetch_array($stmt) ) { 
        $resultarray[] = $row;
      }
      return $resultarray;
      exit();
  }
}

function getPost($Subdistrict){
  global $connMS;
  $sql = "SELECT Remark From [dbo].Subdistrict WHERE Subdistrict_ID = '".$Subdistrict."'";
  $stmt = sqlsrv_query( $connMS, $sql );
  if(sqlsrv_has_rows($stmt)) {
    $row = sqlsrv_fetch_array($stmt);
    return $row["Remark"];
    exit();
  }
}

function getReferralType(){
  global $connMS;
  $sql = "SELECT * From [dbo].Referral_Type WHERE Active = 'Y' ORDER BY Referral_Type_ID ASC ";
  $stmt = sqlsrv_query( $connMS, $sql );
  if(sqlsrv_has_rows($stmt)) {
      while( $row = sqlsrv_fetch_array($stmt) ) { 
        $resultarray[] = $row;
      }
      return $resultarray;
      exit();
  }
}


function getCompareStatusAll(){
  global $connMS;
  $sql = "SELECT * From [dbo].Compare_Status WHERE Active = 'Y' ";
  $stmt = sqlsrv_query( $connMS, $sql );
  if(sqlsrv_has_rows($stmt)) {
      while( $row = sqlsrv_fetch_array($stmt) ) { 
        $resultarray[] = $row;
      }
      return $resultarray;
      exit();
  }
}


function getCarMakeAll(){
  global $connMS;
  $sql = "SELECT * From [dbo].Car_Make WHERE Active = 'Y' ORDER BY Make_Name_TH ASC";
  $stmt = sqlsrv_query( $connMS, $sql );
  if(sqlsrv_has_rows($stmt)) {
      while( $row = sqlsrv_fetch_array($stmt) ) { 
        $resultarray[] = $row;
      }
      return $resultarray;
      exit();
  }
}

function getCarModelAll($make){
  global $connMS;
  $sql = "SELECT * From [dbo].Asia_Car_Model WHERE Make_Desc = '".$make."' ORDER BY Model_Desc ASC";
  $stmt = sqlsrv_query( $connMS, $sql );
  if(sqlsrv_has_rows($stmt)) {
      while( $row = sqlsrv_fetch_array($stmt) ) { 
        $resultarray[] = $row;
      }
      return $resultarray;
      exit();
  }
}

function getBeneficiary(){
  global $connMS;
  $sql = "SELECT * From [dbo].Beneficiary  ORDER BY Order_Index ASC";
  $stmt = sqlsrv_query( $connMS, $sql );
  if(sqlsrv_has_rows($stmt)) {
      while( $row = sqlsrv_fetch_array($stmt) ) { 
        $resultarray[] = $row;
      }
      return $resultarray;
      exit();
  }
}

function getInstallmentlist($po){
  global $connMS;
  $sql = "SELECT * From [dbo].Installment  WHERE PO_ID = '".$po."' ORDER BY Installment_ID ASC";
  $stmt = sqlsrv_query( $connMS, $sql );
  if(sqlsrv_has_rows($stmt)) {
      while( $row = sqlsrv_fetch_array($stmt) ) { 
        $resultarray[] = $row;
      }
      return $resultarray;
      exit();
  }
}


function getReportFollowByUser($UserCode){
  global $connMS;
  $resultarray = array();
  $dateStart = date("Y-m")."-01";
  $dateLastmonth = date("Y-m-t", strtotime($dateStart));

  $sql = "SELECT convert(char(10), Remind.Remind_Date, 103) AS RemindDate, COUNT(Followup.Followup_ID) AS countFU
    from Followup
    LEFT JOIN Compare_Policy ON Followup.Compare_Policy_ID = Compare_Policy.CP_ID
    LEFT JOIN My_User ON My_User.User_ID = Compare_Policy.Employee_ID
    LEFT JOIN Department ON My_User.User_Dept_ID = Department.Department_ID 
    LEFT JOIN Remind ON Followup.Followup_ID = Remind.Remark  

    WHERE 
    Followup.Followup_ID = (SELECT MAX(Followup_ID) FROM Followup WHERE Compare_Policy_ID = Compare_Policy.CP_ID  )
    AND Followup.Followup_DateTime = (SELECT MAX(Followup_DateTime) FROM Followup WHERE Compare_Policy_ID = Compare_Policy.CP_ID  )
    AND Followup.Followup_Status_ID not in ('009', '010')
    AND Compare_Policy.Compare_Status_ID NOT IN ('POI','REF','REJ','SMS') 
    AND My_User.User_ID = '".$UserCode."'
    
    AND Remind.Remind_Date BETWEEN '".$dateStart."' AND '".$dateLastmonth."'
    GROUP BY convert(char(10), Remind.Remind_Date, 103)
    ORDER BY convert(char(10), Remind.Remind_Date, 103) ASC";

    $stmt = sqlsrv_query( $connMS, $sql );
    if(sqlsrv_has_rows($stmt)) {
        while( $row = sqlsrv_fetch_array($stmt) ) { 
          $resultarray[] = $row;
        }
        return $resultarray;
        exit();
    } 

}


function getNoticeForindex($type){
  global $conn2;
    $sql = "SELECT * FROM notice  WHERE  enable = '1' AND  showtype = '".$type."' ORDER BY notice_date DESC ";
    $act = $conn2->GetAll($sql);
    return $act;
}



function EmailCost($arr)
{

    // echo "<pre>".print_r($arr,1)."</pre>";
    $mail            = new PHPMailer\PHPMailer\PHPMailer(true);
    $mail->CharSet   = 'utf-8';
    $mail->SMTPDebug = 0;
    $mail->Timeout   = 90; // Enable verbose debug output
    $mail->isSMTP(); // Set mailer to use SMTP
    $mail->Host       = 'smtp.gmail.com'; // Specify main and backup SMTP servers
    $mail->SMTPAuth   = true;
    $mail->Username   = "admin@asiadirect.co.th";
    $mail->Password   = "@db6251000";
    $mail->SMTPSecure = 'tls'; // Enable TLS encryption, `ssl` also accepted
    $mail->Port       = 587; // TCP port to connect to
    $mail->setFrom('admin@asiadirect.co.th', 'Asia Direct Broker');
    $mail->AddReplyTo("info@asiadirect.co.th", "Asia Direct");
    $mail->AddReplyTo("puangpaka.b@asiadirect.co.th", "Puangpaka Boontip");

    // $mail->AddAddress('teerakan.s@asiadirect.co.th', 'IT'); // Name is optional
    // $mail->AddAddress("puangpaka.b@asiadirect.co.th", "Puangpaka Boontip");
    if($arr["mailsend"]){
      $mail->AddAddress($arr["mailsend"], $arr["customerName"]); 
    }
    $mail->isHTML(true); // Set email format to HTML
    $mail->Subject = $arr["subject"];
    $mail->Body    = $arr["bodymail"];

//   Attachments

    if ($arr["files"]) {
      $fileName = "ADB-ConditionCost".date("Y").".pdf";
      $mail->addAttachment("../".$arr["files"], $fileName);
    }


    if (!$mail->Send()) {
        return 0;
    } else {
        return 1;
    }
    exit();
}


/* *******************************  End Function For Send Work System ****************************  */