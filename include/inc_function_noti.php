<?php

function dateThai($date, $upMonth){

	$thaimonth = array("","มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม"); 
	$dayex = explode("-", $date);   
	if($upMonth){
		$date = date("Y-n-d", strtotime("+".$upMonth." day", strtotime($date) ));
	}else{
		$date;
	}
	// echo date("n",strtotime("+".$upMonth." day", $date));
	$day = date("j",strtotime($date));
	$month = $thaimonth[date("n",strtotime($date))];
	$year = $dayex[0]+543;
	return  "$day $month $year"; 
}

function dateThaiDown($date, $upMonth){

	$thaimonth = array("","มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม"); 
	$dayex = explode("-", $date);   
	if($upMonth){
		$date = date("Y-n-d", strtotime("+".$upMonth." day", strtotime($date) ));
	}else{
		$date;
	}
	// echo date("n",strtotime("+".$upMonth." day", $date));
	$day = date("j",strtotime($date));
	$month = $thaimonth[date("n",strtotime($date))];
	$year = date("Y",strtotime($date))+543;
	
	if($day > 5 && $day <= 15 ){
		$day = 5;
	}else if($day >= 16 && $day <= 25){
		$day = 15;
	}else if($day >= 26 && $day <= 31){
		$day = 25;
	}else if($day >= 1 && $day <= 5){
		$day = 25;
		$m = date("n",strtotime($date))-1;
		if($m <= 0){
			$m = 12;
			$year = $year - 1 ;
		}
		$month =  $thaimonth[$m];
	}
	return  "$day $month $year"; 
}

function getCPByid($cpid){
	global $connMS;
	$sql = "SELECT  * From [dbo].Compare_Policy  WHERE CP_ID = '".$cpid."' ";
	$stmt = sqlsrv_query( $connMS, $sql );
  if(sqlsrv_has_rows($stmt)) {
    $row = sqlsrv_fetch_array($stmt);
    return $row;
    exit();
  }  
}


function getInser(){
	global $connMS; 
    $resultarray = array();
    $sql = "SELECT *  FROM [dbo].[Insurer]  WHERE (Active = 'Y') ORDER BY Insurer_Initials DESC";
    // echo "<pre>".print_r($make,1)."</pre>";
    $stmt = sqlsrv_query( $connMS, $sql );
    if(sqlsrv_has_rows($stmt)) {
        while( $row = sqlsrv_fetch_array($stmt) ) { 
          $resultarray[] = $row;
        }
        return $resultarray;
        exit();
    }
}

function getInserByID($id){
	global $connMS; 
	$sql = "SELECT Insurer_Name FROM [dbo].[Insurer] WHERE Insurer.Insurer_ID = '".$id."' ";
	$stmt = sqlsrv_query( $connMS, $sql );
	if(sqlsrv_has_rows($stmt)) {
		$row = sqlsrv_fetch_array($stmt);
    return $row["Insurer_Name"];
    exit();
	}
}

function getInsurance(){
	global $connMS; 
    $resultarray = array();
    $sql = "SELECT *  FROM [dbo].[Insurance]  WHERE  (Insurance_Group_ID = 'MT') ORDER BY Insurance_ID ASC";
    $stmt = sqlsrv_query( $connMS, $sql );
    // echo "<pre>".print_r($stmt,1)."</pre>";
    if(sqlsrv_has_rows($stmt)) {
        while( $row = sqlsrv_fetch_array($stmt) ) { 
          $resultarray[] = $row;
        }
        return $resultarray;
        exit();
    }
}

function getPOAll($poid){
	global $connMS; 
    $resultarray = array();
    $sql = "SELECT *, Purchase_Order.Coverage_Start_Date AS PO_Coverage_Start_Date, Purchase_Order.Coverage_End_Date AS PO_Coverage_End_Date FROM [dbo].[Purchase_Order]   
    INNER JOIN [dbo].[Purchase_Order_Insurer] ON Purchase_Order.PO_ID = Purchase_Order_Insurer.PO_ID 
    INNER JOIN [dbo].[Compare_Policy] ON Purchase_Order.PO_ID = Compare_Policy.CP_PO_ID
    INNER JOIN [dbo].[Car_Detail] ON Purchase_Order.PO_ID = Car_Detail.PO_ID 
    INNER JOIN [dbo].[Customer] ON Purchase_Order.Customer_ID = Customer.Customer_ID 
    INNER JOIN [dbo].[My_User] ON Compare_Policy.Employee_ID = My_User.User_ID 
    INNER JOIN [dbo].[Purchase_Order_Mail_Address] ON Purchase_Order.PO_ID = Purchase_Order_Mail_Address.PO_ID 
    WHERE Purchase_Order.PO_ID = '".$poid."' ";
    $stmt = sqlsrv_query( $connMS, $sql );
    if(sqlsrv_has_rows($stmt)) {
        // while( $row = sqlsrv_fetch_array($stmt) ) { 
        //   $resultarray[] = $row;
        // }
    	$row = sqlsrv_fetch_array($stmt);
        return $row;
        exit();
    }
}

function getSubdistrict($id){
	global $connMS; 
	$sql = "SELECT Subdistrict_Name_TH FROM [dbo].[Subdistrict] WHERE Subdistrict.Subdistrict_ID = '".$id."' ";
	$stmt = sqlsrv_query( $connMS, $sql );
	if(sqlsrv_has_rows($stmt)) {
		$row = sqlsrv_fetch_array($stmt);
    return $row["Subdistrict_Name_TH"];
    exit();
	}
}

function getDistrict($id){
	global $connMS; 
	$sql = "SELECT District_Name_TH FROM [dbo].[District] WHERE District.District_ID = '".$id."' ";
	$stmt = sqlsrv_query( $connMS, $sql );
	if(sqlsrv_has_rows($stmt)) {
		$row = sqlsrv_fetch_array($stmt);
    return $row["District_Name_TH"];
    exit();
	} 
}

function getProvince($id){
	global $connMS; 
	$sql = "SELECT Province_Name_TH FROM [dbo].[Province] WHERE Province.Province_ID = '".$id."' ";
	$stmt = sqlsrv_query( $connMS, $sql );
	if(sqlsrv_has_rows($stmt)) {
		$row = sqlsrv_fetch_array($stmt);
    return $row["Province_Name_TH"];
    exit();
	}
}

function getCarMake($id){
	global $connMS; 
	$sql = "SELECT Make_Name_TH FROM [dbo].[Car_Make] WHERE Car_Make.Make_ID = '".$id."' ";
	$stmt = sqlsrv_query( $connMS, $sql );
	if(sqlsrv_has_rows($stmt)) {
		$row = sqlsrv_fetch_array($stmt);
    return $row["Make_Name_TH"];
    exit();
	}
}

function getCarModel($id){
	global $connMS; 
	$sql = "SELECT Model_Desc FROM [dbo].[Asia_Car_Model] WHERE Asia_Car_Model.Model_ID = '".$id."' ";
	$stmt = sqlsrv_query( $connMS, $sql );
	if(sqlsrv_has_rows($stmt)) {
		$row = sqlsrv_fetch_array($stmt);
    return $row["Model_Desc"] ? $row["Model_Desc"] : $id;
    exit();
	}
}

function getCompulsoryByID($id){
	global $connMS; 
	$sql = "SELECT * FROM [dbo].[Purchase_Order_Insurer] WHERE Purchase_Order_Insurer.PO_ID = '".$id."' ";
	$stmt = sqlsrv_query( $connMS, $sql );
	if(sqlsrv_has_rows($stmt)) {
		$row = sqlsrv_fetch_array($stmt);
    return $row;
    exit();
	}
}

function getCoverageItem($id){
	global $connMS; 
    $resultarray = array();
    $sql = "SELECT * FROM [dbo].[Compare_Policy_Coverage_Item] WHERE Compare_Policy_Coverage_Item.CP_ID = '".$id."' AND CPO_ID ='2' ";
    $stmt = sqlsrv_query( $connMS, $sql );
    if(sqlsrv_has_rows($stmt)) {
        while( $row = sqlsrv_fetch_array($stmt) ) { 
        	$id = $row["Coverage_Item_ID"];
          $resultarray[$id] = floatval($row["Coverage_Value"]);

        }
        return $resultarray;
        exit();
    }
}

function getCoverageItemName($id){
	global $connMS; 
    $resultarray = array();
   	$sql = "SELECT * FROM [dbo].[Coverage_Item] WHERE Coverage_Item.Coverage_Item_ID = '".$id."' ";
    $stmt = sqlsrv_query( $connMS, $sql );
    if(sqlsrv_has_rows($stmt)) {
			$row = sqlsrv_fetch_array($stmt);
	    return $row;
	    exit();
		}
}

function forCoverageItem($fors){
	global $connMS; 
	$i = 0;
	$resultarray = array();
	foreach ($fors as $key => $value) {
		if($key >= 512 && $key <= 527){
		 	$sql = "SELECT * FROM [dbo].[Coverage_Item] WHERE Coverage_Item.Coverage_Item_ID = '".$key."' ";
		 	$stmt = sqlsrv_query( $connMS, $sql );
		    if(sqlsrv_has_rows($stmt)) {
		    	while( $row = sqlsrv_fetch_array($stmt) ) { 
					$resultarray[$i]["num".$i] = $row["Remark"];
					$resultarray[$i]["price".$i] = $value;
					}
				}
			$i++;
		}
	}
		// echo "<pre>".print_r($row,1)."</pre>";
		return $resultarray;
		exit();
	
}
function getCompareOption($id){
	global $connMS; 
    $resultarray = array();
    $sql = "SELECT * FROM [dbo].[Compare_Option] WHERE Compare_Option.CP_ID = '".$id."' AND CPO_ID ='2' ";    
    $stmt = sqlsrv_query( $connMS, $sql );
  if(sqlsrv_has_rows($stmt)) {
		$row = sqlsrv_fetch_array($stmt);
    return $row;
    exit();
	}
}


function getInsuranceByID($id){
	global $connMS; 
    $resultarray = array();
    $sql = "SELECT *  FROM [dbo].[Insurance]  WHERE Insurance.Insurance_ID = '".$id."'";
    $stmt = sqlsrv_query( $connMS, $sql );
  if(sqlsrv_has_rows($stmt)) {
		$row = sqlsrv_fetch_array($stmt);
    return $row;
    exit();
	}
}

function delMyPicBarcode(){
	$files = glob('picBarcode/*'); // get all file names
	if($files){
		foreach($files as $file){ // iterate files
		  if(is_file($file))
		  	// echo $file."<br>";
		    unlink($file); // delete file
		}
	}
}

