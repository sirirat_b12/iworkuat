<?php

function getCPoffer($SearchOption="", $DefaultSearch="", $insurance_type="", $MyUser="", $CompareType="", $CompareStatus="", $CPDateFrom="", $CPDateTo="", $CEDateFrom="", $CEDateTo="", $CPCreateFrom="", $CPCreateTo=""){
	global $connMS;
	$resultarray = array();
	$SearchOption = $SearchOption;
	$DefaultSearch = trim($DefaultSearch);
// exit();
	$sql = "SELECT Compare_Policy.CP_ID, Compare_Policy.CP_PO_ID, Compare_Policy.CP_Date, Compare_Policy.Create_Date, Compare_Policy.Employee_ID, Compare_Policy.Active, Compare_Policy.Coverage_Start_Date, Compare_Policy.Coverage_End_Date, Compare_Policy.Compare_Status_ID, Compare_Policy.Active, Customer.Customer_ID, Customer.Customer_FName, Customer.Customer_LName, Customer.Tel_No, My_User.User_FName, My_User.User_LName, Customer.Mobile_No, Compare_Status.Compare_Status, 
	  Compare_Car_Detail.Plate_No, Compare_Car_Detail.Sub_Model, Province.Province_Name_TH, Car_Make.Make_Name_TH, Asia_Car_Model.Model_Desc, Referral_Type.Referral_Type,
	  (SELECT TOP 1 Followup.Followup_Detail FROM [dbo].Followup WHERE Followup.Compare_Policy_ID = Compare_Policy.CP_ID ORDER BY Followup.Create_Date DESC) AS Followup,
	  (SELECT TOP 1 CASE WHEN Remind.Remind_Date IS NOT NULL THEN Remind.Remind_Date ELSE '".date("Y-m-d 23:59:59")."' END
	  From [dbo].Followup LEFT JOIN [dbo].[Remind] ON Followup.Followup_ID = Remind.Remark 
	  WHERE Followup.Compare_Policy_ID = Compare_Policy.CP_ID 
	  ORDER BY Followup.Create_Date DESC  ) AS Remind_Date 
  FROM [dbo].[Compare_Policy] 
  INNER JOIN [dbo].[Compare_Status] ON Compare_Policy.Compare_Status_ID = Compare_Status.Compare_Status_ID
  LEFT JOIN [dbo].[Compare_Car_Detail] ON Compare_Policy.CP_ID = Compare_Car_Detail.CP_ID
  LEFT JOIN [dbo].[Car_Make] ON Compare_Car_Detail.Make_ID = Car_Make.Make_ID
  LEFT JOIN [dbo].[Asia_Car_Model] ON Compare_Car_Detail.Model = Asia_Car_Model.Model_ID
  LEFT JOIN [dbo].[Province] ON Compare_Car_Detail.Plate_Province = Province.Province_ID
  LEFT JOIN [dbo].[Customer] ON Compare_Policy.Customer_ID = Customer.Customer_ID
  LEFT JOIN [dbo].[Customer_Registration] ON Customer.Customer_ID = Customer_Registration.Customer_ID
  LEFT JOIN [dbo].[Referral_Type] ON Customer_Registration.Referral_Type_ID = Referral_Type.Referral_Type_ID
  LEFT JOIN [dbo].My_User ON Compare_Policy.Employee_ID = My_User.User_ID WHERE ";
	
	
	if($insurance_type){
		if($insurance_type == "active"){
			if($CompareStatus == "IPC" || $CompareStatus == "OPN" || $CompareStatus == "NEW"){
				$sql .= "Compare_Policy.Compare_Status_ID = '".$CompareStatus."' ";
			}else{
				$sql .= "Compare_Policy.Compare_Status_ID IN ('IPC','OPN','NEW') ";
			}
		}else if ($insurance_type == "done") {
			if($CompareStatus == "REJ" || $CompareStatus == "POI"){
				$sql .= "Compare_Policy.Compare_Status_ID = '".$CompareStatus."' ";
			}else{
				$sql .= "Compare_Policy.Compare_Status_ID IN ('REJ','POI') ";
			}
		}else if ($insurance_type == "all") {
			if($CompareStatus){
				$sql .= "Compare_Policy.Compare_Status_ID = '".$CompareStatus."' ";
			}else{
				$sql .= "Compare_Policy.Compare_Status_ID IN ('IPC','OPN','REJ','POI','NEW') ";
			}	
		}
	}

	if($DefaultSearch){
		if($SearchOption == "ContactNo"){
			$sql .= "AND Customer.Mobile_No LIKE '%".$DefaultSearch."%'  OR Customer.Tel_No LIKE '%".$DefaultSearch."%'  ";
		}else if ($SearchOption == "CustomerName") {
			$sql .= "AND Customer_FName LIKE '%".$DefaultSearch."%' ";
		}else if ($SearchOption == "CPID") {
			$sql .= "AND Compare_Policy.CP_ID LIKE '%".$DefaultSearch."%' ";
		}
	}

	if(trim($MyUser)){
		$sql .= "AND Compare_Policy.Employee_ID = '".$MyUser."' ";
	
	}

	// if($CompareStatus){
	// 	$sql .= "AND Compare_Policy.Compare_Status_ID = '".$CompareStatus."' ";
	// }

	if($CompareType){
		if($CompareType == "R"){
			$sql .= "AND Compare_Policy.CP_PO_ID <> '' ";
		}else if($CompareType == "N"){
			$sql .= "AND Compare_Policy.CP_PO_ID = '' ";
		}
	}

	if($CPDateFrom){
		if($CPDateFrom && $CPDateTo){
			$sql .= "AND Compare_Policy.CP_Date BETWEEN '".$CPDateFrom."' AND '".$CPDateTo."' ";
		}else{
			$sql .= "AND Compare_Policy.CP_Date BETWEEN '".$CPDateFrom."' AND '".date("Y-m-d")."' ";
		}
	}

	if($CEDateFrom){
		if($CEDateFrom && $CPDateTo){
			$sql .= "AND Compare_Policy.CP_Date BETWEEN '".$CEDateFrom."' AND '".$CEDateTo."' ";
		}else{
			$sql .= "AND Compare_Policy.CP_Date BETWEEN '".$CEDateFrom."' AND '".date("Y-m-d")."' ";
		}
	}

	if($CPCreateFrom){
		if($CPCreateFrom && $CPCreateTo){
			$sql .= "AND Compare_Policy.Create_Date BETWEEN '".$CPCreateFrom."' AND '".$CPCreateTo."' ";
		}else{
			$sql .= "AND Compare_Policy.Create_Date BETWEEN '".$CPCreateFrom."' AND '".date("Y-m-d H:m:s")."' ";
		}
	}


	$sql .= "ORDER BY Compare_Policy.Compare_Status_ID ASC , Remind_Date ASC, Compare_Policy.CP_Date DESC";
  // echo $sql;
  $stmt = sqlsrv_query( $connMS, $sql );
  if(sqlsrv_has_rows($stmt)) {
      while( $row = sqlsrv_fetch_array($stmt) ) { 
        $resultarray[] = $row;
      }
      return $resultarray;
      exit();
  }

}

function getCPByid($cpid){
	global $connMS;
	$sql = "SELECT  * From [dbo].Compare_Policy  WHERE CP_ID = '".$cpid."' ";
	$stmt = sqlsrv_query( $connMS, $sql );
  if(sqlsrv_has_rows($stmt)) {
    $row = sqlsrv_fetch_array($stmt);
    return $row;
    exit();
  }  
}

function getFollowupBycpid($cpid){
	global $connMS;
	$sql = "SELECT  Followup.Followup_ID, Followup.Customer_ID, Followup.Compare_Policy_ID, Followup.PO_ID, Followup.Followup_DateTime, Followup.Followup_Type_ID, Followup.Followup_Topic, Followup.Followup_Detail, Followup.Followup_Status_ID, Followup.Followup_By, Followup.Remark, Remind.Remind_Date, Followup_Status.Followup_Status_Desc
	From [dbo].Followup LEFT JOIN [dbo].[Remind] ON Followup.Followup_ID = Remind.Remark
	LEFT JOIN [dbo].[Followup_Status] ON Followup.Followup_Status_ID = Followup_Status.Followup_Status_ID
	WHERE Followup.Compare_Policy_ID = '".$cpid."'
	ORDER BY Followup.Create_Date DESC";

	$stmt = sqlsrv_query( $connMS, $sql );
  if(sqlsrv_has_rows($stmt)) {
      while( $row = sqlsrv_fetch_array($stmt) ) { 
        $resultarray[] = $row;
      }
      return $resultarray;
      exit();
  }
}


function getMaxIDFollowup(){
	global $connMS;
	$year = date("Y");
	$sql = "SELECT MAX(Followup_ID) AS cpid From [dbo].Followup WHERE year(Followup_DateTime) = '".$year."'";
	$stmt = sqlsrv_query( $connMS, $sql );
  if(sqlsrv_has_rows($stmt)) {
    $row = sqlsrv_fetch_array($stmt);
    $txt = "FU".(date("y")+43);
    $idex = explode($txt, $row["cpid"]);
    $cpidMax = $txt."".str_pad(intval($idex[1])+1,6,"0",STR_PAD_LEFT);
    return $cpidMax;
    exit();
  }  
}

function getMaxIDRemind(){
	global $connMS;
	$year = date("Y");
	$sql = "SELECT MAX(Remind_ID) AS id From [dbo].Remind WHERE year(Issue_Date) = '".$year."'";
	$stmt = sqlsrv_query( $connMS, $sql );
  if(sqlsrv_has_rows($stmt)) {
    $row = sqlsrv_fetch_array($stmt);
    $txt = "RM".(date("y")+43);
    $idex = explode($txt, $row["id"]);
    $idMax = $txt."".str_pad(intval($idex[1])+1,6,"0",STR_PAD_LEFT);
    return $idMax;
    exit();
  }  
}

function getMaxIDRCustomer($type){
	global $connMS;
	$year = date("Y");
	$sql = "SELECT MAX(Customer_ID) AS id From [dbo].Customer WHERE year(Create_Date) = '".$year."' AND Customer_Type = '".$type."'";
	$stmt = sqlsrv_query( $connMS, $sql );
  if(sqlsrv_has_rows($stmt)) {
    $row = sqlsrv_fetch_array($stmt);
    $replace = array("P","C");
		$retxt = str_replace($replace, "", $row["id"]);
    $txt = $type."".intval($retxt+1);
    // $idex = explode($txt, $row["id"]);
    // $idMax = $txt."".str_pad(intval($idex[1])+1,6,"0",STR_PAD_LEFT);
    return $txt;
    exit();
  }  
}

function getComparePolicyID(){
	global $connMS;
	$year = date("Y");
	$sql = "SELECT TOP (1) CP_ID AS cpid From [dbo].Compare_Policy WHERE year(Create_Date) = '".$year."' ORDER BY Create_Date DESC ";
	$stmt = sqlsrv_query( $connMS, $sql );
  if(sqlsrv_has_rows($stmt)) {
    $row = sqlsrv_fetch_array($stmt);
    $txt = "CP".(date("y")+43);
    $idex = substr($row["cpid"], 5, 9);
    $cpidMax = $txt."".str_pad(intval($idex)+1,6,"0",STR_PAD_LEFT);
    return $cpidMax;
    exit();
  }  
}

function htmlTabelFollow($cpid, $case){
	$getFollowupBycpid = getFollowupBycpid($cpid);
	$html = '';
	// echo "<pre>".print_r($getFollowupBycpid,1)."</pre>"; 
	foreach ($getFollowupBycpid as $key => $value) {
		$Remind_Date = $value["Remind_Date"] ? $value["Remind_Date"]->format("d/m/Y H:i") : "-";
		$html .= '
			<tr>
				<td class="t_c" >';
				if($value["Followup_Status_ID"] != '009' && $value["Followup_Status_ID"] != '010'){
					if($case == "pageAdd"){
						$html .= "<span class='c2457ff fs14 cursorPoin' onclick=editFollow('".$value["Followup_ID"]."','".$_POST["cpid"]."')><i class='fa fa-edit fs14'></i></span> ";
					}else{
						 $html .= "<span class='c2457ff fs14 cursorPoin' onclick=\"editFollow('".$value['Followup_ID']."')\"><i class='fa fa-edit fs14'></i></span>";
					}
				}
		$html .= '		
				</td>
				<td class="t_c">'.$value["Followup_DateTime"]->format("d/m/Y").'</td>
				<td>'.$value["Followup_Detail"].'</td>
				<td class="t_c">'.$value["Followup_Status_Desc"].'</td>
				<td class="t_c">'.$Remind_Date.'</td>
			</tr>
		';
	}
	return  $html;
	exit();
}

function getRowCPAllBycode($cpid=""){
	global $connMS;
// exit();
	$sql = "SELECT Compare_Policy.*, Customer.Customer_ID, Customer.Customer_FName, Customer.Customer_LName, Customer.Tel_No, My_User.User_ID, Title.Title_Name, My_User.User_FName, My_User.User_LName, My_User.User_Phone, My_User.User_Email, Customer.Mobile_No, Compare_Status.Compare_Status, 
	  Compare_Car_Detail.Plate_No, Compare_Car_Detail.Plate_Province, Compare_Car_Detail.Sub_Model, Province.Province_Name_TH, Car_Make.Make_Name_TH, Asia_Car_Model.Model_Desc, Referral_Type.Referral_Type, Compare_Car_Detail.Make_ID, Compare_Car_Detail.Model, Compare_Car_Detail.Body_Type, Compare_Car_Detail.Frame_No, Compare_Car_Detail.Seat_CC_Weight, Compare_Car_Detail.Car_Code, Compare_Car_Detail.Accessory,
	  (SELECT TOP 1 [dbo].[Remind].Remind_Date From [dbo].Followup LEFT JOIN [dbo].[Remind] ON Followup.Followup_ID = Remind.Remark WHERE Followup.Compare_Policy_ID = Compare_Policy.CP_ID ORDER BY Followup.Create_Date DESC  ) AS Remind_Date
  FROM [dbo].[Compare_Policy] 
  INNER JOIN [dbo].[Compare_Status] ON Compare_Policy.Compare_Status_ID = Compare_Status.Compare_Status_ID
  LEFT JOIN [dbo].[Compare_Car_Detail] ON Compare_Policy.CP_ID = Compare_Car_Detail.CP_ID
  LEFT JOIN [dbo].[Car_Make] ON Compare_Car_Detail.Make_ID = Car_Make.Make_ID
  LEFT JOIN [dbo].[Asia_Car_Model] ON Compare_Car_Detail.Model = Asia_Car_Model.Model_ID
  LEFT JOIN [dbo].[Province] ON Compare_Car_Detail.Plate_Province = Province.Province_ID
  LEFT JOIN [dbo].[Customer] ON Compare_Policy.Customer_ID = Customer.Customer_ID
  LEFT JOIN [dbo].[Customer_Registration] ON Customer.Customer_ID = Customer_Registration.Customer_ID
  LEFT JOIN [dbo].[Referral_Type] ON Customer_Registration.Referral_Type_ID = Referral_Type.Referral_Type_ID
  LEFT JOIN [dbo].My_User ON Compare_Policy.Employee_ID = My_User.User_ID 
  LEFT JOIN [dbo].Title ON Customer.Customer_Title = Title.Title_ID
  WHERE Compare_Policy.CP_ID = '".$cpid."' ";
  // echo $sql;
  $stmt = sqlsrv_query( $connMS, $sql );
  if(sqlsrv_has_rows($stmt)) {
  	$row = sqlsrv_fetch_array($stmt);
  	return  $row;
    exit();
  }

}

function getRowCoverageItem($id){
	global $connMS;
	$sql = "SELECT * FROM [dbo].[Coverage_Item] WHERE Coverage_Item_ID = '".$id."' ";
  // echo $sql;
  $stmt = sqlsrv_query( $connMS, $sql );
  if(sqlsrv_has_rows($stmt)) {
  	$row = sqlsrv_fetch_array($stmt);
  	return  $row["Remark"];
    exit();
  }
}
function getCoverageItemList($cpid=""){
	global $connMS;
	$CoverageList = array();
	$sql = "SELECT Compare_Policy_Coverage_Item.*,Coverage_Item.Coverage_Item_Desc, Coverage_Item.Coverage_Item_Unit, Coverage_Item.Active
	FROM [dbo].[Compare_Policy_Coverage_Item] 
	LEFT JOIN [dbo].[Coverage_Item] ON Compare_Policy_Coverage_Item.Coverage_Item_ID = Coverage_Item.Coverage_Item_ID
	WHERE Compare_Policy_Coverage_Item.CP_ID = '".$cpid."' ORDER BY Compare_Policy_Coverage_Item.CPO_ID ASC,Compare_Policy_Coverage_Item.Order_Index ASC";
	$stmt = sqlsrv_query( $connMS, $sql );
  if(sqlsrv_has_rows($stmt)) { $i = 0;
      while( $row = sqlsrv_fetch_array($stmt) ) { 
        // $resultarray["CoverageValue"][] = $row;
         // echo "<pre>".print_r($CoverageList,1)."</pre>"; 
      	// echo "<br>".$row["Coverage_Item_ID"]." * ".$key = array_search($row["Coverage_Item_ID"], array_column( $CoverageList, 'Coverage_Item_ID'));
        if( !array_search($row["Coverage_Item_ID"], array_column( $CoverageList, 'Coverage_Item_ID')) ){
        	if($row["Coverage_Item_ID"] == "501" || $row["Coverage_Item_ID"] == "506" || $row["Coverage_Item_ID"] == "511"){
        		$item = "G";
        	}else{
        		$item = "I";
        	}
	        $CoverageList[$row["Order_Index"]."".$row["Coverage_Item_ID"]]["desc"] = $row["Coverage_Item_Desc"];
	        $CoverageList[$row["Order_Index"]."".$row["Coverage_Item_ID"]]["item"] = $item;
	        $CoverageList[$row["Order_Index"]."".$row["Coverage_Item_ID"]]["Coverage_Item_ID"] = $row["Coverage_Item_ID"];
	        $CoverageList[$row["Order_Index"]."".$row["Coverage_Item_ID"]]["Order_Index"] = $row["Order_Index"];
	      }
        $i++;
      }
      // array_push($resultarray["CoverageList"], $CoverageList);
      // echo "<pre>".print_r($CoverageList,1)."</pre>"; 
      $resultarray["CoverageList"] =  $CoverageList; 
      return $resultarray;
      exit();
  }
}

function getAllCoverageItemByid($cpid="", $num){
	global $connMS;
	$sql = "SELECT Compare_Policy_Coverage_Item.Coverage_Item_ID, Compare_Policy_Coverage_Item.Coverage_Value
	FROM [dbo].[Compare_Policy_Coverage_Item] 
	LEFT JOIN [dbo].[Coverage_Item] ON Compare_Policy_Coverage_Item.Coverage_Item_ID = Coverage_Item.Coverage_Item_ID
	WHERE Compare_Policy_Coverage_Item.CP_ID = '".$cpid."' AND Compare_Policy_Coverage_Item.CPO_ID = '".$num."' ORDER BY Coverage_Item_ID ASC";
  $stmt = sqlsrv_query( $connMS, $sql );
  if(sqlsrv_has_rows($stmt)) {
  	while( $row = sqlsrv_fetch_array($stmt) ) { 
  		$resultarray[$row["Coverage_Item_ID"]] = $row["Coverage_Value"];
  	}
  	return  $resultarray;
    exit();
  }
}

function getCompareOption($cpid=""){
	global $connMS;
	$sql = "SELECT Compare_Option.*, Insurer.Insurer_Initials, Insurance.Insurance_Name, 
	CASE WHEN Compare_Option.Insurance_Package_ID = '001' THEN NULL ELSE Insurance_Package.Insurance_Package_Name END AS Insurance_Package_Name
	FROM [dbo].[Compare_Option] 
	LEFT JOIN [dbo].[Insurer] ON Compare_Option.Insurer_ID = Insurer.Insurer_ID
	LEFT JOIN [dbo].[Insurance] ON Compare_Option.Insurance_ID = Insurance.Insurance_ID
	LEFT JOIN [dbo].[Insurance_Package] ON Compare_Option.Insurance_Package_ID = Insurance_Package.Insurance_Package_ID
	WHERE Compare_Option.CP_ID = '".$cpid."' ORDER BY CPO_ID ASC";
	$stmt = sqlsrv_query( $connMS, $sql );
	$i = 0 ;
  if(sqlsrv_has_rows($stmt)) {
      while( $row = sqlsrv_fetch_array($stmt) ) { 
        $resultarray[$i]["option"] = $row;
        $resultarray[$i]["item"] = getAllCoverageItemByid($cpid, $row["CPO_ID"]);
        // $resultarray[$i]["person"] = getAllCoverageItemByid($cpid, $row["CPO_ID"]);
        $i++;
      }
      return $resultarray;
      exit();
  }
}

?>