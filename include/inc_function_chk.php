<?php 
function getallProvince(){
    global $conn2;
    $act = $conn2->GetAll( "SELECT * FROM ck_province ORDER BY province_name ASC");
    // echo "<pre>".print_r($act,1)."</pre>";
    return $act;
} 

function getallProvinceID($name){
    global $conn2;
    $act = $conn2->GetRow( "SELECT * FROM ck_province WHERE province_name ='".$name."'");
    // echo "<pre>".print_r($act,1)."</pre>";
    return $act;
} 

function upDateQCOpenfile($numcode, $pocode){
    global $conn2;
    $timeOpen = $conn2->GetOne( "SELECT datetime_chk_open FROM noti_work WHERE noti_work_code ='".$numcode."'");
    if(!$timeOpen){
      $data = array();
      $data["datetime_chk_open"] = date("Y-m-d H:i:s");
      $conn2->AutoExecute("noti_work", $data, 'UPDATE', "noti_work_code = '".$numcode."' "); 
    }
    return 1;
}
// function getRowPOByCode($pocode){
// 	global $connMS;
// 	$pocode = trim($pocode);
// 	$resultarray = array();
//   $sql = "SELECT 
//   Purchase_Order.PO_ID, Purchase_Order.Coverage_Start_Date, Purchase_Order.Capital, Purchase_Order.Insurance_Package_ID, Purchase_Order.Compulsory, Purchase_Order.Net_Premium, Purchase_Order.Discount, Purchase_Order.Total_Premium, Purchase_Order.Premium_After_Disc, Insurance_Package.Insurance_Package_Name, Purchase_Order.Marine_Reference, ADB_Premium.Premium_Desc, Insurance.Insurance_Name, Customer.Customer_Type AS cus_type, Customer.Customer_FName AS FName, Customer.Customer_LName AS LName, Insurer.Insurer_Initials AS Insurer_Initials 
//   FROM [dbo].[Purchase_Order] INNER JOIN [dbo].[Customer] ON Purchase_Order.Customer_ID = Customer.Customer_ID 
//   INNER JOIN [dbo].[Purchase_Order_Insurer] ON Purchase_Order.PO_ID = Purchase_Order_Insurer.PO_ID 
//   INNER JOIN [dbo].[Insurance] ON Purchase_Order.Insurance_ID = Insurance.Insurance_ID
//   INNER JOIN [dbo].[Insurer] ON Purchase_Order_Insurer.Insurer_ID = Insurer.Insurer_ID
//   LEFT JOIN [dbo].[Insurance_Package] ON Purchase_Order.Insurance_Package_ID = Insurance_Package.Insurance_Package_ID
//   LEFT JOIN [dbo].[ADB_Premium] ON Purchase_Order.Premium_ID = ADB_Premium.Premium_ID 
//   WHERE Purchase_Order.PO_ID = '".$pocode."' ";
//   $stmt = sqlsrv_query( $connMS, $sql );
//   if(sqlsrv_has_rows($stmt)) {
//     $row = sqlsrv_fetch_array($stmt);
//     return $row;
//     exit();
//   }  
// 	exit();
// }

function getRowPOByCode($pocode){
  global $connMS;
  $pocode = trim($pocode);
  $resultarray = array();
  $sql = "SELECT 
  Purchase_Order.PO_ID, Purchase_Order.Insurance_ID, Purchase_Order.Coverage_Start_Date, Purchase_Order.Capital, Purchase_Order.Insurance_Package_ID, Purchase_Order.Compulsory, Purchase_Order.Net_Premium, Purchase_Order.Discount, Purchase_Order.Total_Premium, Purchase_Order.Premium_After_Disc, Insurance_Package.Insurance_Package_Name, Purchase_Order.Marine_Reference, ADB_Premium.Premium_Desc, Insurance.Insurance_Name, Customer.Customer_Type AS cus_type, Title.Title_Name AS Title_Name, Customer.Customer_FName AS FName, Customer.Customer_LName AS LName, Insurer.Insurer_Initials AS Insurer_Initials , Delivery_Type.Delivery_Type_Desc
  FROM [dbo].[Purchase_Order] INNER JOIN [dbo].[Customer] ON Purchase_Order.Customer_ID = Customer.Customer_ID 
  LEFT JOIN [dbo].[Purchase_Order_Insurer] ON Purchase_Order.PO_ID = Purchase_Order_Insurer.PO_ID 
  LEFT JOIN [dbo].[Insurance] ON Purchase_Order.Insurance_ID = Insurance.Insurance_ID
  LEFT JOIN [dbo].[Insurer] ON Purchase_Order_Insurer.Insurer_ID = Insurer.Insurer_ID
  LEFT JOIN [dbo].[Title] ON Customer.Customer_Title  = Title.Title_ID
  LEFT JOIN [dbo].[Insurance_Package] ON Purchase_Order.Insurance_Package_ID = Insurance_Package.Insurance_Package_ID
  LEFT JOIN [dbo].[ADB_Premium] ON Purchase_Order.Premium_ID = ADB_Premium.Premium_ID 
  LEFT JOIN [dbo].[Policy_Delivery] ON Purchase_Order.PO_ID = Policy_Delivery.PO_ID
  LEFT JOIN [dbo].[Delivery_Type] ON Policy_Delivery.Delivery_Type_ID = Delivery_Type.Delivery_Type_ID
  WHERE Purchase_Order.PO_ID = '".$pocode."' ";
  $stmt = sqlsrv_query( $connMS, $sql );
  if(sqlsrv_has_rows($stmt)) {
    $row = sqlsrv_fetch_array($stmt);
    return $row;
    exit();
  }  
  exit();
}

function getAllSendPO($userCode, $userType="", $status="", $pocode="", $sup_code=""){   
    global $conn2;  
    $sql = "SELECT noti_work.*,
              admin.personnel_firstname AS chk_name,
              admin.personnel_lastname AS chk_lname, 
              team.personnel_supervisor
            FROM
              noti_work
              LEFT JOIN ck_personnel admin ON noti_work.chk_code = admin.personnel_code 
              LEFT JOIN ck_personnel team ON noti_work.personnel_code = team.personnel_code ";

    $team = "";
    if($sup_code){
      $team = " AND team.personnel_supervisor = '$sup_code'";
    }

    if('teamorders.php'==basename($_SERVER[PHP_SELF]) && !eregi('admin', $userCode)) {
      $sql .=" 
      WHERE noti_work.enable = '1' 
      AND ck_personnel.personnel_code NOT LIKE 'admin'
      AND (select count(ck_personnel.personnel_code) from ck_personnel where `ck_personnel`.`personnel_supervisor` = '$userCode') > 0
      
      AND (noti_work.personnel_code IN (select ck_personnel.personnel_code from ck_personnel where `ck_personnel`.`personnel_supervisor` = '$userCode'))";
      
      if(isset($status) && !$pocode){       
        $sql .= " AND noti_work.status = '".$status."' $team";
      }else if($pocode){
        $sql .= " AND noti_work.po_code LIKE '%".$pocode."%'  "; 
      }



    } else if($userCode && $userType == "QualityControl"){
      if($pocode){
        // $sql .= "WHERE noti_work.po_code LIKE '%".$pocode."%' AND  noti_work.chk_code = '".$userCode."'  OR noti_work.personnel_code = '".$userCode."' ";
        $sql .= "WHERE noti_work.po_code LIKE '%".$pocode."%' ";  
      }else{
        $sql .= " WHERE noti_work.status = '".$status."' AND noti_work.enable = '1'  AND  noti_work.chk_code = '".$userCode."' OR noti_work.personnel_code = '".$userCode."' $team";
      }
    
    }else if($userCode && $userType != "QualityControl"){
      // $sql .= " WHERE noti_work.enable = '1' AND noti_work.personnel_code = '".$userCode."' ";
      if(isset($status)){
         $sql .= " WHERE noti_work.enable = '1' AND noti_work.personnel_code = '".$userCode."' AND noti_work.status = '".$status."' $team";
      }
      if($pocode){
         $sql .= " WHERE noti_work.enable = '1'  AND noti_work.po_code LIKE '%".$pocode."%'  ";
      }
    }else if($pocode){
      $sql .= "WHERE noti_work.po_code LIKE '%".$pocode."%'  "; 
    
    }else{
      $sql .= " WHERE noti_work.enable = '1'  AND noti_work.status = '".$status."' $team"; 
      // if(isset($status)){
      //      $sql .= " AND noti_work.status = '".$status."' ";
      // }
    }
    
     $sql .= " ORDER BY noti_work.urgent DESC, noti_work.start_cover_date DESC, noti_work.created_date DESC LIMIT 0,200";
	// echo $sql;
    $send_orders = $conn2->GetAll( $sql);
    // echo "<pre>".print_r($make,1)."</pre>";
    return $send_orders;

} 

function getAllListForMove($userCode){
   $userType = $_SESSION["User"]['type'];
    global $conn2;  
    // $sql = "SELECT *,noti_work.personnel_code AS personnel_code ,ck_personnel.personnel_firstname AS chk_name, ck_personnel.personnel_lastname AS chk_lname, 
    // ck_personnel.personnel_firstname AS Adname, ck_personnel.personnel_lastname AS chk_lname
    // FROM noti_work LEFT JOIN ck_personnel ON noti_work.chk_code = ck_personnel.personnel_code";
    
    if($userType == "QualityControl"){
    //   $sql = "SELECT *,noti_work.personnel_code AS personnel_code ,ck_personnel.personnel_firstname AS Fname, ck_personnel.personnel_lastname AS Lname
    // FROM noti_work LEFT JOIN ck_personnel ON noti_work.chk_code = ck_personnel.personnel_code WHERE noti_work.status = '0' AND noti_work.enable = '1' AND  noti_work.chk_code = '".$userCode."' OR noti_work.personnel_code = '".$userCode."' ";

    $sql = "SELECT *,noti_work.personnel_code AS personnel_code ,ck_personnel.personnel_firstname AS Fname, ck_personnel.personnel_lastname AS Lname
    FROM noti_work LEFT JOIN ck_personnel ON noti_work.chk_code = ck_personnel.personnel_code WHERE noti_work.status = '0' AND noti_work.enable = '1' ";
    
    }else if($userType == "Admin"){
      $sql = "SELECT DISTINCT noti_work.*,noti_work.personnel_code AS personnel_code ,ck_personnel.personnel_firstname AS Fname, ck_personnel.personnel_lastname AS Lname
    FROM noti_work INNER JOIN ck_personnel ON noti_work.admin_code = ck_personnel.personnel_code 
    WHERE noti_work.status = '2' AND noti_work.enable = '1' ";
    
    }else{
      $sql = " SELECT *,noti_work.personnel_code AS personnel_code ,ck_personnel.personnel_firstname AS Fname, ck_personnel.personnel_lastname AS Lname
    FROM noti_work LEFT JOIN ck_personnel ON noti_work.chk_code = ck_personnel.personnel_code WHERE noti_work.enable = '1' AND noti_work.status IN ('0','2') ";
    }
    $sql .= " ORDER BY noti_work.start_cover_date DESC, noti_work.created_date DESC";
// echo  $sql;
    $send_orders = $conn2->GetAll( $sql);
    // echo "<pre>".print_r($make,1)."</pre>";
    return $send_orders;
} 


function getCompanyBysrv(){
    global $connMS; 
    $resultarray = array();
    $sql = $sql = "SELECT *  FROM [dbo].[Insurer]  WHERE (Active = 'Y') ORDER BY Insurer_Initials ASC";
    // echo "<pre>".print_r($make,1)."</pre>";
    $stmt = sqlsrv_query( $connMS, $sql );
    if(sqlsrv_has_rows($stmt)) {
        while( $row = sqlsrv_fetch_array($stmt) ) { 
          $resultarray[] = $row;
        }
        return $resultarray;
        exit();
    }
    
}

function getCompanyByID($id){
    global $connMS; 
    $resultarray = array();
    $sql = $sql = "SELECT *  FROM [dbo].[Insurer]  WHERE (Insurer_ID = '".$id."')";
    // echo "<pre>".print_r($make,1)."</pre>";
    $stmt = sqlsrv_query( $connMS, $sql );
    if(sqlsrv_has_rows($stmt)) {
        while( $row = sqlsrv_fetch_array($stmt) ) { 
          $resultarray[] = $row;
        }
        return $resultarray;
        exit();
    }
    
}

function getNotiWorkByID($code){
  global $conn2;
  $noti_work = $conn2->GetRow( "SELECT * FROM noti_work WHERE noti_work_code ='".$code."'");
  return $noti_work;   
}

function getNotiWorkFilesByID($code){
  global $conn2;
  $noti_work_files = $conn2->GetAll( "SELECT * FROM noti_work_files WHERE noti_work_code ='".$code."'");
  return $noti_work_files;   
}

function getNotiComment($code){
  global $conn2;
  $noti_comment = $conn2->GetAll( "SELECT * FROM noti_comment WHERE noti_work_code ='".$code."' ORDER BY datetime ASC ");
  return $noti_comment;   
}

function getCommentlist(){
  global $conn2;
  $rs = $conn2->GetAll( "SELECT * FROM comment_lists WHERE enable ='1' ORDER BY commentlists ASC ");
  return $rs;   
}

function getCommenStatus(){
  global $conn2;
  $rs = $conn2->GetAll( "SELECT * FROM comment_status WHERE enable ='1' ORDER BY commentstatus ASC ");
  return $rs;   
}

function getQCcheck(){
  global $conn2; 
  $sql = "SELECT ck_personnel.personnel_code, (SELECT COUNT(noti_work.noti_work_id) from noti_work WHERE date(noti_work.created_date) = '".date("Y-m-d")."' AND noti_work.chk_code = ck_personnel.personnel_code) AS num
    FROM ck_personnel WHERE ck_personnel.personnel_workin = '1' AND ck_personnel.personnel_status = '1' AND ck_personnel.personnel_type = 'QualityControl' GROUP BY ck_personnel.personnel_code ORDER BY num ASC ";
  $rs = $conn2->GetRow($sql);
  return $rs;   
}


function getAdminforwork(){
  global $conn2; 
  $sql = "SELECT ck_personnel.personnel_code, (SELECT COUNT(noti_work.noti_work_id) from noti_work WHERE date(noti_work.created_date) = '".date("Y-m-d")."' AND noti_work.admin_code = ck_personnel.personnel_code) AS num
    FROM ck_personnel WHERE ck_personnel.personnel_workin = '1' AND ck_personnel.personnel_status = '1' AND ck_personnel.personnel_type = 'Admin' AND ck_personnel.personnel_code != 'AdminNsn' GROUP BY ck_personnel.personnel_code ORDER BY num ASC ";
  $rs = $conn2->GetRow($sql);
  return $rs;   
}

function chkPOConcomitant($po, $work_code){
  global $conn2; 
  $sql = "SELECT admin_code FROM noti_work  WHERE refer_po_code ='".$po."' AND refer_work_code ='".$work_code."' ";
  $rs = $conn2->GetOne($sql);
  return $rs; 
}

function getQCcheckAll(){
  global $conn2; 
  $rs = $conn2->GetAll( "SELECT * FROM ck_personnel WHERE personnel_status = '1' AND personnel_type ='QualityControl'  ");
  return $rs;   
}


function getPremium(){
  global $connMS; 
  $resultarray = array();
  $sql = $sql = "SELECT *  FROM [dbo].[ADB_Premium]  WHERE (Active = 'Y') ORDER BY Premium_Desc ASC ";
  // echo "<pre>".print_r($make,1)."</pre>";
  $stmt = sqlsrv_query( $connMS, $sql );
  if(sqlsrv_has_rows($stmt)) {
      while( $row = sqlsrv_fetch_array($stmt) ) { 
        $resultarray[] = $row;
      }
      return $resultarray;
      exit();
    }  
}


function getPurchaseBycode($code){
  global $connMS; 
  $resultarray = array();
  $sql = "SELECT *,Customer.Contact_Person AS CustomerPerson, Customer.Tel_No AS CustomerTel, Customer.Fax AS CustomerFax,Customer.Mobile_No AS CustomerMobile_No, Customer.Customer_ID AS Customercode, Customer.Customer_Type AS cus_type, Customer.Customer_FName AS FName, Customer.Customer_LName AS LName, Insurer.Insurer_Initials AS Insurer_Initials, Insurer.Insurer_Account,  Insurer.OIC_No,
    Customer.Addr1 AS  cusAddr1, Customer.Addr2 AS  cusAddr2, Customer.Subdistrict_ID AS  cusSubdistrict, Customer.District_ID AS  cusDistrict, 
    Customer.Province_ID AS  cusProvince, Customer.Post_Code AS  cusPost_Code,
    Customer_Mail_Address.Receiver_Name AS  cussendReceiver_Name, Customer_Mail_Address.Addr1 AS  cussendAddr1, Customer_Mail_Address.Addr2 AS  cussendAddr2, 
    Customer_Mail_Address.Subdistrict_ID AS  cussendSubdistrict, Customer_Mail_Address.District_ID AS  cussendDistrict, 
    Customer_Mail_Address.Province_ID AS  cussendProvince, Customer_Mail_Address.Post_Code AS  cussendPost_Code, 
    Purchase_Order_Mail_Address.Addr1 AS  posendAddr1, Purchase_Order_Mail_Address.Addr2 AS  posendAddr2, 
    Purchase_Order_Mail_Address.Subdistrict_ID AS  posendSubdistrict, Purchase_Order_Mail_Address.District_ID AS  posendDistrict, 
    Purchase_Order_Mail_Address.Province_ID AS  posendProvince, Purchase_Order_Mail_Address.Post_Code AS  posendPost_Code, 
    Purchase_Order_Mail_Address.Remark AS  posendPostRemark, Title.Title_Name AS  Title_Name,
    Purchase_Order.Net_Premium AS pcNetPremium, Purchase_Order.Compulsory AS pcCompulsory, Purchase_Order.Total_Premium AS pcPremium, Purchase_Order.Discount AS pcDiscount, Purchase_Order.Premium_After_Disc AS pctaxamount, Purchase_Order.Capital AS insuere_cost, Purchase_Order.Net_Premium AS netpremium, Purchase_Order.Compulsory AS Compulsory2, Purchase_Order_Insurer.Insurer_ID AS Insurer_ID, Purchase_Order_Insurer.Notice_No
  FROM [dbo].[Purchase_Order] INNER JOIN [dbo].[Customer] ON Purchase_Order.Customer_ID = Customer.Customer_ID 
  LEFT JOIN [dbo].[Purchase_Order_Insurer] ON Purchase_Order.PO_ID = Purchase_Order_Insurer.PO_ID 
  LEFT JOIN [dbo].[Insurance] ON Purchase_Order.Insurance_ID = Insurance.Insurance_ID
  LEFT JOIN [dbo].[Purchase_Order_Mail_Address] ON Purchase_Order.PO_ID = Purchase_Order_Mail_Address.PO_ID  
  LEFT JOIN [dbo].[Customer_Mail_Address] ON Purchase_Order.Customer_ID = Customer_Mail_Address.Customer_ID
  LEFT JOIN [dbo].[Insurer] ON Purchase_Order_Insurer.Insurer_ID = Insurer.Insurer_ID 
  LEFT JOIN [dbo].[Title] ON Customer.Customer_Title  = Title.Title_ID 
  LEFT JOIN [dbo].[Insurance_Package] ON Purchase_Order.Insurance_Package_ID = Insurance_Package.Insurance_Package_ID
  WHERE Purchase_Order.PO_ID = '".$code."' ";
  // echo "<pre>".print_r($make,1)."</pre>";
  $stmt = sqlsrv_query( $connMS, $sql );
  if(sqlsrv_has_rows($stmt)) {
    $row = sqlsrv_fetch_array($stmt);
    return $row;
    exit();
  }  
}

function getPolicyDelivery($code){
  global $connMS; 
  $resultarray = array();
 $sql = "SELECT Policy_Delivery.Delivery_Type_ID, Delivery_Type.Delivery_Type_Desc, Policy_Delivery.Messenger_ID, Policy_Delivery.Remark, Policy_Delivery.Delivery_Place, Messenger.Messenger_Name, Delivery_Status.Delivery_Status_Desc FROM [dbo].[Policy_Delivery]  
  LEFT JOIN [dbo].[Delivery_Status] ON Policy_Delivery.Delivery_Status_ID = Delivery_Status.Delivery_Status_ID
  LEFT JOIN [dbo].[Delivery_Type] ON Policy_Delivery.Delivery_Type_ID = Delivery_Type.Delivery_Type_ID
  LEFT JOIN [dbo].[Messenger] ON Policy_Delivery.Messenger_ID = Messenger.Messenger_ID
  WHERE Policy_Delivery.PO_ID = '".$code."' ";
  $stmt = sqlsrv_query( $connMS, $sql );
  if(sqlsrv_has_rows($stmt)) {
    $row = sqlsrv_fetch_array($stmt);
    return $row;
    exit();
  } 
}



function getCardetailBycode($code){
  global $connMS; 
  $resultarray = array();
  $sql = "SELECT * FROM [dbo].[Car_Detail]  
  LEFT JOIN [dbo].[Car_Make] ON Car_Detail.Make_ID = Car_Make.Make_ID
  LEFT JOIN [dbo].[Car_Type] ON Car_Detail.Car_Code = Car_Type.Car_Type_ID 
  LEFT JOIN [dbo].[Asia_Car_Model] ON Car_Detail.Model = Asia_Car_Model.Model_ID 
  LEFT JOIN [dbo].[Beneficiary] ON Car_Detail.Beneficiary = Beneficiary.Beneficiary_ID  
  WHERE Car_Detail.PO_ID = '".$code."' ";
  // echo "<pre>".print_r($make,1)."</pre>";
  $stmt = sqlsrv_query( $connMS, $sql );
  if(sqlsrv_has_rows($stmt)) {
    $row = sqlsrv_fetch_array($stmt);
    return $row;
    exit();
  }  
}

function getSubdistrictBycode($code){
  global $connMS; 
  $resultarray = array();
  $sql = "SELECT Subdistrict.Subdistrict_Name_TH FROM [dbo].[Subdistrict]  WHERE Subdistrict_ID = '".$code."' ";
  // echo "<pre>".print_r($make,1)."</pre>";
  $stmt = sqlsrv_query( $connMS, $sql );
  if(sqlsrv_has_rows($stmt)) {
    $row = sqlsrv_fetch_array($stmt);
    return $row["Subdistrict_Name_TH"];
    exit();
  }  
}

function getDistrictBycode($code){
  global $connMS; 
  $resultarray = array();
  $sql = "SELECT District.District_Name_TH FROM [dbo].[District]  WHERE District_ID = '".$code."' ";
  // echo "<pre>".print_r($make,1)."</pre>";
  $stmt = sqlsrv_query( $connMS, $sql );
  if(sqlsrv_has_rows($stmt)) {
    $row = sqlsrv_fetch_array($stmt);
    return $row["District_Name_TH"];
    exit();
  }  
}

function getOperationType($id){
  global $connMS; 
  $resultarray = array();
  $sql = "SELECT Operation_Type.Operation_Type_Desc FROM [dbo].[Operation_Type]  WHERE Operation_Type_ID = '".$id."' ";
  // echo "<pre>".print_r($make,1)."</pre>";
  $stmt = sqlsrv_query( $connMS, $sql );
  if(sqlsrv_has_rows($stmt)) {
    $row = sqlsrv_fetch_array($stmt);
    return $row["Operation_Type_Desc"];
    exit();
  }  
}


function getProvinceBycode($code){
  global $connMS; 
  $resultarray = array();
  $sql = "SELECT * FROM [dbo].[Province]  WHERE Province_ID = '".$code."' ";
  // echo "<pre>".print_r($make,1)."</pre>";
  $stmt = sqlsrv_query( $connMS, $sql );
  if(sqlsrv_has_rows($stmt)) {
    $row = sqlsrv_fetch_array($stmt);
    return $row["Province_Name_TH"];
    exit();
  }  
}

function caseTitle($name){
  switch ($name) {
    case "Customercode": return "รหัส"; break;
    case "CustomerOperation": return "การดำเนินการ"; break;
    case "Customertype": return "ประเภทลูกค้า"; break;
    case "Customernameth": return "ชื่อ-นามสกุล TH"; break;
    case "Customernameen": return "ชื่อ-นามสกุล EN"; break;
    case "cusFolder": return "เลขบัตรประชาชน / เลขทะเบียนผู้ประกอบการ"; break;
    case "cusPerson": return "ชื่อผู้ติดต่อ"; break;
    case "cusTel": return "โทรศัพท์"; break;
    case "cusFax": return "โทรสาร"; break;
    case "cusMobile": return "มือถือ"; break;
    case "cusEmil": return "eMail"; break;
    case "cusMobileI": return "Mobile I"; break;
    case "cusMobileII": return "Mobile II"; break;
    case "cusLand": return "Land Line"; break;
    case "cusLind": return "Line ID"; break;
    case "cusAddr": return "ที่อยู่"; break;
    case "cusAddSend": return "ที่อยู่ในการส่งเอกสาร"; break;
    case "cusProperties": return "รายการเอาประกัน"; break;
    case "carPlate": return "เลขทะเบียน"; break;
    case "carMake": return "ยี่ห้อรถ"; break;
    case "carModel": return "รุ่น"; break;
    case "carCodes": return "รหัสรถ"; break;
    case "carsubModel": return "รุ่นย่อย/ปี"; break;
    case "carbodyType": return "ประเภทตัวถัง"; break;
    case "carFrame": return "เลขตัวถัง"; break;
    case "carAccessory": return "อุปกรณ์ตกแต่ง"; break;
    case "carSeat": return "ที่นั่ง/ซีซี/น้ำหนัก"; break;
    case "carRepair": return "ประเภทการซ่อม"; break;
    case "carNoOf": return "จำนวนผู้ขับขี่"; break;
    case "carDriver": return "ผู้ขับขี 1"; break;
    case "carCoDriver": return "ผู้ขับขี 2"; break;
    case "carBeneficiary": return "ผู้รับผลประโยชน์"; break;
    case "carDeductible": return "หมายเหตุการแจ้งงาน [ใน iBroker]"; break;
    case "carPOAddrSend": return "ที่อยู่ในการจัดส่งเอกสาร"; break;
    case "DeliveryTypeDesc": return "ประเภทการจัดส่ง"; break;
    case "MessengerName": return "พนักงานส่ง"; break;
    case "DeliveryPlace": return "สถานที่พนักงานส่ง"; break;
    case "DeliveryStatusDesc": return "สถานะการส่ง"; break;
    case "DeliveryRemark": return "หมายเหตุในการส่งเอกสาร"; break;
    
    case "filecarback": return "รูปรถด้านท้าย"; break;
    case "filecarleft": return "รูปรถด้านซ้าย"; break;
    case "filecarright": return "รูปรถด้านขวา"; break;
    case "filecarfront": return "รูปรถด้านหน้า"; break;
    case "filecarback": return "รูปรถด้านท้าย"; break;
    case "filecarbook": return "สำเนารถ"; break;
    case "filecardriver": return "ใบขับบี่"; break;
    case "filecompany": return "หนังสือรับรองบริษัท"; break;
    case "filecompanycard": return "บัตร ปชช. / บัตรกรรมการ"; break;
    case "filereCheck1": return "หลักฐานประกอบตรวจ 1"; break;
    case "filereCheck2": return "หลักฐานประกอบตรวจ 2"; break;
    case "filereCheck3": return "หลักฐานประกอบตรวจ 3"; break;
    case "filereCheck4": return "หลักฐานประกอบตรวจ 4"; break;
    case "filereCheck5": return "หลักฐานประกอบตรวจ 5"; break;
    case "filereCheck6": return "หลักฐานประกอบตรวจ 6"; break;
    case "filecarall1": return "เอกสารอื่นๆ 1"; break;
    case "filecarall2": return "เอกสารอื่นๆ 2"; break;
    case "filecarall3": return "เอกสารอื่นๆ 3"; break;
    case "filecarall4": return "เอกสารอื่นๆ 4"; break;
    
    case "insurPackageName": return "แพจเกต"; break;
    case "insurTypeIns": return "ประกันชั้น"; break;
    case "insurNameInsur": return "บรัษัท"; break;
    case "insurCost": return "ทุนประกัน"; break;
    case "insurNetpremium": return "เบี้ยสุทธิ"; break;
    case "insurPremium": return "เบี้ยรวมภาษี"; break;
    case "insurDiscount": return "ส่วนลด"; break;
    case "insurTaxamount": return "เบี้ยรวมภาษี"; break;
    case "insurCoverageItem": return "คุ้มครอง"; break;
    case "insurCoverageStart": return "เริ่มวันคุ้มครอง"; break;
    case "insurCoverageEnd": return "สิ้นสุดวันคุ้มครอง"; break;
    case "insurPOAddrSendRemark": return "หมายเหตุการจัดส่งเอกสาร"; break;
    
  }
}


function setStatus($name){
  switch ($name) {
    case "0": return "<b style='color: #2457ff;'>รอตรวจ</b>"; break;
    case "1": return "<b style='color: #f44;'>ไม่ผ่าน</b>"; break;
    case "2": return "<b style='color: #ff8000;'>รอแจ้งงาน</b>"; break;
    case "3": return "<b style='color: #c600c4;'>แจ้งงาน</b>"; break;
    case "4": return "<b style='color: #00ac0a;'>เสร็จ</b>"; break;
  }
}
function getAdminFromInsurer($insurer){
    global $conn2; 
    $rs = $conn2->GetROW( "SELECT insurer_admin FROM ck_insurer WHERE insurer_initials ='".$insurer."' ");
    return $rs["insurer_admin"];   
}

function getNotiWorkCheck($num, $po){
  global $conn2;
  $noti_work_chk = $conn2->GetAll( "SELECT * FROM noti_work_chk WHERE noti_work_code ='".$num."' AND po_id ='".$po."' ORDER BY datetime DESC  ");
  return $noti_work_chk;   
}

function getInsuranceType(){
  global $connMS; 
  $resultarray = array();
  $sql = "SELECT Insurance_Name FROM [dbo].[Insurance] WHERE Insurance_Group_ID = 'MT' ";
  // echo "<pre>".print_r($make,1)."</pre>";
  $stmt = sqlsrv_query( $connMS, $sql );
  if(sqlsrv_has_rows($stmt)) {
      while( $row = sqlsrv_fetch_array($stmt) ) { 
        $resultarray[] = $row;
      }
      return $resultarray;
      exit();
  } 
}
function getCoverageItem($id){
  global $connMS; 
  $resultarray = array();
  $sql = "SELECT Purchase_Order_Coverage_Item.Coverage_Item_ID, Purchase_Order_Coverage_Item.Coverage_Value, Purchase_Order_Coverage_Item.Order_Index,
  Coverage_Item.Coverage_Item_Desc, Coverage_Item.Coverage_Item_Unit, Coverage_Item.Remark
  FROM [dbo].[Purchase_Order_Coverage_Item] 
  INNER JOIN [dbo].[Coverage_Item] ON Purchase_Order_Coverage_Item.Coverage_Item_ID = Coverage_Item.Coverage_Item_ID
  WHERE Purchase_Order_Coverage_Item.POID = '".$id."' 
  ORDER BY Purchase_Order_Coverage_Item.Order_Index ASC";
  // echo "<pre>".print_r($make,1)."</pre>";
  $stmt = sqlsrv_query( $connMS, $sql );
  if(sqlsrv_has_rows($stmt)) {
      while( $row = sqlsrv_fetch_array($stmt) ) { 
        $resultarray[] = $row;
      }
      return $resultarray;
      exit();
  } 
}

function getInsurerByAdmin($code){
  global $conn2; 
  $rs = $conn2->GetAll( "SELECT * FROM ck_insurer WHERE insurer_admin ='".$code."' ");
  return $rs; 
}

function getImageEdit($code, $file_type){
  global $conn2; 
  $rs = $conn2->GetROW( "SELECT * FROM noti_work_files WHERE noti_work_code ='".$code."' AND file_type ='".$file_type."' ");
  return $rs;
}
function getSendPOByAdmin($code, $status, $pocode){
  global $conn2; 
  $arr = array();
  // $getInsurerByAdmin = getInsurerByAdmin($code);
  if(!$pocode){
    if($code == "All"){
      $sql = "SELECT noti_work.*,ck_personnel.personnel_firstname AS chk_name, ck_personnel.personnel_lastname AS chk_lname
      -- , (SELECT personnel_branch from ck_personnel Where ck_personnel.personnel_code = noti_work.personnel_code) AS personnel_branch
        FROM noti_work LEFT JOIN ck_personnel ON noti_work.chk_code = ck_personnel.personnel_code
        WHERE noti_work.enable='1' AND noti_work.status IN (".$status.")";
        // $arr = $conn2->GetAssoc( $sql);
    }else{
      // foreach ($getInsurerByAdmin as $key => $value) {
        $sql = "SELECT noti_work.*,ck_personnel.personnel_firstname AS chk_name, ck_personnel.personnel_lastname AS chk_lname 
        -- , (SELECT personnel_branch from ck_personnel Where ck_personnel.personnel_code = noti_work.personnel_code) AS personnel_branch
        FROM noti_work LEFT JOIN ck_personnel ON noti_work.chk_code = ck_personnel.personnel_code
        WHERE noti_work.admin_code ='".$code."' AND noti_work.enable='1' AND noti_work.status IN (".$status.")";
        // echo $sql;
        // $arr = $conn2->GetAssoc($sql);
        // foreach ($rs as $key => $valRS) {
        //   array_push($arr, $valRS);
        // }
      // }
    }
    if($status == 4){
      $sql .= "ORDER BY noti_work.datetime_send DESC, noti_work.datetime_send_open DESC LIMIT 0,200";
    }else{
      
      $sql .= "ORDER BY noti_work.urgent DESC, noti_work.status DESC, noti_work.datetime_send DESC  LIMIT 0,200";
    }
    $arr = $conn2->GetAssoc($sql);


  }else{
    $sql = "SELECT noti_work.*,ck_personnel.personnel_firstname AS chk_name, ck_personnel.personnel_lastname AS chk_lname
    -- , (SELECT personnel_branch from ck_personnel Where ck_personnel.personnel_code = noti_work.personnel_code) AS personnel_branch 
        FROM noti_work LEFT JOIN ck_personnel ON noti_work.chk_code = ck_personnel.personnel_code
        WHERE noti_work.po_code LIKE '%".$pocode."%' ORDER BY noti_work.urgent DESC, noti_work.datetime_chk DESC ";
        $rs = $conn2->GetAll( $sql);
        foreach ($rs as $key => $valRS) {
          array_push($arr, $valRS);
        }
  }
  echo $sql;
  return $arr;
}



function getPersonnelByType($types=NULL){
	global $conn2; 
  if($types){
    $sql = "SELECT * FROM ck_personnel WHERE personnel_type ='".$types."' AND personnel_status = '1' ORDER BY personnel_code";
  }else{
    $sql = "SELECT * FROM ck_personnel WHERE personnel_status = '1' ORDER BY personnel_code";
  }
  $rs = $conn2->GetAll($sql);
  return $rs;

}

function getCommentLists(){
	global $conn2; 
  $rs = $conn2->GetAll( "SELECT * FROM comment_lists ORDER BY comment_lists_id ");
  return $rs;

}

function getCommentStatus(){
	global $conn2; 
  $rs = $conn2->GetAll( "SELECT * FROM comment_status ORDER BY comment_status_id ");
  return $rs;
}

function getNotiCommentByAddto($code){
  global $conn2; 
  $rs = $conn2->GetAll( "SELECT * FROM noti_comment INNER JOIN noti_work ON noti_comment.noti_work_id = noti_work.noti_work_id  WHERE noti_comment.comment_to = '".$code."' ORDER BY noti_comment.datetime DESC LIMIT 0,100");
  return $rs;

}

function getFollowupBydate($date, $user){
  global $connMS; 
  $UserType = $_SESSION["User"]['type'];
  $resultarray = array();
  $datenext = date("Y-m-d", strtotime($date . "+1 days"));
  $sql = "SELECT Followup.Followup_ID, Followup.Followup_Detail, Followup.Compare_Policy_ID, Followup.Followup_DateTime, 
  Remind.Remind_Date, Followup_Status.Followup_Status_Desc, Followup_Type.Followup_Type_Desc, Customer.Customer_id , Customer.Customer_FName, Customer.Customer_LName, 
  Customer.Tel_No, Customer.Mobile_No, Customer.EMail,Remind_Method.Remind_Method_ID,Remind_Method.Remind_Method_Desc
  FROM [dbo].[Followup] 
  INNER JOIN [dbo].[Followup_Status] ON Followup.Followup_Status_ID = Followup_Status.Followup_Status_ID
  INNER JOIN [dbo].[Followup_Type] ON Followup.Followup_Type_ID = Followup_Type.Followup_Type_ID
  INNER JOIN [dbo].[Customer] ON Followup.Customer_ID = Customer.Customer_ID
  LEFT JOIN [dbo].[Remind] ON Followup.Followup_ID = Remind.Remark
  LEFT JOIN [dbo].[Remind_Method] ON Remind.Remind_Method_ID = Remind_Method.Remind_Method_ID
  WHERE Remind.Remind_Date >= '".$date."' AND Remind.Remind_Date < '".$datenext."' AND  Followup.Active = 'Y' ";
  if($UserType != "SuperAdmin"){
    $sql .= " AND Followup.Followup_By = '".$user."' "; 
  }  
  $sql .= " ORDER BY Remind.Remind_Date ASC"; 
  // echo $sql;
  // echo "<pre>".print_r($make,1)."</pre>";
  $stmt = sqlsrv_query( $connMS, $sql );
  if(sqlsrv_has_rows($stmt)) {
      while( $row = sqlsrv_fetch_array($stmt) ) { 
        $resultarray[] = $row;
      }
      return $resultarray;
      exit();
  } 
}

function getCountPOAllDate($date, $user){
  global $connMS; 
    $resultarray = array();
    $datestart = $date." 00:00:00";
    // $dateNow = date("Y-m-d", strtotime($datestart . "+1 days"))." 23:59:59";
    $dateNow = date("Y-m-d", strtotime($datestart))." 23:59:59";
    $sql = "SELECT  count(Purchase_Order.PO_ID) As count, sum(abs(Purchase_Order.Premium_After_Disc)) AS sumTotal
    FROM [dbo].[Purchase_Order] 
    LEFT JOIN  [dbo].[Installment] ON Purchase_Order.PO_ID = Installment.PO_ID 
    LEFT JOIN [dbo].Receive_Installment ON Installment.PO_ID = Receive_Installment.PO_ID and Receive_Installment.Installment_ID= Installment.Installment_ID
    LEFT JOIN [dbo].[Receive] ON Receive_Installment.Receive_ID = Receive.Receive_ID
    WHERE Receive.Receive_Date >= '".$datestart."' AND Receive.Receive_Date <= '".$dateNow."' AND  Purchase_Order.Active = 'Y' AND Receive.Receive_Status_ID = '003' AND Receive_Installment.Installment_ID = '1' AND Purchase_Order.Operation_Type_ID IN ('N','R') AND Purchase_Order.Status_ID IN ('RV','RVP','CLS','REJ') ";

    if($_SESSION["User"]['type'] == "Sale" && $_SESSION["User"]['team']){
      $sql .= " AND Employee_ID = '".$user."' "; 
    } 
    // echo $sql; 
    $stmt = sqlsrv_query( $connMS, $sql );
    if(sqlsrv_has_rows($stmt)) {
      $row = sqlsrv_fetch_array($stmt);
      return $row;
      exit();
    }
}

function getCountPOStatus($user, $status, $Operation, $case, $mounth){
  global $connMS; 
    $resultarray = array();
    $datestart =  date("Y")."-".$mounth."-01";
    // $dateNow = date("Y-m-d")." 23:59:59";
    if($mounth == date("n")){
      $dateNow = date("Y-m-d")." 23:59:59";
    }else{
      $dateNow = date("Y")."-".$mounth."-".date('t',strtotime(date("Y")."-".$mounth))." 23:59:59";
    }
    if($case == "List"){
      $sql = "SELECT Receive.Receive_Date,Purchase_Order.PO_Date, Purchase_Order.PO_ID, Purchase_Order.Employee_ID, Purchase_Order.Operation_Type_ID, Purchase_Order.Status_ID,Purchase_Order.Premium_After_Disc, Customer.Customer_FName, Customer.Customer_LName, Customer.Tel_No";
    }else{
      $sql = "SELECT  count(Purchase_Order.PO_ID) As count, sum(Purchase_Order.Premium_After_Disc) AS sumTotal ";
    }
    $sql .= " FROM [dbo].[Purchase_Order] 
    LEFT JOIN  [dbo].[Installment] ON Purchase_Order.PO_ID = Installment.PO_ID
    LEFT JOIN  [dbo].[Customer] ON Purchase_Order.Customer_ID = Customer.Customer_ID";
    if($Operation){
     $sql .= " LEFT JOIN [dbo].Receive_Installment ON Installment.PO_ID = Receive_Installment.PO_ID and Receive_Installment.Installment_ID= Installment.Installment_ID 
      LEFT JOIN [dbo].[Receive] ON Receive_Installment.Receive_ID = Receive.Receive_ID
      WHERE Receive.Receive_Date >= '".$datestart."' AND Receive.Receive_Date <= '".$dateNow."' AND  Purchase_Order.Active = 'Y' 
      AND Receive.Receive_Status_ID = '003' AND Receive_Installment.Installment_ID = '1' ";
    }else{
      $sql .= "
      WHERE Purchase_Order.PO_Date >= '".$datestart."' AND Purchase_Order.PO_Date <= '".$dateNow."' AND  Purchase_Order.Active = 'Y' ";
    }
    
    if($_SESSION["User"]['type'] == "Sale" && $_SESSION["User"]['team']){
      $sql .= " AND Purchase_Order.Employee_ID = '".$user."' "; 
    }
    if($Operation == 'N'){
       $sql .= " AND Purchase_Order.Operation_Type_ID = 'N'  ";
    }else if($Operation == 'R'){
       $sql .= " AND Purchase_Order.Operation_Type_ID = 'R'  ";
    }else if($Operation == 'C'){
       $sql .= " AND Purchase_Order.Operation_Type_ID = 'C'  ";
    }else if($Operation == 'D'){
       $sql .= " AND Purchase_Order.Operation_Type_ID = 'D'  ";
    }
    if($status){
      // if($status == "CBE"){
      //   $sql .= " AND Purchase_Order.Status_ID IN ('CBE','CBW','REJ')  ";
      // }else if($status == "process"){
      //   $sql .= " AND Purchase_Order.Status_ID NOT IN ('RV','RVP','CLS', 'REJ')  ";
      // }else if($status == "All"){
      //   $sql .= " AND Purchase_Order.Status_ID IN ('RV','RVP','CLS','CBE','CBW','REJ')  ";
      // }else{
      //   $sql .= " AND Purchase_Order.Status_ID = '".$status."' ";
      if($status == "process"){
        $sql .= " AND Purchase_Order.Status_ID NOT IN ('RV','RVP','CLS', 'REJ')  ";
      }
    } 
    if($Finance){
      $sql .= " AND Finance_Status_ID = '".$Finance."' ";
    } 
    // echo $sql;
    $stmt = sqlsrv_query( $connMS, $sql );
    if($case == "List"){
      if(sqlsrv_has_rows($stmt)) {
        while( $row = sqlsrv_fetch_array($stmt) ) { 
          $resultarray[] = $row;
        }
        return $resultarray;
        exit();
      }  
    }else{
      if(sqlsrv_has_rows($stmt)) {
        $row = sqlsrv_fetch_array($stmt);
        return $row;
        exit();
      } 
    }
}


function getCountPOReportforIndex($mounth){
  global $connMS;
  $resultarray = array();
    $datestart =  date("Y")."-".$mounth."-01";
    // $dateNow = date("Y-m-d")." 23:59:59";
    if($mounth == date("n")){
      $dateNow = date("Y-m-d")." 23:59:59";
    }else{
      $dateNow = date("Y")."-".$mounth."-".date('t',strtotime(date("Y")."-".$mounth))." 23:59:59";
    }
   
   $sql = "SELECT 
    SUM(case when Purchase_Order.Operation_Type_ID = 'N' AND Purchase_Order.Status_ID IN ('RV','RVP','CLS','REJ')  then Purchase_Order.Premium_After_Disc end) AS sumNew
    ,count(case when Purchase_Order.Operation_Type_ID = 'N' AND Purchase_Order.Status_ID IN ('RV','RVP','CLS','REJ')  then Purchase_Order.PO_ID end) AS sumNewCount
    ,SUM(case when Purchase_Order.Operation_Type_ID = 'R' AND Purchase_Order.Status_ID IN ('RV','RVP','CLS','REJ')  then Purchase_Order.Premium_After_Disc end) AS sumRenew
    ,count(case when Purchase_Order.Operation_Type_ID = 'R' AND Purchase_Order.Status_ID IN ('RV','RVP','CLS','REJ')  then Purchase_Order.PO_ID end) AS sumRenewCount
    ,SUM(case when Purchase_Order.Operation_Type_ID = 'C' then Purchase_Order.Premium_After_Disc end) AS sumC
    ,count(case when Purchase_Order.Operation_Type_ID = 'C' then Purchase_Order.PO_ID end) AS sumCCount
    ,SUM(case when Purchase_Order.Operation_Type_ID = 'D' then Purchase_Order.Premium_After_Disc end) AS sumD
    ,count(case when Purchase_Order.Operation_Type_ID = 'D' then Purchase_Order.PO_ID end) AS sumDCount
    ,(select SUM(Purchase_Order.Premium_After_Disc) 
    FROM [dbo].[Purchase_Order]
    WHERE Purchase_Order.PO_Date >= '".$datestart."' 
    AND Purchase_Order.PO_Date <= '".$dateNow."' 
    AND Purchase_Order.Agent_ID = '00001'
    AND Purchase_Order.Active = 'Y' 
    AND Purchase_Order.Status_ID NOT IN ('RV','RVP','CLS', 'REJ')  ";
    if($_SESSION["User"]['type'] == "Sale" && $_SESSION["User"]['team']){
      $sql .= " AND Purchase_Order.Employee_ID = '".$_SESSION["User"]['UserCode']."' "; 
    }
    $sql .= " ) AS sumPaying
    
    ,(select count(Purchase_Order.PO_ID) 
    FROM [dbo].[Purchase_Order]
    WHERE Purchase_Order.PO_Date >= '".$datestart."' 
    AND Purchase_Order.PO_Date <= '".$dateNow."' 
    AND Purchase_Order.Agent_ID = '00001'
    AND Purchase_Order.Active = 'Y' 
    AND Purchase_Order.Status_ID NOT IN ('RV','RVP','CLS', 'REJ') ";
    if($_SESSION["User"]['type'] == "Sale" && $_SESSION["User"]['team']){
      $sql .= " AND Purchase_Order.Employee_ID = '".$_SESSION["User"]['UserCode']."' "; 
    }
    $sql .= " ) AS sumPayingCount
    FROM [dbo].[Purchase_Order] 
    LEFT JOIN [dbo].[Installment] ON Purchase_Order.PO_ID = Installment.PO_ID
    LEFT JOIN [dbo].Receive_Installment ON Installment.PO_ID = Receive_Installment.PO_ID and Receive_Installment.Installment_ID= Installment.Installment_ID 
    LEFT JOIN [dbo].[Receive] ON Receive_Installment.Receive_ID = Receive.Receive_ID
    WHERE 
    Purchase_Order.Active = 'Y' 
    AND Purchase_Order.Agent_ID = '00001' 
    AND Receive.Receive_Status_ID = '003' 
    AND Receive_Installment.Installment_ID = '1'
    AND Receive.Receive_Date >= '".$datestart."' AND Receive.Receive_Date <= '".$dateNow."' ";

    if($_SESSION["User"]['type'] == "Sale" && $_SESSION["User"]['team']){
      $sql .= " AND Purchase_Order.Employee_ID = '".$_SESSION["User"]['UserCode']."' "; 
    }
  // echo $sql;
  $stmt = sqlsrv_query( $connMS, $sql );
    if(sqlsrv_has_rows($stmt)) {
      $row = sqlsrv_fetch_array($stmt);
      return $row;
      exit();
    }
}


function getCPbyPhone($phone){
  global $connMS;
  $resultarray = array();
  $sql = "SELECT Compare_Policy.CP_PO_ID, Compare_Policy.CP_Date, Compare_Policy.CP_ID, Compare_Policy.Compare_Status_ID, Compare_Policy.Active, Customer.Customer_ID, Customer.Customer_FName, Customer.Customer_LName, Customer.Tel_No, My_User.User_FName, My_User.User_LName, Customer.Mobile_No
  FROM [dbo].[Compare_Policy] 
  INNER JOIN [dbo].[Customer] ON Compare_Policy.Customer_ID = Customer.Customer_ID  
  INNER JOIN [dbo].My_User ON Compare_Policy.Employee_ID = My_User.User_ID 
  WHERE Customer.Mobile_No LIKE '%".$phone."%' OR Customer.Tel_No LIKE '%".$phone."%'  ";
  // echo $sql;
  $stmt = sqlsrv_query( $connMS, $sql );
  if(sqlsrv_has_rows($stmt)) {
      while( $row = sqlsrv_fetch_array($stmt) ) { 
        $resultarray[] = $row;
      }
      return $resultarray;
      exit();
  } 
}


function getPOMarineBycode($code){
  global $connMS; 
    $resultarray = array();
    $sql = "SELECT Purchase_Order.Marine_Reference FROM [dbo].[Purchase_Order] WHERE PO_ID = '".$code."' "; 
    $stmt = sqlsrv_query( $connMS, $sql );
    if(sqlsrv_has_rows($stmt)) {
      $row = sqlsrv_fetch_array($stmt);
      return $row["Marine_Reference"];
      exit();
    }
}

function getPolicyNumber($code){
  global $connMS; 
    $resultarray = array();
    $sql = "SELECT Policy_No, Compulsory_No FROM [dbo].[Purchase_Order_Insurer] WHERE PO_ID = '".$code."' "; 
    $stmt = sqlsrv_query( $connMS, $sql );
    if(sqlsrv_has_rows($stmt)) {
      $row = sqlsrv_fetch_array($stmt);
      return $row;
      exit();
    }
}

function getInstallmentPO($po){
  global $connMS;
  $resultarray = array();
  $getPOMarineBycode = getPOMarineBycode($po);
  $sql = "SELECT Installment.PO_ID, Installment.Installment_ID, Installment.ISTM_Total_Amount, Installment.Installment_Due_Date, 
  Installment.ISTM_Net_Premium, Installment.ISTM_Duty, Installment.ISTM_Tax, Installment.ISTM_Total_Premium,
  Installment.ISTM_Discount, Installment.ISTM_Total_Amount, Installment_Status.Installment_Status, Purchase_Order.Customer_ID, Insurance.Insurance_Name
  FROM [dbo].[Installment]
  INNER JOIN [dbo].[Purchase_Order] ON Installment.PO_ID = Purchase_Order.PO_ID
  INNER JOIN [dbo].[Insurance] ON Purchase_Order.Insurance_ID = Insurance.Insurance_ID   
  INNER JOIN [dbo].[Installment_Status] ON Installment.Installment_Status_ID = Installment_Status.Installment_Status_ID
  WHERE Installment.PO_ID = '".$po."' ";
  if($getPOMarineBycode){
    $sql .= " OR Installment.PO_ID = '".$getPOMarineBycode."' ";
  }
  $sql .= "ORDER BY Installment.Installment_ID ASC  ";
  // echo $sql;
  $stmt = sqlsrv_query( $connMS, $sql );
  if(sqlsrv_has_rows($stmt)) {
      while( $row = sqlsrv_fetch_array($stmt) ) { 
        $resultarray[] = $row;
      }
      return $resultarray;
      exit();
  } 
}



function getGiftvoucher($status){
  global $conn2; 
  $rs = $conn2->GetAll( "SELECT * FROM noti_work WHERE giftvoucher IS NOT NULL AND giftvoucher != '0' AND giftvoucher != '' AND enable = 1 AND status_giftvoucher = '".$status."' ORDER BY created_date DESC ");
  return $rs;
}

function getRowpersonnelByCode($code){
  global $conn2; 
  $rs = $conn2->GetRow("SELECT * FROM ck_personnel WHERE personnel_code = '".$code."'  ");
  return $rs;
}


function getInsuranceRequest($pocode){
  global $connMS; 
    $resultarray = array();
    $sql = "SELECT * FROM [dbo].[Insurance_Request] WHERE PO_ID = '".$pocode."' "; 
    $stmt = sqlsrv_query( $connMS, $sql );
    if(sqlsrv_has_rows($stmt)) {
      $row = sqlsrv_fetch_array($stmt);
      return $row;
      exit();
    }
}

function getRowInstallment($pocode, $insID){
  global $connMS; 
    $resultarray = array();
    $sql = "SELECT * FROM [dbo].[Installment] WHERE PO_ID = '".$pocode."' AND Installment_ID = '".$insID."'"; 
    $stmt = sqlsrv_query( $connMS, $sql );
    if(sqlsrv_has_rows($stmt)) {
      $row = sqlsrv_fetch_array($stmt);
      return $row;
      exit();
    }
}

function delMyPicBarcode(){
  $files = glob('picBarcode/*'); // get all file names
  if($files){
    foreach($files as $file){ // iterate files
      if(is_file($file))
        // echo $file."<br>";
        unlink($file); // delete file
    }
  }
}


function getNotiCode($Insurance_ID, $poCode){
  global $conn2; 
  
  $number = $conn2->GetOne("SELECT count(noti_work_id) FROM noti_work WHERE date(created_date) = '".date("Y-m-d")."' ");
  
  if($Insurance_ID == "029"){
    $noti_work_code =  'A'.date("ymds")."".substr($poCode, 8, 9)."".str_pad($number+1, 3, '0', STR_PAD_LEFT);
  }else{
    $noti_work_code =  'N'.date("ymds")."".substr($poCode, 8, 9)."".str_pad($number+1, 3, '0', STR_PAD_LEFT);
  }

  return $noti_work_code;
}


function searchPO($pocode, $search_type){
  global $connMS; 
  
  // $pocode = trim($_POST["pocode"]);
  $resultarray = array();
  $sql = "SELECT 
  Purchase_Order.PO_ID, Purchase_Order.Coverage_Start_Date, Purchase_Order.Capital, Purchase_Order.Insurance_Package_ID, Purchase_Order.Compulsory, Purchase_Order.Net_Premium, Purchase_Order.Discount, Purchase_Order.Total_Premium, Purchase_Order.Premium_After_Disc,  Purchase_Order.Deductible, Insurance_Package.Insurance_Package_Name, Purchase_Order.Marine_Reference, ADB_Premium.Premium_Desc, Insurance.Insurance_Name, Customer.Customer_Type AS cus_type, Title.Title_Name AS Title_Name, Customer.Customer_FName AS FName, Customer.Customer_LName AS LName, Insurer.Insurer_Initials AS Insurer_Initials,
  Delivery_Type.Delivery_Type_Desc, Insurer.Insurer_Name, Policy_Delivery.Delivery_Type_ID
  FROM [dbo].[Purchase_Order] INNER JOIN [dbo].[Customer] ON Purchase_Order.Customer_ID = Customer.Customer_ID 
  LEFT JOIN [dbo].[Purchase_Order_Insurer] ON Purchase_Order.PO_ID = Purchase_Order_Insurer.PO_ID 
  LEFT JOIN [dbo].[Insurance] ON Purchase_Order.Insurance_ID = Insurance.Insurance_ID
  LEFT JOIN [dbo].[Insurer] ON Purchase_Order_Insurer.Insurer_ID = Insurer.Insurer_ID
  LEFT JOIN [dbo].[Title] ON Customer.Customer_Title  = Title.Title_ID 
  LEFT JOIN [dbo].[Insurance_Package] ON Purchase_Order.Insurance_Package_ID = Insurance_Package.Insurance_Package_ID
  LEFT JOIN [dbo].[ADB_Premium] ON Purchase_Order.Premium_ID = ADB_Premium.Premium_ID 
  LEFT JOIN [dbo].[Policy_Delivery] ON Purchase_Order.PO_ID = Policy_Delivery.PO_ID
  LEFT JOIN [dbo].[Delivery_Type] ON Policy_Delivery.Delivery_Type_ID = Delivery_Type.Delivery_Type_ID";

  if($search_type == 1){
    $sql .= " WHERE Purchase_Order.PO_ID = '".$pocode."' ";
  }else{
    $sql .= " WHERE Purchase_Order.Customer_ID = '".$pocode."' ORDER BY Purchase_Order.Create_Date DESC";
  }
  // echo $sql;
  $stmt = sqlsrv_query( $connMS, $sql );
  if(sqlsrv_has_rows($stmt)) {
      while( $row = sqlsrv_fetch_array($stmt) ) {
        $resultarray[] = $row;
      }
      return $resultarray;
      exit();
  }
}

function getChkWorkresend($po_code){
  global $conn2; 
  $rs = $conn2->GetRow("SELECT * FROM noti_work WHERE po_code = '".$po_code."' AND enable = '1'  ");
  return $rs["po_code"];
}



/* ******************************  sand email ****************************** */

function getEmailInsur($code){
  global $conn2; 
  // echo "SELECT * FROM insur_email WHERE ck_insurer_key = '".$code."' AND enable = '1' ";
  $rs = $conn2->GetAll("SELECT * FROM insur_email WHERE ck_insurer_key = '".$code."' AND enable = '1' ");
  return $rs;
}


function getEmailInsurAll($code){
  global $conn2; 
  $rs = $conn2->GetAll("SELECT * FROM insur_email WHERE ck_insurer_key = '".$code."' ");
  return $rs;
}

function getEmailInsurCancel($code)
{
    global $conn2;
    $rs = $conn2->GetAll("SELECT * FROM insur_email WHERE ck_insurer_key = '" . $code . "' AND active_cancel = '1' ");
    return $rs;
}


function getSupervisor($code){
  global $conn2; 
  $rs = $conn2->GetRow("SELECT * FROM ck_personnel WHERE personnel_code = '".$code."'  ");
  return $rs;
}


function mailInsur($arr, $files){

  
  $mail = new PHPMailer\PHPMailer\PHPMailer(true);
  $mail->CharSet = 'utf-8';
  $mail->SMTPDebug = 0;   
  $mail->Timeout   = 90;                              // Enable verbose debug output
  $mail->isSMTP();                                      // Set mailer to use SMTP
  $mail->Host = 'smtp.gmail.com'; // Specify main and backup SMTP servers
  $mail->SMTPAuth = true;  
  $mail->Username = "admin@asiadirect.co.th";
  $mail->Password = "@db6251000";
  $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
  $mail->Port = 587;                                    // TCP port to connect to
  // $mail->setFrom('admin@asiadirect.co.th', 'Asia Direct Broker');
  $mail->setFrom($_SESSION["User"]['email'], $_SESSION["User"]['firstname']." Asia Direct" );
  $mail->AddReplyTo($_SESSION["User"]['email'], $_SESSION["User"]['firstname']." Asia Direct" );
  if($_POST["mailsend"]){ 
    foreach ($_POST["mailsend"] as $key => $valMail) {
      $nameMail = explode("|", $valMail);
      $nameemail = $nameMail[1] ? $nameMail[1] : $nameMail[0];
      if(ereg_replace('[[:space:]]+', ' ', trim($nameMail[2])) == "CC"){
        $mail->AddCC($nameMail[0], $nameemail);
      }else{
        $mail->AddAddress($nameMail[0], $nameemail); //ลุกค้า
      }
    }
  }

  // $mail->AddAddress('teerakan.s@asiadirect.co.th', 'IT');               // Name is optional
  $mail->isHTML(true);                                  // Set email format to HTML
  $mail->Subject = $arr["subject"];
  $mail->Body    = $arr["bodymail"];
//   Attachments
  if($arr["files"] ){
    foreach ($arr["files"] as $key => $value) {
      $namefile = explode("/", $value);
      $path = '../'.$value;
      $mail->addAttachment($path, $namefile[2]); 
    }
  }


  if($files){
    foreach ($files["files"]['tmp_name'] as $key => $value) {
      $uploadfile = tempnam(sys_get_temp_dir(), sha1($_FILES['files']['name'][$key]));
      $filename = $_FILES['files']['name'][$key];
      if (move_uploaded_file($_FILES['files']['tmp_name'][$key], $uploadfile)) {
          $mail->addAttachment($uploadfile, $filename);
      }
    }
  }

  if(!$mail->Send()) {
    return 0;
  } else {
    return 1;
  }
  exit();
}



function curlLineNotiCHK($user, $msg){
  $url  = 'https://www.asiadirect.co.th/line-at/ibroker/linemeapi.php';
  $data = array(
      'user_id' => $user,
      'status' => "CheckWork",
      'messages' => $msg
  );
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
    curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $result = curl_exec($ch);
    $error_msg = curl_error($ch);
    curl_close($ch);
  return 1;
  exit();
}

