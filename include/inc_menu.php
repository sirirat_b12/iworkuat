<!-- WRAPPER -->
<div id="wrapper">
    <!-- NAVBAR -->
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container-fluid">
            <div id="navbar-menu">
            	<ul class="nav navbar-nav navbar-left">
                    <li><a href="chkinser.php" title="เมนูเช็คเบี้ย" ti>เช็คเบี้ย</a></li>

                    <?php if($_SESSION["User"]['type'] != "Admin"){ ?>
                        <?php if($_SESSION["User"]['type'] != "CallCenter"  ){ ?>
                        <li><a href="chklists.php" title="เมนูแสดงรายการทั้งหมด">รายการเช็คเบี้ย</a></li>
                        <?php } ?>
                        <li><a href="sendorders.php" title="เมนู แจ้งงาน">แจ้งงาน</a></li>
                    <?php } ?>

                        <li><a href="offerinsurance.php" title="ข้อเสนอประกันภัย">CP</a></li>
                    <?php if($_SESSION["User"]['type'] == "SuperAdmin" || $_SESSION["User"]['type'] == "Admin"  ){ ?>
                        <li><a href="ordersadmins.php" title="เมนู แจ้งงาน">แจ้งงาน Admin</a></li>
                       
                    <?php } ?>

                    <li><a href="#" class="chkCustomer" title="เช็คข้อมูล CP ">เช็คข้อมูล</a></li>
            	</ul>
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown">
                        <div class="mt25 c2457ff  fwb fs25">iWorkUAT</div>
                       <!--  <?php if($_SESSION["User"]['type'] != "Accounting"){ ?>
	                        <a href="#" class="dropdown-toggle icon-menu" data-toggle="dropdown" onclick="notificationTime('<?php echo $_SESSION["User"]['UserCode'] ?>')">
	                            <i class="lnr lnr-alarm"></i><span class="badge bg-danger"></span>
	                        </a>
                        <?php } ?> -->
                        <ul class="dropdown-menu notifications p5" id="liNoti"></ul>
                    </li>
                     <?php //if($_SESSION["User"]['type'] == "SuperAdmin"){ ?>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle icon-menu" data-toggle="dropdown"><i class="fas fa-folder-open"></i></a>
                            <ul class="dropdown-menu">
                                <li><a href="ins_follow.php">งวดชำระ</a></li>
                                <li><a href="cancelpolicy.php">ยกเลิกกรมธรรม์</a></li>
                                <li><a href="listpayment_tor.php">เอกสารผ่อน</a></li>
                                
                            </ul>
                        </li>
                    <?php // } ?>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle icon-menu" data-toggle="dropdown"><i class="fas fa-hand-holding-usd"></i></a>
                        <ul class="dropdown-menu">
                                <li><a href="pay_omise.php">QR Code [Omise]</a></li>
                                <li><a href="pay_qrcode.php">QR Code PromptPay</a></li>
                            <?php if($_SESSION["User"]['type'] == "SuperAdmin"){ ?>
                                <li><a href="listpayment.php">สร้าง QR Code เก่า</a></li>
                            <?php } ?>
                            <li><a href="counters.php">Counter Service</a></li>
                            <li><a href="listpayment.php">ชำระธนาคาร</a></li>
                            <!-- <li><a href="https://www.asiadirect.co.th/2c2p/pay/send.php" target="_torasia">บัตรเครดิต [ผ่อนชำระ]</a></li> -->
                            <li><a href="https://www.asiadirect.co.th/2c2p/pay/index.php" target="_torasia">บัตรเครดิต [ผ่อนชำระ]</a></li>
                            <li><a href="https://www.asiadirect.co.th/pay/index.php" target="_full">บัตรเครดิต [ชำระเต็ม]</a></li>
                            <li><a href="https://www.asiadirect.co.th/payplus/send.php" target="_kplus">K-Plus กสิกรไทย</a></li>
                            <?php if($_SESSION["User"]['type'] == "SuperAdmin" || $_SESSION["User"]['type'] == "Accounting"){ ?>
                                <li><a href="counterspay.php">Counter Service Pay</a></li>
                                <li><a href="pay_qrcodelists.php">QR Code New Pay</a></li>
                            <?php } ?>
                            
							<!-- <li><a href="#tor_mmodal" data-toggle="modal" data-target="#tor_mmodal">omise dev</a></li> -->
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle icon-menu" data-toggle="dropdown"><i class="fas fa-chess-queen"></i></a>
                        <ul class="dropdown-menu notifications">
                            <li><a href="#" class="btnUser">แก้ไขข้อมูลส่วนตัว</a></li>
                            <li><a href="adbfiles.php" class="btnUser">ค้นหา File</a></li>
                            <li><a href="confirmpo.php" class="btnUser">Confirm PO</a></li>
                            <li><a href="insurrepair.php" class="btnUser">อู่ซ่อมรถ</a></li>
                            <li><a href="roadsides.php" class="btnUser">ช่วยเหลือฉุกเฉิน</a></li>
                            <li><a href="searchphone.php">ค้นหา CP</a></li>
                            <li><a href="teamorders.php">Team Orders</a></li>
                            <li><a href="delivery_back.php">จดหมายตีกลับ</a></li>
                            <?php if($_SESSION["User"]['type'] == "CallCenter" || $_SESSION["User"]['type'] == "SuperAdmin" || $_SESSION["User"]['type'] == "Admin"  ){ ?>
                                <li><a href="callcenters.php">Callcenters</a></li>
                                <li><a href="settingcomment.php">เพิ่ม หมายเหตุ</a></li>
                            <?php } ?>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle icon-menu" data-toggle="dropdown"> <i class="fa fa-cogs"></i></a>
                        <ul class="dropdown-menu notifications">
                            <?php if($_SESSION["User"]['type'] == "SuperAdmin" || $_SESSION["User"]['type'] == "Admin"  ){ ?>
                                <li><a href="companybyadmin.php">ผู้รับผิดชอบแจ้งงาน</a></li>
                                <li><a href="emailinsur.php">อีเมล์บริษัทประกัน</a></li>
                                
                                <?php if($_SESSION["User"]['type'] == "SuperAdmin" || $_SESSION["User"]['UserCode'] == "ADB59009"  ){ ?>
                                    <li><a href="rechkgiftvouchers.php">ของสัมมนาคุณ</a></li>
                                <?php } ?>
                            <?php } ?>

                            <?php if( $_SESSION["User"]['type'] == "CallCenter" || $_SESSION["User"]['type'] == "SuperAdmin" || $_SESSION["User"]['UserCode'] == "ADB59009" || $_SESSION["User"]['UserCode'] == "ADB58003"){ ?>
                                <li><a href="moveleadqc.php">ย้าย Lead</a></li>
                                <li><a href="checkinwork.php">การเข้างาน</a></li>
                            <?php } ?>
                            <?php if($_SESSION["User"]['type'] == "SuperAdmin"){ ?>
                                <li><a href="personals.php">เพิ่มผู้ใช้</a></li>
                                <li><a href="#" class="btnaddNotice">เพิ่มประกาศ</a></li>
                            <?php } ?>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle icon-menu" data-toggle="dropdown"><i class="fas fa-chart-area"></i></a>
                        <ul class="dropdown-menu notifications">
                        	<?php if($_SESSION["User"]['type'] == "SuperAdmin" || $_SESSION["User"]['type'] == "Admin" || $_SESSION["User"]['type'] == "CallCenter" ){ ?>
                            <!-- <li><a href="report_qc.php" >Report QC</a></li> -->
                            <!-- <li><a href="report_sale.php" >Report Sale</a></li> -->
                            <li><a href="report_admin.php" >Report Admin</a></li>
                            <li><a href="cancelpolicy_report.php" >Report ยกเลิกกรมธรรม์</a></li>
                            <li><a href="report_inform.php" >รายงาน Admin ต่อวัน</a></li>
                          <?php } ?>
                            <li><a href="report_follow.php" >รายงาน Follow UP</a></li>
                            <li><a href="report_callcenter.php" >Report Callcenter</a></li>
                            <li><a href="report_fu.php" >รายงานการติดตาม</a></li>
                            <li><a href="report_salesum.php" >รายงานยอดขาย ADB</a></li>
                            <li><a href="report_renews.php" >รายงานยอดขาย Renew</a></li>
                            <li><a href="report_saleasiavib.php" >รายงานยอด ASIA VIB</a></li>
                            <li><a href="report_saleagent.php" >รายงานยอดขาย Agent</a></li>
                            <li><a href="report_search.php" >รายงานการค้นหา</a></li>
                            <?php if($_SESSION["User"]['type'] == "SuperAdmin"){ ?>
                                <li><a href="report_commis.php">Commission New</a></li>
                            <?php } ?>
                        </ul>
                    </li>
                    
                	<li><a href="#" onclick="logout()"><i class="fas fa-sign-out-alt"></i><span ></span></a></li>
                	<li><a href="index.php" ><i class="lnr lnr-user"></i> <span><?php echo $userProfile["personnel_firstname"]; ?></span></a></li>
                </ul>
            </div>
        </div>
    </nav>
