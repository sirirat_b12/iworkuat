<?php
session_start();
date_default_timezone_set("Asia/Bangkok");
header('Content-Type: text/html; charset=utf-8');

include "../inc_config.php";
include "inc_function.php"; 
include "inc_function_offer.php"; 

// echo "<pre>".print_r($_POST,1)."</pre>";
if($_POST["action"] == "getFollowup"){
	$getCPByid = getCPByid($_POST["cpid"]);
	$getFollowupBycpid = getFollowupBycpid($_POST["cpid"]);
	$getFollowupStatus = getFollowupStatus();
	$getFollowupType = getFollowupType();
	$getRejectReason = getRejectReason();
	 // echo "<pre>".print_r($getFollowupBycpid,1)."</pre>";  
	$html = "";
	$html .= '
		<div class="p15">
			<div class="row">
				<div class="col-md-8" style="background-color: #ffffed;">
					<p class="cff2da5 mt5"><b>รายละเอียดการติดตาม </b></p>
					<div class="p10" >
							<table  class="table" data-toggle="table" style="background-color: #ffffed;">
								<thead class="fs12 c000000">
									<tr class="cff2da5">
										<th class="t_c">แก้ไข</th>
										<th class="t_c">วันที่ติดตาม</th>
										<th class="t_c">รายละเอียด</th>
										<th class="t_c">ผลการติดตาม</th>
										<th class="t_c">นัดหมายครั้งถัดไป</th>
									</tr>
								</thead>
								<tbody class="fs12 c000000" id="tablelist_'.$_POST["cpid"].'">
					';
				foreach ($getFollowupBycpid as $key => $value) {
					$Remind_Date = $value["Remind_Date"] ? $value["Remind_Date"]->format("d/m/Y H:i") : "-";
					$html .= '
						<tr>
							<td class="t_c" >';
							if($value["Followup_Status_ID"] != '009' && $value["Followup_Status_ID"] != '010'){
								$html .= "<span class='c2457ff fs14 cursorPoin' onclick=editFollow('".$value["Followup_ID"]."','".$_POST["cpid"]."')><i class='fa fa-edit fs14'></i></span> ";
							}
					$html .= '
							</td>
							<td class="t_c">'.$value["Followup_DateTime"]->format("d/m/Y").'</td>
							<td>'.$value["Followup_Detail"].'</td>
							<td class="t_c">'.$value["Followup_Status_Desc"].'</td>
							<td class="t_c">'.$Remind_Date.'</td>
						</tr>
					';
				}
	$html .= '		</tbody>
							</table>
				</div>
			</div>
			<div class="col-md-4" style="background-color: #fff;" id="boxAdd'.$_POST["cpid"].'">
					<div class="p10">
						<p>เพิ่มการติดตาม</p>
						<div class="row mt5">
							<input type="hidden" value="'.$getCPByid["Customer_ID"].'" id="customer'.$_POST["cpid"].'">
							<input type="hidden" value="'.$getCPByid["Employee_ID"].'" id="emp'.$_POST["cpid"].'">
							<input type="hidden" value="pageAdd" id="pageaction'.$_POST["cpid"].'">
							<div class="col-md-6 mt5">
								<label for="FollowupDateTime'.$_POST["cpid"].'" class="cff2da5" style="font-size: 12px !important;">วัน-เวลาที่ติดตาม</label>
								<input type="date" name="FollowupDateTime'.$_POST["cpid"].'"  id="FollowupDateTime'.$_POST["cpid"].'" class="fs12 form-control" value="'.date("Y-m-d").'">
							</div>
							<div class="col-md-6 mt5">
								<label for="FollowupTypeID'.$_POST["cpid"].'" class="cff2da5" style="font-size: 12px !important;">ประเภทการติดตาม</label>
								<select name="FollowupTypeID'.$_POST["cpid"].'" id="FollowupTypeID'.$_POST["cpid"].'" class="fs12 form-control">
									<option value="">::: กรุณาเลือก :::</option>';
								foreach ($getFollowupType as $key => $value) {
									if($value["Followup_Type_ID"] == "003" ){
										$html .= '<option value="'.$value["Followup_Type_ID"].'" selected>'.$value["Followup_Type_Desc"].'</option>';
									}else{
										$html .= '<option value="'.$value["Followup_Type_ID"].'" >'.$value["Followup_Type_Desc"].'</option>';
									}
								}
							$html .='
								</select>
							</div>
						</div>
						<div class="row mt5">
							<div class="col-md-12 mt5">
								<label for="FollowupDetail'.$_POST["cpid"].'" class="cff2da5" style="font-size: 12px !important;">รายละเอียดการติดตาม</label>
								<textarea name="FollowupDetail'.$_POST["cpid"].'" id="FollowupDetail'.$_POST["cpid"].'" class="fs12 form-control"></textarea>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6 mt5">
								<label for="FollowupStatusID'.$_POST["cpid"].'" class="cff2da5" style="font-size: 12px !important;">สถานะการติดตาม</label>
								<select name="FollowupStatusID'.$_POST["cpid"].'" id="FollowupStatusID'.$_POST["cpid"].'" class="fs12 form-control" onchange="changeStatus(\''.$_POST["cpid"].'\',\'Add\')"> >
									<option value="">::: กรุณาเลือก :::</option>';
								foreach ($getFollowupStatus as $key => $value) {
									$html .= '<option value="'.$value["Followup_Status_ID"].'">'.$value["Followup_Status_Desc"].'</option>';
								}
							$html .='
								</select>
							</div>
						</div>
						<div class="row mt5 dn" id="dateRemind'.$_POST["cpid"].'">
							<div class="col-md-6 mt5">
								<label for="RemindDate'.$_POST["cpid"].'" class="cff2da5" style="font-size: 12px !important;">วัน-เวลาที่นัดหมาย</label>
								<input type="date" name="RemindDate'.$_POST["cpid"].'" id="RemindDate'.$_POST["cpid"].'" class="fs12 form-control"  >
							</div>
							<div class="col-md-6 mt5 ">
								<select name="Remindtime1_'.$_POST["cpid"].'" id="Remindtime1_'.$_POST["cpid"].'" class="fs12 form-control fl mr10" style="width: 40%;margin-top: 18px;">
									';
									for ($i=6; $i <= 20; $i++) { 
									$html .='<option value="'.$i.'">'.$i.'</option>';
								}
							$html .='
								</select>
								<select name="Remindtime2_'.$_POST["cpid"].'" id="Remindtime2_'.$_POST["cpid"].'" class="fs12 form-control ml5" style="width: 40%;margin-top: 18px;">
									<option value="15">15</option>
									<option value="30">30</option>
									<option value="45">45</option>
								</select>
							</div>
						</div>
						<div class="row mt5 dn" id="Reject'.$_POST["cpid"].'">
							<div class="col-md-12 mt5">
								<label for="RejectReason'.$_POST["cpid"].'" class="cff2da5" style="font-size: 12px !important;">เหตุผลที่ยกเลิก</label>
								<select name="RejectReason'.$_POST["cpid"].'" id="RejectReason'.$_POST["cpid"].'" class="fs12 form-control" onchange="changeStatus(this)">>
									<option value="">::: กรุณาเลือก :::</option>';
								foreach ($getRejectReason as $key => $value) {
									$html .= '<option value="'.$value["Reject_Reason_ID"].'">'.$value["Reject_Reason"].'</option>';
								}
							$html .='
								</select>
							</div>
						</div>
						<div class="t_c mt15">
							<span id="txtbtn'.$_POST["cpid"].'"></span>';
						$html .="	<button type='button' class='btn btn-success'  id='btnAddFollowup".$_POST["cpid"]."' onclick=addFollowup('".$_POST["cpid"]."')>บันทึก</button>";
						$html .=' </div>
					</div>
			</div>
			<div class="col-md-4 dn" style="background-color: #fff;" id="boxEdit'.$_POST["cpid"].'">
				<div class="p10">
						<p>แก้ไขข้อมูล</p>
						<input type="hidden" value="pageAdd" id="editpageaction'.$_POST["cpid"].'">
						<div class="row mt5">
							<div class="col-md-6 mt5">
								<label for="editFollowupDateTime'.$_POST["cpid"].'" class="cff2da5" style="font-size: 12px !important;">วัน-เวลาที่ติดตาม</label>
								<input type="date" name="editFollowupDateTime'.$_POST["cpid"].'"  id="editFollowupDateTime'.$_POST["cpid"].'" class="fs12 form-control" >
							</div>
							<div class="col-md-6 mt5">
								<label for="editFollowupTypeID'.$_POST["cpid"].'" class="cff2da5" style="font-size: 12px !important;">ประเภทการติดตาม</label>
								<select name="editFollowupTypeID'.$_POST["cpid"].'" id="editFollowupTypeID'.$_POST["cpid"].'" class="fs12 form-control">
									<option value="">::: กรุณาเลือก :::</option>';
								foreach ($getFollowupType as $key => $value) {
									if($value["Followup_Type_ID"] == "003" ){
										$html .= '<option value="'.$value["Followup_Type_ID"].'" selected>'.$value["Followup_Type_Desc"].'</option>';
									}else{
										$html .= '<option value="'.$value["Followup_Type_ID"].'" >'.$value["Followup_Type_Desc"].'</option>';
									}
								}
							$html .='
								</select>
							</div>
						</div>
						<div class="row mt5">
							<div class="col-md-12 mt5">
								<label for="editFollowupDetail'.$_POST["cpid"].'" class="cff2da5" style="font-size: 12px !important;">รายละเอียดการติดตาม</label>
								<textarea name="editFollowupDetail'.$_POST["cpid"].'" id="editFollowupDetail'.$_POST["cpid"].'" class="fs12 form-control"></textarea>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6 mt5">
								<label for="editFollowupStatusID'.$_POST["cpid"].'" class="cff2da5" style="font-size: 12px !important;">สถานะการติดตาม</label>
								<select name="editFollowupStatusID'.$_POST["cpid"].'" id="editFollowupStatusID'.$_POST["cpid"].'" class="fs12 form-control" onchange="changeStatus(\''.$_POST["cpid"].'\',\'Edit\')">>
									<option value="">::: กรุณาเลือก :::</option>';
								foreach ($getFollowupStatus as $key => $value) {
									if($value["Followup_Status_ID"] != "009" && $value["Followup_Status_ID"] != "010" ){
										$html .= '<option value="'.$value["Followup_Status_ID"].'">'.$value["Followup_Status_Desc"].'</option>';
									}
								}
							$html .='
								</select>
							</div>
						</div>
						<div class="row mt5 dn" id="editdateRemind'.$_POST["cpid"].'">
							<div class="col-md-6 mt5">
								<label for="editRemindDate'.$_POST["cpid"].'" class="cff2da5" style="font-size: 12px !important;">วัน-เวลาที่นัดหมาย</label>
								<input type="date" name="editRemindDate'.$_POST["cpid"].'" id="editRemindDate'.$_POST["cpid"].'" class="fs12 form-control"  >
							</div>
							<div class="col-md-6 mt5 ">
								<select name="editRemindtime1_'.$_POST["cpid"].'" id="editRemindtime1_'.$_POST["cpid"].'" class="fs12 form-control fl mr10" style="width: 40%;margin-top: 18px;">
									';
									for ($i=6; $i <= 20; $i++) { 
									$html .='<option value="'.str_pad($i,2,"0",STR_PAD_LEFT).'">'.$i.'</option>';
								}
							$html .='
								</select>
								<select name="editRemindtime2_'.$_POST["cpid"].'" id="editRemindtime2_'.$_POST["cpid"].'" class="fs12 form-control ml5" style="width: 40%;margin-top: 18px;">
									<option value="00">00</option>
									<option value="15">15</option>
									<option value="30">30</option>
									<option value="45">45</option>
								</select>
							</div>
						</div>
						<div class="row mt5 dn" id="editReject'.$_POST["cpid"].'">
							<div class="col-md-12 mt5">
								<label for="editRejectReason'.$_POST["cpid"].'" class="cff2da5" style="font-size: 12px !important;">เหตุผลที่ยกเลิก</label>
								<select name="editRejectReason'.$_POST["cpid"].'" id="editRejectReason'.$_POST["cpid"].'" class="fs12 form-control" onchange="changeStatus(this)">>
									<option value="">::: กรุณาเลือก :::</option>';
								foreach ($getRejectReason as $key => $value) {
									$html .= '<option value="'.$value["Reject_Reason_ID"].'">'.$value["Reject_Reason"].'</option>';
								}
							$html .='
								</select>
							</div>
						</div>
						<div class="t_c mt15">
						<input type="hidden" id="editFollowid'.$_POST["cpid"].'">';
						$html .="
							<button type='button' class='btn btn-info' data-cpid='".$_POST["cpid"]."'  onclick=closeFollowup('".$_POST["cpid"]."')>ยกเลิก</button>
							<button type='button' class='btn btn-success' data-cpid='".$_POST["cpid"]."' onclick=updateFollowlist('".$_POST["cpid"]."')>แก้ไข</button>
						";
						$html .='
						</div>
					</div>
			</div>
		</div>';
	echo $html;
	exit();

}else if($_POST["action"] == "addFollowup"){
	$Followid = getMaxIDFollowup();
	 // echo "<pre>".print_r($_POST,1)."</pre>";  exit();
	 $user =$_SESSION["User"]['UserCode']; 
	 $arr = array();
	 $arr["Followup_ID"] = $Followid;
	 $arr["Customer_ID"] = $_POST["customer"];
	 $arr["Compare_Policy_ID"] = $_POST["cpid"];
	 $arr["PO_ID"] = $arr["PO_ID"];	
	 $arr["Followup_DateTime"] = $_POST["FollowupDateTime"];
	 $arr["Followup_Type_ID"] = $_POST["FollowupTypeID"];
	 $arr["Followup_Topic"] = $_POST["Followup_Topic"];
	 $arr["Followup_Detail"] = $_POST["FollowupDetail"];
	 $arr["Followup_Status_ID"] = $_POST["FollowupStatusID"];
	 $arr["Followup_By"] = $_SESSION["User"]['UserCode'];
	 $arr["Remark"] = $arr["Remark"];
	 $arr["Active"] = "Y";
	 $arr["Create_Date"] = date('Y-m-d H:i:s');
	 $arr["Create_By"] = $_SESSION["User"]['UserCode'];	
	 $arr["Update_Date"] = date('Y-m-d H:i:s');
	 $arr["Update_By"] = $_SESSION["User"]['UserCode']; 
	 // echo "<pre>".print_r($arr,1)."</pre>";exit();
	 $sql = "INSERT INTO [dbo].[Followup] ([Followup_ID], [Customer_ID], [Compare_Policy_ID], [PO_ID], [Followup_DateTime], [Followup_Type_ID], [Followup_Topic], [Followup_Detail], [Followup_Status_ID], [Followup_By], [Remark], [Active], [Create_Date], [Create_By], [Update_Date], [Update_By]) 
	 VALUES ( '".$arr["Followup_ID"]."', '".$arr["Customer_ID"]."', '".$arr["Compare_Policy_ID"]."', '".$arr["PO_ID"]."', '".$arr["Followup_DateTime"]."', '".$arr["Followup_Type_ID"]."', '".$arr["Followup_Topic"]."', '".$arr["Followup_Detail"]."', '".$arr["Followup_Status_ID"]."', '".$arr["Followup_By"]."', '".$arr["Remark"]."', '".$arr["Active"]."', '".$arr["Create_Date"]."', '".$arr["Create_By"]."', '".$arr["Update_Date"]."', '".$arr["Update_By"]."' ) ";

	$stmt = sqlsrv_query( $connMS, $sql);
	// echo "<pre>".print_r($stmt,1)."</pre>"; 

	 // $stmt =true;
	if($stmt) {
		if($_POST["Remind"] && $_POST["FollowupStatusID"] != "010" && $_POST["FollowupStatusID"] != "009"){
			$Remindid = getMaxIDRemind();
			
			$sqlRemind = "INSERT INTO [dbo].[Remind] ( [Remind_ID], [Issue_Date], [Remind_Date], [Remind_Method_ID], [Customer_ID], [Compare_Policy_ID], [PO_ID], [Remind_Topic], [Remind_Detail], [Remind_Status_ID], [Remind_By], [Remark], [Active], [Create_Date], [Create_By], [Update_Date], [Update_By])
     VALUES ( '".$Remindid."', '".$_POST["FollowupDateTime"]."', '".$_POST["Remind"]."', '001', '".$_POST["customer"]."', '".$_POST["cpid"]."', '', '', '', '001', '".$user."', '".$Followid."', '', '".date('Y-m-d H:i:s')."', '".$user."', '".date('Y-m-d H:i:s')."', '".$user."' ) ";
     	
     	sqlsrv_query( $connMS, $sqlRemind);

     	$sqlUpdate = "UPDATE [dbo].[Compare_Policy] SET [Compare_Status_ID] = 'IPC' ,[Reject_Reason_ID] = '', [Update_Date] = '".date('Y-m-d H:i:s')."', [Update_By] = '".$user."' WHERE CP_ID = '".$_POST["cpid"]."' ";
			sqlsrv_query( $connMS, $sqlUpdate);


		 	echo $ntml =  htmlTabelFollow($_POST["cpid"], $_POST["pageaction"]);
		
		}else if($_POST["FollowupStatusID"] == "009"){
			$sqlUpdate = "UPDATE [dbo].[Compare_Policy] SET [Compare_Status_ID] = 'REJ' ,[Reject_Reason_ID] = '".$_POST["RejectReason"]."', [Update_Date] = '".date('Y-m-d H:i:s')."', [Update_By] = '".$user."' WHERE CP_ID = '".$_POST["cpid"]."' ";
			sqlsrv_query( $connMS, $sqlUpdate);
			echo $ntml =  htmlTabelFollow($_POST["cpid"], $_POST["pageaction"]);
		
		}else if($_POST["FollowupStatusID"] == "010"){
			$sqlUpdate = "UPDATE [dbo].[Compare_Policy] SET [Compare_Status_ID] = 'POI' , [Reject_Reason_ID] = '011', [Update_Date] = '".date('Y-m-d H:i:s')."', [Update_By] = '".$user."' WHERE CP_ID = '".$_POST["cpid"]."' ";
			sqlsrv_query( $connMS, $sqlUpdate);
			echo $ntml = htmlTabelFollow($_POST["cpid"], $_POST["pageaction"]);

		}else{
			echo "0";
		}
	}else{
		echo "0";
	}

	// sqlsrv_close($conn);
}else if($_POST["action"] == "geteditFollowup"){
	$flid = $_POST["flid"];
	$sql = "SELECT  Followup.Followup_ID, Followup.Customer_ID, Followup.Compare_Policy_ID, Followup.PO_ID, Followup.Followup_DateTime, Followup.Followup_Type_ID, Followup.Followup_Topic, Followup.Followup_Detail, Followup.Followup_Status_ID, Followup.Followup_By, Followup.Remark, Remind.Remind_Date, Followup_Status.Followup_Status_Desc
	From [dbo].Followup LEFT JOIN [dbo].[Remind] ON Followup.Followup_ID = Remind.Remark
	LEFT JOIN [dbo].[Followup_Status] ON Followup.Followup_Status_ID = Followup_Status.Followup_Status_ID
	WHERE Followup.Followup_ID = '".$flid."'";

	$stmt = sqlsrv_query( $connMS, $sql );
  if(sqlsrv_has_rows($stmt)) {
    $row = sqlsrv_fetch_array($stmt);
   // echo "<pre>".print_r($row,1)."</pre>";
    $row["FollowupDate"] = $row["Followup_DateTime"] ? $row["Followup_DateTime"]->format("Y-m-d") : "" ;
    $row["RemindDate"] = $row["Remind_Date"] ? $row["Remind_Date"]->format("Y-m-d") : "" ;
    $row["Remindtime1"] = $row["Remind_Date"] ? $row["Remind_Date"]->format("H") : "" ;
    $row["Remindtime2"] = $row["Remind_Date"] ? $row["Remind_Date"]->format("i") : "" ;
    if($row["Followup_Status_ID"] == '009'){
    	$getCP = getCPByid($row["Compare_Policy_ID"]);
    	$row["RejectReason"] = $getCP["Reject_Reason_ID"];
    }
    echo json_encode($row);
    exit();
  }  

}else if($_POST["action"] == "updateFollowuplist"){
	// echo "<pre>".print_r($_POST,1)."</pre>";
	$user = $_SESSION["User"]['UserCode']; 
	
	$sqlUpdate = "UPDATE [dbo].[Followup]  SET [Followup_DateTime] = '".$_POST["FollowupDateTime"]."', [Followup_Type_ID] = '".$_POST["FollowupTypeID"]."', [Followup_Detail] = '".$_POST["FollowupDetail"]."', [Followup_Status_ID] = '".$_POST["FollowupStatusID"]."', [Update_Date] = '".date('Y-m-d H:i:s')."', [Update_By] = '".$user."' 
		WHERE Followup_ID = '".$_POST["Followupid"]."' ";
	$rsFollow = sqlsrv_query( $connMS, $sqlUpdate);
	

	$sqlRemind = "UPDATE [dbo].[Remind]  SET [Issue_Date] = '".$_POST["FollowupDateTime"]."', [Remind_Date] = '".$_POST["Remind"]."', [Update_Date] = '".date('Y-m-d H:i:s')."', [Update_By] = '".$user."' WHERE Remark = '".$_POST["Followupid"]."' ";
	$rsRemind = sqlsrv_query( $connMS, $sqlRemind);

	echo $html = htmlTabelFollow($_POST["cpid"], $_POST["pageaction"]);
	 exit();

}else if($_POST["action"] == "CustomerFilter"){
	$val = $_POST["val"];
	// echo "<pre>".print_r($_POST,1)."</pre>";
	$sql = "SELECT * From [dbo].Customer 
	WHERE Customer_ID LIKE '%".$val."%' OR Tel_No LIKE '%".$val."%' OR Mobile_No LIKE '%".$val."%' OR Customer_FName LIKE '%".$val."%' OR Customer_LName LIKE '%".$val."%'  ";
	$stmt = sqlsrv_query( $connMS, $sql );
  if(sqlsrv_has_rows($stmt)) {
      while( $row = sqlsrv_fetch_array($stmt) ) { 
        $resultarray[] = $row;
      }
    echo  json_encode($resultarray);
    exit();
  }

}else if($_POST["action"] == "CustomerRegistration"){

	$sql = "SELECT User_ID From [dbo].Customer_Registration WHERE Customer_ID = '".$_POST["cusid"]."'";
  $stmt = sqlsrv_query( $connMS, $sql );
  if(sqlsrv_has_rows($stmt)) {
    $row = sqlsrv_fetch_array($stmt);
    echo $row["User_ID"];
    exit();
  }  

}else if($_POST["action"] == "getDistrict"){
  $getDistrict = getDistrictByProvince($_POST["ProvinceID"]);
  echo json_encode($getDistrict);
  exit();

}else if($_POST["action"] == "getSubdistrict"){
	$getSubdistrict = getSubdistrictByDistrict($_POST["District_ID"]);
  echo json_encode($getSubdistrict);
  exit();

}else if($_POST["action"] == "getPostCode"){
	echo $getPostCode = getPost($_POST["Subdistrict_ID"]);
  exit();

}else if($_POST["action"] == "addCustomer"){
	// echo "<pre>".print_r($_POST,1)."</pre>";
  $data = array();
	foreach ($_POST["frm"] as $key => $value) {
		$data[$value["name"]] = $value["value"];
	}
	$date = date('Y-m-d H:i:s');
	$UserCode = $_SESSION["User"]['UserCode'];

	$Customer_ID = getMaxIDRCustomer($data["Customer_Type"]);
	$sqlCus = "INSERT INTO [dbo].[Customer]
           ([Customer_ID], [Customer_Type], [Customer_Title], [Customer_FName], [Customer_LName], [Customer_FName_EN], [Customer_LName_EN], [Business_Type_ID], [Customer_Group_ID], [Customer_Folder], [Contact_Person], [Addr1], [Addr2], [Subdistrict_ID], [District_ID], [Province_ID], [Post_Code], [Tel_No], [Fax], [Mobile_No], [EMail], [Mobile_I], [Mobile_II], [Land_Line], [Line_ID], [Remark], [Active], [Create_Date], [Create_By], [Update_Date], [Update_By]) 
           VALUES ('".$Customer_ID."', '".$data["Customer_Type"]."', '".$data["Customer_Title"]."', '".$data["Customer_FName"]."', '".$data["Customer_LName"]."',
           '', '', '', 'G00000', '".$data["CustomerFolder"]."','".$data["ContactPerson"]."', '".$data["Addr1"]."', '', '".$data["Subdistrict_ID"]."', '".$data["District_ID"]."', '".$data["Province_ID"]."', '".$data["Post_Code"]."', '".$data["Tel_No"]."', '".$data["Fax"]."', '".$data["Mobile_No"]."', '".$data["EMail"]."', '".$data["Mobile_I"]."',  '".$data["Mobile_II"]."','".$data["Land_Line"]."' ,  '".$data["Line_ID"]."',  '',  'Y',  '".$date."',  '".$UserCode."',  '".$date."', '".$UserCode."' ) ";
  sqlsrv_query( $connMS, $sqlCus);
  
  $sqlCustomerMail = "INSERT INTO [dbo].[Customer_Mail_Address]
           ([Customer_ID], [Receiver_Name], [Addr1], [Addr2], [Subdistrict_ID], [District_ID], [Province_ID], [Post_Code], [Remark], [Active], [Create_Date], [Create_By], [Update_Date], [Update_By]) VALUES ('".$Customer_ID."', '".$data["Receiver_Name"]."', '".$data["Addr1_2"]."', '', '".$data["Subdistrict_ID_2"]."', '".$data["District_ID_2"]."', '".$data["ProvinceID_2"]."', '".$data["Post_Code_2"]."', '', 'Y', '".$date."',  '".$UserCode."',  '".$date."', '".$UserCode."')" ;
  sqlsrv_query( $connMS, $sqlCustomerMail);
 
  $regisRemake = $data["Customer_FName"]." ".$data["Customer_LName"]."  ::  ".$data["Tel_No"].", Assign To ".$data["UserID"];
  $sqlCusRegis = "INSERT INTO [dbo].[Customer_Registration]
           ([Customer_ID], [Registration_Date], [User_ID], [Agent_ID], [Referral_Type_ID], [Referral_Detail], [Discount], [Credit_Term], [Campaign_ID], [Remark], [Active], [Create_Date], [Create_By], [Update_Date], [Update_By]) VALUES ('".$Customer_ID."', '".$date."', '".$data["UserID"]."', '00001', '".$data["ReferralTypeID"]."', '".$data["ReferralDetail"]."', '', '', '', '".$regisRemake."', 'Y', '".$date."',  '".$UserCode."',  '".$date."', '".$UserCode."')";
  sqlsrv_query( $connMS, $sqlCusRegis);
  
  $rs = array();
  $rs["UserCode"] = $data["UserID"];
  $rs["Customer_ID"] = $Customer_ID;
  $rs["Customer_FName"] = $data["Customer_FName"];
  $rs["Customer_LName"] = $data["Customer_LName"];
	echo json_encode($rs);
  exit();

}else if($_POST["action"] == "addCP"){
	// echo "<pre>".print_r($_POST,1)."</pre>";
	$CP_ID = getComparePolicyID();
	$date = date('Y-m-d H:i:s');
	$UserCode = $_SESSION["User"]['UserCode'];
	$sql1 = "INSERT INTO [dbo].[Compare_Policy] ([CP_ID], [CP_Date], [CP_PO_ID], [Customer_ID], [Agent_ID], [Employee_ID], [Insurance_Group_ID], [Coverage_Start_Date], [Coverage_End_Date], [Compare_Status_ID], [Reject_Reason_ID], [Reject_Reason_Desc], [Remark], [Active], [Create_Date], [Create_By], [Update_Date], [Update_By])
     VALUES ('".$CP_ID."', '".$_POST["CPDate"]."', '".$_POST["POID"]."', '".$_POST["CustomerID"]."', '00001', '".$_POST["EmployeeID"]."', '".$_POST["InsuranceGroupID"]."', '".$_POST["CoverageStartDate"]."', '".$_POST["CoverageEndDate"]."', 'NEW', '', '', '',  'Y', '".$date."',  '".$UserCode."',  '".$date."', '".$UserCode."')";

  $stmt1 = sqlsrv_query( $connMS, $sql1);

  $sql2 = "INSERT INTO [dbo].[Compare_Car_Detail] ([CP_ID], [Plate_No], [Plate_Province], [Make_ID], [Model], [Sub_Model], [Body_Type], [Car_Code], [Frame_No], [Accessory], [Seat_CC_Weight], [Repair_Type], [Driver_Name], [Driver_DOB], [Driver_License_No], [Co_Driver_Name], [Co_Driver_DOB], [Co_Driver_License_No], [Receiver], [Sender], [Remark], [Active], [Create_Date], [Create_By], [Update_Date], [Update_By])
     VALUES ('".$CP_ID."', '', '', '', '', '0', '', '', '', '', '0', '', '', '1900-01-01 00:00:00.000', '', '', '1900-01-01 00:00:00.000', '',  '', '', '', 'Y', '".$date."',  '".$UserCode."',  '".$date."', '".$UserCode."')";

  $stmt2 = sqlsrv_query( $connMS, $sql2);

// echo "<pre>".print_r($_POST,1)."</pre>";
// 	exit();
  if($stmt1){
	  echo "<script>alert('ทำรายการสำเร็จ');</script>";
		echo '<META http-equiv="refresh" content="0;URL=../offeredits.php?cpid='.$CP_ID.'">';
		exit();
	}else{
		echo "<script>alert('ไม่สามารถทำรายการได้ กรุณาลองอีกครั้ง');</script>";
		echo '<META http-equiv="refresh" content="0;URL=../offeradd.php">';
		exit();
	}

}else if($_POST["action"] == "changGetModel"){
  $getCarModelAll = getCarModelAll($_POST["Model"]);
  echo json_encode($getCarModelAll);
  exit();

}else if($_POST["action"] == "updateCPDetail"){
	$_POST["Model"]  = $_POST["Model_ID"] ? $_POST["Model_ID"] : $_POST["Model_ID2"];

	$user =$_SESSION["User"]['UserCode']; 
	$sqlCP = "UPDATE [dbo].[Compare_Policy] SET [CP_Date] = '".$_POST["CP_Date"]."', [Employee_ID] = '".$_POST["Employee_ID"]."', [Insurance_Group_ID] = '".$_POST["Insurance_Group_ID"]."' , [Coverage_Start_Date] = '".$_POST["Coverage_Start_Date"]."' , [Coverage_End_Date] = '".$_POST["Coverage_End_Date"]."' , [Compare_Status_ID] = '".$_POST["Compare_Status_ID"]."' , [Reject_Reason_ID] = '".$_POST["Reject_Reason_ID"]."' , [Reject_Reason_Desc] = '".$_POST["Reject_Reason_Desc"]."' , [Remark] = '".$_POST["Remark"]."' , [Active] = '".$_POST["Active"]."' , [Update_Date] = '".date('Y-m-d H:i:s')."', [Update_By] = '".$user."' WHERE CP_ID = '".$_POST["CP_ID"]."' ";
	$stmtCP = sqlsrv_query( $connMS, $sqlCP);
	
	$sqlCar = "UPDATE [dbo].[Compare_Car_Detail] SET [Plate_No] = '".$_POST["Plate_No"]."', [Plate_Province] = '".$_POST["Plate_Province"]."', [Make_ID] = '".$_POST["Make_ID"]."', [Model] = '".$_POST["Model"]."', [Sub_Model] = '".$_POST["Sub_Model"]."', [Body_Type] = '".$_POST["Body_Type"]."', [Car_Code] = '".$_POST["CarCode"]."', [Frame_No] = '".$_POST["Frame_No"]."', [Accessory] = '".$_POST["Accessory"]."', [Seat_CC_Weight] = '".$_POST["Seat_CC_Weight"]."', [Remark] = '".$_POST["Remark"]."' , [Active] = '".$_POST["Active"]."', [Update_Date] = '".date('Y-m-d H:i:s')."', [Update_By] = '".$user."' WHERE CP_ID = '".$_POST["CP_ID"]."' ";
	$stmtCar = sqlsrv_query( $connMS, $sqlCar);
	
	// echo "<pre>".print_r($_POST,1)."</pre>";
	// exit();

	if($stmtCP || $stmtCar){
	  echo "<script>alert('แก้ไขรายการสำเร็จ');</script>";
		echo '<META http-equiv="refresh" content="0;URL=../offeredits.php?cpid='.$_POST["CP_ID"].'">';
		exit();
	}else{
		echo "<script>alert('ไม่สามารถทำรายการได้ กรุณาลองอีกครั้ง');</script>";
		echo '<META http-equiv="refresh" content="0;URL=../offeredits.php?cpid='.$_POST["CP_ID"].'">';
		exit();
	}
	exit();


}else if($_POST["action"] == "delCoverge"){
	 	$sqlCompareOption = "DELETE FROM [dbo].[Compare_Option] WHERE CP_ID = '".$_POST["cpcode"]."' AND CPO_ID = '".$_POST["num"]."'";
	 	$sqlCoverageItem = "DELETE FROM [dbo].[Compare_Policy_Coverage_Item] WHERE CP_ID = '".$_POST["cpcode"]."' AND CPO_ID = '".$_POST["num"]."'";
	 	$stmtCompareOption = sqlsrv_query( $connMS, $sqlCompareOption);
	 	$stmtCoverageItem = sqlsrv_query( $connMS, $sqlCoverageItem);
	 	if($stmtCompareOption || $stmtCoverageItem){
	 		echo "1";
	 	}else{
	 		echo "0";
	 	}
	 exit();

}else if($_POST["action"] == "getRowOptions"){
	$sql = "SELECT * FROM [dbo].[Compare_Option] WHERE CP_ID = '".$_POST["cpid"]."' AND CPO_ID = '".$_POST["optionid"]."' ";
  $stmt = sqlsrv_query( $connMS, $sql );
  if(sqlsrv_has_rows($stmt)) {
  	$row = sqlsrv_fetch_array($stmt);
  	echo  json_encode($row);
    exit();
  }
	exit();


}else if($_POST["action"] == "editConverage"){
	$date = date('Y-m-d H:i:s');
	$UserCode = $_SESSION["User"]['UserCode'];
	$numDown = ($_POST["optionDiscount"] == 10 && $_POST["optionDiscount"] = 1) ? 1 : $_POST["optionDiscount"];
	// echo "<pre>".print_r($_POST,1)."</pre>";
	$sql = "UPDATE [dbo].[Compare_Option] SET [Compulsory] = '".$_POST["editCompulsory"]."', [Discount] = '".$_POST["editDiscount"]."', [Total_Premium] = '".$_POST["editTotal_Premium"]."', [Premium_After_Disc] = '".$_POST["editPremium_After_Disc"]."', [Active] = '".$numDown."', [Update_Date] = '".$date."', [Update_By] = '".$UserCode."' WHERE CP_ID = '".$_POST["editcpid"]."' AND  CPO_ID = '".$_POST["editoptionid"]."' ";
	$stmt = sqlsrv_query( $connMS, $sql);
  if($stmt){
	  echo "<script>alert('แก้ไขข้อเสนอ ".$_POST["editcpid"]." รายการที่ ".$_POST["editoptionid"]." สำเร็จ');</script>";
		echo '<META http-equiv="refresh" content="0;URL=../offeredits.php?cpid='.$_POST["editcpid"].'">';
		exit();
	}else{
		echo "<script>alert('ไม่สามารถทำรายการได้ กรุณาลองอีกครั้ง');</script>";
		echo '<META http-equiv="refresh" content="0;URL=../offeredits.php?cpid='.$_POST["editcpid"].'">';
		exit();
	}

	exit();
}

