<?php
session_start();
date_default_timezone_set("Asia/Bangkok");
header('Content-Type: text/html; charset=utf-8');

include "../inc_config.php";
require '../phpmailer6/src/PHPMailer.php';
require '../phpmailer6/src/SMTP.php';
require '../phpmailer6/src/Exception.php';
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;
include "../include/inc_function.php"; 
include "../include/inc_function_chk.php"; 
// echo "<pre>".print_r($_POST,1)."</pre>"; exit();
if($_POST["actions"] == "login"){
	$user = addslashes(trim($_POST['personnel_code']));
	$pass= addslashes(trim($_POST['password']));
	$rs = $conn2->GetRow("SELECT * FROM ck_personnel WHERE personnel_code = '".$user."' AND personnel_password = '".$pass."' AND personnel_status = '1' ");
// echo "<pre>".print_r($rs,1)."</pre>";
	if($rs){
		$_SESSION["User"]['UserID'] = $rs["personnel_id"];
		$_SESSION["User"]['UserCode'] = $rs["personnel_code"];
		$_SESSION["User"]['firstname'] = $rs["personnel_firstname"];
		$_SESSION["User"]['lastname'] = $rs["personnel_lastname"];
		$_SESSION["User"]['email'] = $rs["personnel_email"];
		$_SESSION["User"]['phone'] = $rs["personnel_phone"];
		$_SESSION["User"]['type'] = $rs["personnel_type"];
		$_SESSION["User"]['team'] = $rs["personnel_supervisor"];

		$data = array();
		$data["ck_personnel_id"] = $rs["personnel_id"];
		$data["prsonnel_code"] = $rs["personnel_code"];
		$data["name"] = $rs["personnel_firstname"]." ".$rs["personnel_lastname"];
		$data["types"] = $rs["personnel_type"];
		$data["status"] = "IN";
		$data["ip"] = isset($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR'];
		$data["times"] 	= date("Y-m-d H:i:s");
		insertLogin($data); 
		echo "<script>alert('ยินดีต้อนรับ คุณ ".$rs["personnel_firstname"]." ".$rs["personnel_lastname"]." เข้าสู่ระบบ');</script>";
		if($_SESSION["User"]['type'] == "QualityControl"){
			echo '<META http-equiv="refresh" content="0;URL=../sendorders.php">';
		}else if($_SESSION["User"]['type'] == "Admin"){
			echo '<META http-equiv="refresh" content="0;URL=../ordersadmins.php">';
		}else{
			echo '<META http-equiv="refresh" content="0;URL=../index.php">';
		}
		exit();
	}else{
		echo "<script>alert('ไม่สามารถเข้าสู่ระบบได้ กรุณาลองใหม่อีกครั้ง!!!');</script>";
		echo '<META http-equiv="refresh" content="0;URL=../login.php">';
		exit();
	}

}else if($_POST["action"] == "logout"){
// $data = array();
// $data["ck_personnel_id"] = $_SESSION["User"]['UserID'];
// $data["prsonnel_code"] = $_SESSION["User"]['UserCode'];
// $data["name"] = $_SESSION["User"]['firstname']." ".$_SESSION["User"]['lastname'];
// $data["types"] = $_SESSION["User"]['type'];
// $data["status"] = "OUT";
// $data["ip"] = isset($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR'];
// $data["times"] 	= date("Y-m-d H:i:s");
// insertLogin($data); 
	session_destroy();
	echo "1";
	exit();

}else if($_POST["action"] == "generation"){
	$brand = $_POST["brand"];
	$car_model = $conn2->GetAssoc("SELECT * FROM ck_redbook WHERE redbook_tks_make = '".$brand."' GROUP BY redbook_tks_model ORDER BY redbook_tks_model ASC ");
	echo json_encode($car_model);
	exit();

}else if($_POST["action"] == "sqlCC") {
	$brand = $_POST["brand"];
	$model = $_POST["model"];
	$year = $_POST["year"];
	$sql = "SELECT * FROM ck_redbook WHERE redbook_tks_make='".$brand."' AND redbook_tks_model='".$model."' AND redbook_tks_yeargroup='".$year."' GROUP BY redbook_tks_cc ORDER BY redbook_tks_cc ASC";
	$act = $conn2->GetAssoc($sql);
	echo json_encode($act);
	exit();

}else if ($_POST["action"] == "getInserance") {
	$rs_repair = $_REQUEST['repair'];
	$whereCC  =  str_replace( 'cc', '', $_POST["cc"]);
	$newformatdate = date("d-m-Y", strtotime($_REQUEST['year']));
	$newdate = explode("-",$newformatdate);
	$minyear = (date("Y")-$_REQUEST['year'])+1;

	$insurance = "ck_insurance";
	$insurancecost = "ck_insurance_cost";
	$insuranceprotect = "ck_insurance_protect";
	$sql_redbook = "SELECT * FROM ck_redbook WHERE redbook_tks_make = '".$_REQUEST['brand']."' AND redbook_tks_model= '".$_REQUEST['generation']."' AND redbook_tks_yeargroup = '".$_REQUEST['year']."' ";
	if($_REQUEST['cc']){
		$sql_redbook .= "AND redbook_tks_cc = '".$_REQUEST['cc']."' ";
	}
	$ck_redbook = $conn2->GetRow($sql_redbook);
	$balanceCost = $ck_redbook["redbook_tks_goodretail"] ? $ck_redbook["redbook_tks_goodretail"] : 0;

	$sql = 'SELECT ';
	$sql .= $insurance.'.*,';
	$sql .= $insurancecost.'.*,';
	$sql .= 'ck_insurer_roadside.contact, ck_insurer_roadside.description, ';
	$sql .= 'conditionpay.ConditionPay_id AS condiPayID , conditionpay.Status AS condiPayStatus';
// $sql .= ' FROM '.$insurance.','.$insurancecost." "; 
	$sql .= ' FROM ck_insurance LEFT JOIN ck_insurance_cost ON ck_insurance.insurance_id = ck_insurance_cost.insurance_id
	LEFT JOIN ck_insurer_roadside ON ck_insurance.insurance_insurer = ck_insurer_roadside.insurance_Name AND ck_insurance.insurance_type = ck_insurer_roadside.insurance_type
	LEFT JOIN conditionpay ON ck_insurance.insurance_insurer = conditionpay.insurance_Name ';


	$sql .= ' WHERE '.$insurancecost.'.inscost_brand = "'.$_REQUEST['brand'].'" and ';
	if($_REQUEST['user'] != "gobear"){
		if($_REQUEST['class'] != "all"){
			$sql .= $insurance.'.insurance_type="'.$_REQUEST['class'].'" and ';
		}
	}


	$sql .= $insurancecost.'.inscost_gen = "'.$_REQUEST['generation'].'" and ';
  //$sql .= $insurancecost.'.inscost_minyear <= "'.$minyear.'" and '.$insurancecost.'.inscost_maxyear >= "'.$minyear.'" and ';
	if($_REQUEST['insurers'] != '0'){
		$sql .= $insurance.'.insurance_insurer="'.$_REQUEST['insurers'].'" and ';
	}

	if($rs_repair){
		$sql .= $insurance.'.insurance_repair="'.$rs_repair.'" and ';
	}

	if($whereCC > 2000 || $whereCC > "2000"){
		$sql .= $insurance.'.insurance_cc_types IN (0, 2) and ';
	}else{
		$sql .= $insurance.'.insurance_cc_types IN (0, 1) and ';
	}

	if($_REQUEST['deduct'] == 0){
		$sql .= $insurance.'.insurance_deductible = 0 AND ';
	}else{
		$sql .= $insurance.'.insurance_deductible != 0 AND ';
	}

	$sql .= $insurance.'.insurance_end >= "'.date("Y-m-d").'" AND';
	$sql .= '('.$minyear.' BETWEEN inscost_minyear AND inscost_maxyear ) AND ';
	$sql .= $insurance.'.insurance_id = '.$insurancecost.'.insurance_id AND '.$insurance.'.status_internal = "1"';
	// $sql .= 'AND CASE WHEN (ck_insurance.insurance_type = "1" AND ck_insurance_cost.inscost_minamount != 0 AND ck_insurance_cost.inscost_maxamount != 0 ) 
	// THEN "'.$balanceCost.'" BETWEEN ck_insurance_cost.inscost_minamount AND ck_insurance_cost.inscost_maxamount
	// WHEN (ck_insurance.insurance_type = "1" AND ck_insurance_cost.inscost_minamount != "0" AND ck_insurance_cost.inscost_maxamount != "0")
	// THEN 1=1
	// ELSE ck_insurance_cost.inscost_minamount >= 0 END';
	$sql .= " ORDER BY ck_insurance.insurance_insurer = 'เอเชียประกันภัย 1950' DESC, ".$insurancecost.".inscost_taxamount ASC"; 

	$ins = $conn2->GetAll($sql);
// echo $sql;
// echo "<pre>".print_r($ins,1)."</pre>"; 
	if($ins){
		$resultarray = array();
		$i = 1;
		foreach ($ins as $key => $valIns){ 
			$repair = ($valIns['insurance_repair'] == "hall") ? "ซ่อมห้าง" : "ซ่อมอู่" ;
			$insurer_code = $conn2->GetRow("SELECT * FROM ck_insurer WHERE insurer_name = '".$valIns['insurance_insurer']."' ");

			if($valIns['insurance_license_types'] == "1"){
				$license_types = "ทั้งประเทศ";
			}else if($valIns['insurance_license_types'] == "2"){
				$license_types = "ต่างจังหวัด";
			}else{
				$license_types = "กทม. และปริมฌฑล";
			}

			$goodretail = $ck_redbook["redbook_tks_goodretail"];
			$minamount = ($valIns["inscost_minamount"]!=0) ? $valIns["inscost_minamount"] : $goodretail;
			$maxamount = ($valIns["inscost_maxamount"]!=0) ? $valIns["inscost_maxamount"] : $goodretail;


			//if($goodretail >= $minamount && $goodretail <= $maxamount || $goodretail == 0){
				$tax = 0;
				$pre = 0;
				$pay3 = 0; 
				$pay6 = 0;
				$pay10 = 0;
				$pay12 = 0;
				$discount = 0.0;
				$premium = 0.0;
				$discountSum = 0.0;
				$down = 0.0;
				$getcc = 0;
				$discountSumCredit = 0;
				$premiumCredit = 0;
				$tax = $valIns["inscost_taxamount"];
				$pre = $valIns["inscost_premamount"];
				if($valIns["salecost"]){
					$discount = ($valIns["inscost_premamount"]  * $valIns["percen_sale"] ) / 100;
					$premium = $valIns["inscost_taxamount"] - $valIns["salecost"];
					$discountSum = $valIns["salecost"];
				}else if($valIns["percen_sale"] > 1){
					$discount = ($valIns["inscost_premamount"]  * $valIns["percen_sale"] ) / 100;
					$premium = ceil(($valIns["inscost_taxamount"] - $discount) / 100) * 100;
					$discountSum = $valIns["inscost_taxamount"] - $premium;

					// กรณีตัดบัตร
					// if($valIns['insurance_insurer'] == "วิริยะประกันภัย"){
					if($valIns['condiPayID']){
						// $discountPercen = 3 ;
						$discountCredit = 0 ;
						$premiumCredit = $valIns["inscost_taxamount"] - $discountCredit;
						$discountSumCredit = $valIns["inscost_taxamount"] - $premiumCredit;
					}else{
						$discountPercen = 2 ;
						$discountCredit = ($valIns["inscost_premamount"]  * ($valIns["percen_sale"] - $discountPercen) ) / 100;
						$premiumCredit = ceil(($valIns["inscost_taxamount"] - $discountCredit) / 100) * 100 ;
						$discountSumCredit = $valIns["inscost_taxamount"] - $premiumCredit;
					}
				}else{
					$premium = $valIns["inscost_taxamount"];
					$premiumCredit = $premium;
				}
				$pay2 = ($valIns['bank_sale_two']) ? calDiscount($tax, $pre, $valIns['bank_sale_two']) : $tax ;
				$pay3 = ($valIns['bank_sale_three']) ? calDiscount($tax, $pre, $valIns['bank_sale_three']) : $tax ; 
				$pay6 = ($valIns['bank_sale_six']) ? calDiscount($tax, $pre, $valIns['bank_sale_six']) : $tax ; 
				$pay10 = ($valIns['bank_sale_ten']) ? calDiscount($tax, $pre, $valIns['bank_sale_ten']) : $tax ;
				$pay12 = ($valIns['bank_sale_twelve']) ? calDiscount($tax, $pre, $valIns['bank_sale_twelve']) : $valIns['bank_sale_twelve'];  

				$down = ($valIns["installment"]) ? ceil($valIns["inscost_taxamount"]/$valIns["installment"]) : 0 ;
				$down3 = ceil($pay3/3);
				$down6 = ceil($pay6/6);

				$num = count($_SESSION['inslist']['costid']);
				if(!$num){
					$_SESSION['inslist']['costid'] =  array();
				}
				$key = array_search($valIns["inscost_id"] , $_SESSION['inslist']['costid']);
				$ck = ((string)$key == "") ? '' : 'checked'; 

				$details = 
				'<tr id=ins'.$valIns["inscost_id"].'>
				<td style="width: 1%;text-align: center;"><b>'.$i++.'.</b><div><input type="checkbox" name="chkIns[]" id="chkIns'.$valIns["inscost_id"].'" class="chkIns" onclick="chkIns('.$valIns['insurance_id'].','.$valIns['inscost_id'].');" value="'.$valIns['insurance_id'].'" data-id="'.$valIns['inscost_id'].'" '.$ck.' ></div></td>
				<td style="width: 30%;"">
				<div><b>ประกันชั้น :</b> '.$valIns['insurance_type'].'</div>
				<div><b>บริษัท :</b> <span class="c9c00c8">'.$insurer_code["insurer_code"].' '.$valIns['insurance_insurer'].'</span></div>
				<div><b>รหัสแพ็คเกจ :</b> '.$valIns['insurance_Number'].'</div>
				<div><b>รหัสกลุ่มรถ :</b> '.$valIns['insurance_code'].'</div>
				<div><b>แพ็คเกจ :</b> <span class="c9c00c8">'.$valIns['insurance_Name'].'</span></div>
				<div><b>การซ่อม :</b> <span class="c9c00c8">'.$repair.'</span></div>
				<div><b>อายุรถ :</b> '.$valIns['inscost_minyear']."-".$valIns['inscost_maxyear'].' ปี</div>
				<div><b>ทุนประกันกลาง : <span class="cff2da5">'.number_format($ck_redbook["redbook_tks_goodretail"]).' </span></b>บาท</div>
				<div><b>ทุนประกัน : <span class="cff8000">'.number_format($valIns['inscost_minamount'])." - ".number_format($valIns['inscost_maxamount']).'</span> </b>บาท </div>
				<div><b>ค่าเสียหายส่วนแรก : <span class="cff2da5">'.$valIns['insurance_deductible'].'</span> </b>บาท </div>
				<div><b>หมดแพ็คเกจ :</b> <span class="c2457ff">'.$valIns['insurance_end'].'</span></div>';
				if(trim($valIns['insurance_desc'])) {
					$details .=
					'<div><b><span onclick="noteShow('.$valIns["inscost_id"].')" class="cursorPoin c00ac0a"><i class="fas fa-comment-alt"></i> หมายเหตุ</span></b></div>
					<div class="dn" id="desc_'.$valIns["inscost_id"].'">'.nl2br($valIns['insurance_desc']).'</div>';
				}
				if($valIns['description']){
					$details .='<div><b><span onclick="showRoadside(\''.$valIns["insurance_insurer"].'\',\''.$valIns['insurance_type'].'\')" class="cursorPoin cff2da5"><i class="fas fa-hand-point-right"></i> บริการช่วยเหลือฉุกเฉิน</span></b></div>';
				}
				$details .=
				'</td>

				<td>
				<div><b>ประเภทเบี้ย : </b>'.$license_types.'</div>';

				if($valIns['insurance_type'] != "2+ (3M)" && $valIns['insurance_type'] != "1 (3M)"){
					$details .= '
					<div><b>เบี้ยสุทธิ : <span class="cff2da5">'.number_format($valIns['inscost_premamount'],2).'</span> </b> บาท</div>
					<div><b>เบี้ยรวมภาษีอากร : <span class="cff2da5">'.number_format($valIns['inscost_taxamount'],2).'</span> </b>บาท</div>';
				}else{
					$details .= '
					<div><b>เบี้ยสุทธิ : <span class="cff2da5">'.number_format($valIns['inscost_premamount'] * 4,2).'</span> </b> บาท</div>
					<div><b>เบี้ยรวมภาษีอากร : <span class="cff2da5">'.number_format($valIns['inscost_taxamount'] * 4,2).'</span> </b>บาท</div>';
				}

				if($valIns['insurance_type'] != "2+ (3M)" && $valIns['insurance_type'] != "1 (3M)"){

					$details .='<div><b><span onclick="btnShowPay('.$valIns["inscost_id"].')" class="cursorPoin c00ac0a"><i class="fas fa-hand-point-right"></i>การชำระเงิน</span></b></div>';

					$details .='
					<div id="btnShowPay_'.$valIns["inscost_id"].'" class="mt5 pt5 dn" style="border-top: 1px solid #ff8000;" ><b>ชำระงวดเดียว</b> 
						<div class="fs12">
							<b class="c2457ff">QR|โอน|7-11</b> 
							<div class="pl10">จ่าย : <span class="cff2da5 fwb">'.number_format($premium,2).'</span> </b>บาท | ลด : <span class="cff2da5 fwb">'.number_format($discountSum,2).'</span> บาท</div>
						</div>
						<div class="fs12">
							<b class="c2457ff">บัตรเครดิต</b> 
							<div class="pl10">จ่าย : <span class="cff2da5 fwb">'.number_format($premiumCredit,2).'</span> </b>บาท | ลด : <span class="cff2da5 fwb">'.number_format($discountSumCredit,2).'</span> บาท</div>
						</div>
					<div class="mt5 pt5 " style="border-top: 1px solid #ff8000;"><b>ผ่อนชำระ</b> 
					';

					// if($discountSum) { 
					// 	$details .= "<div class='pl10'>  ลดไป<b class='cff2da5'> : ".number_format($discountSum)."</b> บาท</b></div>"; 
					// }

					if($valIns["installment"]) { 
						$details .= '<div class="fs12 "><b class="c2457ff">ผ่อนบัตรสูงสุด: '.$valIns['installment'].' เดือน</b></div>';
						$details .= '<div class="fs12 pl10">  งวดละ <b class="cff2da5">'.number_format($down).'</b> x '.$valIns['installment'].' = <b class="cff2da5">'.number_format($down*$valIns['installment']).' </b>บ.</div>';
					}

					// if($valIns['insurance_insurer'] == "เอเชียประกันภัย 1950" && $valIns['insurance_type'] != "3"){
					// 		$down3Month = ceil(($pay6 * 30 ) / 100 );
					// 		$down3NextMonth = ceil(($pay6 - $down3Month ) / 5 );
					// 		$down3Next2Month = ceil($pay6 - ($down3Month + $down3NextMonth) );
					// 		$discountDown3 = number_format(round($tax-$pay6));

					// 		$details .= '<div class="mt5"><b>ผ่อน 6 เดือน : <span class="cff2da5">'.number_format($pay6).' </span></b> บ. 
					// 		| ลด <b class="cff2da5"> '.$discountDown3.'</b> บ. </div>';
					// 		$details .= '<div class="pl10">งวดที่ 1 : <b class="cff2da5">'.number_format($down3Month).'</b> บาท </div>';
					// 		$details .= '<div class="pl10">งวดที่ 2 - 6 : <b class="cff2da5">'.number_format($down3NextMonth).'</b> บาท </div>';
					//}else{
						if($valIns['insurance_type'] != "3" && $valIns['bank_sale_two']) { 
							$down2Month = $pay2 / 2 ;
							$down2NextMonth = ceil(($pay2 - $down2Month ) / 2 );
							$discountDown2 = number_format(round($tax-$pay2));

							$details .= '<div class="mt5"><b>ผ่อน 2 เดือน : <span class="cff2da5">'.number_format($pay2).' </span></b> บ. 
							| ลด <b class="cff2da5"> '.$discountDown2.'</b> บ. </div>';
							$details .= '<div class="pl10">งวดที่ 1 : <b class="cff2da5">'.number_format($down2Month).'</b> บาท </div>';
							$details .= '<div class="pl10">งวดที่ 2 : <b class="cff2da5">'.number_format($down2Month).'</b> บาท </div>';
						}
						if($valIns['insurance_type'] != "3" && $valIns['bank_sale_three']) { 
							$down3Month = ceil(($pay3 * 40 ) / 100 );
							$down3NextMonth = ceil(($pay3 - $down3Month ) / 2 );
							$down3Next2Month = ceil($pay3 - ($down3Month + $down3NextMonth) );
							$discountDown3 = number_format(round($tax-$pay3));

							$details .= '<div class="mt5"><b>ผ่อน 3 เดือน : <span class="cff2da5">'.number_format($pay3).' </span></b> บ. 
							| ลด <b class="cff2da5"> '.$discountDown3.'</b> บ. </div>';
							$details .= '<div class="pl10">งวดที่ 1 : <b class="cff2da5">'.number_format($down3Month).'</b> บาท </div>';
							$details .= '<div class="pl10">งวดที่ 2 : <b class="cff2da5">'.number_format($down3NextMonth).'</b> บาท </div>';
							$details .= '<div class="pl10">งวดที่ 3 : <b class="cff2da5">'.number_format($down3Next2Month).'</b> บาท </div>';
						}
						
					//}
					}else{
						$pay3m = $valIns['inscost_taxamount'] * 4;
						$pay3mDownNext = ceil($valIns['inscost_taxamount'] / 2);

						$details .= '<div class="mt5 pt5 " style="border-top: 1px solid #ff8000;"><b>ผ่อนชำระ</b>'; 
						$details .= '<div class="pl10">งวดแรก : <b class="cff2da5">'.number_format($valIns['inscost_taxamount'],2).'</b> บาท </div>';
						$details .= '<div class="pl10">งวด 2-7 : <b class="cff2da5">'.number_format($pay3mDownNext,2).'</b> บาท </div>';
					}

					$details .= '</div></div>';
// echo date("Ymd",strtotime($valIns['condiPayEndDate'])
					if($valIns['insurance_type'] == "1" && $valIns['insurance_insurer'] == 'วิริยะประกันภัย') {
							$details .='<div><b><span onclick="showCondiANC()" class="cursorPoin cf40053 fwb fs14"><i class="fas fa-hand-point-right"></i> เงื่อนไขผ่อนกับ ANC </span></b></div>';
					}
					if($valIns['condiPayID'] && $valIns['condiPayStatus'] ==1){
						$details .='<div><b><span onclick="showCondiPay(\''.$valIns["condiPayID"].'\')" class="cursorPoin cf40053 fwb fs14"><i class="fas fa-hand-point-right"></i> เงื่อนไขชำระบัตร </span></b></div>';
					}
				$details .= '</td>';

				$details .= '<td>';
				$details .= '<div class="c9c00c8"><b>ความรับผิดชอบต่อบุคคลภายนอก</b></div>';
				$protect1 = getProtect($valIns['insurance_id'], "ความรับผิดชอบต่อบุคคลภายนอก");
				$protect2 = getProtect($valIns['insurance_id'], "ความรับผิดต่อตัวรถยนต์");
				$protect3 = getProtect($valIns['insurance_id'], "ความคุ้มครองตามเอกสารแนบท้าย");

				foreach ($protect1 as $key => $velProtact1){
					$details .=  "- ".$velProtact1['protect_name'].': <span class="c2457ff">'.number_format($velProtact1['protect_cost']) .'</span> บาท <br>';
				}
				$details .= '<div class="c9c00c8"><b>ความรับผิดต่อตัวรถยนต์</b></div>';
				foreach ($protect2 as $key => $velProtact2){
					if($velProtact2['protect_name'] == 'คุ้มครองน้ำท่วม'){
						if($velProtact2['protect_cost'] == 1){
							$details .=  "- ".$velProtact2['protect_name'].': <span class="c2457ff">ตามทุนประกันภัย</span> <br>';
						}else if($velProtact2['protect_cost'] == 0){
							$details .=  "- ".$velProtact2['protect_name'].': <span class="c2457ff">ไม่คุ้มครอง</span> <br>';
						}else{
							$details .=  "- ".$velProtact2['protect_name'].': <span class="c2457ff">'.number_format($velProtact2['protect_cost']) .'</span> บาท <br>';
						}
					}else{
						if($velProtact2['protect_cost'] != 0){
							$details .=  "- ".$velProtact2['protect_name'].': <span class="c2457ff">'.number_format($velProtact2['protect_cost']) .'</span> บาท <br>';
						}else{
							$details .=  "- ".$velProtact2['protect_name'].': <span class="c2457ff">ตามทุนประกันภัย</span> <br>';
						}
					}
				}
				$details .= '<div class="c9c00c8"><b>ความคุ้มครองตามเอกสารแนบท้าย</b></div>';
				foreach ($protect3 as $key => $velProtact3){
					$details .=  "- ".$velProtact3['protect_name'].': <span class="c2457ff">'.number_format($velProtact3['protect_cost']) .'</span> บาท <br>';
				}
				$details .= '<div><b>คุ้มครองคนขับขี่ 1 คน และผู้โดยสาร <span class="c2457ff">'.$valIns["no_protect"].'</span> คน</b></div>';
				$details .= '</td>';
				$details .= '</tr>';
				echo $details;
			//}
		}
	}

}else if ($_POST["action"] == "getInsurList") {
	$planid = $_POST["insid"];
	$costid = $_POST["costid"];
	$resultarray = array();
// echo  "<pre>".print_r($_SESSION['inslist'],1)."</pre>"; 
	$num = count($_SESSION['inslist']['costid']);
	if(!$num){
		$_SESSION['inslist']['costid'] =  array();
	}
	if($planid || $costid){
		$key = array_search($costid , $_SESSION['inslist']['costid']);
		if((string)$key == ""){
			if($num < 3){
				$_SESSION['inslist']['planid'][] = $planid;
				$_SESSION['inslist']['costid'][] = $costid;
				$error = 1;
			}else{
			$error = "สามารถเลือกได้เพียง 3 แพคเกตเท่านั้น"; //เกิน 3 แพคเกจ
		}
	}else{
		$error = "แพคเกตซ้ำกรุณาเลือกใหม่"; //ซ้ำกัน
	}
}else{
	$error = 1;
}
$otherData = getlistIns();
$resultarray['dataInSur']  	= $otherData;
$resultarray['error']   = $error;
echo json_encode($resultarray);
exit();

}else if ($_POST["action"] == "searchCustomers") {
	$contact_us = $conn->GetAll("SELECT * FROM contact_us WHERE tel = '".$_POST["phone"]."' ");
	echo json_encode($contact_us);
	exit();

}else if($_POST["action"] == "delpo"){
	unset($_SESSION['inslist']);
	echo 1;
	exit();

}else if($_POST["action"] == "delSessionPO"){
	$keyplan = array_search($_POST["costid"], $_SESSION['inslist']['costid']);
	if($keyplan!==false){
		unset($_SESSION['inslist']['planid'][$keyplan]);
		unset($_SESSION['inslist']['costid'][$keyplan]);
	}
	echo 1;
	exit();

}else if($_POST["action"] == "sendListsProduct"){ //cellcanter

// echo "<pre>".print_r($_POST,1)."</pre>";
// exit();
	$ck_redbook = getRedbook($_POST["make"], $_POST["model"], $_POST["year"], $_POST["cc"]);
	$goodretail = $ck_redbook["redbook_tks_goodretail"];

	$dataArray1 = array();
	$dataArray2 = array();
	$dataSend = array();
	$i = 0;
	$code = chkCodePO();

	$dataArray1["pocode"] 			= $code;
	$dataArray1["callcase"] 		= $_POST["callcase"];
	$dataArray1["callnew"] 		= $_POST["casenew"];
	$dataArray1["name"] 			= $_POST["name"];
	$dataArray1["email"] 			= $_POST["email"];
	$dataArray1["phone"] 			= $_POST["phone"];
	$dataArray1["line"] 			= $_POST["line"];
	$dataArray1["personnel_code"] 	= $_SESSION["User"]['UserCode'];
	$dataArray1["personnel_name"] 	= $_SESSION["User"]['firstname']." ".$_SESSION["User"]['lastname'];
	$dataArray1["make"] 			= $_POST["make"];
	$dataArray1["model"] 			= $_POST["model"];
	$dataArray1["CarModelDesc"] 	= $ck_redbook["redbook_tks_longdesc"];
	$dataArray1["year"] 			= $_POST["year"];
	$dataArray1["cc"] 				= $_POST["cc"];
	$dataArray1["class"] 			= $_POST["class"];
	$dataArray1["repairs"] 			= $_POST["repair"];
	$dataArray1["ip"] 				= $_SERVER['REMOTE_ADDR'] ; 
	$dataArray1["newfrom"] 			= $_POST["newfrom"];
	$dataArray1["remark"] 			= $_POST["remark"]; 
	$dataArray1["created_date"] 	= date("Y-m-d H:i:s");
// $insertSQL = $conn2->AutoExecute("log_api", $dataArray1, 'INSERT'); 
//  $api_id = $conn2->Insert_ID();
	$dataSend["data1"] = $dataArray1;
// sendMailForContact($dataSend);
// exit();
	$ibor = curlSenddataInsur($dataSend);
// echo "<pre>".print_r($dataSend,1)."</pre>";
// exit();
	$responsecode = getTextBetweenTags($ibor["ImportWebLeadResult"], "RESPONSECODE");
	$cpid = getTextBetweenTags($ibor["ImportWebLeadResult"], "CPID");
	$empcode = getTextBetweenTags($ibor["ImportWebLeadResult"], "EMPCODE");
	$arr = array();
	$arr["responsecode"] = $responsecode ;
	$arr["cpid"] = $cpid;
	if($empcode) {
		$emp = getPersonnelCode($empcode);
		$arr["personnel_code"] = $empcode;
		$arr["personnel_name"] = $emp["personnel_firstname"]." ".$emp["personnel_lastname"];
	}
  // echo "<pre>".print_r($arr,1)."</pre>";
// exit();
	$conn2->AutoExecute("log_api", $arr, 'UPDATE', "api_id = ".$api_id); 
// exit();
	$mail = sendMailForContact($dataSend);
// echo "<pre>".print_r($dataSend,1)."</pre>"; exit();
	unset($_SESSION['inslist']);
	echo "<script>alert('ระบบทำการส่งข้อเสนอเรียบร้อยแล้ว');</script>";
	echo '<META http-equiv="refresh" content="0;URL=../chkinser.php">';
	exit();

}else if($_POST["action"] == "getAllCustomerlist"){
	$log_apilists = $conn2->GetAll("SELECT * FROM log_apilists WHERE api_pocode = '".$_POST["pcode"]."' ");
// echo "<pre>".print_r($log_apilists,1)."</pre>";
	$details = "";
	if($log_apilists){
		foreach ($log_apilists as $key => $valList) {
			$repairs = ($valList['repairs']=="hall") ? "ซ่อมห้าง" : "ซ่อมอู่" ;
			$status = ($valList['status']=="1") ? "ยืนยันข้อเสนอ" : "" ;
			$details .= '
			<div class="col-md-4">
			<div class="boxlistapi" id="'.$_POST["pcode"].'_'.$valList["apilists_id"].'">
			<p class="fs14 t_r c02c ">'.$status.'</p>
			<p><b>ประกันชั้น </b>'.$valList["class"].' '.$repairs.'<b> บริษัท </b>'.$valList["insurercode"].' '.$valList["insurername"].' </p>
			<p><b>แพคเกต </b>'.$valList["package_name"].' <b>ทุนระกัน </b>'.number_format( $valList["c536"],2).' บาท</p>	
			<p><b>เบี้ยสุทธิ </b> <span class="cff8000"> '.number_format( $valList["netpremium"],2).' </span> <b> บาท</b> </p>
			<p><b>เบี้รวมภาษี </b> <span class="cff8000"> '.number_format( $valList["premium"],2).' </span> <b> บาท</b> </p>
			<p><b>ส่วนลด </b> <span class="cff8000"> '.number_format( $valList["discount"],2).' </span> <b> บาท</b> </p>
			<p><b>เบี้ยประกันภัย (ไม่รวม พรบ.) </b> <span class="cff8000"> '.number_format( $valList["taxamount"],2).' </span> <b> บาท</b> </p>';
			if($valList["actprice"]){
				$details .= '
				<p><b>พรบ. </b> <span class="cff8000"> '.number_format( $valList["actprice"],2).' </span> <b> บาท</b> </p>
				<p><b>เบี้ยประกันภัย (รวม พรบ.) </b> <span class="cff8000"> '.number_format( $valList["taxamount"]+$valList["actprice"],2).' </span> <b> บาท</b> </p>';
			}
			$details .= '
			<div>
			<div><b>ความรับผิดต่อบุคคลภายนอก</b></div>
			<div>1) ความเสียหายต่อชีวิต ร่างกาย หรืออนามัย (บาท/คน)  <span class="cff8000">'.number_format( $valList["c502"],2).' </span> บาท</div>
			<div>ความเสียหายต่อชีวิต ร่างกาย หรืออนามัย (บาท/ครั้ง) <span class="cff8000">'.number_format( $valList["c503"],2).' </span> บาท</div>
			<div>2) ความเสียหายต่อทรัพย์สิน (บาท/ครั้ง) <span class="cff8000">'.number_format( $valList["c504"],2).' </span> บาท</div>
			<div>2.1) ความเสียหายส่วนแรก (บาท/ครั้ง) <span class="cff8000">'.number_format( $valList["deduct"],2).' </span> บาท</div>
			<div><b>รถยนต์เสียหาย สูญหาย ไฟไหม้</b></div>
			<div>1) ความเสียหายต่อรถยนต์ (บาท/ครั้ง) <span class="cff8000">'.number_format( $valList["c536"],2).' </span> บาท</div>
			<div>2) ความเสียหายส่วนแรก (กรณีฝ่ายผิด) (บาท/ครั้ง) <span class="cff8000">'.number_format( $valList["deduct"],2).' </span> บาท</div>
			<div>3) รถยนต์สูญหาย/ไฟไหม้ (บาท) <span class="cff8000">'.number_format( $valList["c507"],2).' </span> บาท</div>
			<div><b>ความคุ้มครองตามเอกสารแนบท้าย</b></div>
			<div>1) อุบัติเหตุส่วนบุคคล <span class="cff8000">'.number_format( $valList["c517"],2).' </span> บาท</div>
			<div>2) ค่ารักษาพยาบาล '.($valList["no_ofpax"]+1).'คน) (บาท/คน) <span class="cff8000">'.number_format( $valList["c525"],2).' </span> บาท</div>
			<div>3) การประกันตัวผู้ขับขี่ (บาท/ครั้ง) <span class="cff8000">'.number_format( $valList["c532"],2).' </span> บาท</div>
			</div>

			</div>
			</div>
			';
		}
	}else{
		$details .= '<div class="col-md-12 t_c"><h5>ไม่พบข้อมูล</h5></div>';
	}
	echo $details;
	exit();


}else if($_POST["action"] == "updateProfile"){

// echo "<pre>".print_r($_POST,1)."</pre>"; exit();

	$rs = $conn2->AutoExecute("ck_personnel", $_POST, 'UPDATE', "personnel_id = ".$_POST["userid"]);
	if($rs){
		echo "<script>alert('แก้ไขข้อมูลสำเร็จ');</script>";
	}else{
		echo "<script>alert('กรุณาลองใหม่อีกครั้');</script>";
	}

	echo '<meta http-equiv="Refresh" content="0;' .$_SERVER['HTTP_REFERER']. '">';
	exit();

}else if($_POST["action"] == "sendleadnew"){

	$ck_redbook = getRedbook($_POST["make"], $_POST["model"], $_POST["year"], $_POST["cc"]);
	$goodretail = $ck_redbook["redbook_tks_goodretail"];

	$dataArray1 = array();
	$dataArray2 = array();
	$dataSend = array();
	$i = 0;
	$code = chkCodePO();
// echo "<pre>".print_r($_POST,1)."</pre>";  
	$dataArray1["pocode"] 	= $code;
	$dataArray1["callcase"] = $_POST["callcase"];
	$dataArray1["callnew"] 		= $_POST["casenew"];
	$dataArray1["name"] 	= $_POST["name"];
	$dataArray1["email"] 	= $_POST["email"];
	$dataArray1["phone"] 	= $_POST["phone"];
	$dataArray1["line"] 			= $_POST["line"];
	$dataArray1["personnel_code"] 	= $_SESSION["User"]['UserCode'];
	$dataArray1["personnel_name"] 	= $_SESSION["User"]['firstname']." ".$_SESSION["User"]['lastname'];
	$dataArray1["make"] 	= $_POST["make"];
	$dataArray1["model"] 	= $_POST["model"];
	$dataArray1["CarModelDesc"] 	= $ck_redbook["redbook_tks_longdesc"];
	$dataArray1["year"] 	= $_POST["year"];
	$dataArray1["cc"] 		= $_POST["cc"];
	$dataArray1["class"] 	= $_POST["class01"];
	$dataArray1["cartype"] = $_POST["ins"]["0"]["cartype"];
	$dataArray1["repairs"] 	= $_POST["repair"]; 
	$dataArray1["plate_no"] 	= $_POST["plate_no"];
	$dataArray1["plate_province"] 	= $_POST["plate_province"];
	$dataArray1["body_type"] 	= $_POST["body_type"];
	$dataArray1["frame_no"] 	= $_POST["frame_no"];
	$dataArray1["start_cover_date"] 	= $_POST["start_cover_date"];
	$dataArray1["end_cover_date"] 	= $_POST["end_cover_date"];
	$dataArray1["ip"] 		= $_SERVER['REMOTE_ADDR'] ; 
	$dataArray1["isReplace"] 	= $_POST["isReplace"];
	$dataArray1["newfrom"] 	= $_POST["newfrom"];
	$dataArray1["remark"] 	= $_POST["remark"];
	$dataArray1["cpid"] 		= $_POST["cpid"] ;  
	$dataArray1["status"] 	= "0";
	$dataArray1["created_date"] 	= date("Y-m-d H:i:s");

// echo "<pre>".print_r($dataArray1,1)."</pre>";  exit();
	$insertSQL = $conn2->AutoExecute("log_api", $dataArray1, 'INSERT'); 
	$api_id = $conn2->Insert_ID();
	$dataSend["data1"] = $dataArray1;

  // echo "<pre>".print_r($dataArray1,1)."</pre>";	
	foreach($_POST["ins"] as $key => $value) {
		$dataArray2["api_id"] = $api_id;
		$dataArray2["api_pocode"] = $code;
		$dataArray2["insurance_id"] = $value["insurance_id"];
		$dataArray2["insurance_cost_id"] = $value["insurance_cost_id"];
		$dataArray2["insurercode"] = $value["insurercode"];
		$dataArray2["insurername"] = $value["insurername"];
		$dataArray2["class"] = $value["class"];
		$dataArray2["cartype"] = $value["cartype"];
		$dataArray2["package_code"] = $value["package_code"];
		$dataArray2["package_name"] = $value["package_name"];
		$dataArray2["repairs"] = $value["repairs"];
		$dataArray2["netpremium"] = str_replace( ',', '', $value["netpremium"]);
		$dataArray2["premium"] = str_replace( ',', '', $value["premium"]);
		$dataArray2["discount"] = str_replace( ',', '', $value["discount"]);
		$dataArray2["taxamount"] = str_replace( ',', '', $value["taxamount"]) - $value["actprice"];
		$dataArray2["inscost"] = str_replace( ',', '', $value["inscost"]);
		$dataArray2["deduct"] = str_replace( ',', '', $value["deduct"]);
		$dataArray2["actprice"] = $value["actprice"];
		$dataArray2["installment"] = $value["install"];
		$dataArray2["downs"] = str_replace( ',', '', $value["downs"]);
		$dataArray2["no_ofpax"] = $value["no_ofpax"];
		$dataArray2["C502"] = str_replace( ',', '', $value["C502"]);
		$dataArray2["C503"] = str_replace( ',', '', $value["C503"]);
		$dataArray2["C504"] = str_replace( ',', '', $value["C504"]);
		$dataArray2["C536"] = str_replace( ',', '', $value["C536"]);
		$dataArray2["c537"] = str_replace( ',', '', $value["c537"]);
		$dataArray2["C507"] = str_replace( ',', '', $value["C507"]);
		$dataArray2["C508"] = str_replace( ',', '', $value["C508"]);
		$dataArray2["C517"] = str_replace( ',', '', $value["C517"]);
		$dataArray2["C525"] = str_replace( ',', '', $value["C525"]);
		$dataArray2["C532"] = str_replace( ',', '', $value["C532"]);
		$dataArray2["showdown"] = $value["showdown"] ? $value["showdown"] : 0 ;
		$dataArray2["showdiscount"] = $value["showdiscount"] ? $value["showdiscount"] : 0 ;		
	// $dataArray2["downcash_1"] = str_replace( ',', '', $value["downcash_1"]);
	// $dataArray2["downcash_2"] = str_replace( ',', '', $value["downcash_2"]);
	// $dataArray2["downcash_3"] = str_replace( ',', '', $value["downcash_3"]);
		$dataArray2["status"] = 0;

		$insertSQL2 = $conn2->AutoExecute("log_apilists", $dataArray2, 'INSERT'); 
		$apilistsID = $conn2->Insert_ID();
		if($value["downcash"]){
			foreach ($value["downcash"] as $key => $valDowncash) {
				if($key == 1){
					$dataArray2["downcash"][$key] = $valDowncash - $value["actprice"];
					$arrDown["price"] = $valDowncash - $value["actprice"];
				}else{
					$dataArray2["downcash"][$key] = $valDowncash ;
					$arrDown["price"] = $valDowncash;
				}
				$arrDown["log_apilists_id"] = $apilistsID;
				$arrDown["periods"] = $key;
				$arrDown["status"] = 0;
				$insertSQL2 = $conn2->AutoExecute("log_apil_dowsprice", $arrDown, 'INSERT'); 
			}
		}
		$dataSend["data2"][$i++] = $dataArray2;

	}
// $mail = sendMailForContact($dataSend); exit();
// exit();
	$ibor = curlSenddataInsur($dataSend);
// echo "<pre>".print_r($dataSend,1)."</pre>"; exit();; 
	$responsecode = getTextBetweenTags($ibor["ImportWebLeadResult"], "RESPONSECODE");
	$cpid = getTextBetweenTags($ibor["ImportWebLeadResult"], "CPID");
	$empcode = getTextBetweenTags($ibor["ImportWebLeadResult"], "EMPCODE");
	$arr = array();
	$arr["responsecode"] = $responsecode ;
	$arr["cpid"] = $cpid;
	if($empcode) {
		$emp = getPersonnelCode($empcode);
		$arr["personnel_code"] = $empcode;
		$arr["personnel_name"] = $emp["personnel_firstname"]." ".$emp["personnel_lastname"];
	}
	$conn2->AutoExecute("log_api", $arr, 'UPDATE', "api_id = ".$api_id); 
  // echo "<pre>".print_r($ibor,1)."</pre>"; exit();
// $mail = sendMailForContact($dataSend); exit();
	if($responsecode == 1){
		$mail = sendMailForContact($dataSend);
		unset($_SESSION['inslist']);
		echo "<script>alert('ระบบทำการส่งข้อเสนอเรียบร้อยแล้ว');</script>";
		if($_POST["cpid"]){
			echo '<META http-equiv="refresh" content="0;URL=../offeredits.php?cpid='.$_POST["cpid"].'">';
		}else{
			echo '<META http-equiv="refresh" content="0;URL=../chkinser.php">';
		}
		exit();
	}else if($responsecode == 2){
		unset($_SESSION['inslist']);
		echo "<script>alert('พบลูกค้าอยู่ในระบบ iBroker แล้ว');</script>";
		echo '<META http-equiv="refresh" content="0;URL=../chkinser.php">';
		exit();
	}else if($responsecode == 4){
		$mail = sendMailForContact($dataSend);
		unset($_SESSION['inslist']);
		echo "<script>alert('แก้ไขข้อเสนอในระบบ iBroker เรียบร้อย');</script>";
		echo '<META http-equiv="refresh" content="0;URL=../offeredits.php?cpid='.$_POST["cpid"].'">';
  // 	if($_POST["cpid"]){
	 	// 	echo '<META http-equiv="refresh" content="0;URL=../offeredits.php?cpid='.$_POST["cpid"].'">';
	 	// }else{
			// echo '<META http-equiv="refresh" content="0;URL=../offeredits.php?cpid='.$_POST["cpid"].'">';
	 	// }

		exit();
	}else{
  	// unset($_SESSION['inslist']);
  	// echo $_SERVER['HTTP_REFERER'];
		echo "<script>alert('ไม่สามารถทำรายการได้ กรุณาลองใหม่');</script>";
		echo '<META http-equiv="refresh" content="0;URL='.$_SERVER['HTTP_REFERER'].'">';
		exit();
	}
// echo "<pre>".print_r($dataSend,1)."</pre>"; exit();
	exit();

}else if($_POST["action"] == "sendUpdateLead"){

// echo "<pre>".print_r($_POST,1)."</pre>"; 
	$dataArray1 = array();
	$dataArray2 = array();
	$dataSend["data1"] = getAllCustomerCase($_POST["pocode"]);
// $dataSend["data1"] = $api;
	foreach($_POST["ins"] as $key => $value) {
	// if($value["status"] == "active"){
		$apilists_id = $value["apilists_id"];
		$dataArray2["api_id"] = $_POST["api_id"];
		$dataArray2["api_pocode"] = $_POST["pocode"];
		$dataArray2["insurance_id"] = $value["insurance_id"];
		$dataArray2["insurance_cost_id"] = $value["insurance_cost_id"];
		$dataArray2["insurercode"] = $value["insurercode"];
		$dataArray2["insurername"] = $value["insurername"];
		$dataArray2["class"] = $value["class"];
		$dataArray2["cartype"] = $value["cartype"];
		$dataArray2["package_code"] = $value["package_code"];
		$dataArray2["package_name"] = $value["package_name"];
		$dataArray2["repairs"] = $value["repairs"];
		$dataArray2["netpremium"] = str_replace( ',', '', $value["netpremium"]);
		$dataArray2["premium"] = str_replace( ',', '', $value["premium"]);
		$dataArray2["discount"] = str_replace( ',', '', $value["discount"]);
		$dataArray2["taxamount"] = str_replace( ',', '', $value["taxamount"]);
		$dataArray2["inscost"] = str_replace( ',', '', $value["inscost"]);
		$dataArray2["deduct"] = str_replace( ',', '', $value["deduct"]);
		$dataArray2["actprice"] = str_replace( ',', '', $value["actprice"]);
		$dataArray2["installment"] = $value["install"];
		$dataArray2["downs"] = str_replace( ',', '', $value["downs"]);
		$dataArray2["no_ofpax"] = $value["no_ofpax"];
		$dataArray2["C502"] = str_replace( ',', '', $value["C502"]);
		$dataArray2["C503"] = str_replace( ',', '', $value["C503"]);
		$dataArray2["C504"] = str_replace( ',', '', $value["C504"]);
		$dataArray2["C536"] = str_replace( ',', '', $value["C536"]);
		$dataArray2["c537"] = str_replace( ',', '', $value["c537"]);
		$dataArray2["C507"] = str_replace( ',', '', $value["C507"]);
		$dataArray2["C508"] = str_replace( ',', '', $value["C508"]);
		$dataArray2["C517"] = str_replace( ',', '', $value["C517"]);
		$dataArray2["C525"] = str_replace( ',', '', $value["C525"]);
		$dataArray2["C532"] = str_replace( ',', '', $value["C532"]);
		$dataArray2["showdown"] = $value["showdown"] ? $value["showdown"] : 0 ;
		$dataArray2["showdiscount"] = $value["showdiscount"] ? $value["showdiscount"] : 0 ;
		$dataArray2["status"] = ($_POST["statusMain"] == "active") ? 1 : 0;

		$conn2->AutoExecute("log_apilists", $dataArray2, 'UPDATE', "apilists_id = '".$value["apilists_id"]."' ");
		$dowsprice = $conn2->GetAll("SELECT * FROM log_apil_dowsprice WHERE log_apilists_id = '".$apilists_id."' ");
		// echo "<br>".count($dowsprice)."- ".count($value["downcash"]);
		if(count($dowsprice) == count($value["downcash"])){
			$periods = 1;
			foreach ($dowsprice as $key => $valDowsprice) {
				$valDowncash = $value["downcash"][$periods];
				if($key == 0){
					$dataArray2["downcash"][$key] = $valDowncash - $value["actprice"];
					$arrDown["price"] = $valDowncash - $value["actprice"];
				}else{
					$dataArray2["downcash"][$key] = $valDowncash ;
					$arrDown["price"] = $valDowncash;
				}
				$arrDown["log_apilists_id"] = $apilists_id;
				$arrDown["status"] = 0;
				// echo "<br>UPDATE";
				// echo $arrDown["periods"] = $periods;
				$conn2->AutoExecute("log_apil_dowsprice", $arrDown, 'UPDATE', "log_apilists_id = '".$value["apilists_id"]."' AND periods = '".$periods."' ");
				// echo "<br>".$key."downcash:".$arrDown["price"]." - ".$value["downcash"][$periods];
				$periods++;
			}
		}else{
			// echo "<br>DELETE";
			$conn2->Execute("DELETE FROM log_apil_dowsprice Where log_apilists_id = '".$value["apilists_id"]."'");
			if($value["showdown"]){
				foreach ($value["downcash"] as $key => $valDowncash) {
					if($key == 1){
						$dataArray2["downcash"][$key] = $valDowncash - $value["actprice"];
						$arrDown["price"] = $valDowncash - $value["actprice"];
					}else{
						$dataArray2["downcash"][$key] = $valDowncash ;
						$arrDown["price"] = $valDowncash;
					}
					$periods++;
					$arrDown["log_apilists_id"] = $apilists_id;
					$arrDown["status"] = 0;
					$arrDown["periods"] = $key;
					// echo "<br>INSERT"; 
					// echo "<br>".$key."downcash:".$arrDown["price"]." - ".$value["downcash"];
					$insertSQL2 = $conn2->AutoExecute("log_apil_dowsprice", $arrDown, 'INSERT');
				}
			}
		}

		if($_POST["statusMain"] == "active"){
			if($value["status"] == "active"){
				$dataSend["data2"][0] = $dataArray2;
			}
		}else{
			$dataSend["data2"][$i++] = $dataArray2;
		}
	// }
	}
// echo "<pre>".print_r($dataSend ,1)."</pre>";
// echo "<pre>".print_r($dataArray2 ,1)."</pre>";
// $ibor = curlSenddataInsur($dataSend);
// $mail = sendMailForContact($dataSend);
// echo $apilists_id; exit();
	$ibor = curlSenddataInsur($dataSend);
	$responsecode = getTextBetweenTags($ibor["ImportWebLeadResult"], "RESPONSECODE");
	$cpid = getTextBetweenTags($ibor["ImportWebLeadResult"], "CPID");
// echo "<pre>".print_r($dataArray2,1)."</pre>"; 
// $responsecode = 1;
	if($responsecode == 4){
		$mail = sendMailForContact($dataSend);
		if($_POST["statusMain"] == "active"){
			$arr["responsecode"] 	= $responsecode ;
			$arr["cpid"] = $cpid;
			$arr["status"] 	= 1;
			$arr["update_date"] 	= date("Y-m-d H:i:s");
			$conn2->AutoExecute("log_api", $arr, 'UPDATE', "pocode = '".$_POST["pocode"]."' ");
			$arr2["status"] 	= 1;
			$conn2->AutoExecute("log_apilists", $arr2, 'UPDATE', "apilists_id = '".$apilists_id."' ");
		}
		echo "<script>alert('แก้ไขข้อเสนอในระบบ iBroker เรียบร้อย');</script>";
		echo '<META http-equiv="refresh" content="0;URL=../chklists.php">';
		exit();
	}else{
  	// unset($_SESSION['inslist']);
  	// echo $_SERVER['HTTP_REFERER'];
		echo "<script>alert('ไม่สามารถทำรายการได้ กรุณาลองใหม่');</script>";
		echo '<META http-equiv="refresh" content="0;URL='.$_SERVER['HTTP_REFERER'].'">';
		exit();
	}


	exit();

}else if($_POST["action"] == "userAdd"){
	$chk = $conn2->GetROW("SELECT * FROM ck_personnel WHERE personnel_code = '".$_POST["personnel_code"]."' ");
	if(!$chk){
		$rs = $conn2->AutoExecute("ck_personnel", $_POST, 'INSERT');
		if($rs){
			echo "<script>alert('เพิ่มข้อมูลสำเร็จ');</script>";
		}else{
			echo "<script>alert('กรุณาลองใหม่อีกครั้ง');</script>";
		}
		echo '<meta http-equiv="Refresh" content="0;' .$_SERVER['HTTP_REFERER']. '">';
		exit();
	}else{
		echo "<script>alert('พบรหัส login ซ้ำ กรุณาตรวจสอบใหม่');</script>";
	}

	echo '<meta http-equiv="Refresh" content="0;' .$_SERVER['HTTP_REFERER']. '">';
	exit();

}else if($_POST["action"] == "getUsers"){
	$ck_personnel = $conn2->GetRow("SELECT * FROM ck_personnel WHERE personnel_id = '".$_POST["id"]."' ");
	echo json_encode($ck_personnel);
	exit();

}else if($_POST["action"] == "chkNumnoti"){
	$count = $conn2->GetOne("SELECT count(noti_comment_id) FROM noti_comment WHERE views = '0'  AND comment_to = '".$_POST["code"]."' ");
	echo $count;
	exit();


}else if($_POST["action"] == "getNtificationTime"){
	$html = "";
	$arr = array();
	$noti_comment = $conn2->GetAll("SELECT noti_comment.noti_work_id, noti_comment.noti_work_code, noti_comment.datetime, noti_comment.comment, noti_comment.comment_byname,noti_work.po_code
		FROM noti_comment INNER JOIN noti_work ON noti_comment.noti_work_code = noti_work.noti_work_code 
		WHERE comment_to = '".$_POST["code"]."' ORDER BY datetime DESC LIMIT 0,10 ");
// echo "<pre>".print_r($noti_comment,1)."</pre>"; 
// echo json_encode($ck_personnel);
	foreach ($noti_comment as $key => $value) {
		$co = ($value["views"] == 0) ? "cff8000" : "c4d";
		$gb = ($value["views"] == 0) ? "bgededed" : "";
		if($_SESSION["User"]['type'] == "QualityControl"){
			$url = "sendorderslists.php?pocode=".$value["po_code"]."&numcode=".$value["noti_work_code"];
		}else{
			$url = "sendordersviews.php?pocode=".$value["po_code"]."&numcode=".$value["noti_work_code"];
		}
		$html .= ' 
		<li class="'.$gb.'"><a href="'.$url.'" class="notification-item p5" >
		<div class="fs12 '.$co.'">'.$value["comment"].'</div>
		<div class="fs10 ca5a5a5">'.$value["noti_work_code"].' | '.$value["comment_byname"].'</div>
		<div class="fs10 ca5a5a5">'.$value["datetime"].'</div>
		</a></li>';

		$arr["views"] = 1;
		$conn2->AutoExecute("noti_comment", $arr, 'UPDATE', "noti_work_code = '".$value["noti_work_code"]."' ");
	}
	$html .= '<li><a href="index.php" class="more">See all notifications</a></li>';
	echo $html;
	exit();

}else if($_POST["action"] == "getRoadsideIns"){
	$roadside = $conn2->GetRow("SELECT * FROM ck_insurer_roadside WHERE insurance_Name = '".$_POST["insName"]."' AND insurance_type = '".$_POST["insClass"]."' ");
	echo json_encode($roadside);
	exit();


}else if($_POST["action"] == "getDateONcp"){

	$sql = "SELECT Compare_Policy.*, Customer.Customer_FName, Customer.Customer_LName, Customer.Tel_No, Customer.Mobile_No, Customer.EMail, Customer.Line_ID, Compare_Car_Detail.Plate_No, Compare_Car_Detail.Frame_No, Compare_Car_Detail.Body_Type, Province.Province_Name_TH
	FROM Compare_Policy
	LEFT JOIN [dbo].[Customer] ON Compare_Policy.Customer_ID = Customer.Customer_ID
	LEFT JOIN [dbo].[Compare_Car_Detail] ON Compare_Policy.CP_ID = Compare_Car_Detail.CP_ID
	LEFT JOIN [dbo].[Province] ON Compare_Car_Detail.Plate_Province = Province.Province_ID
	WHERE Compare_Policy.CP_ID = '".$_POST["cpid"]."' ";
	$stmt = sqlsrv_query( $connMS, $sql );
	if(sqlsrv_has_rows($stmt)) {
		$row = sqlsrv_fetch_array($stmt);
		echo json_encode($row);
		exit();
	}


	
}else if($_POST["action"] == "getCondiPay"){
	$conditionpay = $conn2->GetRow("SELECT * FROM conditionpay WHERE ConditionPay_id = '".$_POST["con_ID"]."'  ");
	echo json_encode($conditionpay);
	exit();


}else if($_POST["action"] == "addNotice"){
	$_POST["notice_date"] = date("Y-m-d H:i:s");
	$_POST["enable"] = 1;
	$insertSQL2 = $conn2->AutoExecute("notice", $_POST, 'INSERT');
	echo "<script>alert('เพิ่มข้อมูลสำเร็จ');</script>";
	echo '<META http-equiv="refresh" content="0;URL=../index.php">';
	exit();


}else if($_POST["action"] == "sendMailCostCondition"){
 	// echo "<pre>".print_r($_POST,1)."</pre>";
 	// exit();
 	if($_POST["mailsend"]){
	 	$EmailCost = EmailCost($_POST);
	 	if($EmailCost){
		 	echo "<script>alert('ทำรายการสำเร็จ');</script>";
			echo '<META http-equiv="refresh" content="0;URL=../email_cost.php">';	
	 	}else{
	 		echo "<script>alert('ทำรายการไม่สำเร็จกรุณาลองใหม่');</script>";
			echo '<META http-equiv="refresh" content="0;URL=../email_cost.php">';	
	 	}
	}else{
		echo "<script>alert('ทำรายการไม่สำเร็จกรุณาลองใหม่');</script>";
		echo '<META http-equiv="refresh" content="0;URL=../email_cost.php">';	
	}
 	exit();

}

?>
