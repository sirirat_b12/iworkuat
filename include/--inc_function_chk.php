<?php 
function getallProvince(){
    global $conn2;
    $act = $conn2->GetAll( "SELECT * FROM ck_province ORDER BY province_name ASC");
    // echo "<pre>".print_r($act,1)."</pre>";
    return $act;
} 

function getallProvinceID($name){
    global $conn2;
    $act = $conn2->GetRow( "SELECT * FROM ck_province WHERE province_name ='".$name."'");
    // echo "<pre>".print_r($act,1)."</pre>";
    return $act;
} 


function getRowPOByCode($pocode){
	global $connMS;
	$pocode = trim($pocode);
	$resultarray = array();
  $sql = "SELECT 
  Purchase_Order.PO_ID, Purchase_Order.Coverage_Start_Date, Purchase_Order.Capital, Purchase_Order.Insurance_Package_ID, Purchase_Order.Compulsory, Purchase_Order.Net_Premium, Purchase_Order.Discount, Purchase_Order.Total_Premium, Purchase_Order.Premium_After_Disc, Insurance_Package.Insurance_Package_Name, Purchase_Order.Marine_Reference, ADB_Premium.Premium_Desc, Insurance.Insurance_Name, Customer.Customer_Type AS cus_type, Customer.Customer_FName AS FName, Customer.Customer_LName AS LName, Insurer.Insurer_Initials AS Insurer_Initials 
  FROM [iBrokerADB].[dbo].[Purchase_Order] INNER JOIN [iBrokerADB].[dbo].[Customer] ON Purchase_Order.Customer_ID = Customer.Customer_ID 
  INNER JOIN [iBrokerADB].[dbo].[Purchase_Order_Insurer] ON Purchase_Order.PO_ID = Purchase_Order_Insurer.PO_ID 
  INNER JOIN [iBrokerADB].[dbo].[Insurance] ON Purchase_Order.Insurance_ID = Insurance.Insurance_ID
  INNER JOIN [iBrokerADB].[dbo].[Insurer] ON Purchase_Order_Insurer.Insurer_ID = Insurer.Insurer_ID
  LEFT JOIN [iBrokerADB].[dbo].[Insurance_Package] ON Purchase_Order.Insurance_Package_ID = Insurance_Package.Insurance_Package_ID
  LEFT JOIN [iBrokerADB].[dbo].[ADB_Premium] ON Purchase_Order.Premium_ID = ADB_Premium.Premium_ID 
  WHERE Purchase_Order.PO_ID = '".$pocode."' ";
  $stmt = sqlsrv_query( $connMS, $sql );
  if(sqlsrv_has_rows($stmt)) {
    $row = sqlsrv_fetch_array($stmt);
    return $row;
    exit();
  }  
	exit();
}

function getAllSendPO($userCode, $status){
    global $conn2; 
    $sql = "SELECT *,noti_work.personnel_code AS personnel_code ,ck_personnel.personnel_firstname AS chk_name, ck_personnel.personnel_lastname AS chk_lname 
      FROM noti_work LEFT JOIN ck_personnel ON noti_work.chk_code = ck_personnel.personnel_code ";
    if($userCode && $status == "QualityControl"){
      $sql .= " WHERE noti_work.status = '0' AND noti_work.enable = '1'  AND  noti_work.chk_code = '".$userCode."' OR noti_work.personnel_code = '".$userCode."' ";
    }else if($userCode && $status != "QualityControl"){
      $sql .= " WHERE noti_work.enable = '1' AND noti_work.personnel_code = '".$userCode."' ";
    }
    $sql .= " ORDER BY noti_work.start_cover_date DESC, noti_work.created_date DESC LIMIT 0,200";
// echo  $sql;
    $send_orders = $conn2->GetAll( $sql);
    // echo "<pre>".print_r($make,1)."</pre>";
    return $send_orders;
} 

function getAllListForMove($userCode, $userType){
    global $conn2;  
    // $sql = "SELECT *,noti_work.personnel_code AS personnel_code ,ck_personnel.personnel_firstname AS chk_name, ck_personnel.personnel_lastname AS chk_lname, 
    // ck_personnel.personnel_firstname AS Adname, ck_personnel.personnel_lastname AS chk_lname
    // FROM noti_work LEFT JOIN ck_personnel ON noti_work.chk_code = ck_personnel.personnel_code";
    
    if($userType == "QualityControl"){
      $sql = "SELECT *,noti_work.personnel_code AS personnel_code ,ck_personnel.personnel_firstname AS Fname, ck_personnel.personnel_lastname AS Lname
    FROM noti_work LEFT JOIN ck_personnel ON noti_work.chk_code = ck_personnel.personnel_code WHERE noti_work.status = '0' AND noti_work.enable = '1' AND  noti_work.chk_code = '".$userCode."' OR noti_work.personnel_code = '".$userCode."' ";
    
    }else if($userType == "Admin"){
      $sql = "SELECT *,noti_work.personnel_code AS personnel_code ,ck_personnel.personnel_firstname AS Fname, ck_personnel.personnel_lastname AS Lname
    FROM noti_work LEFT JOIN ck_personnel ON noti_work.admin_code = ck_personnel.personnel_code 
    WHERE noti_work.status = '2' AND noti_work.enable = '1' AND noti_work.admin_code = '".$userCode."' ";
    
    }else{
      $sql = " SELECT *,noti_work.personnel_code AS personnel_code ,ck_personnel.personnel_firstname AS Fname, ck_personnel.personnel_lastname AS Lname
    FROM noti_work LEFT JOIN ck_personnel ON noti_work.chk_code = ck_personnel.personnel_code WHERE noti_work.enable = '1' AND noti_work.status IN ('0','2') ";
    }
    $sql .= " ORDER BY noti_work.start_cover_date DESC, noti_work.created_date DESC";
// echo  $sql;
    $send_orders = $conn2->GetAll( $sql);
    // echo "<pre>".print_r($make,1)."</pre>";
    return $send_orders;
} 


function getCompanyBysrv(){
    global $connMS; 
    $resultarray = array();
    $sql = $sql = "SELECT *  FROM [iBrokerADB].[dbo].[Insurer]  WHERE (Active = 'Y') ORDER BY Insurer_Initials DESC";
    // echo "<pre>".print_r($make,1)."</pre>";
    $stmt = sqlsrv_query( $connMS, $sql );
    if(sqlsrv_has_rows($stmt)) {
        while( $row = sqlsrv_fetch_array($stmt) ) { 
          $resultarray[] = $row;
        }
        return $resultarray;
        exit();
    }
    
}

function getCompanyByID($id){
    global $connMS; 
    $resultarray = array();
    $sql = $sql = "SELECT *  FROM [iBrokerADB].[dbo].[Insurer]  WHERE (Insurer_ID = '".$id."')";
    // echo "<pre>".print_r($make,1)."</pre>";
    $stmt = sqlsrv_query( $connMS, $sql );
    if(sqlsrv_has_rows($stmt)) {
        while( $row = sqlsrv_fetch_array($stmt) ) { 
          $resultarray[] = $row;
        }
        return $resultarray;
        exit();
    }
    
}

function getNotiWorkByID($code){
  global $conn2;
  $noti_work = $conn2->GetRow( "SELECT * FROM noti_work WHERE noti_work_code ='".$code."'");
  return $noti_work;   
}

function getNotiWorkFilesByID($code){
  global $conn2;
  $noti_work_files = $conn2->GetAll( "SELECT * FROM noti_work_files WHERE noti_work_code ='".$code."'");
  return $noti_work_files;   
}

function getNotiComment($code){
  global $conn2;
  $noti_comment = $conn2->GetAll( "SELECT * FROM noti_comment WHERE noti_work_code ='".$code."' ORDER BY datetime ASC ");
  return $noti_comment;   
}

function getCommentlist(){
  global $conn2;
  $rs = $conn2->GetAll( "SELECT * FROM comment_lists WHERE enable ='1' ORDER BY commentlists ASC ");
  return $rs;   
}

function getCommenStatus(){
  global $conn2;
  $rs = $conn2->GetAll( "SELECT * FROM comment_status WHERE enable ='1' ORDER BY commentstatus ASC ");
  return $rs;   
}

function getQCcheck(){
  global $conn2; 
  $rs = $conn2->GetAll( "SELECT personnel_code FROM ck_personnel WHERE personnel_workin ='1' AND personnel_status = '1' AND personnel_type ='QualityControl'  ");
  return $rs;   
}

function getQCcheckAll(){
  global $conn2; 
  $rs = $conn2->GetAll( "SELECT * FROM ck_personnel WHERE personnel_status = '1' AND personnel_type ='QualityControl'  ");
  return $rs;   
}


function getPremium(){
  global $connMS; 
  $resultarray = array();
  $sql = $sql = "SELECT *  FROM [iBrokerADB].[dbo].[ADB_Premium]  WHERE (Active = 'Y') ORDER BY Premium_Desc ASC ";
  // echo "<pre>".print_r($make,1)."</pre>";
  $stmt = sqlsrv_query( $connMS, $sql );
  if(sqlsrv_has_rows($stmt)) {
      while( $row = sqlsrv_fetch_array($stmt) ) { 
        $resultarray[] = $row;
      }
      return $resultarray;
      exit();
    }  
}


function getPurchaseBycode($code){
  global $connMS; 
  $resultarray = array();
  $sql = "SELECT *,Customer.Contact_Person AS CustomerPerson, Customer.Tel_No AS CustomerTel, Customer.Fax AS CustomerFax,Customer.Mobile_No AS CustomerMobile_No, Customer.Customer_ID AS Customercode, Customer.Customer_Type AS cus_type, Customer.Customer_FName AS FName, Customer.Customer_LName AS LName, Insurer.Insurer_Initials AS Insurer_Initials, 
    Customer.Addr1 AS  cusAddr1, Customer.Addr2 AS  cusAddr2, Customer.Subdistrict_ID AS  cusSubdistrict, Customer.District_ID AS  cusDistrict, 
    Customer.Province_ID AS  cusProvince, Customer.Post_Code AS  cusPost_Code,
    Customer_Mail_Address.Receiver_Name AS  cussendReceiver_Name, Customer_Mail_Address.Addr1 AS  cussendAddr1, Customer_Mail_Address.Addr2 AS  cussendAddr2, 
    Customer_Mail_Address.Subdistrict_ID AS  cussendSubdistrict, Customer_Mail_Address.District_ID AS  cussendDistrict, 
    Customer_Mail_Address.Province_ID AS  cussendProvince, Customer_Mail_Address.Post_Code AS  cussendPost_Code, 
    Purchase_Order_Mail_Address.Addr1 AS  posendAddr1, Purchase_Order_Mail_Address.Addr2 AS  posendAddr2, 
    Purchase_Order_Mail_Address.Subdistrict_ID AS  posendSubdistrict, Purchase_Order_Mail_Address.District_ID AS  posendDistrict, 
    Purchase_Order_Mail_Address.Province_ID AS  posendProvince, Purchase_Order_Mail_Address.Post_Code AS  posendPost_Code, 
    Purchase_Order_Mail_Address.Remark AS  posendPostRemark
  FROM [iBrokerADB].[dbo].[Purchase_Order] INNER JOIN [iBrokerADB].[dbo].[Customer] ON Purchase_Order.Customer_ID = Customer.Customer_ID 
  INNER JOIN [iBrokerADB].[dbo].[Purchase_Order_Insurer] ON Purchase_Order.PO_ID = Purchase_Order_Insurer.PO_ID 
  INNER JOIN [iBrokerADB].[dbo].[Insurance] ON Purchase_Order.Insurance_ID = Insurance.Insurance_ID
  INNER JOIN [iBrokerADB].[dbo].[Purchase_Order_Mail_Address] ON Purchase_Order.PO_ID = Purchase_Order_Mail_Address.PO_ID  
  INNER JOIN [iBrokerADB].[dbo].[Customer_Mail_Address] ON Purchase_Order.Customer_ID = Customer_Mail_Address.Customer_ID
  INNER JOIN [iBrokerADB].[dbo].[Insurer] ON Purchase_Order_Insurer.Insurer_ID = Insurer.Insurer_ID 
  LEFT JOIN [iBrokerADB].[dbo].[Insurance_Package] ON Purchase_Order.Insurance_Package_ID = Insurance_Package.Insurance_Package_ID
  WHERE Purchase_Order.PO_ID = '".$code."' ";
  // echo "<pre>".print_r($make,1)."</pre>";
  $stmt = sqlsrv_query( $connMS, $sql );
  if(sqlsrv_has_rows($stmt)) {
    $row = sqlsrv_fetch_array($stmt);
    return $row;
    exit();
  }  
}

function getCardetailBycode($code){
  global $connMS; 
  $resultarray = array();
  $sql = "SELECT * FROM [iBrokerADB].[dbo].[Car_Detail]  
  INNER JOIN [iBrokerADB].[dbo].[Car_Make] ON Car_Detail.Make_ID = Car_Make.Make_ID
  LEFT JOIN [iBrokerADB].[dbo].[Car_Type] ON Car_Detail.Car_Code = Car_Type.Car_Type_ID 
  LEFT JOIN [iBrokerADB].[dbo].[Asia_Car_Model] ON Car_Detail.Model = Asia_Car_Model.Model_ID 
  LEFT JOIN [iBrokerADB].[dbo].[Beneficiary] ON Car_Detail.Beneficiary = Beneficiary.Beneficiary_ID  
  WHERE Car_Detail.PO_ID = '".$code."' ";
  // echo "<pre>".print_r($make,1)."</pre>";
  $stmt = sqlsrv_query( $connMS, $sql );
  if(sqlsrv_has_rows($stmt)) {
    $row = sqlsrv_fetch_array($stmt);
    return $row;
    exit();
  }  
}

function getSubdistrictBycode($code){
  global $connMS; 
  $resultarray = array();
  $sql = "SELECT * FROM [iBrokerADB].[dbo].[Subdistrict]  WHERE Subdistrict_ID = '".$code."' ";
  // echo "<pre>".print_r($make,1)."</pre>";
  $stmt = sqlsrv_query( $connMS, $sql );
  if(sqlsrv_has_rows($stmt)) {
    $row = sqlsrv_fetch_array($stmt);
    return $row["Subdistrict_Name_TH"];
    exit();
  }  
}

function getDistrictBycode($code){
  global $connMS; 
  $resultarray = array();
  $sql = "SELECT * FROM [iBrokerADB].[dbo].[District]  WHERE District_ID = '".$code."' ";
  // echo "<pre>".print_r($make,1)."</pre>";
  $stmt = sqlsrv_query( $connMS, $sql );
  if(sqlsrv_has_rows($stmt)) {
    $row = sqlsrv_fetch_array($stmt);
    return $row["District_Name_TH"];
    exit();
  }  
}

function getOperationType($id){
  global $connMS; 
  $resultarray = array();
  $sql = "SELECT * FROM [iBrokerADB].[dbo].[Operation_Type]  WHERE Operation_Type_ID = '".$id."' ";
  // echo "<pre>".print_r($make,1)."</pre>";
  $stmt = sqlsrv_query( $connMS, $sql );
  if(sqlsrv_has_rows($stmt)) {
    $row = sqlsrv_fetch_array($stmt);
    return $row["Operation_Type_Desc"];
    exit();
  }  
}


function getProvinceBycode($code){
  global $connMS; 
  $resultarray = array();
  $sql = "SELECT * FROM [iBrokerADB].[dbo].[Province]  WHERE Province_ID = '".$code."' ";
  // echo "<pre>".print_r($make,1)."</pre>";
  $stmt = sqlsrv_query( $connMS, $sql );
  if(sqlsrv_has_rows($stmt)) {
    $row = sqlsrv_fetch_array($stmt);
    return $row["Province_Name_TH"];
    exit();
  }  
}

function caseTitle($name){
  switch ($name) {
    case "Customercode": return "รหัส"; break;
    case "CustomerOperation": return "การดำเนินการ"; break;
    case "Customertype": return "ประเภทลูกค้า"; break;
    case "Customernameth": return "ชื่อ-นามสกุล TH"; break;
    case "Customernameen": return "ชื่อ-นามสกุล EN"; break;
    case "cusFolder": return "เลขบัตรประชาชน / เลขทะเบียนผู้ประกอบการ"; break;
    case "cusPerson": return "ชื่อผู้ติดต่อ"; break;
    case "cusTel": return "โทรศัพท์"; break;
    case "cusFax": return "โทรสาร"; break;
    case "cusMobile": return "มือถือ"; break;
    case "cusEmil": return "eMail"; break;
    case "cusMobileI": return "Mobile I"; break;
    case "cusMobileII": return "Mobile II"; break;
    case "cusLand": return "Land Line"; break;
    case "cusLind": return "Line ID"; break;
    case "cusAddr": return "ที่อยู่"; break;
    case "cusAddSend": return "ที่อยู่ในการส่งเอกสาร"; break;
    case "cusProperties": return "รายการเอาประกัน"; break;
    case "carPlate": return "เลขทะเบียน"; break;
    case "carMake": return "ยี่ห้อรถ"; break;
    case "carModel": return "รุ่น"; break;
    case "carCodes": return "รหัสรถ"; break;
    case "carsubModel": return "รุ่นย่อย/ปี"; break;
    case "carbodyType": return "ประเภทตัวถัง"; break;
    case "carFrame": return "เลขตัวถัง"; break;
    case "carAccessory": return "อุปกรณ์ตกแต่ง"; break;
    case "carSeat": return "ที่นั่ง/ซีซี/น้ำหนัก"; break;
    case "carRepair": return "ประเภทการซ่อม"; break;
    case "carNoOf": return "จำนวนผู้ขับขี่"; break;
    case "carDriver": return "ผู้ขับขี 1"; break;
    case "carCoDriver": return "ผู้ขับขี 2"; break;
    case "carBeneficiary": return "ผู้รับผลประโยชน์"; break;
    case "carPOAddrSend": return "ที่อยู่ในการจัดส่งเอกสาร"; break;
    
    case "filecarback": return "รูปรถด้านท้าย"; break;
    case "filecarleft": return "รูปรถด้านซ้าย"; break;
    case "filecarright": return "รูปรถด้านขวา"; break;
    case "filecarfront": return "รูปรถด้านหน้า"; break;
    case "filecarback": return "รูปรถด้านท้าย"; break;
    case "filecarbook": return "สำเนารถ"; break;
    case "filecardriver": return "ใบขับบี่"; break;
    case "filecompany": return "หนังสือรับรองบริษัท"; break;
    case "filecompanycard": return "บัตร ปชช. กรรมการ"; break;
    case "filereCheck1": return "หลักฐานประกอบตรวจ 1"; break;
    case "filereCheck2": return "หลักฐานประกอบตรวจ 2"; break;
    case "filereCheck3": return "หลักฐานประกอบตรวจ 3"; break;
    case "filereCheck4": return "หลักฐานประกอบตรวจ 4"; break;
    case "filecarall1": return "เอกสารอื่นๆ 1"; break;
    case "filecarall2": return "เอกสารอื่นๆ 2"; break;
    case "filecarall3": return "เอกสารอื่นๆ 3"; break;
    case "filecarall4": return "เอกสารอื่นๆ 4"; break;
    
    case "insurPackageName": return "แพจเกต"; break;
    case "insurTypeIns": return "ประกันชั้น"; break;
    case "insurNameInsur": return "บรัษัท"; break;
    case "insurCost": return "ทุนประกัน"; break;
    case "insurNetpremium": return "เบี้ยสุทธิ"; break;
    case "insurPremium": return "เบี้ยรวมภาษี"; break;
    case "insurDiscount": return "ส่วนลด"; break;
    case "insurTaxamount": return "เบี้ยรวมภาษี"; break;
    case "insurCoverageItem": return "คุ้มครอง"; break;
    case "insurCoverageStart": return "เริ่มวันคุ้มครอง"; break;
    case "insurCoverageEnd": return "สิ้นสุดวันคุ้มครอง"; break;
    case "insurPOAddrSendRemark": return "หมายเหตุการจัดส่งเอกสาร"; break;
    
  }
}

function setStatus($name){
  switch ($name) {
    case "0": return "<b style='color: #2457ff;'>รอตรวจ</b>"; break;
    case "1": return "<b style='color: #f44;'>ไม่ผ่าน</b>"; break;
    case "2": return "<b style='color: #ff8000;'>รอแจ้งงาน</b>"; break;
    case "3": return "<b style='color: #c600c4;'>แจ้งงาน</b>"; break;
    case "4": return "<b style='color: #00ac0a;'>เสร็จ</b>"; break;
  }
}
function getAdminFromInsurer($insurer){
    global $conn2; 
    $rs = $conn2->GetROW( "SELECT insurer_admin FROM ck_insurer WHERE insurer_initials ='".$insurer."' ");
    return $rs["insurer_admin"];   
}

function getNotiWorkCheck($num, $po){
  global $conn2;
  $noti_work_chk = $conn2->GetROW( "SELECT * FROM noti_work_chk WHERE noti_work_code ='".$num."' AND po_id ='".$po."' ORDER BY datetime DESC LIMIT 1 ");
  return $noti_work_chk;   
}

function getInsuranceType(){
  global $connMS; 
  $resultarray = array();
  $sql = "SELECT Insurance_Name FROM [iBrokerADB].[dbo].[Insurance] WHERE Insurance_Group_ID = 'MT' ";
  // echo "<pre>".print_r($make,1)."</pre>";
  $stmt = sqlsrv_query( $connMS, $sql );
  if(sqlsrv_has_rows($stmt)) {
      while( $row = sqlsrv_fetch_array($stmt) ) { 
        $resultarray[] = $row;
      }
      return $resultarray;
      exit();
  } 
}
function getCoverageItem($id){
  global $connMS; 
  $resultarray = array();
  $sql = "SELECT Purchase_Order_Coverage_Item.Coverage_Item_ID, Purchase_Order_Coverage_Item.Coverage_Value, Purchase_Order_Coverage_Item.Order_Index,
  Coverage_Item.Coverage_Item_Desc, Coverage_Item.Coverage_Item_Unit
  FROM [iBrokerADB].[dbo].[Purchase_Order_Coverage_Item] 
  INNER JOIN [iBrokerADB].[dbo].[Coverage_Item] ON Purchase_Order_Coverage_Item.Coverage_Item_ID = Coverage_Item.Coverage_Item_ID
  WHERE Purchase_Order_Coverage_Item.POID = '".$id."' 
  ORDER BY Purchase_Order_Coverage_Item.Order_Index ASC";
  // echo "<pre>".print_r($make,1)."</pre>";
  $stmt = sqlsrv_query( $connMS, $sql );
  if(sqlsrv_has_rows($stmt)) {
      while( $row = sqlsrv_fetch_array($stmt) ) { 
        $resultarray[] = $row;
      }
      return $resultarray;
      exit();
  } 
}

function getInsurerByAdmin($code){
  global $conn2; 
  $rs = $conn2->GetAll( "SELECT * FROM ck_insurer WHERE insurer_admin ='".$code."' ");
  return $rs; 
}

function getImageEdit($code, $file_type){
  global $conn2; 
  $rs = $conn2->GetROW( "SELECT file_name, name_types FROM noti_work_files WHERE noti_work_code ='".$code."' AND file_type ='".$file_type."' ");
  return $rs;
}

function getSendPOByAdmin($code){
  global $conn2; 
  $arr = array();
  $getInsurerByAdmin = getInsurerByAdmin($code);
  if($code == "All"){
    $sql = "SELECT *,ck_personnel.personnel_firstname AS chk_name, ck_personnel.personnel_lastname AS chk_lname 
      FROM noti_work LEFT JOIN ck_personnel ON noti_work.chk_code = ck_personnel.personnel_code
      WHERE noti_work.enable='1' AND noti_work.status IN ('2','3')
      ORDER BY noti_work.created_date ASC LIMIT 0,300";
      $arr = $conn2->GetAll( $sql);
  }else{
    foreach ($getInsurerByAdmin as $key => $value) {
      $sql = "SELECT *,ck_personnel.personnel_firstname AS chk_name, ck_personnel.personnel_lastname AS chk_lname 
      FROM noti_work LEFT JOIN ck_personnel ON noti_work.chk_code = ck_personnel.personnel_code
      WHERE noti_work.insuere_company ='".$value["insurer_initials"]."' AND noti_work.enable='1' AND noti_work.status IN ('2','3')
      ORDER BY  noti_work.created_date ASC LIMIT 0,300";
      $rs = $conn2->GetAll( $sql);
      foreach ($rs as $key => $valRS) {
        array_push($arr, $valRS);
      }
    }
  }
  return $arr;
}


function getPersonnelByType($types){
	global $conn2; 
  if($types){
    $sql = "SELECT * FROM ck_personnel WHERE personnel_type ='".$types."' AND personnel_status = '1' ORDER BY personnel_code";
  }else{
    $sql = "SELECT * FROM ck_personnel WHERE personnel_status = '1' ORDER BY personnel_code";
  }
  $rs = $conn2->GetAll($sql);
  return $rs;

}

function getCommentLists(){
	global $conn2; 
  $rs = $conn2->GetAll( "SELECT * FROM comment_lists ORDER BY comment_lists_id ");
  return $rs;

}

function getCommentStatus(){
	global $conn2; 
  $rs = $conn2->GetAll( "SELECT * FROM comment_status ORDER BY comment_status_id ");
  return $rs;
}

function getNotiCommentByAddto($code){
  global $conn2; 
  $rs = $conn2->GetAll( "SELECT * FROM noti_comment INNER JOIN noti_work ON noti_comment.noti_work_id = noti_work.noti_work_id  WHERE noti_comment.comment_to = '".$code."' ORDER BY noti_comment.datetime DESC LIMIT 0,100");
  return $rs;

}

function getFollowupBydate($date, $user){
  global $connMS; 
  $resultarray = array();
  $datenext = date("Y-m-d", strtotime($date . "+1 days"));
  $sql = "SELECT Followup.Followup_ID, Followup.Followup_Detail, Followup.Compare_Policy_ID, Followup.Followup_DateTime, 
  Remind.Remind_Date, Followup_Status.Followup_Status_Desc, Followup_Type.Followup_Type_Desc, Customer.Customer_id , Customer.Customer_FName, Customer.Customer_LName, 
  Customer.Tel_No, Customer.Mobile_No, Customer.EMail,Remind_Method.Remind_Method_ID,Remind_Method.Remind_Method_Desc
  FROM [iBrokerADB].[dbo].[Followup] 
  INNER JOIN [iBrokerADB].[dbo].[Followup_Status] ON Followup.Followup_Status_ID = Followup_Status.Followup_Status_ID
  INNER JOIN [iBrokerADB].[dbo].[Followup_Type] ON Followup.Followup_Type_ID = Followup_Type.Followup_Type_ID
  INNER JOIN [iBrokerADB].[dbo].[Customer] ON Followup.Customer_ID = Customer.Customer_ID
  LEFT JOIN [iBrokerADB].[dbo].[Remind] ON Followup.Followup_ID = Remind.Remark
  LEFT JOIN [iBrokerADB].[dbo].[Remind_Method] ON Remind.Remind_Method_ID = Remind_Method.Remind_Method_ID
  WHERE Remind.Remind_Date >= '".$date."' AND Remind.Remind_Date < '".$datenext."' AND  Followup.Active = 'Y' ";
  if($user != "admin"){
    $sql .= " AND Followup.Followup_By = '".$user."' "; 
  }  
  $sql .= " ORDER BY Remind.Remind_Date ASC"; 
  // echo $sql;
  // echo "<pre>".print_r($make,1)."</pre>";
  $stmt = sqlsrv_query( $connMS, $sql );
  if(sqlsrv_has_rows($stmt)) {
      while( $row = sqlsrv_fetch_array($stmt) ) { 
        $resultarray[] = $row;
      }
      return $resultarray;
      exit();
  } 
}

function getCountPOAllDate($date, $user){
  global $connMS; 
    $resultarray = array();
    $datestart = $date;
    $dateNow = date("Y-m-d", strtotime($datestart . "+1 days"));
    $sql = "SELECT count(PO_ID) As count, sum(Premium_After_Disc) AS sumTotal
    FROM [iBrokerADB].[dbo].[Purchase_Order]
    WHERE PO_Date >= '".$datestart."' AND PO_Date < '".$dateNow."' AND  Active = 'Y' ";
    if($user != "admin"){
      $sql .= " AND Employee_ID = '".$user."' "; 
    }  
    $stmt = sqlsrv_query( $connMS, $sql );
    if(sqlsrv_has_rows($stmt)) {
      $row = sqlsrv_fetch_array($stmt);
      return $row;
      exit();
    }
}
function getCountPOStatus($user, $status){
  global $connMS; 
    $resultarray = array();
    $datestart = date("Y-m")."-01";
    $dateNow = date("Y-m-d");
    $sql = "SELECT count(PO_ID) As count, sum(Premium_After_Disc) AS sumTotal
    FROM [iBrokerADB].[dbo].[Purchase_Order]
    WHERE PO_Date >= '".$datestart."' AND PO_Date < '".$dateNow."' AND  Active = 'Y' ";
    if($user != "admin"){
      $sql .= " AND Employee_ID = '".$user."' "; 
    }
    if($status){
      if($status == "CBE"){
        $sql .= " AND Status_ID IN ('CBE','CBW','REJ')  ";
      }else if($status == "OPN"){
        $sql .= " AND Status_ID IN ('OPN','IPC','IPI', 'IPR', 'NEW', 'RCA', 'PLC', 'PPS')  ";
      }else{
        $sql .= " AND Status_ID = '".$status."' ";
      }
    } 
    // if($Finance){
    //   $sql .= " AND Finance_Status_ID = '".$Finance."' ";
    // }
    // echo $sql;
    $stmt = sqlsrv_query( $connMS, $sql );
    if(sqlsrv_has_rows($stmt)) {
      $row = sqlsrv_fetch_array($stmt);
      return $row;
      exit();
    }
}

function getCPbyPhone($phone){
  global $connMS;
  $resultarray = array();
  $sql = "SELECT Compare_Policy.CP_PO_ID, Compare_Policy.CP_Date, Compare_Policy.CP_ID, Compare_Policy.Compare_Status_ID, Compare_Policy.Active, Customer.Customer_ID, Customer.Customer_FName, Customer.Customer_LName, Customer.Tel_No, My_User.User_FName, My_User.User_LName, Customer.Mobile_No
  FROM [iBrokerADB].[dbo].[Compare_Policy] 
  INNER JOIN [iBrokerADB].[dbo].[Customer] ON Compare_Policy.Customer_ID = Customer.Customer_ID  
  INNER JOIN [iBrokerADB].[dbo].My_User ON Compare_Policy.Employee_ID = My_User.User_ID 
  WHERE Customer.Mobile_No LIKE '%".$phone."%' OR Customer.Tel_No LIKE '%".$phone."%'  ";
  // echo $sql;
  $stmt = sqlsrv_query( $connMS, $sql );
  if(sqlsrv_has_rows($stmt)) {
      while( $row = sqlsrv_fetch_array($stmt) ) { 
        $resultarray[] = $row;
      }
      return $resultarray;
      exit();
  } 
}