<?php
session_start();

if (!isset($_SESSION["User"]['UserCode'])) {
	echo "<pre>".print_r($_SESSION,1)."</pre>";
	exit;
    echo "<script>alert('กรุณาเข้าสู่ระบบ!!!');</script>";
	echo '<META http-equiv="refresh" content="0;URL=login.php">';
	exit();
}

include "include/inc_header.php";
include "include/inc_menu.php";

$txtse = ($_GET["txtse"]) ? $_GET["txtse"] : date("n");
// $getNotiComment = getNotiCommentByAddto($UserCode);
$dateNow               = date("Y-m-d");
$dateNext              = date("Y-m-d", strtotime($date . "+1 days"));
$getFollowupBydate     = getFollowupBydate($dateNow, $UserCode);
$getFollowupBydateNext = getFollowupBydate($dateNext, $UserCode);

$getCountPOStatus      = getCountPOReportforIndex($txtse);
$getReportFollowByUser = getReportFollowByUser($UserCode);
$getNoticeForindex = getNoticeForindex();

echo "xxxxxxxxxxxxxxxxx".$UserCode = $_SESSION["User"]['UserCode'];
?>
<div class="main">
	<div class="main-content">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-3">
					<div class="panel panel-scrolling" style="background-color: #fffdee;">
						<div class="panel-heading"><h3 class="panel-title">นัดหมาย วันนี้</h3></div>
						<div class="panel-body">
							<?php
							if ($getFollowupBydate) {
								$i = 1;
								foreach ($getFollowupBydate as $key => $value) {
									?>
									<div class="notiBoxIndex blogBox_1  moreBox_1 <?php if ($i++ > 15) {echo "dn";}?>" >
										<div>
											<span class="fs10" style="color: #2457ff;"><?php echo " <a class='fs14 cff2da5 fwb' href='offeredits.php?cpid=" . $value["Compare_Policy_ID"] . "' target='_bank'>" . $value["Compare_Policy_ID"] . "</a> | " . $value["Remind_Date"]->format("Y-m-d h:i") . " | <b>นัดหมายครั้งถัดไป : </b> " . $value["Remind_Date"]->format("Y-m-d h:i") ?></span>
										</div>
										<div><sapn class="cf40053 fs12"><a class="cf40053 fs12" href='offeredits.php?cpid=".$value["Compare_Policy_ID"]."' target='_bank'><?php echo $value["Followup_Detail"]; ?></a> </sapn></div>
										<div><span class="fs10"><?php echo " <b>ประเภท: </b> " . $value["Followup_Type_Desc"] . " | <b>สถานะ: </b> " . $value["Followup_Status_Desc"] . " | <b>ผ่าน : </b> " . $value["Remind_Method_Desc"] ?></span> </div>
										<div>
											<span class="fs10"><?php echo " <b>ลุกค้า: </b> " . $value["Customer_FName"] . " " . $value["Customer_LName"] . " | " . $value["Tel_No"] ?>
											<?php if ($value["EMail"]) {?>
												<?php echo " | " . $value["EMail"] ?></span>
											<?php }?>
										</span>
									</div>
								</div>
							<?php }?>
							<div class="clearfix mt15">
								<button type="button" class="btn btn-primary btn-bottom center-block loadMore" id="loadMore_1" onclick="btnLoadMore(1)">เพิ่มเติม</button>
							</div>
						<?php }?>
					</div>
				</div>
			</div>
			<div class="col-lg-3">
				<div class="panel panel-scrolling" style="background-color: #e5f7fe;">
					<div class="panel-heading"><h3 class="panel-title">นัดหมาย พรุ่งนี้</h3></div>
					<div class="panel-body">
						<?php
						if ($getFollowupBydateNext) {
							$i = 1;
							foreach ($getFollowupBydateNext as $key => $value) {
								?>
								<div class="notiBoxIndex blogBox_2  moreBox_2 <?php if ($i++ > 15) {echo "dn";}?>" >
									<div>
										<span class="fs10" style="color: #2457ff;"><?php echo " <a class='fs14 cff2da5 fwb' href='offeredits.php?cpid=" . $value["Compare_Policy_ID"] . "' target='_bank'>" . $value["Compare_Policy_ID"] . "</a> | " . $value["Remind_Date"]->format("Y-m-d h:i") . " | <b>นัดหมายครั้งถัดไป : </b> " . $value["Remind_Date"]->format("Y-m-d h:i") ?></span>
									</div>
									<div><sapn class="cf40053 fs12"><a class="cf40053 fs12" href='offeredits.php?cpid=".$value["Compare_Policy_ID"]."' target='_bank'><?php echo $value["Followup_Detail"]; ?></a> </sapn></div>
									<div><span class="fs10"><?php echo " <b>ประเภท: </b> " . $value["Followup_Type_Desc"] . " | <b>สถานะ: </b> " . $value["Followup_Status_Desc"] . " | <b>ผ่าน : </b> " . $value["Remind_Method_Desc"] ?></span> </div>
									<div>
										<span class="fs10"><?php echo " <b>ลุกค้า: </b> " . $value["Customer_FName"] . " " . $value["Customer_LName"] . " | " . $value["Tel_No"] ?>
										<?php if ($value["EMail"]) {?>
											<?php echo " | " . $value["EMail"] ?></span>
										<?php }?>
									</span>
								</div>
							</div>
						<?php }?>
						<div class="clearfix mt15">
							<button type="button" class="btn btn-primary btn-bottom center-block loadMore" id="loadMore_2" onclick="btnLoadMore(2)">เพิ่มเติม</button>
						</div>
					<?php }?>
				</div>
			</div>
		</div>
		<div class="col-lg-3">
			<div class="panel panel-scrolling" style="background-color: #ffe8e8;">
				<div class="panel-heading"><h3 class="panel-title">นัดหมายภายในเดือน</h3></div>
				<div class="panel-body">
					<?php $i = 1;
					foreach ($getReportFollowByUser as $key => $value) {

						?>
						<div class="notiBoxIndex blogBox_3  moreBox_3 " >
							<div><?php echo $value["RemindDate"] ?> <span class="fs14 cff2da5 fwb">[ <?php echo $value["countFU"] ?> ]</span></div>
						</div>
					<?php }?>
				</div>
			</div>
		</div>
		<div class="col-lg-3">
			<div class="panel panel-scrolling" style="background-color: #ffdff7;">
				<div class="panel-heading"><h3 class="panel-title">ประกาศ</h3></div>
				<div class="panel-body">
					<?php if($getNoticeForindex){
							foreach ($getNoticeForindex as $key => $value) {
					?>
						<div class="notiBoxIndex blogBox_3  moreBox_3 " >
							<div>
								<div class="c2457ff fs16">ประกาศ ณ วันที่ <?php echo $value["notice_date"] ?> </div>
								<span class="fs12 "><?php echo $value["notice_detail"] ?> </span>
							</div>
						</div>
					<?php }
						}?>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="panel panel-headline">
			<div class="panel-heading">
				<h3 class="panel-title dib fl">รายงานยอดขาย <?php echo "ปี " . date("Y"); ?></h3>
				<input type="hidden" value="<?php echo $txtse; ?>" id="seMounth">
				<div class="col-md-4">
					<span class="fs18 cff2da5 mr15">เดือน</span>
					<select name="mounth" id="mounthOpt" class="form-control formInput2 dib" style="width: 80%" >
						<option value="">:: กรุณาเลือก ::</option>
						<?php for ($i = 1; $i <= date("n"); $i++) {?>
							<option value="<?php echo $i; ?>" <?php if ($txtse == $i) {echo "selected";}?>>
								<?php echo date("F", strtotime(date("Y") . "-" . $i . "-01")); ?>
							</option>
						<?php }?>
					</select>
				</div>
				<!-- </div> -->
			</div>
			<div class="panel-body clearb">
				<div class="row">
					<div class="col-md-2">
						<div class="metric">
							<span><a href="reportlists.php?Operation=N&txtse=<?php echo $txtse; ?>" target="_bank" class="cff2da5 fwb">งานใหม่</a></span>
							<p>
								<span class="number fs15"><?php echo number_format($getCountPOStatus["sumNewCount"]); ?> / <?php echo number_format($getCountPOStatus["sumNew"], 2); ?></span>
								<span class="title">จำนวน/เงิน(รวมยกเลิก)</span>
							</p>
						</div>
					</div>
					<div class="col-md-2">
						<div class="metric">
							<span ><a href="reportlists.php?Operation=R&txtse=<?php echo $txtse; ?>" target="_bank" class="cff2da5 fwb">ต่ออายุ</a></span>
							<p>
								<span class="number fs15"><?php echo number_format($getCountPOStatus["sumRenewCount"]); ?> / <?php echo number_format($getCountPOStatus["sumRenew"], 2); ?></span>
								<span class="title">จำนวน/เงิน(รวมยกเลิก)</span>
							</p>
						</div>
					</div>
					<div class="col-md-2">
						<div class="metric">
							<span >ลดหนี้</span>
							<p>
								<span class="number fs15"><?php echo number_format($getCountPOStatus["sumCCount"]); ?> / <?php echo number_format($getCountPOStatus["sumC"], 2); ?></span>
								<!-- <span class="number fs15"><span id="totalCount"></span>/<span id="totalPrice"></span></span> -->
								<span class="title">จำนวน/เงิน</span>
							</p>
						</div>
					</div>
					<div class="col-md-2">
						<div class="metric">
							<span >เพิ่มหนี้</span>
							<p>
								<span class="number fs15"><?php echo number_format($getCountPOStatus["sumDCount"]); ?> / <?php echo number_format($getCountPOStatus["sumD"], 2); ?></span>
								<span class="title">จำนวน/เงิน</span>
							</p>
						</div>
					</div>
					<div class="col-md-2">
						<div class="metric">
							<span >กำลังดำเนินการ</span>
							<p>
								<span class="number fs15"><?php echo number_format($getCountPOStatus["sumPayingCount"]); ?> / <?php echo number_format($getCountPOStatus["sumPaying"], 2); ?></span>
								<span class="title">จำนวน/เงิน</span>
							</p>
						</div>
					</div>
					<div class="col-md-2">
						<div class="metric">
							<span >รวมยอดขาย</span>
							<?php
							$caseAllCount = $getCountPOStatus["sumNewCount"] + $getCountPOStatus["sumRenewCount"];
							$caseAllPrice = $getCountPOStatus["sumNew"] + $getCountPOStatus["sumRenew"];
							?>
							<p>
								<span class="number fs15"><?php echo number_format($caseAllCount); ?> / <?php echo number_format($caseAllPrice, 2); ?></span>
								<span class="title">จำนวน/เงิน</span>
							</p>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="p15"><div id="headline-chart" class="ct-chart"></div></div>
					</div>
					<div class="col-md-12">
						<canvas id="canvas" height="80"></canvas>
					</div>
				</div>

			</div>
		</div>
	</div>
</div>
</div>
</div>
<?php include "include/inc_footer.php";

echo "<pre>".print_r($_SESSION,1)."</pre>";
?>


<!-- <script src="assets/vendor/chartist/dist/chartist-plugin-axistitle.min.js"></script> -->
<script src="js/Chart.bundle.js"></script>
<script src="js/utils.js"></script>
<script>

	$("#mounthOpt").on('change', function() {
		if(this.value){
			window.location.href = "index.php?txtse="+this.value;
		}
	});

	Chart.plugins.register({
		afterDatasetsDraw: function(chart) {
			var ctx = chart.ctx;
			chart.data.datasets.forEach(function(dataset, i) {
				var meta = chart.getDatasetMeta(i);
				if (!meta.hidden) {
					meta.data.forEach(function(element, index) {
						ctx.fillStyle = 'rgba(125, 125, 125, 1)';

						var fontSize = 10;
						var fontStyle = 'normal';
						var fontFamily = 'Arial';
						ctx.font = Chart.helpers.fontString(fontSize, fontStyle, fontFamily);
						var dataString = dataset.data[index].toString();
						ctx.textAlign = 'center';
						ctx.textBaseline = 'middle';

						var padding = 10;
						var position = element.tooltipPosition();
						ctx.fillText(dataString, position.x, position.y - (fontSize / 2) - padding);
					});
				}
			});
		}
	});
	window.onload = function() {
		var ctx = document.getElementById('canvas').getContext('2d');
		var seriescount = [];
		var code = "<?php echo $_SESSION["User"]['UserCode']; ?>";
		var data, options;
		var totalCount = 0;
		var totalPrice = 0.00;
		var mounth = $("#seMounth").val();
		$.ajax({
			url: 'include/inc_action_ms.php',
			type:'POST',
			dataType: 'json',
			data: {action: 'getDatePO', code:code, mounth:mounth},
			success:function(rs){
				var labelArray = [];
				var series = [];
				for (var i in rs){
					var date = new Date(rs[i].PO_Date.date);
					var day = ("0" + date.getDate()).slice(-2);
					labelArray.push(day);
					series.push(rs[i].sumTotal);
					// totalPrice = totalPrice+rs[i].sumTotal;
				}
				// console.log(series);
				// console.log(totalPrice.toFixed(2));
				var color = Chart.helpers.color;
				var config = {
					type: 'line',
					data: {
						labels: labelArray,
						datasets: [{
							label: 'จำนวน(บ)',
							data: series,
							backgroundColor: color(window.chartColors.orange).alpha(0.2).rgbString(),
							borderColor: window.chartColors.orange,
							fill: true,
							pointRadius: 8,
							pointHoverRadius: 15,
						}]
					},
					options: {
						responsive: true,
						legend: {
							position: 'bottom',
						},
						tooltips: {
							mode: 'index',
							intersect: false,
						},
						hover: {
							mode: 'nearest',
							intersect: true
						},
						elements: {
							point: {
								pointStyle: 'rectRot'
							}
						},
						scales: {
							xAxes: [{
								display: true,
								scaleLabel: {
									display: true,
									labelString: 'วันที่'
								}
							}],
							yAxes: [{
								display: true,
								scaleLabel: {
									display: true,
									labelString: 'จำนวน'
								}
							}]
						},
						title: {
							display: true,
							text: 'รายงานยอดขาย <?php echo date("F", strtotime($txtse)) . " " . date("Y") ?>'
						}
					}
				};
				window.myLine = new Chart(ctx, config);
			}
		});

	};



	function btnLoadMore(id){
		loadMore = $('.loadMore').attr('id');
		if(id!=3){
			sh = 15;
		}else{
			sh = 10;
		}
		$(".moreBox").slice(0, sh).show();
		if ($(".blogBox:hidden").length != 0) {
			$("#loadMore").show();
		}


		$(".moreBox_"+id+":hidden").slice(0, sh).slideDown();
		if ($(".moreBox_"+id+":hidden").length == 0) {
			$("#loadMore_"+id).fadeOut('slow');
		}
	}
</script>