<?php 

session_start();
if(!isset($_SESSION["User"]['UserCode'])){
	echo "<script>alert('กรุณาเข้าสู่ระบบ!!!');</script>";
	echo '<META http-equiv="refresh" content="0;URL=login.php">';
	exit();
}
if(!$_SESSION["User"]['type'] == "SuperAdmin" || !$_SESSION["User"]['type'] == "QualityControl"  ){
	echo '<META http-equiv="refresh" content="0;URL=sendorders.php">';
	exit();
}
header('Content-Type: text/html; charset=utf-8');
include "include/inc_header.php"; 
include "include/inc_menu.php"; 

$getCommentLists = getCommentLists();
$getPersonnelAdmin = getPersonnelByType("Admin");
// echo "<pre>".print_r($getPersonnelAdmin,1)."</pre>";
?>
<div class="main">
	<div class="main-content p20">
		<div class="p20">
				<div class="row">
					<div class="col-md-12">
						<div class="panel">
								<div class="panel-heading">
									<h4 class="panel-title"><b>ตั้งค่า Comment</b></h4>
								</div>
								<div class="panel-body fs14">
									<div class="row">
										<div class="col-md-12">
											<h4>เพิ่มข้อมูล</h4>
											<select name="chk_code" id="chk_code">
												<option value=""></option>
											</select>
											<div class="col-md-2" onclick="btnAdd()"><button class="btn btn-info">เพิ่มข้อมูล</button></div>
										</div>
									</div>
									<div class="row mt20">
										<div class="col-md-12">
											<table class="table">
												<thead>
													<tr>
														<th  class="t_c" style="width: 5%;">ลำดับ</th>
														<th class="t_c">รายการ</th>
														<th class="t_c" style="width: 10%;">โดย</th>
														<th class="t_c" style="width: 10%;">เวลา</th>
														<th class="t_c" style="width: 10%;">ใช้งาน</th>
														<th class="t_c" style="width: 5%;">ลบ</th>
													</tr>
												</thead>
												<tbody>
													<?php $i=1; foreach ($getCommentLists as $key => $value) { ?>
														<tr>
															<td class="t_c"><?php echo $i++;?></td>
															<td><?php echo $value["commentlists"];?></td>
															<td class="t_c" ><?php echo $value["post_by"];?></td>
															<td class="t_c" ><?php echo $value["datetime"];?></td>
															<td class="t_c"><input type="checkbox" onclick="fnUpdate('<?php echo $value["comment_lists_id"];?>');" id="status_<?php echo $value["comment_lists_id"] ?>" name="status" <?php echo ($value["enable"] == 1) ? "checked":"" ;?>></td>
															<td class="t_c"><a href="javascript:MyFunction();" style="color: #f00;" onclick="btnDel('<?php echo $value["comment_lists_id"];?>')"><i class="fa fa-trash"></i></a></td>
														</tr>
													<?php } ?>
												</tbody>
											</table>
										</div>
									</div>
								</div>
						</div>
					</div>
				</div>
		</div>
	</div>
</div>
<?php include "include/inc_footer.php"; ?> 
<script type="text/javascript">
	function btnAdd() {
		comment = $("#comment").val();
		if(comment){
			if(confirm("ต้องการเพิ่มข้อมูล?")){
				$.ajax({ 
					url: 'include/inc_action_chk.php',
					type:'POST',
					data: {action: 'addCommentList', commentlists:comment},
					success:function(rs){
						if(rs){
							alert("เพิ่มข้อมูลสำเร็จ");
							window.location.reload(true);
						}else{
							alert("ไม่สามารถทำรายได้ กรุณาลองใหม่");
						}
					}
				});
			}
		}else{
			alert("กรุณากรอกข้อมูล");
		}
	}
	function fnUpdate(id) {
		if($('#status_'+id).is(':checked')){ 
			enable = "1";
		}else{
			enable = "0";
		}
		$.ajax({ 
			url: 'include/inc_action_chk.php',
			type:'POST',
			data: {action: 'updateCommentList', comment_lists_id:id, enable:enable},
			success:function(rs){ 
			}
		});
	}
	function btnDel(id) {
		if(confirm("ต้องการลบข้อมูล")){
			$.ajax({ 
				url: 'include/inc_action_chk.php',
				type:'POST',
				data: {action: 'delCommentList', id:id},
				success:function(rs){ 
					alert("ทำรายการสำเร็จ");
					window.location.reload(true);
				}
			});
		}
	}
</script>