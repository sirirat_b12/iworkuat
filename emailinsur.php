<?php 

session_start();
$UserCode = $_SESSION["User"]['UserCode'];
$userType = $_SESSION["User"]['type'];

if(!isset($UserCode)){
	echo "<script>alert('กรุณาเข้าสู่ระบบ!!!');</script>";
	echo '<META http-equiv="refresh" content="0;URL=login.php">';
	exit();
}
header('Content-Type: text/html; charset=utf-8');
include "include/inc_header.php"; 
include "include/inc_menu.php"; 


$getInsurer = getInsurerAll();
if($_GET["insur"]){
	$getEmailInsur = getEmailInsurAll($_GET["insur"]);
}
// echo "<pre>".print_r($getEmailInsur,1)."</pre>";
?>
<div class="main">
	<div class="main-content p20">
		<div class="p20">
				<div class="row">
					<div class="col-md-12">
						<div class="panel">
								<div class="panel-body fs14">
									<div class="row mt20">
										<div class="col-md-1"><h4>บริษัท</h4></div>
										<div class="col-md-3">
											<select name="chk_code" id="chk_code" class="form-control formInput2">
												<option value="">กรุณาเลือก</option>
												<?php foreach ($getInsurer as $key => $value) { ?>
													<option value="<?php echo $value["insurer_initials"] ?>" <?php echo ($value["insurer_initials"]==$_GET["insur"]) ? "selected" : "";  ?> >
														<?php echo $value["insurer_name"] ?>
													</option>
												<?php } ?>
											</select>
										</div>
									</div>
									<div class="row mt20">
										<div class="col-md-12">
											<form action="include/inc_action_chk.php" method="POST" id="frmMoveQC">
												<table class="table table-hover" id="tableMoveQC">
													<thead>
														<tr>
															<th class="t_c">ลำดับ</th>
															<th class="t_c">อีเมล์</th>
															<th class="t_c">ชื่อ-นามสกุล</th>
															<th class="t_c">TO</th>
															<th class="t_c">แจ้งงาน</th>
															<th class="t_c">ยกเลิก</th>
															<th class="t_c">ลบ</th>
														</tr>
													</thead>
													<tbody>
														<?php $i=1;  foreach ($getEmailInsur as $key => $value) { ?>
																<tr >
																	<th class="t_c"><?php echo $i++ ?></th>
																	<td class="t_c"><?php echo $value["email"] ?></td>
																	<td class="t_c"><?php echo $value["name"] ?></td>
																	<td class="t_c">
																		<input type="checkbox" onclick="UpdateCC('<?php echo $value["insur_email_id"];?>');" id="status_cc_<?php echo $value["insur_email_id"] ?>" name="status" <?php echo ($value["setting_cc"] == 1) ? "checked":"" ;?>>
																	</td>
																	<td class="t_c">
																		<input type="checkbox" onclick="fnUpdate('<?php echo $value["insur_email_id"];?>');" id="status_<?php echo $value["insur_email_id"] ?>" name="status" <?php echo ($value["enable"] == 1) ? "checked":"" ;?>>
																	</td>
																	<td class="t_c">
																		<input type="checkbox" onclick="fnUpdateActiveCancel('<?php echo $value["insur_email_id"];?>');" id="cancel_<?php echo $value["insur_email_id"] ?>" name="active_cancel" <?php echo ($value["active_cancel"] == 1) ? "checked":"" ;?>>
																	</td>
																	<td class="t_c">
																		<span class="cursorPoin cf80000" onclick="btnDel('<?php echo $value["insur_email_id"];?>')"><i class="fa fa-trash"></i></span>
																	</td>

																</tr>
														<?php } ?>
													</tbody>
												</table>
											</form>
										</div>
									</div>
									<div class="row mt20 p10 bgbaf4bc">
										<div class="col-md-2">
											<div class="form-group">
											  <label for="subject" class="cf40053">อีเมล์</label>
											  <input type="text" name="email" id="addemail" class="form-control formInput2" >
											</div>
										</div>
										<div class="col-md-2">
											<div class="form-group">
											  <label for="subject" class="cf40053">ชื่อ</label>
											  <input type="text" name="name" id="addname" class="form-control formInput2" >
											</div>
										</div>
										<div class="col-md-1">
												<span class="cursorPoin cff2da5 pt15 dib fwb fs16" onclick="addEmailInsur()">เพิ่มข้อมูล</span>
										</div>
									</div>
								</div>
						</div>
					</div>
				</div>
		</div>
	</div>
</div>
<?php include "include/inc_footer.php"; ?> 
<script type="text/javascript">

$( "#chk_code" ).change(function() {
  var chk_code = $("#chk_code").val();
	if(chk_code){
		 window.location.href = "emailinsur.php?insur="+chk_code;
	}
});

function addEmailInsur() {
	var chk_code = $("#chk_code").val();
	var name = $("#addname").val();
	var email = $("#addemail").val();
	if(name && email){
		$.ajax({ 
			url: 'include/inc_action_chk.php',
			type:'POST',
			data: {action: 'addEmailInsur', chk_code:chk_code, name:name, email:email},
			success:function(rs){ 
				window.location.href = "emailinsur.php?insur="+chk_code;
			}
		})
	}else{
		alert("กรุณากรอก ชื่อ หรืออีเมล์ ให้ถูกต้อง");
	}
}

function fnUpdate(id) {
		if($('#status_'+id).is(':checked')){ 
			enable = "1";
		}else{
			enable = "0";
		}
		$.ajax({ 
			url: 'include/inc_action_chk.php',
			type:'POST',
			data: {action: 'updateStatusEmailInsur', insur_email_id:id, enable:enable},
			success:function(rs){ 
				console.log(rs);
			}
		})
}

function fnUpdateActiveSend(id) {
		if($('#send_'+id).is(':checked')){ 
			enable = "1";
		}else{
			enable = "0";
		}
		$.ajax({ 
			url: 'include/inc_action_chk.php',
			type:'POST',
			data: {action: 'fnUpdateActiveSend', insur_email_id:id, enable:enable},
			success:function(rs){ 
				console.log(rs);
			}
		})
}

function fnUpdateActiveCancel(id) {
		if($('#cancel_'+id).is(':checked')){ 
			enable = "1";
		}else{
			enable = "0";
		}
		// console.log($('#cancel_'+id).val());
		$.ajax({ 
			url: 'include/inc_action_chk.php',
			type:'POST',
			data: {action: 'fnUpdateActiveCancel', insur_email_id:id, enable:enable},
			success:function(rs){ 
				console.log(rs);
			}
		})
}

function UpdateCC(id) {
		if($('#status_cc_'+id).is(':checked')){ 
			enable = "1";
		}else{
			enable = "0";
		}
		console.log(enable);
		$.ajax({ 
			url: 'include/inc_action_chk.php',
			type:'POST',
			data: {action: 'updateStatusCC', insur_email_id:id, enable:enable},
			success:function(rs){ 
				console.log(rs);
			}
		})
}
function btnDel(id) {
	var chk_code = $("#chk_code").val();
	// console.log(chk_code);
	if(confirm("ต้องการลบข้อมูล")){
		$.ajax({ 
			url: 'include/inc_action_chk.php',
			type:'POST',
			data: {action: 'delStatusEmailInsur', id:id},
			success:function(rs){ 
				window.location.href = "emailinsur.php?insur="+chk_code;
			}
		});
	}
}
</script>