<?php 
session_start();
$UserCode = $_SESSION["User"]['UserCode'];
$userType = $_SESSION["User"]['type'];

if(!isset($UserCode)){
	echo "<script>alert('กรุณาเข้าสู่ระบบ!!!');</script>";
	echo '<META http-equiv="refresh" content="0;URL=login.php">';
	exit();
}

header('Content-Type: text/html; charset=utf-8');
include "include/inc_header.php"; 
include "include/inc_menu.php"; 
include "reports/inc_function_report.php";
// include('qrcode/qrcode.class.php');


$getSearchCar = getSearch($_GET["y"], $_GET["m"]);
// echo "<pre>".print_r($getSearchCar,1)."</pre>";

?>
<div class="main">
	<div class="">
		<div class="p20">
			<div class="row">
				<div class="col-md-12">
					<div class="panel">
						<div class="panel-heading">
							<h4 class="panel-title"><b>รายงานการค้นหา</b></h4>
						</div>
						<div class="panel-body fs14">
							<div class="row">
								<div class="col-md-4 ">
									<span class="fs18 cff2da5 mr15">เดือน</span>
									<select name="mounth" id="mounthOpt" class="form-control formInput2 dib" style="width: 80%" >
										<option value="">:: กรุณาเลือก ::</option>
										<?php 
										$Getdate = $_GET["y"]."-".$_GET["m"];
										$dateLast = ( date("Y") - 1)."-".date("m");
										for($i=1; $i<=12; $i++) { 
											$txtM = "+".$i." month";
											$dateForValue = date("Y-m",strtotime($txtM, strtotime($dateLast)));
											?>
											<option value="<?php echo $dateForValue ;?>" <?php if($Getdate ==  $dateForValue){ echo "selected"; } ?>>
												<?php echo date("Y m F",strtotime($dateForValue)) ?>
											</option>
										<?php } ?>
									</select>
								</div>
							</div>
							<div class="row mt20">
								<div class="col-md-12">
									<div class="row">
										<div class="col-md-2"></div><div class="col-md-10"><?php echo $value["PO_ID"]; ?></div>
									</div>
									<table class="table table-hover table-bordered" id="tableSearch">
										<thead>
											<tr class="bgbaf4bc">
												<th class="t_c">ลำดับ</th>
												<th class="t_c">ยี่ห้อ</th>
												<th class="t_c">รุ่นรถ</th>
												<th class="t_c">ปี</th>
												<th class="t_c">การค้นหา</th>
											</tr>
										</thead>
										<tbody id="table_data" class="table-list fs13">
											<?php $i=1; foreach ($getSearchCar as $key => $value) { ?>
												<tr class="table-list fs13" id="table_data">
													<td class="t_c" width="1%"><?php echo $i++; ?></td>
													<td class="t_c"><?php echo $value["make"]; ?></td>
													<td class="t_c"><?php echo $value["model"]; ?></td>
													<td class="t_c"><?php echo $value["year"]; ?></td>
													<td class="t_c"><?php echo $value["search"]; ?></td>
												</tr>
											<?php } ?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php include "include/inc_footer.php"; ?> 
<script src="js/Chart.bundle.js"></script>
<script src="js/utils.js"></script>
<script type="text/javascript">
	$("#mounthOpt").on('change', function() {
		val = this.value.split("-");
		// console.log(val[0]);
		if(this.value){
			window.location.href = "report_search.php?y="+val[0]+"&m="+val[1];
		}

	});

	<?php if($_GET["y"]&&$_GET["m"]){?>
		function getDetailPO(user, date){
			y = <?php echo $_GET["y"]; ?>;
			m = <?php echo $_GET["m"]; ?>;
			if(y){
				window.location.href = "report_search.php?y="+y+"&m="+m+"&user="+user+"&date="+date;
			}
		}
	<?php } ?>
</script>