<?php
session_start();

if (!isset($_SESSION["User"]['UserCode'])) {
    echo "<script>alert('กรุณาเข้าสู่ระบบ!!!');</script>";
    echo '<META http-equiv="refresh" content="0;URL=login.php">';
    exit();
}

include "include/inc_header.php";
include "include/inc_menu.php";
include "adbfiles/inc_function_files.php";
// $apiMain = getAllCustomerMain($sale);
$getFileAll = getFileAllADB();
// echo "<pre>".print_r($getFileAll,1)."</pre>";
// exit();
?>
<div class="main">
	<div class="main-content p20">
		<div class=" bgff">
			<div class="p20">
			<h3 class="text-center">รายชื่อลูกค้า</h3>
				<table id="table"  class="table table-striped p5" data-toggle="table" data-pagination="true" data-search="true"  data-page-size="50" data-page-list="[50, 100, 150, 200, 250, 300]" >
					<thead>
						<tr>
							<th class="t_c">ชื่อ-นามสกุล</th>
							<th class="t_c">ทะเบียนรถ</th>
							<th class="t_c">รูป</th>
						</tr>
					</thead>
					<tbody class="fs12">
						<?php
if ($getFileAll) {
    foreach ($getFileAll as $key => $value) {
        ?>
							<tr>
								
								<td class="t_l"><?php echo $value["adbfiles_name"]; ?></td>
								<td class="t_l"><?php echo $value["adbfiles_plate_no"]; ?></td>
								<td class="text-center">
									<a href="http://61.90.142.230/iadb/adbfile/<?php echo $value["adbfiles_pathfile"]."/".$value["adbfiles_filename"]; ?>" data-magnify="gallery" data-src="" data-group="a"><i class="fas fa-images"></i></a>
								</td>
								
							</tr>
						<?php }
}?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<?php include "include/inc_footer.php";?>
<link href="magnifybox/css/jquery.magnify.css" rel="stylesheet">
<script src="magnifybox/js/jquery.magnify.js"></script>
<script type="text/javascript">

</script>
