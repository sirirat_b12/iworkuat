<?php
header('Content-Type: text/html; charset=utf-8');
date_default_timezone_set("Asia/Bangkok");
ini_set('memory_limit', '2048M');
// include "include/inc_header.php"; 
include "inc_config.php";
include "include/inc_function_noti.php";

include("mpdf/mpdf.php");
include('barcodeClsass/BCGFont.php');
include('barcodeClsass/BCGColor.php');
include('barcodeClsass/BCGDrawing.php'); 
include('barcodeClsass/BCGcode128.barcode.php');


function genBarcode($po,$MyBarCode){
	// $font = new BCGFontFile('./font/Arial.ttf', 18);
	$font = new BCGFont('barcodeClsass/font/Arial.ttf', 10);
	$colorFront = new BCGColor(0, 0, 0);
	$colorBack = new BCGColor(255, 255, 255);
	$color_white = new BCGColor(255, 255, 255); 
	$barcode = $MyBarCode;
	// Barcode Part
	$code = new BCGcode128();
	$code->setScale(1);
	$code->setThickness(30);
	$code->setForegroundColor($colorFront);
	$code->setBackgroundColor($colorBack);
	$code->setFont(0);
	$code->setStart(1);
	$code->setTilde(true);
	$code->parse($barcode);

	// Drawing Part
	$purl = "picBarcode/".$po.".png";
	$drawing = new BCGDrawing($purl, $color_white);
	// $drawing = new BCGDrawing($purl, $color_white);
	$drawing->setBarcode($code);
	$drawing->draw();


	$drawing->finish(BCGDrawing::IMG_FORMAT_PNG);
}



$mpdf = new mPDF('UTF-8','A4','','',15,15,40,30,20,20);
$mpdf->autoScriptToLang = true;


$cp_id = $_GET["cpid"];
$getCPByid = getCPByid($cp_id);
$getPOAll = getPOAll($getCPByid["CP_PO_ID"]);
$getCompareOption = getCompareOption($cp_id);

if(!$getCompareOption){
	echo "<script>alert('ไม่สามารถสร้างใบเตือนได้');</script>";
	echo '<META http-equiv="refresh" content="0;URL=offeredits.php?cpid='.$cp_id.'">';
	exit();
}
// echo "<pre>".print_r($getCompareOption,1)."</pre>";
// exit();
$insurTypes = $getCompareOption["Insurance_ID"];
$sumTotal = ( $getCompareOption["Total_Premium"] + $getCompareOption["Compulsory"] ) - $getCompareOption["Discount"];
$sumTotalDown = $getCompareOption["Total_Premium"] + $getCompareOption["Compulsory"];
$down1 = round( ($sumTotalDown * 40 ) / 100) ;
$down2 = ($sumTotalDown - $down1) / 2;

$pageStyle = 
	'<style>
	.container{
			font-family: thsaraban;
	    font-size: 16px;
	    line-height: 16px;
	}
	table{
		font-family: thsaraban;
	    font-size: 16px;
	    line-height: 15px;
	}
	th{
		padding: 5px;
    font-size: 16px;
    border-bottom: 3px solid #000;
    border-top: 1px solid #000;
    text-align:left;
  }
  td{
		padding: 5px;
	}
	p{
	    text-align: justify;
	}
	h1{
	    text-align: center;
	}
	.fs10{ font-size:10px;}
	.fs12{ font-size:12px;}
	.fs14{ font-size:14px;}
	.fs16{ font-size:16px;}
	.fs18{ font-size:18px;}
	.fwb{font-weight: bold;}
	.t_c{text-align: center;}
	.t_l{text-align: left;}
	.t_r{text-align: right;}
	.mt5{margin-top:5px;}
	.mt10{margin-top:10px;}
	.mt15{margin-top:15px;}
	.mt20{margin-top:20px;}
	.mt30{margin-top:30px;}
	.mt40{margin-top:40px;}
	.mt50{margin-top:50px;}
	.mb5{margin-bottom:5px;}
	.mb10{margin-bottom:10px;}
	.mb15{margin-bottom:15px;}
	.ml5{margin-left:5px;}
	.ml10{margin-left:10px;}
	.ml15{margin-left:15px;}
	.ml20{margin-left:20px;}
	.ml52{margin-left:52px;}
	.m0{margin:0px}
	p{margin:0}
	.p5{padding:5px}
	.p10{padding:10px}
	.pt5{padding-top:5px}
	.pb10{padding-bottom:10px;}
	.bgea{background-color: #eaeaea;}
	.bor_1{border:1px solid #000;}
	.bor_t{border-top:1px solid #000;}
	.bor_l{border-left:1px solid #000;}
	.bor_r{border-right:1px solid #000;}
	.bor_b{border-bottom:1px solid #000;}

	.tableCar th{
		padding: 5px;
    font-size: 16px;
    border-bottom: 1px solid #000;
    border-top: 1px solid #000;
    border-right:1px solid #000;
    text-align:center;
  }
	.tableCar td {
		font-size: 16px;
		border-bottom: 1px solid #000;
		border-right:1px solid #000;
	}
	.tableCar th:last-child, .tableCar td:last-child{ border-right:none;}
	.boxCoverage{
		line-height: 15px;
		float: left; 
		text-align:center; 
		width: 33.20%;
	}
	.fl{float: left; }
	.barcode {
		padding: 1.5mm;
		margin: 0;
		vertical-align: top;
		color: #000000;
		width:50px;
	}
	.carditBox td{padding:10px}
	</style>';


	$mpdf->WriteHTML(pagePDF1($poID));
	$mpdf->AddPage('P','','','','',5,5,30,20,10,10);
	$mpdf->WriteHTML(pagePDF2($poID));
	$mpdf->AddPage('P','','','','',5,5,30,20,10,10);
	$mpdf->WriteHTML(pagePDF3($poID));
	$mpdf->Output();



function pagePDF1($poID){
	global $pageStyle, $getPOAll, $insurTypes; 
	$page1 = "";
	$startDate = explode(" ", $getPOAll["PO_Coverage_End_Date"]->date);
	$endDatePO = $getPOAll["PO_Coverage_End_Date"]->format('Y-m-d H:i:s');
	$endDate = explode(" ",$endDatePO);
	// echo "<pre>".print_r($getPOAll,1)."</pre>";
	$getSubdistrict = getSubdistrict($getPOAll["Subdistrict_ID"]);
	if($getPOAll["Mobile_No"]){
		if($getPOAll["Mobile_No"] != $getPOAll["Tel_No"]){
			$Mobile_No = " หรือ ".$getPOAll["Mobile_No"];
		}
	}
	$title = ($getPOAll["Customer_Type"]=="P") ? "คุณ" : "";
	$page1 .= $pageStyle;
	$page1 .= '
		<div class="container fs18" style="margin-top: 50px; line-height: 18px;">
			<div >
				<p><b>เรียน</b> <span class="ml15">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; '.$title." ".$getPOAll["Customer_FName"]." ".$getPOAll["Customer_LName"].'</span></p>
				<p class="ml52"><span >'.$getPOAll["Addr1"].'</span></p>
				<p class="ml52"><span>ตำบล/แขวง '.getSubdistrict($getPOAll["Subdistrict_ID"]).'</span> <span class="ml15">อำเภอ/เขต '.getDistrict($getPOAll["District_ID"]).'</span></p>
				<p class="ml52"><span>'.getProvince($getPOAll["Province_ID"]).'&nbsp;&nbsp;'.$getPOAll["Post_Code"].'</p>
				<p class="mt50"><b>เรื่อง </b> <span class="ml15">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;แจ้งประกันภัยสิ้นสุดความคุ้มครอง</span></p>
			</div>
			<div style="margin-top: 15px; ">
					<p style="text-indent: 53px;">บริษัทเอเชียไดเร็ค อินชัวรันส์ โบรคเกอร์ จำกัด ขอขอบคุณ คุณ'.$getPOAll["Customer_FName"]." ".$getPOAll["Customer_LName"].' ในความไว้วางใจ ที่ท่านให้โอกาส บริษัทฯ เป็นผู้ดูแลการประกันรถยนต์ในปีที่ผ่านมา </p>
					<p style="text-indent: 53px; ">บริษัทฯ ขอเรียนให้ทราบว่า ประกันภัยรถยนต์ ทะเบียน <b>'.$getPOAll["Plate_No"].' '.getProvince($getPOAll["Plate_Province"]).'</b> รับประกันโดย '.getInserByID($getPOAll["Insurer_ID"]).' กรมธรรม์เลขที่ <b>'.$getPOAll["Policy_No"].'</b> จะสิ้นสุดความคุ้มครอง ในวันที่ <b>'.dateThai($endDate[0],0).'</b> </p>
			</div>';
	if($insurTypes == "036" || $insurTypes == "039" ){ 
		$page1 .= '
			<div style="margin-top: 5px;">
					<p style="text-indent: 53px;">เพื่อความคุ้มครองอย่างต่อเนื่อง บริษัทฯ ยินดีมอบสิทธิประโยชน์ระดับ <b>Gold Member</b> สำหรับลูกค้าที่ประสงค์ต่ออายุกรมธรรม์ <b>ประกันภัยประเภท 1 และ 2+ เท่านั้น</b> โดยเจ้าหน้าที่จะติดต่อท่าน ตามหมายเลขโทรศัพท์ '.$getPOAll["Tel_No"]."".$Mobile_No.' ที่ให้ไว้ หากท่านเปลี่ยนแปลงหมายเลขโทรศัพท์ กรุณาติดต่อกลับที่หมายเลข 02-089-2000 หรือส่งอีเมล์ adb@asiadirect.co.th หรือ Line ID: @asiadirect เพื่อยืนยันรับสิทธิ์
					</p>
			</div>
			<div>
				<p class="mt20"><b>รายละเอียดสิทธิพิเศษ</b> (กรณีประกันภัยประเภทชั้น 1 และ 2+ เท่านั้น)</p>
				<table style="width:100%; margin-top:10px;"  border="0" cellspacing="0">
					<thead>
						<tr cellpadding="3">
							<th width="25%"><b>บริการ</b></th>
							<th width="20%"><b>สิทธิพื้นฐาน</b></th>
							<th><b>สิทธิระดับ Gold Member</b></th>
							<th><b>มูลค่า</b></th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>- เติมน้ำมันฉุกเฉิน</td>
							<td>ฟรี 5 ลิตร ต่อปี</td>
							<td><b>ฟรี 10 ลิตร ต่อปี </b><span class="fs14">(ครั้งละไม่เกิน 10 ลิตร)</span></td>
							<td>1,000 บาท</td>
						</tr>
						<tr>
							<td>- รถยกฉุกเฉิน</td>
							<td>ฟรี 20 กิโลเมตรแรก</td>
							<td><b>ฟรี 50 กิโลเมตรแรก</b></td>
							<td>3,800 บาท</td>
						</tr>
						<tr>
							<td>- เงินชดเชยค่าเดินทางระหว่าง รถยนต์เข้าอู่ซ่อม <span class="fs14">(จากอุบัติเหตุรถชนกับยานพาหนะ ทางบกและซ่อมเกิน 12 ชั่วโมง)</span></td>
							<td>-</td>
							<td><b>ครั้งละ 1,000 บาท สูงสุด 5 ครั้งต่อปี</b></td>
							<td>5,000 บาท</td>
						</tr>
						<tr>
							<td style="border-bottom: 3px solid #000;">- บริการโรงแรมที่พักหรือรถเช่า</td>
							<td style="border-bottom: 3px solid #000;">-</td>
							<td style="border-bottom: 3px solid #000;"><b>วงเงินไม่เกิน 1,500 บาทต่อวัน สูงสุด 2 วัน ต่อปี </b><span class="fs14"><br>(กรณีรถเสียห่างจากที่พักเกิน 300 กิโลเมตร)</span></td>
							<td style="border-bottom: 3px solid #000;">3,000 บาท</td>
						</tr>
					</tbody>
				</table>
				<p class="t_r mt15 fs18">(สิทธิพิเศษดังกล่าวมีมูลค่ารวมทั้งสิ้น <b>12,800</b> บาท)</p>
			</div>';
	}
	$page1 .= '
			<p class="mt15" style="text-indent: 53px;">บริษัทเอเชียไดเร็ค อินชัวรันส์ โบรคเกอร์ จำกัด มีความมุ่งมั่นที่จะสรรหาเบี้ยราคาพิเศษที่คุ้มค่าและบริการที่ครอบคลุม เพื่อให้ท่านได้รับประโยชน์สูงสุด บริษัทฯ หวังเป็นอย่างยิ่งที่จะได้รับการติดต่อกลับจากท่านโดยเร็ว (ขออภัยหากท่านได้รับการติดต่อแล้ว)</p>
			<div class="mt40">
				<p class="t_c">ขอแสดงความนับถือ</p>
				<p class="t_c">ปัญจรัตน์ นพวัชร์เศรณี</p>
				<p class="t_c">(ผู้จัดการฝ่ายลูกค้าสัมพันธ์)</p>
			</div>
		</div>
	';
	return $page1;
}

function pagePDF2($poID){
	global $poID, $pageStyle, $getPOAll, $insurTypes, $getCompareOption, $sumTotal, $sumTotalDown, $down1, $down2; 

	$endDatePO = $getPOAll["PO_Coverage_End_Date"]->format('Y-m-d H:i:s');
	$endDate = explode(" ",$endDatePO);
	$getCompulsoryByID = getCompulsoryByID($getPOAll["Marine_Reference"]);
	$Driver_Not_Name = $getPOAll["Driver_Name"] ? '' : 'checked="checked"';
	$Driver_Name = $getPOAll["Driver_Name"] ? 'checked="checked"' : '';
	$DriverDate = $getPOAll["Driver_DOB"]->format('Y-n-d H:i:s');
	$Driver_DOB = explode(" ",$DriverDate);
	$cp_id = $getPOAll["CP_ID"];
	$getCoverageItem = getCoverageItem($cp_id); 
	// $getCompareOption = getCompareOption($cp_id);
	$getInsuranceByID = getInsuranceByID($getCompareOption["Insurance_ID"]);
	$InsuranceNameCompulsory = ($getCompareOption["Compulsory"]) ? "+ พรบ. " : "";
	$InsuranceName = $getInsuranceByID["Insurance_Name"]." ".$InsuranceNameCompulsory;
	$repairG = ($getCompareOption["Repair_Type"] == "G") ? 'checked="checked"' : '';
	$repairC = ($getCompareOption["Repair_Type"] == "C") ? 'checked="checked"' : '';
	$forCoverageItem = forCoverageItem($getCoverageItem);
	$page2 = "";
	$page2 .= $pageStyle;
	$page2 .= '
		<div class="container fs16" style="margin-top: 50px;">
			<div class="p5 bor_1">
				<p class="t_c fs16"><b>แจ้งต่ออายุกรมธรรม์ ประกันภัยรถยนต์</b></p>
				<p class="mt20"><b>เรียน</b> ท่านผู้เอาประกันภัย เนื่องจากกรมธรรม์ของท่านใกล้สิ้นสุดความคุ้มครอง <br>
				<b>กรุณาติดต่อกลับ ตัวแทนขาย คุณ'.$getPOAll["User_FName"].' '.$getPOAll["User_LName"].' '.$getPOAll["User_Phone"].' หรือ Call Center 02-089-2000<b> 
					<br>บริษัทฯ รู้สึกเป็นเกียรติ และมีความยินดีอย่างยิ่งที่ได้รับความไว้วางใจจากท่าน ที่ให้ บริษัทเอเชียไดเร็ค อินชัวรันส์ เป็นผู้ดูแลการประกันรถยนต์
				</p>
			</div>
			<div class="p5 bor_l bor_r ">
				<p><input id="checkBox" type="checkbox">
				<b>ข้าพเจ้ามีความประสงค์ต่ออายุ เนื่องด้วยข้อบังคับทางกฏหมายกรุณาระบุ<br> เลขที่บัตรประชาชน</b> .............................................................................. <b>โทร</b>...............................................
				</p>
				<p class="ml15">ขั้นตอนการแจ้งต่ออายุกรมธรรม์</p>
				<p class="ml15">1. ยืนยันการต่ออายุ โดยส่งเอกสารฉบับนี้มาที่ แฟกซ์ : 02-089-2088 หรืออีเมล์: adb@asiadirect.co.th</p>
				<p class="ml15">2. ชำระเงิน และส่งหลักฐานการชำระเงิน ตามรายละเอียดการชำระเงิน</p>

			</div>
			<div class="p5 bor_1">
				<p><b>ข้อมูลประกันภัย</b></p>
					<div class="ml10">
						<p>ทะเบียน <b>'.$getPOAll["Plate_No"].' '.getProvince($getPOAll["Plate_Province"]).'</b> กรมธรรม์เลขที่ <b>'.$getPOAll["Policy_No"].'</b></p>
						<p>รับประกันโดย <b>'.getInserByID($getPOAll["Insurer_ID"]).'</b> ซึ่งจะหมดอายุวันที่ <b>'.dateThai($endDate[0]).'</b></p>
						';
			if($getPOAll["Marine_Reference"]){
				$page2 .= '<p>และ พรบ. กรมธรรม์เลขที่ <b>'.$getCompulsoryByID["Compulsory_No"].'</b>
				รับประกันโดย <b>'.getInserByID($getCompulsoryByID["Insurer_ID"]).'</b> ซึ่งจะหมดอายุวันที่ <b>'.dateThai($endDate[0]).'</b></p>';
			}
	$page2 .= '
						<p><b>เพื่อการรับประกันอย่างต่อเนื่อง กรุณาแจ้งต่ออายุกรมธรรม์ล่วงหน้าก่อนครบกำหนดอย่างน้อย 7 วัน มีรายละเอียดการประกันภัยดังต่อไปนี้</b></p>
					</div>
			</div>
			<div class="p5 bor_l bor_r bor_b" style="height: 50px;">
				<p>
					<b>กรมธรรม์แบบ</b> 
					<input type="checkbox" '.$Driver_Not_Name.'>ไม่ระบุชื่อผู้ขับขี่ &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="checkbox" '.$Driver_Name.'>ระบุชื่อผู้ขับขี่
				</p>';
	if($getPOAll["Driver_Name"]){
		$page2 .= '
				<div>
					<p style="float: left; width: 40%;">ผู้ขับขี่ 1 : <b>'.$getPOAll["Driver_Name"].'</b></p>
					<p style="float: left;width: 25%;">วัน/เดือน/ปีเกิด : <b>'.dateThai($Driver_DOB[0]).'</b></p>
					<p style="float: left; margin-left:20px; width: 30%;"> เลขที่ใบขับขี่ : <b>'.$getPOAll["Driver_License_No"].'</b></p>
				</div>';
	}else{
		$page2 .= '
				<div>
					<p style="float: left; width: 40%;">ผู้ขับขี่ 1 : .................................................................................</p>
					<p style="float: left; width: 25%;">วัน/เดือน/ปีเกิด : .........................................</p>
					<p style="float: left; margin-left:20px; width: 30%;"> เลขที่บัตรประชาชน : .........................................</p>
				</div>';
	}
	$page2 .= '
				<div>
					<p style="float: left; width: 40%;">ผู้ขับขี่ 2 : .................................................................................</p>
					<p style="float: left;width: 25%;">วัน/เดือน/ปีเกิด : .........................................</p>
					<p style="float: left; margin-left:20px; width: 30%;"> เลขที่บัตรประชาชน : .........................................</p>
				</div>
			</div>
			<div class="bor_l bor_r bor_b">
				<p class="ml5 pt5 mb5">
					<b>รายการรถยนต์ที่เอาประกันภัย</b> 
					<input type="checkbox" '.$repairG.'>ซ่อมอู่ &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="checkbox" '.$repairC.'>ซ่อมห้าง
				</p>
				<div>
					<table class="tableCar fs12" style="width:100%;" border="0" cellspacing="0">
						<thead>
							<tr >
								<th>ยี่ห้อรถ</th>
								<th>รุ่น</th>
								<th>ทะเบียน</th>
								<th>เลขตัวถัง</th>
								<th>ปี รุ่น</th>
								<th>แบบตัวถัง</th>
								<th style="border-right:none;">ที่นั่ง/ขนาด/น้ำหนัก</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td style="text-align:center; width: 10%;">'.getCarMake($getPOAll["Make_ID"]).'</td>
								<td style="text-align:center; width: 15%;">'.getCarModel($getPOAll["Model"]).'</td>
								<td style="text-align:center; width: 20%;">'.$getPOAll["Plate_No"].' '.getProvince($getPOAll["Plate_Province"]).'</td>
								<td style="text-align:center; width: 15%;">'.$getPOAll["Frame_No"].'</td>
								<td style="text-align:center; width: 10%;">'.$getPOAll["Sub_Model"].'</td>
								<td style="text-align:center; width: 10%;">'.$getPOAll["Body_Type"].'</td>
								<td style="text-align:center; width: 10%; border-right:none;">'.$getPOAll["Seat_CC_Weight"].'</td>
							</tr>
						</tbody>
					</table>
					<p class="ml5 pt5 mb5">รายการตกแต่งเปลี่ยนแปลงรถยนต์เพิ่มเติม (โปรดระบุรายละเอียด) : ................................................................................................................................</p>
				</div>
			</div>
			<div class="bor_l bor_r bor_b " style="clear: both;">
				<p class="ml5 pt5 mb5 t_c"><b>รายละเอียดความคุ้มครอง ภาคสมัครใจ</b></p>
				<div class="bor_t bor_b ">
					<div class="boxCoverage bor_r "><p class="mt5 t_c mb5">ความรับผิดต่อบุคคลภายนอก</p></div>
					<div class="boxCoverage bor_r" ><p class="mt5 t_c mb5">รถยนต์เสียหาย สูญหาย ไฟไหม้</p></div>
					<div class="boxCoverage"><p class="mt5 t_c mb5">ความคุ้มครองตามเอกสารแนบท้าย</p></div>
				</div>
				<div >
					<div class="boxCoverage bor_r " style="height:145px;">
						<div class="p5">
							<p>1) ความเสียหายต่อชีวิต ร่างกาย หรืออนามัยเฉพาะส่วนเกินวงเงิน สูงสุดตาม พ.ร.บ. </p>
							<div>
								<p style="float: left;width: 70%; text-align:right; margin-right: 20px;"><b>'.number_format($getCoverageItem["502"],2).'</b> </p>
								<p style="float: left;width: 20%; text-align:right;">บาท/ครั้ง</p>
							</div>
							<div>
								<p style="float: left;width: 70%; text-align:right; margin-right: 20px;"><b>'.number_format($getCoverageItem["503"],2).'</p>
								<p style="float: left;width: 20%; text-align:right;">บาท/ครั้ง</p>
							</div>
							<div style="clear: both;">
								<p>2) ความเสียหายต่อทรัพย์สิน</p>
								<p style="float: left;width: 70%; text-align:right; margin-right: 20px;"><b>'.number_format($getCoverageItem["504"],2).'</p>
								<p style="float: left;width: 20%; text-align:right;">บาท/ครั้ง</p>
							</div>
							<div class="ml10" style="clear: both;">
								<p>2.1) ความเสียหายส่วนแรก</p>
								<p style="float: left;width: 70%; text-align:right; margin-right: 20px;"><b>'.number_format($getCoverageItem["505"],2).'</p>
								<p style="float: left;width: 20%; text-align:right;">บาท/ครั้ง</p>
							</div>
						</div>
					</div>
					<div class="boxCoverage " style="height:145px;">
						<div class="p5">
							<div>
								<p>1) ความคุ้มครองความเสียหายต่อรถยนต์</p>
								<p style="float: left;width: 70%;text-align:right;margin-right: 20px;"><b>'.number_format($getCoverageItem["536"],2).'</b></p>
								<p style="float: left;width: 20%; text-align:right;">บาท/ครั้ง</p>
							</div>
							<div  style="clear: both;">
								<p class="ml10">1.1) ความเสียหายส่วนแรก</p>
								<p style="float: left;width: 70%;text-align:right;margin-right: 21px;"><b>'.number_format($getCoverageItem["506"],2).'</b></p>
								<p style="float: left;width: 20%; text-align:right;">บาท/ครั้ง</p>
							</div>
							<div style="clear: both;">
								<p>2) รถยนต์สูญหาย/ไฟไหม้</p>
								<p style="float: left;width: 70%;text-align:right;margin-right: 20px;"><b>'.number_format($getCoverageItem["507"],2).'</b></p>
								<p style="float: left;width: 20%; text-align:right;">บาท/ครั้ง</p>
							</div>
						</div>
					</div>
					<div class="boxCoverage bor_l" style="height:145px;">
						<div class="p5">
							<div>
								<p>1) อุบัติเหตุส่วนบุคคล</p>
							</div>
							<div class="ml5" style="clear: both;">
								<p>1.1 เสียชีวิต สูญเสียอวัยวะ ทุพพลภาพถาวร</p>
								<p style="float: left;width: 40%;margin-left: 10px;">ก) ผู้ขับขี่ 1 คน</p>
								<p style="float: left;width: 30%;text-align:right;margin-right: 20px;"><b>'.number_format($forCoverageItem[0]["price0"],2).'</b> </p>
								<p style="float: left;width: 20%; text-align:right;">บาท/คน</p>

								<p style="float: left;width: 40%;margin-left: 10px;">ข) ผู้โดยสาร '.$forCoverageItem[0]["num0"].' คน</p>
								<p style="float: left;width: 30%;text-align:right;margin-right: 20px;"><b>'.number_format($forCoverageItem[0]["price0"],2).'</b> </p>
								<p style="float: left;width: 20%; text-align:right;">บาท/คน</p>
							</div>
							<div class="ml5" style="clear: both;">
								<p>1.2 ทุพพลภาพชั่วคราว</p>
								<p style="float: left;width: 40%;margin-left: 10px;">ก) ผู้ขับขี่ - คน</p>
								<p style="float: left;width: 30%;text-align:right;margin-right: 20px;"><b> 0.00</b> </p>
								<p style="float: left;width: 20%; text-align:right;">บาท/คน</p>

								<p style="float: left;width: 40%;margin-left: 10px;">ข) ผู้โดยสาร - คน</p>
								<p style="float: left;width: 30%;text-align:right;margin-right: 20px;"><b> 0.00</b> </p>
								<p style="float: left;width: 20%; text-align:right;">บาท/คน</p>
							</div>
							<div style="clear: both;">
								<p style="float: left;width: 44.5%;">2) ค่ารักษาพยาบาล</p>
								<p style="float: left;width: 30.5%; text-align:right;margin-right: 20px;"><b>'.number_format($forCoverageItem[1]["price1"],2).'</b> </p>
								<p style="float: left;width: 20%; text-align:right;">บาท/ครั้ง</p>
							</div>
							<div style="clear: both;">
								<p style="float: left;width: 44.5%;">3) การประกันตัวผู้ขับขี่</p>
								<p style="float: left;width: 30.5%; text-align:right;margin-right: 20px;"><b>'.number_format($getCoverageItem["532"],2).'</b> </p>
								<p style="float: left;width: 20%; text-align:right;">บาท/ครั้ง</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="bor_l bor_r bor_b " style="clear: both;">
				<div class="">
					<div class="fl bor_r" style="width: 49%; height:150px;">
						<div class="p5 " >
							<div><p >ประกันภัยรถยนต์ประเภท : <b>'.$InsuranceName.'</b></p></div>
							<div  class="mt10">
								<p style="float: left;width: 40%;">เบี้ยประกันภัย:</p>
								<p style="float: left;width: 40%; text-align:right; margin-right: 10px;"><b>'.number_format($getCompareOption["Total_Premium"],2).'</b></p>
								<p style="float: left;width: 10%; text-align:right;">บาท</p>
							</div>
							<div>
								<p style="float: left;width: 40%;">พรบ.:</p>
								<p style="float: left;width: 40%; text-align:right; margin-right: 10px;"><b>'.number_format($getCompareOption["Compulsory"],2).'</b></p>
								<p style="float: left;width: 10%; text-align:right;">บาท</p>
							</div>
							<div>
								<p style="float: left;width: 40%;">ส่วนลด:</p>
								<p style="float: left;width: 40%; text-align:right; margin-right: 10px;"><b>'.number_format($getCompareOption["Discount"],2).'</b></p>
								<p style="float: left;width: 10%; text-align:right;">บาท</p>
							</div>
							<div>
								<p style="float: left;width: 40%;">รวม:</p>
								<p style="float: left;width: 40%; text-align:right; margin-right: 10px;"><b>'.number_format($sumTotal,2).'</b></p>
								<p style="float: left;width: 10%; text-align:right;">บาท</p>
							</div>
						</div>
					</div>
					<div class="fl" style="width: 49%; height:150px;">
						<div class="p5">
							<div><p ><b>เพื่อเป็นการลดภาระค่าใช้จ่าย ท่านสามารถเลือกการชำระเงินได้ดังนี้</b></p></div>
							<div  class="mt10" >
								<div><input type="checkbox" > ชำระเต็มจำนวน</div>
								<p class="ml20"><b>ชำระภายในวันที่ '.dateThaiDown($endDate[0]).' ลดเหลือ '.number_format($sumTotal,2).' บาท</b></p>
							</div>
							<div  class="mt10" >
								<div><input type="checkbox" > ผ่อนชำระ 3 งวด</div>
								<div class="ml20">
									<p style="float: left;width: 50%;">ชำระภายในวันที่ '.dateThaiDown($endDate[0]).'</p>
									<p style="float: left;width: 40%; text-align:right; margin-right: 10px;"><b>'.number_format($down1,2).'</b></p>
									<p style="float: left;width: 10%; text-align:right;">บาท</p>
								</div>
								<div class="ml20">
									<p style="float: left;width: 50%;">ชำระภายในวันที่ '.dateThaiDown($endDate[0],30).'</p>
									<p style="float: left;width: 40%; text-align:right; margin-right: 10px;"><b>'.number_format($down2,2).'</b></p>
									<p style="float: left;width: 10%; text-align:right;">บาท</p>
								</div>
								<div class="ml20">
									<p style="float: left;width: 50%;">ชำระภายในวันที่ '.dateThaiDown($endDate[0],60).'</p>
									<p style="float: left;width: 40%; text-align:right; margin-right: 10px;"><b>'.number_format($down2,2).'</b></p>
									<p style="float: left;width: 10%; text-align:right;">บาท</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="fs14 mt15 " style="clear: both;">
				<u><b>เงื่อนไขการเอาประกันภัย</b></u>
				<br>1) ทางบริษัทฯ มีบริการผ่อนชำระค่าเบี้ยประกัน สำหรับลูกค้าของบริษัทฯ โดยไม่เสียดอกเบี้ย ซึ่งผู้เอาประกันจะต้องชำระเข้ามาที่บริษัท เอเชียไดเร็ค อินชัวรันส์ โบรคเกอร์ จำกัด เท่านั้น ตามเอกสารการชำระเบี้ยประกัน เนื่องจากบริษัทประกันภัย ไม่มีนโยบายผ่อนชำระ
				<br>2) ในกรณีที่มีการเรียกร้องค่าสินไหมทดแทน ผู้เอาประกัน จะต้องชำระค่าเบี้ยประกันส่วนที่เหลือให้ครบเต็มจำนวน มิฉะนั้นจะไม่สามารถเรียกร้องค่าสินไหมทดแทนได้
				<br>3) กรณีที่มีอุบัติเหตุ หรือเรียกร้องสินไหมทดแทน หลังจากออกหนังสือเตือนต่ออายุฉบับนี้ เบี้ยประกันภัย อาจมีการเปลี่ยนแปลง
			</div>
		</div>
	';
	return $page2;
}

function pagePDF3($poID){
	global $poID, $pageStyle, $getPOAll, $insurTypes, $getCompareOption, $sumTotal, $sumTotalDown, $down1, $down2, $barcode;
	$endDatePO = $getPOAll["PO_Coverage_End_Date"]->format('Y-m-d H:i:s');
	$endDate = explode(" ",$endDatePO);
	// echo "<pre>".print_r($endDate,1)."</pre>";
	$code_cp = trim($getPOAll["CP_ID"],"CP")."01";
	$code_customer = trim($getPOAll["Customer_ID"],"P");
	$genBarcode = "|024554000009700".chr(10)."".$code_cp."".chr(10)."".$code_customer."".chr(10)."".str_replace(".", "", $sumTotal);
	$MyBarCode = genBarcode($poID, $genBarcode);
	$barcode = "|024554000009700 ".$code_cp." ".$code_customer." ".str_replace(".", "", $sumTotal);
	$page2 = "";
	$page2 .= $pageStyle;
	$page2 .= '
		<div class="container fs16" style="margin-top: 50px;">
			<div class="">
				<p class=""><b>ช่องทางการชำระเงิน</b></p>
				<p>1. ชำระผ่านใบแจ้งการชำระเงิน</p>
				<p class="t_r">สำหรับธนาคาร</p>
			</div>
			<div class="bor_l bor_r bor_b" style="line-height:18px;">
				<div class="bor_t">
					<div class="fl" style="width:60%">
						<div class="p10">
							<div style="float: left;margin-right: 5px; width:100px;"><img src="img/logo_110.png" alt="" width="70px;"></div>
							<div class="fl ">
								<p><b>เลขประจำตัวผู้เสียภาษี 0245540000097</b></p>
								<p>บริษัท เอเชียไดเร็ค อินชัวรันส์ โบรคเกอร์ จำกัด</p>
								<p>Asia Direct Insurance Broker Co.,Ltd.</p>
								<p>เลขที่ 626 อาคารบีบีดี (พระราม 4) ชั้น 11 ถนนพระรามที่ 4</p>
								<p>แขวงมหาพฤฒาราม เขตบางรัก กรุงเทพฯ 10500</p>
							</div>	
						</div>
					</div>
					<div class="fl bor_l">
						<div class="p5">
							<div >
								<p><b>สาขา</b>.......................................... <b>วันที่</b> .............................</p>
								<p><b>SERVICE CODE : ADB</b></p>
								<p><b>ชื่อ-นามสกุล /Name</b> : '.$getPOAll["Customer_FName"]." ".$getPOAll["Customer_LName"].'</p>
								<p><b>หมายเลขใบสั่งซื้อ / Ref. 1</b> : '.$code_cp.' </p>
								<p><b>รหัสประจำตัวลูกค้า / Ref. 2</b> : '.$code_customer.'</p>
								<p><b>ค่าประกันงวดที่ 1 กำหนดชำระภายใน</b> '.dateThaiDown($endDate[0]).'</p>
								<p><b>ยอดชำระ</b> '.number_format($sumTotal,2).' <b>บาท</b></p>
							</div>	
						</div>
					</div>
				</div>
			</div>
			<div class="bor_l bor_r " style="clear: both;">
				<div class="p5">
					<div>
						<input id="checkBox" type="checkbox">
						<span style="margin-left:15px" ><img src="img/logo_bangkok.png" width="15px;"></span><span style="margin-left:15px">&nbsp;&nbsp;บมจ. ธนาคารกรุงเทพ / Bangkok Bank (Br.no. 1161) (Comcode : 34811)</span>
					</div>
					<div>
						<input id="checkBox" type="checkbox">
						<span style="margin-left:15px" ><img src="img/logo_kbank.png" width="15px;"></span><span style="margin-left:15px">&nbsp;&nbsp;บมจ.ธนาคารกสิกรไทย / Kasikorn Bank เลขที่บัญชี 630-1-00382-8 สาขา จามจุรีสแควร์</span>
					</div>
					<div>
						<input id="checkBox" type="checkbox">
						<span style="margin-left:15px" ><img src="img/cd-icon-tbank.png" width="15px;"></span><span style="margin-left:15px">&nbsp;&nbsp;บมจ.ธนาคารธนชาต / Thanachart Bank Service Code : 7315</span>
					</div>
					<div class="mt5">
						ชำระโดย &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
						<input id="checkBox" type="checkbox"> เงินสด/Cash &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
						<input id="checkBox" type="checkbox"> เช็ค/Cheque
					</div>
				</div>
			</div>
			<div class="bor_l bor_r bor_b" style="clear: both;">
				<div>
					<table class="tableCar fs12" style="width:100%;" border="0" cellspacing="0">
						<thead>
							<tr >
								<th>เช็คธนาคาร/สาขา Drawee Bank/Branch</th>
								<th>หมายเลขเช็ค/Cheque No.</th>
								<th>เช็คลงวันที่/Date</th>
								<th style="border-right:none;">จำนวนเงิน/Amount</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td style="width:30%; height:25px;" ></td>
								<td style="width:20%;"></td>
								<td style="width:20%;"></td>
								<td style="width:30%; text-align:center; border-right:none; font-size:12px" rowspan="2" ><b>'.number_format($sumTotal,2).'</b></td>
							</tr>
							<tr>
								<td colspan="3"><p style="margin-top: 5px;"><b>เงินสด :</b></p></td>
							</tr>
						</tbody>
					</table>
					<p class="p5" style="margin-top: 5px;"><b>จำนวนเงิน(ตัวหนังสือ) /Amount in Words :</b></p>
				</div>
			</div>
			<div>
				<div class="mt10 t_c">
					<img src="picBarcode/'.$poID.'.png" alt="" width="300x" height="30px">
					<p class="t_c">'.$barcode.'</p>
				</div>
			</div>

			<div class="mt20" style="clear: both;">
				<p>2. ชำระเบี้ยประกันผ่านบัตรเครดิต</p>
				<div class="bor_1 p5" style="line-height:20px">
					<p class="fs14">ข้าพเจ้ามีความประสงค์และยินยอมให้ทางบริษัท เอเชียไดเร็ค อินชัวรันส์ โบรคเกอร์ จำกัด เรียกเก็บเงินจากบัตรเครดิตของข้าพเจ้า เพื่อชำระค่าเบี้ยประกัน มีรายละเอียดของบัตร ดังนี้</p>
					<p><b>รายละเอียดเจ้าของบัตร</b></p>
					<p>ชื่อ – นามสกุล (ตามบัตร) : .........................................................................................................................</p>
					<p>เบอร์โทรศัพท์ : ................................................... แฟ๊กซ์ : ................................................... E-mail : .....................................................................................................</p>
					<p style="">ประเภทบัตร</p>
					<div class="ml10">
						<p style="float:left; width:20%;"><input type="checkbox" style="width:50px;height:50px;">&nbsp;&nbsp;<img src="img/visa.png" alt="" style="width:25px"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;VISA CARD</p>
						<p style="float:left; width:20%;"><input type="checkbox">&nbsp;&nbsp;<img src="img/MasterCard.png" alt="" style="width:25px"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MASTER CARD</p>
					</div>
					<div class="ml10">
						<p style="float:left; width:20%;"> </p>
						<p style="float:left; width:20%;"><input type="checkbox">&nbsp;&nbsp;<img src="img/logo_kbank.png" alt="" style="width:20px"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ธนาคารกสิกรไทย</p>
						<p style="float:left; width:20%;"><input type="checkbox">&nbsp;&nbsp;<img src="img/ktb_01.png" alt="" style="width:20px"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ธนาคารกรุงไทย</p>
						<p style="float:left; width:30%; margin-top:8px;"><input type="checkbox">&nbsp;&nbsp;อื่น...................................................</p>
					</div>
					<div style="clear: both;">
						<p style="float:left; width:15%;">หมายเลขบัตรเครดิต</p> 
						<p style="float:left; width:45%;">
							<span><img src="img/inputbox.png" alt="" width="80px"></span>
							<span><img src="img/inputbox.png" alt="" width="80px"></span> 
							<span><img src="img/inputbox.png" alt="" width="80px"></span>
							<span><img src="img/inputbox.png" alt="" width="80px"></span>
						</p>
						<p style="float:left; width:15%;">บัตรหมดอายุวันที่</p> 
						<p style="float:left;"><span><img src="img/inputbox2.png" alt="" width="90px"></span></p>
					</div>
					<div>
						<p><b>รายละเอียดการชำระ</b></p>
						<div style="float: left;width: 45%;">
								<div><input type="checkbox" > ชำระเต็มจำนวน</div>
								<p class="ml20"><b>ภายในวันที่ '.dateThaiDown($endDate[0]).' ลดเหลือ '.number_format($sumTotal,2).' บาท</b></p>
							</div>
							<div style="float: left;width: 45%; line-height:15px;">
								<div><input type="checkbox" > ผ่อน 3 งวด</div>
								<div class="ml20">
									<p style="float: left;width: 50%;">ภายในวันที่ '.dateThaiDown($endDate[0]).'</p>
									<p style="float: left;width: 20%; text-align:right; margin-right: 10px;"><b>'.number_format($down1,2).'</b></p>
									<p style="float: left;width: 10%; text-align:right;">บาท</p>
								</div>
								<div class="ml20">
									<p style="float: left;width: 50%;">ภายในวันที่ '.dateThaiDown($endDate[0],30).'</p>
									<p style="float: left;width: 20%; text-align:right; margin-right: 10px;"><b>'.number_format($down2,2).'</b></p>
									<p style="float: left;width: 10%; text-align:right;">บาท</p>
								</div>
								<div class="ml20">
									<p style="float: left;width: 50%;">ภายในวันที่ '.dateThaiDown($endDate[0],60).'</p>
									<p style="float: left;width: 20%; text-align:right; margin-right: 10px;"><b>'.number_format($down2,2).'</b></p>
									<p style="float: left;width: 10%; text-align:right;">บาท</p>
								</div>
							</div>
					</div>
					<p class="t_c">ลายเซ็น (เจ้าของบัตร) :................................................................................. วันที่ : ...................................................</p>
					<p class="t_c">(หลักฐานการอนุมัติ 1.สำเนาบัตรเครดิตด้านหน้า 2.สำเนาบัตรประชาชนของผู้ถือบัตร เซ็นรับรองสำเนาถูกต้องทุกฉบับ)</p>
				</div>
			</div>
			<div class="bor_b bor_l bor_r p5">
				<p><b>สำหรับเจ้าหน้าที่บริษัทฯ</b></p>
				<p>จำนวนเงินที่ขออนุมัติ.....................................................................................................รหัสอนุมัติบัตร.....................................................................................................</p>
				<br><p>ลงชื่อเจ้าหน้าที่ : .................................................................................เบอร์ติดต่อ.................................................................................วันที่ : .........../.........../...........</p>
			</div>
			<p class="fs18 t_c mt5"><b>กรุณาส่งหลักฐานการชำระเงิน มาที่ แฟกซ์ : 02-089-2088 หรือส่งอีเมล์ : adb@asiadirect.co.th หรือ Line ID : @asiadirect</b></p>
		</div>
	';
	return $page2;
}