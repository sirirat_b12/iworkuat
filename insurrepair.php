<?php 

session_start();
$UserCode = $_SESSION["User"]['UserCode'];
$userType = $_SESSION["User"]['type'];

if(!isset($UserCode)){
	echo "<script>alert('กรุณาเข้าสู่ระบบ!!!');</script>";
	echo '<META http-equiv="refresh" content="0;URL=login.php">';
	exit();
}
header('Content-Type: text/html; charset=utf-8');
include "include/inc_header.php"; 
include "include/inc_menu.php"; 


$getInsurer = getInsurer();
if($_GET["insur"]){
	$getEmailInsur = getEmailInsurAll($_GET["insur"]);
}
// echo "<pre>".print_r($getEmailInsur,1)."</pre>";
?>
<div class="main">
	<div class="main-content p20">
		<div class="p20">
				<div class="row">
					<div class="col-md-12">
						<div class="panel">
								<div class="panel-body fs14">
									<div class="row mt20">
										<div class="col-md-12">
											<table class="table table-hover" id="tableMoveQC">
												<thead>
													<tr>
														<th class="t_c">ลำดับ</th>
														<th class="t_c">บริษัทประกัน</th>													
													</tr>
												</thead>
												<tbody>
													<tr >
														<td class="t_c" width="2%">1</td>
														<td ><a href="https://www.viriyah.co.th/th/contact-repair-standard.php#.WyNzqKczaUk" target="_bank"> วิริยะ</a></td>
													</tr>
													<tr >
														<td class="t_c" width="2%">2</td>
														<td ><a href="https://www.asiainsurance.co.th/wp2web/other/garage-list/" target="_bank"> เอเชีย</a></td>
													</tr>
													<tr >
														<td class="t_c" width="2%">3</td>
														<td ><a href="https://www.asset.co.th/products-servicecenter.php" target="_bank"> สินทรัพย์</a></td>
													</tr>
													<tr >
														<td class="t_c" width="2%">4</td>
														<td ><a href="https://www.smk.co.th/Garage.aspx" target="_bank"> สินมั่นคง</a></td>
													</tr>
													<tr >
														<td class="t_c" width="2%">5</td>
														<td ><a href="http://www.segroup.co.th/seic/service_new_garage.php" target="_bank"> อาคเนย์</a></td>
													</tr>
													<tr >
														<td class="t_c" width="2%">6</td>
														<td ><a href="https://www.thaivivat.co.th/th/service.php" target="_bank"> ไทยวิวัฒน์</a></td>
													</tr>
													<tr >
														<td class="t_c" width="2%">7</td>
														<td ><a href="https://www.mticonnect.com/Service/Search#Garage" target="_bank"> เมืองไทย</a></td>
													</tr>
													<tr >
														<td class="t_c" width="2%">8</td>
														<td ><a href="https://www.dhipaya.co.th/INSURANCE/SEARCH_GARAGE.ASPX?ID=0&idMenu=503" target="_bank"> ทิพย</a></td>
													</tr>
													<tr >
														<td class="t_c" width="2%">9</td>
														<td ><a href="https://www.safety.co.th/th/repairers" target="_bank"> คุ้มภัย</a></td>
													</tr>
													<tr >
														<td class="t_c" width="2%">10</td>
														<td ><a href="https://www.navakij.co.th/partner" target="_bank"> นวกิจ</a></td>
													</tr>
													<tr >
														<td class="t_c" width="2%">11</td>
														<td ><a href="https://www.allianz.co.th/th_TH/services/garage-list.html" target="_bank"> อลิอันซ์</a></td>
													</tr>
													<tr >
														<td class="t_c" width="2%">12</td>
														<td ><a href="http://www.bangkokinsurance.com/claim/distributor" target="_bank"> กรุงเทพ</a></td>
													</tr>
													<tr >
														<td class="t_c" width="2%">13</td>
														<td ><a href="https://www.thaiins.com/home/page_service.php?list=3" target="_bank"> ไทยประกัน</a></td>
													</tr>
													<tr >
														<td class="t_c" width="2%">14</td>
														<td ><a href="https://www.thaipaiboon.com/main.php?m=corporate&p=mod_supportservice&cid=3" target="_bank"> ไทยไพบูลย์</a></td>
													</tr>
													<tr >
														<td class="t_c" width="2%">15</td>
														<td ><a href="https://www.lmginsurance.co.th/th/Pages/Search.aspx" target="_bank"> LMG</a></td>
													</tr>
													<tr >
														<td class="t_c" width="2%">16</td>
														<td ><a href="https://www.thaisri.com/services/garage-list/" target="_bank"> ไทยศรี</a></td>
													</tr>
													<tr >
														<td class="t_c" width="2%">17</td>
														<td ><a href=https://www.kpi.co.th/Service/SearchGarage" target="_bank"> กรุงไทย</a></td>
													</tr>
													<tr >
														<td class="t_c" width="2%">18</td>
														<td ><a href="http://www.kskinsurance.co.th/index.php?option=com_content&view=article&id=486&Itemid=201&lang=th" target="_bank"> KSK</a></td>
													</tr>
													<tr >
														<td class="t_c" width="2%">18</td>
														<td ><a href="https://www.thanachartinsurance.co.th/tnifrontend/tnigarage.aspx?default=1" target="_bank"> ธนชาต</a></td>
													</tr>
													<tr >
														<td class="t_c" width="2%">19</td>
														<td ><a href="https://www.jpinsurance.co.th/%E0%B8%84%E0%B9%89%E0%B8%99%E0%B8%AB%E0%B8%B2%E0%B8%AD%E0%B8%B9%E0%B9%88%E0%B8%A3%E0%B8%96%E0%B8%A2%E0%B8%99%E0%B8%95%E0%B9%8C%E0%B8%84%E0%B9%89%E0%B8%99%E0%B8%AB%E0%B8%B2%E0%B8%AD%E0%B8%B9%E0%B9%88%E0%B9%81%E0%B8%A5%E0%B8%B0%E0%B9%82%E0%B8%A3%E0%B8%87%E0%B8%9E%E0%B8%A2%E0%B8%B2%E0%B8%9A%E0%B8%B2%E0%B8%A5/" target="_bank"> เจพีประกันภัย</a></td>
													</tr>
													<tr >
														<td class="t_c" width="2%">19</td>
														<td ><a href="https://www.axa.co.th/garage-locator" target="_bank"> แอกซ่าประกันภัย</a></td>
													</tr>
													<tr >
														<td class="t_c" width="2%">20</td>
														<td ><a href="https://www.sompo.co.th/page/motor_claim" target="_bank"> ซมโปะประกันภัย</a></td>
													</tr>
													<tr >
														<td class="t_c" width="2%">21</td>
														<td ><a href="https://www.tokiomarine.com/th/th-general/home/resources/find-a-service-provider/workshop-locator.html" target="_bank"> โตเกียวมารีนประกันภัย</a></td>
													</tr>
												</tbody>
											</table>
										</div>
									</div>
								</div>
						</div>
					</div>
				</div>
		</div>
	</div>
</div>
<?php include "include/inc_footer.php"; ?> 
