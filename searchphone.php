<?php 

session_start();
if(!isset($_SESSION["User"]['UserCode'])){
	echo "<script>alert('กรุณาเข้าสู่ระบบ!!!');</script>";
	echo '<META http-equiv="refresh" content="0;URL=login.php">';
	exit();
}
header('Content-Type: text/html; charset=utf-8');
include "include/inc_header.php"; 
include "include/inc_menu.php"; 

$phone = $_GET["phone"];
// $phone = "0929496429";
$getCPbyPhone = $phone ? getCPbyPhone($phone) : "";
// echo "<pre>".print_r($getCPbyPhone,1)."</pre>";
?> 
<div class="main">
	<div class="main-content">
		<div class="container-fluid">
			<div>
					<div class="panel ">
						<div class="panel-heading"><h3 class="panel-title">ค้นหา</h3></div>
						<div class="panel-body">
							<div class="row p10" >
								<div class="col-md-12 t_c">
									<form action="searchphone.php" method="GET">
										<i class="fa fa-phone-square"></i>
										<input type="text" name="phone" id="phone"  value="<?php echo $phone ?>" class="form-control formInput2 dib" style="width: 15%">
					<!-- 					<span class="cursorPoin btn btn-success" onclick="btnSearch()">ค้นหา</span> -->
										<input type="submit" class="btn btn-success" value="ค้นหา">
									</form>
								</div>
							</div>
							<div class="row mt20">
								<div class="col-md-12">
									<table class="table table-striped" id="tableListChk">
										<thead>
											<tr>
												<td class="t_c">CP</td>
												<td class="t_c">PO</td>
												<td>วันที่ออกข้อเสนอ</td>
												<td>รหัสลูค้า</td>
												<td>ชื่อ-นามสกุล</td>
												<td>เบอร์โทร</td>
												<td class="t_c">พนักงาน</td>
												<td class="t_c">สถานะ</td>
												<td class="t_c">การติดต่อ</td>
											</tr>
										</thead>
										<tbody class="table-list">
											<?php
												if($getCPbyPhone){
													foreach ($getCPbyPhone as $key => $value) { 
											?>
												<tr class="fs12">
													<td class="t_c"><?php echo $value["CP_ID"]; ?></td>
													<td class="t_c"><?php echo $value["CP_PO_ID"]; ?></td>
													<td><?php echo $value["CP_Date"]->format("Y-m-d h:i"); ?></td>
													<td><?php echo $value["Customer_ID"]; ?></td>
													<td><?php echo $value["Customer_FName"]." ".$value["Customer_LName"]; ?></td>
													<td><?php echo $value["Tel_No"]." | ".$value["Mobile_No"]; ?></td>
													<td class="t_c"><?php echo $value["User_FName"]." ".$value["User_LName"]; ?></td>
													<td class="t_c"><?php echo $value["Compare_Status_ID"]; ?></td>
													<td class="t_c"><?php echo $value["Active"]; ?></td>
												</tr>
											<?php }
													}else{ ?>
												<tr>
													<td colspan="9" class="text-center">กรุณาเลือกข้อมูล</td>
												</tr>
											<?php } ?>
									</table>
									<?php if(!$getCPbyPhone) { ?>
									<div class="t_c mt70"><a href="#" class="btn btn-primary btnAddCus  fwb"><i class="lnr lnr-location"></i> สร้างลูกค้าใหม่</a></div>
									<?php } ?>
								</div>
							</div>
						</div>
					</div>
			</div>
		</div>
	</div>
</div>
<?php include "include/inc_footer.php"; ?> 
<script type="text/javascript">
	$(".btnAddCus").click(function() {
		// console.log($("#brand").val());
		// $("#call_make").val($("#brand").val());
		// $("#call_model").val($("#model").val());
		// $("#call_cc").val($("#cc").val());
		// $("#call_year").val($("#year").val());
		// $("#call_class").val($("#class").val());
		// $("#call_repair").val($("#repair").val());
		$('#sendPOCustomer').modal('show'); 
	});
</script>