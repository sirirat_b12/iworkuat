<?php 

session_start();
if(!isset($_SESSION["User"]['UserCode'])){
	echo "<script>alert('กรุณาเข้าสู่ระบบ!!!');</script>";
	echo '<META http-equiv="refresh" content="0;URL=login.php">';
	exit();
} 
if($_SESSION["User"]['type'] != "Sale" && $_SESSION["User"]['type'] != "SuperAdmin"){
	echo '<META http-equiv="refresh" content="0;URL=index.php">';
	exit();
}
header('Content-Type: text/html; charset=utf-8');
include "include/inc_header.php"; 
include "include/inc_menu.php"; 
include "include/inc_function_offer.php"; 

$UserCode = $_SESSION["User"]['UserCode'];
$UserType = $_SESSION["User"]['type'];
$cpid = $_GET["cpid"];
$getMyUserAll = getMyUserAll();
$getInsuranceGroup = getInsuranceGroup();
$getTitle = getTitle();
$getProvinceAll = getProvinceAll();
$getReferralType = getReferralType();
$getCompareStatusAll = getCompareStatusAll();
$getRejectReason = getRejectReason();
$getcpAll = getRowCPAllBycode($cpid);
$getProvinceAll = getProvinceAll();
$getCarMakeAll = getCarMakeAll();
$getCarModelAll = getCarModelAll($getcpAll["Make_Name_TH"]);
$getFollowupBycpid = getFollowupBycpid($cpid);
$getFollowupType = getFollowupType();
$getFollowupStatus = getFollowupStatus();
$getCoverageItem = getCoverageItemList($cpid);
$getCompareOption = getCompareOption($cpid);
$CoverageValue = $getCoverageItem["CoverageValue"];
$CoverageList = $getCoverageItem["CoverageList"];
ksort($CoverageList);

// echo "<br><br><pre>".print_r($CoverageList,1)."</pre>"; 
// exit();
// echo $getcpAll["CP_Date"]->format("Y-m-d");
?> 
<div class="main">
	<div class="main-content">
		<div class=" bgff"> 
			<div class="row m0" >
				<div class="col-md-6" >
					<form action="include/inc_action_offer.php" method="post" name="frmeditCP" id="frmAddCP" class="frm">
						<input type="hidden" name="action" value="updateCPDetail">
						<input type="hidden" name="CP_ID" value="<?php echo $getcpAll["CP_ID"]; ?>">
						<div class="col-md-12" >
							<div class="cff2da5"><h4>ข้อเสนอกรมธรรม์ <?php echo $getcpAll["CP_ID"]; ?></h4> </div>
								<div class="fs14">
									<div >
										<b><?php echo $getcpAll["Title_Name"]." ".$getcpAll["Customer_FName"]." ".$getcpAll["Customer_LName"] ; ?></b>
										<b>โทรศัพท์ : </b><?php echo $getcpAll["Tel_No"] ? $getcpAll["Tel_No"] : "-";?> | <?php echo $getcpAll["Mobile_No"] ? $getcpAll["Mobile_No"] : "-";?>
										<b>จากเลขที่ใบสั่งซื้อ : </b><?php echo $getcpAll["CP_PO_ID"] ? $getcpAll["CP_PO_ID"] : "-"; ?>
									</div>
									<div class="fs12">
										<div><b>วันที่สร้าง :</b> <?php echo $getcpAll["Create_Date"]->format("d-m-Y H:i:s"); ?> | <b>ผู้สร้าง :</b> <?php echo $getcpAll["Create_By"]; ?></div>
										<div><b>วันที่แก้ไข :</b> <?php echo $getcpAll["Update_Date"]->format("d-m-Y H:i:s"); ?> | <b>ผู้แก้ไข :</b> <?php echo $getcpAll["Update_By"]; ?></div>
									</div>
								</div>
								<div class="row mt15">
									<div class="col-md-4">
										<label for="CP_Date">วันที่ทำข้อเสนอ *</label>
										<input type="date" value="<?php echo $getcpAll["CP_Date"]->format("Y-m-d"); ?>" class="form-control formInput2" name="CP_Date" required>
									</div>
									<div class="col-md-4">
										<label for="CoverageStartDate">เริ่มคุ้มครอง *</label>
										<input type="date" class="form-control formInput2" name="Coverage_Start_Date" value="<?php echo ($getcpAll["Coverage_Start_Date"]->format("Y") != "1900") ? $getcpAll["Coverage_Start_Date"]->format("Y-m-d") : ""; ?>" required>
									</div>
									<div class="col-md-4">
										<label for="CoverageEndDate">สิ้นความคุ้มครอง *</label>
										<input type="date" class="form-control formInput2" name="Coverage_End_Date" value="<?php echo ($getcpAll["Coverage_End_Date"]->format("Y") != "1900") ? $getcpAll["Coverage_End_Date"]->format("Y-m-d") : ""; ?>" required>
									</div>
								</div>
								<div class="row mt15">
									<div class="col-md-6">
										<label for="Employee_ID">พนักงาน *</label>
										<select name="Employee_ID" id="Employee_ID" class="form-control formInput2" required <?php if($UserType=="Sale"){ echo "readonly"; } ?> >
											<option value="">:: กรุณาเลือก ::</option>
											<?php foreach ($getMyUserAll as $key => $value) { ?>
													<option value="<?php echo  $value["User_ID"] ?>" <?php if($value["User_ID"] == $getcpAll["Employee_ID"]){ echo "selected"; } ?>>
														<?php echo  $value["User_ID"]." ".$value["User_FName"]." ".$value["User_LName"]; ?>
													</option>
											<?php } ?>
										</select>
									</div>
									<div class="col-md-6">
										<label for="Insurance_Group_ID">กลุ่มประเภทประกัน</label>
										<select name="Insurance_Group_ID" id="Insurance_Group_ID" class="form-control formInput2">
											<option value="">:: กรุณาเลือก ::</option>
											<?php foreach ($getInsuranceGroup as $key => $value) { ?>
													<option value="<?php echo  $value["Insurance_Group_ID"] ?>" <?php if($value["Insurance_Group_ID"] == $getcpAll["Insurance_Group_ID"]){ echo "selected"; } ?> >
														<?php echo  $value["Insurance_Group_Initials"]." ".$value["Insurance_Group_Detail"]; ?>
													</option>
											<?php } ?>
										</select>
									</div>
								</div>
								<div class="row mt15">
									<div class="col-md-6">
										<label >สถานะการติดตาม </label><br>
										<input type="radio" name="Active" value="Y" <?php if($getcpAll["Active"]=="Y"){ echo "checked"; } ?> > <span class="fs12">ติดต่อได้</span> 
										<input type="radio" name="Active" value="N" <?php if($getcpAll["Active"]=="N"){ echo "checked"; } ?>> <span class="fs12">ติดต่อไม่ได้</span>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label for="Compare_Status_ID">สถานะข้อเสนอ </label><br>
											<select id="Compare_Status_ID" class="form-control formInput2 formInput" name="Compare_Status_ID" required>
												<option value="">:: กรุณาเลือก ::</option>
												<?php foreach ($getCompareStatusAll as $key => $value) { ?>
														<option value="<?php echo  $value["Compare_Status_ID"] ?>" <?php if($value["Compare_Status_ID"] == $getcpAll["Compare_Status_ID"]){ echo "selected"; } ?> >
															<?php echo  $value["Compare_Status"]; ?>
														</option>
												<?php } ?>
						          </select>
										</div>
									</div>
								</div>
								<div class="row <?php if($getcpAll["Compare_Status_ID"] != "REJ"){echo "dn"; }?>" id="boxfrmReject">
									<div class="col-md-6">
										<div class="form-group">
											<label for="Reject_Reason_ID">เหตุผลที่ยกเลิก </label><br>
											<select id="Reject_Reason_ID" class="form-control formInput2 formInput" name="Reject_Reason_ID">
												<option value="">:: กรุณาเลือก ::</option>
												<?php foreach ($getRejectReason as $key => $value) { ?>
														<option value="<?php echo  $value["Reject_Reason_ID"] ?>" <?php if($value["Reject_Reason_ID"] == $getcpAll["Reject_Reason_ID"]){ echo "selected"; } ?> >
															<?php echo  $value["Reject_Reason"]; ?>
														</option>
												<?php } ?>
						          </select>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label for="CompareStatus">รายละเอียดการยกเลิก </label><br>
											<textarea name="Reject_Reason_Desc" class="form-control fs12"><?php echo  $getcpAll["Reject_Reason_Desc"]; ?></textarea>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<div class="form-group">
											<label for="CompareStatus">หมายเหตุ </label><br>
											<textarea name="Remark" class="form-control fs12"><?php echo  $getcpAll["Remark"]; ?></textarea>
										</div>
									</div>
								</div>			
						</div>
						<div class="col-md-12 mb50" >
							<div class="cff2da5"><h4>รายละเอียดรถยนต์ </h4> </div>
								<div class="row">
									<div class="col-md-4">
										<label for="CPDate">เลขทะเบียน</label>
										<input type="text" value="<?php echo $getcpAll["Plate_No"]; ?>" class="form-control formInput2" name="Plate_No" >
									</div>
									<div class="col-md-4">
										<label for="Plate_Province">ทะเบียนจังหวัด</label>
										<select name="Plate_Province" id="Plate_Province" class="form-control formInput2" >
											<option value="">:: กรุณาเลือก ::</option>
											<?php foreach ($getProvinceAll as $key => $value) { ?>
													<option value="<?php echo  $value["Province_ID"] ?>" <?php if($value["Province_ID"] == $getcpAll["Plate_Province"]){ echo "selected"; } ?>><?php echo  $value["Province_Name_TH"]; ?></option>
											<?php } ?>
										</select>
									</div>
									<div class="col-md-4">
										<label for="CarCode">รหัสรถยนต์</label>
										<input type="text" class="form-control formInput2" name="CarCode" value="<?php echo  $getcpAll["Car_Code"]; ?>" maxlength="3" onkeyup="chknum(this,event)">
									</div>
								</div>
								<div class="row mt15">
									<div class="col-md-4">
										<label for="Make_ID">ยี่ห้อรถ</label>
										<select name="Make_ID" id="Make_ID" class="form-control formInput2" onclick="changModel()" >
											<option value="">:: กรุณาเลือก ::</option>
											<?php foreach ($getCarMakeAll as $key => $value) { ?>
													<option value="<?php echo  $value["Make_ID"] ?>" <?php if($value["Make_ID"] == $getcpAll["Make_ID"]){ echo "selected"; } ?>><?php echo  trim($value["Make_Name_TH"]); ?></option>
											<?php } ?>
										</select>
									</div>
									<div class="col-md-4">
										<label for="Model_ID">รุ่น</label>
										<select name="Model_ID" id="Model_ID" class="form-control formInput2" >
											<option value="">:: กรุณาเลือก ::</option>
											<?php foreach ($getCarModelAll as $key => $value) { ?>
													<option value="<?php echo  str_replace(' ', '', $value["Model_ID"]); ?>" <?php if(trim($value["Model_ID"]) == $getcpAll["Model"]){ echo "selected"; } ?>>
														<?php echo trim($value["Model_Desc"]); ?>
													</option>
											<?php } ?>
										</select>
									</div>
									<div class="col-md-4">
										<label for="Model_ID2"></label>
										<input type="text" class="form-control formInput2" name="Model_ID2" value="<?php echo  $getcpAll["Model"]; ?>">
									</div>
								</div>
								<div class="row mt15">
									<div class="col-md-3">
										<label for="Sub_Model">รุ่นย่อย/ปี</label>
										<input type="text" class="form-control formInput2" name="Sub_Model" value="<?php echo  $getcpAll["Sub_Model"]; ?>">
									</div>
									<div class="col-md-3">
										<label for="Body_Type">ประเภทตัวถัง</label>
										<input type="text" class="form-control formInput2" name="Body_Type" value="<?php echo  $getcpAll["Body_Type"]; ?>">
									</div>
									<div class="col-md-3">
										<label for="Frame_No">เลขตัวถัง</label>
										<input type="text" class="form-control formInput2" name="Frame_No" value="<?php echo  $getcpAll["Frame_No"]; ?>">
									</div>
									<div class="col-md-3">
										<label for="Seat_CC_Weight">ที่นั่ง/ซีซี/น้ำหนัก</label>
										<input type="text" class="form-control formInput2" name="Seat_CC_Weight" value="<?php echo  $getcpAll["Seat_CC_Weight"]; ?>">
									</div>
								</div>
								<div class="row mt15">
									<div class="col-md-12">
										<label for="Seat_CC_Weight">อุปกรณ์ตกแต่ง</label>
										<textarea name="Accessory"  class="form-control "><?php echo $getcpAll["Accessory"]; ?></textarea>
									</div>
								</div>
								<div class="row mt15 t_c">
									<a href="offeredits.php?cpid=<?php echo $_GET["cpid"]?> " class="btn btn-danger">รีเซ็ต</a>
									<button type="submit" class="btn btn-success">แก้ไข</button>
								</div>			
						</div>
					</form>
				</div>
				<div class="col-md-6" >
					<div class="mt10">
						<h4 class="cff2da5 dib">รายละเอียดการติดตาม</h4>
						<input type="hidden" value="<?php echo $getcpAll["Customer_ID"]; ?>" id="Customer_ID">
						<input type="hidden" value="<?php echo $getcpAll["Employee_ID"]; ?>" id="Employee_ID">
						<input type="hidden" value="pageEdit" id="pageaction">
						<!-- <span class="cursorPoin" style="color: #00bc08;" onclick="closeFollowup(1)"><i class="fa fa-plus"></i> เพิ่มการติดตาม</span> -->
						<div class="mb15 " style="background-color: #fffef2;">
							<div  class="p10 dn" id="boxAddFollow">
								<div class="row mt5">
									<div class="col-md-6 mt5">
										<label class="cff2da5" >วัน-เวลาที่ติดตาม</label>
										<input type="date" name="FollowupDateTime"  id="FollowupDateTime" class="fs12 form-control" value="<?php echo date("Y-m-d"); ?>">
									</div>
									<div class="col-md-6 mt5">
										<label for="FollowupTypeID" class="cff2da5" >ประเภทการติดตาม</label>
										<select name="FollowupTypeID" id="FollowupTypeID" class="fs12 form-control">
											<option value="">::: กรุณาเลือก :::</option>
										<?php foreach ($getFollowupType as $key => $value) { ?>
													<option value="<?php echo $value["Followup_Type_ID"]; ?>" <?php if($value["Followup_Type_ID"] == "003" ){ echo "selected"; } ?> >
														<?php echo $value["Followup_Type_Desc"]; ?>
													</option>
										<?php	} ?>
										</select>
									</div>
								</div>
								<div class="row mt5">
									<div class="col-md-12">
										<label  class="cff2da5" >รายละเอียดการติดตาม</label>
										<textarea name="FollowupDetail" id="FollowupDetail" class="fs12 form-control"></textarea>
									</div>
								</div>
								<div class="row mt5">
									<div class="col-md-4">
										<label  class="cff2da5" >สถานะการติดตาม</label>
										<select name="FollowupStatus" id="FollowupStatus" class="fs12 form-control" onchange="changeStatus()">>
											<option value="">::: กรุณาเลือก :::</option>
											<?php foreach ($getFollowupStatus as $key => $value) { ?>
												<option value="<?php echo $value["Followup_Status_ID"]; ?>"><?php echo $value["Followup_Status_Desc"]; ?></option>
											<?php	} ?>
										</select>
									</div>
									<div class="col-md-8 dn" id="BoxRemindDate">
										<div class="col-md-6">
											<label for="RemindDate" class="cff2da5" >วัน-เวลาที่นัดหมาย</label>
											<input type="date" name="RemindDate" id="RemindDate" class="fs12 form-control"  >
										</div>
										<div class="col-md-6">
											<select name="Remindtime1" id="Remindtime1" class="fs12 form-control fl mr10" style="width: 40%;margin-top: 18px;">
												<?php for ($i=6; $i <= 20; $i++) {  ?>
													<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
												<?php	} ?>
											</select>
											<select name="Remindtime2" id="Remindtime2" class="fs12 form-control ml5" style="width: 40%;margin-top: 18px;">
												<option value="15">15</option>
												<option value="30">30</option>
												<option value="45">45</option>
											</select>
										</div>
									</div>
									<div class="col-md-8 dn" id="BoxRejectReason">
										<label for="RejectReason" class="cff2da5" >เหตุผลที่ยกเลิก</label>
										<select name="RejectReason<?php echo $_POST["cpid"]; ?>" id="RejectReason<?php echo $_POST["cpid"]; ?>" class="fs12 form-control" >
											<option value="">::: กรุณาเลือก :::</option>
											<?php foreach ($getRejectReason as $key => $value) { ?>
												<option value="<?php echo $value["Reject_Reason_ID"]; ?>"><?php echo $value["Reject_Reason"]; ?></option>';
										 <?php } ?>
										</select>
									</div>
								</div>
								<div class="row t_c mt15">
									<button type="button" class="btn btn-danger" onclick="closeFollowup(0)">ยกเลิก</button>
									<button type="button" class="btn btn-success" onclick="addFollowup('<?php echo $cpid ?>')">เพิ่ม</button>
								</div>
							</div>
							<div  class="p10 dn" id="boxEditFollow">
								<input type="hidden"  id="Followup_ID">
								<div class="row mt5">
									<div class="col-md-6 mt5">
										<label class="cff2da5" >วัน-เวลาที่ติดตาม</label>
										<input type="date" name="FollowupDateTime"  id="editFollowupDateTime" class="fs12 form-control" >
									</div>
									<div class="col-md-6 mt5">
										<label class="cff2da5" >ประเภทการติดตาม</label>
										<select name="editFollowupTypeID" id="editFollowupTypeID" class="fs12 form-control">
											<option value="">::: กรุณาเลือก :::</option>
										<?php foreach ($getFollowupType as $key => $value) { ?>
													<option value="<?php echo $value["Followup_Type_ID"]; ?>" <?php if($value["Followup_Type_ID"] == "003" ){ echo "selected"; } ?> >
														<?php echo $value["Followup_Type_Desc"]; ?>
													</option>
										<?php	} ?>
										</select>
									</div>
								</div>
								<div class="row mt5">
									<div class="col-md-12">
										<label  class="cff2da5" >รายละเอียดการติดตาม</label>
										<textarea name="FollowupDetail" id="editFollowupDetail" class="fs12 form-control"></textarea>
									</div>
								</div>
								<div class="row mt5">
									<div class="col-md-4">
										<label  class="cff2da5" >สถานะการติดตาม</label>
										<select name="FollowupStatus" id="editFollowupStatus" class="fs12 form-control" >
											<option value="">::: กรุณาเลือก :::</option>
											<?php foreach ($getFollowupStatus as $key => $value) { 
															if($value["Followup_Status_ID"] != "009" && $value["Followup_Status_ID"] != "010"){
												?>
												<option value="<?php echo $value["Followup_Status_ID"]; ?>"><?php echo $value["Followup_Status_Desc"]; ?></option>
											<?php	 }
											} ?>
										</select>
									</div>
									<div class="col-md-8 dn" id="BoxeditRemindDate">
										<div class="col-md-6">
											<label for="RemindDate" class="cff2da5" >วัน-เวลาที่นัดหมาย</label>
											<input type="date" name="RemindDate" id="editRemindDate" class="fs12 form-control"  >
										</div>
										<div class="col-md-6">
											<select name="Remindtime1" id="editRemindtime1" class="fs12 form-control fl mr10" style="width: 40%;margin-top: 18px;">
												<?php for ($i=6; $i <= 20; $i++) {  ?>
													<option value="<?php echo str_pad($i,2,"0",STR_PAD_LEFT); ?>"><?php echo $i; ?></option>
												<?php	} ?>
											</select>
											<select name="Remindtime2" id="editRemindtime2" class="fs12 form-control ml5" style="width: 40%;margin-top: 18px;">
												<option value="00">00</option>
												<option value="15">15</option>
												<option value="30">30</option>
												<option value="45">45</option>
											</select>
										</div>
									</div>
								</div>
									<div class="dn" id="txtedit" >... กรุณารอ ...</div>
									<div class="row t_c mt15" id="btnedit">
										<button type="button" class="btn btn-danger" onclick="closeFollowup(2)">ยกเลิก</button>
										<button type="button" class="btn btn-success" onclick="updateFollowlist('<?php echo $cpid?>')">แก้ไข</button>
									</div>
							</div>
						</div>
						<div >
							<table id="tablefollow" class="table p5 table-bordered table-hover" >
								<thead class="fs12 c000000 p5">
									<tr class="cff2da5">
										<th class="t_c"><span class="cursorPoin" style="color: #00bc08;" onclick="closeFollowup(1)"><i class="fa fa-plus"></i></span></th>
										<th class="t_c">วันที่ติดตาม</th>
										<th class="t_c">รายละเอียด</th>
										<th class="t_c" style="white-space: nowrap;">ผลการติดตาม</th>
										<th class="t_c" style="white-space: nowrap;">นัดหมายครั้งถัดไป</th>
									</tr>
								</thead>
								<tbody class="fs12 c000000" id="tablelist">
									<?php foreach ($getFollowupBycpid as $key => $value) {
										$Remind_Date = $value["Remind_Date"] ? $value["Remind_Date"]->format("d/m/Y H:i") : "-";
									?>
											<tr>
												<td class="t_c" >
											<?php	if($value["Followup_Status_ID"] != '009' && $value["Followup_Status_ID"] != '010'){ ?>
													<span class="c2457ff fs14 cursorPoin"  onclick="editFollow('<?php echo $value["Followup_ID"]?>')"><i class="fa fa-edit fs14"></i></span>
											<?php	} ?>
												</td>
												<td class="t_c"><?php echo $value["Followup_DateTime"]->format("d/m/Y"); ?></td>
												<td class="t_l"><?php echo $value["Followup_Detail"]; ?></td>
												<td class="t_c" style="width: 140px;"><?php echo $value["Followup_Status_Desc"]; ?></td>
												<td class="t_c"><?php echo $Remind_Date; ?></td>
											</tr>
									<?php	} ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="row m0" >
				<div class="p2 t_c " style="background-color: #8fd9fc;">
					<h4 >ข้อเสนอกรมธรรม์ ความคุ้มครอง</h4>
					<a href="chkinser.php?cpid=<?php echo $cpid ?>" class="cff2da5"><i class="fa fa-plus"></i> แก้ไขข้อเสนอ</a>
					<?php if(count($getCompareOption) > 0 && count($getCompareOption) <= 3){ ?>
					 |  <a href="export_cp.php?cpid=<?php echo $cpid ?>" target="_bank" class="cff2da5"><i class="fas fa-file-pdf-o" ></i> ใบเสนอราคา</a>
					<?php } ?>
				</div> 
				<div>
					<?php if(count($getCompareOption)){ ?>
						<table id="tablecp" class="table p5 table-bordered table-hover" >
							<thead class="fs16 " style="background-color: #c9eeff;">
								<tr>
									<th class="t_c " >ความคุ้มครอง</th>
									<?php foreach ($getCompareOption as $key => $value) { ?>
										<th class="t_c "><?php echo $value["option"]["Insurer_Initials"] ?> </th>
									<?php } ?>
								</tr>
							</thead>
							<tbody class="fs12">
								<tr>
									<th class="t_c c000000 fs14" ></th>
									<?php foreach ($getCompareOption as $key => $value) { ?>
										<th class="t_c c000000 fs14">
											<?php if($_SESSION["User"]['type'] == "SuperAdmin"){ ?>
											<span class="cursorPoin c00ac0a" onclick="craeatePO('<?php echo $cpid ?>','<?php echo $value["option"]["CPO_ID"] ?>')">ออก PO </span> | 
											<?php } ?>
											<span class="cursorPoin cff2da5" onclick="btnShowmodel('<?php echo $value["option"]["CPO_ID"] ?>','<?php echo $cpid ?>')" data-id="<?php echo $value["option"]["CPO_ID"] ?>" data-cpid="<?php echo $cpid ?>"> <i class="fa fa-edit" ></i> แก้ไข</span>
											| <span class="cursorPoin" style="color: #ff0000;" onclick="delCoverge('<?php echo $cpid ?>','<?php echo $value["option"]["CPO_ID"] ?>')"><i class="fa fa-minus-square " ></i> ลบ</span>
									<?php } ?>
								</tr>
								<tr>
									<td class="t_c" width: 25%;>ประเภทประกัน</td>
									<?php foreach ($getCompareOption as $key => $value) { 
											$repair = ($value["option"]["Repair_Type"] == "C") ? "ซ่อมห้าง":"ซ่อมอู่";
											$Current_Or_Proposal = ($value["option"]["Current_Or_Proposal"] == "C") ? "ความคุ้มครองเดิม":"ข้อเสนอใหม่";
									?>
										<td class="t_c">
											<div><?php echo $Current_Or_Proposal." | ".$repair; ?></div>
											<div><?php echo $value["option"]["Insurance_Name"]; ?></div>
											<div><?php echo $value["option"]["Insurance_Package_Name"] ?></div>
										</td>
									<?php } ?>
								</tr>
								<?php foreach ($CoverageList as $key => $value) { ?>
									<tr class="<?php if($value["item"] == "G"){echo "fwb bgf5"; }else { echo "pl30"; }?>">
											<td class="t_l <?php if($value["item"] == "G"){echo "fwb bgf5"; }else { echo "pl30"; }?>"><?php echo $value["desc"];?></td>
												<?php 
													for ($i=0; $i < count($getCompareOption) ; $i++) { 
														if($value["item"] == "I"){
															$price = (intval($getCompareOption[$i]["item"][$value["Coverage_Item_ID"]]) != "") ? number_format($getCompareOption[$i]["item"][$value["Coverage_Item_ID"]],2) : "-";  
															echo '<td class="t_r ">'.number_format($getCompareOption[$i]["item"][$value["Coverage_Item_ID"]],2).'</td>';
														}else{
															echo '<td class="t_r "></td>';
														}
													}
												?>
									</tr>
								<?php } ?>
									<tr class="bgf6fbff">
										<td class="t_r"><b>เบี้ยประกันภัยสุทธิ</b></td>
										<?php foreach ($getCompareOption as $key => $value) { ?>
											<td class="t_r fwb c00ac0a"><?php echo number_format($value["option"]["Net_Premium"],2); ?></td>
										<?php } ?>
									</tr>
									<tr class="bgf6fbff">
										<td class="t_r"><b>อากร</b></td>
										<?php foreach ($getCompareOption as $key => $value) { ?>
											<td class="t_r fwb c00ac0a"><?php echo number_format($value["option"]["Duty"],2); ?></td>
										<?php } ?>
									</tr>
									<tr class="bgf6fbff">
										<td class="t_r"><b>ภาษี</b></td>
										<?php foreach ($getCompareOption as $key => $value) { ?>
											<td class="t_r fwb c00ac0a"><?php echo number_format($value["option"]["Tax"],2); ?></td>
										<?php } ?>
									</tr>
									<tr class="bgf6fbff">
										<td class="t_r"><b>เบี้ยประกันภัยรวม</b></td>
										<?php foreach ($getCompareOption as $key => $value) { ?>
											<td class="t_r fwb c00ac0a"><?php echo number_format($value["option"]["Total_Premium"],2); ?></td>
										<?php } ?>
									</tr>
									<tr class="bgf6fbff">
										<td class="t_r"><b>เบี้ยประกันภัยภาคบังคับ</b></td>
										<?php foreach ($getCompareOption as $key => $value) { ?>
											<td class="t_r fwb c00ac0a"><?php echo number_format($value["option"]["Compulsory"],2); ?></td>
										<?php } ?>
									</tr>
									<tr class="bgf6fbff">
										<td class="t_r"><b>ส่วนลดพิเศษ</b></td>
										<?php foreach ($getCompareOption as $key => $value) { ?>
											<td class="t_r fwb c00ac0a"><?php echo number_format($value["option"]["Discount"],2); ?></td>
										<?php } ?>
									</tr>
									<tr class="bgf6fbff">
										<td class="t_r"><b>เบี้ยประกันภัย (รวม พรบ.)</b></td>
										<?php foreach ($getCompareOption as $key => $value) { ?>
											<td class="t_r fwb c00ac0a"><?php echo number_format($value["option"]["Premium_After_Disc"],2); ?></td>
										<?php } ?>
									</tr>
							</tbody>
						</table>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade  bs-example-modal-lg" id="editConverage" tabindex="-1" role="dialog" aria-labelledby="editConverage">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header text-c">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="editConveragetitle"></h4>
			</div>
			<div class="modal-body">
				<form action="include/inc_action_offer.php" method="POST"  name="listProfile" >
					<input type="hidden" name="action"  value="editConverage">
					<input type="hidden" name="editcpid" id="editcpid" readonly>
					<input type="hidden" name="editoptionid" id="editoptionid" readonly>
					<input type="hidden" name="countDowns" id="countDowns" readonly>
					<div class="row">
						<div class="col-md-6">
							<div class="fwb">รายละเอียดการชำระเงิน</div>
							<div class="row">
								<div class="col-md-6 t_r mt5">เบี้ยประกันภัยสุทธิ</div>
								<div class="col-md-6"><input type="text" class="form-control formInput2 t_r" style="width: auto;" id="editNet_Premium" name="editNet_Premium" readonly></div>
							</div>
							<div class="row">
								<div class="col-md-6 t_r mt5">เบี้ยประกันภัยรวม</div>
								<div class="col-md-6"><input type="text" class="form-control formInput2 t_r" style="width: auto;" id="editTotal_Premium" name="editTotal_Premium" readonly></div>
							</div>
							<div class="row">
								<div class="col-md-6 t_r mt5">เบี้ยประกันภัยภาคบังคับ</div>
								<div class="col-md-6"><input type="text" class="form-control formInput2 t_r" style="width: auto;" id="editCompulsory" name="editCompulsory" readonly></div>
							</div>
							<div class="row">
								<div class="col-md-6 t_r mt5">ส่วนลดพิเศษ</div>
								<div class="col-md-6"><input type="text" class="form-control formInput2 t_r" style="width: auto;" id="editDiscount" name="editDiscount" readonly></div>
							</div>
							<div class="row">
								<div class="col-md-6 t_r mt5">เบี้ยประกันภัย (รวม พรบ.)</div>
								<div class="col-md-6"><input type="text" class="form-control formInput2 t_r" style="width: auto;" id="editPremium_After_Disc" name="editPremium_After_Disc" readonly></div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="fwb">การชำระเงิน</div>
							<div class="col-md-12 mb15">
								<label for="">การผ่อน</label>
								<select name="optionDiscount" id="optionDownMonth" class="form-control formInput2" onchange="seleltDownMonth()">
									<option value="">-- กรุณาเลือก --</option>
									<option value="1">ชำระเงินสดหรือบัตรโดยไม่ผ่อน</option>
									<option value="10">ผ่อนบัตรเครดิต 10 เดือน</option>
									<option value="3">ผ่อนเงินสด 3 เดือน</option>
									<option value="4">ผ่อนเงินสด 4 เดือน</option>
									<option value="6">ผ่อนเงินสด 6 เดือน</option>
								</select>
							</div>
							<div class="col-md-8 dn" id="boxNumDown">
								<label for="">ส่วนลด</label><br>
								<input type="radio" name="numDown" class="numDown" value="3"> <span class="fs12">ลด 3%</span>
								<input type="radio" name="numDown" class="numDown" value="5"> <span class="fs12">ลด 5%</span>
								<span id="disper5" class="dn"><input type="radio" name="numDown" class="numDown" value="7"> <span class="fs12">ลด 7%</span></span>
								<input type="radio" name="numDown" class="numDown" value="0"> <span class="fs12">ไม่ลด</span>
							</div>
							<div class="col-md-2"><span class="cursorPoin fwb btn btn-info" onclick="seleltDiscount()">เลือก</span></div>
							<div class="clearb pt20 cff2da5 " id="boxDetailDown"></div>
						</div>
					</div>
					<div class="t_c mt10">
						<button type="button" class="btn btn-default" data-dismiss="modal">ยกเลิก</button>
						<button type="submit" class="btn btn-success btnsendPOemail"><i class="fa fa-check-circle"></i> แก้ไขข้อมูล</button>
					</div>
				</form>
			</div>

		</div>
	</div>
</div>

<?php include "include/inc_footer.php"; ?> 

<script type="text/javascript">

	$('#editConverage').on('hidden.bs.modal', function () {
	 location.reload();
	});

function btnShowmodel(id,cpid){
	$.ajax({ 
		url: 'include/inc_action_offer.php',
		type:'POST',
		dataType: 'json',
		data: {action:"getRowOptions", cpid:cpid, optionid:id},
		success:function(rs){
			$("#editNet_Premium").val(rs.Net_Premium);
			$("#editTotal_Premium").val(rs.Total_Premium);
			$("#editCompulsory").val(rs.Compulsory);
			$("#editDiscount").val(rs.Discount);
			$("#editPremium_After_Disc").val(rs.Premium_After_Disc);
		}
	});
	$("#editConveragetitle").html(cpid+" Option: "+id);
	$('#editcpid').val(cpid); 
	$('#editoptionid').val(id); 
  $('#editConverage').modal('show'); 
};

function seleltDownMonth(){
	$("#boxDetailDown").html("");
	optionDownMonth = $("#optionDownMonth").val();
	$(".numDown").prop( "checked", false );
	if(optionDownMonth == "10"){
		$("#boxNumDown").hide();
	}else{
		$("#boxNumDown").show();
		if(optionDownMonth == "1"){
			$("#disper5").show();
		}else{
			$("#disper5").hide();
		}
	}
}

function seleltDiscount(){
	credit = 0.0;
	$("#boxDetailDown").html("");
	id = $("#editcpid").val();
	cpid = $("#editoptionid").val();
	NetPremium = $("#editNet_Premium").val();
	TotalPremium = $("#editTotal_Premium").val();
	Compulsory = parseFloat($("#editCompulsory").val());
	optionDownMonth = parseInt($("#optionDownMonth").val());
	numDown = $("input[name='numDown']:checked").val();
	$("#countDowns").val(optionDownMonth);

	if(optionDownMonth){
		if(optionDownMonth == 10){
			pay = parseFloat(TotalPremium) + parseFloat(Compulsory);
			credit = Math.ceil(Math.floor(pay) / 10);
			Premium_After_Disc = credit*10;
			dis = pay - Premium_After_Disc;
			$("#editPremium_After_Disc").val(Premium_After_Disc.toFixed(2));
			$("#editDiscount").val(dis.toFixed(2));
			$("#boxDetailDown").html("<b>ผ่อนบัตรเครดิต 10 เดือน</b><br>เดือนหละ "+numCommas(credit)+ " X 10 = <b>"+numCommas(Premium_After_Disc)+"</b> บาท");

		}else{
			if(numDown){
				countDis = optionDownMonth - 1;
				if(numDown>0){
					down = TotalPremium - ((NetPremium * numDown ) / 100);
					sumTotal = Math.round(down/100)*100;
				}else{
					down = Math.floor(TotalPremium) ;
					sumTotal = down;
				}
				down1 = Math.round((sumTotal * 40) / 100);
				down2 = Math.round((sumTotal - down1) / parseInt(countDis));
				dis = TotalPremium - sumTotal;
				Premium_After_Disc = parseFloat(sumTotal) + Compulsory;
				down1Compu = down1+Compulsory;
				console.log("sumTotal:"+sumTotal);
				console.log("sumTotal:"+sumTotal);
				$("#editPremium_After_Disc").val(Premium_After_Disc.toFixed(2));
				$("#editDiscount").val(dis.toFixed(2));
				if(optionDownMonth == 1){
					$("#boxDetailDown").html("<b>ชำระเงินสดหรือบัตรโดยไม่ผ่อน ยอดรวมทั้งหมด "+numCommas(sumTotal)+" บาท");
				}else{
					$("#boxDetailDown").html("<b>ผ่อนเงินสด "+optionDownMonth+" เดือน</b><br>งวดแรก "+numCommas(down1Compu)+ " บาท <br> งวดที่ 2-"+optionDownMonth+" งวดหละ "+numCommas(down2)+ " บาท");
				}
			}else{
				alert("กรุณาเลือกส่วนลด");
			}
		}
	}else{
		alert("กรุณาเลือกประเภทการผ่อนฃำระ");
	}
}

function chknum(ip,ek) {
		if(!isNaN(ip.value)){
			return true;
		}
		ip.value="";
		alert('กรุณากรอกเป็นหมายเลขเท่านั้น');
		return true;
	}

function ShowAddCus(rs){
	if(rs == 0){
		$("#BoxAddCustomer").slideUp();
	}else if(rs == 1){
		$("#BoxAddCustomer").slideDown();
	}else if(rs == 2){
		$("#BoxAddCustomer").slideDown();
	}
}

$("#Compare_Status_ID").change(function() {
	var val = $("#Compare_Status_ID").val();
	if(val == 'REJ'){
		$("#boxfrmReject").show();
	}else{
		$("#boxfrmReject").hide();
	}
});

function closeFollowup(rs){
	if(rs == 0){
		$("#boxAddFollow").slideUp();
		$("#boxEditFollow").slideUp();
	}else if(rs == 1){
		$("#boxAddFollow").slideDown();
		$("#boxEditFollow").slideUp();
	}else if(rs == 2){
		$("#boxEditFollow").slideUp();
		$("#boxAddFollow").slideUp();
	}
}

function changModel(){
	Model = $('#Make_ID').find(":selected").text();
	// console.log(Model);
	$.ajax({ 
		url: 'include/inc_action_offer.php',
		type:'POST',
		dataType: 'json',
		data: {action:"changGetModel", Model:Model},
		success:function(rs){
			console.log(rs);
			$("#Model_ID").empty();
			$("#Model_ID").append('<option value="0">:: กรุณาเลือก ::</option>');
			$.each( rs, function( key, val ) {
		    $("#Model_ID").append('<option value="'+val.Model_ID.replace(/ /g,"")+'">'+val.Model_Desc+'</option>');
		  });
		}
	});
}

function changeStatus(){
	var val = $('#FollowupStatus').val();
	// console.log(val);
	if(val == '009'){
		$("#BoxRejectReason").show();
		$("#BoxRemindDate").hide();
	}else if(val == '' || val == '010'){
		$("#BoxRejectReason").hide();
		$("#BoxRemindDate").hide();
	}else{
		$("#BoxRejectReason").hide();
		$("#BoxRemindDate").show();
	}
}

function craeatePO(cp, num){
	if(confirm("คุณต้องการสร้าง PO จาก "+cp+ " ข้อเสนอที่ "+num+"?")){

	}
}

function addFollowup(cpid){

	$("#btnAddFollowup").slideUp();
	$("#txtbtn").html("กรุณารอ...");
	customer = $("#Customer_ID").val();
	emp = $("#Employee_ID").val();
	pageaction = $("#pageaction").val();
	console.log(pageaction);
	FollowupDateTime = $("#FollowupDateTime").val();
	FollowupTypeID = $("#FollowupTypeID").val();
	FollowupDetail = $("#FollowupDetail").val();
	FollowupStatusID = $("#FollowupStatus").val();
	RemindDate = $("#RemindDate").val();
	Remindtime1 = $("#Remindtime1").val();
	Remindtime2 = $("#Remindtime2").val();
	RejectReason = $("#RejectReason").val();
	Remind = RemindDate+" "+Remindtime1+":"+Remindtime2;
	// console.log(cpid+" | " +FollowupDateTime+" | "+FollowupTypeID+" | "+FollowupStatusID+" | "+RemindDate+" | "+Remindtime1+" | "+Remindtime2+" | "+FollowupDetail);
	if(!FollowupDetail){
		alert("กรุณากรอกรายละเอียด");
	}else if(!FollowupStatusID){
		alert("กรุณาเลือกสถานะการติดตาม");
	}else if(FollowupStatusID && FollowupStatusID != '009' && FollowupStatusID != '010' && !RemindDate){
		alert("กรุณาเลือก วัน-เวลาที่นัดหมาย");
	}else if(FollowupStatusID == '009' && !RejectReason){
		alert("กรุณาเลือกเหตุผลที่ยกเลิก");
	}else{
		$.ajax({ 
			url: 'include/inc_action_offer.php',
			type:'POST',
			data: {action:"addFollowup", cpid:cpid, pageaction:pageaction, customer:customer, emp:emp, FollowupDateTime:FollowupDateTime, FollowupTypeID:FollowupTypeID, FollowupStatusID:FollowupStatusID, FollowupDetail:FollowupDetail, Remind:Remind, RejectReason:RejectReason},
			success:function(rs){
				// console.log(rs);
				$("#boxAddFollow").slideUp();
				$("#FollowupDetail").val("");
				$("#FollowupStatus").val("");
				$("#tablelist").empty();
				$("#tablelist").html(rs);
				$("#BoxRejectReason").hide();
				$("#BoxRemindDate").hide();
				if(rs != "0"){
					alert("เพิ่มการติดตาม  "+cpid+" เรียบร้อย");
				}
			}
		});	
	}
}

function editFollow(rs){
	// console.log(rs);
	$("#boxAddFollow").slideUp();
	$("#boxEditFollow").slideDown();
	$("#Followup_ID").val(rs);
	if(rs){
		$.ajax({ 
			url: 'include/inc_action_offer.php',
			type:'POST',
			dataType: 'json',
			data: {action:"geteditFollowup", flid:rs},
			success:function(rs){
				// console.log(rs);
				$("#editFollowid").val(rs.Followup_ID);
				$("#editFollowupDetail").val(rs.Followup_Detail);
				$("#editFollowupStatus").val(rs.Followup_Status_ID);
				$("#editFollowupTypeID").val(rs.Followup_Type_ID);
				$("#editFollowupDateTime").val(rs.FollowupDate);

				// // if(rs.Followup_Status_ID == "009"){
				// // 	$("#BoxRejectReason").show();
				// // 	$("#BoxRemindDate").hide();
				// // 	$("#editRejectReason").val(rs.RejectReason);

				// // }else if(rs.Followup_Status_ID == "010") {
				// // 	$("#BoxRejectReason").hide();
				// // 	$("#BoxRemindDate").hide();
				// // }else{
				// 	// $("#BoxeditRejectReason").hide();
					$("#BoxeditRemindDate").show();

					$("#editRemindDate").val(rs.RemindDate);
					$("#editRemindtime1").val(rs.Remindtime1);
					$("#editRemindtime2").val(rs.Remindtime2);
				// }
			}
		});
	}
}

function updateFollowlist(cpid){
	Followupid = $("#Followup_ID").val();
	pageaction = $("#pageaction").val();
	FollowupDateTime = $("#editFollowupDateTime").val();
	FollowupTypeID = $("#editFollowupTypeID").val();
	FollowupDetail = $("#editFollowupDetail").val();
	FollowupStatusID = $("#editFollowupStatus").val();
	RemindDate = $("#editRemindDate").val();
	Remindtime1 = $("#editRemindtime1").val();
	Remindtime2 = $("#editRemindtime2").val();
	Remind = RemindDate+" "+Remindtime1+":"+Remindtime2;
	if(!FollowupDetail){
		alert("กรุณากรอกรายละเอียด");
	}else if(!FollowupStatusID){
		alert("กรุณาเลือกสถานะการติดตาม");
	}else if(FollowupStatusID && FollowupStatusID != '009' && FollowupStatusID != '010' && !RemindDate){
		alert("กรุณาเลือก วัน-เวลาที่นัดหมาย");
	}else{
		$.ajax({ 
			url: 'include/inc_action_offer.php',
			type:'POST',
			data: {action:"updateFollowuplist", cpid:cpid, pageaction:pageaction, Followupid:Followupid, FollowupDateTime:FollowupDateTime, FollowupTypeID:FollowupTypeID, FollowupStatusID:FollowupStatusID, FollowupDetail:FollowupDetail, Remind:Remind},
			success:function(rs){
				// console.log(rs);
					
				if(rs){
					alert("แก้ไขกาาติดตาม เรียบร้อย");
					$("#boxEditFollow").slideUp();
					$("#tablelist").empty();
					$("#tablelist").html(rs);
				}
			}
		});
	}
}

function delCoverge(cp, num){
	if( confirm("คุณต้องการลบข้อเสนอ?")){
		$.ajax({ 
			url: 'include/inc_action_offer.php',
			type:'POST',
			// dataType: 'json',
			data: {action:"delCoverge", cpcode:cp, num:num},
			success:function(rs){
				console.log(rs);
				if(rs == 1){
					alert("ทำรายการสำเร็จ");
					window.location.reload(true);
				}
			}
		});
	}
}
</script>