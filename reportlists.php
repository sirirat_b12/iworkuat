<?php 
session_start();

if(!isset($_SESSION["User"]['UserCode'])){
	echo "<script>alert('กรุณาเข้าสู่ระบบ!!!');</script>";
	echo '<META http-equiv="refresh" content="0;URL=login.php">';
	exit();
}

include "include/inc_header.php"; 
// include "include/inc_function.php"; 
include "include/inc_menu.php"; 

$UserCode = $_SESSION["User"]['UserCode'];
$Operation = $_GET["Operation"];
$mounth = ($_GET["txtse"]) ? $_GET["txtse"] : date("n");
$getCountPOStatus = getCountPOStatus($UserCode, "All", $Operation, "List", $mounth);
// $apiMain = getAllCustomerMain($sale); 
 // echo "<pre>".print_r($getCountPOStatus,1)."</pre>";
?> 

<div class="main">
	<div class="main-content p20">
		<div class=" bgff"> 
			<div class="p20">
			<h3 class="text-center">รายชื่อลูกค้า</h3>
				<table id="table" class="table table-striped p5" data-toggle="table" data-pagination="true" data-search="true"  data-page-size="200" data-page-list="[200, 300, 400, 500]" data-show-columns="true" data-height="750" >
					<thead>
						<tr>
							<th data-sortable="true" data-field="pocode" data-sortable="true" class="t_c">PO</th>
							<th>ยอด</th>
							<th class="t_c">ลูกค้า</th>
							<th class="t_c">โทร</th>
							<th data-sortable="true" class="t_c">วันที่รับชำระงวดแรก</th>
							<th data-sortable="true" class="t_c">วันที่สั่งซื้อ</th>
							<th class="t_c">พนักงาน</th>
							<th class="t_c">การดำเนินการ</th>
							<th class="t_c">สถานะ</th>
						</tr>
					</thead>
					<tbody class="fs12">
						<?php 
								foreach ($getCountPOStatus as $key => $value) { 
						?>
							<tr>
								<td class="t_c fwb cff2da5"><?php echo $value["PO_ID"]; ?></td>
								<td class="t_r fwb"><?php echo number_format($value["Premium_After_Disc"],2); ?></td>
								<td class="t_l"><?php echo $value["Customer_FName"]." ".$value["Customer_LName"]; ?></td>
								<td class="t_c "><?php echo $value["Tel_No"]; ?></td>
								<td class="t_c fwb c0aaaef"><?php echo $value["Receive_Date"]->format("d-m-Y"); ?></td>
								<td class="t_c cff2da5 fwb "><?php echo $value["PO_Date"]->format("d-m-Y"); ?></td>
								<td class="t_c "><?php echo $value["Employee_ID"]; ?></td>
								<td class="t_c "><?php echo $value["Operation_Type_ID"]; ?></td>
								<td class="t_c "><?php echo $value["Status_ID"]; ?></td>
							</tr>
						<?php } ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<?php include "include/inc_footer.php"; ?> 
