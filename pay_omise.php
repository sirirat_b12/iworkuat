<?php 

session_start();
$UserCode = $_SESSION["User"]['UserCode'];
$userType = $_SESSION["User"]['type'];

if(!isset($UserCode)){
	echo "<script>alert('กรุณาเข้าสู่ระบบ!!!');</script>";
	echo '<META http-equiv="refresh" content="0;URL=login.php">';
	exit();
}

header('Content-Type: text/html; charset=utf-8');
include "include/inc_header.php"; 
include "include/inc_menu.php"; 
include "inc_omise/inc_function_omise.php";


if(isset($_GET["txtse"])){
	$getList_pay = getList_pay($UserCode, $_GET["txtse"]);
	// $paycose = $getList_pay[0]["paycode"];
}


function getList_pay($UserCode, $txtse=""){
	global $connMS;
	$resultarray = array();
	$txtse = trim($txtse);
	$sql = "SELECT * FROM pay_omise " ;
	if($txtse){
		$sql .=" WHERE Customer_ID = '".$txtse."' and  create_by = '".$UserCode."' ORDER BY create_date ASC";
	}else{
		$sql .=" WHERE create_by = '".$UserCode."' ORDER BY create_date ASC";
	}
	// echo $sql;
	$stmt = sqlsrv_query( $connMS, $sql );
    if(sqlsrv_has_rows($stmt)) {
	    while( $row = sqlsrv_fetch_array($stmt,SQLSRV_FETCH_ASSOC) ) { 
	      $resultarray[] = $row;
	    }
	    return $resultarray;
			exit();
		}
}



// echo "<pre>".print_r($_SESSION,1)."</pre>";
?>
<div class="main">
	<div class="">
		<div class="p20">
				<div class="row">
					<div class="col-md-12">
						<div class="panel">
								<div class="panel-heading">
									<h4 class="panel-title"><b>ระบบชำระเงิน Omise</b></h4>
								</div>
								<div class="panel-body fs14">
									<div class="row">
										<div class="col-md-5 t_r"><h4>รหัสลูกค้า :</h4></div>
										<div class="col-md-2">
											<input class="form-control formInput2" type="text" name="txtSearch" id="txtSearch"  value="<?php echo $_GET["txtsearch"] ?>" required>
										</div>
										<div class="col-md-2">
											<button type="button" id="btn_send_order" class="btn  btn-info"><i class="fa fa-search-plus" aria-hidden="true"></i> เช็คข้อมูล</button>
										</div>
										<div class="col-md-3 ">
											<div class="fs25 cf40053 fwb ">สำหรับสร้างการชำระ</div>
										</div> 
									</div>
									<div class="row mt20">
										<div class="col-md-12">
											<div class="row">
												<div class="col-md-2"></div><div class="col-md-10"><?php echo $value["PO_ID"]; ?></div>
											</div>
											<form action="inc_omise/inc_action_omise.php" method="POST" id="frmListPay">
												<input type="hidden" name="action" value="inserListpay">
												<input type="hidden" name="txtsearch_hidden" id="txtsearch_hidden" value="">
												<table class="table table-hover" id="tableMoveQC">
													<thead>
														<tr>
															<th class="t_c">
																<div><input type="checkbox" id="chkInsAll" name="chkInsAll" ></div>
															</th>
															<th class="t_c">งวดที่</th>
															<th class="t_c">PO</th>
															<th class="t_c">ชื่อ-นามสกุล</th>
															<th class="t_c">ประเภทประกัน</th>
															<th class="t_c">กำหนดชำระ</th>
															<th class="t_r">เบี้ยสุทธิ</th>
															<th class="t_r">อากร</th>
															<th class="t_r">ภาษี</th>
															<th class="t_r">เบี้ยรวม</th>
															<th class="t_r">ส่วนลด</th>
															<th class="t_r">จ่ายสุทธิ</th>
															<th class="t_c">สถานะของงวดชำระ</th>
															<!-- <th class="t_c">ผ่อนชำระ</th> -->
															<!-- <th class="t_c">QR Code</th> -->
															<!-- <th class="t_c" >หมายเหตุ</th> -->
														</tr>
													</thead>
													<tbody id="table_data" class="table-list fs13"><tr><td colspan="12" class="text-center">กรุณาเลือกข้อมูล</td></tr></tbody>
												</table>
												<div class="p10 cff2da5" style="background-color: #ebebeb;">
													<div class="row">



														<div class="col-md-4">
															<p class="fwb">หมายเหตุ</p>
															<input type="text" name="remark" id="txtremark"  class="form-control cff2da5" value="">
														</div>

														
														<div class="col-md-4">
															<p class="fwb">Phone</p>
															<input type="text" name="phone" id="txtphone" class="form-control cff2da5" >
														</div>
														<div class="col-md-4">
															<p class="fwb">Email</p>
															<input type="text" name="email" id="txtemail" class="form-control cff2da5" >
														</div>




													</div>
													<div class="row">
														<div id="Waittxt" class="fs20 t_c  fwb mt25 dn">กรุณารอ..</div>
														<div class="col-md-12 mt20 t_c">
															<input type="button" class="btn btn-success" id="btnsub" value="ตกลง" onclick="btnsubmit()">
															<a href="counters.php" class="btn btn-danger" id="btncancel">ยกเลิก</a>
														</div>
													</div>
												</div>
											</form>
										</div>
									</div>
								</div>
						</div>
					</div>
				</div>
		</div>
	</div>
	<center>แก้ เบอร์โทร sms ด้วย actionSendSMS -> /adbchk/inc_omise/inc_function_omise.php</center>
<!-- 
<br><center>แก้ config -> /adbchk/inc_omise/inc_action_omise.php</center>
<br><center>แก้ config -> /omise/include/config.php</center> --> 


<div class="">
		<div class="p20">
			<div class="row">
				<div class="col-md-12">
					<div class="panel">
							<div class="panel-heading">
								<div class="row mt15">
									<div class="col-md-2 cff2da5">
										<label for="filter">รหัสลูกค้า</label>
										<input type="text" class="cff2da5 form-control fs12" id="txtse" value="<?php echo trim($_GET["txtse"]); ?>">
									</div>
									<div class="col-md-7 ">
										<span class="btn btn-success mt20" onclick="filterPOcode()">ค้นหา</span>
										<a href="counters.php" class="btn btn-danger mt20">รีเช็ต</a>
									</div>
									<div class="col-md-3 ">
										<div class="fs25 cf40053 fwb ">ค้นหารายการที่สร้าง</div>
									</div>
								</div>
							</div>
							<div class="panel-body fs14">
									<table id="tablecp" class="table p5" data-toggle="table" data-pagination="true" data-page-size="100" data-page-list="[100, 200, 250, 300]">
										<thead class="fs13 c000000">
											<tr>
												<th class="t_c">#</th>
												<th class="t_c">Ref.1</th>
												<th class="t_c">Pay desc</th>
												<th class="t_c">จำนวน</th>
												<th class="t_c">รหัสลูกค้า</th>

												<th class="t_c">email</th>
												<th class="t_c">เบอร์ที่ส่ง sms</th>
												<th class="t_c">ผู้สร้าง</th>
												<th class="t_c">วันที่สร้าง</th>

												<th class="t_c">RV</th>
												<th class="t_c">Link</th>
												<th class="t_c">QR</th>
												<!-- 
												<th class="t_c">วันที่ชำระ</th>
												<th class="t_c">งวดชำระ</th>
												<th class="t_c">Ref.2</th>
												<th class="t_c">ชื่อ-นามสกุล</th>
												<th class="t_c">เบอร์</th>
												<th class="t_c">PO Code</th>
												<th class="t_c">ประเภท</th>
												<th class="t_c">วันครบชำระ</th> -->
											</tr>
										</thead>
										<tbody class="fs12 c000000">


										<?php 
									 
											// print_r($getList_pay);
											foreach ($getList_pay as $key => $value) {
echo "<tr>";
 

// $link_omise =  'https://adbwecare.com/omise/link_customer_iwork/index.php?linkbox=true&code='.base64_encode($value["reference_1"]);
$link_omise =  'https://adbwecare.com/omise/iwork_customer_qr/index.php?code='.base64_encode($value["reference_1"]);
?>
<!-- 
<td class="t_c cf80000 "> <span onclick="alert('ลบ');" class="cursorPoin"><i class="fas fa-trash"></i></span>  </td> -->
<?php
echo '<td class="t_c cf80000" colspan="1">'; 

echo '<a href="http://adbwecare.com/omise/webhook/update_rv.php?reference_1='.$value["reference_1"].'" target="_blank" >#</a>';
echo "</td>"; 

echo '<td class="t_c cf80000" colspan="1">'; 

echo '<a href="'.$link_omise.'" target="_blank" >'.$value["reference_1"].'</a>';
echo "</td>"; 

echo '<td class="t_c cf80000" colspan="1">';
echo $value["pay_description"] ;
echo "</td>"; 


echo '<td class="t_c cf80000" colspan="1">';
echo number_format($value["amount"],2) ;
echo "</td>"; 


echo '<td class="t_c cf80000" colspan="1">';
echo $value["Customer_ID"] ;
echo "</td>"; 


echo '<td class="t_c cf80000" colspan="1">';
echo $value["email"] ;
echo "</td>"; 


echo '<td class="t_c cf80000" colspan="1">';
echo $value["tel_sms"] ;
echo "</td>"; 


echo '<td class="t_c cf80000" colspan="1">';
echo $value["create_by"] ;
echo "</td>"; 


echo '<td class="t_c cf80000" colspan="1">';
echo $value["create_date"]->format("d/m/Y H:i:s");
echo "</td>"; 

echo '<td class="t_c cf80000" colspan="1">';
echo $value["Receive_ID"] ;
echo "</td>"; 


echo '<td class="t_c cf80000" colspan="1">'; 
echo '<a href="'.$value["short_url"].'" target="_blank" >Link</a>';
echo "</td>";

echo '<td class="t_c cf80000" colspan="1">'; 
echo '<a href="'.$link_omise.'" target="_blank" >QR</a>';
echo "</td>"; 

echo "</tr>"; 
											}
											?>
									</tbody>
								</table>
							</div>
					</div>
				</div>
			</div>
		</div>
	</div>




 





</div>
<?php include "include/inc_footer.php"; ?> 
<!-- <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script> -->
<script src="fancybox/dist/jquery.fancybox.min.js"></script>
<script type="text/javascript">
$('[data-fancybox]').fancybox({
	toolbar  : false,
	smallBtn : true,
	iframe : {
		preload : false
	}
})




// function fnDelCS(cs_id, paycode){
// 	if(confirm("คุณต้องการลบการชำระเงิน รหัส "+paycode)){
//   	$.ajax({ 
// 				url: 'counters/inc_action_counters.php',
// 				type:'POST',
// 				data: {action: 'deleteListcs', cs_id:cs_id, paycode:paycode},
// 				success:function(rs){ 
// 					// console.log(rs);
// 					if(rs == 1){
// 						alert("ลบการชำระเงิน "+paycode+" สำเร็๋จ");
// 						window.location.href = "counters.php?txtse="+paycode;
// 					}else{
// 						alert("ไม่สามารถลบการชำระเงิน "+paycode+" กรุณาลองใหม่");
// 						window.location.href = "counters.php?txtse="+paycode;
// 					}
// 				}
// 		});
// 	}
// }
  

function filterPOcode(){
	txtse = $("#txtse").val();
	if(txtse){
		window.location.href = "pay_omise.php?txtse="+txtse;
	}else{
		alert("กรุณากรอก ข้อมูล เพื่อค้นหา");
	}
}





// -- tor 28-09-2021

$("#btn_send_order").click(function() {
		$("#table_data").empty();
  	var txtsearch = $("#txtSearch").val();
  	if(txtsearch){
    	$.ajax({ 
					url: 'inc_omise/inc_action_omise.php',
					type:'POST',
					data: {action: 'GetListCS', txtsearch:txtsearch},
					success:function(rs){ 
						// console.log(rs);
						$('#txtsearch_hidden').val(txtsearch);
						$('#table_data').append(rs);


					
					}
			});
			$.ajax({ 
					url: 'inc_omise/inc_action_omise.php',
					type:'POST',
					dataType: 'json',
					data: {action: 'GetDataPhone', txtsearch:txtsearch},
					success:function(rs){ 
						// console.log(rs.phone);
						$("#txtphone").val(rs.phone);
						$("#txtemail").val(rs.email);
						$('#txtremark').val(rs.remark);
					}
			});
		}else{
			// alert("กรุณากรอกรหัสลูกค้าหรือเบอร์โทรศัพท์ลูกค้า");
			alert("กรุณากรอกรหัสลูกค้า");
		}
});

//check all 
$("#chkInsAll").click(function() {
  if($('#chkInsAll').is(':checked')){
    $(".inskey").prop('checked', true);
    
  }else{
    $(".inskey").prop('checked', false);
    
  }
});




// function -- 
function btnsubmit(){
	var countCK = $('input[name="inskey[]"]:checked').length;
	if(countCK > 0){
		
		// if( confirm("คุณต้องการสร้างการชำระเงินผ่านระบบ Omise?")){
		// if( confirm("ระบบกำลังจะส่งข้อความไปที่โทรศัพท์ของลูกค้าเพื่อชำระเงิน...เบอร์โทรถูกต้อง ? ")){
		if( confirm("กำลังจะส่งข้อความไปที่เบอร์ "+$("#txtphone").val())){
			$("#btnsub, #btncancel").hide();
			$("#Waittxt").show();
			$("#frmListPay").submit();
		}
	}else{
		alert("กรุณาเลือกงวดที่ชำระ");
		$("#btnsub, #btncancel").show();
		$("#Waittxt").hide();
	}
}





</script>