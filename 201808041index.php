<?php 
session_start();
// echo "<pre>".print_r($_SESSION,1)."</pre>";exit();
if(!isset($_SESSION["User"]['UserCode'])){
	// echo "<script>alert('กรุณาเข้าสู่ระบบ!!!');</script>";
	echo '<META http-equiv="refresh" content="0;URL=login.php">';
	exit();
}

include "include/inc_header.php"; 
include "include/inc_menu.php"; 
$UserCode = $_SESSION["User"]['UserCode'];
$getNotiComment = getNotiCommentByAddto($UserCode);
$dateNow = date("Y-m-d");
$dateNext = date("Y-m-d", strtotime($date . "+1 days"));
$getFollowupBydate = getFollowupBydate($dateNow, $UserCode);
$getFollowupBydateNext = getFollowupBydate($dateNext, $UserCode);
// $getDatePO = getDatePO($user);
// echo "<pre>".print_r($_SESSION,1)."</pre>";
//  $status = "OPN";
//     $Finance = "";
// $getCountPOStatus = getCountPOStatus($status, $Finance);
//  echo "<pre>".print_r($getCountPOStatus,1)."</pre>";
?> 
<div class="main">
	<div class="main-content">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-4">
					<div class="panel panel-scrolling" style="background-color: #fffdee;">
						<div class="panel-heading"><h3 class="panel-title">นัดหมาย วันนี้</h3></div>
						<div class="panel-body">
							<?php 
									if($getFollowupBydate){
										$i=1; 
										foreach ($getFollowupBydate as $key => $value) { 
							?>
								<div class="notiBoxIndex blogBox_1  moreBox_1 <?php if($i++ > 15){ echo "dn"; } ?>" >
									<div>
										<span class="fs10" style="color: #2457ff;"><?php echo $value["Remind_Date"]->format("Y-m-d h:i")." | ".$value["Compare_Policy_ID"]." | <b>นัดหมายครั้งถัดไป : </b> ".$value["Remind_Date"]->format("Y-m-d h:i")?></span> 
									</div>
									<div><sapn class="cf40053 fs12"><?php echo $value["Followup_Detail"]; ?> </sapn></div>
									<div><span class="fs10"><?php echo " <b>ประเภท: </b> ".$value["Followup_Type_Desc"]." | <b>สถานะ: </b> ".$value["Followup_Status_Desc"]." | <b>ผ่าน : </b> ".$value["Remind_Method_Desc"] ?></span> </div>
									<div>
										<span class="fs10"><?php echo " <b>ลุกค้า: </b> ".$value["Customer_FName"]." ".$value["Customer_LName"]." | ".$value["Tel_No"] ?>
										<?php if($value["EMail"]){ ?>
												<?php echo " | ".$value["EMail"] ?></span> 
											<?php } ?>
											</span> 
									</div>
								</div>
							<?php } ?>
								<div class="clearfix mt15">
									<button type="button" class="btn btn-primary btn-bottom center-block loadMore" id="loadMore_1" onclick="btnLoadMore(1)">เพิ่มเติม</button>
								</div>
							<?php } ?>
						</div>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="panel panel-scrolling" style="background-color: #e5f7fe;">
						<div class="panel-heading"><h3 class="panel-title">นัดหมาย พรุ่งนี้</h3></div>
						<div class="panel-body">
								<?php 
										if($getFollowupBydateNext){
											$i=1; 
											foreach ($getFollowupBydateNext as $key => $value) { 
								?>
									<div class="notiBoxIndex blogBox_2  moreBox_2 <?php if($i++ > 15){ echo "dn"; } ?>" >
										<div>
											<span class="fs10" style="color: #2457ff;"><?php echo $value["Remind_Date"]->format("Y-m-d h:i")." | ".$value["Compare_Policy_ID"]." | <b>นัดหมายครั้งถัดไป : </b> ".$value["Remind_Date"]->format("Y-m-d h:i")?></span> 
										</div>
										<div><sapn class="cf40053 fs12"><?php echo $value["Followup_Detail"]; ?> </sapn></div>
										<div><span class="fs10"><?php echo " <b>ประเภท: </b> ".$value["Followup_Type_Desc"]." | <b>สถานะ: </b> ".$value["Followup_Status_Desc"]." | <b>ผ่าน : </b> ".$value["Remind_Method_Desc"] ?></span> </div>
										<div>
											<span class="fs10"><?php echo " <b>ลุกค้า: </b> ".$value["Customer_FName"]." ".$value["Customer_LName"]." | ".$value["Tel_No"] ?>
											<?php if($value["EMail"]){ ?>
													<?php echo " | ".$value["EMail"] ?></span> 
												<?php } ?>
												</span> 
										</div>
									</div>
								<?php } ?>
								<div class="clearfix mt15">
									<button type="button" class="btn btn-primary btn-bottom center-block loadMore" id="loadMore_2" onclick="btnLoadMore(2)">เพิ่มเติม</button>
								</div>
							<?php } ?>
						</div>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="panel panel-scrolling" style="background-color: #e2ffe8;">
						<div class="panel-heading"><h3 class="panel-title">แจ้งเตือน ระบบแจ้งงาน</h3></div>
						<div class="panel-body">
							<?php $i=1; 
							foreach ($getNotiComment as $key => $value) { 
								if($_SESSION["User"]['type'] == "QualityControl"){
									$url = "sendorderslists.php?pocode=".$value["po_code"]."&numcode=".$value["noti_work_code"];
								}else{
									$url = "sendordersviews.php?pocode=".$value["po_code"]."&numcode=".$value["noti_work_code"];
								}
							?>
								<div class="notiBoxIndex blogBox_3  moreBox_3 <?php if($i++ > 10){ echo "dn"; } ?>" >
									<div><a href="<?php echo $url ?>"><sapn class="cf40053"><?php echo $value["comment"]; ?> </sapn></a></div>
									<div><span class="fs10"><?php echo $value["datetime"]." | ".$value["noti_work_code"]." | ".$value["po_code"]." | ".$value["comment_byname"]; ?></span> </div>
								</div>
							<?php } ?>
							<div class="clearfix mt15"><button type="button" class="btn btn-primary btn-bottom center-block "  id="loadMore_3" onclick="btnLoadMore(3)">เพิ่มเติม</button></div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="panel panel-headline">
						<div class="panel-heading">
							<h3 class="panel-title">ยอดขายในเดือน <?php echo date("F");?></h3>
							<!-- <p class="panel-subtitle">Period: Oct 14, 2016 - Oct 21, 2016</p> -->
						</div>
						<div class="panel-body">
							<div class="row">
								<div class="col-md-2">
									<div class="metric">
										<span >งานใหม่</span>
										<?php $caseNew = getCountPOStatus($UserCode, "All", "N"); ?>
										<p>
											<span class="number fs15"><?php echo number_format($caseNew["count"]); ?> / <?php echo number_format($caseNew["sumTotal"],2); ?></span>
											<!-- <span class="number fs15"><span id="totalCount"></span>/<span id="totalPrice"></span></span> -->
											<span class="title">จำนวน/เงิน</span>
										</p>
									</div>
								</div>
								<div class="col-md-2">
									<div class="metric">
										<span >ต่ออายุ</span>
										<?php $caseRenew = getCountPOStatus($UserCode, "All", "R"); ?>
										<p>
											<span class="number fs15"><?php echo number_format($caseRenew["count"]); ?> / <?php echo number_format($caseRenew["sumTotal"],2); ?></span>
											<span class="title">จำนวน/เงิน</span>
										</p>
									</div>
								</div>
								<div class="col-md-2">
									<div class="metric">
										<span >ลดหนี้</span>
										<?php $caseC = getCountPOStatus($UserCode, "All", "C"); ?>
										<p>
											<span class="number fs15"><?php echo number_format($caseC["count"]); ?> / <?php echo number_format($caseC["sumTotal"],2); ?></span>
											<!-- <span class="number fs15"><span id="totalCount"></span>/<span id="totalPrice"></span></span> -->
											<span class="title">จำนวน/เงิน</span>
										</p>
									</div>
								</div>
								<div class="col-md-2">
									<div class="metric">
										<span >เพิ่มหนี้</span>
										<?php $caseD = getCountPOStatus($UserCode, "All", "D"); ?>
										<p>
											<span class="number fs15"><?php echo number_format($caseD["count"]); ?> / <?php echo number_format($caseD["sumTotal"],2); ?></span>
											<span class="title">จำนวน/เงิน</span>
										</p>
									</div>
								</div>
								<!-- <div class="col-md-2">
									<div class="metric">
										<span >ยกเลิก</span>
										<?php $caseRV = getCountPOStatus($UserCode, "CBE"); ?>
										<p>
											<span class="number fs15"><?php echo number_format($caseRV["count"]); ?> / <?php echo number_format($caseRV["sumTotal"],2); ?></span>
											<span class="title">จำนวน/เงิน</span>
										</p>
									</div>
								</div> -->
								<div class="col-md-2">
									<div class="metric">
										<span >กำลังดำเนินการ</span>
										<?php $caseProcess = getCountPOStatus($UserCode, "process"); ?>
										<p>
											<span class="number fs15"><?php echo number_format($caseProcess["count"]); ?> / <?php echo number_format($caseProcess["sumTotal"],2); ?></span>
											<span class="title">จำนวน/เงิน</span>
										</p>
									</div>
								</div>
								<div class="col-md-2">
									<div class="metric">
										<span >รวมยอดขาย</span>
										<?php 
											$caseAllCount = $caseNew["count"] + $caseRenew["count"];
											$caseAllPrice = $caseNew["sumTotal"] + $caseRenew["sumTotal"]; 
										?>
										<p>
											<span class="number fs15"><?php echo number_format($caseAllCount); ?> / <?php echo number_format($caseAllPrice,2); ?></span>
											<span class="title">จำนวน/เงิน</span>
										</p>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="p15"><div id="headline-chart" class="ct-chart"></div></div>
								</div>
							</div>

						</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php include "include/inc_footer.php"; ?> 
<script src="assets/vendor/chartist/dist/chartist-plugin-axistitle.min.js"></script>
<script>
$( document ).ready(function () {
	var labelArray = [];
	var series = [];
	var seriescount = [];
	var code = "<?php echo $_SESSION["User"]['UserCode'];  ?>";
	var data, options;
	var totalCount = 0;
	var totalPrice = 0.00;
	$.ajax({ 
		url: 'include/inc_action_ms.php',
		type:'POST',
		dataType: 'json',
		data: {action: 'getDatePO', code:code},
		success:function(rs){  
			// console.log(rs);
			for (var i in rs){
				var date = new Date(rs[i].PO_Date.date);
				var day = ("0" + date.getDate()).slice(-2);
        labelArray.push(day);
        series.push(rs[i].sumTotal);
        // seriescount.push(rs[i].count);
       	// totalCount = totalCount + rs[i].count;
       	// totalPrice = parseFloat(totalPrice) + parseFloat(rs[i].sumTotal);
      }
      // console.log(totalPrice.toFixed(2));
      // $("#totalCount").html(numCommas(totalCount));
      // $("#totalPrice").html(numCommas(totalPrice.toFixed(2)));

	      data = {
					labels: labelArray,
					series: [ series]
				};

				options = {
					height: 450,
					showArea: true,
					showLine: true,
					showPoint: true,
					fullWidth: false,
					axisX: {
						showGrid: true
					},
					lineSmooth: true,
					chartPadding: {top: 20,right: 0,bottom: 20,left: 20},
					plugins: [ctPointLabels({ textAnchor: 'middle'}),
				    Chartist.plugins.ctAxisTitle({
                axisX: {
                    axisTitle: '<?php echo date("F");?>',
                    axisClass: 'ct-axis-title',
                    offset: {
                        x: 0,
                        y: 35
                    },
                    textAnchor: 'middle'
                },
                axisY: {
                    axisTitle: 'จำนวน',
                    axisClass: 'ct-axis-title',
                    offset: {
                        x: 0,
                        y: -1
                    },
                    flipTitle: false
                }
            })
				  ]
				};
				new Chartist.Line('#headline-chart', data, options);	
			}
	});

			
});

function ctPointLabels(options) {
  return function ctPointLabels(chart) {
	  var defaultOptions = {
	      labelClass: 'ct-label',
	      labelOffset: {
	          x: 0,
	          y: -10
	      },
	      textAnchor: 'middle'
	  };
	  
    options = Chartist.extend({}, defaultOptions, options);

    if (chart instanceof Chartist.Line) {
      chart.on('draw', function (data) {
          if (data.type === 'point') {
              data.group.elem('text', {
                  x: data.x + options.labelOffset.x,
                  y: data.y + options.labelOffset.y,
                  style: 'text-anchor: ' + options.textAnchor
              }, options.labelClass).text(data.value.y);  // 07.11.17 added ".y"
          }
      });
    }
  }
}

function btnLoadMore(id){
	loadMore = $('.loadMore').attr('id');
	if(id!=3){
		sh = 15;
	}else{
		sh = 10;
	}
	$(".moreBox").slice(0, sh).show();
	if ($(".blogBox:hidden").length != 0) {
		$("#loadMore").show();
	}	


	$(".moreBox_"+id+":hidden").slice(0, sh).slideDown();
	if ($(".moreBox_"+id+":hidden").length == 0) {
		$("#loadMore_"+id).fadeOut('slow');
	}
}
</script>