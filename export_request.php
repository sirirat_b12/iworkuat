<?php 
ini_set("memory_limit","1200M");
session_start();
if(!isset($_SESSION["User"]['UserCode'])){
	echo "<script>alert('กรุณาเข้าสู่ระบบ!!!');</script>";
	echo '<META http-equiv="refresh" content="0;URL=login.php">';
	exit();
}
header('Content-Type: text/html; charset=utf-8');
// include "include/inc_header.php"; 
include "inc_config.php";
include "include/inc_function.php";
include "include/inc_function_chk.php"; 

$pocode = $_GET["pocode"];
// $getIR = getInsuranceRequest($pocode);
$getPO = getPurchaseBycode($pocode);

if(!$getPO){
	echo '<META http-equiv="refresh" content="0;URL=ordersadmins.php">';
	exit();
}
$Cardetail = getCardetailBycode($pocode);
$getCoverageItem = getCoverageItem($pocode);

$Policy_No1 = $getPO["Policy_No"] ? $getPO["Policy_No"] : $getPO["Compulsory_No"];
$Policy_No = $Policy_No1 ? $Policy_No1 : '-';
$Notice_No = $getPO["Notice_No"] ? $getPO["Notice_No"] : "-";
$OIC_No = $getPO["OIC_No"] ? $getPO["OIC_No"] : "-";
if($getPO["cusAddr2"]){
	$eAddr = explode("|", $getPO["cusAddr2"]);
	if($eAddr[0]){
		$addr = "เลขที่ ".$eAddr[0];
	}if($eAddr[1]){
		$addr .= " อาคาร".$eAddr[1];
	}if($eAddr[2]){
		$addr .= " หมู่ที่ ".$eAddr[2];
	}if($eAddr[3]){
		$addr .= " ซอย ".$eAddr[3];
	}if($eAddr[4]){
		$addr .= " ถนน ".$eAddr[4];
	}
}else{
	$addr = $getPO["cusAddr1"];
}

$addressCus = $addr."<br>ตำบล/แขวง ".getSubdistrictBycode($getPO["cusSubdistrict"])." อำเภอ/เขต ".getDistrictBycode($getPO["cusDistrict"])."<br> จังหวัด ".getProvinceBycode($getPO["cusProvince"])." รหัสไปรษณีย์ ".$getPO["cusPost_Code"];
$DriverCase1 = (!$Cardetail["Driver_Name"]) ? " / " : "";
$DriverCase2 = ($Cardetail["Driver_Name"]) ? " / " : "";
$DriverDate = $Cardetail["Driver_DOB"]->format('Y-n-d H:i:s');
$Driver_DOB = explode(" ",$DriverDate);

$co_DriverDate = $Cardetail["Co_Driver_DOB"]->format('Y-n-d H:i:s');
$co_Driver_DOB = explode(" ",$co_DriverDate);


$repairG = ($Cardetail["Repair_Type"] == "G") ? 'checked="checked"' : '';
$repairC = ($Cardetail["Repair_Type"] == "C") ? 'checked="checked"' : '';
// if($getPO["Operation_Type_ID"] == "N"){
// 	$OperationN = "/";
// }else{
// 	$OperationR = "/";
// }

$getPOLast = getPurchaseBycode($getPO["Reference_ID"]);
if($getPO["Insurer_ID"] == $getPOLast["Insurer_ID"]){
	$OperationR = "/";
}else{
	$OperationN = "/";
}

$phoneInser = $getPO["Insurer_Account"] ? $getPO["Insurer_Account"] : "-";

// echo "<pre>".print_r($getCoverageItem,1)."</pre>";
// echo "<pre>".print_r($Cardetail,1)."</pre>";
include("mpdf/mpdf.php");

$mpdf = new mPDF('UTF-8','A4','','',5,5,5,5,10,10);
$mpdf->autoScriptToLang = true;

$html = '
<style>
	.container{
		font-family: thsaraban;
	    font-size: 16px;
	    line-height: 18px;
	    color:#000;
	}
	table{
		font-family: thsaraban;
	  font-size: 16px;
	  line-height: 15px;
	  color: #000;
	}
	p{
	    text-align: justify;
	}
	h1{
	    text-align: center;
	}
	.tableCar th{
		padding: 5px;
    border-bottom: 1px solid #000;
    border-top: 1px solid #000;
    border-right:1px solid #000;
    text-align:center;
  }
	.tableCar td {
		line-height: 15px;
		border-bottom: 1px solid #000;
		border-right:1px solid #000;
	}
	.tableCar th:last-child, .tableCar td:last-child{ border-right:none;}
	.{ font-size:10px;}
	.fs12{ font-size:12px;}
	.fs14{ font-size:14px;}
	.fs16{ font-size:16px;}
	.fs18{ font-size:18px;}
	.fs20{ font-size:20px;}
	.fwb{font-weight: bold;}
	.t_c{text-align: center;}
	.t_l{text-align: left;}
	.t_r{text-align: right;}
	.t_c{text-align: center;}
	.t_l{text-align: left;}
	.t_r{text-align: right;}
	.mt5{margin-top:5px;}
	.mt10{margin-top:10px;}
	.mt15{margin-top:15px;}
	.mt20{margin-top:20px;}
	.mt30{margin-top:30px;}
	.mt40{margin-top:40px;}
	.mt50{margin-top:50px;}
	.mb5{margin-bottom:5px;}
	.mb10{margin-bottom:10px;}
	.mb15{margin-bottom:15px;}
	.ml5{margin-left:5px;}
	.ml10{margin-left:10px;}
	.ml15{margin-left:15px;}
	.ml20{margin-left:20px;}
	.ml52{margin-left:52px;}
	.m0{margin:0px}
	p{margin:0}
	.fl{float: left; }
	.p5{padding:5px}
	.p10{padding:10px}
	.pt5{padding-top:5px}
	.pb10{padding-bottom:10px;}
	.bgea{background-color: #eaeaea;}
	.bor_1{border:1px solid #000;}
	.bor_t{border-top:1px solid #000;}
	.bor_l{border-left:1px solid #000;}
	.bor_r{border-right:1px solid #000;}
	.bor_b{border-bottom:1px solid #000;}
	.boxCoverage{
		line-height: 18px;
		float: left; 
		text-align:center; 
		width: 33.20%;
	}
</style>';
$html .='
		<div class="container">
			<div class="t_c">
				<p class="t_r fs10">'.date("Y-m-d H:i:s").'</p><img src="img/logo2.jpg" alt="" style="width:50px">
				<div class="fs16 fwb" style="margin-top:5px">บริษัท เอเชียไดเร็ค อินชัวร์รันส์ โบรคเกอร์ จำกัด</div>
				<div class="fs16">เลขที่ 626 อาคารบีบีดี (พระราม 4) ชั้น 11 ถนนพระรามที่ 4  แขวงมหาพฤฒาราม เขตบางรัก กรุงเทพฯ 10500  </div>
				<div class="fs14">โทร.02-089-2000 | แฟกซ์: 02-089-2088 | www.asiadirect.co.th | Line: @asiadirect | FB: AsiaDirectBroker</div>
			</div>
			<div class="p5 bor_1 mt10">
				<p class="t_c fs18"><b>คำขอเอาประกันภัยรถยนต์</b></p>
				<div class="fs18  p5 fl">
					<p>Agent : <b>ADB</b> | Agent Code: <b>'.$OIC_No.'</b> | เลขที่ : '.$pocode.' | เลขกรมธรรม์ : <b>'.$Policy_No.'</b> | เลขที่รับแจ้ง : <b>'.$Notice_No.'</b></p>
					<p style="">ผู้รับประกัน : <b>'.$getPO["Insurer_Name"].'</b> | เบอร์โทรศัพท์รับแจ้งเหตุ : <b>'.$phoneInser.'</b></p>
				</div>
			</div>
			<div class="bor_l bor_r " style="clear: both;">
				<div class="bor_r p5 fl t_r" style="width: 49%;height:150px; ">
					<p><b>ผู้ขอเอาประกันภัย</b> :</p>
					<p><b>ชื่อ :</b> '.$getPO["Title_Name"].' '.$getPO["FName"].' '.$getPO["LName"].' </p>
					<p><b>เลขบัตรประชาชน :</b> '.$getPO["Customer_Folder"].'</p>
					<p><b>โทรศัพท์ :</b> '.$getPO["CustomerTel"].'<p>
					<p ><b>ที่อยู่ :</b> '.$addressCus.'<p>
				</div>
				<div class="fl p5" style="width: 48%; height:150px;">
					<p>[ '.$OperationN.' ] ประกันใหม่ [ '.$OperationR.' ] ต่ออายุ</p>
					<p><b>ประเภทประกัน</b> : '.$getPO["Insurance_Name"].'</p>
					<p><b>การใช้รถยนต์</b> : '.$Cardetail["Car_Type_Desc"].'</p>
					<p><b>วันเริ่มคุ้มครอง</b> : '.$getPO["Coverage_Start_Date"]->format('d/m/Y').'</p>
					<p><b>วันสิ้นสุดความคุ้มครอง</b> : '.$getPO["Coverage_End_Date"]->format('d/m/Y').'</p>
					<p><b>Package</b> '.$getPO["Insurance_Package_Name"].'<p>
				</div>
			</div>
			<div class="bor_1 p5" style="clear: both;">
				<div>
					<p style="float: left;width: 35%;">[ '.$DriverCase1.' ] ไม่ระบุผู้ขับขี่ [ '.$DriverCase2.' ] ระบุผู้ขับขี่</p>
				</div>
			';
	if($Cardetail["Driver_Name"]){
		$html .= '
					<div>
						<p style="float: left; width: 40%;">ผู้ขับขี่ 1 : <b><br>'.$Cardetail["Driver_Name"].'</b></p>
						<p style="float: left;width: 25%;">วัน/เดือน/ปีเกิด : <b><br>'.dateThai($Driver_DOB[0]).'</b></p>
						<p style="float: left; margin-left:20px; width: 15%;"> เลขที่บัตรประชาชน<b><br>'.$Cardetail["Receiver"].'</b></p>
						<p style="float: left; margin-left:20px; width: 15%;"> เลขที่ใบขับขี่<b><br>'.$Cardetail["Driver_License_No"].'</b></p>
					</div>';
			if($Cardetail["Co_Driver_Name"]){
				$html .= '	
						<div>
							<p style="float: left; width: 40%;">ผู้ขับขี่ 2 : <b><br>'.$Cardetail["Co_Driver_Name"].'</b></p>
							<p style="float: left;width: 25%;">วัน/เดือน/ปีเกิด : <b><br>'.dateThai($co_Driver_DOB[0]).'</b></p>
							<p style="float: left; margin-left:20px; width: 15%;"> เลขที่บัตรประชาชน<b><br>'.$Cardetail["Sender"].'</b></p>
							<p style="float: left; margin-left:20px; width: 15%;"> เลขที่ใบขับขี่<b><br>'.$Cardetail["Co_Driver_License_No"].'</b></p>
						</div>';
			}else{
				$html .= '	
						<div>
							<p style="float: left; width: 40%;">ผู้ขับขี่ 2 <br> .................................................................................</p>
							<p style="float: left; width: 25%;">วัน/เดือน/ปีเกิด <br> .........................................</p>
							<p style="float: left; margin-left:10px; width: 15%;"> เลขที่บัตรประชาชน <br> ..................................</p>
							<p style="float: left; margin-left:10px; width: 15%;"> เลขที่ขับขี่ <br> ..................................</p>
						</div>';
			}
	}else{
		$html .= '
				<div>
					<p style="float: left; width: 40%;">ผู้ขับขี่ 1 <br> .................................................................................</p>
					<p style="float: left; width: 25%;">วัน/เดือน/ปีเกิด <br> .........................................</p>
					<p style="float: left; margin-left:10px; width: 15%;"> เลขที่บัตรประชาชน <br> ..................................</p>
					<p style="float: left; margin-left:10px; width: 15%;"> เลขที่ขับขี่ <br> ..................................</p>
				</div>
				<div>
					<p style="float: left; width: 40%;">ผู้ขับขี่ 2 <br> .................................................................................</p>
					<p style="float: left; width: 25%;">วัน/เดือน/ปีเกิด <br> .........................................</p>
					<p style="float: left; margin-left:10px; width: 15%;"> เลขที่บัตรประชาชน <br> ..................................</p>
					<p style="float: left; margin-left:10px; width: 15%;"> เลขที่ขับขี่ <br> ..................................</p>
				</div>';
	}
$html .= '
				</div>
				<div class="bor_r bor_l bor_b  p5" style="clear: both;">ผู้รับผลประโยชน์ : '.$Cardetail["Beneficiary_Name"].'</div>
				<div class="bor_l bor_r bor_b">
					<p class="ml5 pt5 mb5 ">
						<b>รายการรถยนต์ที่เอาประกันภัย</b> 
						<input type="checkbox" '.$repairG.'>ซ่อมอู่ &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<input type="checkbox" '.$repairC.'>ซ่อมห้าง
					</p>
					<div>
						<table class="tableCar fs16" style="width:100%;" border="0" cellspacing="0">
							<thead>
								<tr >
									<th>รหัส</th>
									<th>ยี่ห้อรถ</th>
									<th>รุ่น</th>
									<th>ทะเบียน</th>
									<th>เลขตัวถัง</th>
									<th>ปี รุ่น</th>
									<th>แบบตัวถัง</th>
									<th style="border-right:none;">ที่นั่ง/ขนาด/น้ำหนัก</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td style="text-align:center; width: 5%;">'.$Cardetail["Car_Type_ID"].'</td>
									<td style="text-align:center; width: 10%;">'.$Cardetail["Make_Name_TH"].'</td>
									<td style="text-align:center; width: 15%;">'.$Cardetail["Model_Desc"].'</td>
									<td style="text-align:center; width: 25%;">'.$Cardetail["Plate_No"].' '.getProvinceBycode($Cardetail["Plate_Province"]).'</td>
									<td style="text-align:center; width: 15%;">'.$Cardetail["Frame_No"].'</td>
									<td style="text-align:center; width: 10%;">'.$Cardetail["Sub_Model"].'</td>
									<td style="text-align:center; width: 10%;">'.$Cardetail["Body_Type"].'</td>
									<td style="text-align:center; width: 10%; border-right:none;">'.$Cardetail["Seat_CC_Weight"].'</td>
								</tr>
							</tbody>
						</table>
						<p class="ml5 pt5 mb5 ">รายการตกแต่งเปลี่ยนแปลงรถยนต์เพิ่มเติม (โปรดระบุรายละเอียด) : '.$Cardetail["Accessory"].'</p>
					</div>
				</div>
				<div class="bor_r bor_l bor_b  p5" style="clear: both;">
					จำนวนเงินเอาประกันภัย : กรมธรรม์ประกันภัยนี้ให้การคุ้มครองเฉพาะข้อตกลงคุ้มครองที่มีจำนวนเงินเอาประกันภัยระบุไว้เท่านั้น
				</div>
				<div class=" bor_r " style="clear: both;">
					<div class=" bor_b bor_l">
						<div class="boxCoverage bor_r fwb"><p class="mt5 t_c mb5">ความรับผิดต่อบุคคลภายนอก</p></div>
						<div class="boxCoverage bor_r fwb" ><p class="mt5 t_c mb5">รถยนต์เสียหาย สูญหาย ไฟไหม้</p></div>
						<div class="boxCoverage fwb"><p class="mt5 t_c mb5">ความคุ้มครองตามเอกสารแนบท้าย</p></div>
					</div>
					<div >
						<div class="boxCoverage bor_r bor_l" style="height:180px;">
							<div class="p5">
								<p>1) ความเสียหายต่อชีวิต ร่างกาย หรืออนามัยเฉพาะส่วนเกินวงเงิน สูงสุดตาม พ.ร.บ. </p>
							';
								foreach ($getCoverageItem as $key => $data) {
									if($data["Coverage_Item_ID"] == "502"){ 
										$html .= '
										<div>
											<p style="float: left;width: 70%; text-align:right; margin-right: 20px;"><b>'.number_format($data["Coverage_Value"],2).'</b> </p>
											<p style="float: left;width: 20%; text-align:right;">บาท/ครั้ง</p> 
										</div>';
									}
								}
								foreach ($getCoverageItem as $key => $data) {
									if($data["Coverage_Item_ID"] == "503"){ 
										$html .= '
										<div>
											<p style="float: left;width: 70%; text-align:right; margin-right: 20px;"><b>'.number_format($data["Coverage_Value"],2).'</b> </p>
											<p style="float: left;width: 20%; text-align:right;">บาท/ครั้ง</p> 
										</div>';
									}
								}
								foreach ($getCoverageItem as $key => $data) {
									if($data["Coverage_Item_ID"] == "504"){ 
										$html .= '
										<div style="clear: both;">
											<p>2) ความเสียหายต่อทรัพย์สิน</p>
											<p style="float: left;width: 70%; text-align:right; margin-right: 20px;"><b>'.number_format($data["Coverage_Value"],2).'</b> </p>
											<p style="float: left;width: 20%; text-align:right;">บาท/ครั้ง</p> 
										</div>';
									}
								}
								foreach ($getCoverageItem as $key => $data) {
									if($data["Coverage_Item_ID"] == "505"){ 
										$html .= '
										<div class="ml10" style="clear: both;">
											<p>2.1) ความเสียหายส่วนแรก</p>
											<p style="float: left;width: 70%; text-align:right; margin-right: 20px;"><b>'.number_format($data["Coverage_Value"],2).'</b> </p>
											<p style="float: left;width: 20%; text-align:right;">บาท/ครั้ง</p> 
										</div>';
									}
								}
								$html .= '
							</div>
						</div>
						<div class="boxCoverage " style="height:180px;">
							<div class="p5">';
								foreach ($getCoverageItem as $key => $data) {
									if($data["Coverage_Item_ID"] == "536"){ 
										$html .= '
										<div style="clear: both;">
											<p>1) ความคุ้มครองความเสียหายต่อรถยนต์</p>
											<p style="float: left;width: 70%; text-align:right; margin-right: 20px;"><b>'.number_format($data["Coverage_Value"],2).'</b> </p>
											<p style="float: left;width: 20%; text-align:right;">บาท/ครั้ง</p> 
										</div>';
									}
								}
								foreach ($getCoverageItem as $key => $data) {
									if($data["Coverage_Item_ID"] == "506"){ 
										$html .= '
										<div class="ml10" style="clear: both;">
											<p class="ml10">1.1) ความเสียหายส่วนแรก</p>
											<p style="float: left;width: 70%; text-align:right; margin-right: 21px;"><b>'.number_format($data["Coverage_Value"],2).'</b> </p>
											<p style="float: left;width: 20%; text-align:right;">บาท/ครั้ง</p> 
										</div>';
									}
								}
								foreach ($getCoverageItem as $key => $data) {
									if($data["Coverage_Item_ID"] == "507"){ 
										$html .= '
										<div style="clear: both;">
											<p>2) รถยนต์สูญหาย/ไฟไหม้</p>
											<p style="float: left;width: 70%; text-align:right; margin-right: 20px;"><b>'.number_format($data["Coverage_Value"],2).'</b> </p>
											<p style="float: left;width: 20%; text-align:right;">บาท/ครั้ง</p> 
										</div>';
									}
								}
							$html .= '
								
							</div>
						</div>
						<div class="boxCoverage bor_l " style="height:180px;">
							<div class="p5">
								<div>
									<p>1) อุบัติเหตุส่วนบุคคล</p>
								</div>
								<div class="ml5" style="clear: both;">
									<p>1.1 เสียชีวิต สูญเสียอวัยวะ ทุพพลภาพถาวร</p>';
									foreach ($getCoverageItem as $key => $data) {
										if($data["Coverage_Item_ID"] >= "512" && $data["Coverage_Item_ID"] <= "519"){ 
											$html .= '
											<div style="clear: both;">
												<p style="float: left;width: 40%;margin-left: 10px;">ก) ผู้ขับขี่ 1 คน</p>
												<p style="float: left;width: 30%; text-align:right;margin-right: 20px;"><b>'.number_format($data["Coverage_Value"],2).'</b> </p>
												<p style="float: left;width: 20%; text-align:right;">บาท/ครั้ง</p>

												<p style="float: left;width: 40%;margin-left: 10px;">ข) ผู้โดยสาร '.$data["Remark"].' คน</p>
												<p style="float: left;width: 30%;text-align:right;margin-right: 20px;"><b>'.number_format($data["Coverage_Value"],2).'</b> </p>
												<p style="float: left;width: 20%; text-align:right;">บาท/คน</p>
											</div>';
										}
									}
								$html .= '
									
								<div class="ml5" style="clear: both;">
									<p>1.2 ทุพพลภาพชั่วคราว</p>
									<p style="float: left;width: 40%;margin-left: 10px;">ก) ผู้ขับขี่ 1 คน</p>
									<p style="float: left;width: 30%;text-align:right;margin-right: 20px;"><b> 0.00</b> </p>
									<p style="float: left;width: 20%; text-align:right;">บาท/คน</p>

									<p style="float: left;width: 40%;margin-left: 10px;">ข) ผู้โดยสาร - คน</p>
									<p style="float: left;width: 30%;text-align:right;margin-right: 20px;"><b> 0.00</b> </p>
									<p style="float: left;width: 20%; text-align:right;">บาท/คน</p>
								</div>
								';
								foreach ($getCoverageItem as $key => $data) {
									if($data["Coverage_Item_ID"] >= "520" && $data["Coverage_Item_ID"] <= "527"){ 
										$html .= '
										<div style="clear: both;">
											<p style="float: left;width: 44.5%;">2) ค่ารักษาพยาบาล</p>
											<p style="float: left;width: 30%; text-align:right;margin-right: 20px;"><b>'.number_format($data["Coverage_Value"],2).'</b> </p>
											<p style="float: left;width: 20%; text-align:right;">บาท/ครั้ง</p>
										</div>';
									}
								}
								foreach ($getCoverageItem as $key => $data) {
									if($data["Coverage_Item_ID"] == "532"){ 
										$html .= '
										<div style="clear: both;">
											<p style="float: left;width: 44.5%;">3) การประกันตัวผู้ขับขี่</p>
											<p style="float: left;width: 30%; text-align:right;margin-right: 20px;"><b>'.number_format($data["Coverage_Value"],2).'</b> </p>
											<p style="float: left;width: 20%; text-align:right;">บาท/ครั้ง</p>
										</div>';
									}
								}
							$html .= '
							</div>
						</div>
					</div>
			</div>
			<div class="bor_t bor_l bor_b fs18 p5" style="clear: both;">
				<p><b>ประเภทประกัน  : </b> '.$getPO["Insurance_Name"].'</p>
				<p><b>วันเริ่มคุ้มครอง</b> : '.$getPO["Coverage_Start_Date"]->format('d/m/Y').' | <b>วันสิ้นสุดความคุ้มครอง</b> : '.$getPO["Coverage_End_Date"]->format('d/m/Y').'</p>
				<p class="fs20 mt5 t_r"><b>เบี้ยประกันสุทธิ  : </b> '.number_format($getPO["pcNetPremium"],2).' บาท </p>
				<p class="fs20 mt5 t_r"><b>เบี้ยรวม  : </b> '.number_format($getPO["pcPremium"],2).' บาท </p>
			</div>
			<div class=" bor_l bor_b fs16 p5" style="clear: both;">
				<p><b>หมายเหตุการแจ้งงาน</b></p>
				<p>'.$getPO["Deductible"].'</p>
			</div>
			';
$html .= '
			
		</div>
		';

// echo $html;
// echo $html;
$mpdf->SetWatermarkText('Asia Direct');
$mpdf->showWatermarkText = true;
$mpdf->WriteHTML($html);
if($_GET["case"] == "D"){
	$mpdf->Output("ADBIR-".$pocode.".pdf","D");
}else{
	$mpdf->Output("ADBIR-".$pocode.".pdf","I");
}
exit;

?>