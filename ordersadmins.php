<?php 
session_start();
// echo "<pre>".print_r($_SESSION,1)."</pre>";
// exit();
if(!$_SESSION["User"]['UserCode']){
	echo "<script>alert('กรุณาเข้าสู่ระบบ!!!');</script>";
	echo '<META http-equiv="refresh" content="0;URL=login.php">';
	exit();
} 
if($_SESSION["User"]['type'] != "Admin" && $_SESSION["User"]['type'] != "SuperAdmin"){
	echo '<META http-equiv="refresh" content="0;URL=chkinser.php">';
	exit();
}
header('Content-Type: text/html; charset=utf-8');
include "include/inc_header.php"; 
include "include/inc_menu.php"; 

$UserCode = $_SESSION["User"]['UserCode'];
$getInsurerByAdmin = getInsurerByAdmin($UserCode);
$status = $_GET["filter"] ? $_GET["filter"] : "'2','3'" ;
if($_GET["filter"] || !$_GET["pocode"]){
	if($_SESSION["User"]['type'] == "SuperAdmin"){
		$getSendPOByAdmin = getSendPOByAdmin("All",$status, "");
	}else{	
		$getSendPOByAdmin = getSendPOByAdmin($UserCode,$status, "");
	}
}else if($_GET["pocode"]){
	$getSendPOByAdmin = getSendPOByAdmin("All",$status,$_GET["pocode"]);
}
// echo "<pre>".print_r($getSendPOByAdmin,1)."</pre>";
?> 
<div class="main">
	<div class="main-content p20">
		<div class=" bgff"> 
			<div class="p20">
			<!-- <div>
				<h3 >รายการแจ้งงาน</h3> 
				<span> <b>บริษัทที่ดูแล </b>|</span>
				<?php foreach ($getInsurerByAdmin as $key => $value) {
						echo "<span>".$value["insurer_initials"]."</span> | ";
					}
				 ?>
				</div> -->
				<div class="row">
					<div class="col-md-2 mb15">
						<label for="filterStatus">สถานะ</label>
						<select class="filterStatus form-control fs12" data-tableId="table1" onchange="filterTable()" name="filterStatus">
							<!-- <option value="">ทั้งหมด</option> -->
							<option value="2" <?php if($status == 2){ echo "selected";} ?> >รอแจ้งงาน</option>
							<option value="3" <?php if($status == 3){ echo "selected";} ?>>แจ้งงาน</option>
							<option value="4" <?php if($status == 4){ echo "selected";} ?>>เสร็จ</option>
						</select>
					</div>
				<!-- <div class="col-md-2 mb15">
					<label for="filter">อื่นๆ</label>
					<input type="text" class="filterAll form-control fs12">
				</div> -->
				<div class="col-md-2 mb15">
					<label for="filter">PO</label>
					<input type="text" class=" form-control fs12" id="pocodeSe" value="<?php echo $_GET["pocode"];  ?>">
				</div>
				<div class="col-md-2 mb15"><span class="btn btn-success mt20" onclick="filterPOcode()">ค้นหา</span></div>
			</div>
			<table id="table" class="table table-striped p5" data-toggle="table" data-detail-view="true" data-pagination="true" data-page-size="100" data-page-list="[100, 150, 200, 250, 300]">
				<thead>
					<tr>
						<th class="t_c"><i class="fa fa-list-ul" aria-hidden="true"></i></th>
						<th class="t_c wpno" style="background-color: #ffffff8a;"><i class="fa fa-edit"></i></th>
						<th data-field="statusType" data-sortable="true" class="t_c">สถานะ</th> 
						<th data-field="noti_work_code" class="t_c dn"></th>
						<th data-field="pocode" data-sortable="true" class="t_c">PO</th>
						<th class="t_c">ชื่อ-นามสกุล</th>
						<th class="t_c">แพจเกต</th>
						<th data-field="start_cover_date" data-sortable="true" class="t_c">วันคุ้มครอง</th>
						<th class="t_c">ทุนประกัน</th>
						<th class="t_c">เบี้ยรวม</th>
						<th class="t_c" data-sortable="true"  >ผู้ขาย</th>
						<!-- <th class="t_c">ผู้ตรวจ</th> -->
						<th data-field="created_date" data-sortable="true" class="t_c">วันที่ทำรายการ</th>
						<!-- <th data-field="datetime_chk" data-sortable="true" class="t_c">เวลาตรวจ</th> -->
					</tr>
				</thead>
				<tbody class="fs12">
					<?php 
							//if($apiMain){
					foreach ($getSendPOByAdmin as $key => $value) { 
						if($value["urgent"] == "1"){
							$bgColor = "class='bgNEW'";
						}else if($value["start_cover_date"] == date("Y-m-d")){
							$bgColor = "class='bgbaf4bc'";
						}else{
							$bgColor = "";
						}

						if($value["enable"] == "0"){
							$enable = "<span style='color: #f44;'>ยกเลิก</span>";
						}else{
							$enable = "<span style='color: #009710;'>เปิดใช้</span>";
						}

						if($value["endorse"] == "1"){
							$endorse = "<span style='color: #f44;'>*สลักหลัง</span>";
						}else{
							$endorse ="";
						}
						?>
						<tr <?php echo $bgColor; ?> data-status="<?php echo $value["status"]; ?>" data-inser="<?php echo $value["insuere_company"]; ?>">
								<!-- <td>
										<a href="sendordersviews.php?pocode=<?php echo $value["po_code"];  ?>&numcode=<?php echo $value["noti_work_code"];  ?>" style="color: #003cff;"><i class="fa fa-search-plus" aria-hidden="true"></i></a>
									</td> -->
									<td>
										<a href="workout.php?pocode=<?php echo $value["po_code"];  ?>&numcode=<?php echo $value["noti_work_code"];  ?>"><i class="fas fa-sun cff2da5"></i></a>
									</td>
									<td class="nowrap t_c" >
										
										<?php if($value["status"] != 1){ ?>
											<span class="fs14 btnselecttitle" title="ตีกลับ" style="color: #ffac00;" onclick="changStatus('<?php echo $value["noti_work_id"]?>','<?php echo $value["noti_work_code"]; ?>',1);"><i class="fa fa-undo"></i></span>  
										<?php }if($value["status"] != 3){ ?>
											| <span class="fs14 btnselecttitle" title="กำลังแจ้งงาน" style="color: #003cff;" onclick="changStatus('<?php echo $value["noti_work_id"]?>','<?php echo $value["noti_work_code"]; ?>',3);"><i class="fa fa-hourglass-end"></i></span>
										<?php }if($value["status"] != 4 && $value["status"] != 2){ ?>
											| <span class="fs14 btnselecttitle" title="แจ้งงานเสร็จสิ้น" style="color: #ff0000;" onclick="changStatus('<?php echo $value["noti_work_id"]?>','<?php echo $value["noti_work_code"]; ?>',4);"><i class="fa fa-bell"></i></span>
										<?php } ?>
										<div><a href="admin_email.php?code=<?php echo $value["noti_work_code"]; ?>" target="_blak" style="color: #FF5722;" class="fs14 btnselecttitle" ><i class="fas fa-envelope"></i></a></div>
									</td>
									<td>
										<div><?php echo setStatus($value["status"]); ?></div>
										<div style="color: #ff0000;" class="fs14 fwb" ><?php echo $endorse; ?></div>
									</td>
									<td class="t_l dn"><?php echo $value["noti_work_code"]; ?></td>
									<td class=" t_l rowpo"> 
										<div class="c2457ff fwb"><?php echo $value["po_code"]; ?></div>
										<div class="clearfix fs10"><?php echo $value["noti_work_code"]; ?></div>
									</td>
									<td class="t_l">
										<div><b><?php echo $value["cus_name"]; ?></b></div>
										<?php if($value["status"] == 4) { echo "<div class='c9c00c8'>".$value["policy_number"]."</div>"; } ?>
										<div class="cff2da5 fwb fs14"><?php echo $value["operation_type"] ? "++".$value["operation_type"]."++" : ""; ?></div>
									</td>
									<td class="t_l">
										<div class="clearfix">
											<span class="c2457ff"><?php echo $value["insuere_company"]; ?></span> | <span class="cf40053"><?php echo $value["insurance_type"]; ?></span></div>
											<div class="clearfix c9c00c8"><?php echo $value["package_name"]; ?></div>
											<div class="clearfix"><?php echo $value["send_type"] ? "<b>การส่ง :</b> ".$value["send_type"] : ""; ?></div>
										</td>
										<td class="c2457ff t_c fwb"><?php echo date("d-m-Y",strtotime($value["start_cover_date"])); ?></td>
										<td class="t_r"><?php echo number_format($value["insuere_cost"],2); ?></td>
										<td class="t_r">
											<div class="clearfix "><b>สุทธิ: </b><?php echo number_format($value["netpremium"],2); ?></div>
											<div class="clearfix "><b>รวมภาษี: </b><?php echo number_format($value["premium"],2); ?></div>
											<div class="clearfix "><b>จ่าย: </b><?php echo number_format($value["taxamount"],2); ?></div>
										</td>
										<td class="t_l">
											<div class="clearfix c2457ff "><?php echo $value["personnel_name"]; ?></div>
											<div class="fs10 cf80000"><?php echo $value["created_date"]; ?></div>
										</td>
										<td class="t_l nowrap fs10">
											<div class="cff2da5">เริ่ม: <?php echo $value["datetime_send_open"]; ?></div>
											<div class="c00ac0a">เสร็จ: <?php echo $value["datetime_send"]; ?></div>
										</td>
										<span style="display: none;" id="desc<?php echo $key; ?>">
											<div class="row" id="dataBox<?php echo $key; ?>"></div>
										</span>
									</tr>
								<?php }
						//} ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	.
</div>


<?php include "include/inc_footer.php"; ?> 
<!-- <script src="//code.jquery.com/jquery-3.2.1.min.js"></script> -->
<!-- <script src="fancybox/dist/jquery.fancybox.min.js"></script> -->
<!-- Magnify Image Viewer CSS -->
<link href="magnifybox/css/jquery.magnify.css" rel="stylesheet">
<!-- Magnify Image Viewer JS -->
<script src="magnifybox/js/jquery.magnify.js"></script>
<script type="text/javascript">
	var $table = $('#table');
	$table.on('expand-row.bs.table', function (e, index, row, $detail) {
		var noti_id = row.noti_work_code; 
		var res = $("#desc" + index).html(); 
		$.ajax({ 
			url: 'include/inc_action_chk.php',
			type:'POST',
			data: {action:"getImageOrNote", noti_id:noti_id},
			success:function(rs){
			// console.log(rs);
			$detail.html(rs);
		}
		});
	});
	function queryParams() {
		return {
			type: 'owner',
			sort: 'updated',
			direction: 'desc',
			per_page: 1,
			page: 1
		};
	}

	function filterTable(val){
		var filterStatus = $(".filterStatus").val();
		if(filterStatus){
			window.location.href = "ordersadmins.php?filter="+filterStatus;
		}
	}

	$(".filterAll").keyup(function() {
		var searchText = $(this).val().toLowerCase();
		$.each($('tbody tr'), function() {
			if($(this).text().toLowerCase().indexOf(searchText) === -1)
				$(this).hide();
			else
				$(this).show();                
		});
	});

	function filterPOcode(){
		pocodeSe = $("#pocodeSe").val();
		console.log(pocodeSe);
		if(pocodeSe){
			window.location.href = "ordersadmins.php?pocode="+pocodeSe;
		}else{
			alert("กรุณากรอก PO เพื่อค้นหา");
		}
	}

	function changStatus(id, codes, status){
// console.log(codes);
if(status == 1){
	txt = "ต้องการยกเลิก รายการแจ้งงานนี้";
}else if(status == 3){
	txt = "ยืนยันการรับงาน";
}else if(status == 4){
	txt = "ยืนยันการแจ้งงานสมบูรณ์";
}
if(confirm(txt)){
	$.ajax({ 
		url: 'include/inc_action_chk.php',
		type:'POST',
		data: {action: 'upDateStatus', id:id, code:codes, status:status},
		success:function(rs){ 
			window.location.reload(true);
		}
	});
}

}

function btnComments(e){
	var val = e.id;
	code = val.split("ments_");
	mentID = code[1];
	mentTxt = $("#comment_"+mentID).val();
// console.log("#listMent_"+mentTxt);
if(mentTxt != 0){
	$.ajax({ 
		url: 'include/inc_action_chk.php',
		type:'POST',
		data: {action: 'addComments', noti_work_code:mentID, comment:mentTxt},
		success:function(rs){
			// console.log("#listMent_"+mentID);
			$("#listMent_"+mentID).empty();
			$("#comment_"+mentID).val("");
			$("#listMent_"+mentID).append(rs);
		}
	});
}else{
	alert("กรุณากรอก Comment");
}
}

// $( '[data-fancybox="watermark"]' ).fancybox({
// 	buttons : [
// 	'download',
// 	'zoom',
// 	'close'
// 	],
// 	protect : false,
// 	animationEffect : "zoom-in-out",
// 	transitionEffect : "fade",
// 	zoomOpacity : "auto",
// 	animationDuration : 500,
// 	zoomType: 'innerzoom',

// });

</script>