<?php 

session_start();
$UserCode = $_SESSION["User"]['UserCode'];
$userType = $_SESSION["User"]['type'];

if(!isset($UserCode)){
	echo "<script>alert('กรุณาเข้าสู่ระบบ!!!');</script>";
	echo '<META http-equiv="refresh" content="0;URL=login.php">';
	exit();
}
if($UserCode != "ADB59009" && $_SESSION["User"]['type'] != "SuperAdmin"){
	echo '<META http-equiv="refresh" content="0;URL=index.php">';
	exit();
}
header('Content-Type: text/html; charset=utf-8');
include "include/inc_header.php"; 
include "include/inc_menu.php"; 

$status = $_GET["filter"] ? $_GET["filter"] : 0 ;
$getGiftvoucher = getGiftvoucher($status);


// echo "<pre>".print_r($getGiftvoucher,1)."</pre>";
?>
<div class="main">
	<div class="main-content p20">
		<div class="p20">
				<div class="row">
					<div class="col-md-12">
						<div class="panel">
								<div class="panel-heading">
									<h4 class="panel-title"><b>ของสัมมนาคุณ</b></h4>
								</div>
								<div class="panel-body fs14">
									<div class="row mt20">
										<div class="col-md-2 mb15">
												<label for="filterStatus">สถานะ</label>
												<select class="filterStatus form-control fs12" data-tableId="table1" onchange="filterTable()" name="filterStatus">
													<!-- <option value="">ทั้งหมด</option> -->
												  <option value="0" <?php if($status == 0){ echo "selected";} ?> >รอเบิก</option>
												  <option value="1" <?php if($status == 1){ echo "selected";} ?>>เบิกแล้ว</option>
												</select>
										</div>
										<div class="col-md-2 mb15">
											<label for="filter">อื่นๆ</label>
											<input type="text" class="filterAll form-control fs12">
										</div>
										<div class="col-md-12">
												<table class="table table-hover" id="tableMoveQC">
													<thead>
														<tr>
															<th data-field="statusType" class="t_c">สถานะ</th>
															<th data-field="noti_work_code" class="t_c dn"></th>
															<th data-field="pocode" data-sortable="true" class="t_c">PO</th>
															<th class="t_c">ชื่อ-นามสกุล</th>
															<th class="t_c">แพจเกต</th>
															<th data-field="start_cover_date" data-sortable="true" class="t_c">วันคุ้มครอง</th>
															<th class="t_c">ผู้ขาย</th>
															<th class="t_c">ของสัมมานคุณ</th> 
															<th class="t_c">ของสัมมานคุณให้</th> 
															<th data-field="created_date" data-sortable="true" class="t_c">วันที่ทำรายการ</th>
															<th class="t_c"></th> 
														</tr>
													</thead>
													<tbody>
														<?php $i=1;  foreach ($getGiftvoucher as $key => $value) { ?>
																<tr >
																	<td class="wpno t_c"><?php echo setStatus($value["status"]); ?></td>
																	<td class="t_l dn"><?php echo $value["noti_work_code"]; ?></td>
																	<td class=" t_l"> 
																		<b><?php echo $value["po_code"]; ?></b>
																		<div class="clearfix fs10"><?php echo $value["noti_work_code"]; ?></div>
																	</td>
																	<td class="t_l <?php echo $bgColor; ?>"><b><?php echo $value["cus_name"]; ?></b></td>
																	<td class="t_l">
																		<div class="clearfix fs10 c2457ff"><?php echo $value["insuere_company"]; ?></div>
																		<div class="clearfix c9c00c8"><?php echo $value["package_name"]; ?></div>
																		<div class="clearfix fs10 "><?php echo $value["insurance_type"]; ?></div>
																	</td>
																	<td class="t_c "><?php echo $value["start_cover_date"]; ?></td>
																	<td class="t_c ">
																			<div class="clearfix c2457ff "><?php echo $value["personnel_name"]; ?></div>
																			<div class="clearfix fs10 "><?php echo $value["send_type"] ? "<b>การส่ง :</b> ".$value["send_type"] : ""; ?></div>
																	</td>
																	<td ><?php echo $value["giftvoucher"]; ?></td>
																	<td class="t_c "><?php echo $value["giftvoucher_po"]; ?></td>
																	<td class="t_c "><?php echo $value["created_date"]; ?></td>
																	<td class="t_c ">
																		<?php if($value["status_giftvoucher"] == 0){ ?>
																		<spaen class="cursorPoin wpno" style="color: #ff2da5;" onclick="changStatus(<?php echo $value["noti_work_id"]; ?>)" >เบิกแล้ว</spaen>
																		<?php } ?>
																	</td>
																</tr>
														<?php } ?>
													</tbody>
												</table>
										</div>
									</div>
								</div>
						</div>
					</div>
				</div>
		</div>
	</div>
</div>
<?php include "include/inc_footer.php"; ?> 
<script type="text/javascript">
	
function filterTable(val){
	var filterStatus = $(".filterStatus").val();
	if(filterStatus){
		 window.location.href = "rechkgiftvouchers.php?filter="+filterStatus;
	}
}
$(".filterAll").keyup(function() {
   var searchText = $(this).val().toLowerCase();
    $.each($('tbody tr'), function() {
      if($(this).text().toLowerCase().indexOf(searchText) === -1)
         $(this).hide();
      else
         $(this).show();                
    });
});

function changStatus(rs){
	console.log(rs);
	if( confirm("เบิกของสัมมนาคุณเรียบร้อย?") ){
		$.ajax({ 
			url: 'include/inc_action_chk.php',
			type:'POST',
			data: {action: 'changStatusGift', id:rs},
			success:function(rs){
				// window.location.reload(true);
			}
		});
	}
}
</script>