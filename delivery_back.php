<?php
session_start();
ini_set('upload_max_filesize', '512M');
ini_set("memory_limit","512M");
if(!isset($_SESSION["User"]['UserCode'])){
	echo "<script>alert('กรุณาเข้าสู่ระบบ!!!');</script>";
	echo '<META http-equiv="refresh" content="0;URL=login.php">';
	exit();
}
header('Content-Type: text/html; charset=utf-8');
include "include/inc_header.php"; 
include "include/inc_menu.php"; 
include "deliveryback/inc_function_delivery.php";

$year = date("Y");
if($_SESSION["User"]['type'] == "Sale"){
	$case = $_SESSION["User"]['UserCode']; 
}

if(!$_GET["po"]){
	$_GET["txtse"] = ($_GET["txtse"]) ? $_GET["txtse"] : date("Y-n");
	$_GET["statusTxt"] = ($_GET["stxt"]) ? $_GET["stxt"] : "";
	$getDeliveryBackAll = getDeliveryBackAll($case, $_GET["txtse"], "", $_GET["statusTxt"]);

}else{
	$getDeliveryBackAll = getDeliveryBackAll($case, $_GET["txtse"], $_GET["po"]);
}
// echo "<pre>".print_r($getDeliveryBackAll,1)."</pre>";
?>

<?php if($_SESSION["User"]['type'] != "Sale"){?>
<div class="main">
	<div class="main-content p20">
		<div class=" bgff"> 
			<div class="p20">
				<div class="" id="fromSendWork">
					<div class="row p15">
						<div class="col-md-6 bgfffbd8 p15">
							<div class="col-md-4" class="text-r">
								<div class="form-group">
									<input class="form-control formInput2" type="text" name="po_code_search" id="po_code_search" placeholder="เลข PO " required>
								</div>
							</div>
							<div class="col-md-1 text-r">
								<div class="form-group" class="text-c">
									<button type="button" class="btn btn-success" data-toggle="modal" data-target="#ModalAdd"><i class="fa fa-search-plus" aria-hidden="true"></i>  ค้นข้อมูล</button>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php } ?>

<div class="<?php if($_SESSION["User"]['type'] == "Sale"){ echo "main"; }?> clearb">
	<div class="main-content p20">
		<div class=" bgff"> 
			<div class="row p20">
				<div class="col-md-2">
					<span class="fs18 cff2da5 mr15">เดือน</span>
					<select name="mounth" id="mounthOpt" class="form-control formInput2 " >
						<option value="">:: กรุณาเลือก ::</option>
						<option value="2020-10" <?php if($_GET["txtse"] == "2020-10"){ echo "selected"; } ?>>10 October 2020</option>
						<option value="2020-11" <?php if($_GET["txtse"] == "2020-11"){ echo "selected"; } ?>>11 November 2020</option>
						<option value="2020-12" <?php if($_GET["txtse"] == "2020-12"){ echo "selected"; } ?>>12 December 2020</option>
						<?php for($i=1; $i<=12; $i++) { ?>
							<option value="<?php echo date("Y-n", strtotime($year."-".$i."-01"));?>" <?php if($_GET["txtse"] ==  date("Y-n", strtotime($year."-".$i."-01"))){ echo "selected"; } ?>>
								<?php echo date("m F Y", strtotime($year."-".$i."-01")) ?>
							</option>
						<?php } ?>
					</select>
				</div>
				<div class="col-md-4">
					<span class="fs18 cff2da5 mr15">สถานะ</span>
					<select name="statustxt" id="statustxt" class="form-control formInput2">		
						<option value="">กรุณาเลือก</option>
						<option value="แอดมินรับเอกสารตีกลับ" <?php if($_GET["statusTxt"] ==  "แอดมินรับเอกสารตีกลับ"){ echo "selected"; } ?> >แอดมินรับเอกสารตีกลับ</option>
						<option value="แจ้งSalesรับทราบ" <?php if($_GET["statusTxt"] ==  "แจ้งSalesรับทราบ"){ echo "selected"; } ?>>แจ้งSalesรับทราบ</option>
						<option value="รอSalesตรวจสอบข้อมูลอีกครั้ง" <?php if($_GET["statusTxt"] ==  "รอSalesตรวจสอบข้อมูลอีกครั้ง"){ echo "selected"; } ?>>รอSalesตรวจสอบข้อมูลอีกครั้ง</option>
						<option value="รอแอดมินจัดส่งให้ใหม่" <?php if($_GET["statusTxt"] ==  "รอแอดมินจัดส่งให้ใหม่"){ echo "selected"; } ?>>รอแอดมินจัดส่งให้ใหม่</option>
						<option value="กำลังจัดส่ง" <?php if($_GET["statusTxt"] ==  "กำลังจัดส่ง"){ echo "selected"; } ?>>กำลังจัดส่ง</option>
						<option value="ลูกค้าติดต่อแจ้งที่อยู่แล้ว" <?php if($_GET["statusTxt"] ==  "ลูกค้าติดต่อแจ้งที่อยู่แล้ว"){ echo "selected"; } ?>>ลูกค้าติดต่อแจ้งที่อยู่แล้ว</option>
						<option value="ไม่สามาถรถติดตามลูกค้าได้" <?php if($_GET["statusTxt"] ==  "ไม่สามาถรถติดตามลูกค้าได้"){ echo "selected"; } ?>>ไม่สามาถรถติดตามลูกค้าได้</option>
					</select>
				</div>
				<div class="col-md-2">
					<span class=" cff2da5 mr15">PO</span>
					<input type="text" name="txt_po" id="txt_po" class="form-control formInput2" value="<?php echo $_GET["po"]; ?> ">
				</div>
				<div class="col-md-3">
					<input type="button" class="btn btn-success" onclick="searchPO()" value="ค้นหา PO">
					<input type="button" class="btn btn-warning" onclick="reset()" value="ยกเลิก">
					<sapn class="btn btn-info" onclick="btnExport()">Export</sapn>
				</div>
			</div>
			<div class="p20">
				<table class="table table-responsive table-hover table-striped table-bordered">
					<thead>
						<tr class="t_c">
							<th class="t_c">#</th>
							<th class="t_c">PO</th>
							<th class="t_c">ประเภท</th>
							<th class="t_c">วันที่รับเอกสาร</th>
							<th class="t_c">พนักงานขาย</th>
							<th class="t_c">Tracking Number</th>
							<th class="t_c">ที่อยู่</th>
							<th class="t_c">เหตุผลในการตีกลับ</th>
							<th class="t_c">ผลการตรวจสอบ</th>
							<th class="t_c">สถานะ</th>
							<th class="t_c">ผู้ตรวจสอบ/ผู้แจ้ง</th>
							<th class="t_c">หมายเหตุ</th>
						</tr>
					</thead>
					<tbody class="fs12">
						<?php foreach ($getDeliveryBackAll as $key => $value) { 
							if($value["Addr2"]){
								$eAddr = explode("|", $value["Addr2"]);
								if($eAddr[0]){
									$addr = "เลขที่ ".$eAddr[0];
								}if($eAddr[1]){
									$addr .= " อาคาร".$eAddr[1];
								}if($eAddr[2]){
									$addr .= " หมู่ที่ ".$eAddr[2];
								}if($eAddr[3]){
									$addr .= " ซอย ".$eAddr[3];
								}if($eAddr[4]){
									$addr .= " ถนน ".$eAddr[4];
								}
							}else{
								$addr = $value["Addr1"];
							}

							if($value["delivery_back_type"]=="policy"){
								$type = "กรมธรรม์";
							}else if($value["delivery_back_type"]=="act"){
								$type = "พรบ.";
							}else if($value["delivery_back_type"]=="noti"){
								$type = "ใบเตือน";
							}else if($value["delivery_back_type"]=="endorse"){
								$type = "สลักหลัง";
							}else if($value["delivery_back_type"]=="giftcard"){
								$type = "บัตรกำนัล";
							}
							
							$addr = $addr." <br>ตำบล/เขต ".$value["Subdistrict_Name_TH"]." อำเภอ/เขต ".$value["District_Name_TH"]." จังหวัด ".$value["Province_Name_TH"]." ".$value["Post_Code"];
						?>
							<tr>
								<td>
									<div class="fwb  cursorPoin c00ac0a" onclick="fnEditDelivery('<?php echo $value["delivery_back_id"]; ?>')">แก้ไข</div>
									<?php if($_SESSION["User"]['type'] != "Sale"){ ?>
									<div class="fwb fs14 cursorPoin cff0000" onclick="fnDelectDelivery('<?php echo $value["delivery_back_id"]; ?>')">ลบ</div>
									<?php } ?>
								</td>
								<td class="fwb t_c">
									<div class="cf40053 "><?php echo $value["po_id"]; ?></div>
								</td>
								<td class="fwb t_c nowrap c2457ff"><?php echo $type; ?></td>
								<td class="fwb t_c nowrap cff2da5"><?php echo $value["delivery_back_date"]->format("d-m-Y"); ?></td>
								<td class="nowrap"><?php echo $value["User_FName"]." ".$value["User_LName"]; ?></td>
								<td class="fwb t_c">
									<div><?php echo $value["tracking"]; ?></div>
									<div class="cf40053 "><?php echo $value["delivery_Type"]; ?></div>
								</td>
								<td><?php echo $addr; ?></td>
								<td><?php echo $value["reason"]; ?></td>
								<td><?php echo $value["result"]; ?></td>
								<td><?php echo $value["status"]; ?></td>
								<td>
									<div><?php echo $value["DBCreate_By"]; ?></div>
									<div><?php echo $value["DBCreate_Date"]->format("d-m-Y H:i:s"); ?></div>
								</td>
								<td><?php echo $value["note"]; ?></td>
							</tr>
						<?php } ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="ModalAdd" tabindex="-1" role="dialog" aria-labelledby="memberModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-send">
    <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
          <h4 class="modal-title" id="memberModalLabel">เพิ่ม จดหมายตีกลับ</h4>
      </div>
      <div class="dash"></div>
    </div>
  </div>
</div>


<div class="modal fade" id="ModalEdit" tabindex="-1" role="dialog" aria-labelledby="memberModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-send">
    <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
          <h4 class="modal-title" id="memberModalLabel">แก้ไข จดหมายตีกลับ</h4>
      </div>
      <div id="bodyModalEdit"></div>
    </div>
  </div>
</div>

<?php include "include/inc_footer.php"; ?> 

<script type="text/javascript">

	$("#mounthOpt").on('change', function() {
		if(this.value){
			window.location.href = "delivery_back.php?txtse="+this.value;
		}

  	});

	$("#statustxt").on('change', function() {
  		mounth = $("#mounthOpt").val();
  		statustxt = $("#statustxt").val();
		if(statustxt){
			window.location.href = "delivery_back.php?txtse="+mounth+"&stxt="+statustxt;
		}

  	});


	$('#ModalAdd').on('show.bs.modal', function (event) {
		var poCode = $("#po_code_search").val();
    var button = $(event.relatedTarget) // Button that triggered the modal
    var recipient = button.data('whatever') // Extract info from data-* attributes
    var modal = $(this);
    var dataString = 'poCode='+poCode;

    $.ajax({
    	type: "POST",
    	url: "deliveryback/daliveryadd.php",
    	data: dataString,
    	cache: false,
    	success: function (data) {
          modal.find('.dash').html(data);
        },
        error: function(err) {
        }
      });  
  });


  function fnEditDelivery(rs){
    $.ajax({ 
				url: 'deliveryback/daliveryedit.php',
				type:'POST',
				data: {delivery_back_id:rs},
				success:function(data){ 
					// console.log(data);
					$('#bodyModalEdit').html(data);
					$('#ModalEdit').modal('toggle');
				}
		});  
  }

	function fnDelectDelivery(rs){
		if(confirm("คุณต้องการลบรายการหรือไม่?")){
	  	$.ajax({ 
					url: 'deliveryback/inc_action_delivery.php',
					type:'POST',
					data: {action: 'deleteDelivery', delivery_back_id:rs},
					success:function(rs){ 
						// console.log(rs);
						if(rs == 1){
							alert("ลบรายการสำเร็๋จ");
							window.location.href = "delivery_back.php";
						}else{
							alert("ไม่สามารถทำรายการได้ กรุณาลองใหม่");
							window.location.href = "delivery_back.php";
						}
					}
			});
		}  
	}

	function searchPO(){
		po = $("#txt_po").val();
		console.log(po);
		if(po){
			window.location.href = "delivery_back.php?po="+po;
		}else{
			alert("กรุณาใส่ PO");
		}
	}
	
	function reset(){
		window.location.href = "delivery_back.php";
	}

	function btnExport(){
		txtse = $("#mounthOpt").val();
		if(txtse){
			window.open("deliveryback/deliveryback_export.php?txtse="+txtse);
			// window.location.href = "inscancel/inscancel_export.php?txtse="+txtse;
		}else{
			alert("กรุณาเลือกเดือนที่จะดึงข้อมูล");
		}
	}
</script>