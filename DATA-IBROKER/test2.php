
<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>jQuery UI Dialog with Form Attribute</title>

<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>


<!-- 
<link rel="stylesheet" href="https://www.tjvantoll.com/demos/assets/jquery-ui/1.10.2/jquery-ui.css">
<script src="https://www.tjvantoll.com/demos/assets/jquery/1.9.1/jquery.js"></script>
<script src="https://www.tjvantoll.com/demos/assets/jquery-ui/1.10.2/jquery-ui.js"></script>
 -->



</head>

<body>


    <form id="myForm" action="http://google.com">        
        <label for="search">Search For:</label>
        <input type="text" id="search" name="q">
        <button type="submit">Find</button>
    </form>


    <script>
        // Prevent the demo from stealing focus.
        //$.ui.dialog.prototype._focusTabbable = $.noop;

        $( "form" ).dialog({
            open: function() {
                // On open, hide the original submit button
                $( this ).find( "[type=submit]" ).hide();
            },
            buttons: [
                {
                    text: "Findaaa",
                    click: $.noop,
                    type: "submit",
                    form: "myForm"
                },
                {
                    text: "Close",
                    click: function() {
                        $( this ).dialog( "close" );
                    }
                }
            ]
        });
    </script>
</body>
</html>