
<?php
date_default_timezone_set('Asia/Bangkok');
header('Content-Type: text/html; charset=utf-8');
include "include/inc_header.php"; 
// include "include/inc_menu.php"; 
include "inc_qrcode/inc_function_qr.php";
include('inc_qrcode/qrcode/qrcode.class.php');

// echo "<br>http://localhost/insuercoop/showqr.php?code=NjExMTIxMTQxNTUxMDAwNDA=";
$code =  base64_decode($_GET["code"]);

if(isset($code)){
	$getListQRCode = getListQRCodeShowPage($code);
   // echo "<pre>".print_r($getListQRCode,1)."</pre>";
	if(!$getListQRCode){
		echo "<script>alert('ไม่พบข้อมูลกรุณา ติดต่อพนักงาน 02-089-2000 คะ');</script>";
		echo '<META http-equiv="refresh" content="0;URL=http://asiadirect.co.th">';
		exit();
	}else{
		$dataMain = $getListQRCode["dataMain"];
		$dataDesc = $getListQRCode["dataDesc"];
		$countDataDesc = count($dataDesc);
		$stausPay = ($dataMain["status_pay"]== 1) ? "ชำระแล้ว" : "รอชำระ" ;
		if (!in_array($err, array('L', 'M', 'Q', 'H'))) $err = 'L';
		$installment = str_pad($getListQRCode["dataDesc"][0]["installment_num"], 2, "0", STR_PAD_LEFT);
		$code = $dataMain["qrCode"];
		$due_date = $dataDesc[0]["installment_due_date"]->format("d/m/Y");
		$title = $dataMain["reference_1"]." ".$dataMain["reference_2"]." ".$dataMain["sumAmount"]." ".$installment." ".$due_date; 
		// echo "<pre>".print_r($dataDesc,1)."</pre>";
	  $url = "inc_qrcode/qrcode/image.php?msg=".urlencode($code)."&amp;err=".urlencode($err)."&amp;title=".urlencode($title);
	}
// $paycose = $getListCounterAll[0]["paycode"];



}


?>

	<nav class="clearfix navbar navbar-default navbar-fixed-top">
		<div class="container">
			<div class="row" >
				<div class="col align-self-center logoName ">
					<div class="text-center title_logo cffffff"><a href="index.php" class="cffffff">เอเชียไดเร็ค อินชัวรันส์ โบรคเกอร์</a></div>
				</div>
			</div>
		</div>
	</nav>
	<div class="clearfix container mt60 fontqr " >
		<div class="row mt10" >
			<div class="insNametop"><p>พร้อมเพย์ QRCode </p></div>
			<div class="row " style="background-color: #e0f6ff;margin: 0;padding: 25px 0px;">
				<div class="col-md-6 text-center mt20">
					
					<?php if($dataMain["status_pay"]== 0){?>
						<div>
							<?php if($dataMain["status_pay"] == 0 && $dataMain["enable"] == 'Y'){?> 
								<div style="padding:10 px;">
									<a href="<?php echo $url ?>" data-fancybox="QRCode" class="fancybox" title="<?php echo $dataMain["reference_1"] ?>">
										<img src="<?php echo $url ?>" alt="generation qr-code" title="<?php echo $dataMain["reference_1"] ?>" name="<?php echo $dataMain["reference_1"] ?>">
									</a>
									<p class="mt20"><a download="<?php echo $dataMain["reference_1"].".png" ?>" href="<?php echo $url ?>" title="<?php echo $dataMain["reference_1"] ?>" class="fs18 fwb cff2da5" ><b> >> โหลดรูปภาพ << </b></a></p>
								</div>
							<?php }else{?>
								<div class="fs18 cff2da5">ยกเลิกการชำระ <br><br>กรุณาติดต่อ พนักงานขาย 02-089-2000</div>
							<?php } ?>	
						</div>
					<?php }else{?>
						<div class="t_l" rowspan="<?php echo $countDataDesc;?>">
							<?php if($dataMain["receive_id"]){?>
								<div class="cff8000 fs16 fwb t_c"><?php echo $dataMain["receive_id"]; ?></div>
								<div class="fa12 t_c"><?php echo $dataMain["paymentTime"]->format("d/m/Y H:i:s"); ?></div>
								<div class="fa16"><b>PayCode:</b> <span class="cff2da5"><?php echo $dataMain["paymentId"]; ?></span></div>
								<div class="fa16"><b>ConfirmId:</b> <span class="cff2da5"><?php echo $dataMain["confirmId"]; ?></span></div>
								<div class="fa12"><b>PayCodeTime:</b> <span class="cff2da5"><?php echo $dataMain["paymentTime"]->format("d/m/Y H:i:s"); ?></span></div>
							<?php } ?>
						</div>
					<?php } ?>
				</div>
				<div class="col-md-6 mt20">
					<div class="fwb fs18 ">รายละเอียด</div>
					<div class="t_c fwb fs18 "><span >สถานะ : </span><span class="cff2da5"><?php echo $stausPay; ?></span></div>
					<div class="t_c fwb fs18 mt15"><span >ยอดเงิน : </span><span class="cff2da5"><?php echo number_format($dataMain["sumAmount"],2); ?></span> บาท</div>
					<div class="mt15 mb15 p10" style="border-top: 4px dotted #ddd;border-bottom: 4px dotted #ddd;">
						<div class="fwb">รายละเอียด</div>
						<div><b>รหัสลูกค้า: </b><?php echo $dataMain["customer_id"]; ?></div>
						<?php foreach ($dataDesc as $key => $value) { ?>
								<div class="fwb mt5">
									<span class="cff2da5"><?php echo $value["po_id"]; ?></span> 
									<span><?php echo $value["customer_name"]; ?></span>
									<span class="fr dib"><?php echo number_format($value["amount"],2); ?></span>
								</div>
								<div class="mt5"><span class="fwb">ชำระวันที่: </span><?php echo $value["installment_due_date"]->format("d/m/Y"); ?></div>
						<?php } ?>
					</div>
					<div>
						<div class=""><b>Ref.1</b> <?php echo $dataMain["reference_1"]; ?></div>
						<div class=""><b>Ref.2</b> <?php echo $dataMain["reference_2"]; ?></div>
						<div class=""><b>Transaction</b> <?php echo $dataMain["transactionId"]; ?></div>
						<div><b>เวลาสร้าง: </b><?php echo $dataMain["created_date"]->format("d/m/Y H:i:s"); ?> | <b>โดย: </b><?php echo $dataMain["created_by"]; ?></div>
						<div class="fs10"></div>
					</div>
				</div>
			</div>
			<div class="insNamebottom "><p>ขอบคุณที่ใช้บริการ โทร. 02-089-2000</p></div>
		</div>
	</div>

<div class="bgff mt15 fontqr">
	<div class="clearfix container ">
		<div class="row" style="margin: 20px 0px 20px 0px;">
			<div class="col-xs-12 col-sm-7 col-md-7">
				<div style="font-size: 16px;">
					<h4 class="c0aaaef fw">บริษัท เอเชียไดเร็ค อินชัวรันส์ โบรคเกอร์ จำกัด</h4>
					<h4 class="fw mt20">สำนักงานใหญ่</h4>
					<div>เลขที่ 626 อาคารบีบีดี (พระราม 4) ชั้น 11 ถนนพระรามที่ 4 </div>
					<div>แขวงมหาพฤฒาราม เขตบางรัก กรุงเทพฯ 10500</div>
					<div>โทร. <a href="tel:020892000" class="c00a9f1">02-089-2000</a> | <a href="https://line.me/ti/p/%40bnl8398c">Line@</a> | <a href="https://www.facebook.com/AsiaDirectBroker/">Facebook</a></div>
					<div style="margin-top: 15px;text-align: center;color: #ff8000;font-size: 20px">เปิดให้บริการ วันจันทร์-วันเสาร์ เวลา 8.30-17.00 น.</div>
				</div>
			</div>
			<div class="col-xs-12 col-sm-5 col-md-5">
				<div><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3875.747288511422!2d100.5255115!3d13.733744!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xd890ff1b78bfdca3!2z4LmA4Lit4LmA4LiK4Li14Lii4LmE4LiU4LmA4Lij4LmH4LiEIOC5guC4muC4o-C4hOC5gOC4geC4reC4o-C5jCAtIGFzaWFkaXJlY3QuY28udGg!5e0!3m2!1sth!2s!4v1508925919086" width="100%" height="200" frameborder="0" style="border: 5px solid #fff;border-radius: 10px;" allowfullscreen></iframe>
				</div>
			</div>
		</div>
	</div>
</div>
<div>
	<div class="clearfix fontqr" style="background-color: #00a9f1;padding-bottom: 10px;">
		<div  class="row ">
			<div class="col-xs-12 col-sm-12 col-lg-12 text-center">
				<div class="menufooter">
					<a class="footer_a" href="https://www.asiadirect.co.th">หน้าแรก</a> | 
					<a class="footer_a" href="https://www.asiadirect.co.th/whyadb.php">ทำไมต้อง Asia Direct</a> | 
					<a class="footer_a" href="https://www.asiadirect.co.th/adbcertificates.php">หนังสือรับรอง</a> | 
					<a class="footer_a" href="https://www.asiadirect.co.th/privacy_policy.php">นโยบายส่วนบุคคล</a> | 
					<a class="footer_a" href="https://www.asiadirect.co.th/contactus.php">ติดต่อเรา</a>
				</div>	
			</div>
		</div>
	</div>
</div>
<script src="assets/vendor/jquery/jquery.min.js"></script>
<script src="fancybox/dist/jquery.fancybox.min.js"></script>
<script>
	$( '[data-fancybox="QRCode"]' ).fancybox({
  buttons : [
    'download',
    'zoom',
    'close'
  ],
  protect : false,
  animationEffect : "zoom-in-out",
  transitionEffect : "fade",
  zoomOpacity : "auto",
  animationDuration : 500,
  zoomType: 'innerzoom',

});
</script>
