<?php 

session_start();
$UserCode = $_SESSION["User"]['UserCode'];
$userType = $_SESSION["User"]['type'];

if(!isset($UserCode)){
	echo "<script>alert('กรุณาเข้าสู่ระบบ!!!');</script>";
	echo '<META http-equiv="refresh" content="0;URL=login.php">';
	exit();
}
header('Content-Type: text/html; charset=utf-8');
include "include/inc_header.php"; 
include "include/inc_menu.php"; 


$getINSRoadside = getINSRoadside();
// echo "<pre>".print_r($getEmailInsur,1)."</pre>";
?>
<div class="main">
	<div class="main-content p20">
		<div class="p20">
				<div class="row">
					<div class="col-md-12">
						<div class="panel">
								<div class="panel-body fs14">
									<div class="row mt20">
										<div class="col-md-12">
											<table class="table table-hover" id="tableMoveQC">
												<thead>
													<tr>
														<th class="t_c">ลำดับ</th>
														<th class="t_c">บริษัทประกัน</th>													
													</tr>
												</thead>
												<tbody>
													<?php $i=1; 
													foreach ($getINSRoadside as $value) { ?>
														<tr >
															<td class="t_c" width="2%">
																<?php echo $i++; ?>
															</td>
															<td >
																<div><b><span onclick="showRoadside('<?php echo $value["insurance_Name"]?>')" class="cursorPoin"></i> <?php echo $value["insurance_Name"]?></span></b>
																</div>
															</td>
														</tr>
													<?php } ?>
												</tbody>
											</table>
										</div>
									</div>
								</div>
						</div>
					</div>
				</div>
		</div>
	</div>
</div>
<div class="modal fade" id="Roadside" tabindex="-1" role="dialog" aria-labelledby="RoadsideTitle" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<p class="modal-title fs16 fwb cff8000 " id="memberModalLabel">บริการช่วยเหลือฉุกเฉิน<span id="insName"></span> บริษัท <span id="insClass"></span></p>
			</div>
			<div class="">
				<div class="p20">
					<div class="row">
						<div class="col-md-12">
							<!-- <div >
								<p class="fwb fs18 c2457ff ">สายด่วน/ เบอร์ติดต่อ</p>
								<p class="mt15 lh20" id="RoadsideContact"></p>
							</div> -->
							<div >
								<p class="fwb fs18 c2457ff ">รายละเอียด</p>
								<p class="mt15 lh20" id="RoadsideDescription"></p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php include "include/inc_footer.php"; ?> 
<link href="magnifybox/css/jquery.magnify.css" rel="stylesheet">
<script src="magnifybox/js/jquery.magnify.js"></script>
<script type="text/javascript">
function showRoadside(insName){
		console.log("insName");
		$("#insClass").html("");
		$.ajax({ 
			url: 'include/inc_action.php',
			type:'POST',
			// dataType: 'json',
			// data: {action:'getRoadsideIns',insName:insName, insClass:insClass},
			data: {action:'getRoadsideIns',insName:insName},
			success:function(rs){
				console.log(rs);
				$("#RoadsideContact, #RoadsideDescription").empty();
				// $("#RoadsideContact").html(rs);
				$("#RoadsideDescription").html(rs);
				$("#insClass").html(insName);
			}
		});
		$('#Roadside').modal();
	}
</script>