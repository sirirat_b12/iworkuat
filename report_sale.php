<?php 
session_start();
ini_set('max_execution_time', 186);
ini_set('memory_limit', '2048M');
$UserCode = $_SESSION["User"]['UserCode'];
$userType = $_SESSION["User"]['type'];

if(!isset($UserCode)){
	echo "<script>alert('กรุณาเข้าสู่ระบบ!!!');</script>";
	echo '<META http-equiv="refresh" content="0;URL=login.php">';
	exit();
}

header('Content-Type: text/html; charset=utf-8');
include "include/inc_header.php"; 
include "include/inc_menu.php"; 
include "reports/inc_function_report.php";
$year = date("Y");
$getMonthSelect = getMonthSelect();
$getstaticSale = getstaticSale($_GET["months"]);
if($_GET["user"]){
	$getDetailPOBySale = getDetailPOBySale($_GET["user"], $_GET["months"]);
}
// echo "<pre>".print_r($getstaticSale,1)."</pre>";
?>

<div class="main">
	<div class="">
		<div class="p20">
				<div class="row">
					<div class="col-md-12">
						<div class="panel">
								<div class="panel-heading">
									<h4 class="panel-title"><b>รายงาน Sale <?php echo "ปี ".$year;?></b></h4>
								</div>
								<div class="panel-body fs14">
									<div class="row">
										<div class="col-md-3 ">
											<span class="fs18 cff2da5 mr15">เดือน</span>
											<select name="mounth" id="mounthOpt" class="form-control formInput2 dib" style="width: 80%">
												<option value="">:: กรุณาเลือก ::</option>
												<?php foreach ($getMonthSelect as $key => $value) { ?>
													<option value="<?php echo $value["months"];?>" <?php if($_GET["months"] ==  $value["months"]){ echo "selected"; } ?>>
														<?php echo date("F", strtotime($year."-".$value["months"]."-01")) ?>
													</option>
												<?php } ?>
											</select>
										</div>
									</div>
									<div class="row mt20">
										<div class="col-md-12">
											<div class="row">
												<div class="col-md-2"></div><div class="col-md-10"><?php echo $value["PO_ID"]; ?></div>
											</div>
											<?php if($getstaticSale){?>
												<table class="table table-hover table-bordered" id="tableMoveQC">
													<thead>
														<tr class="bgbaf4bc">
															<th class="t_c">รหัส</th>
															<th class="t_c">ขื่อผู้ขาย</th>
															<th class="t_c">จำนวนทั้งหมด</th>
															<th class="t_c">ใช้งาน</th>
															<th class="t_c">ยกเลิก</th>
															<th class="t_c">แจ้งงานสำเร็จ</th>
															<!-- <th class="t_c">ส่วนลด</th>
															<th class="t_c">ยอดรวม</th> -->
														</tr>
													</thead>
													<tbody id="table_data" class="table-list fs13">
														<?php 
															foreach ($getstaticSale as $key => $value) { 
																$numallTotal = $numallTotal+$value["numall"];
																$enable1Total = $enable1Total+$value["enable_1"];
																$enable0Total = $enable0Total+$value["enable_0"];
																$status4Total = $status4Total+$value["status_4"];
																$discountTotal = $discountTotal+$value["discount"];
																$taxamountTotal = $taxamountTotal+$value["taxamount"];
														?>
															<tr class="">
																<td	class="t_c"><?php echo $value["personnel_code"] ?></td>
																<td class="t_c"><?php echo $value["personnel_name"] ?></td>
																<td class="t_c ">
																	<span class="cursorPoin cff2da5" onclick="getDetailPO('<?php echo $value["personnel_code"];?>','<?php echo $_GET["months"];?>')"><?php echo $value["numall"] ?></span>
																</td>
																<td class="t_c"><?php echo $value["enable_1"] ?></td>
																<td class="t_c"><?php echo $value["enable_0"] ?></td>
																<td class="t_c"><?php echo $value["status_4"] ?></td>
																<!-- <td class="t_r"><?php echo number_format($value["discount"],2) ?></td>
																<td class="t_r"><?php echo number_format($value["taxamount"],2) ?></td> -->
															</tr>
														<?php } ?>
															<tr class="fwb bgNEW">
																<td	class="t_r" colspan="2">รวม</td>
																<td class="t_c"><?php echo number_format($numallTotal) ?></td>
																<td class="t_c"><?php echo number_format($enable1Total) ?></td>
																<td class="t_c"><?php echo number_format($enable0Total) ?></td>
																<td class="t_c"><?php echo number_format($status4Total) ?></td>
															<!-- 	<td class="t_r"><?php echo number_format($discountTotal,2) ?></td>
																<td class="t_r"><?php echo number_format($taxamountTotal,2) ?></td> -->
															</tr>
													</tbody>
												</table>
											<?php } ?>
											<?php if($_GET["user"]){?>
												<div class="row mt20">
													<table class="table table-hover" >
																<thead>
																	<tr>
																		<th class="t_c">#</th>
																		<th class="t_c">PO</th>
																		<th class="t_c" style="width: 10%">สถานะ</th>
																		<th class="t_c" style="width: 20%">ลูกค้า</th>
																		<th class="t_c" style="width: 10%">พนักงาน</th>
																		<th class="t_c" style="width: 10%">ผู้ตรวจ</th>
																		<th class="t_c" style="width: 25%">สรุปขั้นตอนตรวจ</th>
																	</tr>
																</thead>
																<tbody id="tableListDetail" class="table-list fs13">
																	<?php $i=1; foreach ($getDetailPOBySale as $key => $value) { 
																		$status = ($value["enable"]==1) ? "<span class='fwb c00ac0a'>ใช้งาน</span>" : "<span class='fwb cf80000'>ยกเลิก</span>";;
																	?>
																		<tr>
																			<td><?php echo $i++; ?></td>
																			<td class="t_l">
																				<div class="fwb cff2da5">
																					<a target="_Blank" class="cff2da5" href="sendordersviews.php?pocode=<?php echo $value["po_code"] ?>&numcode=<?php echo $value["noti_work_code"] ?>"><?php echo $value["po_code"] ?> | <?php echo $value["noti_work_code"] ?></a>
																				</div>
																				<div><?php echo $value["insurance_type"] ?> | <?php echo $value["insuere_company"] ?></div>
																				<div class="c2457ff"><?php echo $value["package_name"] ?></div>
																			</td>
																			<td class="t_c"><?php echo setStatusPO($value["status"])." | ".$status; ?></td>
																			<td class="t_l"><?php echo $value["cus_name"] ?></td>
																			<td class="t_t">
																				<div><?php echo $value["personnel_name"] ?></div>
																				<div><?php echo date("d-m-Y H:i:m",strtotime($value["created_date"])) ?></div>
																			</td>
																			<td class="t_t">
																				<div><?php echo $value["chk_code"] ?></div>
																				<div><?php echo $value["datetime_chk"] ?></div>
																			</td>
																			<td class="t_t">
																				<?php foreach ($value["steplists"] as $key => $valStep) { ?>
																					<div class=" c2457ff"> >> <?php echo $valStep["comment"] ?></div>
																					<div class="fs10"><b><?php echo $valStep["datetime"] ?></b> | <?php echo $valStep["comment_byname"] ?></div>
																				<?php } ?>
																			</td>
																		</tr>
																	<?php } ?>
																</tbody>
															</table>
												</div>
											<?php } ?>
										</div>
									</div>

								</div>
						</div>
					</div>
				</div>
		</div>
	</div>
</div>

<?php include "include/inc_footer.php"; ?> 
<script src="fancybox/dist/jquery.fancybox.min.js"></script>
<script type="text/javascript">

$("#mounthOpt").on('change', function() {
	if(this.value){
		window.location.href = "report_sale.php?months="+this.value;
	}

});


<?php if($_GET["months"]){?>
function getDetailPO(user, date){
	txtse = <?php echo $_GET["months"]; ?>;
	if(txtse){
		window.location.href = "report_sale.php?months="+txtse+"&user="+user;
	}
}
<?php } ?>

</script>