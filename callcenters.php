<?php
session_start();
ini_set('upload_max_filesize', '512M');
ini_set("memory_limit","512M");
if(!isset($_SESSION["User"]['UserCode'])){
	echo "<script>alert('กรุณาเข้าสู่ระบบ!!!');</script>";
	echo '<META http-equiv="refresh" content="0;URL=login.php">';
	exit();
}
header('Content-Type: text/html; charset=utf-8');
include "include/inc_header.php"; 
include "include/inc_menu.php"; 
include "callcenters/inc_function_call.php";


$year = date("Y");
if($_SESSION["User"]['type'] == "Sale"){
	$case = $_SESSION["User"]['UserCode']; 
}

$_GET["txtse"] = ($_GET["txtse"]) ? $_GET["txtse"] : date("Y-n");
$getcallcenters = getcallcenters($_GET["txtse"]);
// echo "<pre>".print_r($getDeliveryBackAll,1)."</pre>";
?>

<div class="main">
	<div class="p20">
		<div class="panel bgff"> 
			<div class="row p20">
				<div class="col-md-2">
					<span class="fs18 cff2da5 mr15">เดือน</span>
					<select name="mounth" id="mounthOpt" class="form-control formInput2 " >
						<option value="">:: กรุณาเลือก ::</option>
						<?php for($i=1; $i<=12; $i++) { ?>
							<option value="<?php echo date("Y-n", strtotime($year."-".$i."-01"));?>" <?php if($_GET["txtse"] ==  date("Y-n", strtotime($year."-".$i."-01"))){ echo "selected"; } ?>>
								<?php echo date("m F Y", strtotime($year."-".$i."-01")) ?>
							</option>
						<?php } ?>
					</select>
				</div>
				<div class="col-md-6">
					<?php if($_SESSION["User"]['type'] != "Sale"){?>
						<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#ModalAdd"><i class="fa fa-search-plus" aria-hidden="true"></i>  เพิ่มข้อมูล</button>
					<?php } ?>
				</div>
			</div>
			<div class="p20">
				<table class="table table-responsive table-hover table-striped table-bordered" data-search="true" data-toggle="table" data-pagination="true" data-page-size="100" data-page-list="[100, 200, 250, 300]">
					<thead>
						<tr class="table p5">
							<?php if($_SESSION["User"]['type'] != "Sale"){?>
							<th class="t_c">#</th>
							<?php } ?>
							<th class="t_c">ลำดับ</th>
							<th class="t_c">วันที่ติดต่อ</th>
							<th class="t_c">ชื่อลูกค้า</th>
							<th class="t_c">ชื่อพนักงาน</th>
							<th class="t_c">ช่องทาง</th>
							<th class="t_c">โทรศัพท์</th>
							<th class="t_c">เรื่องที่ติดต่อ</th>
							<th class="t_c">รายละเอียด</th>
						</tr>
					</thead>
					<tbody class="fs12">
						<?php
							if($getcallcenters){  
								$i = 1;
								foreach ($getcallcenters as $key => $value) { 

						?>
								<tr>
									<?php if($_SESSION["User"]['type'] != "Sale"){?>
									<td class="text-center">
										<div class="fwb fs14 cursorPoin cff0000" onclick="fnDelectDelivery('<?php echo $value["Call_ID"]; ?>')">ลบ</div>
									</td>
									<?php } ?>
									<td class="text-center"><?php echo $i++; ?></td>
									<td class="t_c nowrap ">
										<div class="cff2da5"><?php echo $value["Call_Date"]->format("d-m-Y H:i:s"); ?></div>
										<div><?php echo $value["User_FName"]." ".$value["User_LName"]; ?></div>
									</td>
									<td class="fwb c2457ff">
										<div><?php echo $value["PO_ID"]; ?></div>
										<div><?php echo $value["Customer_Name"]; ?></div>
									</td>
									<td class="fwb "><?php echo $value["Employee_Neme"]; ?></td>
									<td class="text-center c9c00c8"><?php echo $value["Channel"]; ?></td>
									<td class="text-center">
										<div><?php echo $value["Phone"]; ?></div>
										<div><?php echo $value["Email"]; ?></div>
									</td>
									<td><?php echo $value["Call_Subject"]; ?></td>
									<td class="t_l"><?php echo $value["Call_Detail"]; ?></td>
								</tr>
						<?php }
							} ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="ModalAdd" tabindex="-1" role="dialog" aria-labelledby="memberModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-send">
    <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
          <h4 class="modal-title" id="memberModalLabel">เพิ่ม การติดต่อ</h4>
      </div>
      <div class="dash"></div>
    </div>
  </div>
</div>


<?php include "include/inc_footer.php"; ?> 

<script type="text/javascript">
	

	$('#ModalAdd').on('show.bs.modal', function (event) {
	    var button = $(event.relatedTarget) // Button that triggered the modal
	    var recipient = button.data('whatever') // Extract info from data-* attributes
	    var modal = $(this);

	    $.ajax({
	    	type: "POST",
	    	url: "callcenters/callcentersadd.php",
	    	cache: false,
	    	success: function (data) {
	          modal.find('.dash').html(data);
	        },
	        error: function(err) {
	        }
	      });  
	  });

	function fnSearch(){
		txt_search = $("#txt_search").val();
		console.log(txt_search);
		if(txt_search){
			$.ajax({ 
				url: 'callcenters/inc_action_call.php',
				type:'POST',
				data: {action: 'getPOonPhone', txt_search:txt_search},
				success:function(rs){
					// console.log(rs)
					if(rs){
						data = JSON.parse(rs);
						$("#PO_ID").val(data.PO_ID);
						$("#Customer_ID").val(data.Customer_ID);
						$("#Customer_Name").val(data.Customer_FName+" "+data.Customer_LName);
						$("#Employee_ID").val(data.User_ID);
						$("#Employee_Neme").val(data.User_FName+" "+data.User_LName);

					if(data.Tel_No){
						$("#Call_Phone").val(data.Tel_No);
					}else{
						$("#Call_Phone").val(data.Mobile_No);
					}
						$("#Call_Email").val(data.EMail);
						// $("#Channel").val("Phone");
					}
				}
			});
		}else{
			alert("กรุณากรอกการค้นหา");
		}
	}

	function fnDelectDelivery(rs){
		console.log(rs);
		if(confirm("คุณต้องการลบรายการหรือไม่?")){
		  	$.ajax({ 
				url: 'callcenters/inc_action_call.php',
				type:'POST',
				data: {action: 'deleteCallcenter', Call_ID:rs},
				success:function(rs){ 
					if(rs == 1){
						alert("ลบรายการสำเร็๋จ");
						window.location.href = "callcenters.php";
					}else{
						alert("ไม่สามารถทำรายการได้ กรุณาลองใหม่");
						window.location.href = "callcenters.php";
					}
				}
			});
		}  
	}

	$("#mounthOpt").on('change', function() {
		if(this.value){
			window.location.href = "callcenters.php?txtse="+this.value;
		}

  	});
</script>