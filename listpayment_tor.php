<?php 

session_start();
$UserCode = $_SESSION["User"]['UserCode'];
$userType = $_SESSION["User"]['type'];

if(!isset($UserCode)){
	echo "<script>alert('กรุณาเข้าสู่ระบบ!!!');</script>";
	echo '<META http-equiv="refresh" content="0;URL=login.php">';
	exit();
}

header('Content-Type: text/html; charset=utf-8');
include "include/inc_header.php"; 
include "include/inc_menu.php"; 
include('qrcode/qrcode.class.php');

if(isset($_GET["po"])){
 $getInstallmentPO = getRowPOByCode($_GET["po"]); 
} 
// else $_GET['po'] = 'PO61001234';
// echo "<pre>".print_r($getInstallmentPO,1)."</pre>";
?>
<div class="main">
	<div class="main-content p20">
		<div class="p20">
				<div class="row">
					<div class="col-md-12">
						<div class="panel">							
								<div class="panel-heading">
									<h4 class="panel-title"><b>เอกสารผ่อน - ใบคำขอผ่อนชำระเบี้ยประกันภัย</b></h4>
									<div> 							
									</div>
								</div>
								<div class="panel-body fs14">
									<div class="row">
										<div class="col-md-4"></div>
										<div class="col-md-1 t_r"><h4>PO :</h4></div>
										<div class="col-md-2">
											<input class="form-control formInput2" type="text" name="po_code_search" id="po_code_search"  value="<?php echo $_GET["po"] ?>" required>
										</div>
										<div class="col-md-2">
											<button type="button" id="btn_send_order" class="btn  btn-info"><i class="fa fa-search-plus" aria-hidden="true"></i> เช็คข้อมูล</button>
										</div>
										
									</div>
									<br><br>
									<div class="row mt20">
										<div class="col-md-12">
											<div class="row">
												<!-- <div class="col-md-2"></div><div class="col-md-10"><?php echo $getInstallmentPO["PO_ID"]; ?></div> -->
											</div>
											<form action="include/inc_action_chk.php" method="POST" id="frmMoveQC">
												<table class="table table-hover" id="tableMoveQC">
													<thead>
														<tr>
															
															<th data-field="noti_work_code" class="t_c">PO</th>
															<th data-field="pocode" data-sortable="true" class="t_c">ประเภทประกัน</th>
															<th class="t_c">ชื่อ</th>
															<th class="t_c">วันเริ่มคุ้มครอง</th>
															<th class="t_r">เบี้ยสุทธิ</th>
															<th class="t_r">อากร</th>
															<th class="t_r">ภาษี</th>
															<th class="t_r">เบี้ยรวม</th>
															<th class="t_r">ส่วนลด</th>
															<th class="t_r">จ่ายสุทธิ</th>
															<th class="t_c">ผ่อนชำระ</th>															
														</tr>
													</thead>
													<?php if(isset($_GET["po"])){ ?>
													<tbody>
														<tr >					
															<td class="wpno t_c" style="width: 10%;"><?php echo $getInstallmentPO["PO_ID"]; ?></td>
															<td class="t_c ">[<?php echo $getInstallmentPO["Insurer_Initials"]; ?>]&nbsp;<?php echo $getInstallmentPO["Insurance_Name"]; ?></td>
															<td class="t_c "><?php echo $getInstallmentPO["FName"]; ?>&nbsp;<?php echo $getInstallmentPO["LName"]; ?></td>
															<td class="t_c "><?php echo $getInstallmentPO["Coverage_Start_Date"]->format("d/m/Y"); ?></td>
															<td class=" t_r"> <b><?php echo number_format($getInstallmentPO["Net_Premium"],2); ?></b></td>
															<td class=" t_r"> <b><?php echo number_format($getInstallmentPO["ISTM_Duty"],2); ?></b></td>
															<td class=" t_r"> <b><?php echo number_format($getInstallmentPO["ISTM_Tax"],2); ?></b></td>
															<td class=" t_r"> <b><?php echo number_format($getInstallmentPO["Total_Premium"],2); ?></b></td>
															<td class=" t_r"> <b><?php echo number_format($getInstallmentPO["Discount"],2); ?></b></td>
															<td class=" t_r"> <b><?php echo number_format($getInstallmentPO["Premium_After_Disc"],2); ?></b></td>
															<td class=" t_c"> <u><a class="cff2da5 fwb" data-fancybox="" data-type="iframe" href="http://61.90.142.230/adbchk/ibroker-attach/1POPUP/?poid=<?php echo $getInstallmentPO["PO_ID"]; ?>">เอกสารผ่อน</a></u></td>									
														</tr>														
													</tbody>
												<?php } ?>
												</table>
											</form>
										</div>
									</div>
								</div>
						</div>
					</div>
				</div>
		</div>
	</div>
</div>
<?php include "include/inc_footer.php"; ?> 
<script src="fancybox/dist/jquery.fancybox.min.js"></script>
<script type="text/javascript">

$(document).keypress(function(e) {
    if(e.which === 13) {
        // enter has been pressed, execute a click on .js-new:
        $("#btn_send_order").first().click();
    }
});


$('[data-fancybox]').fancybox({
	toolbar  : false,
	smallBtn : true,
	iframe : { 		preload : false 	}
});


$("#btn_send_order").click(function() {
	var poCode = $("#po_code_search").val();	
	if(poCode){
		window.location.href = "listpayment_tor.php?po="+poCode;
	}else{
		alert("กรุณากรอกข้อมูล PO ");
	}
});
  

$( '[data-fancybox="watermark"]' ).fancybox({
  buttons : [
    'download',
    'zoom',
    'close'
  ],
  protect : false,
  animationEffect : "zoom-in-out",
  transitionEffect : "fade",
  zoomOpacity : "auto",
  animationDuration : 500,
  zoomType: 'innerzoom',

});
</script>